% Lecture des dimensions d'une variable
%
% Syntax
%   [dim, nomDim] = get_varDim(a, ident)
%
% Input Arguments
%   a     : Une instance de la classe cl_netcdf
%   ident : Soit le numero de la variable, soit son nom
%
% Output Arguments
%   dim    : Dimensions de la variable
%   nomDim : Noms des dimensions
%
% Examples
%   fileName = getNomFicDatabase('example.nc')
%   a = cl_netcdf('fileName', fileName)
%   fileName = get(a, 'fileName')
%
%   [dim, nomDim] = get_varDim(a, 1)
%   [dim, nomDim] = get_varDim(a, 'lat')
%
% See also cl_netcdf cl_netcdf/set_value Authors
% Authors : JMA
% VERSION  : $Id: get.m,v 1.3 2003/09/02 09:59:25 augustin Exp augustin $
% ----------------------------------------------------------------------------

function [dim, nomDim] = get_varDim(this, nomVar)

dim = [];
nomDim = {};

if length(this) ~= 1
    my_warndlg('cl_netcdf/get_varDim : L''instance doit etre de dimension 1', 1);
    return
end

if ischar(nomVar)
    numVar = find_numVar(this, nomVar);
    if isempty(numVar)
        str = sprintf('"%s" not found in "%s"', nomVar, this.fileName);
        my_warndlg(str, 1);
        return
    end
elseif isnumeric(nomVar)
    numVar = nomVar;
    N = length(this.skeleton.var);
    if (numVar < 1) || (numVar > N)
        str = sprintf('"%s" has %d variables. numVar=%d is incompatible ', this.fileName, N, numVar);
        my_warndlg(str, 1);
        return
    end
else
    my_warndlg('nomVar must be a string or an integer', 1);
    return
end

nomsVarDim = this.skeleton.var(numVar).dim;
for i=1:length(nomsVarDim)
    nomDim1 = nomsVarDim(i).name;
    for k=1:length(this.skeleton.dim)
        if strcmp(nomDim1, this.skeleton.dim(k).name)
            dim(i)    = this.skeleton.dim(k).value; %#ok<AGROW>
            nomDim{i} = this.skeleton.dim(k).name; %#ok<AGROW>
        end
    end
end

