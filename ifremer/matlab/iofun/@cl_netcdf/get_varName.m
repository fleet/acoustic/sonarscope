% Lecture des dimensions d'une variable
%
% Syntax
%   VarName = get_varName(this, VarNum)
%
% Input Arguments
%   a      : Une instance de la classe cl_netcdf
%   VarNum : Soit le numero de la variable, soit son nom
%
% Output Arguments
%   VarName : Noms de la variable
%
% Examples
%   fileName = getNomFicDatabase('example.nc')
%   a = cl_netcdf('fileName', fileName)
%   fileName = get(a, 'fileName')
%
%   VarName = get_varName(a, 1)
%
% See also cl_netcdf Authors
% Authors : JMA
% VERSION  : $Id: get.m,v 1.3 2003/09/02 09:59:25 augustin Exp augustin $
% ----------------------------------------------------------------------------

function VarName = get_varName(this, VarNum)

if ischar(VarNum)
    VarNum = find_numVar(this, VarNum);
end

VarName = this.skeleton.var(VarNum).name;
