% Lecture du type d'une variable
%
% Syntax
%   VarType = get_varType(this, VarNum)
%
% Input Arguments
%   a      : Une instance de la classe cl_netcdf
%   VarNum : Soit le numero de la variable, soit son nom
%
% Output Arguments
%   VarType : Noms de la variable
%
% Examples
%   fileName = getNomFicDatabase('example.nc')
%   a = cl_netcdf('fileName', fileName)
%   fileName = get(a, 'fileName')
%
%   VarType = get_varType(a, 1)
%
% See also cl_netcdf Authors
% Authors : JMA
% VERSION  : $Id: get.m,v 1.3 2003/09/02 09:59:25 augustin Exp augustin $
% ----------------------------------------------------------------------------

function [VarType, MatlabType, valMin, valMax] = get_varType(this, VarNum)

if ischar(VarNum)
    VarNum = find_numVar(this, VarNum);
    if isempty(VarNum)
        VarType = [];
        MatlabType = [];
        valMin = [];
        valMax = [];
        return
    end 
end

VarType = this.skeleton.var(VarNum).type;

switch VarType
    case 'NC_CHAR'
        MatlabType = 'char';
        valMin = 0;
        valMax = 255;
    case 'NC_LONG'
        MatlabType = 'int32';
        valMin = intmin('int32');
        valMax = intmax('int32');
    case 'NC_SHORT'
        MatlabType = 'int16';
        valMin = intmin('int16');
        valMax = intmax('int16');
    case 'NC_FLOAT'
        MatlabType = 'single';
        valMin = realmin('single');
        valMax = realmax('single');
    case 'NC_DOUBLE'
        MatlabType = 'double';
        valMin = realmin('double');
        valMax = realmax('double');
    case 'NC_BYTE'
        valMin = 0;
        valMax = 255;
    otherwise
        my_warndlg([VarType ' pas trait� dans get_varType'], 1);
end
