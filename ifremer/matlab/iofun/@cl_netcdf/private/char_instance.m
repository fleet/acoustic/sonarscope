% Transformation en chaine de caracteres d'une instance
%
% Syntax
%   str = char_instance(a)
%
% Input Arguments
%   a : instance de cl_netcdf
%
% Output Arguments
%   str : une chaine de caracteres representative de l instance
%
% Examples
%   fileName = getNomFicDatabase('example.nc');
%   a = cl_netcdf('fileName', fileName);
%   str = char(a)
%
% See also cl_netcdf Authors
% Authors : JMA
% VERSION  : $Id: char_instance.m,v 1.4 2003/09/10 11:03:13 augustin Exp $
% ----------------------------------------------------------------------------

function str = char_instance(this)
    
str = [];
str{end+1} = sprintf('fileName <-> %s', this.fileName);

if isempty(this.skeleton)
    str{end+1} = sprintf('skeleton <-> []');
else
    sk = this.skeleton;
    str{end+1} = sprintf('skeleton <-> sk');
    for i=1:length(sk.att)
        value = sk.att(i).value;
        type = sk.att(i).type;
        str{end+1} = sprintf(' '); %#ok<AGROW>
        str{end+1} = sprintf('\t\tsk.att(%d).type   = ''%s'';', i, type); %#ok<AGROW>
        str{end+1} = sprintf('\t\tsk.att(%d).name   = ''%s'';', i, sk.att(i).name); %#ok<AGROW>
        switch type
            case {'ncchar'; 'javaChar'; 'NC_CHAR'}
                str{end+1} = sprintf('\t\tsk.att(%d).value = ''%s'';', i, value); %#ok<AGROW>
            case {'nclong'; 'ncshort'; 'ncint32'; 'ncint16'; 'NC_LONG'; 'NC_SHORT'}
                pppp = num2str(value, '%d');
                p = cellstr(pppp);
                s = sprintf('%s ', p{:});
                str{end+1} = sprintf('\t\tsk.att(%d).value = [%s];', i, s); %#ok<AGROW>
                % str{end+1} = sprintf('\t\tsk.att(%d).value = [%s];', i, num2str(value));
            case {'ncdouble'; 'ncfloat'; 'NC_SINGLE'; 'NC_DOUBLE'}
                pppp = num2str(value, '%f');
                p = cellstr(pppp);
                s = sprintf('%s ', p{:});
                str{end+1} = sprintf('\t\tsk.att(%d).value = [%s];', i, s); %#ok<AGROW>
                %str{end+1} = sprintf('\t\tsk.att(%d).value = [%s];', i, num2str(value));
            otherwise
                str{end+1} = sprintf('\t\tsk.att(%d).value = PAS ENCORE FAIT', i); %#ok<AGROW>
        end
    end
    
    for i=1:length(sk.dim)
        str{end+1} = sprintf(' '); %#ok<AGROW>
        str{end+1} = sprintf('\t\tsk.dim(%d).name  = ''%s'';', i, sk.dim(i).name); %#ok<AGROW>
        str{end+1} = sprintf('\t\tsk.dim(%d).value = %d;', i, sk.dim(i).value); %#ok<AGROW>
    end
    
    for i=1:length(sk.var)
        PourCent = '%';
        str{end+1} = sprintf(' '); %#ok<AGROW>
        N = cdf_get_numelVar(sk, sk.var(i).name);

        str{end+1} = sprintf('\t\tsk.var(%d).name  = ''%s''; %s (%d values)', i, sk.var(i).name, PourCent, N); %#ok<AGROW>
        if N < 20^2 
            Value = get_value(this, i);
            if ischar(Value)
                str{end+1} = sprintf('\t\t%s Value : %s', PourCent, Value'); %#ok<AGROW>
            else
                str{end+1} = sprintf('\t\t%s Value : %s', PourCent, num2strCode(Value)); %#ok<AGROW>
            end
        end
        str{end+1} = sprintf('\t\t%s value : lecture et ecriture par appel aux methodes :', PourCent); %#ok<AGROW>
        str{end+1} = sprintf('\t\t           %s v = get_value(a, ''%s'');   a = set_value(a, ''%s'', v);', PourCent, sk.var(i).name, sk.var(i).name); %#ok<AGROW>
        str{end+1} = sprintf('\t\t           %s v = get_value(a, %d);       a = set_value(a, %d, v);', PourCent, i, i); %#ok<AGROW>
        str{end+1} = sprintf('\t\t           %s get_value et set_value acceptent le sous echantillonage', PourCent); %#ok<AGROW>
        str{end+1} = sprintf('\t\t           %s get_value(a, ident, ''sub1'', sub1, ''sub2'', sub2, ...);', PourCent); %#ok<AGROW>
        str{end+1} = sprintf('\t\tsk.var(%d).type  = ''%s'';', i, sk.var(i).type); %#ok<AGROW>
        
        for j=1:length(sk.var(i).dim)
            nomDim = sk.var(i).dim(j).name;
            valDim = get_valDim(this, nomDim);
            PourCent = '%';
            str{end+1} = sprintf('\t\tsk.var(%d).dim(%d).name  = ''%s''; \t%s (value : %d)', i, j, nomDim, PourCent, valDim); %#ok<AGROW>
        end
        
        for j=1:length(sk.var(i).att)
            value = sk.var(i).att(j).value;
            % if strcmp(class(value),'ucar.ma2.ArrayObject$D1')
            sk.var(i).att(j).type  = ['NC_', upper(class(value))];
            type  = sk.var(i).att(j).type;
            str{end+1} = sprintf(' '); %#ok<AGROW>
            str{end+1} = sprintf('\t\tsk.var(%d).att(%d).name  = ''%s'';', i, j, sk.var(i).att(j).name); %#ok<AGROW>
            str{end+1} = sprintf('\t\tsk.var(%d).att(%d).type  = ''%s'';', i, j, type); %#ok<AGROW>
            switch type
                case {'ncchar'; 'char'; 'javachar';'NC_CHAR'}
                    str{end+1} = sprintf('\t\tsk.var(%d).att(%d).value = ''%s'';', i, j, value); %#ok<AGROW>
                case {'ncdouble'; 'double'; 'javadouble'}
                    pppp = num2str(value, '%f');
                    p = cellstr(pppp);
                    s = sprintf('%s ', p{:});
                    str{end+1} = sprintf('\t\tsk.var(%d).att(%d).value = [%s];', i, j, s); %#ok<AGROW>
                case {'ncfloat'; 'float'; 'javafloat'; 'NC_SINGLE'}
                    pppp = num2str(value, '%f');
                    p = cellstr(pppp);
                    s = sprintf('%s ', p{:});
                    str{end+1} = sprintf('\t\tsk.var(%d).att(%d).value = [%s];', i, j, s); %#ok<AGROW>
                    % str{end+1} = sprintf('\t\tsk.var(%d).att(%d).value = [%s];', i, j, num2str(value));
                case {'nclong'; 'long'; 'NC_LONG'}
                    pppp = num2str(value, '%l');
                    p = cellstr(pppp);
                    s = sprintf('%s ', p{:});
                    str{end+1} = sprintf('\t\tsk.var(%d).att(%d).value = [%s];', i, j, s); %#ok<AGROW>
                    % str{end+1} = sprintf('\t\tsk.var(%d).att(%d).value = [%s];', i, j, num2str(value));
                case {'ncint'; 'NC_INT32'}
                    pppp = num2str(value, '%d');
                    p = cellstr(pppp);
                    s = sprintf('%s ', p{:});
                    str{end+1} = sprintf('\t\tsk.var(%d).att(%d).value = [%s];', i, j, s); %#ok<AGROW>
                    % str{end+1} = sprintf('\t\tsk.var(%d).att(%d).value = [%s];', i, j, num2str(value));
                case {'ncshort'; 'short'; 'javaShort'; 'javaint'}
                    pppp = num2str(value, '%d');
                    p = cellstr(pppp);
                    s = sprintf('%s ', p{:});
                    str{end+1} = sprintf('\t\tsk.var(%d).att(%d).value = [%s];', i, j, s); %#ok<AGROW>
                    % str{end+1} = sprintf('\t\tsk.var(%d).att(%d).value = [%s];', i, j, num2str(value));
                otherwise
                    str{end+1} = sprintf('\t\tsk.var(%d).att(%d).value = %s', i, j, 'PAS ENCORE TRAITE'); %#ok<AGROW>
            end
        end
    end
end

% ---------------------------
% Concatenation en une chaine

str = cell2str(str);
