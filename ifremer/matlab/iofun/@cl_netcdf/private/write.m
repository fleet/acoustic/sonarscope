% Write a netCDF file from structure of data and format
%
% Syntax
%    status = write(this, s)
%
% Input Arguments
%   this       : instance of cl_netcdf_writer
%   s          : data and format structure
%
% Output Arguments
%   status = 1 or 0
%
% Examples
%    s = []; s.dim = []; s.att = []; s.var = [];
%
%    s.dim(1).name  = 'lat';
%    s.dim(1).value = 5;
%    s.dim(2).name  = 'lon';
%    s.dim(2).value = 10;
%
%    s.att(1).name  = 'target';
%    s.att(1).value = 'example';
%    %s.att(1).type  = 'ncchar';
%    s.att(2).name  = 'version';
%    s.att(2).value = [0,1,0];
%    %s.att(2).type  = 'ncfloat';
%
%    s.var(1).name  = 'lat';
%    s.var(1).type  = 'ncfloat';
%    s.var(1).dim(1).name  = s.dim(1).name;
%    s.var(1).att(1).name  = 'long_name';
%    s.var(1).att(1).value = 'latitude in degrees north';
%    s.var(1).value = linspace(0, 90, 5);
%
%    s.var(2).name  = 'lon';
%    s.var(2).type  = 'ncfloat';
%    s.var(2).dim(1).name  = s.dim(2).name;
%    s.var(2).att(1).name  = 'long_name';
%    s.var(2).att(1).value = 'longitude in degrees east';
%    % s.var(2).value = linspace(0, 180, 10);
%
%    s.var(3).name  = 'var';
%    s.var(3).type  = 'ncfloat';
%    s.var(3).dim(1).name  = s.dim(1).name;
%    s.var(3).dim(2).name  = s.dim(1).name;
%    s.var(3).dim(3).name  = s.dim(2).name;
%    % s.var(3).value = rand(5,5,10);
%
%    fileTemp = [tempname '.nc']
%    a = cl_netcdf('fileName', fileTemp, 'skeleton', s);
%
%    b = cl_netcdf('fileName', fileTemp);
%    get_value(b, 3)
%
% Remarks :
%
% See also
% Authors : DCF
% ----------------------------------------------------------------------------

function status = write(this)

status = 1;

if exist(this.fileName, 'file')
    try
        W = warning;
        warning('off')
        delete(this.fileName)
        warning(W);
    catch %#ok<CTCH>
    end
end

%% Open file in w mode

idFile = netcdf.create(this.fileName, 'NC_SHARE');
if isempty(idFile)
    status = 0;
    return
end

s = this.skeleton;

%% Dimensions

if isfield(s, 'dim')
    if isfield(s.dim, 'name') && isfield(s.dim, 'value')
        for i=1:length(s.dim)
            dimid = netcdf.defDim(idFile, s.dim(i).name, s.dim(i).value); %#ok<NASGU>
        end
    end
end

%% Attributs

if isfield(s, 'att')
    if isfield(s.att, 'name') && isfield(s.att, 'value')
        for i=1:length(s.att)
            attValue = s.att(i).value;
            if isempty(attValue)
                if ischar(attValue)
                    attValue = ' ';
                else
                    attValue = 0;
                end
            end
            netcdf.putAtt(idFile, -1, s.att(i).name, attValue); % -1 = getConstant('NC_GLOBAL') 
        end
    end
end

%% Variables

if isfield(s, 'var') && isfield(s.var, 'name') && isfield(s.var, 'dim') && isfield(s.var, 'type')
    for i=1:length(s.var)
        varName = s.var(i).name;
%         type = s.var(i).type(3:numel(s.var(i).type));
        
        dimid  = [];
        dimVal = [];
        for iDim=1:length(s.var(i).dim)
            [dimid(iDim), dimVal(iDim)] = find_numDim(this, s.var(i).dim(iDim).name); %#ok<AGROW>
        end
        
        DataType = s.var(i).type;
        varid = netcdf.defVar(idFile, s.var(i).name, DataType, dimid-1);

        % ----------------------
        % Attributs de variables

        missing_value = 0;
        if isfield(s.var(i), 'att')
            if isfield(s.var(i).att, 'name') && isfield(s.var(i).att, 'value')
                for j=1:length(s.var(i).att)
                    attName = s.var(i).att(j).name;
                    if strcmp(attName, '_FillValue')
                        attName = 'FillValue_';
                    end

                    attValue = s.var(i).att(j).value;
                    if isempty(attValue)
                        if ischar(attValue)
                            attValue = ' ';
                        else
                            attValue = 0;
                        end
                    end
                    netcdf.putAtt(idFile, varid, attName, attValue);
                end
            end
        end

        
        % ------------------------------------------------
        % Valeur de la variable d'initialisation
        % nbValDim = get_valDim(this, s.var(i).dim(1).name);

        netcdf.endDef(idFile);
        
        switch s.var(i).type
            case 'NC_CHAR'
                missing_value = char(missing_value);
            case 'NC_BYTE'
                missing_value = uint8(missing_value);
            case 'NC_SHORT'
                missing_value = int16(missing_value);
            case 'NC_LONG'
                missing_value = int32(missing_value);
            case 'NC_FLOAT'
            case 'NC_DOUBLE'
            otherwise
                str1 = sprintf('"%s" non pr�vu dans cl_netcdf/write', s.var(i).type);
                str2 = sprintf('"%s" non pr�vu dans cl_netcdf/write', s.var(i).type);
                my_warndlg(Lang(str1,str2), 1);
        end
        
        switch length(dimVal)
            case 0
                netcdf.putVar(idFile, varid, missing_value)
            case 1
                try
                    data = repmat(missing_value, [1 dimVal]);
                    netcdf.putVar(idFile, varid, data)
                catch %#ok<CTCH>
                    lengthBlock = 1e6;
                    data = repmat(missing_value, [1 lengthBlock]);
                    str1 = sprintf('Initializing variable "%s"', varName);
                    str2 = sprintf('Initializing variable "%s"', varName);
                    hw = create_waitbar(Lang(str1,str2), 'N', nBlocks);
                    nBlocks = floor(dimVal / lengthBlock);
                    for k=1:nBlocks
                        my_waitbar(k, nBlocks, hw)
                    	netcdf.putVar(idFile, varid, [0 (k-1)*lengthBlock], size(data), [1 1], data)
                    end
                    my_close(hw)
                    netcdf.putVar(idFile, varid, [0 dimVal-lengthBlock], size(data), [1 1], data)
                end
            otherwise
                try
                    data = repmat(missing_value, dimVal);
                    netcdf.putVar(idFile, varid, data)
                    %                     else
                catch %#ok<CTCH>
                    data = repmat(missing_value, [dimVal(1) 1]);
                    str1 = sprintf('Initialisation de la variable "%s"', varName);
                    str2 = sprintf('Initializing variable "%s"', varName);
                    N = dimVal(2);
                    hw = create_waitbar(Lang(str1,str2), 'N', N);
                    for iLig=1:N
                        my_waitbar(iLig, dimVal(2), hw)
                        netcdf.putVar(idFile, varid, [0 iLig-1], size(data), [1 1], data)
                    end
                    my_close(hw)
                end
        end
        
        % Re-enter define mode.
        netcdf.reDef(idFile);
    end
end

%% Close file

netcdf.close(idFile);
