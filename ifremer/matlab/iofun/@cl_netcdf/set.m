% Acces en ecriture des proprietes d'une instance
%
% Syntax
%   a = set(a, ...)
%
% Input Arguments
%   a : Une instance de la classe cl_netcdf
%
% Name-Value Pair Arguments
%   fileName : File name
%
% Output Arguments
%   [] : Sauve l'instance dans a
%   a  : Instance de la classe
%
% Examples
%   nomFic1 = getNomFicDatabase('example.nc')
%   a = cl_netcdf('fileName', nomFic1)
%   s = get(a, 'skeleton')
%
%   val = get_value(a, 1);
%   val = get_value(a, 'lat');
%
%   nomFic2 = tempname
%   b = cl_netcdf
%   b = set(b, 'fileName', nomFic2)
%   b = set(b, 'skeleton', s)
%
% See also cl_netcdf cl_netcdf/get Authors
% Authors : JMA, GLU
% ----------------------------------------------------------------------------

function varargout = set(this, varargin)

%% On verifie que l'instance est bien de demension 1

if length(this) ~= 1
    my_warndlg('cl_netcdf/get : L''instance doit etre de dimension 1', 1);
    return
end

%% Affectation des valeurs ne necessitant pas de verification

[varargin, fileName] = getPropertyValue(varargin, 'fileName', []);
[varargin, skeleton] = getPropertyValue(varargin, 'skeleton', []); %#ok<ASGLU>

if ~isempty(fileName)
    this.fileName = fileName;
    if isempty(skeleton)
        this.skeleton = get_skeleton(this);
    end
end

if ~isempty(skeleton)
    this.skeleton =  skeleton;
    status = write(this);
    % Ajout des dimensions et attributs ajout�s par CIB_DOU lors de
    % la cr�ation d'un fichier.
    %     nDim = length(this.skeleton.dim);
    %     nAtt = length(this.skeleton.att);
    %     this.skeleton.dim(nDim+1).name = 'CIB_BLOCK_DIM';
    %     this.skeleton.dim(nDim+1).value = 1024;
    %     this.skeleton.att(nAtt+1).name = 'CIB_BLOCKS_FOR_UNLIMITED';
    %     this.skeleton.att(nAtt+1).value = 1;
    if status ~= 1
        my_warndlg('Le fichier n''a pas ete cree', 1);
    end
end

%% Sortie

if nargout == 0
    assignin('caller', inputname(1), this);
else
    varargout{1} = this;
end


function s = get_skeleton(this)

if exist(this.fileName, 'file')
    try
        idFile = netcdf.open(this.fileName, 'NC_NOWRITE');
    catch %#ok<CTCH>
        s = [];
        return
        
    end
    if isempty(idFile)
        s = [];
        return
    end
else
    s = [];
    return
end

%% Determine contents of the file

[ndims, nvars, natts, dimm] = netcdf.inq(idFile); %#ok<ASGLU>

%% Dimensions

s.dim = [];
for k=1:ndims
    [s.dim(k).name, s.dim(k).value] = netcdf.inqDim(idFile,k-1);
end

%% Attributs

s.att = [];
for k=1:natts
    s.att(k).name = netcdf.inqAttName(idFile, -1, k-1); % -1 = getConstant('NC_GLOBAL') 
    [xtype, L]     = netcdf.inqAtt(idFile, -1, s.att(k).name); %#ok<ASGLU>  % -1 = getConstant('NC_GLOBAL') 
    s.att(k).value = netcdf.getAtt(idFile, -1, s.att(k).name); % -1 = getConstant('NC_GLOBAL') 
    %     s.att(k).type = Constants{xtype+1};
    s.att(k).type = xtype2str(xtype);
end

%% Variables

s.var = [];
for k=1:nvars
    [s.var(k).name, xtype, dimids, natts] = netcdf.inqVar(idFile, k-1);
    s.var(k).type = xtype2str(xtype);
    nDims = length(dimids);
    for k2=1:nDims
        %         s.var(k).dim(k2).name = s.dim(dimids(nDims-k2+1)+1).name;
        s.var(k).dim(k2).name = s.dim(dimids(k2)+1).name;
    end
    
    % Attributs de variables
    
    for k2=1:natts
        s.var(k).att(k2).name  = netcdf.inqAttName(idFile, k-1, k2-1);
        s.var(k).att(k2).value = netcdf.getAtt(idFile, k-1, s.var(k).att(k2).name);
    end
end

%% Fermeture du fichier

netcdf.close(idFile);


function str = xtype2str(xtype)
switch xtype
    case 1
        str = 'NC_CHAR'; % NC_BYTE ??? %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case 2
        str = 'NC_CHAR';
    case 3
        str = 'NC_SHORT';
    case 4
        str = 'NC_LONG';
    case 5
        str = 'NC_FLOAT';
    case 6
        str = 'NC_DOUBLE';
    otherwise
        str = 'beurk';
end
