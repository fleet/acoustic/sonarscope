% Lecture d'une variable
%
% Syntax
%   a = set_CodedValue(a, ident, value, ...)
%
% Input Arguments
%   a     : Une instance de la classe cl_netcdf
%   ident : Soit le numero de la variable, soit son nom
%   value : Valeur du parametre dans l'unite
%
% Name-Value Pair Arguments
%   sub1 : Echantillonnage de la premiere dimension
%   sub2 : Echantillonnage de la deuxieme dimension
%   ...
%
% Output Arguments
%   a : Une instance de la classe cl_netcdf
%
% Examples
%   fileName = getNomFicDatabase('example.nc')
%   a = cl_netcdf('fileName', fileName)
%   val1 = get_value(a, 'lon');
%   sk = get(a, 'skeleton')
%
%   nomFic2 = tempname
%   b = cl_netcdf('fileName', nomFic2, 'skeleton', sk)
%   b = set_CodedValue(a, 'lon', val);
%   val2 = get_value(b, 'lon');
%   isequal(val1, val2)
%
% See also cl_netcdf cl_netcdf/set_CodedValue Authors
% Authors : JMA + DCF
% ----------------------------------------------------------------------------

function this = set_CodedValue(this, ident, value, varargin)

[varargin, sub1] = getPropertyValue(varargin, 'sub1', []);
[varargin, sub2] = getPropertyValue(varargin, 'sub2', []);
[varargin, sub3] = getPropertyValue(varargin, 'sub3', []);
[varargin, sub4] = getPropertyValue(varargin, 'sub4', []);
[varargin, sub5] = getPropertyValue(varargin, 'sub5', []);
[varargin, sub6] = getPropertyValue(varargin, 'sub6', []);
[varargin, sub7] = getPropertyValue(varargin, 'sub7', []);
[varargin, sub8] = getPropertyValue(varargin, 'sub8', []); %#ok<ASGLU>

% ---------
% Open file

if exist(this.fileName, 'file')
    idFile = mexCD_OpenFile(this.fileName, 'Write');
    if isempty(idFile)
        return;
    end
else
    return
end

if ischar(ident)
    for i = 1:mexCD_NbVar(idFile)
        varName  = mexCD_NameVar(idFile, i);
        if strcmp(varName, ident)
            ident = i;
            break
        end
    end
elseif isnumeric(ident)
else
    return
end
[Var.name, typeValue, nDimVar, dimVar, Var.value]  = mexCD_GetVar(idFile, ident);
Var.typeValue  = ['nc', typeValue];

% ------------------
% Calcul des indices

if isempty(sub1)
    nomDim = this.skeleton.var(ident).dim(1).name;
    sub1 = 1:get_valDim(this, nomDim);
end

if isempty(sub2)
    if length(this.skeleton.var(ident).dim) >= 2
        nomDim = this.skeleton.var(ident).dim(2).name;
        sub2 = 1:get_valDim(this, nomDim);
    end
end

if isempty(sub3)
    if length(this.skeleton.var(ident).dim) >= 3
        nomDim = this.skeleton.var(ident).dim(3).name;
        sub3 = 1:get_valDim(this, nomDim);
    end
end

if isempty(sub4)
    if length(this.skeleton.var(ident).dim) >= 4
        nomDim = this.skeleton.var(ident).dim(4).name;
        sub4 = 1:get_valDim(this, nomDim);
    end
end

if isempty(sub5)
    if length(this.skeleton.var(ident).dim) >= 5
        nomDim = this.skeleton.var(ident).dim(5).name;
        sub5 = 1:get_valDim(this, nomDim);
    end
end

if isempty(sub6)
    if length(this.skeleton.var(ident).dim) >= 6
        nomDim = this.skeleton.var(ident).dim(6).name;
        sub6 = 1:get_valDim(this, nomDim);
    end
end

if isempty(sub7)
    if length(this.skeleton.var(ident).dim) >= 7
        nomDim = this.skeleton.var(ident).dim(7).name;
        sub7 = 1:get_valDim(this, nomDim);
    end
end

if isempty(sub8)
    if length(this.skeleton.var(ident).dim) >= 8
        nomDim = this.skeleton.var(ident).dim(8).name;
        sub8 = 1:get_valDim(this, nomDim);
    end
end

% ----------------------------------
% Ecriture de la valeur du parametre

% typeValue = class(value);
% Dims = size(value);
% NbDims = ndims(value);

if isempty(sub2) && isempty(sub3) && isempty(sub4) && isempty(sub5) && isempty(sub6) && isempty(sub7) && isempty(sub8)
    Var.value(sub1) = value;
elseif isempty(sub3) && isempty(sub4) && isempty(sub5) && isempty(sub6) && isempty(sub7) && isempty(sub8)
    Var.value(sub1,sub2) = value(:,:);
elseif isempty(sub4) && isempty(sub5) && isempty(sub6) && isempty(sub7) && isempty(sub8)
    Var.value(sub1,sub2,sub3) = value(:,:,:);
elseif isempty(sub5) && isempty(sub6) && isempty(sub7) && isempty(sub8)
    Var.value(sub1,sub2,sub3,sub4) = value(:,:,:,:);
elseif isempty(sub6) && isempty(sub7) && isempty(sub8)
    Var.value(sub1,sub2,sub3,sub4,sub5) = value(:,:,:,:,:);
elseif isempty(sub7) && isempty(sub8)
    Var.value(sub1,sub2,sub3,sub4,sub5,sub6) = value(:,:,:,:,:,:);
elseif isempty(sub8)
    Var.value(sub1,sub2,sub3,sub4,sub5,sub6,sub7) = value(:,:,:,:,:,:,:);
else
    Var.value(sub1,sub2,sub3,sub4,sub5,sub6,sub7,sub8) = value(:,:,:,:,:,:,:,:);
end
CharIsByte = 0;
mexCD_PutData(idFile, ident, value, CharIsByte);

% --------------------
% Fermeture du fichier
mexCD_CloseFile(idFile);
