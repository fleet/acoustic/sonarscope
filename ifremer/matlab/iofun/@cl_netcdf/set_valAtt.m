% Ecritude de la valeur d'un attribut
%
% Syntax
%   a = set_valAtt(a, ident, value)
%
% Input Arguments
%   a     : Une instance de la classe cl_netcdf
%   ident : Numero ou nom de l'attibut
%   value : Valeur de l'attribut
%
% Output Arguments
%   a     : Instance de la classe cl_netcdf
%
% Examples
%   fileName = getNomFicDatabase('REFCar_EM1000.mbg')
%   a = cl_netcdf('fileName', fileName)
%   fileName = get(a, 'fileName')
%
%   val = get_valAtt(a, 47)
%   a = set_valAtt(a, 'mbCycleCounter', val)
%
% See also cl_netcdf cl_netcdf/set_value Authors
% Authors : JMA + DCF
% ----------------------------------------------------------------------------

function this = set_valAtt(this, ident, value)

%% On v�rifie que l'instance est bien de dimension 1

if length(this) ~= 1
    my_warndlg('cl_netcdf/get : L''instance doit etre de dimension 1', 1);
    return
end

if exist(this.fileName, 'file')
    idFile = netcdf.open(this.fileName, 'NC_WRITE');
    if isempty(idFile)
        return
    end
else
    return
end

if isnumeric(ident)
    this.skeleton.att(ident).value = value;
elseif ischar(ident)
    pppp = {this.skeleton.att(:).name};
    i = find(strcmp(pppp, ident) == 1);
    if any(i)
        attName = this.skeleton.att(i).name;
        this.skeleton.att(i).value = value;
    else % ajout �viter bug GLA
        str1 = sprintf('Impossible d''�crire l''attribut "%s" : n''existe pas dans le fichier de Ref\nVeuillez v�rifier que le fichier a �t� correctement �crit', ident);
        str2 = sprintf('Impossible to write attribute "%s" : not present in Ref file\nFile might be corrupted', ident);
        my_warndlg(Lang(str1,str2), 0);
        return
    end
end

% Put file in define mode : n�cessaire parfois (????) pour reforcer le mode �criture.
netcdf.reDef(idFile);

% Attribution d'un attribut Global.
netcdf.putAtt(idFile, -1, attName, value); % -1 = getConstant('NC_GLOBAL') 

%% Fermeture du fichier

netcdf.close(idFile);
