% Lecture de la valeur d'une dimension
%
% Syntax
%   a = set_valVarAtt(a, numVar, nomAtt, valVarAtt)
%
% Input Arguments
%   a         : Une instance de la classe cl_netcdf
%   numVar    : Nom ou numero de la variable
%   nomAtt    : Nom de l'attribut
%   valVarAtt : Valeur de l'attribut
%
% Output Arguments
%   a     : Instance de la classe cl_netcdf
%
% Examples
%   nomFic = getNomFicDatabase('DTS1_ExStr.imo')
%   a = cl_netcdf('fileName', nomFic);
%   cdf_disp_list_var(sk, 'Level', 1)
%   a = set_valVarAtt(a, 81, 'Commentaire', 'Hello world')
%
% See also cl_netcdf cl_netcdf/set_value Authors
% Authors : JMA
% VERSION  : $Id: get.m,v 1.3 2003/09/02 09:59:25 augustin Exp augustin $
% ----------------------------------------------------------------------------

function this = set_valVarAtt(this, numVar, attName, attValue)

% -------------------------------------------------
% On verifie que l'instance est bien de demension 1

if length(this) ~= 1
    my_warndlg('cl_netcdf/get : L''instance doit etre de dimension 1', 1);
    return
end

if exist(this.fileName, 'file')
    idFile = netcdf.open(this.fileName, 'NC_WRITE');
    if isempty(idFile)
        return
    end
else
    return
end

if ischar(numVar)
    numVar = find_numVar(this, numVar);
end

if isempty(attValue)
    if ischar(attValue)
        attValue = ' ';
    else
        attValue = 0;
    end
end

% A l'essai
% Put file in define mode : nécessaire parfois (????) pour reforcer le mode
% Ecriture.
netcdf.reDef(idFile);

netcdf.putAtt(idFile, numVar-1, attName, attValue);

% --------------------
% Fermeture du fichier

netcdf.close(idFile);

numAtt = find_numVarAtt(this, numVar, attName);
this.skeleton.var(numVar).att(numAtt).value = attValue;
