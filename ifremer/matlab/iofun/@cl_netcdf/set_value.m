% Lecture d'une variable
%
% Syntax
%   a = set_value(a, ident, value, ...)
%
% Input Arguments
%   a     : Une instance de la classe cl_netcdf
%   ident : Soit le numero de la variable, soit son nom
%   value : Valeur du parametre dans l'unite
%
% Name-Value Pair Arguments
%   sub1 : Echantillonnage de la premiere dimension
%   sub2 : Echantillonnage de la deuxieme dimension
%   ...
%
% Output Arguments
%   a : Une instance de la classe cl_netcdf
%
% Examples
%   fileName = getNomFicDatabase('example.nc')
%   a = cl_netcdf('fileName', fileName)
%   val1 = get_value(a, 'lon');
%   sk = get(a, 'skeleton')
%
%   nomFic2 = tempname
%   b = cl_netcdf('fileName', nomFic2, 'skeleton', sk)
%   set_value(b, 'lon', val);
%   val2 = get_value(b, 'lon');
%   isequal(val1, val2)
%
% See also cl_netcdf cl_netcdf/set_value Authors
% Authors : JMA + DCF
% ----------------------------------------------------------------------------

function this = set_value(this, ident, value, varargin)

[varargin, fid]  = getPropertyValue(varargin, 'fid', []);
[varargin, sub1] = getPropertyValue(varargin, 'sub1', []); %#ok<ASGLU>

% ncEdit(this.fileName)

s = this.skeleton;

% NB : Le sous-�chantillonnage ne fonctionne pas sur la librairie CIB_DOU
% pour l'instant (09/2006).

% ---------
% Open file en lecture/ecriture

if isempty(fid)
    if exist(this.fileName, 'file')
        idFile = netcdf.open(this.fileName, 'NC_WRITE');
        if isempty(idFile)
            return
        end
    else
        return
    end
else
    idFile = fid;
end

if ischar(ident)
    identVar = [];
    %     for i = 1:mexCD_NbVar(idFile)
    for i=1:length(s.var)
        %         varName = mexCD_NameVar(idFile, i);
        varName = s.var(i).name;
        if strcmp(varName, ident)
            identVar = i;
            break
        end
    end
    if isempty(identVar)
        for i=1:length(s.var)
            %             varName  = mexCD_NameVar(idFile, i);
            varName = s.var(i).name;
            if strcmpi(varName, ident)
                identVar = i;
                break
            end
        end
    end
    ident = identVar;
elseif isnumeric(ident)
else
    return
end

% s.var(ident)
VarTypeValue = s.var(ident).type;

% ATLANTIDE : Pour pouvoir traiter Xmin_metric
switch VarTypeValue
    case 'NC_DOUBLE'
        value = double(value);
    otherwise
        %         VarTypeValue;
end
% ATLANTIDE : Pour pouvoir traiter Xmin_metric


% ----------------------------------------
% Transformation dans l'unite de stockage

subNan = isnan(value);

add_offset    = 0;
scale_factor  = 1;
missing_value = [];
maximum = [];
for j=1:length(s.var(ident).att)
    if strcmpi((s.var(ident).att(j).name), 'add_offset')
        add_offset = s.var(ident).att(j).value(:);
    end
    
    if strcmpi((s.var(ident).att(j).name), 'scale_factor')
        scale_factor = s.var(ident).att(j).value(:);
    end
    
    if strcmpi((s.var(ident).att(j).name), 'missing_value')
        missing_value  = s.var(ident).att(j).value(:);
    end
    
    if strcmpi((s.var(ident).att(j).name), 'maximum')
        maximum  = s.var(ident).att(j).value(:);
    end
end

if ~strcmpi(VarTypeValue, 'NC_CHAR') ||  ~ischar(value)
    if add_offset ~= 0
        value = (value - double(add_offset));
    end
    if scale_factor ~= 1
        value = value / double(scale_factor);
    end
    if ~isempty(missing_value)
        value(subNan) = missing_value;
    end
end

% Le typage des variables en double a �t� effectu� lors du get_value.
switch VarTypeValue
    case 'NC_LONG'
        if isempty(maximum)
            if ~isa(value, 'int32')
                value = int32(floor(value));
            end
        else
            if maximum > (2^31 - 1)
                sub = (value >= 2^32);
                value(sub) = double(value(sub)) - 2^32;
            end
            X = zeros(size(value), 'int32');
            for iCol=1:size(X,2)
                X(:,iCol) = int32(floor(value(:,iCol)));
            end
            value = X;
            clear X
        end
    case 'NC_FLOAT'
        if ~isa(value, 'single')
            X = zeros(size(value), 'single');
            for iCol=1:size(X,2)
                X(:,iCol) = single(floor(value(:,iCol)));
            end
            value = X;
            clear X
        end
    case 'NC_SHORT'
        if ~isa(value, 'int16')
            if isempty(maximum)
                X = zeros(size(value), 'int16');
                for iCol=1:size(X,2)
                    X(:,iCol) = int16(floor(value(:,iCol)));
                end
                value = X;
                clear X
            else
                if maximum > (2^15 - 1)
                    sub = (value >= 2^16);
                    value(sub) = double(value(sub)) - 2^16;
                end
                X = zeros(size(value), 'int16');
                for iCol=1:size(X,2)
                    X(:,iCol) = int16(floor(value(:,iCol)));
                end
                value = X;
                clear X
            end
        end
    case 'NC_BYTE'
        if ~isa(value, 'uint8')
            X = zeros(size(value), 'uint8');
            for iCol=1:size(X,2)
                X(:,iCol) = uint8(floor(value(:,iCol)));
            end
            value = X;
            clear X
        end
    case 'NC_CHAR'
        if ~ischar(value)
            if ~isempty(maximum) && maximum <= 127
                X = zeros(size(value), 'int8');
                for iCol=1:size(X,2)
                    X(:,iCol) = int8(floor(value(:,iCol)));
                end
                value = X;
                clear X
            else
                X = zeros(size(value), 'uint8');
                for iCol=1:size(X,2)
                    X(:,iCol) = uint8(floor(value(:,iCol)));
                end
                value = X;
                clear X
            end
            value = my_transpose(value);
            sz = size(value);
            value = reshape(value, [sz(2) sz(1)]);
        end
    case 'NC_DOUBLE'
        %         VarTypeValue;
    otherwise
        str = sprintf('cl_netcdf/set_Value : "%s" pas encore prevu', VarTypeValue);
        my_warndlg(str, 1);
end

% Tanscodage : on proc�de au transcodage sym�trique de get_value.m
% L'applications du mexSetDimensions comme dans get_value.m ne fonctionne
% pas tel quel.
if strcmp(VarTypeValue, 'NC_CHAR')
    if numel(value) ~= 1
        sz = size(value);
        if (sz(1) == 1) || (sz(2) == 1)
            value = my_transpose(value);
        else
            if (sz(1)== sz(2))
                value = reshape(value, [sz(2) sz(1)]);
                value = my_transpose(value);
            end
        end
    end
else
    if numel(value) ~= 1
        sz = size(value);
        if (sz(1) == 1) && (sz(2) ~= 1)
            value = my_transpose(value);
        elseif (sz(1) ~= 1) && (sz(2) == 1)
        elseif (sz(1) == 1) && (sz(2) == 1)
            
        else
            value = my_transpose(value);
            %             value = reshape(value, sz);
        end
    end
end

switch VarTypeValue
    case 'NC_CHAR'
        if ~ischar(value)
            value = char(value);
        end
    otherwise
        if ischar(value)
            value = uint8(value);
        end
end

% try
if isempty(value)
    if isempty(fid)
        netcdf.close(idFile);
    end
    return
end

if isempty(sub1)
    netcdf.putVar(idFile, ident-1, value)
else
    start  = zeros(1,ndims(value));
    count  = ones(1,ndims(value)) .* size(value);
    stride = ones(1,ndims(value));
    start(2) = sub1(1)-1;
    if length(sub1) ~= 1
        stride(2) = sub1(2) - sub1(1);
    end
    netcdf.putVar(idFile, ident-1, start, count, stride, value)
end
% catch
%     disp('netcdf.putVar Erreur')
%     ident
%     whos value
%     s.var(ident)
% end

% --------------------
% Fermeture du fichier

if isempty(fid)
    netcdf.close(idFile);
end
% ncEdit(this.fileName)
