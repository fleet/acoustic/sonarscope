% Write a netCDF file from structure of data and format
%
% Syntax
%    write_autoscale(a, identVar, val)
%
% Input Arguments
%   a        : instance of cl_netcdf
%   identVar : Identificateur de la variable (string ou integer)
%   val      : Valeurs
%
% Name-Value Pair Arguments
%   CLim : Bornes de la donnee ([Min Max] de la donnee sinon)
%
% Output Arguments
%   status = 1 or 0
%
% Examples
%   nomFic1 = getNomFicDatabase('DTS1_ExStr.imo')
%   nomFic2 = [tempname '.nc']
%   copyfile(nomFic1, nomFic2)
%
%   a = cl_netcdf('fileName', nomFic2);
%   numVar = find_numVar(a, 'grWrkImage')
%   val = get_value(a, numVar);
%   status = write_autoscale(a, 'grWrkImage', val)
%
% Remarks :
%
% See also
% Authors : DCF
% ----------------------------------------------------------------------------

function status = write_autoscale(this, varName, val, varargin)

[varargin, CLim] = getPropertyValue(varargin, 'CLim', []);
[varargin, subl] = getPropertyValue(varargin, 'subl', []); %#ok<ASGLU>

status = 0;
if ischar(varName)
    numVar = find_numVar(this, varName);
    if isempty(numVar)
        str = sprintf('"%s" not found in "%s"', varName, this.fileName);
        errordlg(str, 'IFREMER', 'Modal')
        return
    end
elseif isnumeric(varName)
    numVar = varName;
    varName = this.skeleton.var(numVar).name;
    N = length(this.skeleton.var);
    if (numVar < 1) || (numVar > N)
        str = sprintf('"%s" has %d variables. numVar=%d is incompatible ', this.fileName, N, numVar);
        errordlg(str, 'IFREMER', 'Modal')
        return
    end
else
    errordlg('varName must be a string or an integer', 'IFREMER', 'Modal')
    return
end

VarDim = get_varDim(this, numVar);
ValDim = VarDim;
if isempty(subl)
    subl = 1:VarDim(1);
else
    ValDim(1) = length(subl);
end
if isscalar(ValDim)
    if ValDim ~= numel(val)
        str = sprintf('Incompatibilite de taille :\n%s : %s\nVal transmise : %s\nsuby : %s', ...
            varName, num2strCode(VarDim), num2strCode(size(val)), num2strCode(subl));
        errordlg(str, 'IFREMER', 'Modal')
        return
    end
else
    if ~isequal(fliplr(size(val)), ValDim)
        str = sprintf('Incompatibilite de taille :\n%s : %s\nVal transmise : %s\nsuby : %s', ...
            varName, num2strCode(VarDim), num2strCode(size(val)), num2strCode(subl));
        errordlg(str, 'IFREMER', 'Modal')
        return
    end
end

% numAtt = find_numVarAtt(this, numVar, 'type');
% type = this.skeleton.var(numVar).att(numAtt).value
%
% numAtt = find_numVarAtt(this, numVar, 'valid_minimum');
% valid_minimum = this.skeleton.var(numVar).att(numAtt).value

% numAtt = find_numVarAtt(this, numVar, 'valid_maximum');
% valid_maximum = this.skeleton.var(numVar).att(numAtt).value;

numAtt  = find_numVarAtt(this, numVar, 'type');
type    = this.skeleton.var(numVar).att(numAtt).value;

numAtt = find_numVarAtt(this, numVar, 'minimum');
minimum = this.skeleton.var(numVar).att(numAtt).value;

numAtt = find_numVarAtt(this, numVar, 'maximum');
maximum = this.skeleton.var(numVar).att(numAtt).value;

% numAtt = find_numVarAtt(this, numVar, 'missing_value');
% missing_value = this.skeleton.var(numVar).att(numAtt).value;

typeVar = this.skeleton.var(numVar).type;
switch typeVar
    case {'NC_SHORT', 'NC_LONG'}
        if isempty(CLim)
            MinVal = min(val(:));
            MaxVal = max(val(:));
        else
            MinVal = CLim(1);
            MaxVal = CLim(2);
        end
        
        add_offset = MinVal;
        % scale_factor = (MaxVal - MinVal) / ( maximum - minimum);
        scale_factor = (MaxVal - MinVal) / cast( maximum - minimum, class(MaxVal)); %modif GLA
        
        
        numAtt = find_numVarAtt(this, numVar, 'scale_factor');
        this.skeleton.var(numVar).att(numAtt).value = scale_factor;
        
        numAtt = find_numVarAtt(this, numVar, 'add_offset');
        this.skeleton.var(numVar).att(numAtt).value = add_offset;
        
        if isequal(VarDim, ValDim)
            X = val;
        else
            X = get_value(this, numVar);
            X(subl,:) = val;
        end
        
        if scale_factor == 0 % cas d'un valeur constante : on �crit la valeur en prenant garde de ne pas �crire de missing value ex:IMO
            %disp('Danger scale factor = 0 ! mise � 1 par d�faut');
            X = 9999*ones(size(X), class(X));
        end
        
        this = set_valVarAtt(this, numVar, 'scale_factor', scale_factor);
        this = set_valVarAtt(this, numVar, 'add_offset', add_offset);
        %         this = set_valVarAtt(this, numVar, 'missing_value', missing_value);
        %         if ~isvector(X)
        %             X = fliplr(X);
        %         end
        set_value(this, numVar, X);
        
    case 'NC_BYTE'
        if isempty(CLim)
            MinVal = min(val(:));
            MaxVal = max(val(:));
        else
            MinVal = CLim(1);
            MaxVal = CLim(2);
        end
        
        minimum = 1;
        maximum = 255;
        valid_minimum = 1;
        valid_maximum = 255;
        
        add_offset = MinVal;
        scale_factor = (MaxVal - MinVal) / (maximum - minimum);
        
        numAtt = find_numVarAtt(this, numVar, 'scale_factor');
        this.skeleton.var(numVar).att(numAtt).value = scale_factor;
        
        numAtt = find_numVarAtt(this, numVar, 'add_offset');
        this.skeleton.var(numVar).att(numAtt).value = add_offset;
        
        numAtt = find_numVarAtt(this, numVar, 'minimum');
        this.skeleton.var(numVar).att(numAtt).value = minimum;
        
        numAtt = find_numVarAtt(this, numVar, 'maximum');
        this.skeleton.var(numVar).att(numAtt).value = maximum;
        
        numAtt = find_numVarAtt(this, numVar, 'valid_minimum');
        this.skeleton.var(numVar).att(numAtt).value = valid_minimum;
        
        numAtt = find_numVarAtt(this, numVar, 'valid_maximum');
        this.skeleton.var(numVar).att(numAtt).value = valid_maximum;
        
        if isequal(VarDim, ValDim)
            X = val;
        else
            X = get_value(this, numVar);
            X(subl,:) = val;
        end
        
        this = set_valVarAtt(this, numVar, 'scale_factor', scale_factor);
        this = set_valVarAtt(this, numVar, 'add_offset', add_offset);
        this = set_valVarAtt(this, numVar, 'valid_maximum', valid_maximum);
        this = set_valVarAtt(this, numVar, 'valid_minimum', valid_minimum);
        this = set_valVarAtt(this, numVar, 'minimum', minimum);
        this = set_valVarAtt(this, numVar, 'maximum', maximum);
        if ~isvector(X)
            X = fliplr(X);
        end
        set_value(this, numVar, X);
        
    otherwise
        str = sprintf('%s pas encore prevu dans cl_netcdf/write_autoscale', typeVar);
        errordlg(str, 'IFREMER', 'Modal')
        return
end
status = 1;

