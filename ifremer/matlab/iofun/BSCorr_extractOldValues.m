function flag = BSCorr_extractOldValues(nomFic, nomFicSIS)

flag = exist(nomFic, 'file');
if ~flag
    return
end


flag = 0;
fidIn = fopen(nomFic, 'r');
tline = fgetl(fidIn);
while ischar(tline)
    if (length(tline) > 5) && strcmp(tline(1:5), '#SIS ')
        tline = tline(6:end);
        if flag == 0
            fidOut = fopen(nomFicSIS, 'w+');
            flag = 1;
        end
        fprintf(fidOut, '%s\n', tline);
    end
    if (length(tline) > 6) && strcmp(tline(1:6), '##SIS ')
        tline(2:5) = [];
        if flag == 0
            fidOut = fopen(nomFicSIS, 'w+');
            flag = 1;
        end
        fprintf(fidOut, '%s\n', tline);
    end
    
   tline = fgetl(fidIn);
end
fclose(fidIn);
if flag
    fclose(fidOut);
end
