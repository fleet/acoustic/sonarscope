% nomFicIn  = 'C:\Temp\SHOM\Bathy\BATHYMETRIE.txt';
% nomFicOut = 'C:\Temp\SHOM\Bathy\StBrieuc.txt';
% CorrectionFichierAsciiCaris(nomFicIn, nomFicOut)

function CorrectionFichierAsciiCaris(nomFicIn, nomFicOut)

% TODO : 
% Tester si my_tempdir est sur le m�me disque que nomFicIn
% Si c'est un disque diff�rent : recopier le fichier en local
% Tester si my_tempdir est sur le m�me disque que nomFicOut
% Si c'est un disque diff�rent : cr�er le fichier en local et le copier ensuite
% Faire le m�nage

nbOc = sizeFic(nomFicIn);
if nbOc == 0
    return
end

fidIn = fopen(nomFicIn);
if fidIn == -1
    return
end

fidOut = fopen(nomFicOut, 'w+');
if fidOut == -1
    return
end

while 1
    tline = fgetl(fidIn);
    if ~ischar(tline)
        break
    end
    tline = strrep(tline, 'N ', ' -');
    tline = strrep(tline, 'W', '');
    fprintf(fidOut, '%s\n', tline);
end
fclose(fidIn);
fclose(fidOut);
