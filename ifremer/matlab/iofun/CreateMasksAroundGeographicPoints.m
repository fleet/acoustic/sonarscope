% nomFicIn   = 'F:\SPFE\PourMarc\ListGeoGraphicPointsForMarc.txt';
% nomDirOut  = 'F:\SPFE\PourMarc';
% typeFrame  = 'Circle';
% widthFrame = 75/2;
% gridSize   = 1;
% flag = CreateMasksAroundGeographicPoints(nomFicIn, nomDirOut, typeFrame, widthFrame, gridSize);

function CreateMasksAroundGeographicPoints(nomFicIn, nomDirOut, typeFrame, widthFrame, gridSize)

if iscell(nomFicIn)
    nomFicIn = nomFicIn{1};
end

%% Lecture du fichier

fid = fopen(nomFicIn, 'r');
header = fgetl(fid); %#ok<NASGU>
C = textscan(fid, '%s%f%f', 'delimiter', ';');
fclose(fid);

NameMask   = C{1};
LongCentre = C{2};
LatCentre  = C{3};

%% Plot des points

Fig = plot_navigation_Figure([]);
% plot(LongCentre, LatCentre, '*'); grid on
SonarTime = zeros(size(LongCentre));
plot_navigation('Centers', Fig, LongCentre, LatCentre, SonarTime, []);
hold on;

%% Cr�ation des masques

N = length(LongCentre);
str1 = 'Cr�ation des masques';
str2 = 'Create masks';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    a = createMaskCircle(NameMask{k}, LatCentre(k), LongCentre(k), widthFrame, gridSize, typeFrame, nomDirOut, Fig); %#ok<NASGU>
end
my_close(hw)


function a = createMaskCircle(NameMask, LatCentre, LongCentre, Rayon1, resol, typeFrame, nomDirOut, Fig)

radius = atand(Rayon1/6367444);
[latc, lonc] = scircle1(LatCentre, LongCentre, radius);
% figure(Fig); hold on; plot(lonc, latc, '.');
SonarTime = zeros(size(lonc));
plot_navigation(NameMask, Fig, lonc, latc, SonarTime, [])

[flag, a] = create_maskFromLatLongCoordinates(NameMask, lonc, latc, resol, typeFrame);
if flag
    nomFic = fullfile(nomDirOut, [NameMask '-100m.ers']);
    flag = export_ermapper(a, nomFic); %#ok<NASGU>
end


function [flag, a] = create_maskFromLatLongCoordinates(NameMask, lonc, latc, resol, typeFrame)

LatMean = mean(latc);
LonMean = mean(lonc);
Fuseau = floor((LonMean+180)/6 + 1);
if LatMean >= 0
    H = 'N';
else
    H = 'S';
end
Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 3, 'Projection.UTM.Fuseau', Fuseau, 'Projection.UTM.Hemisphere', H);
[x, y, flag] = latlon2xy(Carto, latc, lonc);
if ~flag
    return
end

minx = min(x);
maxx = max(x);
miny = min(y);
maxy = max(y);

x = minx:resol:maxx;
y = miny:resol:maxy;
[lat, lon] = xy2latlon(Carto, x, y);

switch typeFrame
    case 'Circle'
        r = floor(length(lat)/2);
        SE = strel('disk', r, 8);
    case 'Square'
        r = length(lat);
        SE = strel('square', r);
end
I = SE.Neighborhood;

deltaLat = mean(diff(lat));
deltaLon = mean(diff(lon));
lat = lat(1) + (0:(size(I,2)-1)) * deltaLat;
lon = lon(1) + (0:(size(I,1)-1)) * deltaLon;

lat = centrage_magnetique(lat);
lon = centrage_magnetique(lon);

DataType = cl_image.indDataType('Mask');
a = cl_image('Image', I, 'x', lon, 'y', lat, 'DataType', DataType, 'GeometryType', cl_image.indGeometryType('LatLong'), 'Name', NameMask);
% imagesc(a)

flag = 1;
