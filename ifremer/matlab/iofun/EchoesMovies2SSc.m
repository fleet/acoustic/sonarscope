%   nomFicMovies = 'T:\MARMESONET2\LEG1\HAC_NEW2\0408_20091115_183145_Lesuroit_all2hac.xyz';
%   nomFicMovies = 'T:\MARMESONET2\LEG1\HAC_NEW2\0409_20091115_190146_Lesuroit_all2hac.xyz';
%   EchoesMovies2SSc(nomFicMovies)

function EchoesMovies2SSc(nomFicMovies)

%% Lecture du fichier .txt de Movies

[flag, X, Y, V] = read_ASCII(nomFicMovies, 'DisplayEntete', 0, 'NbLigEntete', 0, ...
    'AskSeparator', 0, 'Separator', [], 'AskValNaN', 0, 'ValNaN', [], ...
    'nColumns', 6, 'AskGeometryType', 0, 'GeometryType', 3, ...
    'IndexColumns', [4 3 5 1 2 6], 'DataType', 2, 'Unit', 'm');
FigUtils.createSScFigure; PlotUtils.createSScPlot(X, Y, '*'); grid on

%% Ecriture du Echoes SSc

[nomDir, nomFic] = fileparts(nomFicMovies);
nomFicSSc = fullfile(nomDir, [nomFic '.xml']);

Points.Latitude  = Y;
Points.Longitude = X;
Points.Z         = V(:,1);
Points.iPing     = V(:,2);
Points.Angle     = V(:,3);
Points.Energie   = V(:,4);

WC_plotEchoes(Points, 'Name', nomFicSSc)

flag = WC_SaveEchoes(nomFicSSc, Points);

%% Relecture du fichier pour contr�le

[flag, Data] = XMLBinUtils.readGrpData(nomFicSSc);
WC_plotEchoes(Data)

