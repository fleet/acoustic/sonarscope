%   nomFicEchoes = 'D:\Demos-3DV\Marmesonet\SSc3DV_V2.0\WC\0408_20091115_183145_Lesuroit_Echoes.xml';
%   EchoesSSc2txt(nomFicEchoes)

function EchoesSSc2txt(nomFicEchoes)

%% Lecture du fichier Echoes

[flag, Data] = XMLBinUtils.readGrpData(nomFicEchoes);
WC_plotEchoes(Data)

%% Ecriture du fichier .txt

[nomDir, nomFic] = fileparts(nomFicEchoes);
nomFicTxt = fullfile(nomDir, [nomFic '.txt']);
fid = fopen(nomFicTxt, 'w+');
if fid == -1
    messageErreurFichier(nomFicTxt, 'ReadFailure');
    % return
end
for k1=1:length(Data.Signals)
    fprintf(fid, '%s ', Data.Signals(k1).Name);
end

for k2=1:Data.Dimensions.nbEchoes
    fprintf(fid, '\n');
    for k1=1:length(Data.Signals)
        fprintf(fid, '%s ', num2strPrecis(Data.(Data.Signals(k1).Name)(k2)));
    end
end
fclose(fid);

%% Relecture du fichier .txt

[flag, X, Y, V] = read_ASCII(nomFicTxt, 'DisplayEntete', 0, 'NbLigEntete', 0, ...
    'AskSeparator', 0, 'Separator', [], 'AskValNaN', 0, 'ValNaN', [], ...
    'nColumns', 15, 'AskGeometryType', 0, 'GeometryType', 3, ...
    'IndexColumns', [2 1 3 6 9 10], 'DataType', 2, 'Unit', 'm');
FigUtils.createSScFigure; PlotUtils.createSScPlot(X, Y, '*'); grid on

FigUtils.createSScFigure; plot3(V(:,3), V(:,4), V(:,1), '*'); grid on

