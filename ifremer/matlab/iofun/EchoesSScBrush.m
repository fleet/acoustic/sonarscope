% %   nomFicEchoes = 'D:\Demos-3DV\Marmesonet\SSc3DV_V2.0\WC\0409_20091115_190146_Lesuroit_all2hac.xml';
% %   nomFicEchoes = 'D:\Demos-3DV\Marmesonet\SSc3DV_V2.0\WC\0408_20091115_183145_Lesuroit_all2hac.xml';
%   nomFicEchoes = 'D:\Demos-3DV\Marmesonet\SSc3DV_V2.0\WC\0408_20091115_183145_Lesuroit_Echoes.xml';
%   EchoesSScBrush(nomFicEchoes)

function EchoesSScBrush(nomFicEchoes)
    
%% Lecture du fichier Echoes

[flag, Data] = XMLBinUtils.readGrpData(nomFicEchoes);
% WC_plotEchoes(Data)

%% Plot

Z = Data.Z;
iPing = Data.iPing;
AcrossDistance = Data.AcrossDistance;
% XCoor = Data.XCoor;
% YCoor = Data.YCoor;

X = Data.iPing;
Y = Data.AcrossDistance;
Z = Data.Z;

[X2, Y2, Z2, sub] = edit_xyz(X, Y, Z, 'XLabel', 'Pings', 'YLabel', 'Across distances', 'ZLabel', 'Depths');

fig = FigUtils.createSScFigure;
h1 = subplot(2,2,1); title('iPing - Z');
h2 = subplot(2,2,2); title('AcrossDistance - Z');
h3 = subplot(2,2,3); title('iPing - AcrossDistance');
h4 = subplot(2,2,4); title('3D');

% hp1 = plot3(h1, iPing, AcrossDistance, Z, '*'); grid(h1, 'on'); view(h1, 0, 0)
% hp2 = plot3(h2, iPing, AcrossDistance, Z, '*'); grid(h2, 'on'); view(h2, 90, 0)
% hp3 = plot3(h3, iPing, AcrossDistance, Z, '*'); grid(h3, 'on'); view(h3, 0, 90)
% brush on
% % BrushData = false(size(iPing));
% % set(hp1, 'BrushData', BrushData)
% % set(hp2, 'BrushData', BrushData)
% % set(hp3, 'BrushData', BrushData)
% linkdata

hp1 = plot(h1, iPing, Z, 'o');                  grid(h1, 'on');
hp2 = plot(h2, AcrossDistance, Z, 'o');         grid(h2, 'on');
hp3 = plot(h3, iPing, AcrossDistance, 'o');     grid(h3, 'on');
hp4 = plot3(h4, iPing, AcrossDistance, Z, 'o'); grid(h4, 'on');
set([hp1 hp2 hp3 hp4], 'MarkerFaceColor', [0 0 1], 'MarkerSize', 4);
linkaxes([h1 h3 h4], 'x')
% BrushData = false(size(iPing));
% set(hp1, 'BrushData', BrushData)
% set(hp2, 'BrushData', BrushData)
% set(hp3, 'BrushData', BrushData)
linkdata
% brush on

set(fig, 'DeleteFcn', @DeleteFcnEchoesSScBrush, 'UserData', {Data; nomFicEchoes})

% Pour pouvoir se servir de l'appli, il faut mettre un point d'arr�t avant
% la fin de la fonction (sur l'instruction pi par exemple), sinon, le linkdata n'est plus op�rationnel. J'ai
% essay� de ruser (while, pause, etc ...), rien n'y fait.

pi;

% while ishandle(fig)
%     pause(1)
% end

%{
% TODO : essayer de faire fonctionner ceci
asserv = [3 2 1 0; 2 3 0 0; 1 0 3 0; 0 0 0 3];
linkaxes([h1 h2 h3], asserv)

z = cl_virtual_zoom('hFig', fig, 'hAxes', [h1 h2 h3 h4], 'asserv', asserv);
set(z, 'cbName', 'cb', 'cbObj', 'cl_virtual_zoom')
activer(z)
%}  

function DeleteFcnEchoesSScBrush(fig, varargin)

UserData = get(fig, 'UserData');
Data         = UserData{1};
nomFicEchoes = UserData{2};
hp = findobj(fig, 'Type', 'Line');
BrushData = get(hp(1), 'BrushData');
[nomDir, nomFic] = fileparts(nomFicEchoes);
nomFicSSc = fullfile(nomDir, [nomFic '-Clean.xml']);
sub = (BrushData(1,:) == 0);
Points.Latitude       = Data.Latitude(sub);
Points.Longitude      = Data.Longitude(sub);
Points.Z              = Data.Z(sub);
Points.iPing          = Data.iPing(sub);
Points.Angle          = Data.Angle(sub);
Points.Energie        = Data.Energie(sub);
Points.Date           = Data.Date(sub);
Points.Hour           = Data.Hour(sub);
Points.EnergieTotale  = Data.EnergieTotale(sub);
Points.XCoor          = Data.XCoor(sub);
Points.YCoor          = Data.YCoor(sub);
Points.RangeInSamples = Data.RangeInSamples(sub);
Points.AcrossDistance = Data.AcrossDistance(sub);
Points.Celerite       = Data.Celerite(sub);

flag = WC_SaveEchoes(nomFicSSc, Points);

