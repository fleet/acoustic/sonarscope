function GroupShapeFiles(listShpFileNames, fileNameShpCoverage, fileNameShpNavigation)

%% Processing coverage shp files

% Fig = figure;
STotal = [];
sub = contains(listShpFileNames, 'Coverage');
listFilesCoverage = listShpFileNames(sub);
N = length(listFilesCoverage);
hw = create_waitbar('Processing Shp Coverage files', 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    S = shaperead(listFilesCoverage{k});
%     plotShap(Fig, S);
    if k == 1
        STotal = S;
    else
        STotal = [STotal; S]; %#ok<AGROW>
    end
end
my_close(hw)
if ~isempty(STotal)
    shapewrite(STotal, fileNameShpCoverage)
    
    % Vérification
    
    Fig = figure;
    S = shaperead(fileNameShpCoverage);
    plotShap(Fig, S); title('Coverage lines')
end

%% Processing navigation shp files

% Fig = figure;
STotal = [];
sub = contains(listShpFileNames, 'Navigation');
listFilesNav = listShpFileNames(sub);
N = length(listFilesNav);
hw = create_waitbar('Processing Shp Navigation files', 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    S = shaperead(listFilesNav{k});
%     plotShap(Fig, S);
    if k == 1
        STotal = S;
    else
        STotal = [STotal; S]; %#ok<AGROW>
    end
end
my_close(hw, 'MsgEnd')
if ~isempty(STotal)
    shapewrite(STotal, fileNameShpNavigation)
    
    % Vérification
    Fig = figure;
    S = shaperead(fileNameShpNavigation);
    plotShap(Fig, S); title('Navigation lines');
end


function plotShap(Fig, S)

figure(Fig); grid on; hold on;
for k=1:length(S)
    plot(S(k).X, S(k).Y);
end
