% Creation d'un fichier ErMapper contenant la bas de donnee Levitus
%
% Syntax
%   Levitus2mat
% 
% Examples
%   Levitus2mat
%
% See also WOA13Tomat lecLevitusMat celeriteChen TrajetRayon cli_sound_speed Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function Levitus2mat

global SonarScopeData %#ok<GVMIS>

nomDir  = '\\meskl1\data\Levitus-WOA13\Levitus';
nomDir2 = fullfile(SonarScopeData, 'Levitus');

nomFic = fullfile(nomDir, 'Levitus1x1AnnualTemp.nc');
Levitus2matParam(nomFic, 'Temperature');

nomFic = fullfile(nomDir, 'Levitus1x1AnnualPsal.nc');
Levitus2matParam(nomFic, 'Salinity');

nomFic = fullfile(nomDir, 'Levitus1x1SeasonPsal.nc');
Levitus2matParam(nomFic, 'Salinity');

nomFic = fullfile(nomDir, 'Levitus1x1SeasonTemp.nc');
Levitus2matParam(nomFic, 'Temperature');

nomFic = fullfile(nomDir, 'Levitus1x1MonthPsal.nc');
Levitus2matParam(nomFic, 'Salinity');

nomFic = fullfile(nomDir, 'Levitus1x1MonthTemp.nc');
Levitus2matParam(nomFic, 'Temperature');

nomFic = fullfile(nomDir, 'Levitus5x5AnnualTemp.nc');
Levitus2matParam(nomFic, 'Temperature');

nomFic = fullfile(nomDir, 'Levitus5x5AnnualPsal.nc');
Levitus2matParam(nomFic, 'Salinity');

nomFic = fullfile(nomDir, 'Levitus5x5SeasonPsal.nc');
Levitus2matParam(nomFic, 'Salinity');

nomFic = fullfile(nomDir, 'Levitus5x5SeasonTemp.nc');
Levitus2matParam(nomFic, 'Temperature');

nomFic = fullfile(nomDir, 'Levitus5x5MonthPsal.nc');
Levitus2matParam(nomFic, 'Salinity');

nomFic = fullfile(nomDir, 'Levitus5x5MonthTemp.nc');
Levitus2matParam(nomFic, 'Temperature');

str1 = sprintf('Les fichiers .mat g�n�r�s sur le r�pertoire "%s" doivent �tre copi�s manuellement dans "%s"', nomDir, nomDir2);
str2 = sprintf('The .mat file that has been created in "%s" must be manually copied into "%s"', nomDir, nomDir2);
my_warndlg(Lang(str1,str2), 1);


function Levitus2matParam(nomFic, NomParametre)

if ~exist(nomFic, 'file')
    nomFicGz = [nomFic '.gz'];
    if exist(nomFicGz, 'file')
        disp(['Uncompress : ' nomFicGz ' Please wait'])
        [nomDir, nomFic] = fileparts(nomFicGz);
        nomFic = fullfile(nomDir, nomFic);
        pppp = gunzip(nomFicGz, nomDir); %#ok<NASGU>
        delete(nomFicGz)
    else
        str = sprintf('%s not found', nomFic);
        my_warndlg(str, 1);
        return
    end
end

[nomDir, nomFicParam] = fileparts(nomFic);

% ncdisp(nomFic)
finfo = ncinfo(nomFic);

LATITUDE       = ncread(nomFic, 'LATITUDE');
LONGITUDE      = ncread(nomFic, 'LONGITUDE');
STANDARD_LEVEL = ncread(nomFic, 'STANDARD_LEVEL');
DATE           = ncread(nomFic, 'DATE');
DATE_F         = ncread(nomFic, 'DATE_F');
Value          = ncread(nomFic, 'MEAN_VALUE');
Sigma          = ncread(nomFic, 'DEVIATION');
N              = ncread(nomFic, 'NB_IN_SITU');

NomParam       = ncreadatt(nomFic, 'MEAN_VALUE', 'long_name_f'); %#ok<NASGU>
Unite          = ncreadatt(nomFic, 'MEAN_VALUE', 'unit');
FillValue      = ncreadatt(nomFic, 'MEAN_VALUE', '_FillValue');

listAttributsGlobaux = {finfo.Attributes(:).Name};

k = (strcmp(listAttributsGlobaux, 'Climatology_Name'));
Climatology_Name = finfo.Attributes(k).Value; %#ok<NASGU>

k = (strcmp(listAttributsGlobaux, 'Climatology_Name_f'));
Climatology_Name_f = finfo.Attributes(k).Value; %#ok<NASGU>

k = (strcmp(listAttributsGlobaux, 'Climatology_Version'));
Climatology_Version = finfo.Attributes(k).Value; %#ok<NASGU>

k = (strcmp(listAttributsGlobaux, 'Latitude_Mean_Mesh'));
Latitude_Mean_Mesh = finfo.Attributes(k).Value; %#ok<NASGU>

k = (strcmp(listAttributsGlobaux, 'Longitude_Mean_Mesh'));
Longitude_Mean_Mesh = finfo.Attributes(k).Value; %#ok<NASGU>

k = (strcmp(listAttributsGlobaux, 'In_Situ_Flag'));
In_Situ_Flag = finfo.Attributes(k).Value; %#ok<NASGU>

k = (strcmp(listAttributsGlobaux, 'Error_Flag'));
Error_Flag = finfo.Attributes(k).Value; %#ok<NASGU>

k = (strcmp(listAttributsGlobaux, 'South_latitude'));
South_latitude = finfo.Attributes(k).Value; %#ok<NASGU>

k = (strcmp(listAttributsGlobaux, 'North_latitude'));
North_latitude = finfo.Attributes(k).Value; %#ok<NASGU>

k = (strcmp(listAttributsGlobaux, 'West_longitude'));
West_longitude = finfo.Attributes(k).Value; %#ok<NASGU>

k = (strcmp(listAttributsGlobaux, 'East_longitude'));
East_longitude = finfo.Attributes(k).Value; %#ok<NASGU>


listDimensions = {finfo.Dimensions(:).Name};

k = (strcmp(listDimensions, 'N_STRING64'));
N_STRING64 = finfo.Dimensions(k).Length; %#ok<NASGU>

k = (strcmp(listDimensions, 'N_STRING16'));
N_STRING16 = finfo.Dimensions(k).Length; %#ok<NASGU>

k = (strcmp(listDimensions, 'N_LAT'));
N_LAT = finfo.Dimensions(k).Length; %#ok<NASGU>

k = (strcmp(listDimensions, 'N_LON'));
N_LON = finfo.Dimensions(k).Length; %#ok<NASGU>

k = (strcmp(listDimensions, 'N_DATE_TIME'));
N_DATE_TIME = finfo.Dimensions(k).Length; %#ok<NASGU>

k = (strcmp(listDimensions, 'N_DATE'));
N_DATE = finfo.Dimensions(k).Length; %#ok<NASGU>

k = (strcmp(listDimensions, 'N_LEV'));
N_LEV = finfo.Dimensions(k).Length; %#ok<NASGU>


Value = single(Value);
Sigma = single(Sigma);
N     = single(N);

Value = permute(Value, [1 2 4 3]);
Sigma = permute(Sigma, [1 2 4 3]);
N     = permute(N,     [1 2 4 3]);

% figure; plot(LONGITUDE); grid on
% figure; plot(LATITUDE); grid on

sub = (Value == FillValue);
Value(sub) = NaN; %#ok<NASGU>
Sigma(sub) = NaN; %#ok<NASGU>
N(sub)     = NaN; %#ok<NASGU>

Z        = -STANDARD_LEVEL'; %#ok<NASGU>
Lon      = LONGITUDE'; %#ok<NASGU>
Lat      = LATITUDE'; %#ok<NASGU>
Unit     = Unite; %#ok<NASGU>
NomParam = NomParametre; %#ok<NASGU>
DATE_F   = DATE_F'; %#ok<NASGU>
DATE     = DATE'; %#ok<NASGU>

listVar = {'Climatology_Name'
    'DATE'
    'DATE_F'
    'NomParam'
    'Value'
    'Sigma'
    'N'
    'Z'
    'Lon'
    'Lat'
    'Unit'};
nomFicMat = fullfile(nomDir, [nomFicParam '.mat']);
fprintf('Saving file %s\n', nomFicMat)
save(nomFicMat, listVar{:}, '-v7.3')
