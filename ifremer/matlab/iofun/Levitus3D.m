% Visualisation 3D de donnees Levitus
%
% Syntax
%   Levitus3D(nomFic, ...)
%
% Input Arguments
%   nomFic : Nom du fichier
%
% Name-Value Pair Arguments
%   LonLim   : Limites en Longitude ([-180 180] par defaut)
%   LatLim   : Limites en Latitude ([-90 90] par defaut)
%   xslice   : Longitudes o� placer des coupes ([] par defaut)
%   yslice   : Latitudes  o� placer des coupes ([] par defaut)
%   zslice   : Plans o� placer des coupes ([] par defaut)
%   isovalue : Iso-valeurs � tracer ([] par defaut)
%
% Examples
%   nomFic = getNomFicDatabase('Temperature_Month05.ers');
%   Levitus3D(nomFic)
%   Levitus3D(nomFic, 'zslice', -1)
%   Levitus3D(nomFic, 'xslice', 0)
%   Levitus3D(nomFic, 'yslice', 0)
%   Levitus3D(nomFic, 'xslice', 0, 'yslice', 0)
%   Levitus3D(nomFic, 'xslice', 0, 'yslice', 0, 'zslice', -1)
%   Levitus3D(nomFic, 'zslice', -21:5:-1)
%   Levitus3D(nomFic, 'xslice', -125, 'yslice', -60, 'zslice', -1)
%   Levitus3D(nomFic, 'isovalue', 10)
%   Levitus3D(nomFic, 'zslice', -1, 'isovalue', 10)
%   Levitus3D(nomFic, 'zslice', -1, 'isovalue', [15 25])
%   Levitus3D(nomFic, 'xslice', -125, 'yslice', -60, 'zslice', -1, 'isovalue', 10)
%   Levitus3D(nomFic, 'zslice', -1, 'isovalue', [5 10 15 20], 'LonLim', [-30 20], 'LatLim', [0 50])
%
% See also lecLevitus celeriteChen TrajetRayon cli_sound_speed Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function Levitus3D(nomFic, varargin)

[varargin, LonLim]   = getPropertyValue(varargin, 'LonLim', []);
[varargin, LatLim]   = getPropertyValue(varargin, 'LatLim', []);
[varargin, xslice]   = getPropertyValue(varargin, 'xslice', []);
[varargin, yslice]   = getPropertyValue(varargin, 'yslice', []);
[varargin, zslice]   = getPropertyValue(varargin, 'zslice', []);
[varargin, isovalue] = getPropertyValue(varargin, 'isovalue', []); %#ok<ASGLU>

a = cl_ermapper_ers('FicIn', nomFic);
[I, NullCellValue] = import_ermapper_image(a, nomFic); %#ok<ASGLU> 

[nbRows, nbColumns, nbSlides] = size(I);

Eastings    = get(a, 'Eastings');
Northings   = get(a, 'Northings');
Xdimension  = get(a, 'Xdimension');
Ydimension  = get(a, 'Ydimension');

YRange = Ydimension * nbRows;
XRange = Xdimension * nbColumns;
y = linspace(Northings-YRange, Northings, nbRows) + Ydimension / 2;
y = -y;
x = linspace(Eastings, Eastings+XRange, nbColumns)   - Xdimension / 2;

if ~isempty(LonLim)
    subx = find((x >= LonLim(1)) & (x <= LonLim(2)));
    x = x(subx);
else
    subx = 1:nbColumns;
end

if ~isempty(LatLim)
    suby = find((y >= LatLim(1)) & (y <= LatLim(2)));
    y = y(suby);
else
    suby = 1:nbRows;
end
I = I(suby,subx,:);

z = -(1:nbSlides);

[x,y,z] = meshgrid(x,y,z);

VMin = min(I(:));
VMax = max(I(:));
Color = jet(64);

I = double(I);

figure
if ~isempty(xslice) || ~isempty(yslice) || ~isempty(zslice)
    slice(x, y, z, I, xslice, yslice, zslice);
    colormap(Color)
    shading flat
else
    if isempty(isovalue)
        slice(x, y, z, I, xslice, yslice, -1);
        colormap(Color)
        shading flat
    end
end

if ~isempty(isovalue)
    hold on
    for k=1:length(isovalue)
        drawnow
        p = patch(isosurface(x,y,z,I, isovalue(k)));
        isonormals(x,y,z,I,p)
        indColor = floor(interp1(linspace(VMin, VMax, 64), 1:64, isovalue(k)));
        set(p, 'FaceColor', Color(indColor,:), 'EdgeColor', 'none');
        view(3); axis tight
        camlight
        lighting gouraud
    end
end

colorbar
axis tight
grid on
