function flag = WC_3DMatrix_RemoveXMLBin(Names)

flag = 0;
for k=1:length(Names)
    if isfield(Names(k), 'nomFicXmlRaw')
        flag = WC_3DMatrix_RemoveXMLBin_unitaire(Names(k).nomFicXmlRaw);
        if ~flag
            return
        end
    end
    if isfield(Names(k), 'nomFicXmlComp')
        flag = WC_3DMatrix_RemoveXMLBin_unitaire(Names(k).nomFicXmlComp);
        if ~flag
            return
        end
    end
end


function flag = WC_3DMatrix_RemoveXMLBin_unitaire(XmlFileName)

if exist(XmlFileName, 'file')
    flag = WC_3DMatrix_RemoveXMLBin_unitaireOne(XmlFileName);
else
    for k=0:99
        XmlFileNameOne = strrep(XmlFileName, '.xml', ['_' num2str(k, '%02d') '.xml']);
        if exist(XmlFileNameOne, 'file')
            flag = WC_3DMatrix_RemoveXMLBin_unitaireOne(XmlFileNameOne);
            if ~flag
                break
            end
        else
            flag = 1;
            break
        end
    end
end


function flag = WC_3DMatrix_RemoveXMLBin_unitaireOne(XmlFileName)

flag = my_deleteFile(XmlFileName);
if ~flag
    return
end

MatFileName = strrep(XmlFileName, '.xml', '_xml.mat');
flag = my_deleteFile(MatFileName);
if ~flag
    return
end

DirName = strrep(XmlFileName, '.xml', '');
flag = my_rmdir(DirName);
if ~flag
    return
end
