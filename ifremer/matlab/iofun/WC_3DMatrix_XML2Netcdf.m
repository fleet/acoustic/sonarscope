function flag = WC_3DMatrix_XML2Netcdf(Names)

flag = 0;
N = length(Names);
str1 = 'Conversion XML-Bin en Netcdf';
str2 = 'Convert XML-Bin to Netcdf';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    flag = WC_3DMatrix_XML2Netcdf_unitaire(Names(k));
    if ~flag
        return
    end
end
my_close(hw, 'MsgEnd')


function flag = WC_3DMatrix_XML2Netcdf_unitaire(Names)

flag = WC_3DMatrix_XML2Netcdf_one(Names);
if ~flag
    for k=0:10
        NamesOne = Names;
        NamesOne.nomFicXmlRaw  = strrep(Names.nomFicXmlRaw,  '.xml', ['_' num2str(k, '%02d') '.xml']);
        NamesOne.nomFicXmlComp = strrep(Names.nomFicXmlComp, '.xml', ['_' num2str(k, '%02d') '.xml']);
        flag = WC_3DMatrix_XML2Netcdf_one(NamesOne);
        if ~flag
            break
        end
    end
end
flag = 1;


function flag = WC_3DMatrix_XML2Netcdf_one(Names)

[flag, XMLRaw] = XMLBinUtils.readGrpData(Names.nomFicXmlRaw, 'XMLOnly', 1);
if ~flag
    return
end
[flag, DataRaw] = XMLBinUtils.readGrpData(Names.nomFicXmlRaw, 'Memmapfile', 1);
if ~flag
    return
end

[flag, XMLComp] = XMLBinUtils.readGrpData(Names.nomFicXmlComp, 'XMLOnly', 1);
if ~flag
    return
end
[flag, DataComp] = XMLBinUtils.readGrpData(Names.nomFicXmlComp, 'Memmapfile', 1);
if ~flag
    return
end

%% Cr�ation du fichier Netcdf

[filepath, nomFicSeul] = fileparts(Names.nomFicXmlRaw);
nomFicSeul = strrep(nomFicSeul, '_WCCubeRaw', '_WCCube');
nomFicSeul = strrep(nomFicSeul, '_Raw', ''); % Ajout JMA le 21/03/2021 pour fichiers .all. Attention, tester non r�gression .s7k
ncFileName    = fullfile(filepath, [nomFicSeul '.NC']);
ncFileNameNew = fullfile(filepath, [nomFicSeul '.nc']);

fprintf('Beginning of 3D matrix file "%s"\n', ncFileNameNew);

ncID = netcdf.create(ncFileName, 'NETCDF4');
netcdf.close(ncID);

DataRaw.ReflectivityRaw  = DataRaw.Reflectivity;
DataRaw.ReflectivityComp = DataComp.Reflectivity;
DataRaw = rmfield(DataRaw, 'Reflectivity');

XMLRaw.Images(1).Name   = 'ReflectivityRaw';
XMLRaw.Images(end+1)    = XMLComp.Images(1);
XMLRaw.Images(end).Name = 'ReflectivityComp';
XMLRaw.CLimComp         = XMLComp.CLimComp;

DataRaw.Images = XMLRaw.Images;

ncID = netcdf.create(ncFileName, 'NETCDF4');
netcdf.close(ncID);
flag = NetcdfUtils.XMLBin2Netcdf(XMLRaw, DataRaw, ncFileName, 'WaterColumnCube3D');

%% Renommage de l'extension .NC en .nc pour indiquer � l'utilisateur que le fichier est termin�

my_rename(ncFileName, ncFileNameNew);

fprintf('End of 3D matrix file "%s"\n', ncFileNameNew);
