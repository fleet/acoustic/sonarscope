% Creation d'un fichier ErMapper contenant la bas de donnee Levitus
%
% Syntax
%   WOA13Tomat
% 
% Examples
%   WOA13Tomat
%
% See also lecLevitusMat celeriteChen TrajetRayon cli_sound_speed Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function WOA13Tomat

nomDir  = '\\meskl1\data\Levitus-WOA13\WOA13';

%% Annual
nomFic = fullfile(nomDir, 'woa13_decav_t00_04v2.nc');
WOA13TomatParam(nomFic, 'Temperature');

nomFic = fullfile(nomDir, 'woa13_decav_s00_04v2.nc');
WOA13TomatParam(nomFic, 'Salinity');

%% Seasonal 
% Winter - 01 to 04 months
nomFic = fullfile(nomDir, 'woa13_decav_t13_04v2.nc');
WOA13TomatParam(nomFic, 'Temperature');

nomFic = fullfile(nomDir, 'woa13_decav_s13_04v2.nc');
WOA13TomatParam(nomFic, 'Salinity');

% Spring - 04 to 07 months
nomFic = fullfile(nomDir, 'woa13_decav_t14_04v2.nc');
WOA13TomatParam(nomFic, 'Temperature');

nomFic = fullfile(nomDir, 'woa13_decav_s14_04v2.nc');
WOA13TomatParam(nomFic, 'Salinity');

% Spring - 04 to 07 months
nomFic = fullfile(nomDir, 'woa13_decav_t14_04v2.nc');
WOA13TomatParam(nomFic, 'Temperature');

nomFic = fullfile(nomDir, 'woa13_decav_s14_04v2.nc');
WOA13TomatParam(nomFic, 'Salinity');

% Summer - 07 to 10 months
nomFic = fullfile(nomDir, 'woa13_decav_t15_04v2.nc');
WOA13TomatParam(nomFic, 'Temperature');

nomFic = fullfile(nomDir, 'woa13_decav_s15_04v2.nc');
WOA13TomatParam(nomFic, 'Salinity');

% Automn - 10 to 12 months 
nomFic = fullfile(nomDir, 'woa13_decav_t16_04v2.nc');
WOA13TomatParam(nomFic, 'Temperature');

nomFic = fullfile(nomDir, 'woa13_decav_s16_04v2.nc');
WOA13TomatParam(nomFic, 'Salinity');

%% Monthly
for m=1:12
    nomFic = sprintf('woa13_decav_t%02d_04v2.nc', m);
    nomFic = fullfile(nomDir, nomFic);
    WOA13TomatParam(nomFic, 'Temperature');

    nomFic = sprintf('woa13_decav_s%02d_04v2.nc', m);
    nomFic = fullfile(nomDir, nomFic);
    WOA13TomatParam(nomFic, 'Salinity');
end


function WOA13TomatParam(nomFic, NomParametre)

if ~exist(nomFic, 'file')
    nomFicGz = [nomFic '.gz'];
    if exist(nomFicGz, 'file')
        disp(['Uncompress : ' nomFicGz ' Please wait'])
        [nomDir, nomFic] = fileparts(nomFicGz);
        nomFic = fullfile(nomDir, nomFic);
        pppp = gunzip(nomFicGz, nomDir); %#ok<NASGU>
        delete(nomFicGz)
    else
        str = sprintf('%s not found', nomFic, 'MyLocalDir', MyLocalDir);
        my_warndlg(str, 1);
        return
    end
end

[nomDir, nomFicParam] = fileparts(nomFic);

% ncdisp(nomFic)
finfo = ncinfo(nomFic);
listAttributsGlobaux = {finfo.Attributes(:).Name};
sAtt = [];
for k=1:numel(finfo.Attributes)
    sAtt.(listAttributsGlobaux{k}) = finfo.Attributes(k).Value;
end

listDimensions = {finfo.Dimensions(:).Name};
sDim = [];
for k=1:numel(finfo.Dimensions)
    sDim.(listDimensions{k}) = finfo.Dimensions(k).Length;
end

if strcmpi(NomParametre, 'Temperature')
    nameFieldValue  = 't_mn';
    nameFieldSigma  = 't_sd';
    nameFieldN      = 't_dd';
else
    nameFieldValue  = 's_mn';
    nameFieldSigma  = 's_sd';
    nameFieldN      = 's_dd';
end

LATITUDE       = ncread(nomFic, 'lat');
LONGITUDE      = ncread(nomFic, 'lon');
STANDARD_LEVEL = ncread(nomFic, 'depth');
% TIME           = ncread(nomFic, 'time');
TIMESTART      = sAtt.time_coverage_start;
TIMEDURATION   = sAtt.time_coverage_duration; % in months
Value          = ncread(nomFic, nameFieldValue);
Sigma          = ncread(nomFic, nameFieldSigma);
N              = ncread(nomFic, nameFieldN);

NomParam       = ncreadatt(nomFic, nameFieldValue, 'long_name'); %#ok<NASGU>
Unite          = ncreadatt(nomFic, nameFieldValue, 'units');
FillValue      = ncreadatt(nomFic, nameFieldValue, '_FillValue');


Value = single(Value);
Sigma = single(Sigma);
N     = single(N);

Value = permute(Value, [1 2 4 3]);
Sigma = permute(Sigma, [1 2 4 3]);
N     = permute(N,     [1 2 4 3]);

% figure; plot(LONGITUDE); grid on
% figure; plot(LATITUDE); grid on

sub = (Value == FillValue);
Value(sub) = NaN; %#ok<NASGU>
Sigma(sub) = NaN; %#ok<NASGU>
N(sub)     = NaN; %#ok<NASGU>

Climatology_Name = sAtt.title;  %#ok<NASGU>    % m�me nom de champ que Levitus.
Z               = -STANDARD_LEVEL'; %#ok<NASGU>
Lon             = LONGITUDE'; %#ok<NASGU>
Lat             = LATITUDE'; %#ok<NASGU>
Unit            = Unite; %#ok<NASGU>
NomParam        = NomParametre; %#ok<NASGU>
TIMESTART       = TIMESTART'; %#ok<NASGU>
TIMEDURATION    = TIMEDURATION'; %#ok<NASGU>

listVar = {'Climatology_Name'
            'TIMESTART'
            'TIMEDURATION'
            'NomParam'
            'Value'
            'Sigma'
            'N'
            'Z'
            'Lon'
            'Lat'
            'Unit'};
        
nomFicMat = fullfile(nomDir, [nomFicParam '.mat']);
fprintf('Saving file %s\n', nomFicMat)
save(nomFicMat, listVar{:}, '-v7.3')
