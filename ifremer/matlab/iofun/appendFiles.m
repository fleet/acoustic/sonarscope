function flag = appendFiles(nomFicRawCsv, nomFicOut, varargin)

[varargin, NbHeaders] = getPropertyValue(varargin, 'NbHeaders', 0); %#ok<ASGLU>

[flag, msg] = copyfile(nomFicRawCsv{1}, nomFicOut);
if ~flag
    my_warndlg(msg, 1);
    return
end

fid1 = fopen(nomFicOut, 'a+');
if fid1 == -1
    messageErreurFichier(nomFicOut, 'WriteFailure');
    return
end
for k1=2:length(nomFicRawCsv)
    fid2 = fopen(nomFicRawCsv{k1});
    if fid2 == -1
        messageErreurFichier(nomFicRawCsv{k1}, 'ReadFailure');
        return
    end
    for k2=1:NbHeaders
        fgetl(fid2);
    end
    while 1
        tline = fgetl(fid2);
        if ~ischar(tline)
            break
        end
        fprintf(fid1, '%s\n', tline);
    end
    fclose(fid2);
end
fclose(fid1);
