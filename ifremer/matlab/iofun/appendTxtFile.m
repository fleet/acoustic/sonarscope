function flag = appendTxtFile(nomFic, str)

OK = 0;
for k=1:10 % 10 tentatives espac�es de 5 secondes
    fid = fopen(nomFic, 'a+');
    if fid == -1
        pause(5)
    else
        OK = 1;
        break
    end
end
if OK
    fprintf(fid, '%s\n', str);
    flag = fclose(fid);
end
