% Passage d'une valeur "fichier" � une valeur physique d'une variable d'un
% fichier netcdf de Caraibes
%
% Syntax
%   val = cdf_Caraibes_valFic2valUnit(sk, nomVar, mbVal)
%
% Input Arguments
%   sk     : Squelette d'un fichier netcdf
%   nomVar : Nom de la variable
%   mbVal : Valeur de stockage dans le fichier
%
% Output Arguments
%   val    : Valeur physiaue
%
% Examples
%   nomFic = getNomFicDatabase('DTS1_ExStr.imo')
%   a = cl_netcdf('fileName', nomFic);
%   sk = get(a, 'skeleton')
%   cdf_disp_list_att(sk, 'Level', 1)
%   mbVal = get_CodedValue(a, 'grRawImage_grSyncHeading');
%   val = cdf_Caraibes_valUnit2valFic(sk, 'grRawImage_grSyncHeading', mbVal)
%   mbVal = cdf_Caraibes_valFic2valUnit(sk, 'grRawImage_grSyncHeading', val)
%
% See also cl_netcdf
% Authors : JMA
% -------------------------------------------------------------------------

function val = cdf_Caraibes_valFic2valUnit(sk, nomVar, mbVal)

numVar = cdf_find_numVar(sk, nomVar);

numAtt = cdf_find_numVarAtt(sk, numVar, 'add_offset');
add_offset = sk.var(numVar).att(numAtt).value;

numAtt = cdf_find_numVarAtt(sk, numVar, 'scale_factor');
scale_factor = sk.var(numVar).att(numAtt).value;

% numAtt = cdf_find_numVarAtt(sk, numVar, 'minimum');
% minimum = sk.var(numVar).att(numAtt).value;
%
% numAtt = cdf_find_numVarAtt(sk, numVar, 'maximum');
% maximum = sk.var(numVar).att(numAtt).value;
%
% numAtt = cdf_find_numVarAtt(sk, numVar, 'valid_minimum');
% valid_minimum = sk.var(numVar).att(numAtt).value;
%
% numAtt = cdf_find_numVarAtt(sk, numVar, 'valid_maximum');
% valid_maximum = sk.var(numVar).att(numAtt).value;

% numAtt = cdf_find_numVarAtt(sk, numVar, 'missing_value');
% missing_value = sk.var(numVar).att(numAtt).value;

% val(mbVal == missing_value) = NaN;

% mbVal = (val - add_offset) / scale_factor;
val = (mbVal * scale_factor) + add_offset;

