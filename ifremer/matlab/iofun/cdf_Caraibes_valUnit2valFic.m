% Passage d'une valeur physique � une valeur fichier d'une variable d'un
% fichier netcdf de Caraibes
%
% Syntax
%   mbVal = cdf_Caraibes_valUnit2valFic(sk, nomVar, val)
%
% Input Arguments
%   sk     : Squelette d'un fichier netcdf
%   nomVar : Nom de la variable
%   val    : Valeur physiaue
%
% Output Arguments
%   mbVal : Valeur de stockage dans le fichier
%
% Examples
%   nomFic = getNomFicDatabase('DTS1_ExStr.imo')
%   a = cl_netcdf('fileName', nomFic);
%   sk = get(a, 'skeleton')
%   mbVal = cdf_Caraibes_valUnit2valFic(sk, nomVar, val)
%
% See also cl_netcdf
% Authors : JMA
% -------------------------------------------------------------------------

function mbVal = cdf_Caraibes_valUnit2valFic(sk, nomVar, val)

numVar = cdf_find_numVar(sk, nomVar);

numAtt = cdf_find_numVarAtt(sk, numVar, 'add_offset');
add_offset = sk.var(numVar).att(numAtt).value;

numAtt = cdf_find_numVarAtt(sk, numVar, 'scale_factor');
scale_factor = sk.var(numVar).att(numAtt).value;

% numAtt = cdf_find_numVarAtt(sk, numVar, 'minimum');
% minimum = sk.var(numVar).att(numAtt).value;
%
% numAtt = cdf_find_numVarAtt(sk, numVar, 'maximum');
% maximum = sk.var(numVar).att(numAtt).value;
%
% numAtt = cdf_find_numVarAtt(sk, numVar, 'valid_minimum');
% valid_minimum = sk.var(numVar).att(numAtt).value;
%
% numAtt = cdf_find_numVarAtt(sk, numVar, 'valid_maximum');
% valid_maximum = sk.var(numVar).att(numAtt).value;

numAtt = cdf_find_numVarAtt(sk, numVar, 'missing_value');
missing_value = sk.var(numVar).att(numAtt).value;

mbVal = (val - add_offset) / scale_factor;
% val = (mbVal * scale_factor) + add_offset;

mbVal(isnan(val)) = missing_value;
