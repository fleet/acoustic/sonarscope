% Liste des attributs globaux d'un fichier netcdf de Caraibes
%
% Syntax
%   cdf_disp_list_att(sk, ...)
%
% Input Arguments
%   sk : Squelette d'un fichier netcdf ou nom de fichier
%
% Name-Value Pair Arguments
%   Level    : Niveau de d�tail : (1, 2 ou 3, 1 par d�faut)
%
% Examples
%   nomFic = getNomFicDatabase('DTS1_ExStr.imo')
%   a = cl_netcdf('fileName', nomFic);
%   sk = get(a, 'skeleton')
%   cdf_disp_list_att(sk, 'Level', 1)
%   cdf_disp_list_att(sk, 'Level', 2)
%   cdf_disp_list_att(sk, 'Level', 3)
%
%   cdf_disp_list_att(nomFic, 'Level', 1)
%   cdf_disp_list_att(nomFic, 'Level', 2)
%   cdf_disp_list_att(nomFic, 'Level', 3)
%
% See also cdf_find_numAtt cdf_disp_list_dim cdf_disp_list_var cl_netcdf
% Authors : JMA
% -------------------------------------------------------------------------

function cdf_disp_list_att(sk, varargin)

[varargin, Level] = getPropertyValue(varargin, 'Level', 1); %#ok<ASGLU>

if ischar(sk)
    if is_CDF(sk)
        a = cl_netcdf('fileName', sk);
        sk = get(a, 'skeleton');
    else
        str = sprintf('"%s" does not seem to be a NetCDF file', sk);
        my_warndlg(str, 1);
        return
    end
end

str = [];
for i=1:length(sk.att)
    if Level > 1
        str{end+1} = sprintf(' '); %#ok<AGROW>
    end
    str{end+1} = sprintf('\t\tsk.att(%d).name  = ''%s'';', i, sk.att(i).name); %#ok<AGROW>
    
    if Level > 1
        value = sk.att(i).value;
        sk.att(i).type  = ['nc', class(value)];
        type = sk.att(i).type;
        str{end+1} = sprintf('\t\tsk.att(%d).type   = ''%s'';', i, type); %#ok<AGROW>
    end
    
    if Level > 2
        value = sk.att(i).value;
        sk.att(i).type  = ['nc', class(value)];
        type = sk.att(i).type;
        switch type
            case 'ncchar'
                str{end+1} = sprintf('\t\tsk.att(%d).value = ''%s'';', i, value); %#ok<AGROW>
            case {'nclong'; 'ncshort'}
                str{end+1} = sprintf('\t\tsk.att(%d).value = [%s];', i, num2str(value)); %#ok<AGROW>
            case {'ncdouble'; 'ncfloat'}
                str{end+1} = sprintf('\t\tsk.att(%d).value = [%s];', i, num2str(value)); %#ok<AGROW>
            otherwise
                str{end+1} = sprintf('\t\tsk.att(%d).value = PAS ENCORE FAIT', i); %#ok<AGROW>
        end
    end
end

% ---------------------------
% Concatenation en une chaine

str = cell2str(str);
disp(str)
