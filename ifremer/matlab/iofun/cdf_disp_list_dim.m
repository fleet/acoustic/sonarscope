% Liste des dimensions d'un fichier netcdf de Caraibes
%
% Syntax
%   cdf_disp_list_dim(sk, ...)
%
% Input Arguments
%   sk : Squelette d'un fichier netcdf ou nom de fichier
%
% Name-Value Pair Arguments
%   Level : Niveau de d�tail : (1, 2 ou 3, 1 par d�faut)
%
% Examples
%   nomFic = getNomFicDatabase('DTS1_ExStr.imo')
%   a = cl_netcdf('fileName', nomFic);
%   sk = get(a, 'skeleton')
%   cdf_disp_list_dim(sk, 'Level', 1)
%
%   cdf_disp_list_dim(nomFic, 'Level', 1)
%
% See also cdf_disp_list_att cdf_disp_list_var cl_netcdf
% Authors : JMA
% -------------------------------------------------------------------------

function cdf_disp_list_dim(sk, varargin)

[varargin, Level] = getPropertyValue(varargin, 'Level', 1); %#ok<ASGLU>

if ischar(sk)
    if is_CDF(sk)
        a = cl_netcdf('fileName', sk);
        sk = get(a, 'skeleton');
    else
        str = sprintf('"%s" does not seem to be a NetCDF file', sk);
        my_warndlg(str, 1);
        return
    end
end

str = [];
for i=1:length(sk.dim)
    str{end+1} = sprintf(' '); %#ok<AGROW>
    str{end+1} = sprintf('\t\tsk.dim(%d).name  = ''%s'';', i, sk.dim(i).name); %#ok<AGROW>
    str{end+1} = sprintf('\t\tsk.dim(%d).value = %d;', i, sk.dim(i).value); %#ok<AGROW>
    
    if Level > 1
        str{end+1} = sprintf('\t\t\tAssociated variables'); %#ok<AGROW>
        listeVarAssociee = cdf_find_listVarAssociated2Dim(sk, sk.dim(i).name);
        for k=1:length(listeVarAssociee)
            str{end+1} = sprintf('\t\t\tsk.var(%d).name  = ''%s'';', listeVarAssociee(k), sk.var(listeVarAssociee(k)).name); %#ok<AGROW>
        end
    end
end

% ---------------------------
% Concatenation en une chaine

str = cell2str(str);
disp(str)
