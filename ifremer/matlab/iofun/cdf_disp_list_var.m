% Liste des variables d'un fichier netcdf de Caraibes
%
% Syntax
%   cdf_disp_list_var(sk, ...)
%
% Input Arguments
%   sk : Squelette d'un fichier netcdf ou nom de fichier
%
% Name-Value Pair Arguments
%   Level    : Niveau de d�tail : (1, 2 ou 3, 1 par d�faut)
%   VarListe : Liste des variables (toutes par d�faut)
%
% Examples
%   nomFic = getNomFicDatabase('EM300_Ex2.mnt')
%   a = cl_netcdf('fileName', nomFic);
%   sk = get(a, 'skeleton')
%   cdf_disp_list_var(sk, 'Level', 1)
%   cdf_disp_list_var(sk, 'Level', 2)
%   cdf_disp_list_var(sk, 'Level', 3)
%   cdf_disp_list_var(sk, 'Level', 3, 'VarListe', 2)
%
%   cdf_disp_list_var(nomFic, 'Level', 1)
%   cdf_disp_list_var(nomFic, 'Level', 2)
%   cdf_disp_list_var(nomFic, 'Level', 3)
%   cdf_disp_list_var(nomFic, 'Level', 3, 'VarListe', 82)
%
% See also cdf_disp_list_att cdf_disp_list_dim cl_netcdf
% Authors : JMA
% -------------------------------------------------------------------------

function cdf_disp_list_var(sk, varargin)

if isempty(sk)
    return
end

if ischar(sk)
    if is_CDF(sk)
        a = cl_netcdf('fileName', sk);
        sk = get(a, 'skeleton');
    else
        str = sprintf('"%s" does not seem to be a NetCDF file', sk);
        my_warndlg(str, 1);
        return
    end
end

[varargin, Level]    = getPropertyValue(varargin, 'Level', 1);
[varargin, VarListe] = getPropertyValue(varargin, 'VarListe', 1:length(sk.var)); %#ok<ASGLU>

str = [];
for k=1:length(VarListe)
    i = VarListe(k);
    PoutCent = '%';
    if Level > 1
        str{end+1} = sprintf(' '); %#ok<AGROW>
    end
    N = cdf_get_numelVar(sk, sk.var(i).name);
    str{end+1} = sprintf('\t\tsk.var(%d).name  = ''%s''; \t%s (%d values)', i, sk.var(i).name, PoutCent, N); %#ok<AGROW>
    if Level > 1
        str{end+1} = sprintf('\t\t%s value : lecture et ecriture par appel aux methodes :', PoutCent); %#ok<AGROW>
        str{end+1} = sprintf('\t\t           %s v = get_value(a, ''%s'');   a = set_value(a, ''%s'', v);', PoutCent, sk.var(i).name, sk.var(i).name); %#ok<AGROW>
        str{end+1} = sprintf('\t\t           %s v = get_value(a, %d);       a = set_value(a, %d, v);', PoutCent, i, i); %#ok<AGROW>
        str{end+1} = sprintf('\t\t           %s get_value et set_value acceptent le sous echantillonage', PoutCent); %#ok<AGROW>
        str{end+1} = sprintf('\t\t           %s get_value(a, ident, ''sub1'', sub1, ''sub2'', sub2, ...);', PoutCent); %#ok<AGROW>
        str{end+1} = sprintf('\t\tsk.var(%d).type  = ''%s'';', i, sk.var(i).type); %#ok<AGROW>
        
        for j=1:length(sk.var(i).dim)
            nomDim = sk.var(i).dim(j).name;
            numDim = cdf_find_numDim(sk, nomDim);
            valDim = sk.dim(numDim).value;
            str{end+1} = sprintf('\t\tsk.var(%d).dim(%d).name  = ''%s'';\t%s(value : %d)', i, j, nomDim, PoutCent, valDim); %#ok<AGROW>
        end
    end
    
    if Level > 2
        for j=1:length(sk.var(i).att)
            value = sk.var(i).att(j).value;
            sk.var(i).att(j).type  = ['nc', class(value)];
            type  = sk.var(i).att(j).type;
            str{end+1} = sprintf(' '); %#ok<AGROW>
            str{end+1} = sprintf('\t\tsk.var(%d).att(%d).name  = ''%s'';', i, j, sk.var(i).att(j).name); %#ok<AGROW>
            str{end+1} = sprintf('\t\tsk.var(%d).att(%d).type  = ''%s'';', i, j, type); %#ok<AGROW>
            switch type
                case {'ncchar'; 'char'}
                    str{end+1} = sprintf('\t\tsk.var(%d).att(%d).value = ''%s'';', i, j, value); %#ok<AGROW>
                case {'ncdouble'; 'double'}
                    str{end+1} = sprintf('\t\tsk.var(%d).att(%d).value = [%s];', i, j, num2str(value)); %#ok<AGROW>
                case {'ncfloat'; 'float'}
                    str{end+1} = sprintf('\t\tsk.var(%d).att(%d).value = [%s];', i, j, num2str(value)); %#ok<AGROW>
                case {'nclong'; 'long'}
                    str{end+1} = sprintf('\t\tsk.var(%d).att(%d).value = [%s];', i, j, num2str(value)); %#ok<AGROW>
                case {'ncshort'; 'short'}
                    str{end+1} = sprintf('\t\tsk.var(%d).att(%d).value = [%s];', i, j, num2str(value)); %#ok<AGROW>
                otherwise
                    str{end+1} = sprintf('\t\tsk.var(%d).att(%d).value = PAS ENCORE FAIT', i, i); %#ok<AGROW>
            end
        end
    end
end

% ---------------------------
% Concatenation en une chaine

str = cell2str(str);
disp(str)
