% Liste des variables associ�es � une dimension
%
% Syntax
%   [listVarNum, listVarName] = cdf_find_listVarAssociated2Dim(sk, dimName)
%
% Input Arguments 
%   sk      : Squelette d'un fichier netcdf
%   dimName : Nom(s) de la dimension
%
% Output Arguments 
%   listVarNum  : Liste des num�ros des variables associ�es � dimName
%   listVarName : Liste des noms des variables associ�es � dimName
%
% Examples
%   nomFic = getNomFicDatabase('')
%   a = cl_netcdf('fileName', nomFic);
%   sk = get(a, 'skeleton')
%   listVarNum = cdf_find_listVarAssociated2Dim(sk, dimName)
%
% See also cl_netcdf
% Authors : JMA
% -------------------------------------------------------------------------

function [listVarNum, listVarName] = cdf_find_listVarAssociated2Dim(sk, dimName)

if isnumeric(dimName)
    for k=1:length(dimName)
        dimName{k} = sk.dim(dimName).name;
    end
elseif ~iscell(dimName)
    dimName = {dimName};
end

listVarNum = [];
listVarName = {};
for k1=1:length(sk.var)
    OK = zeros(1,length(dimName));
    for k2=1:length(sk.var(k1).dim)
        nomDimVar = sk.var(k1).dim(k2).name;
        for k3=1:length(dimName)
            if strcmp(nomDimVar, dimName{k3})
                OK(k3) = OK(k3) + 1;
            end
        end
    end
    if sum(OK) == length(dimName)
        listVarNum(end+1) = k1; %#ok<AGROW>
        listVarName{end+1} = sk.var(k1).name; %#ok<AGROW>
    end
end
