% Recherche du numero d'un attribut global d'un fichiet netcdf
%
% Syntax
%   numAtt = cdf_find_numAtt(sk, nomAtt)
%
% Input Arguments 
%   ck     : Squelette du fichier netcdf ou nom de fichier
%   nomAtt : Nom de l'attribut
%
% Output Arguments 
%   numAtt : Numero de l'atribut de la variable ([] si pas trouvee)
%
% Examples
%   nomFic = getNomFicDatabase('DTS1_ExStr.imo')
%   a = cl_netcdf('fileName', nomFic);
%   sk = get(a, 'skeleton')
%   numAtt = cdf_find_numAtt(sk, 'mbStartDate')
%
%   numAtt = cdf_find_numAtt(nomFic, 'mbStartDate')
%
% See also cdf_find_numDim cdf_find_numVar cdf_find_numVarAtt cl_netcdf
% Authors : JMA
% ----------------------------------------------------------------------------

function numAtt = cdf_find_numAtt(sk, nomAtt)

if ischar(sk)
    if is_CDF(sk)
        a = cl_netcdf('fileName', sk);
        sk = get(a, 'skeleton');
    else
        str = sprintf('"%s" does not seem to be a NetCDF file', sk);
        my_warndlg(str, 1);
        return
    end
end

numAtt = [];
for k=1:length(sk.att)
    if strcmp(sk.att(k).name, nomAtt)
        numAtt = k;
        return
    end
end
