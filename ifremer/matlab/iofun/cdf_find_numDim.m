% Recherche du numero d'une dimension d'un fichiet netcdf
%
% Syntax
%   numDim = cdf_find_numDim(sk, nomDim)
%
% Input Arguments 
%   ck     : Squelette du fichier netcdf ou nom de fichier
%   nomDim : Nom de la dimension
%
% Output Arguments 
%   numDim : Numero de la dimension ([] si pas trouvee)
%
% Examples
%   nomFic = getNomFicDatabase('DTS1_ExStr.imo')
%   a = cl_netcdf('fileName', nomFic);
%   sk = get(a, 'skeleton')
%   cdf_disp_list_dim(sk, 'Level', 1)
%   numDim = cdf_find_numDim(sk, 'grWrkImage_NbLin')
%
%   numDim = cdf_find_numDim(nomFic, 'grWrkImage_NbLin')
%
% See also cdf_find_numAtt cdf_find_numVar cdf_find_numVarAtt cl_netcdf
% Authors : JMA
% VERSION  : $Id:$
% ----------------------------------------------------------------------------

function numDim = cdf_find_numDim(sk, nomDim)

if ischar(sk)
    if is_CDF(sk)
        a = cl_netcdf('fileName', sk);
        sk = get(a, 'skeleton');
    else
        str = sprintf('"%s" does not seem to be a NetCDF file', sk);
        my_warndlg(str, 1);
        return
    end
end

numDim = [];
for i=1:length(sk.dim)
    if strcmp(sk.dim(i).name, nomDim)
        numDim = i;
        return
    end
end
