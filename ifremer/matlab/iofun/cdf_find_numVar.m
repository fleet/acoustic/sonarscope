% Recherche du numero d'une variable d'un fichier netcdf
%
% Syntax
%   numVar = cdf_find_numVar(sk, nomVar)
%
% Input Arguments 
%   sk     : Squelette d'un fichier netcdf ou nom de fichier
%   nomVar : Nom de la variable
%
% Output Arguments 
%   numVar : Numero de la variable ([] si pas trouvee)
%
% Examples
%   nomFic = getNomFicDatabase('DTS1_ExStr.imo')
%   a = cl_netcdf('fileName', nomFic);
%   sk = get(a, 'skeleton')
%   cdf_disp_list_var(sk, 'Level', 1)
%   numVar = cdf_find_numVar(sk, 'grWrkImage')
%
%   numVar = cdf_find_numVar(nomFic, 'grWrkImage')
%
% See also cdf_find_numAtt cdf_find_numDim cdf_find_numVarAtt cl_netcdf
% Authors : JMA
% VERSION  : $Id:$
% ----------------------------------------------------------------------------

function numVar = cdf_find_numVar(sk, nomVar)

if ischar(sk)
    if is_CDF(sk)
        a = cl_netcdf('fileName', sk);
        sk = get(a, 'skeleton');
    else
        str = sprintf('"%s" does not seem to be a NetCDF file', sk);
        my_warndlg(str, 1);
        return
    end
end

numVar = [];
for i=1:length(sk.var)
    if strcmp(sk.var(i).name, nomVar)
        numVar = i;
        return
    end
end
