% Recherche du numero d'un attribut d'une variable d'un fichiet netcdf
%
% Syntax
%   numVar = cdf_find_numVarAtt(sk, numVar, nomAtt)
%
% Input Arguments 
%   ck     : Squelette du fichier netcdf ou nom de fichier
%   numVar : Numero de la variable
%   nomAtt : Nom de l'attribut
%
% Output Arguments 
%   numAtt : Numero de l'atribut de la variable ([] si pas trouvee)
%
% Examples
%   nomFic = getNomFicDatabase('DTS1_ExStr.imo')
%   a = cl_netcdf('fileName', nomFic);
%   sk = get(a, 'skeleton')
%   cdf_disp_list_var(sk, 'Level', 3)
%   numVar = cdf_find_numVar(sk, 'grWrkImage')
%   numAtt = cdf_find_numVarAtt(sk, numVar, 'add_offset')
%
%   numVar = cdf_find_numVar(nomFic, 'grWrkImage')
%   numAtt = cdf_find_numVarAtt(nomFic, numVar, 'add_offset')
%
% See also cl_netcdf
% Authors : JMA
% VERSION  : $Id:$
% ----------------------------------------------------------------------------

function numAtt = cdf_find_numVarAtt(sk, numVar, nomAtt)

if ischar(sk)
    if is_CDF(sk)
        a = cl_netcdf('fileName', sk);
        sk = get(a, 'skeleton');
    else
        str = sprintf('"%s" does not seem to be a NetCDF file', sk);
        my_warndlg(str, 1);
        return
    end
end

numAtt = [];
for i=1:length(sk.var(numVar).att)
    if strcmp(sk.var(numVar).att(i).name, nomAtt)
        numAtt = i;
        return
    end
end
