% Nombre de valeurs d'une variable d'un fichier NetCDF
%
% Syntax
%   N = cdf_get_numelVar(sk, varName)
%
% Input Arguments 
%   sk      : Squelette d'un fichier netcdf
%   varName : Nom de la variable
%
% Examples
%   nomFic = getNomFicDatabase('DTS1_ExStr.imo')
%   a = cl_netcdf('fileName', nomFic);
%   sk = get(a, 'skeleton')
%   N = cdf_get_numelVar(sk, 'grWrkImage')
%
% See also cl_netcdf
% Authors : JMA
% VERSION  : $Id:$
% -------------------------------------------------------------------------


function N = cdf_get_numelVar(sk, varName)

numVar = cdf_find_numVar(sk, varName);

N = 1;
for i=1:length(sk.var(numVar).dim)
    dimName = sk.var(numVar).dim(i).name;
    numDim = cdf_find_numDim(sk, dimName);
    N = N * sk.dim(numDim).value;
end
