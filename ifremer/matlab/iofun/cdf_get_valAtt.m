% Recuperation de la valeur d'un attribut d'un fichiet netcdf
%
% Syntax
%   valAtt = cdf_get_valAtt(sk, nomAtt)
%
% Input Arguments 
%   ck     : Squelette du fichier netcdf
%   nomAtt : Nom de l'attribut
%
% Output Arguments 
%   valAtt : Valeur de l'attribut ([] si pas trouvee)
%
% Examples
%   nomFic = getNomFicDatabase('DTS1_ExStr.imo')
%   a = cl_netcdf('fileName', nomFic);
%   sk = get(a, 'skeleton')
%   cdf_disp_list_dim(sk, 'Level', 1)
%   valAtt = cdf_get_valAtt(sk, 'CIB_BLOCKS_FOR_UNLIMITED')
%
% See also cl_netcdf
% Authors : JMA
% VERSION  : $Id:$
% ----------------------------------------------------------------------------

function valAtt = cdf_get_valAtt(sk, nomAtt)

numAtt = cdf_find_numAtt(sk, nomAtt);
if ~isempty(numAtt)
   valAtt = sk.att(numAtt).value;
else
   my_warndlg(Lang('Attribut inexistant dans le squelette de l''objet','Attribut does not exist in the object''s skelet '),1);
   valAtt = [];
end

 