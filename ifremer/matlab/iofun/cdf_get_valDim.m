% Recuperation de la valeur d'une dimension d'un fichiet netcdf
%
% Syntax
%   valDim = cdf_get_valDim(sk, nomDim)
%
% Input Arguments 
%   ck     : Squelette du fichier netcdf
%   nomDim : Nom de la dimension
%
% Output Arguments 
%   valDim : Valeur de la dimension ([] si pas trouvee)
%
% Examples
%   nomFic = getNomFicDatabase('DTS1_ExStr.imo')
%   a = cl_netcdf('fileName', nomFic);
%   sk = get(a, 'skeleton')
%   cdf_disp_list_dim(sk, 'Level', 1)
%   valDim = cdf_get_valDim(sk, 'grWrkImage_NbLin')
%
% See also cl_netcdf
% Authors : JMA
% VERSION  : $Id:$
% ----------------------------------------------------------------------------

function valDim = cdf_get_valDim(sk, nomDim)

numDim = cdf_find_numDim(sk, nomDim);
if ~isempty(numDim)
	valDim = sk.dim(numDim).value;
else
	my_warndlg(Lang('Dimension inexistante dans le squelette de l''objet','Dimension does not exist in the object''s skelet '),1);
	valDim = [];
end
 