% Recuperation de la valeur d'un attribut d'une variable d'un fichiet netcdf
%
% Syntax
%   valVarAtt = cdf_get_valVarAtt(sk, numVar, nomAtt)
%
% Input Arguments 
%   ck     : Squelette du fichier netcdf
%   numVar : Numero de la variable
%   nomAtt : Nom de l'attribut
%
% Output Arguments 
%   valVarAtt : Numero de la dimension ([] si pas trouvee)
%
% Examples
%   nomFic = getNomFicDatabase('DTS1_ExStr.imo')
%   a = cl_netcdf('fileName', nomFic);
%   sk = get(a, 'skeleton')
%   cdf_disp_list_var(sk, 'Level', 1)
%   valVarAtt = cdf_get_valVarAtt(sk, 81, 'Commentaire')
%
% See also cl_netcdf
% Authors : JMA
% VERSION  : $Id:$
% ----------------------------------------------------------------------------

function valVarAtt = cdf_get_valVarAtt(sk, numVar, nomAtt, varargin)

[varargin, noMessage] = getFlag(varargin, 'noMessage'); %#ok<ASGLU>

numAtt = cdf_find_numVarAtt(sk, numVar, nomAtt);
if isempty(numAtt)
    if ~noMessage
        my_warndlg(Lang('Attribut de la variable inexistant dans le squelette de l''objet','Attribut of the variable does not exist in the object''s skelet '),1);
    end
    valVarAtt = [];
else
	valVarAtt = sk.var(numVar).att(numAtt).value;
end

 
