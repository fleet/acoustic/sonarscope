function checkLengthFilename(nomFic)

if length(nomFic) > 255
    str1 = sprintf('Le nom de fichier "%s" est sans doute trop long, il se peut que vous rencontriiez des probl�mes sur certaines machines.', nomFic);
    str2 = sprintf('File name "%s" seems to be too long, you may be in trouble.', nomFic);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'checkLengthFilename');
end
