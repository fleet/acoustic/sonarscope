function compressDirectories_SSc(nomFic, varargin)

[varargin, Tag] = getPropertyValue(varargin, 'Tag', []); %#ok<ASGLU>

if ~iscell(nomFic)
    nomFic = {nomFic};
end

if isempty(Tag)
    str1 = 'Compression des fichiers';
    str2 = 'Zipping files';
else
    str1 = ['Compression des fichiers ' Tag];
    str2 = ['Zipping ' Tag ' files'];
end

N   = length(nomFic);
hw  = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    zipSonarScopeCache(nomFic{k})
end
my_close(hw, 'MsgEnd');

if isempty(Tag)
    str1 = 'La compression des fichiers est termin�e';
    str2 = 'The compression of the cache files is over.';
else
    str1 = ['La compression des fichiers ' Tag ' est termin�e'];
    str2 = ['The compression of the ' Tag ' cache files is over.'];
end
my_warndlg(Lang(str1,str2), 0);
