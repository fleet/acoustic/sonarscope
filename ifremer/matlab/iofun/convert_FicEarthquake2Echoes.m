%   FileName = 'D:\Temp\data_louis2013\hypoDD_Nlloc_3DQuentinBSN.txt';
%   convert_FicEarthquake2Echoes(FileName, 500, 4000, 'Epicenter')

function convert_FicEarthquake2Echoes(FileName, Step, Max, Name)

%% Lecture du fichier

[flag, X, Y, V] = read_ASCII(FileName, 'DisplayEntete', 0, 'NbLigEntete', 1, ...
    'AskSeparator', 0, 'Separator', [], 'AskValNaN', 0, 'ValNaN', [], ...
    'nColumns', 4, 'AskGeometryType', 0, 'GeometryType', 3, ...
    'IndexColumns', [2 1 3 4], 'DataType', 2, 'Unit', 'm');
% FigUtils.createSScFigure; PlotUtils.createSScPlot(X, Y, '*'); grid on
% FigUtils.createSScFigure; plot3(X, Y, Points.Z, '*'); grid on;

%% Récupération de la profondeur

Z = -V(:,1)*1000;

%% Récupération de l'heure

t = yyyymmddhhmmss2Mat(V(:,2));
T = t.timeMat;
% figure; plot(floor(T), '-*'); grid on; title('T')
tDeb = floor(T(1));
T = T - tDeb;
% figure; plot(floor(T), '-*'); grid on; title('T')

%% Echoes

Points.Latitude  = Y;
Points.Longitude = X;
Points.Z         = Z;
Points.Energie   = T;
Points.iPing     = 1:length(T);
% WC_plotEchoes(Points, 'Name', FileName)

[nomDir, nomFic] = fileparts(FileName);
nomFicEchoes = fullfile(nomDir, [nomFic 'aaaa_3D.xml']);
flag = WC_SaveEchoes(nomFicEchoes, Points);

%{
[flag, Data] = XMLBinUtils.readGrpData(nomFicEchoes)
WC_plotEchoes(Data)
%}

%% Echoes par jour

N = ceil(max(T)); % Nombre de jours
nbPointsDay = zeros(1, N);
for k=1:N
    sub = find((T >= (k-1)) & (T < k));
    nbPointsDay(k) = length(sub);
    if ~isempty(sub)
        Points.Latitude  = Y(sub);
        Points.Longitude = X(sub);
        Points.Z         = Z(sub);
        Points.Energie   = T(sub);
        % WC_plotEchoes(Points, 'Name', FileName)
        
        [nomDir, nomFic] = fileparts(FileName);
        nomFicEchoes = fullfile(nomDir, [nomFic '_3D_Day' num2str(k) '.xml']);
        flag = WC_SaveEchoes(nomFicEchoes, Points);
    end
end

%% Markers : Couleur = profondeur

FileNameMarkers = fullfile(nomDir, [nomFic '_Depth.xml']);
PieMarkers.Title         = 'Classe Markers';
PieMarkers.FormatVersion = 20130419;
PieMarkers.Comments      = 'Created from original Data';
PieMarkers.Filename = FileNameMarkers;

N = ceil(Max/Step);
map = jet(N);
map = [map; 0 0 0];
mapUint8 = uint8(255*map);
N = N + 1;
for k=1:N
    PieMarkers.DataTypeDescriptor.DataType(k).Id = k;
    if k == N
        PieMarkers.DataTypeDescriptor.DataType(k).Name = sprintf('%s > %d m', Name, (k-1)*500); % 'Epicentre<5km';
    else
        PieMarkers.DataTypeDescriptor.DataType(k).Name = sprintf('%s [%d %d] m', Name, (k-1)*500, k*500); % 'Epicentre<5km';
    end
    PieMarkers.DataTypeDescriptor.DataType(k).Color = ['#' sprintf('%02X', mapUint8(k,:))]; % '#FF0000';
    PieMarkers.DataTypeDescriptor.DataType(k).NameColor = sprintf('Color%d', k);
end

for k=1:length(X)
    PieMarkers.PieMarker(k).Id  = k;
    PieMarkers.PieMarker(k).Lat = Y(k);
    PieMarkers.PieMarker(k).Lon = X(k);
    PieMarkers.PieMarker(k).PieData.DataTypeId = min(N, floor(-Z(k)/500));
    PieMarkers.PieMarker(k).PieData.Percent    = 1;
end
DPref.StructItem = false;
DPref.CellItem   = false;
xml_write(FileNameMarkers, PieMarkers, 'PieMarkers', DPref);

%% Markers : Couleur = Temps

clear PieMarkers
FileNameMarkers = fullfile(nomDir, [nomFic '_Time.xml']);
PieMarkers.Title         = 'Classe Markers';
PieMarkers.FormatVersion = 20130419;
PieMarkers.Comments      = 'Created from original Data';
PieMarkers.Filename      = FileNameMarkers;

N = ceil(max(T)); % Nombre de jours
map = jet(N);
% map = [map; 0 0 0];
mapUint8 = uint8(255*map);
% N = N + 1;
for k=1:N
    PieMarkers.DataTypeDescriptor.DataType(k).Id = k;
    PieMarkers.DataTypeDescriptor.DataType(k).Name = sprintf('%s (%d)', dayMat2str(tDeb+k-1), nbPointsDay(k)); % 'Epicentre<5km';
    PieMarkers.DataTypeDescriptor.DataType(k).Color = ['#' sprintf('%02X', mapUint8(k,:))]; % '#FF0000';
    PieMarkers.DataTypeDescriptor.DataType(k).NameColor = sprintf('Color%d', k);
end

for k=1:length(X)
    PieMarkers.PieMarker(k).Id  = k;
    PieMarkers.PieMarker(k).Lat = Y(k);
    PieMarkers.PieMarker(k).Lon = X(k);
    PieMarkers.PieMarker(k).PieData.DataTypeId = min(N, floor(T(k))+1);
    PieMarkers.PieMarker(k).PieData.Percent    = 1;
end
DPref.StructItem = false;
DPref.CellItem   = false;
xml_write(FileNameMarkers, PieMarkers, 'PieMarkers', DPref);
