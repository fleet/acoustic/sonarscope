% Comptage du nombre de lignes d'un fichier ASCII. Utlisation d'un script
% PERL tr�s efficace pour le faire.
%
% Syntax
%   N = countLines(nomFic)
%
% Input Arguments 
%   nomFic : nom du fichier
%
% Output Arguments 
%   nbLines : nombre de lignes du fichier.
%
% Examples
%   nomFic = 'C:\Temp\tete_elevation_v2.xml';
%   nbLines = countLines(nomFic)
%
% See also /
% Authors : GLU
% -------------------------------------------------------------------------

function NLignesTotal = countLines(nomFic)

% Comptage du nombre de lignes
try
    NLignesTotal = str2double(perl('countlines.pl', nomFic));
catch %#ok<CTCH>
    NLignesTotal = getNbLinesAsciiFile(nomFic);
end
