function flag = create_XMLBin_ImagesAlongNavigation(I, NomFicXML, map, InfoData)

[nbPings, nbRows] = size(I);

%% Cr�ation du r�pertoire

[nomDirXml, nomDirBin] = fileparts(NomFicXML);
nomDirXml = fullfile(nomDirXml, nomDirBin);
nomDirRoot = fileparts(nomDirXml);
if ~exist(nomDirXml, 'dir')
    flag = mkdir(nomDirXml);
    if ~flag
        messageErreurFichier(nomDirXml, 'WriteFailure');
        return
    end
end

%% Ecriture de l'image PNG

nomFic1 = '00001.png';
nomFic2 = fullfile(nomDirXml, nomFic1);
imwrite(I', map, nomFic2, 'Transparency', 0)

Data.AcrossDistance  =  0;
Data.SliceExists     = true;
Data.Date            = InfoData.Date;
Data.Hour            = InfoData.Hour;
Data.PingNumber      = InfoData.PingNumber;
Data.Tide            = InfoData.Tide;
Data.LatitudeBottom  = InfoData.LatitudeBottom;
Data.LongitudeBottom = InfoData.LongitudeBottom;
Data.LatitudeTop     = InfoData.LatitudeTop;
Data.LongitudeTop    = InfoData.LongitudeTop;
Data.DepthTop        = InfoData.DepthTop;
Data.DepthBottom     = InfoData.DepthBottom;
Data.iSlices         = 1;

%% Attributs globaux

Info.Signature1      = 'SonarScope';
Info.Signature2      = 'ImagesAlongNavigation';
% Info.FormatVersion   = 20100624;
Info.FormatVersion   = 20130125;
Info.Name            = InfoData.name;
Info.ExtractedFrom   = InfoData.from;

Info.Survey         = 'Unknown';
Info.Vessel         = 'Unknown';
Info.Sounder        = 'Unknown';
Info.ChiefScientist = 'Unknown';

%% Dimensions

Info.Dimensions.nbPings  = nbPings;
Info.Dimensions.nbSlices = 1;
Info.Dimensions.nbRows   = nbRows;

%% Signaux

Info.Signals(1).Name          = 'Date';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'Ifremer';
Info.Signals(end).FileName    = fullfile(nomDirBin,'Date.bin');

Info.Signals(end+1).Name      = 'Hour';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'nb milliseconds since midnight';
Info.Signals(end).FileName    = fullfile(nomDirBin,'Hour.bin');

Info.Signals(end+1).Name      = 'PingNumber';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = ' ';
Info.Signals(end).FileName    = fullfile(nomDirBin,'PingNumber.bin');

Info.Signals(end+1).Name      = 'Tide';
Info.Signals(end).Dimensions  = '1, nbPings'; %'1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirBin,'Tide.bin');

Info.Signals(end+1).Name      = 'LatitudeBottom';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirBin, 'LatitudeBottom.bin');

Info.Signals(end+1).Name      = 'LongitudeBottom';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirBin, 'LongitudeBottom.bin');

Info.Signals(end+1).Name      = 'SliceExists';
Info.Signals(end).Dimensions  = 'nbSlices, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirBin,'SliceExists.bin');

Info.Signals(end+1).Name      = 'iSlices';
Info.Signals(end).Dimensions  = 'nbSlices, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirBin,'iSlices.bin');

Info.Signals(end+1).Name      = 'DepthTop';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirBin, 'DepthTop.bin');

Info.Signals(end+1).Name      = 'DepthBottom';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirBin, 'DepthBottom.bin');

Info.Signals(end+1).Name      = 'AcrossDistance';
Info.Signals(end).Dimensions  = 'nbSlices, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirBin, 'AcrossDistance.bin');

%% Images

Info.Images(1).Name           = 'LatitudeTop';
Info.Images(end).Dimensions   = 'nbSlices, nbPings';
Info.Images(end).Storage      = 'double';
Info.Images(end).Unit         = 'deg';
Info.Images(end).FileName     = fullfile(nomDirBin, 'LatitudeTop.bin');

Info.Images(end+1).Name       = 'LongitudeTop';
Info.Images(end).Dimensions   = 'nbSlices, nbPings';
Info.Images(end).Storage      = 'double';
Info.Images(end).Unit         = 'deg';
Info.Images(end).FileName     = fullfile(nomDirBin,'LongitudeTop.bin');

% Info.Images(end+1).Name       = 'Reflectivity';
% Info.Images(end).Dimensions   = 'nbRows, nbPings, nbSlices';
% Info.Images(end).Storage      = 'single';
% Info.Images(end).Unit         = 'dB';
% Info.Images(end).FileName     = fullfile(nomDirBin,'Reflectivity.bin');


%% Cr�ation du fichier XML d�crivant la donn�e

% NomFicXML = fullfile(nomDirRacine, [TypeDataVoxler '.xml']);
xml_write(NomFicXML, Info);
flag = exist(NomFicXML, 'file');
if ~flag
    messageErreur(NomFicXML)
    return
end

%% Cr�ation du r�pertoire Info

nomDirInfo = nomDirXml;
if ~exist(nomDirInfo, 'dir')
    status = mkdir(nomDirInfo);
    if ~status
        messageErreur(nomDirInfo)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:length(Info.Signals)
    flag = writeSignal(nomDirRoot, Info.Signals(k), Data.(Info.Signals(k).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:length(Info.Images)
    flag = writeImage(nomDirRoot, Info.Images(k), Data.(Info.Images(k).Name));
    if ~flag
        return
    end
end

% % % % Comment� pour l'instant flag = zipDirectory(nomDirInfo);
