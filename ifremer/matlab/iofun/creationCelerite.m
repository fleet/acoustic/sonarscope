% 
% nomFicTemp = getNomFicDatabase('Temperature_Annual01.ers');
% nomFicPsal = getNomFicDatabase('Salinity_Annual01.ers');
% creationCelerite(nomFicTemp, nomFicPsal)
% 
% for i=1:4
%     nomFicTemp = sprintf('Temperature_Season%02d', i);
%     nomFicPsal = sprintf('Salinity_Season%02d', i);
%     nomFicTemp = getNomFicDatabase(nomFicTemp);
%     nomFicPsal = getNomFicDatabase(nomFicPsal);
%     creationCelerite(nomFicTemp, nomFicPsal)
% end
% 
% for i=1:12
%     nomFicTemp = sprintf('Temperature_Month%02d', i);
%     nomFicPsal = sprintf('Salinity_Month%02d', i);
%     nomFicTemp = getNomFicDatabase(nomFicTemp);
%     nomFicPsal = getNomFicDatabase(nomFicPsal);
%     creationCelerite(nomFicTemp, nomFicPsal)
% end




function creationCelerite(nomFicTemp, nomFicPsal)

[flag, T] = cl_image.import_ermapper(nomFicTemp);
[flag, S] = cl_image.import_ermapper(nomFicPsal);

CLimGlobal = [Inf -Inf];
for i=1:length(T)
    C(i) = celeriteChen(T(i), S(i)); %#ok
    CLim = C(i).CLim;
    CLimGlobal(1) = min(CLimGlobal(1), CLim(1));
    CLimGlobal(2) = max(CLimGlobal(2), CLim(2));
end

for i=1:length(T)
    C(i).CLim = CLimGlobal;
end

[nomDir, nomFic] = fileparts(nomFicTemp);
mots = strsplit(nomFic, '_');
nomFicOut = fullfile(nomDir, ['Velocity_' mots{2} '.ers']);
export_ermapper(C, nomFicOut);

