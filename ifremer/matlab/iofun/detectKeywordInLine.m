% D�tection d'un mot-cl� dans une ligne de caract�res.
%
% Syntax
%   [flag, numKeyword] = detectKeywordInLine(strKeywords, line)
%
% Input Arguments
%   strKeywords : Liste de mots-cl�s � rechercher dans la cha�ne.
%   line        : cha�ne.
%
% Name-Value Pair Arguments
%
%
% Output Arguments
%   flag        : indicateur de bon fonctionnement.
%   numKeyword  : num�ro de mot-cl� dans le tableau (-1 si non-trouv�).
%
% Examples
%     motsCles = {'Stb', 'Port', 'Starboard', 'Portboard', 'Cent', 'Central', 'Sector'};
%     line = 'On est bien du c�t� Starboard';
%   [flag, numKeyword] = detectKeywordInLine(motsCles, line)
%
% See also cl_simrad_all
% Authors : GLU
% ----------------------------------------------------------------------------

function [flag, numKeyword] = detectKeywordInLine(strKeywords, line, varargin)

[varargin, charStart] = getPropertyValue(varargin, 'charStart', ''); %#ok<ASGLU>

flag       = 0; %#ok<NASGU>
numKeyword = -1;
for k=1:numel(strKeywords)
    %if strfind(upper(line), upper(strKeywords{k}))
    X = regexp(line, [charStart strKeywords{k}], 'match', 'ignorecase');
    if ~isempty(X)
        numKeyword = k;
        % On ne sort pas car on peut rencontrer des chaines
        % satisfaisant � la recherche apr�s un autre r�sultat OK :
        % exemple du Deep.
        break;
    end
end
flag = 1;
