% manager compressed / uncompressed files
%
% Syntax
%    [f, c] = dezip (f, tmp)
% 
% Input Arguments
%   f   : compressed/uncompressed file
%   tmp : temp directory
% Output Arguments
%   f   : uncompressed file
%   c   : compressed flag (0:uncompressed, 1 compressed)
%
% Examples
%    [f, c] = dezip ('ex.Z', '/tmp');
%    exist (f)
%    if (c==1), delete (f); end ;
%    
% Remarks :
%    . manages .Z, .gz, .zip, .bz
%    . following tools must be in user path :
%      compress
%      gzip
%      bzip2
%
% See also 
% Authors : DCF
% VERSION  : $Id$
% ----------------------------------------------------------------------------

function [f, c] = dezip(f, tmp)

isOk = 1;
ret  = f;
c    = 0;

% -----
% Tests

if ~exist(f)
    isOk = 0;
    my_warndlg('dezip : Invalid compressed file name', 1);
elseif ~exist(tmp)
    isOk = 0;
    my_warndlg ('dezip : Invalid temp directory name', 1);
end

% --------------------------------------------
% Check file ext and uncompresses if necessary

if isOk
    [dir, file, ext] = fileparts(f);
    switch ext
        case '.Z'
            c = 1 ;
            % cmd = 'compress -d ';
            cmd = 'gzip -d -S .Z ';
        case '.zip'
            c = 1 ;
            cmd = 'gzip -d -S .zip ';
        case '.gz'
            c = 1 ;
            cmd = 'gzip -d ';
        case '.bz2'
            c = 1 ;
            cmd = 'bzip2 -d ';
        otherwise
            c = 0 ;
    end
    if c == 1
        cmd = ['\cp -f ', f, ' ', tmp, '; ', cmd, fullfile(tmp,[file,ext])];
        % disp (cmd);
        unix (cmd);
        ret = fullfile(tmp,file);
    end
    f = ret;
end

