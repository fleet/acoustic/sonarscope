% Description
%   Display the content of a text file in a figure
%
% Syntax
%   flag = displayTxtFile(nomFicTxt)
%
% Input Arguments
%   nomFicTxt : Text file name
%
% Output Arguments
%   flag : 0=KO, 1=OK
%
% Examples
%   nomFicTxt = 'D:\Temp\ImportFlagsAssociationFiles.txt';
%   flag = displayTxtFile(nomFicTxt)
%
% Authors  : JMA
% See also : textscan my_listdlg
%-------------------------------------------------------------------------------

function flag = displayTxtFile(nomFicTxt)

flag = 0;

if ~exist(nomFicTxt, 'file')
    return
end

fid = fopen(nomFicTxt, 'r');
X = textscan(fid, '%s', 'Delimiter', newline);
fclose(fid);

[~, flag] = my_listdlg(['Content of ' nomFicTxt], X{1}, 'InitialValue', []);
