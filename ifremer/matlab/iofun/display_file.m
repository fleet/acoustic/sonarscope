 % Affichage dans un fenetre d'un fichier ASCII
 %
 % Syntax
 %   display_file(nomFic)
 % 
 % Input Arguments 
 %   nomFic : Nom du fichier
 %
 % Examples
 %   nomFic = 'dfgsdfgsdfgsdfgs';
 %   display_file(nomFic)
 %
 % See also Authors
 % Authors : JMA
 %-------------------------------------------------------------------------------

 function display_file(nomFic)

tline = {};
fid = fopen(nomFic);
if fid == -1
    return
end
while 1
    str = fgetl(fid);
    if ~ischar(str)
        break
    end
    tline{end+1} = str; %#ok
    if length(tline) > 100
        tline{end+1} = 'File to long to be displayed'; %#ok
        break
    end
end
fclose(fid);

edit_infoPixel(tline, [], 'PromptString', 'WMS info')
