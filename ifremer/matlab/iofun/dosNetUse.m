% Utilis� dans  ALL_SurveyReport_Summary et ALL_SurveyReport_Navigation

function dosNetUse(str)

global DosNetUseValue %#ok<GVMIS> 

switch str
    case 'Init'
%         [~, DosNetUseValue] = dos('net use'); % remplac� par JMA le 24/01/2019 car liste incompl�te !!!
        
        nomFic = fullfile(my_tempdir, 'DosNetUseValue.txt');
        cmd = sprintf('net use > "%s"', nomFic);
        dos(cmd);
        
        fid = fopen(nomFic, 'r');
        DosNetUseValue = textscan(fid, '%s', 'Delimiter', newline);
        fclose(fid);
        DosNetUseValue = DosNetUseValue{1};
        
        for k=length(DosNetUseValue):-1:1
            if ~contains(DosNetUseValue{k}, '\\')
                DosNetUseValue(k) = [];
            end
        end
    otherwise
        DosNetUseValue = [];
end
