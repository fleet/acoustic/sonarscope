% Ecriture du descripeur des fichiers ER-Mapper
%
% Syntax
%   status = ecrParamsFicERMapper(DatasetHeader, nomFic, typeCodage)
%
% Input Arguments
%   nomFic        : Nom du fichier
%   DatasetHeader : Parametres de l'image
%   typeCodage    : Type de coage 'float64',
%
% Output Arguments
%   status : 1 ou 0 si le fichier est correctement ecrit
%
% Examples
%    nomFic = '/home/doppler/tmsias/augustin/MRFormatFichier/R2_9925_sonarmosaic';
%    DatasetHeader = lecParamsFicERMapper([nomFic '.ers'])
%    DatasetHeader = lecParamsFicERMapper('/home/doppler/tmsias/augustin/MRFormatFichier/R2_9925.ers')
%    status = ecrParamsFicERMapper(DatasetHeader, '/home/doppler/tmsias/augustin/MRFormatFichier/test.ers')
%    pppp = lecParamsFicERMapper('/home/doppler/tmsias/augustin/MRFormatFichier/test.ers')
%
% See also lecParamsFicERMapper Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function status = ecrParamsFicERMapper(DatasetHeader, nomFic, varargin)

status = 0;
fid = fopen(nomFic, 'w+');
if fid == -1
    disp('Fichier non trouve');
    return
end
[~, nomFic, nomExt] = fileparts(nomFic);
nomFic = [nomFic nomExt];

[varargin, typeCodage] = getPropertyValue(varargin, 'typeCodage', DatasetHeader.RasterInfo.CellType); %#ok<ASGLU>

fprintf(fid, 'DatasetHeader Begin\n');
fprintf(fid, '\tVersion\t= "%s"\n', DatasetHeader.Version);
fprintf(fid, '\tName\t= "%s"\n', nomFic);
%     fprintf(fid, '\tLastUpdated\t= %s\n', DatasetHeader.LastUpdated);
fprintf(fid, '\tDataSetType\t= %s\n', DatasetHeader.DataSetType);
fprintf(fid, '\tDataType\t= %s\n', DatasetHeader.DataType);
fprintf(fid, '\tByteOrder\t= %s\n', DatasetHeader.ByteOrder);

fprintf(fid, '\tCoordinateSpace Begin\n');
fprintf(fid, '\t\tDatum\t= "%s"\n', DatasetHeader.CoordinateSpace.Datum);
fprintf(fid, '\t\tProjection\t= "%s"\n', DatasetHeader.CoordinateSpace.Projection);
fprintf(fid, '\t\tCoordinateType\t= %s\n', DatasetHeader.CoordinateSpace.CoordinateType);
%     fprintf(fid, '\t\tRotation\t= %s\n', DatasetHeader.CoordinateSpace.Rotation);
fprintf(fid, '\tCoordinateSpace End\n');

fprintf(fid, '\tRasterInfo Begin\n');
switch typeCodage
    case {'double'; 'float64'}
        fprintf(fid, '\t\tCellType\t= IEEE8ByteReal\n');
    case {'float'; 'float32'}
        fprintf(fid, '\t\tCellType\t= IEEE4ByteReal\n');
    case {'uint8'; 'Unsigned8BitInteger'}
        fprintf(fid, '\t\tCellType\t= Unsigned8BitInteger\n');
end
fprintf(fid, '\t\tNullCellValue\t= %s\n', num2str(DatasetHeader.RasterInfo.NullCellValue));
fprintf(fid, '\t\tNrOfLines\t= %s\n', num2str(DatasetHeader.RasterInfo.NrOfLines));
fprintf(fid, '\t\tNrOfCellsPerLine\t= %s\n', num2str(DatasetHeader.RasterInfo.NrOfCellsPerLine));

fprintf(fid, '\t\tRegistrationCoord Begin\n');
fprintf(fid, '\t\t\tEastings\t= %s\n', num2str(DatasetHeader.RasterInfo.RegistrationCoord.Eastings));
fprintf(fid, '\t\t\tNorthings\t= %s\n', num2str(DatasetHeader.RasterInfo.RegistrationCoord.Northings));
fprintf(fid, '\t\tRegistrationCoord End\n');

fprintf(fid, '\t\tCellInfo Begin\n');
fprintf(fid, '\t\t\tXdimension\t= %s\n', num2str(DatasetHeader.RasterInfo.CellInfo.Xdimension));
fprintf(fid, '\t\t\tYdimension\t= %s\n', num2str(DatasetHeader.RasterInfo.CellInfo.Ydimension));
fprintf(fid, '\t\tCellInfo End\n');

fprintf(fid, '\t\tNrOfBands\t= %s\n', num2str(DatasetHeader.RasterInfo.NrOfBands));

for i=1:length(DatasetHeader.RasterInfo.BandId.Value)
    fprintf(fid, '\t\tBandId Begin\n');
    fprintf(fid, '\t\t\tValue\t= "%s"\n', DatasetHeader.RasterInfo.BandId.Value{i});
    fprintf(fid, '\t\tBandId End\n');
end

fprintf(fid, '\t\tRegionInfo Begin\n');
fprintf(fid, '\t\t\tType\t= %s\n', DatasetHeader.RasterInfo.RegionInfo.Type);
fprintf(fid, '\t\t\tRegionName\t= "%s"\n', DatasetHeader.RasterInfo.RegionInfo.RegionName);
%     fprintf(fid, '\t\t\tSourceDataset\t= "%s"\n', DatasetHeader.RasterInfo.RegionInfo.SourceDataset);

%     fprintf(fid, '\t\t\tRGBcolour Begin\n');
%     fprintf(fid, '\t\t\t\tRed\t\t= %d\n', DatasetHeader.RasterInfo.RegionInfo.Red);
%     fprintf(fid, '\t\t\t\tGreen\t= %d\n', DatasetHeader.RasterInfo.RegionInfo.Green);
%     fprintf(fid, '\t\t\t\tBlue\t= %d\n', DatasetHeader.RasterInfo.RegionInfo.Blue);
%     fprintf(fid, '\t\t\tRGBcolour End\n');

fprintf(fid, '\t\t\tSubRegion	= {\n');
for i=1:size(DatasetHeader.RasterInfo.RegionInfo.SubRegion, 1)
    fprintf(fid, '\t\t\t\t%s\n', num2str(DatasetHeader.RasterInfo.RegionInfo.SubRegion(i,:)));
end
fprintf(fid, '\t\t\t}\n');
fprintf(fid, '\t\tRegionInfo End\n');

fprintf(fid, '\tRasterInfo End\n');

fprintf(fid, 'DatasetHeader End\n');

% --------------------
% Fermeture du fichier

fclose(fid);
status = 1;
