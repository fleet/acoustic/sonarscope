% Suppression de lignes vides dans un fichier Ascii.
%
% Syntax
%   eraseEmptyLinesInTxtFile(nomFicIn)
%
% Input Arguments
%   nomFicIn : nom du fichier d'entr�e
%
% Examples
%    nomFic = getNomFicDatabase('BScorr_302.txt');
%    eraseEmptyLinesInTxtFile(nomFic);
%
% See also cl_simrad_all
% Authors : GLU (ATL)
% -------------------------------------------------------------------------

function eraseEmptyLinesInTxtFile(nomFicIn)

[~, ficNomSeul, ext] = fileparts(nomFicIn);

fidIn = fopen(nomFicIn, 'r');
if fidIn == -1
    return
end

nomFicOut = fullfile(my_tempdir, [ficNomSeul, '_',  num2str(floor(rand(1)*1e6)), ext]);
fidOut = fopen(nomFicOut, 'wt');
while ~feof(fidIn)
    line = fgetl(fidIn);
    % On �vite d'�crire les lignes vides (ne contenant que les CR + LF)
    if ~isempty(line)
        fprintf(fidOut, '%s\n', line);
    end
end
fclose(fidIn);
fclose(fidOut);

% D�placement du fichier cible en lieu et place du fichier d'origine.
[status, message, messageid] = movefile(nomFicOut, nomFicIn); %#ok<ASGLU>

if exist(nomFicOut, 'file')
    delete(nomFicOut);
end
