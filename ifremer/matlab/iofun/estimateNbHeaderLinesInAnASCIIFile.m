%   TideFile = getNomFicDatabase('ExKongsberg.tide')
%   TideFile = getNomFicDatabase('ExNiwa.tide')
%   TideFile = getNomFicDatabase('ExLibex.tid')
%   TideFile = getNomFicDatabase('ExCaraibes.tide')
%   TideFile = getNomFicDatabase('ExCidco.tide')
%   [flag, nbHeaderLines] = estimateNbHeaderLinesInAnASCIIFile(TideFile, 100)


function [flag, nbHeaderLines, nbWordsExpected] = estimateNbHeaderLinesInAnASCIIFile(nomFic, nbLignesToRead)

flag = 0;
nbHeaderLines   = [];
nbWordsExpected = [];

N = countLines(nomFic);
if isnan(N)
    messageErreurFichier(nomFic, 'ReadFailure');
    return
end

nbLignesToRead = min(N, nbLignesToRead);
nbChar  = zeros(1,nbLignesToRead);
nbWords = zeros(1,nbLignesToRead);

nbHeaderLines1 = 0;
fid = fopen(nomFic, 'r');
for k=1:nbLignesToRead
    tline = fgetl(fid);
    if (length(tline) > 1) && strcmp(tline(1), '#')
        nbHeaderLines1 = nbHeaderLines1 + 1;
    end
end
fclose(fid);

tab = sprintf('\t');
fid = fopen(nomFic, 'r');
for k=1:nbLignesToRead
    tline = fgetl(fid);
    nbChar(k) = length(tline);
    tline = strtrim(tline);
    mots = strsplit(tline, {' '; ';'; tab});
    nbWords(k) = length(mots);
%     fprintf('%d : %d : %s\n',k, nbWords(k), tline);
end
fclose(fid);
nbWordsExpected = median(nbWords);
nbHeaderLines2 = find(nbWords ~= nbWordsExpected, 1, 'last');
if isempty(nbHeaderLines2)
    nbHeaderLines2 = 0;
end

nbHeaderLines = max(nbHeaderLines1, nbHeaderLines2);

flag = 1;
