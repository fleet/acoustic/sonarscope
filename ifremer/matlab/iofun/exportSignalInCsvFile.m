function flag = exportSignalInCsvFile(nomFicCsv, Datetime, varargin)

[varargin, VarNames]       = getPropertyValue(varargin, 'VarNames',       []);
[varargin, OriginFileName] = getPropertyValue(varargin, 'OriginFileName', []);

Value = varargin;

if ~iscell(Value)
    Value = {Value};
end

% flag = 0;

%% Comma or not comma ?

sep = getCSVseparator;

%% Ecriture du fichier

%{
tic
fid = fopen(nomFicCsv, 'w+t');
if fid == -1
     messageErreurFichier(nomFicCsv, 'WriteFailure');
    return
end

if isempty(VarNames)
    for k=1:length(VarNames)
        VarNames{k} = sprintf('Value%d', k);
    end
else
    if ~iscell(VarNames)
        VarNames = {VarNames};
    end
end

str = '';
for k=1:length(VarNames)
    str = sprintf('%s%s%s', str, sep, VarNames{k});
end
if isempty(OriginFileName)
    fprintf(fid, 'Time%sStrTime%s\n', sep, str);
else
    fprintf(fid, 'FileName%sTime%sstrTime%s\n', sep, sep, str);
end

TimeDepth = posixtime(Datetime);

% Datetime.Format = 'yyyy-MM-dd''T''HH:mm:ss.SSS''Z'''; % TODO : tester

N = length(TimeDepth);
for k2=1:N
    str = '';
    for k1=1:length(Value)
        str = sprintf('%s%s%s', str, sep, num2strPrecis(Value{k1}(k2)));
    end
    if isempty(OriginFileName)
        fprintf(fid, '%13.3f%s %s%s\n', TimeDepth(k2), sep, char(Datetime(k2)), str);
    else
        fprintf(fid, '%s%s%13.3f%s %s%s\n', OriginFileName, sep, TimeDepth(k2), sep, char(Datetime(k2)), str);
    end
end
fclose(fid);
toc
%}


% tic

% filename = my_tempname('.csv');
TimeDepth = posixtime(Datetime);
N = length(TimeDepth);
% Datetime.Format = 'yyyy-MM-dd''T''HH:mm:ss.SSS''Z'''; % C'est la norme ISO
Datetime.Format = 'yyyy-MM-dd HH:mm:ss.SSS'; % Préféré par Marc Roche

if isempty(OriginFileName)
    tableVarTypes = {'double'; 'datetime'};
    tableVarNames = {'Time'; 'strTime'};
else
    tableVarTypes = {'char'; 'double'; 'datetime'};
    tableVarNames = {'FileName'; 'Time'; 'strTime'};
end

for k1=1:length(Value)
    tableVarNames{end+1} = VarNames{k1}; %#ok<AGROW>
    tableVarTypes{end+1} = class(Value{k1}); %#ok<AGROW>
end
sz = [N length(tableVarNames)];
T = table('Size', sz, 'VariableTypes', tableVarTypes, 'VariableNames', tableVarNames);

if ~isempty(OriginFileName)
    T.FileName = repmat(OriginFileName, N, 1);
end

Time = TimeDepth; %#ok<NASGU>
T.Time    = TimeDepth(:);
T.strTime = Datetime(:);
for k1=1:length(Value)
    T.(VarNames{k1}) = Value{k1}(:);
end
head(T)
writetable(T, nomFicCsv, 'Delimiter', sep)
% toc

flag = 1;
