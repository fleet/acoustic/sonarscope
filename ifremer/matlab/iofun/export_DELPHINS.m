function flag = export_DELPHINS(nomFicOut, T, Latitude, Longitude, Immersion, Heading, Pitch, Roll, Heave)

%{
UTC Date	Time			Latitude			Longitude			Depth		Heading		Pitch		Roll		Heave
10/10/2013 07:16:27.403900	48�23.520534715409	-004�29.940001620336	0.35500	-0.00000	0.00000	0.00000	0.00000
10/10/2013 07:16:27.903900	48�23.520534715409	-004�29.940001620336	1.03822	-0.00000	0.00000	0.00000	0.00000
10/10/2013 07:16:28.403900	48�23.742576118866	-004�29.952151402019	1.03822	-0.00000	0.00000	0.00000	0.00000
%}

T.Format = 'd/MM/y HH:mm:ss.SSSS00';

%% Ouverture du fichier

fid = fopen(nomFicOut, 'w+');
if fid == -1
     messageErreurFichier(nomFicOut, 'WriteFailure');
     return
end

%% Write header

fprintf(fid, 'UTC Date	Time			Latitude			Longitude			Depth		Heading		Pitch		Roll		Heave\n');

%% lecture des enregistrements de navigation

nbRec = length(T);
[~, filename, Ext] = fileparts(nomFicOut);
str1 = sprintf('Ecriture du fichier %s', [filename Ext]);
str2 = sprintf('Export %s', [filename Ext]);
hw = create_waitbar(Lang(str1,str2), 'N', nbRec);
for k=1:nbRec
    my_waitbar(k, nbRec, hw)
    
    str = sprintf('%s\t%s\t%s\t%f\t%f\t%f\t%f\t%f', T(k), lat2strDELPHINS(Latitude(k)), lon2strDELPHINS(Longitude(k)), ...
        Immersion(k), Heading(k), Pitch(k), Roll(k), Heave(k));
    fprintf(fid, '%s\n', str);
end
my_close(hw, 'MsgEnd')


%% Fermeture du fichier

fclose(fid);

flag = 1;
