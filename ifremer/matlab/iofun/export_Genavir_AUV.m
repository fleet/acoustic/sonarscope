function flag = export_Genavir_AUV(nomFicOut, T, Latitude, Longitude, Immersion)

%{
UTC	Date	Latitude	Longitude	Immersion
22/08/2014	06:46:07.555	43.13923649788	5.45172382991	-2.9
22/08/2014	06:46:13.555	43.13928316434	5.45164098740	-4.0
22/08/2014	06:46:19.555	43.13926650683	5.45153900782	-5.0
22/08/2014	06:46:31.556	43.13913717270	5.45149199168	-15.4
%}

T.Format = 'd/MM/y HH:mm:ss.SSS';

%% Ouverture du fichier

fid = fopen(nomFicOut, 'w+');
if fid == -1
     messageErreurFichier(nomFicOut, 'WriteFailure');
     return
end

%% Write header

fprintf(fid, 'UTC	Date	Latitude	Longitude	Immersion\n');

%% lecture des enregistrements de navigation

nbRec = length(T);
[~, filename, Ext] = fileparts(nomFicOut);
str1 = sprintf('Ecriture du fichier %s', [filename Ext]);
str2 = sprintf('Export %s', [filename Ext]);
hw = create_waitbar(Lang(str1,str2), 'N', nbRec);
for k=1:nbRec
    my_waitbar(k, nbRec, hw)
    
    str = sprintf('%s\t%15.11f\t%16.11f\t%f', T(k), Latitude(k), Longitude(k), Immersion(k));
    fprintf(fid, '%s\n', str);
end
my_close(hw, 'MsgEnd')

%% Fermeture du fichier

fclose(fid);

flag = 1;
