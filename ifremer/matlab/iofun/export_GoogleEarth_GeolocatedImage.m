function export_GoogleEarth_GeolocatedImage(nomFicKmz, nomFicPng, InfoImage)

[nomDir, ~, ExtKMZ] = fileparts(nomFicKmz);

nbImages = length(InfoImage);
flagImages = false(1, nbImages);
MaxValue   = -Inf(1,nbImages);
for k=1:nbImages
    if ~isempty(InfoImage{k})
        flagImages(k) = true;
        if isfield(InfoImage{k}, 'MaxValue')
            MaxValue(k) = InfoImage{k}.MaxValue;
        end
    end
end
InfoImage = InfoImage(flagImages);
nomFicPng = nomFicPng(flagImages);
MaxValue  = MaxValue(flagImages);
[~, indSort] = sort(MaxValue, 'descend');
% MaxValue  = MaxValue(indSort);
nomFicPng = nomFicPng(indSort);
InfoImage = InfoImage(indSort);

%% Lecture du fichier d'exemple

nomFicKmlIn = getNomFicDatabase('ExampleGeolocatedImage.kml');
try
    [tree, treeName] = xml_read(nomFicKmlIn);
    tree.Document.Style = rmfield(tree.Document.Style, 'IconStyle');
    
    % TODO : tentative de d�finir des couleurs : c'est un �chec
    nbColors = 2; % TODO : 256 remplac� par 2 pour debug car c'est plus simple � analyser le .kml
    Map = jet(nbColors);
    Map = uint8(Map * (nbColors-1));
    for k=1:size(Map, 1)
        r = Map(k,1);
        g = Map(k,2);
        b = Map(k,3);
        
        tree.Document.StyleMap(k).ATTRIBUTE.id     = sprintf('Color%03d', k-1);
        tree.Document.StyleMap(k).Pair(1).Key      = 'normal';
        tree.Document.StyleMap(k).Pair(1).styleUrl = sprintf('#Color%03d', k-1);
        tree.Document.StyleMap(k).Pair(2).Key      = 'highlight';
        tree.Document.StyleMap(k).Pair(2).styleUrl = sprintf('#Color%03d', k-1);
        
        tree.Document.Style(k).ATTRIBUTE.id    = sprintf('Color%03d', k-1);
        tree.Document.Style(k).IconStyle.color = sprintf('%02X%02X%02X', r, g, b);
    end
catch ME
    my_warndlg(ME.message, 1);
end

%% Pr�paration du fichier .kml temporaire

if strcmp(ExtKMZ, '.kmz')
    DirTemp = my_tempname;
    flag = mkdir(DirTemp);
    if ~flag
        return
    end
else
    DirTemp = nomDir;
end

DirImages = fullfile(DirTemp, 'files');
flag = mkdir(DirImages);
if ~flag
    return
end
% copyfile(nomFicIcone, DirImages)
nbImages = length(nomFicPng);
str1 = sprintf('Copie des images sur "%s"', DirImages);
str2 = sprintf('Copy images in "%s"', DirImages);
hw = create_waitbar(Lang(str1,str2), 'N', nbImages);
for k=1:nbImages
    my_waitbar(k, nbImages, hw)
    try
        copyfile(nomFicPng{k}, DirImages)
    catch ME
        ME %#ok<NOPRT>
    end
end
my_close(hw)

[~, nomFicSimple] = fileparts(nomFicKmz);
nomFicKmlOut = fullfile(DirTemp, [nomFicSimple, '.kml']);

%% Set the parameters

tree.Document.name = nomFicSimple;
tree.Document.Folder.name = nomFicSimple;

for k=1:nbImages
    [~, nomFicSimple, Ext] = fileparts(nomFicPng{k});
    
    if isfield(InfoImage{k}, 'Tag') && ~isempty(InfoImage{k}.Tag)
        if isfield(InfoImage{k}, 'Datetime')
            TimeImage = InfoImage{k}.Datetime;
            TimeImage.Format = 'yyyyMMdd_hhmmss_SSS';
            name = char(TimeImage);
        else
            PinUpLetter = InfoImage{k}.Tag(1);
            name = sprintf('%s%d', PinUpLetter, k);
        end
    else
        name = sprintf('P%d', k);
    end
    
    tree.Document.Folder.Placemark(k).name                      = name;
    tree.Document.Folder.Placemark(k).description.CDATA_SECTION = sprintf('<img src=''files/%s''>', [nomFicSimple, Ext]);
    tree.Document.Folder.Placemark(k).Point.coordinates         = sprintf('%s, %s, 200', num2strPrecis(InfoImage{k}.Longitude), num2strPrecis(InfoImage{k}.Latitude));
    tree.Document.Folder.Placemark(k).Point.altitudeMode        = 'clampToSeaFloor'; % <gx:altitudeMode>clampToSeaFloor</gx:altitudeMode>
    
    % TODO : voir message TODO pr�c�dent
    c = uint8(rand(1) * (nbColors-1));
    tree.Document.Folder.Placemark(k).styleUrl = sprintf('#Color%03d', c-1);
end

%% Ecriture du fichier

DPref.ItemName   = 'item';
DPref.StructItem = false;
DPref.CellItem   = false;

treeOut.ATTRIBUTE         = tree.ATTRIBUTE;
treeOut.Document.name     = tree.Document.name;
treeOut.Document.open     = tree.Document.open;
treeOut.Document.Style    = tree.Document.Style;
treeOut.Document.StyleMap = tree.Document.StyleMap;
treeOut.Document.Folder   = tree.Document.Folder;

xml_write(nomFicKmlOut, treeOut, treeName, DPref)
% winopen(nomFicKmlOut) % Temporaire pour debug

%% zip .kml to .kmz

if strcmp(ExtKMZ, '.kmz')
    zipKml2kmz(nomFicKmz, DirTemp);
end
