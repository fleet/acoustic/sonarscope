function export_ImageTif32bits(I, nomFic, varargin)

[varargin, MinVal] = getPropertyValue(varargin, 'MinVal', []);
[varargin, MaxVal] = getPropertyValue(varargin, 'MaxVal', []);
[varargin, Mute]   = getPropertyValue(varargin, 'Mute',   0); %#ok<ASGLU>

% W = warning;
% warning('off', 'all');

t = Tiff(nomFic, 'w');

TiffTags.ImageLength     = size(I,1);
TiffTags.ImageWidth      = size(I,2);
TiffTags.Photometric     = Tiff.Photometric.MinIsBlack;
% TiffTags.ColorMap      = Colormap;
TiffTags.BitsPerSample   = 32;
TiffTags.SampleFormat    = 3; % Tiff.SampleFormat.IEEEFP : 3
TiffTags.SamplesPerPixel = 1;

% CCITTFax3 CCITTFax4 LZW JPEG PackBits SGILog SGILog24 Defate
TiffTags.Compression         = Tiff.Compression.LZW;

TiffTags.PlanarConfiguration = Tiff.PlanarConfiguration.Chunky;
% TiffTags.Photometric         = Tiff.Photometric.MinIsBlack;
TiffTags.Software            = 'SonarScope';

if isempty(MinVal)
    TiffTags.SMinSampleValue = double(min(min(I)));
else
    TiffTags.SMinSampleValue = double(MinVal);
end

if isempty(MaxVal)
    TiffTags.SMaxSampleValue = double(max(max(I)));
else
    TiffTags.SMaxSampleValue = double(MaxVal);
end

TiffTags.Artist       = char(java.lang.System.getProperty('user.name'));
TiffTags.HostComputer = char(java.lang.System.getProperty('os.name'));

% Voir https://fr.mathworks.com/matlabcentral/answers/7184-how-can-i-write-32-bit-floating-point-tifs-with-nans

% WhitePoint

% Organisation du fichier : Tiles ou Strips ?
if (size(I,1) >= 160) && (size(I,2) >= 160)
    maxTileWidth  = size(I,2)/10;
    maxTileLength = size(I,1)/10;
    tileWidth  = fix(maxTileWidth/16)*16;
    tileLength = fix(maxTileLength/16)*16;
    
    if tileWidth ~= maxTileWidth
        % la taille des tuiles doit �tre forc�ment d'un mutliple de 16
        tileWidth = tileWidth + 16;
    end
    if tileLength ~= maxTileLength
        % la taille des tuiles doit �tre forc�ment d'un mutliple de 16
        tileLength = tileLength + 16;
    end
    
    TiffTags.TileWidth  = min(tileWidth, 2048); % 2048 Max pour cette version de libTIFF
    TiffTags.TileLength = min(tileLength, 2048);
    if ~Mute
        fprintf('\n -> %s : %d x %d\n', Lang('Tuiles', 'Tiles'), tileWidth, tileWidth);
    end
else
    TiffTags.RowsPerStrip = 1;
    if ~Mute
        fprintf('\n -> %s\n', Lang('Pas besoin de tuiles', 'No need for Tiles'));
    end
end

% Ecriture
try
    t.setTag(TiffTags);
catch ME
    ME
end
t.write(single(I));
t.close();

% warning(W)
