% Ouverture d'un fichier VRML par le navigateur Externe.
%
% Syntax
%   flag = export_VRML_Open(nomFic)
%
% Input Arguments
%   nomFic        : Nom du fichier
%
% Output Arguments
%   flag : 1 si ecriture reussie, 0 sinon
%
% Examples
%
% See also cl_image Authors
% Authors : GLU
%--------------------------------------------------------------------------

function flag = export_VRML_OpenFile(filename)

str1 = 'Export VRML r�ussi, voulez-vous ouvrir le fichier dans un visualisateur externe ?';
str2 = 'VRML export successful : open the file in an external viewer ?';
[rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
if ~flag
    return
end
if rep ==1
    try
        url = filename;
        cmd = sprintf('"C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe" -remote "openurl(%s,new-tab)"',url);
        dos(cmd);
    catch exception %#ok<NASGU>
        try cmd = sprintf('"C:\\Program Files\\Mozilla Firefox\\firefox.exe" -remote "openurl(%s,new-tab)"',url);
            dos(cmd)
        catch exception
            str1 = 'Vous devriez t�l�charge un des logiciels suivants pour visionner votre fichier VRML : <a href="http://www.instantreality.org/downloads/">InstantPlayer</a> ou <a href="http://www.cortona3d.com/Products/Viewer/Cortona-3D-Viewer.aspx">Cortona3D</a>';
            str2 = 'You should download one of these freeware to see your VRML file : <a href="http://www.instantreality.org/downloads/">InstantPlayer</a> or <a href="http://www.cortona3d.com/Products/Viewer/Cortona-3D-Viewer.aspx">Cortona3D</a>';
            disp(Lang(str1,str2));
            str1 = 'Vous devriez t�l�charge un des logiciels suivants pour visionner votre fichier VRML : InstantPlayer ou Cortona-3D-Viewer';
            str2 = 'You should download one of these freeware to see your VRML file : InstantPlayer or Cortona-3D-Viewer';
            helpdlg(Lang( str1,str2), 'Export VRML');
            rethrow(exception);
        end
    end
end
