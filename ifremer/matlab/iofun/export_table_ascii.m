% Exportation de signaux dans un fichier ASCII
%
% Syntax
%   flag = export_table_ascii(nomsFicSignaux, listeSignaux, Data, Separator, TimeFormat)
%
% Input Arguments
%   nomsFicSignaux : Nom du fichier ASCII
%   listeSignaux   : Liste des champs de la structure
%   Data           : Structure contenant les signaux
%   Separator      : S�parateur
%   TimeFormat     : Type de codage de la date si l'un des signaux contient une date
%                    1= JJ/MM/AAAA, 2=YYYY/MM/DD
%
% Output Arguments
%   flag : 1=OK, 0=KO
%
% Examples
%   nomsFic = my_tempname('.csv')
%   Data.toto = 1:20;
%   Data.titi = rand(1,20);
%   Data.monTemps = cl_time('timeMat', linspace(now, now+1, 20))
%   Data.maLatitude = linspace(45, 48, 20);
%   Data.longitudeMachin = linspace(-1, 1, 20);
%   flag = export_table_ascii(nomsFic, {'monTemps'; 'toto'; 'titi'; 'maLatitude'; 'longitudeMachin'}, Data, ';', 1)
%   winopen(nomsFic)
%
% Remarks : Le temps doit �tre transmis sous forme d'objet de la classe cl_time
%           Tout nom de signal contenant la chaine de caract�res "latitude"
%           sera format�e en N/S DD MM.xxxx
%           Tout nom de signal contenant la chaine de caract�res "longitude"
%           sera format�e en E/W DDD MM.xxxx
%
% See also Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function flag = export_table_ascii(nomsFicSignaux, listeSignaux, Data, Separator, TimeFormat, varargin)

[varargin, Courbes] = getPropertyValue(varargin, 'Courbes', []); %#ok<ASGLU>

flag = 0;

if isempty(Data)
    str = sprintf('%s not created because data is empty.', nomsFicSignaux);
    my_warndlg(str, 0);
    return
end


[fid, message] = fopen(nomsFicSignaux, 'w+');
if fid == -1
    my_warndlg(message, 1);
    return
end

%% Ecritude de l'ent�te

str = '';
for k=1:length(listeSignaux)
    if k==1
        Sep = '';
    else
        Sep = Separator;
    end
    
    x = Data.(listeSignaux{k});
    if isa(x, 'cl_time')
        str = sprintf('%s%s%s (Date)%s%s (Hour)', str, Sep, listeSignaux{k}, Separator, listeSignaux{k});
    else
        str = sprintf('%s%s%s', str, Sep, listeSignaux{k});
    end
end

for k=1:length(Courbes)
    if (k+length(listeSignaux)) == 1
        Sep = '';
    else
        Sep = Separator;
    end
    str = sprintf('%s%s%s', str, Sep, Courbes(k).Nom);
end
fprintf(fid, '%s\n', str);

%% Ecriture des signaux

nl = length(Data.(listeSignaux{1})) * length(listeSignaux);
hw = create_waitbar('ASCII table exportation', 'N', nl);
for k1=1:length(Data.(listeSignaux{1}))
    str = '';
    for k2=1:length(listeSignaux)
        il = k2 + (k1-1)* length(listeSignaux);
        my_waitbar(il, nl, hw);
        if k2==1
            Sep = '';
        else
            Sep = Separator;
        end
        
        x = Data.(listeSignaux{k2})(k1);
        if isa(x, 'cl_time')
            str = sprintf('%s%s%s%s%s', str, Sep, date2str(x, 'Format', TimeFormat), Separator, heure2str(x));
            %         elseif ~isempty(strfind(lower(listeSignaux{k2}), 'latitude'))
            %             str = sprintf('%s%s%s', str, Sep, lat2str(x));
            %         elseif ~isempty(strfind(lower(listeSignaux{k2}), 'longitude'))
            %             str = sprintf('%s%s%s', str, Sep, lon2str(x));
        else
            str = sprintf('%s%s%f', str, Sep, x);
        end
    end
    
    for k2=1:length(Courbes)
        if (k2+length(listeSignaux)) == 1
            Sep = '';
        else
            Sep = Separator;
        end
        str = sprintf('%s%s%f', str, Sep, Courbes(k2).Values(k1));
    end
    
    fprintf(fid, '%s\n', str);
end
my_close(hw, 'MsgEnd');

fclose(fid);
flag = 1;

