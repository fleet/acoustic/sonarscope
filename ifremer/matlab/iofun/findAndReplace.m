function findAndReplace(nomFic, string1, string2, string3, string4, suffix)

for k=1:length(nomFic)
    fid1 = fopen(nomFic{k});
    if fid1 == -1
        continue
    end
    
    [nomDir, nomFicSeul, ext] = fileparts(nomFic{k});
    nomFicTemp = fullfile(nomDir, [nomFicSeul '_temp' ext]);
    fid2 = fopen(nomFicTemp, 'w+t');
    if fid2 == -1
        messageErreurFichier(nomFicTemp, 'WriteFailure');
        fclose(fid1);
        return
    end
    
    while 1
        tline = fgetl(fid1);
        if ~ischar(tline)
            break
        end
        tline = strrep(tline, string1, string2);
        if ~isempty(string3)
            tline = strrep(tline, string3, string4);
        end
        fprintf(fid2, '%s\n', tline);
    end
    fclose(fid1);
    fclose(fid2);
    
    if isempty(suffix)
        delete(nomFic{k})
        my_rename(nomFicTemp, nomFic{k})
    else
        nomFicOut = fullfile(nomDir, [nomFicSeul suffix ext]);
        my_rename(nomFicTemp, nomFicOut)
    end
end
