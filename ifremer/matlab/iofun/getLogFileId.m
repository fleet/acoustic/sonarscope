function X = getLogFileId

global IfrTbxTempDir %#ok<GVMIS>
global logFileId     %#ok<GVMIS> 

if isempty(logFileId)
    logFileName = my_tempname('.txt', 'Prefixe', 'SonarScopeLogFile', 'DirName', IfrTbxTempDir);
    logFileId = log4m.getLogger(logFileName);
    if isdeployed
        logFileId.setCommandWindowLevel(logFileId.INFO);
        logFileId.setLogLevel(logFileId.INFO);
    else
        logFileId.setCommandWindowLevel(logFileId.TRACE);
        logFileId.setLogLevel(logFileId.TRACE);
    end
end
X = logFileId;

%{
                   ALL: 0
                 TRACE: 1
                 DEBUG: 2
                  INFO: 3
                  WARN: 4
                 ERROR: 5
                 FATAL: 6
                   OFF: 7
              fullpath: 'C:\Temp\SonarScopeLogFile_20201126_170311_905791.txt'
    commandWindowLevel: 1
              logLevel: 1
              %}
              