function [flag, tImages, nomCam, NomRacine, NavigationFileFormat] = getTimeSubmarineImages(nomFicNav, listeFicImages)

tImages   = datetime.empty();
nomCam    = [];
NomRacine = [];

N = length(listeFicImages);
[flag, NavigationFileFormat] = identNavigationFileFormat(nomFicNav);
if ~flag
    return
end
switch NavigationFileFormat
    case 'HROV'
        for k=N:-1:1
            [~, nomSimple] = fileparts(listeFicImages{k});
            str = ['20' nomSimple(1:15)];
            strTimeImages(k,:) = str;
        end
        tImages = datetime(strTimeImages, 'InputFormat', 'yyyyMMddHHmmssSSS');
        nomCam = 'SCAMPI'; % 'Unknown'
        NomRacine = 'Images';
        
    case 'OTUS'
        for k=N:-1:1
            [~, tImages(k,1)] = OTUS_getTimeFromFileName(listeFicImages{k});
        end
        nomCam = 'OTUS'; % 'Unknown'
        tImages.Format = 'yyyyMMddHHmmss';
        NomRacine = 'COR_';
        
    case 'SCAMPI'
        [~, nomSimple] = fileparts(listeFicImages{1});
        for k=1:N
            [flag, ~, MillisecondsInImageName] = SCAMPI_getTimeFromFileName(listeFicImages{k});
            if flag
                if MillisecondsInImageName
                    NomRacine = nomSimple(1:end-19);
                else
                    NomRacine = nomSimple(1:end-15);
%                     Capteur = 'SCAMPISansMilliseconds';
                end
                break
            end
        end

        for k=N:-1:1
            [~, tImages(k,1)] = SCAMPI_getTimeFromFileName(listeFicImages{k});
        end
        nomCam = 'SCAMPI'; % 'Unknown'
        tImages.Format = 'yyyyMMddHHmmss';

    otherwise
        [~, nomSimple] = fileparts(listeFicImages{1});
        mots = split(nomSimple, '_');
        indTime = find(~isnan(str2double(mots)));
        strTimeImages = mots{indTime};
        NomRacine = '';
        for k=1:(indTime-1)
            NomRacine = [NomRacine '_' mots{k}]; %#ok<AGROW>
        end
        NomRacine(1) = [];
        nomCam = mots{indTime+1};
        
        for k=1:N
            [~, nomSimple] = fileparts(listeFicImages{k});
            mots = split(nomSimple, '_');
            strTimeImages(k,:) = mots{indTime};
        end
        tImages = datetime(strTimeImages, 'InputFormat', 'yyyyMMddHHmmss');
end
