% -------------------------------------------------------------------------
% Fonction iconize pour int�grer un fichier type jpg en icone.
function outIcon = iconize(a)

% Find the size of the acquired image and determine how much data will need
% to be lost in order to form a 18x18 icon
[r,c,d] = size(a); %#ok
r_skip = ceil(r/18);
c_skip = ceil(c/18);

% Create the 18x18 icon (RGB data)
outIcon = a(1:r_skip:end,1:c_skip:end,:);
if ismatrix(outIcon)
    outIcon(:,:,2) = outIcon(:,:,1);
    outIcon(:,:,3) = outIcon(:,:,1);
end
