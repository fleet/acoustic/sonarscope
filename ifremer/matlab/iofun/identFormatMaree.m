function typeFormat = identFormatMaree(nomFic)

% TODO : revoir enti�rement cette fonction en sortant des formats comme cela
% est fait dans read_TideSimrad
% Trouver automatiquement le nombre d'ent�tes en lisant une centaine
% d'enregistrement et en regardant le nombre de mots et/ou de caract�res de
% chaque ligne.
% Eviter le strsplit car tr�s gourmand en CPU
% En fonction du nombre de mots rechercher automatiquement ce qui est quoi
% On devrait pouvoir faire une fonction de lecture unique du type en
% passant le Format en param�tre. On r�cup�rerait s�par�ment les jours,
% mois, ann�es, etc ...
% fid = fopen(nomFic, 'r');
% data = textscan(fid, '%s %s %f %*[^\n]', 'HeaderLines', nbHeaderLines);
% fclose(fid);

typeFormat = 'Unknown';

fid = fopen(nomFic, 'r');
if fid == -1
    return
end
line1 = fgetl(fid);
if ~ischar(line1)
    return
end
line2 = fgetl(fid);
if ~ischar(line2)
    return
end
fclose(fid);

if contains(line1, 'LIBEX file dump')
    typeFormat = 'LIBEX';
    return
end

if strcmp(line1, 'heure,heure dec.,ZMar�e,Datej,Latitude,Longitude')
    typeFormat = 'MickaelVasquez';
    return
end

if (length(line1) > 6) && strcmp(line1(1:6), '( Tide')
    mots2 = strsplit(line2);
    if length(mots2{1}) == 14
        typeFormat = 'Simrad1';
    else
        typeFormat = 'Simrad2';
    end
    return
end

line1 = rmblank(line1);
line2 = rmblank(line2);
mots1 = strsplit(line1);
mots2 = strsplit(line2);

if (length(mots1) == 1) && isequal(mots1{1}, '--------') ...
        && (length(mots2) == 3) && (length(mots2{1}) == 10) && (length(mots2{2}) == 8)
    typeFormat = 'Caris';
    return
end

if (length(mots1) == 2) && (length(mots1{1}) == 14)
    typeFormat = 'UnivGent';
    return
end

if (length(mots1) == 8) && (length(mots2) == 8)
    flag = 1;
    for i=1:8
        if isnan(str2double(mots1{i})) || isnan(str2double(mots2{i}))
            flag = 0;
            break
        end
    end
    if flag
        typeFormat = 'CaraibesHar';
        return
    end
end

if strcmp(line1, '(Tide)') && (length(mots2) == 2)
    typeFormat = 'ProvenanceInconnue'; % En fait c'est du SIMRAD2 mais qui n'est pas accessible vu comment c'est programm� dans lecFicMareeSimrad : TODO
    return
end

if (length(mots1) > 3) && strcmp(mots1{1}, '#') && strcmp(mots1{2}, 'Station') && strcmp(mots1{3}, ':')
    %{
# Station : Montr�al-Jet�e no.1 * (15520)
# Fuseau horaire : Fuseau horaire : UTC
# Observations de niveaux d'eaux - Donn�es pr�liminaires
# Date;heure;niveau
2011-05-19;02:00;2.391
    %}
    typeFormat = 'CIDCO';
    return
end

% mots3 = strsplit(mots2{1}, '/');
mots3 = strsplit(mots2{1}, mots2{1}(5));
x = str2double(mots3);
if x(1) > 31
    typeFormat = 'Niwa';
else
    typeFormat = 'Caraibes';
end
