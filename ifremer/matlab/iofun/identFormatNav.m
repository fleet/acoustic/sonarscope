% Identification du format de fichier.
%
% Syntax
%   [flag, format] = identFormatNav(nomFicNav) 
%
% Input Arguments
%   nomFicNav : Nom du fichier de navigation
%
% Output Arguments
%   flag    : indicateur de bon fonctionnement (0 : KO, 1 : OK).
%   format  : format de fichier de Nav.
%
% Examples 
%   nomFicNav       = 'F:\SonarScopeData\From CSIRO\Raw\EK60_12Khz-D20120730-T045457.raw.gps.csv';
%   [flag, format]  = identFormatNav(nomFicNav);
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------
function [flag, typeFormat] = identFormatNav(nomFic)

flag = 0;

typeFormat = 'Unknown';

[~,~,ext] = fileparts(nomFic);

fid = fopen(nomFic, 'r');
if fid == -1
    return
end
line1 = fgetl(fid);
if ~ischar(line1)
    return
end
line2 = fgetl(fid);
if ~ischar(line2)
    return
end
fclose(fid);

if strcmp(line1, 'heure,heure dec.,ZMar�e,Datej,Latitude,Longitude')
    typeFormat = 'MickaelVasquez';
    
elseif strcmp(line1, 'GPS_date,GPS_time,GPS_milliseconds,Latitude,Longitude')    
    typeFormat = 'ExRaw';
    
elseif strcmpi(ext, '.nvi')
    typeFormat = 'Nvi';

elseif strcmpi(ext, '.asc')
    typeFormat = 'Ascii';
    
elseif contains(nomFic, '_nav')
    % On suppose le format sous forme tabul�e.
    typeFormat = 'Segy';
else        
    typeFormat = 'SonarScope';
end

flag = 1;

