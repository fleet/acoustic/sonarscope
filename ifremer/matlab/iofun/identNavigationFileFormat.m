function [flag, Format] = identNavigationFileFormat(nomFic)

Format = [];

[~, ~, Ext] = fileparts(nomFic);
if strcmp(Ext, '.xml')
    [flag, XML] = XMLBinUtils.readGrpData(nomFic);
    if ~flag
        return
    end
    Format = XML.Title;
    return
end

Format = 'Other'; % TODO : compl�ter avec OTUS et autres
fid = fopen(nomFic, 'r');
if fid == -1
     messageErreurFichier(nomFic, 'ReadFailure');
     flag = 0;
     return
end
tline = fgetl(fid);
mots = strsplit(tline, {',', ';', '\t'});
if (length(mots) == 7) && (strcmp(strtrim(mots{2}), 'Locality') || strcmp(strtrim(mots{2}), 'Localit�') || strcmp(strtrim(mots{2}), 'Localité')) %% TODO : remplacer par test 'Local'
    Format = 'HROV';
    
elseif ~isempty(find(strcmp(mots, 'LAT_PAGURE'), 1))
    Format = 'PAGURE';
    
elseif strcmpi(mots{1}, 'UTC') && strcmpi(mots{2}, 'Date') && strcmpi(mots{3}, 'Latitude') && strcmpi(mots{4}, 'Longitude')
    Format = 'Genavir_AUV';
    
elseif strcmpi(mots{1}, 'Date') ...
        && (strcmpi(mots{2}, 'Heure') || strcmpi(mots{2}, 'Time')) ...
        && strcmpi(mots{3}, 'Latitude') ...
        && strcmpi(mots{4}, 'Longitude') ...
        && strcmpi(mots{5}, 'Immersion')
    Format = 'Genavir_AUV';
elseif (length(mots) == 3)  && strcmpi(mots{1}(5), '-') && strcmpi(mots{1}(8), '-')
    Format = 'VLIZ';
    
elseif (length(mots) >= 10) ...
        && strcmpi(mots{1}, 'UTC') ...
        && strcmpi(mots{2}, 'Date') ...
        && strcmpi(mots{3}, 'Time') ...
        && strcmpi(mots{4}, 'Latitude') ...
        && strcmpi(mots{5}, 'Longitude') ...
        && strcmpi(mots{6}, 'Depth') ...
        && strcmpi(mots{7}, 'Heading') ...
        && strcmpi(mots{8}, 'Pitch') ...
        && strcmpi(mots{9}, 'Roll') ...
        && strcmpi(mots{10}, 'Heave')
    Format = 'DelphINS';
    
elseif strcmpi(mots{1}, 'Header;') && strcmpi(mots{2}, 'Name;') % Header; Name; Nb Points; Summary; Red; Green; Blue
    Format = 'NoImport';
    
elseif (length(mots) == 4)
    try
        t = datetime(mots{1}, 'InputFormat', 'yyyy MM dd HH mm ss.SS');
        if t ~= NaT
            lon = str2double(mots{2});
            if ~isnan(lon)
                lat = str2double(mots{3});
                if ~isnan(lat)
                    x = str2double(mots{4});
                    if ~isnan(x)
                        Format = 'Dilip';
                    end
                end
            end
        end
    catch
    end
    
elseif (length(mots) == 9)
    try
        t = datetime(str2double(mots{1}), str2double(mots{2}), str2double(mots{3}), str2double(mots{4}), str2double(mots{5}), str2double(mots{6}));
        if t ~= NaT
            lon = str2double(mots{7});
            if ~isnan(lon)
                lat = str2double(mots{8});
                if ~isnan(lat)
                    x = str2double(mots{9});
                    if ~isnan(x)
                        Format = 'Dilip';
                    end
                end
            end
        end
    catch
    end
    
elseif (length(mots) == 2)
    mots = [strsplit(mots{1}, ' ') mots{2}];
    try
        t = datetime(str2double(mots{1}), str2double(mots{2}), str2double(mots{3}), str2double(mots{4}), str2double(mots{5}), str2double(mots{6}));
        if t ~= NaT
            lon = str2double(mots{7});
            if ~isnan(lon)
                lat = str2double(mots{8});
                if ~isnan(lat)
                    x = str2double(mots{9});
                    if ~isnan(x)
                        Format = 'Dilip';
                    end
                end
            end
        end
    catch
    end
end
fclose(fid);
flag = 1;
