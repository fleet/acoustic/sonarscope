function [flag, WC, map] = importWCDepthAcrossDist(FileNameXML, iPing)

flag = 0;
WC   = [];
map  = [];

[nomDir, nomFic] = fileparts(FileNameXML);

kRep = floor(iPing/100);
nomDir = fullfile(nomDir, nomFic);
if ~exist(nomDir, 'dir')
    str1 = sprintf('Le répertoire "%s" n''existe pas.', nomDir);
    str2 = sprintf('Directory "%s" does not exist.', nomDir);
    my_warndlg(Lang(str1,str2), 1);
    return
end

nomDirPngRaw = fullfile(nomDir, num2str(kRep, '%03d'));
if ~exist(nomDirPngRaw, 'dir')
    str1 = sprintf('Le répertoire "%s" n''existe pas.', nomDirPngRaw);
    str2 = sprintf('Directory "%s" does not exist.', nomDirPngRaw);
    my_warndlg(Lang(str1,str2), 1);
    return
end

nomFicPng = [num2str(iPing, '%05d') '.png'];
nomFicPng = fullfile(nomDirPngRaw, nomFicPng);
if ~exist(nomFicPng, 'file')
    str1 = sprintf('Le fichier "%s" n''existe pas.', nomFicPng);
    str2 = sprintf('File "%s" does not exist.', nomFicPng);
    my_warndlg(Lang(str1,str2), 1);
    return
end

[WC, map] = imread(nomFicPng);
% figure; image(X); colormap(map); colorbar

flag = 1;
