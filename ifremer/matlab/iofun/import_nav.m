% Import d'une navigation.
%
% Syntax
%   a = import_nav(nomFicNav) 
%
% Input Arguments
%   nomFicNav : Nom du fichier de navigation
%
% Output Arguments
%   a : Instance de cl_image 
%
% Examples 
%   nomFicNav = 'F:\SonarScopeData\From CSIRO\Raw\EK60_12Khz-D20120730-T045457.raw.gps.csv';
%   c = import_nav(a, nomFicNav)
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, Data] = import_nav(NavFile, varargin)

[nomDir, nomFic, ext] = fileparts(NavFile);
nomFicMat = fullfile(nomDir, [nomFic '_' ext(2:end) '.mat']);

if exist(nomFicMat, 'file') && checkFileCreation(nomFicMat, '20/11/2012') ...
    && checkFilePosterior(nomFicMat, NavFile)
    Data = loadmat(nomFicMat, 'nomVar', 'Data');
else
    [flag, typeFormat] = identFormatNav(NavFile);
    switch typeFormat
        case 'ExRaw'
            [flag, Data] = lecFicNavExRaw(NavFile);
            if ~flag
                return
            end
            
        case 'Nvi'
            % Modif JMA le 23/06/2014 : Suppression PFlag
%             [flag, Data.Nav.Lat, Data.Nav.Lon, Data.Time, Data.Nav.Heading, Data.Nav.Speed, Data.Nav.Immersion, ...
%                 Data.Nav.Altitude, Data.PFlag, Data.Nav.Type, Data.Nav.Quality] = lecFicNavCaraibes(NavFile);
            [flag, Data.Nav.Lat, Data.Nav.Lon, Data.Time, Data.Nav.Heading, Data.Nav.Speed, Data.Nav.Immersion, ...
                Data.Nav.Altitude, Data.Nav.Type, Data.Nav.Quality] = lecFicNavCaraibes(NavFile);
            if ~flag
                messageErreurFichier(NavFile, 'ReadFailure');
                return
            end
            
        case 'SonarScope'
            try
                [Data.Nav.Lat, Data.Nav.Lon, Data.Time] = lecFicNavSSC(NavFile);
                if ~flag
                    return
                end
            catch
                % 2�me Format : nomFichier indexTrace S ou N Longitude(ddec) W ou E Latitude(ddec)
                [flag, Data.Nav.Lon, Data.Nav.Lat] = readNavSegyFile(NavFile, 'delimiter', '\t'); %#ok<ASGLU>
            end

        case 'Segy'
            try 
                % 1er Format : indexTrace Longitude(ddec) Latitude(ddec)
                [flag, Data] = lecFicNavAscii(NavFile, 'delimiter', ' '); %#ok<ASGLU>
            catch
                % 2�me Format : nomFichier indexTrace S ou N Longitude(ddec) W ou E Latitude(ddec)
                [flag, Data.Nav.Lon, Data.Nav.Lat] = readNavSegyFile(NavFile, 'delimiter', '\t'); %#ok<ASGLU>
            end
            
        case 'Ascii'
            [flag, Data] = lecFicNavAscii(NavFile);
            if ~flag
                return
            end
                       
        otherwise
            my_warndlg('The Navigation file format is unknown.', 1);
            return
    end
    save(nomFicMat, 'nomFic', 'Data')
end

flag = 1;
