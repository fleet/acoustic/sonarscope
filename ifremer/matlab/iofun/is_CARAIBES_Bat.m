% Test si un fichier est au format du logiciel CARARIBES(R) Multifaisceaux (.mbb, .mbg)
%
% Syntax
%   [flag, SonarIdent] = is_CARAIBES_Bat(nomFic)
%
% Input Arguments
%   nomFic : Nom du fichier
%
% Output Arguments
%   flag       : 1 si oui, 0 si non
%   SonarIdent : Identification du sonar (voir cl_sounder)
%
% Examples
%   nomFic = getNomFicDatabase('EM12D_Ex.mbb')
%   [flag, SonarIdent] = is_CARAIBES_Bat(nomFic)
%
% See also is_CARAIBES_Str is_CARAIBES_Son is_CARAIBES_Mnt is_CARAIBES_Mos is_GMT_file Authors
% Authors : JMA
% VERSION    : $Id: entete.m,v 1.2 2003/11/12 12:46:11 augustin Exp $
%-------------------------------------------------------------------------------

function [flag, SonarIdent] = is_CARAIBES_Bat(nomFic)

flag       = 0;
SonarIdent = 0;

% ----------------------
% Test di fichier Netcdf

fid = fopen(nomFic);
if fid == -1
    return
end

[x, count] = fread(fid, 3, 'char');
fclose(fid);
if count ~= 3
    return
end

str = char(x)';
if ~strcmp(str, 'CDF')
    return
end

% ------------------------------------------
% Recuperation du skelette du fichier netcdf

a = cl_netcdf('fileName', nomFic);
sk = get(a, 'skeleton');

% ------------------------------------------------
% Test si c'est un fichier Caraibes de bathymetrie

numVar = find_numVar(a, 'mbCycle');
if ~isempty(numVar)
    numAtt = find_numAtt(a, 'mbSounder');
    if ~isempty(numAtt)
        mbSounder = sk.att(numAtt).value;
        SonarIdent = SonarIdent_caraibes2sonarScope(mbSounder);
    end
    flag = 1;
end
