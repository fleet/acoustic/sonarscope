% Test si un fichier est au format du logiciel CARARIBES(R) Modele Numerique de Terrain (.mnt)
%
% Syntax
%   flag = is_CARAIBES_Mnt(nomFic)
%
% Input Arguments
%   nomFic : Nom du fichier
%
% Output Arguments
%   flag : 1 si oui, 0 si non
%
% Examples
%   nomFic = getNomFicDatabase('EM300_Ex2.mnt')
%   flag = is_CARAIBES_Mnt(nomFic)
%
% See also cl_car_bat_data Authors
% See also is_CARAIBES_Str is_CARAIBES_Bat is_CARAIBES_Son is_CARAIBES_Mos is_GMT Authors
% Authors : JMA
% VERSION    : $Id: entete.m,v 1.2 2003/11/12 12:46:11 augustin Exp $
%-------------------------------------------------------------------------------

function flag = is_CARAIBES_Mnt(nomFic)

flag = 0;

% ----------------------
% Test di fichier Netcdf

fid = fopen(nomFic);
if fid == -1
    return
end

[x, count] = fread(fid, 3, 'char');
fclose(fid);
if count ~= 3
    return
end

str = char(x)';
if ~strcmp(str, 'CDF')
    return
end

% ------------------------------------------
% Recuperation du skelette du fichier netcdf

a = cl_netcdf('fileName', nomFic);

% -----------------------------------------------
% Test si c'est un fichier Caraibes Sonar Lat�ral

numVar1 = find_numVar(a, 'Type');
numVar2 = find_numVar(a, 'Format');
numVar3 = find_numVar(a, 'Mosaic_type');
if isempty(numVar1)|| isempty(numVar2) || isempty(numVar3)
    return
end

Type = get_value(a, 'Type');
if strcmp(Type, 'MIX') || strcmp(Type, 'DTM')
    flag = 1;
end
