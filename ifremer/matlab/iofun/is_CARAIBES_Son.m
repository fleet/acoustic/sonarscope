% Test si un fichier est au format du logiciel CARARIBES(R) Sonar Lateral (.imo)
%
% Syntax
%   [flag, SonarIdent] = is_CARAIBES_Son(nomFic)
%
% Input Arguments
%   nomFic : Nom du fichier
%
% Output Arguments
%   flag : 1 si oui, 0 si non
%
% Examples
%   nomFic = getNomFicDatabase('DF1000_ExStr.imo')
%   flag = is_CARAIBES_Son(nomFic)
%
% See also is_CARAIBES_Str is_CARAIBES_Bat is_CARAIBES_Mnt is_CARAIBES_Mos is_GMT Authors
% Authors : JMA
% VERSION    : $Id: entete.m,v 1.2 2003/11/12 12:46:11 augustin Exp $
%-------------------------------------------------------------------------------

function flag = is_CARAIBES_Son(nomFic)

flag = 0;

% ----------------------
% Test di fichier Netcdf

fid = fopen(nomFic);
if fid == -1
    return
end

[x, count] = fread(fid, 3, 'char');
fclose(fid);
if count ~= 3
    return
end

str = char(x)';
if ~strcmp(str, 'CDF')
    return
end

% ------------------------------------------
% Recuperation du skelette du fichier netcdf

a = cl_netcdf('fileName', nomFic);
% sk = get(a, 'skeleton');

% -----------------------------------------------
% Test si c'est un fichier Caraibes Sonar Lat�ral

numVar = find_numVar(a, 'grRawImage');
if ~isempty(numVar)
    flag = 1;
end
