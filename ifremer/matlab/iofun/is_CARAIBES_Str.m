% Test si un fichier est au format du logiciel CARARIBES(R) Mosaique Image (.imo, .mos)
%
% Syntax
%   [flag, SonarIdent] = is_CARAIBES_Str(nomFic)
%
% Input Arguments
%   nomFic : Nom du fichier
%
% Output Arguments
%   flag       : 1 si oui, 0 si non
%   SonarIdent : Identification du sonar (voir cl_sounder)
%
% Examples
%   nomFic = getNomFicDatabase('EM300_HYDRATECH.imo')
%   [flag, SonarIdent] = is_CARAIBES_Str(nomFic)
%
% See also is_CARAIBES_Bat is_CARAIBES_Mnt is_CARAIBES_Son is_CARAIBES_Mos is_GMT Authors
% Authors : JMA
% VERSION    : $Id: entete.m,v 1.2 2003/11/12 12:46:11 augustin Exp $
%-------------------------------------------------------------------------------

function [flag, SonarIdent] = is_CARAIBES_Str(nomFic)

flag       = 0;
SonarIdent = 0;

% ----------------------
% Test di fichier Netcdf

fid = fopen(nomFic);
if fid == -1
    return
end

[x, count] = fread(fid, 3, 'char');
fclose(fid);
if count ~= 3
    return
end

str = char(x)';
if ~strcmp(str, 'CDF')
    return
end

% ------------------------------------------
% Recuperation du skelette du fichier netcdf

a = cl_netcdf('fileName', nomFic);

% -----------------------------------------------
% Test si c'est un fichier Caraibes Sonar Lat�ral

numVar1 = find_numVar(a, 'Type');
numVar2 = find_numVar(a, 'Format');
numVar3 = find_numVar(a, 'Mosaic_type');
if isempty(numVar1) || isempty(numVar2) || isempty(numVar3)
    return
end

Type        = get_value(a, 'Type');
Mosaic_type = get_value(a, 'Mosaic_type');

if strcmp(Type, 'MOS') && strcmp(Mosaic_type, 'STR')
    numVar = find_numVar(a, 'PortMode');
    if ~isempty(numVar)
        PortMode = get_value(a, numVar);
        SonarIdent = modeCar2Sim(PortMode(1));
    end
    flag = 1;
end
