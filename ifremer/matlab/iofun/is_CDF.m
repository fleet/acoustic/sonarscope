% Test si un fichier est au format netCDF
%
% Syntax
%   flag = is_CDF(nomFic)
%
% Input Arguments
%   nomFic : Nom du fichier
%
% Output Arguments
%   flag : 1 si oui, 0 si non
%
% Examples
%   nomFic = getNomFicDatabase('Aslanian.grd');
%   flag = is_CDF(nomFic)
%
%   if flag
%     ncdump(nomFic)
%   end
%
% See also is_GMT is_CARAIBES_* Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function flag = is_CDF(nomFic)

flag = 0;

%% Test si fichier Netcdf

if iscell(nomFic)
    nomFic = nomFic{1};
end

fid = fopen(nomFic);
if fid == -1
    return
end

[x, count] = fread(fid, 3, 'char');
fclose(fid);
if count ~= 3
    return
end

str = char(x)';
if ~(strcmp(str, 'CDF') || strcmp(str, '�HD'))
    return
end

flag = 1;
