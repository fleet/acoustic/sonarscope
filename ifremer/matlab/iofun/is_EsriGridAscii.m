% Test si un fichier est au format ESRI Grid Ascii (.asc)
%
% Syntax
%   flag = is_EsriGridAscii(nomFic)
%
% Input Arguments
%   nomFic : Nom du fichier
%
% Output Arguments
%   flag : 1 si oui, 0 si non
%
% Examples
%   nomFic = getNomFicDatabase('*.asc')
%   flag = is_EsriGridAscii(nomFic)
%
%   if flag
%     [flag, a] = import_EsriGridAscii(cl_image, nomFic);
%     imagesc(a)
%   end
%
% See also cl_image/import_gmt is_CARAIBES_* Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function flag = is_EsriGridAscii(nomFic)

flag = 0;

%% Test di fichier Netcdf

fid = fopen(nomFic);
if fid == -1
    return
end

%% 

Line = fgetl(fid);
if ~ischar(Line)
    fclose(fid);
    return
end
mots = strsplit(Line);
if (length(mots) ~= 2) || ~strcmpi(mots{1}, 'ncols')
    fclose(fid);
    return
end

%%

Line = fgetl(fid);
if ~ischar(Line)
    fclose(fid);
    return
end
mots = strsplit(Line);
if (length(mots) ~= 2) || ~strcmpi(mots{1}, 'nrows')
    fclose(fid);
    return
end

%%

Line = fgetl(fid);
if ~ischar(Line)
    fclose(fid);
    return
end
mots = strsplit(Line);
if (length(mots) ~= 2) || ~(strcmpi(mots{1}, 'xllcenter') || strcmpi(mots{1}, 'xllcorner'))
    fclose(fid);
    return
end

%%

Line = fgetl(fid);
if ~ischar(Line)
    fclose(fid);
    return
end
mots = strsplit(Line);
if (length(mots) ~= 2) || ~(strcmpi(mots{1}, 'yllcenter') || strcmpi(mots{1}, 'yllcorner'))
    fclose(fid);
    return
end

%%

Line = fgetl(fid);
if ~ischar(Line)
    fclose(fid);
    return
end
mots = strsplit(Line);
if (length(mots) ~= 2) || (~strcmpi(mots{1}, 'cellsize') && ~strcmpi(mots{1}, 'dx'))
    fclose(fid);
    return
end

% Cas de la fourniture dans le fichier des DeltaX et DeltaY
% Si on a d�j� lu dx
if strcmpi(mots{1}, 'dx')
    Line = fgetl(fid);
    if ~ischar(Line)
        fclose(fid);
        return
    end
    mots = strsplit(Line);
    if (length(mots) ~= 2) || ~strcmpi(mots{1}, 'dy')
        fclose(fid);
        return
    end
end

%%

Line = fgetl(fid);
if ~ischar(Line)
    fclose(fid);
    return
end
mots = strsplit(Line);
if (length(mots) ~= 2) || ~strcmpi(mots{1}, 'nodata_value')
    str1 = sprintf('Le fichier "%s" semble �tre un fichier "EsriGridAscii" mais la ligne "nodata_value" est manquante.', nomFic);
    str2 = sprintf('"%s" seems to obe an "EsriGridAscii" file but the line "nodata_value" is missing.', nomFic);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'EsriGridAscii_nodata_value_isMissing');
%     fclose(fid);
%     return
end

%%

fclose(fid);

flag = 1;
