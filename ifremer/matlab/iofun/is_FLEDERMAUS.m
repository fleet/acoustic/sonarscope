% Test si un fichier est au format du logiciel FLEDERMAUS (.sd)
%
% Syntax
%   flag = is_FLEDERMAUS(nomFic)
%
% Input Arguments
%   nomFic : Nom du fichier
%
% Output Arguments
%   flag : 1 si oui, 0 si non
%
% Examples
%   nomFic = getNomFicDatabase('small.sd')
%   flag = is_FLEDERMAUS(nomFic)
%
%   if flag
%     [a, flag] = import_fledermaus(cl_image, nomFic);
%     imagesc(a)
%   end
%
% See also cl_image/import_gmt is_CARAIBES_* Authors
% Authors : JMA
% VERSION    : $Id: entete.m,v 1.2 2003/11/12 12:46:11 augustin Exp $
%-------------------------------------------------------------------------------

function flag = is_FLEDERMAUS(nomFic)

flag = 0;

% ----------------------
% Test di fichier Netcdf

fid = fopen(nomFic);
if fid == -1
    return
end

[x, count] = fread(fid, 6, 'char');
fclose(fid);
if count ~= 6
    return
end

str = char(x)';
if strcmp(str, '%% TDR')
    flag = 1;
end
