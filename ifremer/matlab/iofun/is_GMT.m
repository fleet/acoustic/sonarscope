% Test si un fichier est au format du logiciel GMT
%
% Syntax
%   flag = is_GMT(nomFic)
%
% Input Arguments
%   nomFic : Nom du fichier
%
% Output Arguments
%   flag : 1 si oui, 0 si non
%
% Examples
%   nomFic = getNomFicDatabase('Aslanian.grd')
%   nomFic = 'C:\TEMP\Test.grd'
%   flag = is_GMT(nomFic)
%
%   if flag
%     [a, flag] = import_gmt(cl_image, nomFic);
%     imagesc(a)
%   end
%
% See also cl_image/import_gmt is_CARAIBES_* Authors
% Authors : JMA
% VERSION    : $Id: entete.m,v 1.2 2003/11/12 12:46:11 augustin Exp $
%-------------------------------------------------------------------------------

function flag = is_GMT(nomFic)

flag = 0;

%% Test di fichier Netcdf

fid = fopen(nomFic);
if fid == -1
    return
end

[x, count] = fread(fid, 3, 'char');
fclose(fid);
if count ~= 3
    return
end

str = char(x)';
if ~strcmp(str, 'CDF')
    return
end

%% Récuperation du skelette du fichier netcdf

a = cl_netcdf('fileName', nomFic);

%% Test si c'est un fichier GMT

    numAtt1 = find_numAtt(a, 'title');
    numAtt2 = find_numAtt(a, 'source');
    numVar1 = find_numVar(a, 'x_range');
    numVar2 = find_numVar(a, 'y_range');
    numVar3 = find_numVar(a, 'spacing');
    numVar4 = find_numVar(a, 'dimension');
    if ~isempty(numAtt1) &&  ~isempty(numAtt2) && ~isempty(numVar1) && ~isempty(numVar2) && ~isempty(numVar3) && ~isempty(numVar4)
        flag = 1;
    else
        numAtt1 = find_numAtt(a, 'title');
        numAtt2 = find_numAtt(a, 'source');
        numVar1 = find_numVar(a, 'x');
        numVar2 = find_numVar(a, 'y');
        numVar3 = find_numVar(a, 'z');
        if ~isempty(numAtt1) &&  ~isempty(numAtt2) && ~isempty(numVar1) && ~isempty(numVar2) && ~isempty(numVar3)
            flag = 1;
        end
    end
