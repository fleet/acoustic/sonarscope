% Lecture d'un fichier de Salinite et Temperature (.csv)
% Ce fichier peut etre obtenu par extraction a travers le web par :
%   my_web(['http://www.ifremer.fr/sismer/references/climatologies/param-profil.cgi?'...
%   'Clim=Levitus&Annee=1998&Time=saison_1x1_season&Lang=f']);
% en demandant 'psat' et 'temp'
%
% Syntax
%   [Z, S, T, lat, lon, Annee, Saison, TailleGrille] = lecCsv( nomFic )
%   lecCsv( nomFic )
%
% Input Arguments
%   nomFic       : Nom du fichier
%
% Output Arguments
%   []           : Auto-plot activation
%   Z            : Profondeuren m (<0).
%   S            : Salinite en PSU.
%   T            : Temperature en �C.
%   lat          : Latitude del'extraction
%   lon          : Longitude del'extraction
%   Annee        : Annee de la synthese
%   Saison       : Saison
%   TailleGrille : Taille de la grille (1=1x1 ou 2=5x5)
%
% Examples
%   nomFic = getNomFicDatabase('BATHYCEL_North-East_Atlantic_Winter.csv')
%   lecCsv(nomFic);
%
%   [Z, S, T] = lecCsv(nomFic);
%     subplot(1, 2, 1); plot(S,Z); grid on;
%     title('Salinite'); xlabel('Salinite (P.S.U.)'); ylabel('Profondeur (m)');
%     subplot(1, 2, 2); plot(T,Z); grid on;
%     title('Temperature'); xlabel('Temperature (�C)'); ylabel('Profondeur (m)');
%
%   [Z, S, T, lat, lon, Annee, Saison, TailleGrille] = lecCsv( nomFic );
%   lat, lon, Annee, Saison, TailleGrille
%
% See also celeriteChen TrajetRayon ClCelerite Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [Z, S, T, lat, lon, Annee, Saison, TailleGrille] = lecCsv(nomFic)

%% Lecture de l'ent�te

fid = fopen(nomFic);
lines = textscan(fid, '%s', 10, 'Delimiter', newline);
fclose(fid);
lines = lines{1};
celldisp(lines)

%% Lecture du type de grille

if contains(lines{4}, 'par degres carres')
    TailleGrille = 1;
else
    TailleGrille = 2;
end

%% Lecture de l'ann�e

mots = strsplit(lines{4}, {'(';')'});
C = textscan(mots{2}, 'version %d');
Annee = C{1}; 

%% Lecture de la p�riode

mots = strsplit(lines{7}, {':';'(';')'});
Saison = strtrim(mots{2});

%% Lecture de la position d'extraction

mots = strsplit(lines{8}, ' ');
lat = str2lat([mots{1}(1) ' ' mots{1}(2:end) ' ' mots{2}]);
lon = str2lon([mots{3}(1) ' ' mots{3}(2:end) ' ' mots{4}]);

%% Lecture du profil de temperature et salinite

fid = fopen(nomFic);
lines = textscan(fid, '%f%f%f', 'Delimiter', ';', 'headerlines', 10);
fclose(fid);
Z = lines{1};
S = lines{2};
T = lines{3};

sub = find(S == -99.9999);
Z(sub) = []; S(sub) = []; T(sub) = [];
Z = -Z;

if nargout == 0
    FigUtils.createSScFigure
	subplot(1, 2, 1); PlotUtils.createSScPlot(S,Z); grid on;
	title('Salinite'); xlabel('Salinite (P.S.U.)'); ylabel('Profondeur (m)');
	subplot(1, 2, 2); PlotUtils.createSScPlot(T,Z); grid on;
	title('Temperature'); xlabel('Temperature (�C)'); ylabel('Profondeur (m)');
end
