% nomFic = 'C:\Temp\Test ContrainteParPositionPrelev\Prelevements.txt';
% [flag, Prelevement] = lecFchierPrelevements(nomFic)


% nomFic = 'C:\Temp\Test ContrainteParPositionPrelev\Prelevements.txt';
% [flag, Prelevements] = lecFchierPrelevements(nomFic)
% 
% nbPrelevements = length(Prelevements);
% colors = jet(nbPrelevements);
% figure;
% for k=1:nbPrelevements
%     hp = plot(Prelevements(k).Lon, Prelevements(k).Lat, '*');
%     set(hp, 'Color', colors(k,:))
%     grid on; hold on;
% end


function [flag, Prelevement] = lecFchierPrelevements(nomFic)

Prelevement = struct([]);

fid = fopen(nomFic);
if fid == -1
    flag = 0;
    return
end
tline = fgetl(fid);
mots = strsplit(tline, ';');
for i=1:length(mots)
    mots{i} = strtrim(mots{i});
end
while 1
    tline = fgetl(fid);
    if ~ischar(tline)
        break
    end
    mots = strsplit(tline, ';');
    for i=1:length(mots)
        mots{i} = strtrim(mots{i});
        mots{i} = regexprep(mots{i}, ',', '.');
    end
    Prelevement(end+1).Name = mots{1}; %#ok<AGROW>
    Prelevement(end).Lat = str2double(mots{2});
    Prelevement(end).Lon = str2double(mots{3});
    if length(mots) > 3
        Prelevement(end).Num = str2double(mots{4});
        Prelevement(end).Description = mots{5};
    else
        Prelevement(end).Num = length(Prelevement);
        Prelevement(end).Description = mots{1};
    end
end
fclose(fid);
flag = 1;
