% Lecture d'un fichier CASINO
%
% Syntax
%   lecFicCasino(fileCasino)
%
% Input Arguments
%   fileCasino : Nom du fichier Casino
%
% Output Arguments
%   
% EXEMPLE :
%   lecFicCasino('D:\perso-jma\Mes Documents\Reson\Extraction20141021_153006_20141027_063018_tvtouist_events_dmd_QC_SCALABRIN_pour_JMA.xlsx');
%
% See also FusionCasinoCSV Authors
% Authors : GLT
%-------------------------------------------------------------------------------
 
function lecFicCasino(fileCasino)

indDate  = [];
indHeure = [];
indLat   = [];
indLon   = [];
[~, ~, raw] = xlsread(fileCasino, 1, 'A1:Z1');
for k=1:length(raw)
    if ischar(raw{k})
        ind = strfind(raw{k}, 'Date');
        if ~isempty(ind)
            indDate(end+1) = k;
        end
        
        ind = strfind(raw{k}, 'Heure');
        if ~isempty(ind)
            indHeure(end+1) = k;
        end
        
        ind = strfind(raw{k}, 'Latitude');
        if ~isempty(ind)
            indLat(end+1) = k;
        end
        
        ind = strfind(raw{k}, 'Longitude');
        if ~isempty(ind)
            indLon(end+1) = k;
        end
    end
end

if isempty(indDate) || isempty(indHeure) || isempty(indLat) || isempty(indLon)
    str1 = sprintf('Je n''ai pas trouv� de champ "Date", "Heure", "Latitude" ou "Longitude" dans la premi�re ligne du fichier %s', fileCasino);
    str2 = sprintf('I did not finf any "Date", "Heure", "Latitude" ou "Longitude" field in the first line of %s', fileCasino);
    my_warndlg(Lang(str1,str2), 1);
    return
end

Alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

indLatDD = [];
if length(indLat) > 1
    for k=1:length(indLat)
        Lettre = Alphabet(indLat(k));
        num = xlsread(fileCasino, 1, [Lettre '2']);
        if ~isempty(num)
            indLatDD = indLat(k);
            break
        end
    end
end

indLonDD = [];
if length(indLon) > 1
    for k=1:length(indLon)
        Lettre = Alphabet(indLon(k));
        num = xlsread(fileCasino, 1, [Lettre '2']);
        if ~isempty(num)
            indLonDD = indLon(k);
            break
        end
    end
end

if isempty(indLatDD)
    % TODO
else
    Lettre = Alphabet(indLatDD);
    Lat = xlsread(fileCasino, 1, [Lettre '2:' Lettre '32767']);
end

if isempty(indLonDD)
    % TODO
else
    Lettre = Alphabet(indLonDD);
    Lon = xlsread(fileCasino, 1, [Lettre '2:' Lettre '32767']);
end

Lettre = Alphabet(indDate);
[~, DateStr] = xlsread(fileCasino, 1, [Lettre '2:' Lettre '32767']);
date  = dayStr2Ifr(DateStr);

Lettre = Alphabet(indHeure);
[~, ~, HeureStr] = xlsread(fileCasino, 1, [Lettre '2:' Lettre '32767']);
heure  = cell2mat(HeureStr(1:length(date)));

T = cl_time('timeMat', date+heure);

plotNavMercator(Lat, Lon, 'Time', T, 'FileName', fileCasino);
