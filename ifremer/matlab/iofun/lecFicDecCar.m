% Lecture d'un fichier de d�coupe au format Caraibes
%
% Syntax
%   SonarTime = lecFicDecCar(nomFic)
% 
% INTPUT PARAMETERS :
%   nomFic : Nom du fichier de d�coupe
%
% Name-Value Pair Arguments
%    
% Output Arguments
%   SonarTime : intervealles de temps
% 
% EXEMPLES :
%   fileIn = 'D:\Temp\Ptoleme.dec';
%   fileIn = 'U:\EM2040_Thalia\20140602_Thalia_ETAL2040\DATA\EM2040\rawdata\PierresNoiresDecoup.dec';
%   fileIn = 'D:\Temp\Ridha\Bulles\ExAvecBulles\FichierDecoupe.txt';
%   fileIn = 'D:\Temp\Ridha\Bulles\ExAvecBulles\exemple.cut';
%   fileIn = 'D:\Temp\Ridha\Bulles\ExAvecBulles\Geoflamme_7150.cut';
%   fileIn = 'D:\Temp\Ridha\Bulles\ExAvecBulles\Geoflamme_7150 - Copie.cut';
%   SonarTime = lecFicDecCar(fileIn)
%
% See also Authors
% Authors : GLT
%-----------------------------------------------------------------------

function SonarTime = lecFicDecCar(fileIn)
% Format de fichier de d�coupe
%> 15/08/2014  23:28:00:000   15/08/2014  23:38:00:000   PTO_CAL001

fidi = fopen(fileIn, 'rt');
% data = textscan(fidi, '%*s%s%s%s%s%*[^\n]',...
%     'HeaderLines',0,...
%     'EndOfLine','\n',...
%     'MultipleDelimsAsOne',1,...
%     'Delimiter',{' '});
data = textscan(fidi, '%*s%s%s%s%s%*[^\n]',...
    'HeaderLines',         0, ...
    'EndOfLine',           '\n', ...
    'MultipleDelimsAsOne', 1, ...
    'Delimiter',           ' ');
fclose(fidi);

% attention, souvent :,.
% calcul des dates
mot1 = strcat(data{1}, strrep(data{2}, '.', ':'));
mot2 = strcat(data{3}, strrep(data{4}, '.', ':'));

% Ajout pour fichier .cut de Delphine o� ellle a ajout� des lignes de commentaire et des lignes vides
nbRows = length(mot1);
flagOK = true(1, nbRows);
for k=1:nbRows
    if length(mot1{k}) < 22
        flagOK(k) = false;
    elseif ~strcmp(mot1{k}([3 6 13 16 19]), '//:::')
        flagOK(k) = false;
    end
end
mot1 = mot1(flagOK);
mot2 = mot2(flagOK);

dateDeb = datenum(mot1, 'dd/mm/yyyyHH:MM:SS:FFF');
dateFin = datenum(mot2, 'dd/mm/yyyyHH:MM:SS:FFF');

SonarTime = [dateDeb, dateFin];
