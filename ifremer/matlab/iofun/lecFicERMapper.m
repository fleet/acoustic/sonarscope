% Lecture du descripeur des fichiers ER-Mapper
%
% Syntax
%   [I, x, y, params] = lecFicERMapper(nomFic)
%
% Input Arguments
%   nomFic : Nom du fichier
%
% Output Arguments
%   []     : Auto-plot activation
%   I      : Image
%   x      : Abscisses
%   y      : Ordonnees
%   params : Parametres associes
%
% Examples
%    nomFic = '/home/doppler/tmsias/augustin/MRFormatFichier/R2_9925_sonarmosaic';
%    lecFicERMapper([nomFic '.ers'])
%    [I, x, y, params] = lecFicERMapper([nomFic '.ers'])
%
% See also Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function varargout = lecFicERMapper(nomFic)

[nomDir, nomFic] = fileparts(nomFic);
nomFic = fullfile(nomDir, nomFic);

params = lecParamsFicERMapper([nomFic '.ers']);

nbColumns = params.RasterInfo.NrOfCellsPerLine;
nbSlides  = params.RasterInfo.NrOfBands;
nbRows    = params.RasterInfo.NrOfLines;

fid = fopen(nomFic);
I = fread(fid, [nbColumns*nbSlides nbRows], params.RasterInfo.CellType);
I(I == params.RasterInfo.NullCellValue) = NaN;
fclose(fid);
I = rot90(I);

I = reshape(I, [nbRows nbColumns nbSlides]);

x = params.RasterInfo.RegistrationCoord.Eastings  + (0:(nbColumns-1)) * params.RasterInfo.CellInfo.Xdimension;
y = params.RasterInfo.RegistrationCoord.Northings + (0:(nbRows-1)) * params.RasterInfo.CellInfo.Ydimension;

if nargout == 0
    for i=1:size(I,3)
        figure; imagesc(x, y, I(:,:,i)); title(params.RasterInfo.BandId.Value{i}); axis xy; axis equal;
    end
else
    varargout{1} = I;
    varargout{2} = x;
    varargout{3} = y;
    varargout{4} = params;
end
