% Lecture d'un profil d'une composante de themo-salinometrie dans la base Levitus
%
% Syntax
%   [flag, Z, M, S, N, info] = lecFicLevitusMat(nomFic, latitude, longitude, trimestre)
%   lecFicLevitusMat( nomFic, latitude, longitude, trimestre)
%
% Input Arguments
%   nomFic    : Nom du fichier Levitus
%   latitude  : Latitude du point d�sir�
%   longitude : Longitude du point d�sir�
%   trimestre : Numeros(s) du (des) trimestre(s) desires(s)
%
% Name-Value Pair Arguments
%   'Handle' : Handle de l'axe de trace
%
% Output Arguments
%   []     : Auto-plot activation
%   flag   : 1=OK, 0=KO
%   Z      : Profondeur (m)
%   M      : Mpyenne du Parametre (Temperature ou Salinite)
%   S      : Ecart types
%   N      : Nombre d'observations
%   info   : Structure contenant les informations :
%               Lat  : Latitude du point
%               Lon  : Longitude de point
%               Date : Epoque
%
% Examples
%   nomFicMat = getNomFicDatabase('Levitus1x1SeasonTemp.mat')
%   lecFicLevitusMat(nomFicMat, 48, -10, 1)    % Affichage des courbes et des ecart-types
%   lecFicLevitusMat(nomFicMat, 48, -10, 1:4)  % Affichage des courbes pour les differentes saisons
%   [flag, Z, M, S, N, Info] = lecFicLevitusMat(nomFicMat, 48, -10, 1);
%     figure
%     subplot(1,3,1); plot(M, Z); grid on; title('Moy')
%     subplot(1,3,2); plot(S, Z); grid on; title('Ecart-type')
%     subplot(1,3,3); plot(N, Z); grid on; title('Nbp')
%
% See also import_Levitus lecLevitusMat celeriteChen TrajetRayon cli_sound_speed Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [flag, Z, M, S, N, info] = lecFicLevitusMat(nomFic, latitude, longitude, trimestre, varargin)

[varargin, Handle] = getPropertyValue(varargin, 'Handle', []); %#ok<ASGLU>

flag = 1;
Z    = [];
M    = [];
S    = [];
N    = [];
info = [];

if ~exist(nomFic, 'file')
    nomFicGz = [nomFic '.gz'];
    if exist(nomFicGz, 'file')
        disp(['Uncompress : ' nomFicGz ' Please wait'])
        [nomDir, nomFic] = fileparts(nomFicGz);
        nomFic = fullfile(nomDir, nomFic);
        %         pppp = gunzip(nomFicGz, nomDir);
        gunzip(nomFicGz, nomDir);
        delete(nomFicGz)
    else
        str = sprintf('%s not found', nomFic);
        my_warndlg(str, 1);
        flag = 0;
        return
    end
end

%% Recherche du point de la grille le plus proche du point demand�

m = matfile(nomFic);
[nDepth, nTime, nLat, nLon] = size(m, 'Value'); %#ok<ASGLU>
LATITUDE         = m.Lat;
LONGITUDE        = m.Lon;
DATE             = m.DATE;
% DATE_F         = m.DATE_F;
Climatology_Name = m.Climatology_Name;

[~, indLat] = min(abs(LATITUDE - latitude));
[~, indLon] = min(abs(LONGITUDE - longitude));
info.Lat = LATITUDE(indLat);
info.Lon = LONGITUDE(indLon);
info.Climatology_Name = Climatology_Name;

if (nargout == 0) || (nargout >= 5)
    info.Lat = LATITUDE(indLat);
    info.Lon = LONGITUDE(indLon);
    info.Climatology_Name = Climatology_Name;
    for k=1:length(trimestre)
        Date1 = DATE(trimestre(k),:);
        Date1 = strtrim(Date1);
        info.Date{k} = Date1;
    end
end

if (nargout == 0) || (nargout >= 4)
    N = m.N(:, trimestre, indLat, indLon);
end

if (nargout == 0) || (nargout >= 3)
    S = m.Sigma(:, trimestre, indLat, indLon);
end

if (nargout == 0) || (nargout >= 2)
    M = m.Value(:, trimestre, indLat, indLon);
end

if (nargout == 0) || (nargout >= 1)
    Z = m.Z;
end

if nargout == 0
    if isempty(Handle)
        figure; axes;
    else
        axes(Handle);
    end
    ColorOrder = get(gca, 'ColorOrder');
    ColorOrder = [ColorOrder ; ColorOrder];
    
    nbTrimestres = length(trimestre);
    for k=1:nbTrimestres
        sub = ~isnan(M(:,k));
        plot(M(sub,k), Z(sub), 'Color', ColorOrder(k, :)); hold on;
        set(gca, 'XAxisLocation', 'top')
        legende{k} = info.Date{k}; %#ok
        
        if(nbTrimestres == 1)
            plot(M(sub,k)-S(sub,k), Z(sub), '--', 'Color', ColorOrder(k, :));
            plot(M(sub,k)+S(sub,k), Z(sub), '--', 'Color', ColorOrder(k, :));
        end
    end
    hold off; grid on;
    if( length(legende) == 1)
        legende = legende{1};
    end
    legend(legende);
    title(sprintf('%s (%s)\n%s %s', m.NomParam, m.Unit, lat2str(info.Lat), lon2str(info.Lon)));
    ylabel('Profondeur (m)')
end
