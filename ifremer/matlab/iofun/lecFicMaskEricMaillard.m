% nomFic = 'S:\EQUIPEMENTS\RESON7150\DONNEES\WATERCOLUMN\20140207_1342_ping\results_ping_00000016.dat';
% nomFic = 'S:\EQUIPEMENTS\RESON7150\DONNEES\WATERCOLUMN\20140208_1031_ping\results_ping_00000041.dat';
% [flag, kdeb, kfin, kCOG] = lecFicMaskEricMaillard(nomFic)
% FigUtils.createSScFigure; PlotUtils.createSScPlot(kdeb); hold on; PlotUtils.createSScPlot(kfin); PlotUtils.createSScPlot(kCOG, 'r'); grid on

function [flag, kdeb, kfin, kCOG] = lecFicMaskEricMaillard(nomFic)

flag = 0;
kdeb = NaN(1,880);
kfin =  NaN(1,880);
kCOG =  NaN(1,880);

%% Ouverture du fichier

fid = fopen(nomFic, 'r');
if fid == -1
     messageErreurFichier(nomFic, 'ReadFailure');
     return
end

%% Lecture de l'ent�te.

for k=1:880
    tline = fgetl(fid);
    if ~strcmp(tline(1), '0')
        X = sscanf(tline, '%f');
        kdeb(k) = X(1);
        kfin(k) = X(2);
        kCOG(k) = X(3);
    end
end

%% Lecture de Index,Longitude,Latitude

fclose(fid);

flag = 1;
