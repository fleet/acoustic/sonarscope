function [flag, Lat, Lon, Time, Heading, Speed, Immersion, Altitude, Type, Pitch, Roll, Heave] = lecFicNav(nomFic)

Lat       = [];
Lon       = [];
Time      = [];
Heading   = [];
Speed     = [];
Immersion = [];
Altitude  = [];
Type      = [];
Pitch     = [];
Roll      = [];
Heave     = [];
        
if iscell(nomFic)
    nomFic = nomFic{1};
end

isCaraibes = is_CDF(nomFic);
if isCaraibes
    [flag, Lat, Lon, Time, Heading, Speed, Immersion, Altitude, Type] = lecFicNavCaraibes(nomFic);
    if ~flag
        messageErreurFichier(nomFic, 'ReadFailure');
        return
    end
    Type = 'Caraibes';
else
    [~, ~, ext] = fileparts(nomFic);
    switch ext
        case '.dbf'
            [flag, Lat, Lon, Time] = lecFicNavExcel_OTUS_DBF(nomFic);
        case '.txt'
            [flag, Type] = identNavigationFileFormat(nomFic);
            if ~flag
                return
            end
            switch Type
                case 'NoImport'
                    str1 = 'La fonction d''import pour ce format n''est pas encore r�alis�e.';
                    str2 = 'Le import function for this file format is not ready yet.';
                    my_warndlg(Lang(str1,str2), 1);
                    flag = 0;
                    return
                    
                case 'HROV'
                    [flag, Lat, Lon, Time] = lecFicNav_HROV(nomFic);
                    
                case 'Genavir_AUV'
                    [flag, Lat, Lon, Immersion, Time, ~, Heading] = lecFicNav_GenavirAUV(nomFic);
                    
                case 'VLIZ'
                    [flag, Lat, Lon, Time] = lecFicNav_VLIZ(nomFic);
                    
                case 'DelphINS'
                    % M�me format que GenavirAUV mais heading, Pitch, Roll en plus
                    [flag, Time, Lat, Lon, Immersion, Heading, Pitch, Roll, Heave]  = lecFicNav_DelphINS(nomFic);
                    
                case 'PAGURE'
                    [flag, Lat, Lon, Time] = lecFicNav_PAGURE(nomFic);
                    
                case 'Dilip'
                    [flag, Lat, Lon, Time, Immersion] = lecFicNav_Dilip(nomFic);
                    
                otherwise
                    [flag, Lat, Lon, Time] = lecFicNavExcel_OTUS_DBF(nomFic);
            end
            
        case '.xml'
            [flag, DataPosition] = XMLBinUtils.readGrpData(nomFic);
            if flag
                Lat     = DataPosition.Latitude;
                Lon     = DataPosition.Longitude;
                Time    = DataPosition.Datetime;
                Heading = DataPosition.Heading;
                %{
                % TODO
                Speed, Immersion, Altitude, Type, Pitch, Roll, Heave
           Height: [6534�1 single]
             Tilt: [6534�1 single]
              Pan: [6534�1 single]
           Focale: [6534�1 single]
             GITE: [6534�1 single]
         ASSIETTE: [6534�1 single]
                %}
            end
            
        otherwise
            % TODO : sortir un flag de lecFicNavSSC
            [Lat, Lon, Time] = lecFicNavSSC(nomFic);
    end
end

if isempty(Speed)
    [~, Speed] = calCapFromLatLon(Lat, Lon, 'Time', Time);
end
