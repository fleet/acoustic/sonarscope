% Lecture d'une navigation associ�e � un fichier Ascii.
%
% Syntax
%   [flag, Data] = lecFicNavAscii(nomFicNav)
%
% Input Arguments
%   nomFicNav : Nom du fichier de navigation
%
% Output Arguments
%   flag : Indicateur de bon fonctionnement.
%   Data : Structure r�sultant Nav.
%
% Examples
%   nomFicNav    = 'F:\SonarScopeData\SEGY\SEGY From Carla\JR269-14_bin0300_lg.asc';
%   [flag, Data] = lecFicNavAscii(nomFicNav);
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, Data] = lecFicNavAscii(nomFicNav, varargin)

[varargin, charDelimiter] = getPropertyValue(varargin, 'delimiter', 'delimiter', ',');
[varargin, NbLinesHeader] = getPropertyValue(varargin, 'NbLinesHeader', 0); %#ok<ASGLU>

flag = 0;

fid = fopen(nomFicNav, 'r');
if fid == -1
    messageErreurFichier(nomFicNav, 'ReadFailure');
    return
end

NLignesTotal = countLines( nomFicNav );
NbPings      = NLignesTotal - NbLinesHeader;

% Lecture de la ligne l'ent�te.
for i=1:NbLinesHeader
    getData.header{i} = fgetl(fid);
end
% Lecture de Index,Longitude,Latitude
getData.Pos = textscan(fid, '%f %f %f', NbPings, 'delimiter', charDelimiter);

fclose(fid);


%%

% Chargement des latitudes et longitudes si on les reconnait.
lat = getData.Pos{3}(:);
lon = getData.Pos{2}(:);
if lat(1) > 90.0 || lat(1) < -90.0
    lat = getData.Pos{2}(:);
    if lat(1) > 90.0 || lat(1) < -90.0
        flag = 0;
        return
    end
end

Data.Nav.lat = lat;
Data.Nav.lon = lon;

flag = 1;
