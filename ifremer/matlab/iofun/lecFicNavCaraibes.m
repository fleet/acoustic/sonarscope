function [flag, Lat, Lon, Time, Heading, Speed, Immesrsion, Altitude, Type, Quality] = lecFicNavCaraibes(nomFicNav)

flag = 0;
Lat        = [];
Lon        = [];
Time       = [];
Heading    = [];
Speed      = [];
Immesrsion = [];
Altitude   = [];
Type       = [];
Quality    = [];

if iscell(nomFicNav)
    nomFicNav = nomFicNav{1};
end

a = cl_netcdf('fileName', nomFicNav);
sk = get(a, 'skeleton');
if isempty(sk)
    return
end

% TODO : en cas de bug voir code en fin de fichier (r�cup�r� sur
% cl_xtf/import_external_nav et cl_rdf/import_external_nav)

Date = get_value(a, 'mbDate'); 
if isempty(Date)
    return
end

Time = get_value(a, 'mbTime'); 
Time = cl_time('timeIfr', Date, Time);

Lon   = get_value(a, 'mbAbscissa', 'Convert', 'double'); 
Lat   = get_value(a, 'mbOrdinate', 'Convert', 'double'); 
PFlag = get_value(a, 'mbPFlag');

% Ajout JMA le 14/05/2019 pour donn�es Carla � bord du PP?
% PFlag(:) = 2; % Recomment� par JMA le 02/08/2021 pour donn�es Delphine
% Fin Ajout JMA le 14/05/2019 pour donn�es Carla � bord du PP?

subPFlag = (PFlag > 0);
Lon = Lon(subPFlag);
Lat = Lat(subPFlag);
Time = Time(subPFlag);

if nargout >= 4
    Heading = get_value(a, 'mbHeading');
    Heading = Heading(subPFlag);
else
    Heading = [];
end

if nargout >= 5
    Speed = get_value(a, 'mbSpeed') * (1852/3600);
    Speed = Speed(subPFlag);
else
    Speed = [];
end

if nargout >= 6
    Immesrsion = get_value(a, 'mbImmersion');
    Immesrsion = Immesrsion(subPFlag);
    
    % Ajout JMA le 14/05/2019 pour donn�es Carla � bord du PP?
    Immesrsion = -abs(Immesrsion);
    % Fin Ajout JMA le 14/05/2019 pour donn�es Carla � bord du PP?
else
    Immesrsion = [];
end

if nargout >= 7
    Altitude = get_value(a, 'mbAltitude');
    Altitude = Altitude(subPFlag);
else
    Altitude = [];
end

if nargout >= 8
    Type = get_value(a, 'mbType');
else
    Type = [];
end

if nargout >= 9
    Quality = get_value(a, 'mbQuality');
else
    Quality = [];
end

flag = 1;

% figure; plot(Lon, Lat); grid on
