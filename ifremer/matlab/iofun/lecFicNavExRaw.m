% Lecture d'une navigation associ�e � un fichier Raw.
%
% Syntax
%   [flag, Data] = lecFicNavExRaw(nomFicNav) 
%
% Input Arguments
%   nomFicNav : Nom du fichier de navigation
%
% Output Arguments
%   flag : Indicateur de bon fonctionnement. 
%   Data : Structure r�sultant (Time, et Nav). 
%
% Examples 
%   nomFicNav       = 'F:\SonarScopeData\From CSIRO\Raw\EK60_12Khz-D20120730-T045457.raw.gps.csv';
%   [flag, Data]    = lecFicNavExRaw(nomFicNav);
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, Data] = lecFicNavExRaw(nomFicNav)

flag = 0;

fid = fopen(nomFicNav, 'r');
if fid == -1
     messageErreurFichier(nomFicNav, 'ReadFailure');
     return
end

NLignesTotal    = countLines( nomFicNav );
delimiter       = ',';

NbLinesHeader   = 1;
NbPings         = NLignesTotal - NbLinesHeader;
% Lecture de la ligne l'ent�te.
for i=1:NbLinesHeader
    getData.header{i}   = fgetl(fid);
end
% Lecture de GPS_date,GPS_time,GPS_milliseconds,Latitude,Longitude
getData.GPS = textscan(fid, '%s%2d:%2d:%2d%d%f%f', NbPings, 'delimiter', delimiter);

fclose(fid);

% Recomposition de la date au format dd/mm/yyyy.
%% TODO : Module A remplacer par datestr.
sz      = size(getData.GPS{1});
date    = datestr(getData.GPS{1}, 'dd/mm/yyyy');
% L'utulisation de hourStr2IFr n'est pas adapt� au format Heure + mS.
heure   = hourHms2Ifr(double(getData.GPS{2}(1:sz(1))), double(getData.GPS{3}(1:sz(1))), double(getData.GPS{4}(1:sz(1))) + double(getData.GPS{5}(1:sz(1)))*0.001);

date2   = dayStr2Ifr(date(1:sz(1), :)); 
t       = cl_time('timeIfr', date2, heure);

Data.Time   = t.timeMat';
%%

% Calcul de vitesse, de heading entre deux points.
lat                 = getData.GPS{6}(:);
lon                 = getData.GPS{7}(:);
[Cap, Speed]        = calCapFromLatLon(lat, lon, 'Time', t);
Data.Nav.cap        = Cap;
Data.Nav.speed      = Speed;
Data.Nav.lat        = lat;
Data.Nav.lon        = lon;

flag = 1;