function [flag, Lat, Lon, Time] = lecFicNavExcel_OTUS_DBF(nomFicNav)

% TODO : refondre cette fonction tel que c'est fait dans lecFicNav_GenavirAUV

flag = 0;

%% Ouverture du fichier

fid = fopen(nomFicNav, 'r');
if fid == -1
     messageErreurFichier(nomFicNav, 'ReadFailure');
     return
end

%% Lecture de l'ent�te.

for k=1:2
    fgetl(fid);
end

%% Lecture de Index,Longitude,Latitude

tline = fgetl(fid);
fclose(fid);

%% D�codage

Annee = tline(7:11);
index = strfind(tline, Annee);
nbRecords = length(index);

Lat  = NaN(nbRecords,1);
Lon  = NaN(nbRecords,1);
Time = NaN(nbRecords,1);
for k=1:nbRecords
    ideb = index(k)-5;
    Enr = tline(ideb:ideb+44);
    Jour    = str2double(Enr(1:2));
    Mois    = str2double(Enr(4:5));
    Annee   = str2double(Enr(7:10));
    Heure   = str2double(Enr(11:12));
    Minute  = str2double(Enr(14:15));
    Seconde = str2double(Enr(17:18));
    
    date  = dayJma2Ifr(Jour, Mois, Annee);
    heure = (Heure*3600 + Minute*60 + Seconde)*1000;
    Time(k) = dayIfr2Mat(date + (heure / 86400000)); % 24*60*60*1000
    
    Lat(k) = str2double(Enr(24:33));
    Lon(k) = str2double(Enr(35:45));
end
% plot_navigation(nomFicNav, [], Lon, Lat, cl_time('timeMat', Time), []);

Time = cl_time('timeMat', Time);

flag = 1;