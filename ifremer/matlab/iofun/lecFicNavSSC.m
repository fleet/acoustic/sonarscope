function [Lat, Lon, Time] = lecFicNavSSC(nomFicNav)

Data = loadmat(nomFicNav, 'nomVar', 'Data');
Lat = Data.Latitude;
Lon = Data.Longitude;
Time = [];

% figure; plot(Lon, Lat); grid on
