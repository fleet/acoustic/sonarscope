%   nomFicNav = 'Y:\private\ifremer\Herve\MAree_thalia\150601_02.NA';
% [flag, NASY2] = lecFicNav_CINNA(nomFicNav);
%
%   [Fig1, hAxes] = plot_navigation_Figure([], 'Name', nomFicNav);
%   plot_navigation(nomFicNav, Fig1, NASY2.Lon, NASY2.Lat, NASY2.Time, []);
%   FigUtils.createSScFigure; PlotUtils.createSScPlot(NASY2.Altitude); grid
%   sub1 = find(NASY2.Flag);
%   sub2 = find(~NASY2.Flag);
%   FigUtils.createSScFigure; PlotUtils.createSScPlot(sub1, NASY2.Altitude(sub1), '.b'); grid on; hold on;  PlotUtils.createSScPlot(sub2, NASY2.Altitude(sub2), '.r');
%
% http://madida/madida/documents/formet/CINNA_ARCHIVAGE_NMEA-V3__v3.html
% https://www.ifremer.fr/madida/documents/analyses/thalia-maree-gps-fevrier2015__v1.pdf

function [flag, NANAV] = lecFicNav_CINNA(nomFicNav, varargin)

[varargin, nomVar] = getPropertyValue(varargin, 'nomVar', 'NASY1'); % Modif JMA le 10/12/2019 : voir email Herv� du jour
[varargin, vrsn]   = getPropertyValue(varargin, 'vrsn', 1); %#ok<ASGLU> % Modif JMA le 10/12/2019 pour donn�es UNIV-ROUEN de l'Haliotis

[~, ~, Ext] = fileparts(nomFicNav);

if strcmpi(Ext, '.vliz')
    vrsn = 3;
end

NANAV.Lat      = [];
NANAV.Lon      = [];
NANAV.Time     = [];
NANAV.Altitude = [];
NANAV.Heading  = [];
NANAV.Roll     = [];
NANAV.Pitch    = [];
NANAV.Heave    = [];
NANAV.Flag     = [];

modeGPS = {'N', 'D', 'K', 'k'};
% Valeur	Mode GPS
% N	Naturel
% D	Diff�rentiel
% K	Kinematic (fix value)
% k	Kinematic (float value)

Version = 20150805;

%% Ouverture du fichier

fid = fopen(nomFicNav, 'r');
if fid == -1
    flag = 0;
    messageErreurFichier(nomFicNav, 'ReadFailure');
    return
end

%% Test if the .mat file exists and is posterior to the .NA and the format is anterior to this function version

[nomDir, nomFic] = fileparts(nomFicNav);
nomFicMat = fullfile(nomDir, [nomFic '.mat']);
if exist(nomFicMat, 'file')
    FormatVersion = loadmat(nomFicMat, 'nomVar', 'FormatVersion');
    if FormatVersion >= Version
        SMat = dir(nomFicMat);
        S_NA = dir(nomFicNav);
        if ~isempty(S_NA) && (SMat.datenum >= S_NA.datenum)
            NANAV = loadmat(nomFicMat, 'nomVar', 'NANAV');
            flag = 1;
            return
        end
    end
end

%% lecture des enregistrements de navigation

switch vrsn
    case 3 % Fichiers VLIZ Ex : [2020-08-03 04:04:24.648]	2	nmea	$GPGGA,020417.91,5113.5887,N,00256.1328,E,2,05,01,+0015,M,+047,M,02,0652*44
        %         data = textscan(fid, '$GPGGA,%10s,%12s,%5s,%s','EndOfLine','\n', 'Delimiter', '\n', 'collectoutput', true);
        
        while ~feof(fid)
            L = fgetl(fid);
            if ~ischar(L)
                break
            end
            if length(L) < 102
                continue
            end
            
            if ~strcmp(L(34:39), '$GPGGA')
                continue
            end
            
            identNav = str2double(L(27));
            if identNav ~= 1
                continue
            end
            
            [flag, Heure, Lat, Lon, PosData] = read_NMEA_GPGGA(L(34:end));
            if ~flag
                continue
            end
            
            Jour    = str2double(L(10:11));
            Mois    = str2double(L(7:8));
            Annee   = str2double(L(2:5));

            NANAV.Time(end+1) = dayIfr2Mat(dayJma2Ifr(Jour, Mois, Annee)) + Heure /  86400000;
            NANAV.Lat(end+1)  = Lat;
            NANAV.Lon(end+1)  = Lon;
%             NANAV.Heading  = trame{6}(1,:);
%             NANAV.Roll     = trame{6}(2,:);
%             NANAV.Pitch    = trame{6}(3,:);
%             NANAV.Heave    = trame{6}(4,:);
            NANAV.Altitude(end+1) = PosData.GeoidalSeparation;
            NANAV.Flag(end+1)     = PosData.QualityIndicator;
        end
        sz = size(NANAV.Lat);
        NANAV.Time    = cl_time('timeMat', NANAV.Time);
        NANAV.Heading = NaN(sz, 'single');
        NANAV.Roll    = NaN(sz, 'single');
        NANAV.Pitch   = NaN(sz, 'single');
        NANAV.Heave   = NaN(sz, 'single');

    case 2 % Ex :$NANAV,21/03/19,08:55:33.856,NASY4,N,50,10.87276,E,001,38.68758,N,00.8,WG84,00/00/00,08:55:34.000,INT,335.15,-01.76,-01.12,+00.00,+00046.579,000,0000,1,000.00,000.02,2,2,0000.000
        % TODO V2 GLT 14/10/2015
        % lecture de l'ensemble du fichier, et r�cup�ration des date, heures, nom
        % trame, et trame compl�te
        data = textscan(fid,'$NANAV,%8s,%12s,%5s,%s','EndOfLine','\n', 'Delimiter', '\n', 'collectoutput', true);
        % On ne garde que les enregistrements correspondant � la trame � lire
        data = data{1};
        data = data(strcmp(data(:,3), nomVar), [1,2,4]);
        
        % on decode la trame � lire en fonction de son format
        switch nomVar(1:4)
            case 'NASY'
                trame = textscan(strjoin(data(:,end)), '%s%f%f%s%f%f%s%*f%*s%*s%*s%*s%f%f%f%f%f%*d%*d',...
                    'EndOfLine', '\r',...
                    'Delimiter', ',',...
                    'collectoutput', true);
                trame = cellfun(@transpose, trame, 'UniformOutput', false); % pour �tre compatible avec l'ancienne version
                NANAV.Lat      = hemi2sign(trame{1}) .* ( trame{2}'*[1;1/60] )';
                NANAV.Lon      = hemi2sign(trame{3}) .* ( trame{4}'*[1;1/60] )';
                NANAV.Time     = cl_time('timeMat', datenum(strcat(data(:,1), data(:,2)), 'dd/mm/yyHH:MM:SS.FFF')');
                NANAV.Heading  = trame{6}(1,:);
                NANAV.Roll     = trame{6}(2,:);
                NANAV.Pitch    = trame{6}(3,:);
                NANAV.Heave    = trame{6}(4,:);
                NANAV.Altitude = trame{6}(5,:);
                NANAV.Flag     = strcmpi(trame{5}, 'k');
                
                switch nomVar
                    % H.BISQUAY : il y a un d�calage syst�matique entre les crit�res de qualit� GPS en sortie de
                    % l�HDS800 et en sortie de l�Hydrins sur Thalia
                    % --> D en hydrins = K en HDS800 et en r�alit�
                    case 'NASY3'
                        NANAV.Flag = NANAV.Flag | strcmpi(trame{5}, 'd');
                end
                
            otherwise
                str1 = sprintf('Trame non CINNA d�cod�e %s', nomVar);
                str2 = sprintf('Not recognized CINNA thread: %s', nomVar);
                my_warndlg(Lang(str1,str2), 1);
        end
        
    case 1 % V1 old
        while ~feof(fid)
            L = fgetl(fid);
            if ~ischar(L)
                break
            end
            if length(L) < 34
                continue
            end
            
            if ~strcmp(L(1:6), '$NANAV')
                continue
            end
            c = 6 + 1;
            Jour    = str2double(L(c+1:c+2));
            Mois    = str2double(L(c+4:c+5));
            Annee   = str2double(L(c+7:c+8)) + 2000;
            c = c + 1 + 8;
            Heure   = str2double(L(c+1:c+2));
            Minute  = str2double(L(c+4:c+5));
            Seconde = str2double(L(c+7:c+12));
            
            c = c + 1 + 12;
            nomCurVar = L(c+1:c+5);
            if ~strcmp(nomCurVar, nomVar) % si il ne s'agit pas d'une variable � lire, on passe
                continue
            end
            
            switch nomCurVar
                case 'NACON' % $NANAV,01/06/15,00:00:00.000,NACON,PTREF,Point de reference            ,NASY1,AQUA1               ,+000.679,-002.698,+010.862,NASY2,HDS800              ,-000.930,+002.137,+009.291,NASY3,HYDRINS             ,+000.000,+000.000,+000.000,NASY4,<non defini>        ,+000.000,+000.000,+000.000,NASYX,<non defini>        ,+000.000,+000.000,+000.000,BATHY,<non defini>        ,+000.000,+000.000,+000.000,00.000,NS1,NAEN1,<non defini>        ,+000.000,+000.000,+000.000,
                case 'NAMTR' % $NANAV,01/06/15,00:00:00.000,NAMTR,TH_HYDRINS,TH_HDS800 ,TH_HDS800 ,TH_HYDRINS,TH_BEN    ,01,TH_HYDRINS,TH_HYDRINS,TH_BEN    ,
                case 'NAVER' % $NANAV,01/06/15,00:00:00.000,NAVER,Version 8.5         ,27/02/15,27/02/15
                case 'NACOU' % $NANAV,01/06/15,00:00:00.000,NACOU,N,47,52.56467,W,003,54.49035,+005.17,+000.00,+099.90,+099.90,039.90,037.46,5,WG84,02,046,SY2,037.40,NOC,999.90,NOC,999.90,HYB,037.41
                    % $NANAV,01/06/15,00:00:00.888,NASY1,N,47,52.56382,W,003,54.49126,D,01.0,WG84,00/00/00,00:00:01.000,INT,037.49,-00.20,-00.06,+00.00,+00062.839,012,0100
                    % $NANAV,02/06/15,23:59:59.820,NASY2,N,48,22.98865,W,004,28.69521,K,00.8,WG84,00/00/00,00:00:00.000,INT,076.53,+00.20,-00.09,+00.00,+00057.775,002,0181
                    % $NANAV,01/06/15,00:00:00.333,NASY3,N,47,52.56467,W,003,54.49035,N,01.1,WG84,00/00/00,00:00:00.210,INT,037.48,-00.19,-00.08,+00.00,+00049.421,012,0100
                case {'NASY1', 'NASY2', 'NASY3'}
                    % TODO : lire tout �a avec un sscanf, �a ira certainement plus vite
                    
                    c = c + 1 + 5;
                    NANAV.Lat{end+1}  = num2latlon(L(c+1), str2double(L(c+3:c+4)), str2double(L(c+6:c+13)));
                    c = c + 1 + 13;
                    NANAV.Lon{end+1}  = num2latlon(L(c+1), str2double(L(c+3:c+5)), str2double(L(c+7:c+14)));
                    NANAV.Time{end+1} = dayIfr2Mat(dayJma2Ifr(Jour, Mois, Annee)) + hourHms2Ifr(Heure, Minute, Seconde) /  86400000;
                    
                    c = c + 1 + 14;
                    X = L(c+1);
                    switch nomCurVar
                        % H.BISQUAY : il y a un d�calage syst�matique entre les crit�res de qualit� GPS en sortie de
                        % l�HDS800 et en sortie de l�Hydrins sur Thalia
                        case 'NASY3'
                            X = modeGPS(min(find(strcmp(X, modeGPS)) + 1,  numel(modeGPS) ) );
                    end
                    if strcmpi(X, 'k')
                        NANAV.Flag{end+1} = 1;
                    else
                        NANAV.Flag{end+1} = 0;
                    end
                    c = c + 1 + 1;
                    HDOP = L(c+1:c+4); %#ok<NASGU>
                    c = c + 1 + 4;
                    SystemGeodesique = L(c+1:c+4); %#ok<NASGU>
                    c = c + 1 + 4;
                    DateInterneRecepteur   = L(c+1:c+8); %#ok<NASGU>
                    c = c + 1 + 8;
                    HeureInterneRecepteur  = L(c+1:c+12); %#ok<NASGU>
                    c = c + 1 + 12;
                    OrigineValeursAltitude = L(c+1:c+3); %#ok<NASGU>
                    c = c + 1 + 3;
                    NANAV.Heading{end+1}   = str2double(L(c+1:c+6));
                    c = c + 1 + 6;
                    NANAV.Roll{end+1}      = str2double(L(c+1:c+6));
                    c = c + 1 + 6;
                    NANAV.Pitch{end+1}     = str2double(L(c+1:c+6));
                    c = c + 1 + 6;
                    NANAV.Heave{end+1}     = str2double(L(c+1:c+6));
                    c = c + 1 + 6;
                    NANAV.Altitude{end+1}  = str2double(L(c+1:c+10));
                    c = c + 1 + 10;
                    AgeDesCorrections      = L(c+1:c+3); %#ok<NASGU>
                    c = c + 1 + 3;
                    StationDeReference     = L(c+1:c+4); %#ok<NASGU>
                    
                    if mod(length(NANAV.Lat), 1000) == 0
                        fprintf('%s\n', L);
                    end
                    
                case 'NASYC' % $NANAV,01/06/15,00:00:01.000,NASYC,N,47,52.56467,W,003,54.49035,N,00.0,WG84,01/06/15,00:00:01.000,COU,037.49,-00.20,-00.06,+00.00,+00049.421,000,0000
                case 'NAEST' % $NANAV,01/06/15,00:00:01.000,NAEST,+0,039.90,+0,037.49,+0,037.37,-1,000.00,-1,000.00,+0,037.44,+0,+05.73,+00.00,W,-1,+00.00,+00.00,W,+0,AT1,000.00,-00.20,-00.06,+00.00,+0,02.6,046.7,02.2,360.0,
                case 'NAEN1'
                case 'BRUTE' % $NANAV,20/05/15,00:00:05.588,BRUTE,TREU ,$ICLTA,000.5,0000,0000
                otherwise
                    str1 = sprintf('La ligne suivante n''est pas prise en compte : %s', L);
                    str2 = sprintf('Not recognized : %s', L);
                    my_warndlg(Lang(str1,str2), 1);
            end
        end
        
        NANAV.Lon      = [NANAV.Lon{:}];
        NANAV.Lat      = [NANAV.Lat{:}];
        NANAV.Time     = [NANAV.Time{:}];
        NANAV.Altitude = [NANAV.Altitude{:}];
        NANAV.Flag     = ([NANAV.Flag{:}] == 1);
        
        NANAV.Heading  = [NANAV.Heading{:}];
        NANAV.Roll     = [NANAV.Roll{:}];
        NANAV.Pitch    = [NANAV.Pitch{:}];
        NANAV.Heave    = [NANAV.Heave{:}];
        
        % [Fig1, hAxes] = plot_navigation_Figure([], 'Name', nomFicNav);
        %  plot_navigation(nomFicNav, Fig1, NANAV.Lon, NANAV.Lat, NANAV.Time, []);
        %  FigUtils.createSScFigure; PlotUtils.createSScPlot(NANAV.Altitude); grid
        NANAV.Time = cl_time('timeMat', NANAV.Time);
        
    otherwise
        disp('pas de version');
end
fclose(fid);

NANAV.Datetime = datetime(NANAV.Time.timeMat, 'ConvertFrom', 'datenum');

flag = 1;

%% Save the data in a .mat file in order it is faster to read next time (and will keep the RTK filters)

FormatVersion = Version;
save(nomFicMat, 'NANAV', 'FormatVersion')

