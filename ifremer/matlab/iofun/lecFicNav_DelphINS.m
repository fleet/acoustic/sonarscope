function [flag, Time, Lat, Lon, Immersion, Heading, Pitch, Roll, Heave] = lecFicNav_DelphINS(nomFicNav)

% TODO : refondre cette fonction tel que c'est fait dans lecFicNav_GenavirAUV

nbRec = getNbLinesAsciiFile(nomFicNav);
if nbRec <= 1
    flag = 0;
    return
end

nbRec = nbRec - 1;

Time      = NaN(nbRec, 1);
Lat       = NaN(nbRec, 1);
Lon       = NaN(nbRec, 1);
Immersion = NaN(nbRec, 1, 'single');
Heading   = NaN(nbRec, 1, 'single');
Pitch     = NaN(nbRec, 1, 'single');
Roll      = NaN(nbRec, 1, 'single');
Heave     = NaN(nbRec, 1, 'single');

%% Ouverture du fichier

fid = fopen(nomFicNav, 'r');
if fid == -1
     messageErreurFichier(nomFicNav, 'ReadFailure');
     return
end

%% Lecture de l'ent�te.

fgetl(fid);

%% lecture des enregistrements de navigation

hw = create_waitbar('Reading Navigation file', 'N', nbRec);
for k=1:nbRec
    my_waitbar(k, nbRec, hw)
    
    tline = fgetl(fid);
    if ~ischar(tline)
        break
    end
    if isempty(tline)
        continue
    end
    mots = strsplit(tline);
    
    Jour  = str2double(mots{1}(1:2));
    Mois  = str2double(mots{1}(4:5));
    Annee = str2double(mots{1}(7:10));
    
    Heure   = str2double(mots{2}(1:2));
    Minute  = str2double(mots{2}(4:5));
    Seconde = str2double(mots{2}(7:end));
    
    ind  = strfind(mots{3}, '�');
    Deg  = str2double(mots{3}(1:(ind-1)));
    Min  = str2double(mots{3}((ind+1):end));
    Lat(k) = sign(Deg) * (abs(Deg) + Min / 60);
    
    ind  = strfind(mots{4}, '�');
    Deg  = str2double(mots{4}(1:(ind-1)));
    Min  = str2double(mots{4}((ind+1):end));
    Lon(k) = sign(Deg) * (abs(Deg) + Min / 60);
    
    Immersion(k) = -str2double(mots{5});
    Heading(k)   = -str2double(mots{6});
    Pitch(k)     = -str2double(mots{7});
    Roll(k)      = -str2double(mots{8});
    Heave(k)     = -str2double(mots{9});
        
    date  = dayJma2Ifr(Jour, Mois, Annee);
    heure = (Heure*3600 + Minute*60 + Seconde)*1000;
    Time(k) = dayIfr2Mat(date + (heure / 86400000)); % 24*60*60*1000
end
my_close(hw)

sub = ~isnan(Lat);
Lat       = Lat(sub);
Lon       = Lon(sub);
Time      = Time(sub);
Immersion = Immersion(sub);
Heading   = Heading(sub);
Pitch     = Pitch(sub);
Roll      = Roll(sub);
Heave     = Heave(sub);

%% Fermeture du fichier

fclose(fid);
    
% plot_navigation(nomFicNav, [], Lon, Lat, cl_time('timeMat', Time), []);

Time = cl_time('timeMat', Time);

flag = 1;