function [flag, Lat, Lon, Time, Immersion] = lecFicNav_Dilip(nomFicNav)

T = readtable(nomFicNav);

Time = datetime(T.Var1 ,T.Var2, T.Var3, T.Var4, T.Var5, T.Var6);
Time = cl_time('timeMat', datenum(Time));

Lon       =  T.Var7;
Lat       =  T.Var8;
Immersion = -T.Var9;

flag = 1;

% figure; plot(Lon, Lat); grid on; axisGeo;
% figure; plot(Time, Lat); grid on;
