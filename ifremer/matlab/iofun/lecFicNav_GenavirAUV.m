% nomFicNav = FtpUtils.importDataForDemoFromSScFTP('DataForDemo/NavAUV/usblRepeater09Modif.txt');
% [flag, Lat, Lon, Immersion, Time, Datetime] = lecFicNav_GenavirAUV(nomFicNav)
%
% UTC	Date	Latitude	Longitude	Immersion
% 20/08/2014	08:41:13.562	43.12191066742	5.50182215373	-0.8
% 20/08/2014	09:04:01.722	43.12201983134	5.50040566127	-75.1
% 20/08/2014	09:04:07.726	43.12205533187	5.50033884048	-79.0

function [flag, Lat, Lon, Immersion, Time, Datetime, Heading] = lecFicNav_GenavirAUV(nomFicNav)

flag      = 0;
Lat       = [];
Lon       = [];
Immersion = [];
Time      = [];
Datetime  = [];
Heading   = [];

% Comment� par JMA le 14/10/2020, mission MAYOBS15 pour AUV

% try % Utilisation de readtable
% %     opts = detectImportOptions(nomFicNav);
% %     preview(nomFicNav, opts)
%     
%     T = readtable(nomFicNav);
% %     T = readtable(nomFicNav, 'Format', '%{dd/MM/yyyy}D %f', 'delimiter', sprintf('\t'), 'HeaderLines', 1);
%    
%     head(T)
%     fprintf('File %s\nFirst column is considered as date, 2nd as time, 3rd as latitude, 4th as longitude and 5th as immersion.\n', nomFicNav);
%     
%     % On connait l'ordre des colonnes mais les noms changent parfois, on
%     % n'utilise donc pas les noms absolus comme T.Latitude, etc ....
%     
%     Datetime  = T.(1) + T.(2);
%     Lat       = T.(3);
%     Lon       = T.(4);
%     Immersion = T.(5);
%     
%     Datetime.Format = 'yyyy-MM-dd HH:mm:ss';
%     Time = cl_time('timeMat', datenum(Datetime));
%     
%     flag = 1;
%     
% catch % Conserver quand m�me cet ancien code au cas o� (pb de NaN par ex ?)
    
    nbRec = getNbLinesAsciiFile(nomFicNav);
    if nbRec <= 1
        flag = 0;
        return
    end
    
    nbRec = nbRec - 1;
    Lat       = NaN(nbRec, 1);
    Lon       = NaN(nbRec, 1);
    Time      = NaN(nbRec, 1);
    Immersion = NaN(nbRec, 1, 'single');
    Heading   = NaN(nbRec, 1, 'single');
    
    %% Ouverture du fichier
    
    fid = fopen(nomFicNav, 'r');
    if fid == -1
        messageErreurFichier(nomFicNav, 'ReadFailure');
        return
    end
    
    %% Lecture de l'ent�te.
    
    fgetl(fid);
    
    %% lecture des enregistrements de navigation
    
    for k=1:nbRec
        tline = fgetl(fid);
        if ~ischar(tline)
            break
        end
        if isempty(tline)
            continue
        end
        %     mots = strsplit(tline);
        %     Jour  = str2double(mots{1}(1:2));
        %     Mois  = str2double(mots{1}(4:5));
        %     Annee = str2double(mots{1}(7:10));
        %
        %     Heure   = str2double(mots{2}(1:2));
        %     Minute  = str2double(mots{2}(4:5));
        %     Seconde = str2double(mots{2}(7:12));
        %
        %     Lat(k)       = str2double(mots{3});
        %     Lon(k)       = str2double(mots{4});
        %     Immersion(k) = str2double(mots{5});
        
        tline = strrep(tline, '/', ' ');
        tline = strrep(tline, ':', ' ');
        tline = strrep(tline, ';', ' ');
        [a, count, errmsg, nextindex] = sscanf(tline, '%f');
        if a(1) > 31
            Jour  = abs(a(3));
            Mois  = abs(a(2));
            Annee = a(1);
        else
            Jour  = a(1);
            Mois  = a(2);
            Annee = a(3);
        end
        Heure        = abs(a(4));
        Minute       = abs(a(5));
        Seconde      = abs(a(6));
        Lat(k)       = a(7);
        Lon(k)       = a(8);
        if length(a) >= 9
            Immersion(k) = a(9);
        end
        if length(a) >= 10
            Heading(k) = a(10);
        end
        
        date  = dayJma2Ifr(Jour, Mois, Annee);
        heure = (Heure*3600 + Minute*60 + Seconde)*1000;
        Time(k) = dayIfr2Mat(date + (heure / 86400000)); % 24*60*60*1000
    end
    
    sub = ~isnan(Lat);
    Lat  = Lat(sub);
    Lon  = Lon(sub);
    Time = Time(sub);
    Immersion = Immersion(sub);
    
    % Ajout JMA le 14/05/2019 pour donn�es Carla � bord du PP?
    Immersion = -abs(Immersion);
    % Fin Ajout JMA le 14/05/2019 pour donn�es Carla � bord du PP?
    
    %% Fermeture du fichier
    
    fclose(fid);
    
    % plot_navigation(nomFicNav, [], Lon, Lat, cl_time('timeMat', Time), []);
    
    Datetime = datetime(Time, 'ConvertFrom', 'datenum');
    Time = cl_time('timeMat', Time);
    
    flag = 1;
% end
