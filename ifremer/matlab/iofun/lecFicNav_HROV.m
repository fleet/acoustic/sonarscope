function [flag, Lat, Lon, Time] = lecFicNav_HROV(nomFicNav)

T = readtable(nomFicNav);
Lat = T.Latitude;
Lon = T.Longitude;

Time = datetime(T.Date, 'InputFormat', 'yyyy-MM-dd HH:mm:ss.SSS');
Time.Format = 'yyyy-MM-dd HH:mm:ss';

flag = 1;

% figure; plot(Lon, Lat); grid on; axisGeo;
% figure; plot(Time, Lat); grid on;
