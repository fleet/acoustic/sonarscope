function [flag, Lat, Lon, Time] = lecFicNav_PAGURE(nomFicNav)

T = readtable(nomFicNav);
Lat = T.LAT_PAGURE;
Lon = T.LONG_PAGURE;

Time = T.DATE + T.HEURE;
Time.Format = 'yyyy-MM-dd HH:mm:ss';

flag = 1;
