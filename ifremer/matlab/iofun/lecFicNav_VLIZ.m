function [flag, Lat, Lon, Time] = lecFicNav_VLIZ(nomFicNav)

% TODO : refondre cette fonction tel que c'est fait dans lecFicNav_GenavirAUV

nbRec = getNbLinesAsciiFile(nomFicNav);
if nbRec <= 1
    flag = 0;
    return
end

nbRec = nbRec - 1;
Lat       = NaN(nbRec, 1);
Lon       = NaN(nbRec, 1);
Time      = NaN(nbRec, 1);
Immersion = NaN(nbRec, 1, 'single');

%% Ouverture du fichier

fid = fopen(nomFicNav, 'r');
if fid == -1
     messageErreurFichier(nomFicNav, 'ReadFailure');
     return
end

%% Lecture de l'ent�te.

% fgetl(fid);

%% lecture des enregistrements de navigation

for k=1:nbRec
    tline = fgetl(fid);
    if ~ischar(tline)
        break
    end
    if isempty(tline)
        continue
    end
%     mots = strsplit(tline);
%     Jour  = str2double(mots{1}(1:2));
%     Mois  = str2double(mots{1}(4:5));
%     Annee = str2double(mots{1}(7:10));
%     
%     Heure   = str2double(mots{2}(1:2));
%     Minute  = str2double(mots{2}(4:5));
%     Seconde = str2double(mots{2}(7:12));
%     
%     Lat(k)       = str2double(mots{3});
%     Lon(k)       = str2double(mots{4});
%     Immersion(k) = str2double(mots{5});
    
    tline = strrep(tline, '/', ' ');
    tline = strrep(tline, ':', ' ');
    tline = strrep(tline, ';', ' ');
    [a, count, errmsg, nextindex] = sscanf(tline, '%f');
    if a(1) > 31
        Jour  = abs(a(3));
        Mois  = abs(a(2));
        Annee = a(1);
    else
        Jour  = a(1);
        Mois  = a(2);
        Annee = a(3);
    end
    Heure        = abs(a(4));
    Minute       = abs(a(5));
    Seconde      = abs(a(6));
    Lat(k)       = a(7);
    Lon(k)       = a(8);
    if length(a) >= 9
        Immersion(k) = a(9);
    end
    
    date  = dayJma2Ifr(Jour, Mois, Annee);
    heure = (Heure*3600 + Minute*60 + Seconde)*1000;
    Time(k) = dayIfr2Mat(date + (heure / 86400000)); % 24*60*60*1000
end

sub = ~isnan(Lat);
Lat  = Lat(sub);
Lon  = Lon(sub);
Time = Time(sub);

%% Fermeture du fichier

fclose(fid);
    
% plot_navigation(nomFicNav, [], Lon, Lat, cl_time('timeMat', Time), []);

Time = cl_time('timeMat', Time);

flag = 1;
