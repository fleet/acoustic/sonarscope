% nomFicSipican = 'D:\Temp\Ridha\TXT\S0820.TXT';
% [flag, Time, Temp, Sal, Depth] = lecFicSipican(nomFicSipican)

function [flag, Time, Temp, Sal, Depth] = lecFicSipican(nomFicSipican)

nbRec = getNbLinesAsciiFile(nomFicSipican);
if nbRec <= 1
    Time  = [];
    Temp  = [];
    Sal   = [];
    Depth = [];
    flag = 0;
    return
end

nbRec = nbRec - 5;

Time  = NaN(nbRec, 1);
Temp  = NaN(nbRec, 1, 'single');
Sal   = NaN(nbRec, 1, 'single');
Depth = NaN(nbRec, 1, 'single');

%% Ouverture du fichier

fid = fopen(nomFicSipican, 'r');
if fid == -1
     messageErreurFichier(nomFicSipican, 'ReadFailure');
     return
end

%% Lecture de l'ent�te.

for k=1:5
    fgetl(fid)
end

%% lecture des enregistrements de navigation

hw = create_waitbar('Reading sipican file', 'N', nbRec);
for k=1:nbRec
    my_waitbar(k, nbRec, hw)
    
    tline = fgetl(fid);
    if ~ischar(tline)
        break
    end
    if isempty(tline)
        continue
    end
    mots = strsplit(tline);
    
    if strcmp(mots{1}, 'Date')
        continue
    end
    
    Temp(k)  = str2double(mots{1});
    Depth(k) = str2double(mots{2});
    Sal(k)   = str2double(mots{3});
    
    Jour  = str2double(mots{4}(1:2));
    Mois  = str2double(mots{4}(4:5));
    Annee = str2double(mots{4}(7:10));
    
    Heure   = str2double(mots{5}(1:2));
    Minute  = str2double(mots{5}(4:5));
    Seconde = str2double(mots{5}(7:end));
        
    date  = dayJma2Ifr(Jour, Mois, Annee);
    heure = (Heure*3600 + Minute*60 + Seconde)*1000;
    Time(k) = dayIfr2Mat(date + (heure / 86400000)); % 24*60*60*1000
end
my_close(hw)


%% Fermeture du fichier

fclose(fid);
    

% plot_navigation(nomFicSipican, [], Lon, Lat, cl_time('timeMat', Time), []);
Time = cl_time('timeMat', Time);

flag = 1;