function [flag, Lat, Lon, Time, depth, vel] = lecFicVelCaraibes(nomFicVel)

if iscell(nomFicVel)
    nomFicVel = nomFicVel{1};
end

a = cl_netcdf('fileName', nomFicVel);
sk = get(a, 'skeleton');
if isempty(sk)
    flag = 0;
    return
end

% TODO : en cas de bug voir code en fin de fichier (r�cup�r� sur
% cl_xtf/import_external_nav et cl_rdf/import_external_nav)

Date = get_value(a, 'mbDate'); 
Time = get_value(a, 'mbTime'); 
Time = cl_time('timeIfr', Date, Time);

Lon = get_value(a, 'mbAbscissa', 'Convert', 'double'); 
Lat = get_value(a, 'mbOrdinate', 'Convert', 'double'); 
% PFlag = get_value(a, 'mbPFlag');
% 
% Lon = Lon(PFlag > 0);
% Lat = Lat(PFlag > 0);
% Time = Time(PFlag > 0);

if nargout >= 4
    depth = get_value(a, 'mbDepth');
else
    depth = [];
end

if nargout >= 5
    vel = get_value(a, 'mbSoundVelocity');
else
    vel = [];
end

flag = 1;