% Lecture d'un profil de themo-salinometrie dans la base Levitus
%
% Syntax
%   [flag, Z, T, S, SigmaT, SigmaS, NT, NS, info] = lecLevitusMat(nomFic, latitude, longitude, trimestre)
%   lecLevitusMat( nomFic, latitude, longitude, trimestre)
% 
% Input Arguments 
%   nomFic    : Nom du fichier Levitus
%   latitude  : Latitude du point desire 
%   longitude : Longitude du point desire 
%   trimestre : Numeros(s) du (des) trimestre(s) desires(s) 
%
% Output Arguments
%   []     : Auto-plot activation
%   flag   : 1=OK, O=KO
%   Z      : Profondeur (m)
%   T      : Temperature (deg)
%   S      : Salinite (0/00)
%   SigmaT : Ecart types de T
%   SigmaS : Ecart types de S
%   NT     : Nombre de mesures utilisees pour moyenner la temperature
%   NS     : Nombre de mesures utilisees pour moyenner la Salinite
%   info   : Structure contenant les informations :
%               Lat  : Latitude du point
%               Lon  : Longitude de point
%               Date : Mois (1-12) ou Trimestre (1-4)
% 
% Examples
%   nomFic = getNomFicDatabase('Levitus1x1MonthTemp.mat');
%   nomFic = strrep(nomFic, 'Temp.mat', '') % TODO : c'est �s top de devoir faire �a !!!
%   lecLevitusMat(nomFic, 48, -10, 1)    % Affichage des courbes et des ecart-types
%   lecLevitusMat(nomFic, 48, -10, 1:4)  % Affichage des courbes pour les differentes saisons
%
%   [flag, Z, T, S] = lecLevitusMat(nomFic, 48, -10, 1);
%     figure; plot(T, Z); grid on;
%   [flag, Z, T, S, ST, SS, NT, NS, info] = lecLevitusMat(nomDir, 48, -10, 1);
%
% See also Levitus2mat lecFicLevitusMat lecLevitusSyntheseTemporelle celeriteChen TrajetRayon cli_sound_speed Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [flag, Z, T, S, SigmaT, SigmaS, NT, NS, info] = lecLevitusMat(nomFic, latitude, longitude, trimestre)

Z      = [];
S      = [];
SigmaS = [];
NS     = [];

%% Lecture de la temp�rature

nomFicTemp = [nomFic 'Temp.mat'];
[flag, ZT, T, SigmaT, NT, info] = lecFicLevitusMat(nomFicTemp, latitude, longitude, trimestre);
if ~flag || isempty(ZT)
    return
end

%% Lecture de la salinit�

nomFicSal  = [nomFic ,'Psal.mat'];
[flag, ZS, S, SigmaS, NS, info] = lecFicLevitusMat(nomFicSal, latitude, longitude, trimestre);
if ~flag || isempty(ZT)
    return
end

%% Extraction des zones communes

[Z, subT, subS] = intersect(-ZT, -ZS);
Z       = -Z(:);
T       = T(subT,:);
SigmaT  = SigmaT(subT,:);
NT      = NT(subT,:);
S       = S(subS,:);
SigmaS  = SigmaS(subS,:);
NS      = NS(subS,:);

if nargout == 0
    figure
    h = subplot(1,2,1);
    lecFicLevitusMat(nomFicTemp, latitude, longitude, trimestre, 'Handle', h);

    h = subplot(1,2,2);
    lecFicLevitusMat(nomFicSal, latitude, longitude, trimestre, 'Handle', h);
end
