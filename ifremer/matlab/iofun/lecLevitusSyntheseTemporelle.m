 % Compilation d'un profil de themo-salinometrie dans la base Levitus
% en raboutant les profils mensuels par les profils trimestriels
% et les trimestriels par les annuels.
%
% Syntax
%   [flag, Z, T, S, ST, SS, NT, NS, INFO] = lecLevitusSyntheseTemporelle( latitude, longitude, grille, type, iMonth)
%   lecLevitusSyntheseTemporelle( latitude, longitude, grille, type, iMonth)
% 
% Input Arguments 
%   nomFic    : Nom du fichier Levitus
%   latitude  : Latitude du point desire 
%   longitude : Longitude du point desire
%   grille    : 1=1x1, 2=5x5
%   type      : 1=Annual, 2=Season, 3=Month
%   iMonth    : Numero du trimestre ou du mois desire (non utilise si "Annual")
%
% Output Arguments
%   []     : Auto-plot activation
%   flag   : 1=OK, 0=KO
%   Z      : Profondeur (m)
%   T      : Temperature (deg)
%   S      : Salinite (0/00)
%   ST     : Ecart types de T
%   SS     : Ecart types de S
%   NT     : Nombre de mesures utilisees pour moyenner la temperature
%   NS     : Nombre de mesures utilisees pour moyenner la Salinite
%   INFO   : Tableau de structures contenant les informations pour chaque state d'extraction :
%               Lat              : Latitude du point
%               Lon              : Longitude de point
%               Climatology_Name : 'Global seasonal LEVITUS climatology (1998 release) per degree square (0-5500m)'
%               Date             : Epoque
%               Z                : Liste des Z
%               ZLim             : Valeurs extremales des profondeurs
% 
% Examples
%   lecLevitusSyntheseTemporelle(48, -10, 2, 3, 8)    % Affichage des courbes et des ecart-types
%   lecLevitusSyntheseTemporelle(48, -10, 2, 2, 1:4)  % Affichage des courbes pour les differentes saisons
%   [flag, Z, T, S] = lecLevitusSyntheseTemporelle(48, -10, 2, 2, 1);
%     FigUtils.createSScFigure; plot(T, Z); grid on;
%   [flag, Z, T, S, ST, SS, NT, NS, INFO] = lecLevitusSyntheseTemporelle(48, -10, 2, 2, 1);
%
% See also lecLevitusMat lecFicLevitus celeriteChen TrajetRayon cli_sound_speed Authors
% Authors : JMA
%--------------------------------------------------------------------------

function [flag, Z, T, S, ST, SS, NT, NS, INFO] = lecLevitusSyntheseTemporelle(latitude, longitude, grille, type, varargin)

if isempty(varargin)
    iMonth = 1;
else
    iMonth = varargin{1};
end

flag = 1;
Z    = [];
T    = [];
S    = [];
ST   = [];
SS   = [];
NT   = [];
NS   = [];
INFO = [];

if (grille == 1) && (type >= 3) % 1x1 Month
    nomFic = getNomFicDatabase('Levitus1x1MonthPsal.mat');
    [flag, z, t, s, st, ss, nt, ns, info] = lecLevitusMat(nomFic(1:end-8), latitude, longitude, iMonth);
    if ~flag
        return
    end
    sub = ~isnan(t) & ~isnan(s);
    if any(sub)
        Z = z(sub);
        T = t(sub);
        S = s(sub);
        ST = st(sub);
        SS = ss(sub);
        NT = nt(sub);
        NS = ns(sub);

        info.Z = z';
        info.FileName = nomFic;
        INFO{end+1} = info;
    end
end
iSeason = floor(1 + (iMonth-1)/3);
        
if (grille == 1) && (type >= 2) % 1x1 Season
    nomFic = getNomFicDatabase('Levitus1x1SeasonPsal.mat');
    [flag, z, t, s, st, ss, nt, ns, info] = lecLevitusMat(nomFic(1:end-8), latitude, longitude, iSeason);
    if ~flag
        return
    end
    info.FileName = nomFic;
    if isempty(Z)
        sub = ~isnan(t) & ~isnan(s);
        if any(sub)
            Z = z(sub);
            T = t(sub);
            S = s(sub);
            ST = st(sub);
            SS = ss(sub);
            NT = nt(sub);
            NS = ns(sub);
            info.Z = z';
            INFO{end+1} = info;
        end
    else
        sub = (z(:) < min(Z)) & ~isnan(t) & ~isnan(s);
        if any(sub)
            Z = [Z;z(sub)];
            T = [T;t(sub,:)];
            S = [S;s(sub,:)];
            ST = [ST;st(sub,:)];
            SS = [SS;ss(sub,:)];
            NT = [NT;nt(sub,:)];
            NS = [NS;ns(sub,:)];
            info.Z = z(sub)';
            INFO{end+1} = info;
        end
    end
end

if (grille == 1) && (type >= 1) % 1x1 Annual
    nomFic = getNomFicDatabase('Levitus1x1AnnualPsal.mat');
    [flag, z, t, s, st, ss, nt, ns, info] = lecLevitusMat(nomFic(1:end-8), latitude, longitude, 1);
    if ~flag
        return
    end
    info.FileName = nomFic;
    if isempty(Z)
        sub = ~isnan(t) & ~isnan(s);
        if any(sub)
            Z = z(sub);
            T = t(sub);
            S = s(sub);
            ST = st(sub);
            SS = ss(sub);
            NT = nt(sub);
            NS = ns(sub);
            info.Z = z';
            INFO{end+1} = info;
        end
    else
        sub = (z(:) < min(Z)) & ~isnan(t) & ~isnan(s);
        if any(sub)
            Z = [Z;z(sub)];
            T = [T;t(sub,:)];
            S = [S;s(sub,:)];
            ST = [ST;st(sub,:)];
            SS = [SS;ss(sub,:)];
            NT = [NT;nt(sub,:)];
            NS = [NS;ns(sub,:)];
            info.Z = z(sub)';
            INFO{end+1} = info;
        end
    end
end

if type >= 3 % 5x5 Month
    nomFic = getNomFicDatabase('Levitus5x5MonthPsal.mat');
    [flag, z, t, s, st, ss, nt, ns, info] = lecLevitusMat(nomFic(1:end-8), latitude, longitude, iMonth);
    if ~flag
        return
    end
    info.FileName = nomFic;
    if isempty(Z)
        sub = ~isnan(t) & ~isnan(s);
        if any(sub)
            Z = z(sub);
            T = t(sub);
            S = s(sub);
            ST = st(sub);
            SS = ss(sub);
            NT = nt(sub);
            NS = ns(sub);
            info.Z = z';
            INFO{end+1} = info;
        end
    else
        sub = (z(:) < min(Z)) & ~isnan(t) & ~isnan(s);
        if any(sub)
            Z = [Z;z(sub)];
            T = [T;t(sub,:)];
            S = [S;s(sub,:)];
            ST = [ST;st(sub,:)];
            SS = [SS;ss(sub,:)];
            NT = [NT;nt(sub,:)];
            NS = [NS;ns(sub,:)];
            info.Z = z(sub)';
            INFO{end+1} = info;
        end
    end
end

if type >= 2 % 5x5 Season
    nomFic = getNomFicDatabase('Levitus5x5SeasonPsal.mat');
    %     nomDir = fileparts(nomFic);
    [flag, z, t, s, st, ss, nt, ns, info] = lecLevitusMat(nomFic(1:end-8), latitude, longitude, iSeason);
    if ~flag
        return
    end
    if ~flag
        return
    end
    info.FileName = nomFic;
    if isempty(Z)
        sub = ~isnan(sum(t,2)) & ~isnan(sum(s,2));
        if any(sub)
            Z = z(sub);
            T = t(sub);
            S = s(sub);
            ST = st(sub);
            SS = ss(sub);
            NT = nt(sub);
            NS = ns(sub);
            info.Z = z';
            INFO{end+1} = info;
        end
    else
        sub = (z(:) < min(Z)) & ~isnan(sum(t,2)) & ~isnan(sum(s,2));
        if any(sub)
            Z = [Z;z(sub)];
            T = [T;t(sub,:)];
            S = [S;s(sub,:)];
            ST = [ST;st(sub,:)];
            SS = [SS;ss(sub,:)];
            NT = [NT;nt(sub,:)];
            NS = [NS;ns(sub,:)];
            info.Z = z(sub)';
            INFO{end+1} = info;
        end
    end
end

if (type >= 1) % 5x5 Annual
    nomFic = getNomFicDatabase('Levitus5x5AnnualPsal.mat');
    [flag, z, t, s, st, ss, nt, ns, info] = lecLevitusMat(nomFic(1:end-8), latitude, longitude, 1);
    if ~flag
        return
    end
    info.FileName = nomFic(1:end-8);
    if isempty(Z)
        sub = ~isnan(sum(t,2)) & ~isnan(sum(s,2));
        if any(sub)
            Z = z(sub);
            T = t(sub);
            S = s(sub);
            ST = st(sub);
            SS = ss(sub);
            NT = nt(sub);
            NS = ns(sub);
        end
        info.Z = z';
        INFO{end+1} = info;
    else
        sub = (z(:) < min(Z)) & ~isnan(sum(t,2)) & ~isnan(sum(s,2));
        if any(sub)
            Z = [Z;z(sub)];
            T = [T;t(sub,:)];
            S = [S;s(sub,:)];
            ST = [ST;st(sub,:)];
            SS = [SS;ss(sub,:)];
            NT = [NT;nt(sub,:)];
            NS = [NS;ns(sub,:)];
            info.Z = z(sub)';
            INFO{end+1} = info;
        end
    end
end

for k=1:length(INFO)
    INFO{k}.ZLim = sprintf('[%f %f]', min(INFO{k}.Z), max(INFO{k}.Z)); %#ok<AGROW>
end

if nargout == 0
    fig = FigUtils.createSScFigure;
    plotZTS(fig, Z, T, S, ST, SS, INFO);
    for k=1:length(INFO)
        disp(INFO{k})
    end
end

function plotZTS(fig, Z, T, S, ST, SS, INFO)

h(1) = subplot(1,2,1);
h(2) = subplot(1,2,2);

ColorOrder = get(gca, 'ColorOrder');
for k1=1:size(T,2)
    k2 = 1 + mod(k1-1, size(ColorOrder, 1));
    kl = 1 + floor((k1-1) / size(ColorOrder, 1));
    lineStyle{1} = '-'; lineStyle{2} ='--';

    sub = find(~isnan(T(:,k1)));

    set(fig, 'CurrentAxes', h(1));
    plot(T(sub,k1), Z(sub), 'Color', ColorOrder(k2, :), 'LineStyle', lineStyle{kl}); hold on;
    set(gca, 'XAxisLocation', 'top')

    set(fig,'CurrentAxes', h(2));
    plot(S(sub,k1), Z(sub), 'Color', ColorOrder(k2, :), 'LineStyle', lineStyle{kl}); hold on;
    set(gca, 'XAxisLocation', 'top')

    legende{k1} = INFO{1}.Date{k1}; %#ok

    if(size(T,2) == 1)
        set(fig,'CurrentAxes', h(1));
        PlotUtils.createSScPlot(T(sub,k1)-ST(sub,k1), Z(sub), '--', 'Color', ColorOrder(k2, :));
        PlotUtils.createSScPlot(T(sub,k1)+ST(sub,k1), Z(sub), '--', 'Color', ColorOrder(k2, :));

        set(fig,'CurrentAxes', h(2));
        PlotUtils.createSScPlot(S(sub,k1)-SS(sub,k1), Z(sub), '--', 'Color', ColorOrder(k2, :));
        PlotUtils.createSScPlot(S(sub,k1)+SS(sub,k1), Z(sub), '--', 'Color', ColorOrder(k2, :));
    end
end

if length(legende) == 1
    legende = legende{1};
end

set(fig, 'CurrentAxes', h(1));
hold off; grid on;
legend(legende);
title(sprintf('%s\n%s %s', 'Temp (deg)', lat2str(INFO{1}.Lat), lon2str(INFO{1}.Lon)));
ylabel('Profondeur (m)')

set(fig, 'CurrentAxes', h(2));
hold off; grid on;
legend(legende);
title(sprintf('%s\n%s %s', 'Salinity (0/00)', lat2str(INFO{1}.Lat), lon2str(INFO{1}.Lon)));
ylabel('Profondeur (m)')

linkaxes(h, 'y')