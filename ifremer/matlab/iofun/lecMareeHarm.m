% Decodage d'un fichier d'harmonique de maree
%
% Syntax
%   [lat, lon, nom, coefs] = lecMareeHarm(nomFic)
%
% Input Arguments
%   nomFic : Nom du fichier d'harmoniques
%
% Output Arguments
%   lat   : Latitudes (deg)
%   lon   : Longitudes  (deg)
%   nom   : Noms des points
%   coefs : Coefficients
%
% Examples
%   nomFic = getNomFicDatabase('france.har');
%   [lat, lon, noms, coefs] = lecMareeHarm(nomFic);
%   figure; plot(lon, lat, '*');
%   noms
%
% See also Authors
% Authors : JMA
% VERSION  : $Id:$
%--------------------------------------------------------------------------------

function [lat, lon, nom, coefs] = lecMareeHarm(nomFic)

fid = fopen(nomFic);
i = 1;
lat = []; lon = []; nom = [];
while 1
    tline = fgetl(fid);
    if ~ischar(tline), break, end

    % disp(tline);
    mots = strsplit(tline);  % il aurait mieux valu constituer mots par saucissonnage en colonnes
    lat(i) = str2double(mots{1}) / 1000; %#ok<AGROW>
    lon(i) = str2double(mots{2}) / 1000; %#ok<AGROW>
    for k=1:11
        coefs(i,k) = str2double(mots{2 + k}); %#ok<AGROW>
    end

    tline = fgetl(fid);
    if ~ischar(tline), break, end
%     disp(tline);
    mots = strsplit( tline );
    switch length(mots)
        case 11
            nom{end+1} = 'Unknown'; %#ok<AGROW>
            for k=1:11
                coefs(i,k+11) = str2double(mots{k}); 
            end
        case 12
            nom{end+1} = mots{1}; %#ok<AGROW>
            for k=1:11
                coefs(i,k+11) = str2double(mots{1 + k}); 
            end
        case 13
            nom{end+1} =[mots{1} ' '  mots{2}]; %#ok<AGROW>
            for k=1:11
                coefs(i,k+11) = str2double(mots{2 + k}); 
            end
    end
    i = i + 1;
end
fclose(fid);