% Lecture du descripeur des fichiers ER-Mapper
%
% Syntax
%   DatasetHeader = lecParamsFicERMapper(nomFic)
%
% Input Arguments
%   nomFic : Nom du fichier
%
% Output Arguments
%   DatasetHeader : Parametres de l'image
%
% Examples
%    nomFic = '/home/doppler/tmsias/augustin/MRFormatFichier/R2_9925_sonarmosaic';
%    DatasetHeader = lecParamsFicERMapper([nomFic '.ers'])
%    DatasetHeader = lecParamsFicERMapper('/home/doppler/tmsias/augustin/MRFormatFichier/pppp')
%
% See also ecrParamsFicERMapper Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function DatasetHeader = lecParamsFicERMapper(nomFic)

Value = [];
DatasetHeader.RasterInfo.CellInfo.Xdimension = 1;
DatasetHeader.RasterInfo.CellInfo.Ydimension = 1;

fid = fopen(nomFic, 'r');
if fid == -1
    disp('Fichier non trouve');
    return
end

while feof(fid) == 0
    line = fgetl(fid);
    line = deblank(line);
    if isempty(line)
        continue
    end

    mots = strsplit(line);
    switch mots{1}
        case 'Version'
            DatasetHeader.Version = mots{3}(2:end-1);   % "6.0"
        case 'Name'
            DatasetHeader.Name = mots{3}(2:end-1);               % Nom du fichier
        case 'Description'
            DatasetHeader.Description = line(strfind(line, '=')+2:end);
        case 'LastUpdated'
            DatasetHeader.LastUpdated = line(strfind(line, '=')+2:end);
        case 'DataSetType'
            DatasetHeader.DataSetType = mots{3};        % ERStorage
        case 'DataType'
            DatasetHeader.DataType = mots{3};           % Raster
        case 'ByteOrder'
            DatasetHeader.ByteOrder = mots{3};          % LSBFirst


        case 'Datum'
            DatasetHeader.CoordinateSpace.Datum = mots{3}(2:end-1);         % "ED50"
        case 'Projection'
            DatasetHeader.CoordinateSpace.Projection = mots{3}(2:end-1);    % "NUTM31"
        case 'CoordinateType' % EN = Coordonnees metriques , RAW=pixels, LL=Geographiques
            DatasetHeader.CoordinateSpace.CoordinateType = mots{3};
        case 'Rotation' % XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
            DatasetHeader.CoordinateSpace.Rotation = mots{3};

        case 'CellType'
            switch mots{3}
                case 'IEEE4ByteReal'
                    DatasetHeader.RasterInfo.CellType = 'float';
                case 'IEEE8ByteReal'
                    DatasetHeader.RasterInfo.CellType = 'double';
                case 'Unsigned8BitInteger'
                    DatasetHeader.RasterInfo.CellType = 'uint8';
                otherwise
                    str = sprintf('%s non pris en compte',  mots{3});
                    my_warndlg(['iofun/lecParamsFicERMapper : ' str], 1);
            end

        case 'NullCellValue'
            DatasetHeader.RasterInfo.NullCellValue = str2double(mots{3});
        case 'Xdimension'
            DatasetHeader.RasterInfo.CellInfo.Xdimension = str2double(mots{3});
        case 'Ydimension'
            DatasetHeader.RasterInfo.CellInfo.Ydimension = str2double(mots{3});
        case 'NrOfLines'
            DatasetHeader.RasterInfo.NrOfLines = str2double(mots{3});
        case 'NrOfCellsPerLine'
            DatasetHeader.RasterInfo.NrOfCellsPerLine = str2double(mots{3});
        case 'Eastings'
            DatasetHeader.RasterInfo.RegistrationCoord.Eastings = str2double(mots{3});
        case 'Northings'
            DatasetHeader.RasterInfo.RegistrationCoord.Northings = str2double(mots{3});

        case 'NrOfBands'
            DatasetHeader.RasterInfo.NrOfBands = str2double(mots{3});

        case 'Value'
            Value{end+1} = line(strfind(line, '=')+3:end-1); %#ok<AGROW>
            DatasetHeader.RasterInfo.BandId.Value = Value;

        case 'Type'
            DatasetHeader.RasterInfo.RegionInfo.Type = mots{3};
        case 'RegionName'
            DatasetHeader.RasterInfo.RegionInfo.RegionName = mots{3}(2:end-1);
        case 'SourceDataset'
            DatasetHeader.RasterInfo.RegionInfo.SourceDataset = mots{3}(2:end-1);

        case 'Red'
            DatasetHeader.RasterInfo.RegionInfo.Red = str2double(mots{3});
        case 'Green'
            DatasetHeader.RasterInfo.RegionInfo.Green = str2double(mots{3});
        case 'Blue'
            DatasetHeader.RasterInfo.RegionInfo.Blue = str2double(mots{3});

        case 'SubRegion'
            line = fgetl(fid);
            line = deblank(line);
            DatasetHeader.RasterInfo.RegionInfo.SubRegion(1,:) = str2double(line);

            line = fgetl(fid);
            line = deblank(line);
            DatasetHeader.RasterInfo.RegionInfo.SubRegion(2,:) = str2double(line);

            line = fgetl(fid);
            line = deblank(line);
            DatasetHeader.RasterInfo.RegionInfo.SubRegion(3,:) = str2double(line);

            line = fgetl(fid);
            line = deblank(line);
            DatasetHeader.RasterInfo.RegionInfo.SubRegion(4,:) = str2double(line);



            %             case 'Rotation'
            %                 DatasetHeader.Rotation = mots{3};
            %             case 'Name'
            %                 DatasetHeader.Name = mots{3};
            %             case 'LastUpdated'
            %                 DatasetHeader.LastUpdated = mots{3};
            %             case 'SourceDataset'
            %                 DatasetHeader.SourceDataset = mots{3};
            %             case 'RGBcolour'
            %                 if strcmp(mots{2}, 'End')
            %                     continue
            %                 end
            %                 line = fgetl(fid);
            %                 line = deblank(line);
            %                 mots = strsplit(line);
            %                 DatasetHeader.RGBcolour.Red = str2double(mots{3});
            %
            %                 line = fgetl(fid);
            %                 line = deblank(line);
            %                 mots = strsplit(line);
            %                 DatasetHeader.RGBcolour.Green = str2double(mots{3});
            %
            %                 line = fgetl(fid);
            %                 line = deblank(line);
            %                 mots = strsplit(line);
            %                 DatasetHeader.RGBcolour.Blue = str2double(mots{3});

        case 'DatasetHeader'
        case 'CoordinateSpace'
        case 'RasterInfo'
        case 'CellInfo'
        case 'RegistrationCoord'
        case 'BandId'
        case 'RegionInfo'
        case 'RGBcolour'
        case '}'
        otherwise
            str = sprintf('Ligne non decodee :\n%s', line);
            my_warndlg(['iofun/lecParamsFicERMapper : ' str], 1);
            return
    end

end

% --------------------
% Fermeture du fichier

fclose(fid);
