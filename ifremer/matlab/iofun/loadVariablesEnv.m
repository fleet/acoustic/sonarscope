% Lecture d'un fichier d'initialisation des variables d'environnement.
%
% Syntax
%   loadReinitMatlab
%
% Input Arguments 
%   Sans objet
%
% Name-Value Pair Arguments 
%   Sans objet
%
% Output Arguments 
%   / (variables globales positionn�es)
% 
% Examples
%   loadVariablesEnv
%
% See also saveVariablesEnv Authors
% Authors : JMA, GLU
% -------------------------------------------------------------------------------

function loadVariablesEnv

global IfrTbx %#ok<GVMIS>
global IfrTbxResources %#ok<GVMIS>
global VisualisateurImageExterne %#ok<GVMIS> 
global Panoply %#ok<GVMIS> 
global Visualisateur3D %#ok<GVMIS> 
global SonarScopeDocumentation %#ok<GVMIS> 
global SonarScopeData %#ok<GVMIS> 
global IfrTbxLang %#ok<GVMIS> %#ok<GVMIS> 
global IfrTbxGUISelectImage %#ok<GVMIS> %#ok<GVMIS> 
global IfrTbxLevelQuestions %#ok<GVMIS> %#ok<GVMIS> 
global IfrTbxGraphicsOnTop %#ok<GVMIS> %#ok<GVMIS> 
global IfrTbxUseLogFile %#ok<GVMIS> %#ok<GVMIS> 
global IfrTbxUseClickRightForFlag %#ok<GVMIS> %#ok<GVMIS> 
global IfrTbxRepDefInput %#ok<GVMIS> 
global IfrTbxRepDefOutput %#ok<GVMIS> %#ok<GVMIS> 
global SizePhysTotalMemory %#ok<GVMIS> 
global IfrTbxTempDir %#ok<GVMIS> %#ok<GVMIS> 
global NUMBER_OF_PROCESSORS %#ok<GVMIS>
global UserAcceptSendingMsg %#ok<GVMIS> 
global UserEmailServerAddress %#ok<GVMIS> 
global UserEmailPortNumber %#ok<GVMIS> 

%% Ouverture  du fichier de configuration

if isdeployed
    nomDir = pwd;
    nomFicConf = fullfile(nomDir, 'SScTbxResources', 'Config.txt');
else
    nomFicConf = fullfile(IfrTbxResources, 'Config.txt');
end

%% Lecture du fichier de configuration

fid = fopen(nomFicConf, 'r');
if fid == -1
    str1 = sprintf('Fichier "%s" non trouv�', nomFicConf);
    str2 = sprintf('File "%s" not found', nomFicConf);
    my_warndlg(Lang(str1,str2), 1);
    return
end
L0 = [];
while feof(fid) == 0
    str = fgetl(fid);
    str = deblank(str);
    if isempty(str)
        continue
    end
    L0{end+1} = str; %#ok<AGROW>
end
fclose(fid);

%% Lecture des cl�s de registres-ressources de l'application.

try
    pppp = winqueryreg('name', 'HKEY_LOCAL_MACHINE', 'Software\Ifremer\SonarScope-Resource');
    for k=1:length(pppp)
        val = winqueryreg('HKEY_LOCAL_MACHINE', 'Software\Ifremer\SonarScope-Resource', pppp{k});
        switch pppp{k}
            case 'Doc'
                SonarScopeDocumentation = fullfile(val, 'AsciiDoctor');
            case 'SonarScopeData'
                SonarScopeData = val;
            case 'VisualisateurImageExterne1'
                VisualisateurImageExterne{1} = val;
            case 'VisualisateurImageExterne2'
                VisualisateurImageExterne{2} = val;
            case 'VisualisateurImageExterne3'
                VisualisateurImageExterne{3} = val;
            case 'Visualisateur3D'
                Visualisateur3D = val;
            case 'Panoply'
                Panoply = val;
        end
    end
catch ME %#ok<NASGU>
    % ME
    % Cas d'absence des cl�s de SonarScope dans le cas d'utilisation
    % d'autres utilitaires.
end

%% Traduction du contenu en variables d'env.

for k=1:length(L0)
    mots = strsplit(L0{k}, '=');
    if length(mots) ~= 1
        switch mots{1}
            case 'IfrTbx'
                IfrTbx = mots{2};
            case 'IfrTbxResources'
                if isdeployed
                    IfrTbxResources = fullfile(nomDir, 'SScTbxResources');
                else
                    IfrTbxResources = mots{2};
                end
            case 'IfrTbxLang'
                IfrTbxLang = mots{2}; % FR ou US
            case 'IfrTbxGUISelectImage'
                IfrTbxGUISelectImage = mots{2}; % Classical ou Simplified
            case 'IfrTbxTempDir'
                IfrTbxTempDir = mots{2};
            case 'IfrTbxUseClickRightForFlag'
                IfrTbxUseClickRightForFlag = mots{2};
            case 'IfrTbxLevelQuestions'
                IfrTbxLevelQuestions = mots{2}; % '1', '2' ou '3'
            case 'IfrTbxGraphicsOnTop'
                IfrTbxGraphicsOnTop = mots{2}; % '1', '2' ou '3'
            case 'IfrTbxUseLogFile'
                IfrTbxUseLogFile = mots{2};
            case 'IfrTbxRepDefInput'
                IfrTbxRepDefInput = mots{2};
            case 'IfrTbxRepDefOutput'
                IfrTbxRepDefOutput = mots{2};
            case 'SonarScopeData'
                SonarScopeData = mots{2};
            case 'VisualisateurImageExterne1'
                VisualisateurImageExterne{1} = mots{2};
            case 'VisualisateurImageExterne2'
                VisualisateurImageExterne{2} = mots{2};
            case 'VisualisateurImageExterne3'
                VisualisateurImageExterne{3} = mots{2};
            case 'Visualisateur3D'
                Visualisateur3D = mots{2};
            case 'Panoply'
                Panoply = mots{2};
            case 'SizePhysTotalMemory'
                SizePhysTotalMemory = str2double(mots{2});
            case 'NUMBER_OF_PROCESSORS'
                NUMBER_OF_PROCESSORS = str2double(mots{2});
            case 'AcceptSendingMsg'
                UserAcceptSendingMsg = str2double(mots{2});
            case 'EmailServerAddress'
                UserEmailServerAddress = mots{2};
            case 'EmailPortNumber'
                UserEmailPortNumber = str2double(mots{2});
        end
    end
end
