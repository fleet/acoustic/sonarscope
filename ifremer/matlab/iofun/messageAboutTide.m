function messageAboutTide(Tag)

switch Tag
    case 'LIBEX' % test� sur ExLibex.tid
    case 'CIDCO' % test� sur ExCidco.tid
%     case 'UnivGent'
    case 'Simrad' % V�rifi� pour sous-cas SIMRAD2
%     case 'Vasquez'
%     case 'Caris'
%     case 'CaraibesFromHar'
%     case 'UnknownOrigin'
    case 'Caraibes' % V�rifi� sur ExCaraibes.tide
    case 'NIWA' % V�rifi� sur .all EM2040 et fichier fourni par le SHOM
    otherwise
        str = sprintf('The Tide import was a mess before this release. I tried to put everything in order but I had not data to check if it is OK for "%s" file format.\n\nPlease send an email to sonarscope@ifremer.fr to tell me if the tide is correctly applied (precise "%s" format).\n\nOnce you have validated the tide on your data I will remove this message for this specific tide file format (%s).\n\nI would appreciate if you could send me an example of this tide file.', Tag, Tag, Tag);
        my_warndlg(str, 1);
end

