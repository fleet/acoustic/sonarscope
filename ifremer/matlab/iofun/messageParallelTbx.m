% flag = messageParallelTbx(0)
% flag = messageParallelTbx(1)

function flag = messageParallelTbx(X, varargin)

[varargin, Potentially] = getPropertyValue(varargin, 'Potentially', 0); %#ok<ASGLU>

if ~X
    flag = 1;
    return
end

useParallel = get_UseParallelTbx;
if useParallel
    flag = 1;
    return
end

if Potentially
    str1 = sprintf('Cette op�ration pourrait utiliser la ParallelToolbox. Son activation se fait maintenant via le bouton dont l''ic�ne est constitu�e de 4 barres parall�les.\n\n');
    str2 = sprintf('This processing could use the parallel toolbox for file checking. The //Tbx activation is activated using the button whose icon is made of 4 parallel bars.\n\n');
else
    str1 = sprintf('Il est possible d''utiliser la ParallelToolbox pour cette op�ration. Son activation se fait maintenant via le bouton dont l''ic�ne est constitu�e de 4 barres parall�les.\n\n');
    str2 = sprintf('It is possible to use the parallel toolbox to do this processing. Its activation is activated using the button whose icon is made of 4 parallel bars.\n\n');
end
str = Lang(str1,str2);

% if useParallel
%     nbProc = str2double(getenv('NUMBER_OF_PROCESSORS'));
%     str1 = sprintf('\n\nuseParallel est actuellement positionn� sur "On" et le nombre de workers est de %d', nbProc);
%     str2 = sprintf('\n\nuseParallel is set to "On" and the number of workers is %d', nbProc);
% else
    str1 = sprintf('\n\nuseParallel est actuellement positionn� sur "Off"\n\n');
    str2 = sprintf('\n\nuseParallel is set to "Off"\n\n');
% end
str = [Lang(str1,str2) str];

str1 = sprintf('Voulez-vous poursuivre ?');
str2 = sprintf('Do you want to continue ?');
str = [str Lang(str1,str2)];

[rep, flag] = my_questdlg(str);
if ~flag
    return
end
if rep == 2
    flag = 0;
end
