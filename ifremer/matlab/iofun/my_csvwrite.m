function flag = my_csvwrite(nomFic, C)

%% Comma or not comma ?

sep = getCSVseparator;

%% Ecriture du fichier

flag = 0;
fid = fopen(nomFic, 'w+');
if fid == -1
    return
end
for k1=1:size(C,1)
    for k2=1:size(C,2)
        V = C{k1,k2};
        switch class(V)
            case 'char'
                fprintf(fid, '%s%s ', V, sep);
            case {'single'; 'double'}
                fprintf(fid, '%s%s ', num2strPrecis(V), sep);
            otherwise
                whos V
        end
    end
    fprintf(fid, '\n');
end
fclose(fid);
flag = 1;
