function flag = my_deleteFile(filename)

if exist(filename, 'file')
    delete(filename);
    if exist(filename, 'file')
        flag = 0;
    else
        flag = 1;
    end
else
    flag = 1;
end
