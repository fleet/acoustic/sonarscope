% Effacement de balises HTML
%
% Syntax
%   str = my_deleteTagHTML(str)
%
% Input Arguments
%   str : cha�ne de caract�res comportant des balises HTML.
%
% Name-Value Pair Arguments
%
% OUTPUT PARAMATERS :
%  str : cha�ne de caract�res expurg�es des balises HTML.
%
% Examples
%     str = '<html><body bgcolor="#FF8080" text="#FF0000"><body width="110px"><DIV ALIGN="CENTER">2013-09-21 15:24:13'
%     str = my_deleteTagHTML(str);
%
% See also summary Authors
% Authors : GLU
% ----------------------------------------------------------------------------
function str = my_deleteTagHTML(str)
    str = regexprep(str, '<[^>]*>', '');
