function status = my_fclose(fid)

if fid ~= 1
    status = fclose(fid);
else
    status = -1;
end
