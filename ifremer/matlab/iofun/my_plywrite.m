% my_plywrite(filename, faces, verts)
% Will write a face vertex mesh data in ply format.
% faces -> polygonal descriptions in terms of vertex indices
% verts -> list of vertex coordinate triplets
% faces and verts can be obtained by using the MATLAB isosurface function.
%
% my_plywrite(filename, faces, verts, rgb)
% Will add color information.
% rgb -> optional list of RGB triplets per vertex
%
%   See also: plywrite plyread
%
% TODO : Coder le stockage de faces

% TODO : débrancher la sélection multiple dans ALL.Params.ExportPLY quand l'export multivariable
% sera branché ici



function flag = my_plywrite(filename, faces, verts, varargin) %#ok<INUSL>

% TODO : Coder le stockage de faces
faces = [];

[varargin, identFormat] = getPropertyValue(varargin, 'Format', 2); %#ok<ASGLU>

flag = 0;

switch identFormat 
    case 1 % ASCII
        codeFormat = ascii;
    case 2 % Binary
        codeFormat = 'binary_little_endian';
    otherwise
        return
end
    
% Create File
fileID = fopen(filename, 'w+');
if fileID == -1
    messageErreurFichier(filename, 'WriteFailure');
    return
end

[nbVerts, nbRows] = size(verts);
% Insert Header
fprintf(fileID, ...
    ['ply\n', ...
    'format %s 1.0\n', ...
    'element vertex %u\n', ...
    'property float64 x\n', ...
    'property float64 y\n', ...
    'property float64 z\n'], codeFormat, nbVerts);
for k=1:(nbRows-3)
    fprintf(fileID, ...
    'property float64 intensity\n');
end
fprintf(fileID, ...
    ['element face %u\n', ...
    'property list uint8 int32 vertex_indices\n', ...
    'end_header\n'], ...
    length(faces));

if identFormat == 1 % ASCII
    for k=1:nbVerts
        fprintf(fileID, '%.6f %.6f %.6f ', verts(k,1), verts(k,2), verts(k,3));
        fprintf(fileID, '%.6f ', verts(k,4:end));
        fprintf(fileID, '\n');
    end
else
	count = fwrite(fileID, verts', 'double');
    if count ~= numel(verts)
        messageErreurFichier(filename, 'WriteFailure');
    end
end
fclose(fileID);

flag = 1;
