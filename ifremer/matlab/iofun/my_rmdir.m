function flag = my_rmdir(filename)

if exist(filename, 'dir')
    rmdir(filename, 's')
    if exist(filename, 'dir')
        flag = 0;
    else
        flag = 1;
    end
else
    flag = 1;
end
