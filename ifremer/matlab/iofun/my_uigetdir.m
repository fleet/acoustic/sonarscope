% Description
%     Descriptif g�n�ral de la fonction (texte qui appara�tra dans le "Current Directory" de l'environnement matlab)
%     Compl�ment de la description (n'appara�tra pas dans le "Current Directory")
%  
%   Syntax
%     [flag, selpath] = my_uigetdir(...)
%
%   Optional Input Arguments
%     initPath : Default folder
%     title    : Title of the window
%  
%   Output Arguments
%     flag    : 0=KO, 1=OK
%     selpath : Selected path
%  
%   Examples
%     [flag, nomDir] = my_uigetdir
%     [flag, nomDir] = my_uigetdir(my_tempdir)
%     [flag, nomDir] = my_uigetdir(my_tempdir, 'Select any folder for this demonstration please')
%  
%   Authors  : JMA
%   See also : uigetdir
%  -------------------------------------------------------------------------------

function [flag, nomDir] = my_uigetdir(varargin)

% Conservation du handle de fen�tre active.
GCF = get(0, 'CurrentFigure');

drawnow
pause(0.1)

if (nargin > 1) && isequal(varargin{1}, 0)
    varargin{1} = my_tempdir;
end
nomDir = uigetdir(varargin{:});
if isequal(nomDir, 0)
    flag = 0;
    nomDir = [];
    return
else
    flag = 1;
end

% Restitution du handle � la fen�tre active.
if ~isempty(GCF)
    figure(GCF);
end
drawnow
pause(0.1)
%{
% Solution de Yair ALTMAN 
uiwait(msgbox(...));
drawnow; pause(0.05);  % this innocent line prevents the Matlab hang 
answer = questdlg(...);
drawnow; pause(0.05);  % this innocent line prevents the Matlab hangswitch answer
%}
warning_blankInFileName(nomDir)