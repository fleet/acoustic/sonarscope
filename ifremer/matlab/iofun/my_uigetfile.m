% D�signation d'un ou plusieurs fichiers.
% Compl�te my_uigetfile pour corriger un bug dans la version native de MatLab
% (perte de focus de la fen�tre).
%
% Syntax
%    [flag, fullFilename] = my_uigetfile(...)
% 
% Input Arguments
%   varargin : param�tres commun � la fonction my_uigetfile de Matlab.
%
% Output Arguments
%   flag         : 0=KO, 1=OK
%   fullFilename : full file name
%
% Examples
%    [flag, xmlFile] = my_uigetfile('*.xml', 'Please select the XML file');
%    if flag
%    	winopen(xmlFile)
%    end
%
%    [flag, filename] = my_uigetfile({'*.txt', 'List file (*.txt)'; ...
%             '*.*', 'All Files (*.*)'}, ...
%             'Pick a file', my_tempdir)
%
%
%    [flag, filename] = my_uigetfile({'*.txt', 'List file (*.txt)'; ...
%             '*.*', 'All Files (*.*)'}, ...
%             'Pick a file', my_tempdir, 'MultiSelect','on')
%
% See also my_uiputfile 
% Authors : GLU
% ----------------------------------------------------------------------------

function [flag, fullFilename] = my_uigetfile(varargin)

% Conservation du handle de fen�tre active.
GCF = get(0, 'CurrentFigure');
drawnow
pause(0.5)

[filename, pathname, filterindex] = uigetfile(varargin{:});
if isequal(filename, 0) || isequal(pathname, 0)
    flag = 0;
    fullFilename = [];
else
    flag = 1;
    fullFilename = fullfile(pathname, filename);
end

% Restitution du handle � la fen�tre active.
if ~isempty(GCF)
    figure(GCF);
end
drawnow
pause(0.5)

%{
% Solution de Yair ALTMAN 
uiwait(msgbox(...));
drawnow; pause(0.05);  % this innocent line prevents the Matlab hang 
answer = questdlg(...);
drawnow; pause(0.05);  % this innocent line prevents the Matlab hangswitch answer
%}

warning_blankInFileName(fullfile(pathname, [filename filterindex]))
