% Sauvegarde d'un ou plusieurs fichiers.
% Compl�te my_uiputfile pour corriger un bug dans la version native de MatLab
% (perte de focus de la fen�tre).
%
% Syntax
%    [flag, filename] = my_uiputfile({'*.txt', 'List file (*.txt)'; ...
%             '*.*',                   'All Files (*.*)'}, ...
%             'Pick a file', ...
%             this.RepDef)
%
% Input Arguments
%   varargin : param�tres commun � la fonction my_uiputfile de MatLaB.
%
% Output Arguments
%   filename    : nom du(des) fichier(s)
%   pathname    : r�pertoire des fichiers
%   filterindex : index des fichiers en sortie
%
% Examples
%     [flag, filename] = my_uiputfile( ...
%         {'*.txt', 'List file (*.txt)'; ...
%         '*.*',    'All Files (*.*)'}, ...
%         'Save as', my_tempdir);
%
% See also my_uiputfile
% Authors : GLU
% ----------------------------------------------------------------------------

function [flag, filename] = my_uiputfile(varargin)

[varargin, checkWrite] = getPropertyValue(varargin, 'checkWrite', 0);

pause(1) % BUG SINON EN R2012b : �a ne pr�sente pas la fen�tre dans certaines circonstances !!!!!!!!!!!!!!!!!!
drawnow

% Conservation du handle de fen�tre active pour pouvoir redonner le focus apr�s
GCF = get(0, 'CurrentFigure');

% pause(1) % BUG SINON EN R2012b : �a ne pr�sente pas la fen�tre dans certaines circonstances !!!!!!!!!!!!!!!!!!
[filename, pathname] = uiputfile(varargin{:});
pause(0.5)

% Restitution du handle � la fen�tre active.
if ~isempty(GCF)
    figure(GCF);
end

if isequal(filename, 0) && isequal(pathname, 0)
    flag = 0;
    filename = [];
    return
else
    flag = 1;
end

filename = fullfile(pathname, filename);

pause(0.5);
drawnow
pause(0.5);
%{
% Solution de Yair ALTMAN
uiwait(msgbox(...));
drawnow; pause(0.05);  % this innocent line prevents the Matlab hang
answer = questdlg(...);
drawnow; pause(0.05);  % this innocent line prevents the Matlab hangswitch answer
%}

if checkWrite
    flag = testEcritureFichier(filename);
    if ~flag
        filename = [];
        return
    end
end

warning_blankInFileName(filename)
