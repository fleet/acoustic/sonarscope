function [flag, mes] = my_xlswrite(nomFic, varargin)

[flag, mes] = xlswrite(nomFic, varargin{:});
if ~flag
    flag = my_wk1write(nomFic, varargin{:});
end
