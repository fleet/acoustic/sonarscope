% Inspection d'un fichier NetCDF
%
% Syntax
%   ncEdit(nomFic)
%
% Input Arguments
%   nomFic : Nom du fichier
%
% Examples
%   nomFic = getNomFicDatabase('EM12D_PRISMED_500m.mnt');
%   ncEdit(nomFic)
%
% See also cl_netcdf Authors
% Authors : JMA
%-------------------------------------------------------------------------

function ncEdit(varargin)

global Panoply %#ok<GVMIS> 

if nargin == 1
    prompt = {'Do a copy of this string. You will have to paste it in the Panoply GUI:'};
    dlg_title = 'Name of the NetCDF file';
    num_lines = 1;
    nomFic = varargin{1};
    defaultans = {nomFic};
    answer = inputdlg(prompt, dlg_title, num_lines, defaultans); %#ok<NASGU>
end

dos(Panoply);
