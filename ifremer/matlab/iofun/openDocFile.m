% Ouverture d'un fichier de bureautique
%
% Syntax
%   openDocFile(nomFic)
% 
% Input Arguments
%   NomFic : Nom du fichier de documentation
%
% Examples 
%   openDocFile('C:\SonarScopeDoc\AsciiDoctor\SScDoc-Head.html')
%
% See also cl_image/openDocFile Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function openDocFile(nomFic)

if ~exist(nomFic, 'file')
    str1 = 'TODO';
    str2 = sprintf('%s does not exist', nomFic);
    my_warndlg(Lang(str1,str2), 1);
    return
end

[~, ~, ext] = fileparts(nomFic);
if strcmp(ext, '.htm') || strcmp(ext, '.html')
    my_web(nomFic)
    return
end

winopen(nomFic)
