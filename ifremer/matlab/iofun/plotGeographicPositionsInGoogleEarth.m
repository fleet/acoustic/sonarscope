% nomFicIn  = 'D:\Temp\Ridha\Zones.txt';
% nomFicOut = 'D:\Temp\Ridha\LePrecieux.kml';
% flag = plotGeographicPositionsInGoogleEarth(nomFicIn, nomFicOut);

function flag = plotGeographicPositionsInGoogleEarth(nomFicIn, nomFicOut, Labels2Plot, strColor, identColor)

if ischar(nomFicIn)
    nomFicIn = {nomFicIn};
end

%% Traitement par fichiers si plusieurs

nbFic = length(nomFicIn);
for k=1:nbFic
    if k == 1
        nomFic = nomFicOut;
    else
        [nomDir, nomFic, Ext] = fileparts(nomFicOut);
        nomFic = fullfile(nomDir, [nomFic, '(' num2str(k) ')', Ext]) ;
    end
    flag = plotGeographicPositionsInGoogleEarth_unitaire(nomFicIn{k}, nomFic, Labels2Plot, strColor, identColor);

    % TODO : export Globe

end

function flag = plotGeographicPositionsInGoogleEarth_unitaire(nomFicIn, nomFicOut, Labels2Plot, strColor, identColor)

flag = 0;
[~, Titre] = fileparts(nomFicOut);

%% Lecture du fichier

T = readtable(nomFicIn);
% T(1:5,:)

Names = T.(1);
Lat   = T.(2);
Lon   = T.(3);

if ~isempty(Labels2Plot)
    sub = find(ismember(T.(4), Labels2Plot));
    Names = Names(sub);
    Lat   = Lat(sub);
    Lon   = Lon(sub);
end

if isempty(Lat)
    str1 = sprintf('Le fichier "%s" n''a pas �t� correctement d�cod�.', nomFicIn);
    str2 = sprintf('File "%s" could not be read.', nomFicIn);
    my_warndlg(Lang(str1,str2), 1);
    return
end

%% Trac� de la figure

FigUtils.createSScFigure; PlotUtils.createSScPlot(Lon, Lat, '*'); grid on;
xlabel('Lon (deg)'); ylabel('Lat (deg)'); title(Titre, 'Interpreter','none');

%% Recherche et lecture du fichier KML d'exemple

nomFicKml = getNomFicDatabase('ExamplePinsGoogleEarth.kml');
if isempty(nomFicKml)
    str1 = 'Le fichier d''exemple "ExamplePinsGoogleEarth.kml" n''a pas �t� trouv�, contactez JMA.';
    str2 = '"ExamplePinsGoogleEarth.kml" could not be found, please call JMA.';
    my_warndlg(Lang(str1,str2), 1);
    return
end
tree = xml_read(nomFicKml);

%{
    http://maps.google.com/mapfiles/kml/pushpin/ylw-pushpin.png
    http://maps.google.com/mapfiles/kml/pushpin/blue-pushpin.png
    http://maps.google.com/mapfiles/kml/pushpin/grn-pushpin.png
    http://maps.google.com/mapfiles/kml/pushpin/ltblu-pushpin.png
    http://maps.google.com/mapfiles/kml/pushpin/pink-pushpin.png
    http://maps.google.com/mapfiles/kml/pushpin/purple-pushpin.png
    http://maps.google.com/mapfiles/kml/pushpin/red-pushpin.png
    http://maps.google.com/mapfiles/kml/pushpin/wht-pushpin.png
%}

for k=1:length(strColor)
    k1 = 2*k - 1;
    k2 = 2*k;
    
    tree.Document.StyleMap(k) = tree.Document.StyleMap(1);
    tree.Document.StyleMap(k).Pair(1).styleUrl = ['#s_' strColor{k} '-pushpin'];
    tree.Document.StyleMap(k).Pair(2).styleUrl = ['#s_' strColor{k} '-pushpin_hl'];
    tree.Document.StyleMap(k).ATTRIBUTE.id   = ['m_' strColor{k} '-pushpin'];
   
    tree.Document.Style(k1) = tree.Document.Style(1);
    tree.Document.Style(k1).IconStyle.Icon = ['http://maps.google.com/mapfiles/kml/pushpin/' strColor{k} '-pushpin.png'];
    tree.Document.Style(k1).ATTRIBUTE.id   = ['s_' strColor{k} '-pushpin_hl'];
    tree.Document.Style(k2) = tree.Document.Style(2);
    tree.Document.Style(k2).IconStyle.Icon = ['http://maps.google.com/mapfiles/kml/pushpin/' strColor{k} '-pushpin.png'];
    tree.Document.Style(k2).ATTRIBUTE.id   = ['s_' strColor{k} '-pushpin'];
end

%% Adaptation de la structure KML aux donn�es

sub = 1:length(Lat);
nbPins = length(sub);
tree.Document.Folder.name = T.Properties.VariableNames{1};
for k=1:nbPins
    tree.Document.Folder.Placemark(k) = tree.Document.Folder.Placemark(1);
    tree.Document.Folder.Placemark(k).name = Names(k);    
    tree.Document.Folder.Placemark(k).LookAt.longitude = Lon(k);
    tree.Document.Folder.Placemark(k).LookAt.latitude  = Lat(k);
    tree.Document.Folder.Placemark(k).Point.coordinates = sprintf('%s,%s,0', num2str(Lon(k),16), num2str(Lat(k),16));
%     tree.Document.Folder.Placemark(k).styleUrl = tree.Document.Folder.Placemark(k).styleUrl;
    tree.Document.Folder.Placemark(k).styleUrl = ['#m_' strColor{identColor} '-pushpin'];
end
tree.Document.Folder.Placemark(nbPins+1:end) = [];
tree.Document.name = Titre;

%% Ecriture du fichier KML de sortie

DPref.StructItem = false;
DPref.CellItem   = false;
xml_write(nomFicOut, tree, 'kml', DPref);

%% Affichage dans Google Earth

winopen(nomFicOut)

%% Ite

flag = 1;
