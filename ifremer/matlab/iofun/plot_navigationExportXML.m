function flag = plot_navigationExportXML(Lat, Lon, Time, Color, Name, Label, nomFicXml, varargin)

[varargin, Immersion] = getPropertyValue(varargin, 'Immersion', []);
[varargin, Heading]   = getPropertyValue(varargin, 'Heading',   []);
[varargin, Roll]      = getPropertyValue(varargin, 'Roll',      []);
[varargin, Pitch]     = getPropertyValue(varargin, 'Pitch',     []);
[varargin, Heave]     = getPropertyValue(varargin, 'Heave',     []); %#ok<ASGLU>

[nomDirXml, nomFic] = fileparts(nomFicXml);

%% G�n�ration du XML SonarScope

Nav.Software = 'SonarScope';
Nav.DataTag  = 'SurveyNavigation';
Nav.Name     = nomFic;

nbLines = length(Name);
Nav.Dimensions.nbLines = nbLines;

% Nav.Names = Name;

for k=1:nbLines
    if ~isempty(Name{k})
        nomDirLine = fullfile(nomDirXml , nomFic);
        if isempty(Immersion)
            ValImmersion = [];
        else
            ValImmersion = Immersion{k};
        end
        if isempty(Heading)
            ValHeading = [];
        else
            ValHeading = Heading{k};
        end
        if isempty(Roll)
            ValRoll = [];
        else
            ValRoll = Roll{k};
        end
        if isempty(Pitch)
            ValPitch = [];
        else
            ValPitch = Pitch{k};
        end
        if isempty(Heave)
            ValHeave = [];
        else
            ValHeave = Heave{k};
        end
        
        [flag, Struct] = get_Line(Lat{k}, Lon{k}, Time{k}, Color{k}, Name{k}, Label{k}, nomDirLine, nomFic, ...
            ValImmersion, ValHeading, ValRoll, ValPitch, ValHeave);
        if ~flag
            return
        end
        Nav.Line(k) = Struct;
    end
end

%% Cr�ation du fichier XML d�crivant la donn�e

DPref.ItemName  = 'item';
DPref.StructItem = false;
DPref.CellItem   = true;
xml_write(nomFicXml, Nav, 'Nav', DPref);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end
flag = 1;


function [flag, Line] = get_Line(Lat, Lon, Time, Color, Name, Label, nomDirLine, nomFicXML, ...
    ValImmersion, ValHeading, ValRoll, ValPitch, ValHeave)

% iLine = sprintf('%03d', iLine);
X = juliandate(Time);
NAV.Date      = floor(X);
NAV.Hour      = mod(X,1) * (24*3600000);
NAV.Longitude = Lon;
NAV.Latitude  = Lat;
[~, filename] = fileparts(Name);

Line.ExtractedFrom  = Name;
Line.Name = filename;
if isempty(Label)
    Line.StartTime = [];
    Line.EndTime   = [];
else  
    Line.StartTime = char(min(Time));
    Line.EndTime   = char(max(Time));
end
Line.Color = Color;

Line.Dimensions.nbPoints = length(Lat);

Line.Signals(1).Name          = 'Date';
Line.Signals(end).Dimensions  = 'nbPoints, 1';
Line.Signals(end).Storage     = 'double';
Line.Signals(end).Unit        = 'Julian Day';
Line.Signals(end).FileName    = fullfile(nomFicXML, filename, 'Date.bin');

Line.Signals(end+1).Name      = 'Hour';
Line.Signals(end).Dimensions  = 'nbPoints, 1';
Line.Signals(end).Storage     = 'double';
Line.Signals(end).Unit        = 'nb millis / midnight';
Line.Signals(end).FileName    = fullfile(nomFicXML, filename, 'Hour.bin');

Line.Signals(end+1).Name      = 'Longitude';
Line.Signals(end).Dimensions  = 'nbPoints, 1';
Line.Signals(end).Storage     = 'double';
Line.Signals(end).Unit        = 'deg';
Line.Signals(end).FileName    = fullfile(nomFicXML, filename, 'Longitude.bin');

Line.Signals(end+1).Name      = 'Latitude';
Line.Signals(end).Dimensions  = 'nbPoints, 1';
Line.Signals(end).Storage     = 'double';
Line.Signals(end).Unit        = 'deg';
Line.Signals(end).FileName    = fullfile(nomFicXML, filename, 'Latitude.bin');

% if ~isempty(ValImmersion)
%     Line.Signals(end+1).Name      = 'Immersion';
%     Line.Signals(end).Dimensions  = 'nbPoints, 1';
%     Line.Signals(end).Storage     = 'single';
%     Line.Signals(end).Unit        = 'm';
%     Line.Signals(end).FileName    = fullfile(nomFicXML, filename, 'Immersion.bin');
%     NAV.Immersion = ValImmersion;
% end

if ~isempty(ValImmersion)
    Line.Signals(end+1).Name      = 'Depth';
    Line.Signals(end).Dimensions  = 'nbPoints, 1';
    Line.Signals(end).Storage     = 'single';
    Line.Signals(end).Unit        = 'm';
    Line.Signals(end).FileName    = fullfile(nomFicXML, filename, 'Depth.bin');
    NAV.Depth = ValImmersion;
end

if ~isempty(ValHeading)
    Line.Signals(end+1).Name      = 'Heading';
    Line.Signals(end).Dimensions  = 'nbPoints, 1';
    Line.Signals(end).Storage     = 'single';
    Line.Signals(end).Unit        = 'deg';
    Line.Signals(end).FileName    = fullfile(nomFicXML, filename, 'Heading.bin');
    NAV.Heading = ValHeading;
end

if ~isempty(ValRoll)
    Line.Signals(end+1).Name      = 'Roll';
    Line.Signals(end).Dimensions  = 'nbPoints, 1';
    Line.Signals(end).Storage     = 'single';
    Line.Signals(end).Unit        = 'deg';
    Line.Signals(end).FileName    = fullfile(nomFicXML, filename, 'Roll.bin');
    NAV.Roll = ValRoll;
end

if ~isempty(ValPitch)
    Line.Signals(end+1).Name      = 'Pitch';
    Line.Signals(end).Dimensions  = 'nbPoints, 1';
    Line.Signals(end).Storage     = 'single';
    Line.Signals(end).Unit        = 'deg';
    Line.Signals(end).FileName    = fullfile(nomFicXML, filename, 'Pitch.bin');
    NAV.Pitch = ValPitch;
end

if ~isempty(ValHeave)
    Line.Signals(end+1).Name      = 'Heave';
    Line.Signals(end).Dimensions  = 'nbPoints, 1';
    Line.Signals(end).Storage     = 'single';
    Line.Signals(end).Unit        = 'm';
    Line.Signals(end).FileName    = fullfile(nomFicXML, filename, 'Heave.bin');
    NAV.Heave = ValHeave;
end

%% Cr�ation du r�pertoire Line

if ~exist(nomDirLine, 'dir')
    status = mkdir(nomDirLine);
    if ~status
        messageErreur(nomDirLine)
        flag = 0;
        return
    end
end

%% Cr�ation des r�pertoires sp�cifiques pour chaque ligne

nomDirLine = fileparts(nomDirLine);
for k=1:length(Line.Signals)
    nomDir = fileparts(Line.Signals(k).FileName);
    nomDir = fullfile(nomDirLine, nomDir);
    if ~exist(nomDir, 'dir')
        status = mkdir(nomDir);
        if ~status
            messageErreur(nomDir)
            flag = 0;
            return
        end
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:length(Line.Signals)
    flag = writeSignal(nomDirLine, Line.Signals(k), NAV.(Line.Signals(k).Name));
    if ~flag
        return
    end
end

flag = 1;
