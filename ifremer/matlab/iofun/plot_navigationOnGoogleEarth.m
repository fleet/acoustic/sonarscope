function plot_navigationOnGoogleEarth(Lat, Lon, Color, Name, Label, nomFicGE, varargin)

[varargin, flagDisplay]     = getPropertyValue(varargin, 'Display',         1);
[varargin, PinsVisibility]  = getPropertyValue(varargin, 'PinsVisibility',  1);
[varargin, LatPatchSwath]   = getPropertyValue(varargin, 'LatPatchSwath',   []);
[varargin, LonPatchSwath]   = getPropertyValue(varargin, 'LonPatchSwath',   []);
[varargin, LatPatchOther]   = getPropertyValue(varargin, 'LatPatchOther',   []);
[varargin, LonPatchOther]   = getPropertyValue(varargin, 'LonPatchOther',   []);
[varargin, LabelPatchOther] = getPropertyValue(varargin, 'LabelPatchOther', []);
[varargin, FaceColor]       = getPropertyValue(varargin, 'FaceColor',       []); %#ok<ASGLU>

if ~iscell(Lat)
    Lat   = {Lat};
    Lon   = {Lon};
    Color = {Color};
    Name  = {Name};
    Label  = {Label};
end

[nomDir, NomDocument, ext] = fileparts(nomFicGE);
if ~strcmp(ext, '.kml')
    [nomDir, NomDocument, ~] = fileparts(nomFicGE);
end
nomFicKml = fullfile(nomDir, [NomDocument '.kml']);
nomFicKmzZip = my_tempname;

for k=1:length(Color)
    C = floor(255*Color{k});
    CodeCouleur{k} = sprintf('%02x', fliplr(C)); %#ok<AGROW>
end
for k=1:length(FaceColor)
    C = floor(255*FaceColor{k});
    CodeCouleurPatchOther{k} = sprintf('%02x', fliplr(C)); %#ok<AGROW>
end

%% Cr�ation du fichier

fid = fopen(nomFicKml, 'wt+');
fprintf(fid, '<?xml version="1.0" encoding="UTF-8"?>\n');
% fprintf(fid, '<kml xmlns="http://earth.google.com/kml/2.0">\n');
% fprintf(fid, 'xmlns:gx="http://www.google.com/kml/ext/2.2">   <!-- required when using gx-prefixed elements -->\n');

fprintf(fid, '<kml xmlns="http://www.opengis.net/kml/2.2" xmlns:gx="http://www.google.com/kml/ext/2.2" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom">\n');

fprintf(fid, '<Document>\n');
fprintf(fid, '  <name>%s</name>\n', NomDocument);

%% D�finition des styles pour la navigation

for k=1:length(Color)
    fprintf(fid, '  <StyleMap id="map_style_%d">\n', k);
    fprintf(fid, '    <Pair>\n');
    fprintf(fid, '      <key>normal</key>\n');
    fprintf(fid, '      <styleUrl>#style_pushpin_normal_%d</styleUrl>\n', k);
    fprintf(fid, '    </Pair>\n');
    fprintf(fid, '    <Pair>\n');
    fprintf(fid, '      <key>highlight</key>\n');
    fprintf(fid, '      <styleUrl>#style_pushpin_highlight_%d</styleUrl>\n', k);
    fprintf(fid, '    </Pair>\n');
    fprintf(fid, '  </StyleMap>\n');
    
    fprintf(fid, '  <Style id="style_pushpin_highlight_%d">\n',k);
    fprintf(fid, '    <IconStyle>\n');
    fprintf(fid, '      <color>ff%s</color>\n', CodeCouleur{k});
    fprintf(fid, '      <scale>1.3</scale>\n');
    fprintf(fid, '      <Icon>\n');
    fprintf(fid, '        <href>http://maps.google.com/mapfiles/kml/pushpin/ylw-pushpin.png</href>\n');
    fprintf(fid, '      </Icon>\n');
    fprintf(fid, '      <hotSpot x="20" y="2" xunits="pixels" yunits="pixels"/>\n');
    fprintf(fid, '    </IconStyle>\n');
    fprintf(fid, '    <LineStyle>\n');
    fprintf(fid, '      <color>ff%s</color>\n', CodeCouleur{k});
    fprintf(fid, '      <width>3</width>\n');
    fprintf(fid, '    </LineStyle>\n');
    fprintf(fid, '    <PolyStyle>\n');
    fprintf(fid, '      <color>ff%s</color>\n', CodeCouleur{k});
    % fprintf(fid, '      <width>2.9</width>\n');
    % fprintf(fid, '      <colorMode>random</colorMode>\n');
    fprintf(fid, '    </PolyStyle>\n');
    fprintf(fid, '  </Style>\n');
    
    fprintf(fid, '  <Style id="style_pushpin_normal_%d">\n',k);
    fprintf(fid, '    <IconStyle>\n');
    % fprintf(fid, '      <color>ff%s</color>\n', CodeCouleur{k});
    fprintf(fid, '      <scale>1.1</scale>\n');
    fprintf(fid, '      <Icon>\n');
    fprintf(fid, '        <href>http://maps.google.com/mapfiles/kml/pushpin/ylw-pushpin.png</href>\n');
    fprintf(fid, '      </Icon>\n');
    fprintf(fid, '      <hotSpot x="20" y="2" xunits="pixels" yunits="pixels"/>\n');
    fprintf(fid, '    </IconStyle>\n');
    fprintf(fid, '    <LineStyle>\n');
    fprintf(fid, '      <color>ff%s</color>\n', CodeCouleur{k});
    fprintf(fid, '      <width>1</width>\n');
    fprintf(fid, '    </LineStyle>\n');
    fprintf(fid, '    <PolyStyle>\n');
    fprintf(fid, '      <color>ff%s</color>\n', CodeCouleur{k});
    % fprintf(fid, '      <width>2.9</width>\n');
    % fprintf(fid, '      <colorMode>random</colorMode>\n');
    fprintf(fid, '    </PolyStyle>\n');
    fprintf(fid, '  </Style>\n');
end

%% D�finition des styles pour les patchs "others"

for k=1:length(FaceColor)
    fprintf(fid, '  <StyleMap id="Patch_map_style_%d">\n', k);
    fprintf(fid, '    <Pair>\n');
    fprintf(fid, '      <key>normal</key>\n');
    fprintf(fid, '      <styleUrl>#Patch_style_pushpin_normal_%d</styleUrl>\n', k);
    fprintf(fid, '    </Pair>\n');
    fprintf(fid, '    <Pair>\n');
    fprintf(fid, '      <key>highlight</key>\n');
    fprintf(fid, '      <styleUrl>#Patch_style_pushpin_highlight_%d</styleUrl>\n', k);
    fprintf(fid, '    </Pair>\n');
    fprintf(fid, '  </StyleMap>\n');
    
    fprintf(fid, '  <Style id="Patch_style_pushpin_highlight_%d">\n',k);
    fprintf(fid, '    <IconStyle>\n');
    fprintf(fid, '      <color>ff%s</color>\n', CodeCouleurPatchOther{k});
    fprintf(fid, '      <scale>1.3</scale>\n');
    fprintf(fid, '      <Icon>\n');
    fprintf(fid, '        <href>http://maps.google.com/mapfiles/kml/pushpin/ylw-pushpin.png</href>\n');
    fprintf(fid, '      </Icon>\n');
    fprintf(fid, '      <hotSpot x="20" y="2" xunits="pixels" yunits="pixels"/>\n');
    fprintf(fid, '    </IconStyle>\n');
    fprintf(fid, '    <LineStyle>\n');
    fprintf(fid, '      <color>ff%s</color>\n', CodeCouleurPatchOther{k});
    fprintf(fid, '      <width>3</width>\n');
    fprintf(fid, '    </LineStyle>\n');
    fprintf(fid, '    <PolyStyle>\n');
    fprintf(fid, '      <color>ff%s</color>\n', CodeCouleurPatchOther{k});
    fprintf(fid, '    </PolyStyle>\n');
    fprintf(fid, '  </Style>\n');
    
    fprintf(fid, '  <Style id="Patch_style_pushpin_normal_%d">\n',k);
    fprintf(fid, '    <IconStyle>\n');
    % fprintf(fid, '      <color>ff%s</color>\n', CodeCouleurPatchOther{k});
    fprintf(fid, '      <scale>1.1</scale>\n');
    fprintf(fid, '      <Icon>\n');
    fprintf(fid, '        <href>http://maps.google.com/mapfiles/kml/pushpin/ylw-pushpin.png</href>\n');
    fprintf(fid, '      </Icon>\n');
    fprintf(fid, '      <hotSpot x="20" y="2" xunits="pixels" yunits="pixels"/>\n');
    fprintf(fid, '    </IconStyle>\n');
    fprintf(fid, '    <LineStyle>\n');
    fprintf(fid, '      <color>ff%s</color>\n', CodeCouleurPatchOther{k});
    fprintf(fid, '      <width>1</width>\n');
    fprintf(fid, '    </LineStyle>\n');
    fprintf(fid, '    <PolyStyle>\n');
    fprintf(fid, '      <color>59%s</color>\n', CodeCouleurPatchOther{k});
    fprintf(fid, '    </PolyStyle>\n');
    fprintf(fid, '  </Style>\n');
end

%% Trac� des punaises

% fprintf(fid, '<Folder>\n');
% fprintf(fid, '  <name>Navigation</name>\n');
fprintf(fid, '<Folder>\n');
fprintf(fid, '  <name>Tags</name>\n');
for k=1:length(Lat)
    if isempty(Lat{k}) || isempty(Lon{k})
        continue
    end
    d = [Lon{k}(:) Lat{k}(:)];
    indMilieu = floor(length(Lon{k})/2);
    if indMilieu == 0
        m = 1;
    else
        m = d(indMilieu, :);
    end
    fprintf(fid, '  <Placemark>\n');
    fprintf(fid, '      <name>" %s"</name>\n', Name{k});
    if ~PinsVisibility
        fprintf(fid, '      <visibility>0</visibility>\n');
    end
    fprintf(fid, '      <description>" %s "</description>\n', Label{k});
    fprintf(fid, '      <styleUrl>#map_style_%d</styleUrl>\n', k);
    fprintf(fid, '      <Point>\n');
    fprintf(fid, '          <altitudeMode>relativeToSeaFloor</altitudeMode>\n');
    fprintf(fid, '          <gx:altitudeMode>relativeToSeaFloor</gx:altitudeMode>\n');
    fprintf(fid, '          <coordinates>\n');
    fprintf(fid, '              %.6f, %.6f, 0.0 \n', m');
    fprintf(fid, '          </coordinates>\n');
    fprintf(fid, '      </Point>\n');
    fprintf(fid, '  </Placemark>\n');
end
fprintf(fid, '</Folder>\n');

%% Trac� des couvertures

if ~isempty(LatPatchSwath)
    fprintf(fid, '<Folder>\n');
    fprintf(fid, '  <name>Swath</name>\n');
    for k=1:length(Lat)
        d = [LonPatchSwath{k}(:) LatPatchSwath{k}(:)];
        fprintf(fid, '  <Placemark>\n');
        fprintf(fid, '      <name>" %s "</name>\n', Name{k});
        fprintf(fid, '      <description>" %s "</description>\n', Label{k});
        fprintf(fid, '      <styleUrl>#map_style_%d</styleUrl>\n', k);
        fprintf(fid, '      <Polygon>\n');
        fprintf(fid, '          <extrude>0</extrude>\n');
        fprintf(fid, '          <altitudeMode>relativeToGround</altitudeMode>\n');
        fprintf(fid, '          <outerBoundaryIs>\n');
        fprintf(fid, '              <LinearRing>\n');
        %     fprintf(fid, '          <tessellate>1</tessellate>\n');
        fprintf(fid, '          <altitudeMode>relativeToSeaFloor</altitudeMode>\n');
        fprintf(fid, '          <gx:altitudeMode>relativeToSeaFloor</gx:altitudeMode>\n');
        fprintf(fid, '                  <coordinates>\n');
        
        fprintf(fid, '                      %.6f,%.6f,0.0 \n', d');
        
        fprintf(fid, '                  </coordinates>\n');
        fprintf(fid, '              </LinearRing>\n');
        fprintf(fid, '          </outerBoundaryIs>\n');
        fprintf(fid, '      </Polygon>\n');
        fprintf(fid, '  </Placemark>\n');
    end
    fprintf(fid, '</Folder>\n');
end

%% Trac� de la navigation � la surface

fprintf(fid, '<Folder>\n');
fprintf(fid, '  <name>Navigation Surface</name>\n');
for k=1:length(Lat)
    d = [Lon{k}(:) Lat{k}(:)];
    fprintf(fid, '  <Placemark>\n');
    fprintf(fid, '      <name>" %s "</name>\n', Name{k});
    fprintf(fid, '      <visibility>0</visibility>\n');
    fprintf(fid, '      <description>" %s "</description>\n', Label{k});
    fprintf(fid, '      <styleUrl>#map_style_%d</styleUrl>\n', k);
    fprintf(fid, '      <LineString>\n');
    %     fprintf(fid, '          <altitudeMode>relativeToGround</altitudeMode>\n');
    %     fprintf(fid, '          <tessellate>1</tessellate>\n');
    %     fprintf(fid, '          <altitudeMode>relativeToGround</altitudeMode>\n');
    fprintf(fid, '          <altitudeMode>absolute</altitudeMode>\n');
    %     fprintf(fid, '          <gx:altitudeMode>relativeToSeaFloor</gx:altitudeMode>\n');
    fprintf(fid, '          <coordinates>\n');
    if size(d, 1) >= 1e4
        fprintf(fid, '              %.6f,%.6f,0.0 \n', d(1:10:end, :)');
    else
        fprintf(fid, '              %.6f,%.6f,0.0 \n', d');
    end
    fprintf(fid, '          </coordinates>\n');
    fprintf(fid, '      </LineString>\n');
    fprintf(fid, '  </Placemark>\n');
end
fprintf(fid, '</Folder>\n');


%% Trac� de la navigation sur le fond

fprintf(fid, '<Folder>\n');
fprintf(fid, '  <name>Navigation Seafloor</name>\n');
for k=1:length(Lat)
    fprintf(fid, '  <Placemark>\n');
    fprintf(fid, '      <name>" %s "</name>\n', Name{k});
    fprintf(fid, '      <description>" %s "</description>\n', Label{k});
    fprintf(fid, '      <styleUrl>#map_style_%d</styleUrl>\n', k);
    fprintf(fid, '      <LineString>\n');
    
    fprintf(fid, '          <gx:altitudeMode>relativeToSeaFloor</gx:altitudeMode>\n');
    % GLU, le 11/01/2013 : pb constat� sur la version Google Earth 7.0.1 avec les fichiers
    % Simon Stevin.
    % Altitude forc�e � 5 m au-dessus du fond pour forcer la superposition
    % des lignes de navigation au-dessus du fond.
%     relHeight = ones(length(Lat{k}),1) * 5;
    % Annul� par JMA le 20/09/2018 car la nav fond se retrouvait largement
    % au dessus du fond (et de la nav surface) pour Shallow Survey 2018,
    % Area1_2
    
    relHeight = ones(length(Lat{k}),1) * 0.5;
    fprintf(fid, '          <coordinates>\n');
    
    % FigUtils.createSScFigure; PlotUtils.createSScPlot(Lon{k}(:), Lat{k}(:)); grid on
    d = [Lon{k}(:) Lat{k}(:) relHeight];
%     if size(d, 1) >= 1e4
        N = size(d, 1);
        step = ceil(N / 5e4);
        if step > 1
            fprintf('The navigation of %s was undersampled by a factor of %d\n', Name{k}, step);
        end
        d = d(1:step:end, :);
%     end
    fprintf(fid, '              %.6f,%.6f,%f \n', d');
    fprintf(fid, '          </coordinates>\n');
    fprintf(fid, '      </LineString>\n');
    fprintf(fid, '  </Placemark>\n');
end
fprintf(fid, '</Folder>\n');
% fprintf(fid, '</Folder>\n');

%% Trac� des punaises des Patchs "Others"

if ~isempty(LatPatchOther)
    fprintf(fid, '<Folder>\n');
    fprintf(fid, '  <name>Others</name>\n');
    fprintf(fid, '<Folder>\n');
    fprintf(fid, '  <name>Patchs</name>\n');
    fprintf(fid, '<Folder>\n');
    fprintf(fid, '  <name>Tags</name>\n');
    for k=1:length(LatPatchOther)
        d = [LonPatchOther{k}(:) LatPatchOther{k}(:)];
        m = d(floor(length(LonPatchOther{k})/2), :);
        fprintf(fid, '  <Placemark>\n');
        fprintf(fid, '      <name>" %s"</name>\n', LabelPatchOther{k});
        fprintf(fid, '      <description>" %s "</description>\n', LabelPatchOther{k});
        fprintf(fid, '      <styleUrl>#Patch_map_style_%d</styleUrl>\n', k);
        fprintf(fid, '      <Point>\n');
        fprintf(fid, '          <altitudeMode>relativeToGround</altitudeMode>\n');
        fprintf(fid, '          <gx:altitudeMode>relativeToSeaFloor</gx:altitudeMode>\n');
        fprintf(fid, '          <coordinates>\n');
        fprintf(fid, '              %.6f,%.6f,0.0 \n', m');
        fprintf(fid, '          </coordinates>\n');
        fprintf(fid, '      </Point>\n');
        fprintf(fid, '  </Placemark>\n');
    end
    fprintf(fid, '</Folder>\n');
    
    
    %% Trac� des Patchs "Others"
    
    fprintf(fid, '<Folder>\n');
    fprintf(fid, '  <name>Patchs</name>\n');
    for k=1:length(LatPatchOther)
        d = [LonPatchOther{k}(:) LatPatchOther{k}(:)];
        fprintf(fid, '  <Placemark>\n');
        fprintf(fid, '      <name>" %s "</name>\n', LabelPatchOther{k});
        fprintf(fid, '      <description>" %s "</description>\n', LabelPatchOther{k});
        fprintf(fid, '      <styleUrl>#Patch_map_style_%d</styleUrl>\n', k);
        fprintf(fid, '      <Polygon>\n');
        fprintf(fid, '          <tessellate>1</tessellate>\n');
        %     fprintf(fid, '          <extrude>0</extrude>\n');
        %     fprintf(fid, '          <altitudeMode>relativeToGround</altitudeMode>\n');
        fprintf(fid, '          <outerBoundaryIs>\n');
        fprintf(fid, '              <LinearRing>\n');
        fprintf(fid, '          <altitudeMode>relativeToGround</altitudeMode>\n');
        fprintf(fid, '          <gx:altitudeMode>relativeToSeaFloor</gx:altitudeMode>\n');
        fprintf(fid, '                  <coordinates>\n');
        
        fprintf(fid, '                      %.6f,%.6f,0.0 \n', d');
        
        fprintf(fid, '                  </coordinates>\n');
        fprintf(fid, '              </LinearRing>\n');
        fprintf(fid, '          </outerBoundaryIs>\n');
        fprintf(fid, '      </Polygon>\n');
        fprintf(fid, '  </Placemark>\n');
    end
    fprintf(fid, '</Folder>\n');
    fprintf(fid, '</Folder>\n');
    fprintf(fid, '</Folder>\n');
end

%% Fermeture du fichier

fprintf(fid, '</Document>\n');
fprintf(fid, '</kml>\n');
fclose(fid);

%% Conversion du .kml en .kmz

if strcmp(ext, '.kmz')
    
    % TODO : Tester zipKml2kmz
    % flag = zipKml2kmz(nomFicKmz, DirTemp)

    zip(nomFicKmzZip, nomFicKml)
    flag = copyfile([nomFicKmzZip, '.zip'], nomFicGE);
    if ~flag
        str = sprintf('Impossible to copy file %s to %s.', [nomFicGE, '.zip'], nomFicGE);
        my_warndlg(str, 1);
        return
    end
    delete([nomFicKmzZip, '.zip'])
    W = warning;
    warning('Off')
    delete(nomFicKml)
    warning(W)
else
    nomFicGE = nomFicKml;
end


%% Visualisation du .kmz dans Google-Earth

if flagDisplay
    winopen(nomFicGE)
end


%% Equivalent en utilisant xml_write mais �a ne marche pas

% Nav.description = ['"' nomFicKmz '"'];
% Nav.LineString.tessellate = 1;
% Nav.LineString.coordinates = [Lat(:) Lon(:) zeros(length(Lat), 1)];
%
% DPref.ItemName  = 'item';
% DPref.StructItem = false;
% DPref.CellItem   = false;
% xml_write(nomFicKmz, Nav, 'Placemark', DPref);
