function plot_navigationOnNasaWorldWind(Lat, Lon, Color, Name, Label, nomFicKmz, varargin)

% Tout reste � faire

%{
<?xml version="1.0" encoding="UTF-8" ?>
<LayerSet Name="LineFeature test" ShowOnlyOneLayer="false" ShowAtStartup="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="LayerSet.xsd">
<LineFeature ShowAtStartup="true">
<Name>LF Simple</Name>
<LineWidth>3.0</LineWidth>
<LineString>
<posList>
0,0 -10,10 -20,0 0,0
</posList>
</LineString>
<FeatureColor>
<Red>0</Red>
<Green>255</Green>
<Blue>0</Blue>
</FeatureColor>
<AltitudeMode>ClampedToGround</AltitudeMode>
</LineFeature>
</LayerSet>


%}

my_warndlg('iofun/plot_navigationOnNasaWorldWind not finished, sorry', 1);

[varargin, LatPatchSwath]   = getPropertyValue(varargin, 'LatPatchSwath', []);
[varargin, LonPatchSwath]   = getPropertyValue(varargin, 'LonPatchSwath', []);
[varargin, LatPatchOther]   = getPropertyValue(varargin, 'LatPatchOther', []);
[varargin, LonPatchOther]   = getPropertyValue(varargin, 'LonPatchOther', []);
[varargin, LabelPatchOther] = getPropertyValue(varargin, 'LabelPatchOther', []);
[varargin, FaceColor]       = getPropertyValue(varargin, 'FaceColor', []); %#ok<ASGLU>

[nomDir, NomDocument] = fileparts(nomFicKmz);
nomFicKml = fullfile(nomDir, [NomDocument '.kml']);
nomFicKmzZip = my_tempname;

for i=1:length(Color)
    C = floor(255*Color{i});
    CodeCouleur{i} = sprintf('%02x', fliplr(C)); %#ok<AGROW>
end
for i=1:length(FaceColor)
    C = floor(255*FaceColor{i});
    CodeCouleurPatchOther{i} = sprintf('%02x', fliplr(C)); %#ok<AGROW>
end

%% Cr�ation du fichier

fid = fopen(nomFicKml, 'wt');
fprintf(fid, '<?xml version="1.0" encoding="UTF-8"?>\n');
fprintf(fid, '<kml xmlns="http://earth.google.com/kml/2.0">\n');
fprintf(fid, '<Document>\n');
fprintf(fid, '  <name>%s</name>\n', NomDocument);

%% D�finition des styles pour la navigation

for i=1:length(Color)
    fprintf(fid, '  <StyleMap id="map_style_%d">\n', i);
    fprintf(fid, '    <Pair>\n');
    fprintf(fid, '      <key>normal</key>\n');
    fprintf(fid, '      <styleUrl>#style_pushpin_normal_%d</styleUrl>\n', i);
    fprintf(fid, '    </Pair>\n');
    fprintf(fid, '    <Pair>\n');
    fprintf(fid, '      <key>highlight</key>\n');
    fprintf(fid, '      <styleUrl>#style_pushpin_highlight_%d</styleUrl>\n', i);
    fprintf(fid, '    </Pair>\n');
    fprintf(fid, '  </StyleMap>\n');
    
    fprintf(fid, '  <Style id="style_pushpin_highlight_%d">\n',i);
    fprintf(fid, '    <IconStyle>\n');
    fprintf(fid, '      <color>ff%s</color>\n', CodeCouleur{i});
    fprintf(fid, '      <scale>1.3</scale>\n');
    fprintf(fid, '      <Icon>\n');
    fprintf(fid, '        <href>http://maps.google.com/mapfiles/kml/pushpin/ylw-pushpin.png</href>\n');
    fprintf(fid, '      </Icon>\n');
    fprintf(fid, '      <hotSpot x="20" y="2" xunits="pixels" yunits="pixels"/>\n');
    fprintf(fid, '    </IconStyle>\n');
    fprintf(fid, '    <LineStyle>\n');
    fprintf(fid, '      <color>ff%s</color>\n', CodeCouleur{i});
    fprintf(fid, '      <width>3</width>\n');
    fprintf(fid, '    </LineStyle>\n');
    fprintf(fid, '    <PolyStyle>\n');
    fprintf(fid, '      <color>ff%s</color>\n', CodeCouleur{i});
    % fprintf(fid, '      <width>2.9</width>\n');
    % fprintf(fid, '      <colorMode>random</colorMode>\n');
    fprintf(fid, '    </PolyStyle>\n');
    fprintf(fid, '  </Style>\n');
    
    fprintf(fid, '  <Style id="style_pushpin_normal_%d">\n',i);
    fprintf(fid, '    <IconStyle>\n');
    % fprintf(fid, '      <color>ff%s</color>\n', CodeCouleur{i});
    fprintf(fid, '      <scale>1.1</scale>\n');
    fprintf(fid, '      <Icon>\n');
    fprintf(fid, '        <href>http://maps.google.com/mapfiles/kml/pushpin/ylw-pushpin.png</href>\n');
    fprintf(fid, '      </Icon>\n');
    fprintf(fid, '      <hotSpot x="20" y="2" xunits="pixels" yunits="pixels"/>\n');
    fprintf(fid, '    </IconStyle>\n');
    fprintf(fid, '    <LineStyle>\n');
    fprintf(fid, '      <color>ff%s</color>\n', CodeCouleur{i});
    fprintf(fid, '      <width>1</width>\n');
    fprintf(fid, '    </LineStyle>\n');
    fprintf(fid, '    <PolyStyle>\n');
    fprintf(fid, '      <color>ff%s</color>\n', CodeCouleur{i});
    % fprintf(fid, '      <width>2.9</width>\n');
    % fprintf(fid, '      <colorMode>random</colorMode>\n');
    fprintf(fid, '    </PolyStyle>\n');
    fprintf(fid, '  </Style>\n');
end

%% D�finition des styles pour les patchs "others"

for i=1:length(FaceColor)
    fprintf(fid, '  <StyleMap id="Patch_map_style_%d">\n', i);
    fprintf(fid, '    <Pair>\n');
    fprintf(fid, '      <key>normal</key>\n');
    fprintf(fid, '      <styleUrl>#Patch_style_pushpin_normal_%d</styleUrl>\n', i);
    fprintf(fid, '    </Pair>\n');
    fprintf(fid, '    <Pair>\n');
    fprintf(fid, '      <key>highlight</key>\n');
    fprintf(fid, '      <styleUrl>#Patch_style_pushpin_highlight_%d</styleUrl>\n', i);
    fprintf(fid, '    </Pair>\n');
    fprintf(fid, '  </StyleMap>\n');
    
    fprintf(fid, '  <Style id="Patch_style_pushpin_highlight_%d">\n',i);
    fprintf(fid, '    <IconStyle>\n');
    fprintf(fid, '      <color>ff%s</color>\n', CodeCouleurPatchOther{i});
    fprintf(fid, '      <scale>1.3</scale>\n');
    fprintf(fid, '      <Icon>\n');
    fprintf(fid, '        <href>http://maps.google.com/mapfiles/kml/pushpin/ylw-pushpin.png</href>\n');
    fprintf(fid, '      </Icon>\n');
    fprintf(fid, '      <hotSpot x="20" y="2" xunits="pixels" yunits="pixels"/>\n');
    fprintf(fid, '    </IconStyle>\n');
    fprintf(fid, '    <LineStyle>\n');
    fprintf(fid, '      <color>ff%s</color>\n', CodeCouleurPatchOther{i});
    fprintf(fid, '      <width>3</width>\n');
    fprintf(fid, '    </LineStyle>\n');
    fprintf(fid, '    <PolyStyle>\n');
    fprintf(fid, '      <color>ff%s</color>\n', CodeCouleurPatchOther{i});
    fprintf(fid, '    </PolyStyle>\n');
    fprintf(fid, '  </Style>\n');
    
    fprintf(fid, '  <Style id="Patch_style_pushpin_normal_%d">\n',i);
    fprintf(fid, '    <IconStyle>\n');
    % fprintf(fid, '      <color>ff%s</color>\n', CodeCouleurPatchOther{i});
    fprintf(fid, '      <scale>1.1</scale>\n');
    fprintf(fid, '      <Icon>\n');
    fprintf(fid, '        <href>http://maps.google.com/mapfiles/kml/pushpin/ylw-pushpin.png</href>\n');
    fprintf(fid, '      </Icon>\n');
    fprintf(fid, '      <hotSpot x="20" y="2" xunits="pixels" yunits="pixels"/>\n');
    fprintf(fid, '    </IconStyle>\n');
    fprintf(fid, '    <LineStyle>\n');
    fprintf(fid, '      <color>ff%s</color>\n', CodeCouleurPatchOther{i});
    fprintf(fid, '      <width>1</width>\n');
    fprintf(fid, '    </LineStyle>\n');
    fprintf(fid, '    <PolyStyle>\n');
    fprintf(fid, '      <color>59%s</color>\n', CodeCouleurPatchOther{i});
    fprintf(fid, '    </PolyStyle>\n');
    fprintf(fid, '  </Style>\n');
end

%% Trac� des punaises

fprintf(fid, '<Folder>\n');
fprintf(fid, '  <name>Navigation</name>\n');
fprintf(fid, '<Folder>\n');
fprintf(fid, '  <name>Tags</name>\n');
for i=1:length(Lat)
    if isempty(Lat{i}) || isempty(Lon{i})
        continue
    end
    d = [Lon{i}(:) Lat{i}(:)];
    m = d(floor(length(Lon{i})/2), :);
    fprintf(fid, '  <Placemark>\n');
    fprintf(fid, '      <name>" %s"</name>\n', Name{i});
    fprintf(fid, '      <description>" %s "</description>\n', Label{i});
    fprintf(fid, '      <styleUrl>#map_style_%d</styleUrl>\n', i);
    fprintf(fid, '      <Point>\n');
    fprintf(fid, '          <coordinates>\n');
    fprintf(fid, '              %.6f, %.6f, 0.0 \n', m');
    fprintf(fid, '          </coordinates>\n');
    fprintf(fid, '      </Point>\n');
    fprintf(fid, '  </Placemark>\n');
end
fprintf(fid, '</Folder>\n');

%% Trac� des couvertures

if ~isempty(LatPatchSwath)
    fprintf(fid, '<Folder>\n');
    fprintf(fid, '  <name>Swath</name>\n');
    for i=1:length(Lat)
        d = [LonPatchSwath{i}(:) LatPatchSwath{i}(:)];
        fprintf(fid, '  <Placemark>\n');
        fprintf(fid, '      <name>" %s "</name>\n', Name{i});
        fprintf(fid, '      <description>" %s "</description>\n', Label{i});
        fprintf(fid, '      <styleUrl>#map_style_%d</styleUrl>\n', i);
        fprintf(fid, '      <Polygon>\n');
        fprintf(fid, '          <extrude>0</extrude>\n');
        fprintf(fid, '          <altitudeMode>relativeToGround</altitudeMode>\n');
        fprintf(fid, '          <outerBoundaryIs>\n');
        fprintf(fid, '              <LinearRing>\n');
        %     fprintf(fid, '          <tessellate>1</tessellate>\n');
        fprintf(fid, '                  <coordinates>\n');
        
        fprintf(fid, '                      %.6f, %.6f, 0.0 \n', d');
        
        fprintf(fid, '                  </coordinates>\n');
        fprintf(fid, '              </LinearRing>\n');
        fprintf(fid, '          </outerBoundaryIs>\n');
        fprintf(fid, '      </Polygon>\n');
        fprintf(fid, '  </Placemark>\n');
    end
    fprintf(fid, '</Folder>\n');
end

%% Trac� de la navigation

fprintf(fid, '<Folder>\n');
fprintf(fid, '  <name>Navigation</name>\n');
for i=1:length(Lat)
    d = [Lon{i}(:) Lat{i}(:)];
    fprintf(fid, '  <Placemark>\n');
    fprintf(fid, '      <name>" %s "</name>\n', Name{i});
    fprintf(fid, '      <description>" %s "</description>\n', Label{i});
    
    fprintf(fid, '      <styleUrl>#map_style_%d</styleUrl>\n', i);
    
    fprintf(fid, '      <LineString>\n');
    fprintf(fid, '          <tessellate>1</tessellate>\n');
    fprintf(fid, '          <coordinates>\n');
    fprintf(fid, '              %.6f, %.6f, 0.0 \n', d');
    fprintf(fid, '          </coordinates>\n');
    fprintf(fid, '      </LineString>\n');
    fprintf(fid, '  </Placemark>\n');
end
fprintf(fid, '</Folder>\n');
fprintf(fid, '</Folder>\n');

%% Trac� des punaises des Patchs "Others"

if ~isempty(LatPatchOther)
    fprintf(fid, '<Folder>\n');
    fprintf(fid, '  <name>Others</name>\n');
    fprintf(fid, '<Folder>\n');
    fprintf(fid, '  <name>Patchs</name>\n');
    fprintf(fid, '<Folder>\n');
    fprintf(fid, '  <name>Tags</name>\n');
    for i=1:length(LatPatchOther)
        d = [LonPatchOther{i}(:) LatPatchOther{i}(:)];
        m = d(floor(length(LonPatchOther{i})/2), :);
        fprintf(fid, '  <Placemark>\n');
        fprintf(fid, '      <name>" %s"</name>\n', LabelPatchOther{i});
        fprintf(fid, '      <description>" %s "</description>\n', LabelPatchOther{i});
        fprintf(fid, '      <styleUrl>#Patch_map_style_%d</styleUrl>\n', i);
        fprintf(fid, '      <Point>\n');
        fprintf(fid, '          <coordinates>\n');
        fprintf(fid, '              %.6f, %.6f, 0.0 \n', m');
        fprintf(fid, '          </coordinates>\n');
        fprintf(fid, '      </Point>\n');
        fprintf(fid, '  </Placemark>\n');
    end
    fprintf(fid, '</Folder>\n');
    
    
    %% Trac� des Patchs "Others"
    
    fprintf(fid, '<Folder>\n');
    fprintf(fid, '  <name>Patchs</name>\n');
    for i=1:length(LatPatchOther)
        d = [LonPatchOther{i}(:) LatPatchOther{i}(:)];
        fprintf(fid, '  <Placemark>\n');
        fprintf(fid, '      <name>" %s "</name>\n', LabelPatchOther{i});
        fprintf(fid, '      <description>" %s "</description>\n', LabelPatchOther{i});
        fprintf(fid, '      <styleUrl>#Patch_map_style_%d</styleUrl>\n', i);
        fprintf(fid, '      <Polygon>\n');
        fprintf(fid, '          <tessellate>1</tessellate>\n');
        %     fprintf(fid, '          <extrude>0</extrude>\n');
        %     fprintf(fid, '          <altitudeMode>relativeToGround</altitudeMode>\n');
        fprintf(fid, '          <outerBoundaryIs>\n');
        fprintf(fid, '              <LinearRing>\n');
        
        fprintf(fid, '                  <coordinates>\n');
        
        fprintf(fid, '                      %.6f, %.6f, 0.0 \n', d');
        
        fprintf(fid, '                  </coordinates>\n');
        fprintf(fid, '              </LinearRing>\n');
        fprintf(fid, '          </outerBoundaryIs>\n');
        fprintf(fid, '      </Polygon>\n');
        fprintf(fid, '  </Placemark>\n');
    end
    fprintf(fid, '</Folder>\n');
    fprintf(fid, '</Folder>\n');
    fprintf(fid, '</Folder>\n');
end

%% Fermeture du fichier

fprintf(fid, '</Document>\n');
fprintf(fid, '</kml>\n');
fclose(fid);

%% Conversion du .kml en .kmz

    % TODO : Tester zipKml2kmz
    % flag = zipKml2kmz(nomFicKmz, DirTemp)

zip(nomFicKmzZip, nomFicKml)
flag = copyfile([nomFicKmzZip, '.zip'], nomFicKmz);
if ~flag
    str = sprintf('Impossible to copy file %s to %s.', [nomFicKmz, '.zip'], nomFicKmz);
    my_warndlg(str, 1);
    return
end
delete([nomFicKmzZip, '.zip'])
delete(nomFicKml)

%% Visualisation du .kmz dans Google-Earth

winopen(nomFicKmz)


%% Equivalent en utilisant xml_write mais �a ne marche pas

% Nav.description = ['"' nomFicKmz '"'];
% Nav.LineString.tessellate = 1;
% Nav.LineString.coordinates = [Lat(:) Lon(:) zeros(length(Lat), 1)];
%
% DPref.ItemName  = 'item';
% DPref.StructItem = false;
% DPref.CellItem   = false;
% xml_write(nomFicKmz, Nav, 'Placemark', DPref);
