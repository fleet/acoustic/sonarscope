function [flag, TideRTK, flagRTK, RTKGeoid, UseRTK] = processRTK(HeightRTK, nomDir, Longitude, Latitude, varargin)

persistent GeoideShomAtlantique GeoideShomMediterranee GeoideBelgique persistent_valueGeoid

[varargin, RTKGeoid] = getPropertyValue(varargin, 'RTKGeoid', []);
[varargin, UseRTK]   = getPropertyValue(varargin, 'UseRTK',   []); %#ok<ASGLU>

if isempty(UseRTK) || ~UseRTK
    flagRTK = [];
    UseRTK  = 0;
    flag    = 1;
    return
end

if isempty(RTKGeoid)
    RTKGeoid.Type          = 0;
    RTKGeoid.SIS.lonGeoid  = []; % RTKGeoid.Type = 'SIS'
    RTKGeoid.SIS.latGeoid  = [];
    RTKGeoid.SIS.fGeoid    = [];
    RTKGeoid.SIS.gGeoid    = [];
    RTKGeoid.SHOM.lonGeoid = []; % RTKGeoid.Type = 'SHOM'
    RTKGeoid.SHOM.latGeoid = [];
    RTKGeoid.SHOM.fgGeoid  = [];
end

%% Lecture du g�o�de de Belgique

if isempty(GeoideBelgique)
    nomFicGeoide = getNomFicDatabase('GeoSep_Belgium_LatLong_Bathymetry.ers');
    if exist(nomFicGeoide, 'file')
        [~, GeoideBelgique] = cl_image.import_ermapper(nomFicGeoide);
    end
end

%% Lecture du g�o�de du SHOM r�gion Atlantique

if isempty(GeoideShomAtlantique)
    nomFicGeoide = getNomFicDatabase('ZH_ell_BATHYELLI_atlantique_v1_2013_glhi_LatLong_Bathymetry.ers');
    if ~isempty(nomFicGeoide)
        [~, GeoideShomAtlantique] = cl_image.import_ermapper(nomFicGeoide);
    end
end

%% Lecture du g�o�de du SHOM r�gion M�diterran�e

if isempty(GeoideShomMediterranee)
    nomFicGeoide = getNomFicDatabase('ZH_ell_BATHYELLI_mediterranee_v1_2013_glhi_LatLong_Bathymetry.ers');
    if ~isempty(nomFicGeoide)
        [~, GeoideShomMediterranee] = cl_image.import_ermapper(nomFicGeoide);
    end
end

%% Test traitement de la donn�e avec les geoides du SHOM

[flag, TideRTK, flagRTK, UseRTK] = processRTK_SHOM(HeightRTK, GeoideShomAtlantique, GeoideShomMediterranee, GeoideBelgique, Longitude, Latitude, UseRTK);
if flag
    RTKGeoid.Type          = 'SHOM';    % Rajout� le 03/03/2014 pour Geoswath  Utile ???
    RTKGeoid.SHOM.lonGeoid = Longitude; % Rajout� le 03/03/2014
    RTKGeoid.SHOM.latGeoid = Latitude;  % Rajout� le 03/03/2014
    RTKGeoid.SHOM.fgGeoid  = TideRTK;   % Rajout� le 03/03/2014
    return
end

%% Recherche du fichier de description du geo�de d�livr� par SIS

nomFicGeoid = fullfile(nomDir, 'geoidmodel.geoid');
nomDirGeoid = nomDir;
RechercheGeoid = true;
while RechercheGeoid
    
    %% Test si on a d�j� identifi� que le g�o�de est d�crit par le fichier "geoidmodel.geoid" d�livr� par SIS
    
    if strcmp(RTKGeoid.Type, 'SIS')
        [flag, TideRTK, flagRTK, UseRTK] = processRTK_SIS(HeightRTK, RTKGeoid.SIS, Longitude, Latitude, UseRTK);
        return
    end
    
    %% Recherche d'un fichier "geoidmodel.geoid"
    
    [flag, lonGeoid, latGeoid, fGeoid, gGeoid] = geoidmodel_read(nomFicGeoid);
    if flag % Cas o� le fichier geoidmodel.geoid a �t� trouv� sur le r�pertoire des .all
        RTKGeoid.Type         = 'SIS';
        RTKGeoid.SIS.lonGeoid = lonGeoid;
        RTKGeoid.SIS.latGeoid = latGeoid;
        RTKGeoid.SIS.fGeoid   = fGeoid;
        RTKGeoid.SIS.gGeoid   = gGeoid;
        [flag, TideRTK, flagRTK, UseRTK] = processRTK_SIS(HeightRTK, RTKGeoid.SIS, Longitude, Latitude, UseRTK);
        if ~flag
            return
        end
        RechercheGeoid = false;
    else % Cas o� le fichier geoidmodel.geoid n'a �t� trouv� sur le r�pertoire des .all
        if ~isempty(UseRTK) && UseRTK
            str1 = 'Vos donn�es se pr�tent th�oriquement � une correction mar�graphique bas�e sur les donn�es RTK. Malheureusement le fichier "geoidmodel.geoid" n''a pas �t� trouv� dans le r�pertoire des .all. Que voulez-vous faire ?';
            str2 = 'Your dataset could be processed with RTK data. Unfortunately the file "geoidmodel.geoid" was not found in the directory of .all files. What do you want to do ?';
            str3 = 'Le rechercher';
            str4 = 'Find it';
            str5 = 'Utiliser une valeur constante';
            str6 = 'Use a constant value';
            [rep, flag] = my_questdlg(Lang(str1,str2), Lang(str3,str4), Lang(str5,str6));
            if ~flag
                return
            end
            if rep == 1
                [flag, nomFicGeoid, nomDirGeoid] = uiSelectFiles('ExtensionFiles', {'.geoid'}, 'RepDefaut', nomDirGeoid);
                if ~flag
                    return
                end
                [flag, lonGeoid, latGeoid, fGeoid, gGeoid] = geoidmodel_read(nomFicGeoid);
                if flag % Cas o� le fichier geoidmodel.geoid a �t� trouv� sur le r�pertoire des .all
                    RTKGeoid.Type         = 'SIS';
                    RTKGeoid.SIS.lonGeoid = lonGeoid;
                    RTKGeoid.SIS.latGeoid = latGeoid;
                    RTKGeoid.SIS.fGeoid   = fGeoid;
                    RTKGeoid.SIS.gGeoid   = gGeoid;
                    [flag, TideRTK, flagRTK, UseRTK] = processRTK_SIS(HeightRTK, RTKGeoid.SIS, Longitude, Latitude, UseRTK);
                    if ~flag
                        return
                    end
                    RechercheGeoid = false;
                end
        
            else % Valeur constante
                if isempty(persistent_valueGeoid)
                    valueGeoid = 0;
                else
                    valueGeoid = persistent_valueGeoid;
                end
                p = ClParametre('Name', 'Constant value', 'Unit', 'm',  'MinValue', -150, 'MaxValue', 150, 'Value', valueGeoid);
                a = StyledParametreDialog('params', p, 'Title', 'Geoid value');
                a.openDialog;
                flag = a.okPressedOut;
                if ~flag
                    return
                end
                Cste = a.getParamsValue;
                persistent_valueGeoid = Cste;
                RechercheGeoid = false;
                [flag, TideRTK, flagRTK, UseRTK] = processRTK_Cste(HeightRTK, Cste, UseRTK);
                RTKGeoid.Type         = 'SIS';
                RTKGeoid.SIS.lonGeoid = [-180 180 180 -180]';
                RTKGeoid.SIS.latGeoid = [-90  -90  90   90]';
                RTKGeoid.SIS.fGeoid   = zeros(4,1);
                RTKGeoid.SIS.gGeoid   = Cste + zeros(4,1);
            end
        end
    end
end


function [flag, TideRTK, flagRTK, UseRTK] = processRTK_SIS(HeightRTK, RTKGeoidSIS, Longitude, Latitude, UseRTK)

flagRTK = 0;

[FGeoid, GGeoid] = geoidmodel_interp(RTKGeoidSIS.lonGeoid, RTKGeoidSIS.latGeoid, RTKGeoidSIS.fGeoid, RTKGeoidSIS.gGeoid, Longitude, Latitude);
if any(isnan(FGeoid))
    if isempty(UseRTK)
        str1 = sprintf('Vos donn�es se pr�tent th�oriquement � une correction mar�graphique bas�e sur les donn�es RTK. Malheureusement le fichier "%s" semble inappropri� � la zone. La donn�e RTK ne sera pas utilis�es.', nomFicGeoid);
        str2 = sprintf('Your dataset could be processed with RTK data. Unfortunately the file "%s" seems to be out of the area, RTK data is set to Off.', nomFicGeoid);
        my_warndlg(Lang(str1,str2), 1); %0, 'Tag', 'RTKIsSetToOff')
    end
    TideRTK = NaN(size(HeightRTK), 'single');
    UseRTK = 0;
else
    if isempty(UseRTK)
        str1 = 'Vos donn�es se pr�tent � une correction mar�graphique bas�e sur les donn�es RTK. Voulez-vous  prendre l''information RTK en compte ?';
        str2 = 'Your dataset can be processed with RTK data. Do you want to proceed with it ?';
        [rep, flag] = my_questdlg(Lang(str1,str2));
        if ~flag
            return
        end
        UseRTK = (rep == 1);
    end
    if UseRTK
        TideRTK = HeightRTK - FGeoid - GGeoid;
        flagRTK = 1;
    else
        TideRTK(:) = NaN;
        return
    end
end
flag = 1;


function [flag, TideRTK, flagRTK, UseRTK] = processRTK_Cste(HeightRTK, Cste, UseRTK)

flagRTK = 0;
if UseRTK
    TideRTK = HeightRTK - Cste;
    flagRTK = 1;
else
    TideRTK(:) = NaN;
    return
end
flag = 1;


function [flag, TideRTK, flagRTK, UseRTK] = processRTK_SHOM(HeightRTK, GeoideShomAtlantique, GeoideShomMediterranee, GeoideBelgique, Longitude, Latitude, UseRTK)

flagRTK = 0;
TideRTK = NaN(size(HeightRTK), 'single');

if ~isempty(GeoideBelgique)
    FGeoid = get_val_xy_1D(GeoideBelgique, Longitude, Latitude);
    if ~all(isnan(FGeoid))
        [flag, TideRTK, flagRTK, UseRTK] = heightRTK2Tide(FGeoid, HeightRTK, flagRTK, UseRTK);
        if flag
            printfGeoid(FGeoid);
            return
        end
    end
end

if ~isempty(GeoideShomAtlantique)
    FGeoid = get_val_xy_1D(GeoideShomAtlantique, Longitude, Latitude);
    if all(isnan(FGeoid))
        FGeoid = get_val_xy_1D(GeoideShomMediterranee, Longitude, Latitude);
        if all(isnan(FGeoid))
            flag = 0;
            return
        else
            printfGeoid(FGeoid);
            my_breakpoint('FctName', 'processRTK'); % L'appel � heightRTK2Tide n'�tait pas fait dans ce cas avant le 06/04/2020 !!!
            [flag, TideRTK, flagRTK, UseRTK] = heightRTK2Tide(FGeoid, HeightRTK, flagRTK, UseRTK);
            if ~flag
                return
            end
        end
    else
        printfGeoid(FGeoid);
        [flag, TideRTK, flagRTK, UseRTK] = heightRTK2Tide(FGeoid, HeightRTK, flagRTK, UseRTK);
        if ~flag
            return
        end
    end
end
flag = 1;


function [flag, TideRTK, flagRTK, UseRTK] = heightRTK2Tide(FGeoid, HeightRTK, flagRTK, UseRTK)

flag = 1;
if isempty(UseRTK)
    str1 = 'Vos donn�es se pr�tent � une correction mar�graphique bas�e sur les donn�es RTK. Voulez-vous  prendre l''information RTK en compte ?';
    str2 = 'Your dataset can be processed with RTK data. Do you want to proceed with it ?';
    [rep, flag] = my_questdlg(Lang(str1,str2));
    if ~flag
        return
    end
    UseRTK = (rep == 1);
end
if UseRTK
    subNaN    = find(isnan(FGeoid));
    subNonNaN = find(~isnan(FGeoid));
    FGeoid(subNaN) = interp1(subNonNaN, FGeoid(subNonNaN), subNaN, 'nearest', 'extrap');
    TideRTK = HeightRTK - FGeoid;
    flagRTK = 1;
else
    TideRTK = NaN(size(HeightRTK), 'single');
end


function printfGeoid(FGeoid)

fprintf('Mean value of FGeoid : %f (m)\n', mean(FGeoid, 'omitnan'));
