function [flag, repImport, repExport] = process_xyz(repImport, repExport)

% TODO : pr�voir de pouvoir mettre les fichiers de sortie sur un autre r�pertoire et r�cup�rer ce r�pertoire dans repExport

%% Liste des fichiers � traiter

[flag, ListeFic, lastDir] = uiSelectFiles('ExtensionFiles', '.xyz', 'RepDefaut', repImport);
if ~flag
    return
end
repImport = lastDir;

%% Initialisations

I0 = cl_image_I0;

NbLigEntete  = [];
Separator    = [];
ValNaN       = [];
nColumns     = [];
GeometryType = [];
IndexColumns = [];
DataType     = [];
TypeAlgo     = [];
Unit         = [];
pasxy        = [];
Carto        = [];

%% Traitement supervis� ou automatique ?

str1 = 'Type de contr�le de la donn�e';
str2 = 'Type of data control';
[rep, flag] = my_questdlg(Lang(str1,str2), Lang('Manuel','Manual'), 'Auto');
if ~flag
    return
end
ControleManuel = (rep == 1);

if ControleManuel
    SigmaXY = 0;
else
    str1 = 'Coeff multiplicateur de l''�cart-type.';
    str2 = 'Standard deviation multiplicator.';
    p = ClParametre('Name', Lang('Seuil','Threshold'), 'MinValue', 0, 'MaxValue', 10, 'Value', 3);
    a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
    % a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
    a.sizes = [0 -2 0 -2 -1 -1 0 0];
    a.openDialog;
    flag = a.okPressedOut;
    if ~flag
        return
    end
    SigmaXY = a.getParamsValue;  
end

%% En avant la musique

AskQuestion = true;

N = length(ListeFic);
str1 = 'Import des fichiers .xyz';
str2 = 'Import of .xyz files';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    fprintf('%s\n', ListeFic{k});
    
    %% Lecture et maillage du fichier
    
    [flag, a, Carto, NbLigEntete, Separator, ValNaN, nColumns, GeometryType, ...
        IndexColumns, DataType, TypeAlgo, Unit, pasxy] = import_xyz(I0, ListeFic{k}, ...
        'NbLigEntete', NbLigEntete, 'Separator', Separator, 'ValNaN', ValNaN, ...
        'nColumns', nColumns, 'GeometryType', GeometryType, 'IndexColumns', IndexColumns, ...
        'DataType', DataType, 'Carto', Carto, 'TypeAlgo', TypeAlgo, 'Unit', Unit, 'pasxy', pasxy, ...
        'AskValNaN', AskQuestion, 'AskSeparator', AskQuestion, 'DisplayEntete', AskQuestion, ...
        'SigmaXY', SigmaXY, 'ControleManuel', ControleManuel);
    if ~flag
        continue
    end
    ImageName = extract_ImageName(a(1));
    AskQuestion = false;
%     imagesc(a)
            
    %% Affichage de l'image dans SonarScope pour v�rifier la donn�e
    
    TD = a(1).DataType;
    if ControleManuel && (TD == cl_image.indDataType('Bathymetry'))
        str1 = 'Une fen�tre SonarScope va s''ouvrir, elle permet de retoucher retoucher l''image.';
        str2 = 'A SonarScope window will open. You can clean the data if necessary.';
        my_warndlg(Lang(str1,str2), 1);
        a = SonarScope(a(1), 'visuProfilY', 1, 'profilY.Type', 'Height', 'visuProfilX', 1, 'ButtonSave', 1);
        
        [flag, indLayers] = listeLayersSynchronises(a, 1, 'memeDataType', 'IncludeCurrentImage', 'OnlyOneLayer', 1);
        if ~flag
            return
        end
        a = a(indLayers);
    end
   
    %% Transformation en LatLong
    
    GT = a(1).GeometryType;
    if GT == cl_image.indGeometryType('GeoYX')
        [a, flag] = GeoYX2LatLong(a(1)); % TODO : peut-�tre passer 'GridStep'
        if ~flag
            continue
        end
    end
    a = update_Name(a, 'Name', ImageName);
    
    %% Export en .ers
    
    ImageName = a(1).Name;
    nomDir = fileparts(ListeFic{k});
    nomFicOut = fullfile(nomDir, [ImageName '.ers']);
    flag = export_ermapper(a(1), nomFicOut);
    if ~flag
        continue
    end
    
    %% On demande si on veut traiter les autres fichiers
    
    if ControleManuel && (k ~= N)
        flag = question_ContinueNextFile();
        if ~flag
            break
        end
    end
    
end
my_close(hw, 'MsgEnd')
