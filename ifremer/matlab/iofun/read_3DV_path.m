function [flag, lat, lon] = read_3DV_path(nomFic)

flag = 0;
lat  = [];
lon  = [];

if ~exist(nomFic, 'file')
    return
end

fid = fopen(nomFic, 'r');
header = fgetl(fid); %#ok<NASGU>
while 1
    tline = fgetl(fid);
    if ~ischar(tline)
        break
    end
    
    tline = strrep(tline, '"', '');
    
    if contains(tline, 'elev')
        break
    end
    
    mots = strsplit(tline, ',');
    if length(mots) >= 2
        lat(end+1) = str2double(mots{1}); %#ok<AGROW>
        lon(end+1) = str2double(mots{2}); %#ok<AGROW>
    end
end
fclose(fid);

if isempty(lat)
    return
end

flag = 1;
