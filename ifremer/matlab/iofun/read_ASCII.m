% Reads a text file containing {x,y,val} data
%
% Syntax
%   [flag, X, Y, V, NbLigEntete, Separator, ValNaN, nColumns, GeometryType, IndexColumns, ...
%        DataType, Unit] = read_ASCII(FileName, ...)
%
% Input Arguments
%   FileName : Name of the text file
%
% Name-Value Pair Arguments
%   DisplayEntete   : 1/0 : 1=Display the first 20 lines in a window
%   NbLigEntete     : Number of heading lines
%   AskSeparator    : 1/0 : 1=aks to the user to define the separator
%   Separator       : Separator if different from usual ones
%   AskValNaN       : 1/0 : 1=aks to the user to define the value for missing data
%   ValNaN          : Missing value
%   nColumns        : Number of columns in the file
%   AskGeometryType : 1/0 : 1=aks to the user to define the GeometryType
%   GeometryType    : See cl_image
%   IndexColumns    : Order of the comumns for X, Y and V
%   DataType        : See cl_image
%   Unit            : Unit of the data
%   sub             : Subsampling of the data (Ex : 1:10:1000)
%   SigmaXY         : Coefficient to exclude data after SigmaXY*SandartDeviation
%
% Output Arguments
%   flag : 1=OK, 0=KO
%   X            : Abscissas
%   Y            : Ordinates
%   V            : Values of the data
%   NbLigEntete  : Number of heading lines
%   Separator    : Separator if different from usual ones
%   ValNaN       : Missing value
%   nColumns     : Number of columns in the file
%   GeometryType : See cl_image
%   IndexColumns : Order of the comumns for X, Y and V
%   DataType     : See cl_image
%   Unit         : Unit of the data
%
% Examples
%   FileName = getNomFicDatabase('LIDAR_1.xyz');
%   [flag, X, Y, V] = read_ASCII(FileName);
%   FigUtils.createSScFigure; PlotUtils.createSScPlot(X, Y, '*'); grid on
%
%   [flag, X, Y, V] = read_ASCII(FileName, 'DisplayEntete', 0, 'NbLigEntete', 0, ...
%       'AskSeparator', 0, 'Separator', [], 'AskValNaN', 0, 'ValNaN', [], ...
%       'nColumns', 4, 'AskGeometryType', 0, 'GeometryType', 2, ...
%       'IndexColumns', [1 2 3], 'DataType', 2, 'Unit', 'm');
%   FigUtils.createSScFigure; PlotUtils.createSScPlot(X, Y, '*'); grid on
%
% See also cl_image/import_xyz Authors
% Authors : JMA
%--------------------------------------------------------------------------

function [flag, X, Y, V, NbLigEntete, Separator, ValNaN, nColumns, GeometryType, IndexColumns, ...
    DataType, Unit] = read_ASCII(FileName, varargin)

persistent persistent_parametres

[varargin, DisplayEntete]   = getPropertyValue(varargin, 'DisplayEntete',   1);
[varargin, NbLigEntete]     = getPropertyValue(varargin, 'NbLigEntete',     []);
[varargin, AskSeparator]    = getPropertyValue(varargin, 'AskSeparator',    1);
[varargin, Separator]       = getPropertyValue(varargin, 'Separator',       []);
[varargin, AskValNaN]       = getPropertyValue(varargin, 'AskValNaN',       1);
[varargin, ValNaN]          = getPropertyValue(varargin, 'ValNaN',          []);
[varargin, nColumns]        = getPropertyValue(varargin, 'nColumns',        []);
[varargin, AskGeometryType] = getPropertyValue(varargin, 'AskGeometryType', 1);
[varargin, GeometryType]    = getPropertyValue(varargin, 'GeometryType',    []);
[varargin, IndexColumns]    = getPropertyValue(varargin, 'IndexColumns',    []);
[varargin, DataType]        = getPropertyValue(varargin, 'DataType',        []);
[varargin, Unit]            = getPropertyValue(varargin, 'Unit',            []);
[varargin, sub]             = getPropertyValue(varargin, 'sub',             []);
[varargin, SigmaXY]         = getPropertyValue(varargin, 'SigmaXY',         []);
[varargin, ScaleFactors]    = getPropertyValue(varargin, 'ScaleFactors',    []);
[varargin, memeReponses]    = getPropertyValue(varargin, 'memeReponses',    false); %#ok<ASGLU>

X = [];
Y = [];
V = [];

TitreDialogBox = 'ASCII file import';

if memeReponses && ~isempty(persistent_parametres)
    NbLigEntete  = persistent_parametres.NbLigEntete;
    Separator    = persistent_parametres.Separator;
    %     nbFields     = persistent_parametres.nbFields;
    ValNaN       = persistent_parametres.ValNaN;
    nColumns     = persistent_parametres.nColumns;
    indX         = persistent_parametres.indX;
    indY         = persistent_parametres.indY;
    indV         = persistent_parametres.indV;
    GeometryType = persistent_parametres.GeometryType;
    DataType     = persistent_parametres.DataType;
    Unit         = persistent_parametres.Unit;
    ScaleFactors = persistent_parametres.ScaleFactors;
else
    
    %% Reads the first 20 lines
    
    fid = fopen(FileName, 'r');
    if fid == -1
        [nomDir, FileName] = fileparts(FileName);
        FileName = fullfile(nomDir, [FileName '.xyz']);
        fid = fopen(FileName, 'r');
        if fid == -1
            flag = 0;
            return
        end
    end
    for k=1:20
        Entete{k} = fgetl(fid);%#ok
    end
    fclose(fid);
    
    %% Decodage guid� par l'op�rateur
    
    if DisplayEntete
        str = sprintf(Lang('Debut du fichier %', 'Begin of the file %s'), FileName);
        LigneTemoin = Entete{end};
        LigneTemoin = strtrim(LigneTemoin); % Rajout� par JMA le 28/09/2017
        mots = strsplit(LigneTemoin);
        
        Entete{end+1} = ' ';
        str1 = 'Information sur le contenu des colonnes pour la derni�re ligne';
        str2 = 'Information about columns content for last line';
        Entete{end+1} = Lang(str1,str2);
        nbFields = length(mots);
        for k=1:nbFields
            Entete{end+1} = sprintf('Column %d : %s', k, mots{k}); %#ok
        end
        my_txtdlg(str, Entete, 'windowstyle', 'normal');
        nbFields = max(3,nbFields); % A priori jamais utile mais on ne sais jamais (cas de la LigneTemoin qui ne le serait pas ?)
    else
        nbFields = 3;
    end
    
    %% Nombre de lignes d'ent�te
    
    if isempty(NbLigEntete)
        NbLigEntete = 0;
        str1 = 'Nb de lignes d''ent�te';
        str2 = 'Nb of Header Lines';
        [flag, NbLigEntete] = inputOneParametre(TitreDialogBox, Lang(str1,str2), 'Value', NbLigEntete, 'Format', '%d');
        if ~flag
            return
        end
    elseif NbLigEntete < 0
        NbLigEntete = -NbLigEntete;
    end
    
    %% S�parateur
    
    if AskSeparator
        if isempty(Separator)
            str1 = 'S�parateur sp�cifique autre que "Espace", "Chararct�re blanc" ou "Tabulation"';
            str2 = 'Specific separator if any. (OK if white-space character like space or Tab).';
            [Separator, flag] = my_inputdlg({Lang(str1,str2)}, {''});
            if ~flag
                return
            end
            Separator = Separator{1};
        end
    end
    
    %% No data
    
    if AskValNaN
        if isempty(ValNaN)
            [ValNaN, flag] = my_inputdlg(Lang({'Sp�cifier la valeur pour "NO-DATA" si besoin'},{'Specific value for "No Data" if any'}), {''});
            if ~flag
                return
            end
            ValNaN = ValNaN{1};
            if ~isempty(ValNaN)
                if strcmpi(ValNaN, 'NaN')
                    ValNaN = [];
                else
                    ValNaN = str2double(ValNaN);
                end
            end
        end
    end
    
    %% Nombe de colonnes utiles
    
    if isempty(nColumns)
        nColumns = nbFields;
        str1 = 'Nb de champs par ligne';
        str2 = 'Nb of fields in columns';
        [flag, nColumns] = inputOneParametre(TitreDialogBox, Lang(str1,str2), 'Value', nColumns, 'Format', '%d');
        if ~flag
            return
        end
    elseif nColumns < 0
        nColumns = -nColumns;
    end
    
    %% Type de g�om�trie
    
    if AskGeometryType
        if isempty(GeometryType)
            [flag, GeometryType] = cl_image.edit_GeometryType;
        elseif GeometryType < 0
            [flag, GeometryType] = cl_image.edit_GeometryType('GeometryType', -GeometryType);
        end
        if ~flag
            return
        end
    end
    
    %% Ordre des colonnes
    
    if isempty(IndexColumns)
        IndexColumns = [1 2 3];
        switch GeometryType
            case cl_image.indGeometryType('LatLong')
                [flag, indY, indX, indV] = getColumnOrder('Latitude', 'Longitude', 'Data', IndexColumns);
            otherwise
                [flag, indX, indY, indV] = getColumnOrder('X', 'Y', 'Data', IndexColumns);
        end
        if ~flag
            return
        end
        IndexColumns = [indX, indY, indV];
    else
        indX = IndexColumns(1);
        indY = IndexColumns(2);
        indV = IndexColumns(3:end);
    end
    
    %% Scale factors on X, Y and Values
    
    if isempty(ScaleFactors)
        IndexColumns = [1 2 3];
        [flag, scaleFactorX, scaleFactorY, scaleFactorV] = getScaleFactors;
        if ~flag
            return
        end
        ScaleFactors(1) = scaleFactorX;
        ScaleFactors(2) = scaleFactorY;
        ScaleFactors(3) = scaleFactorV;
    end
    
    %% Type de donn�es
    
    if isempty(DataType)
        [flag, DataType] = cl_image.edit_DataType;
        if ~flag
            return
        end
    elseif DataType < 0
        [flag, DataType] = cl_image.edit_DataType('DataType', -DataType);
        if ~flag
            return
        end
    end
    
    %% Unit�
    
    if isempty(Unit)
        [flag, Unit] = cl_image.edit_DataUnit('DataType', DataType);
        if ~flag
            return
        end
    end
    
    persistent_parametres.NbLigEntete  = NbLigEntete;
    persistent_parametres.Separator    = Separator;
    %	persistent_parametres.nbFields     = nbFields;
    persistent_parametres.ValNaN       = ValNaN;
    persistent_parametres.nColumns     = nColumns;
    persistent_parametres.indX         = indX;
    persistent_parametres.indY         = indY;
    persistent_parametres.indV         = indV;
    persistent_parametres.GeometryType = GeometryType;
    persistent_parametres.DataType     = DataType;
    persistent_parametres.Unit         = Unit;
    persistent_parametres.ScaleFactors = ScaleFactors;
end

%% Lecture du fichier

if (length(indX) == 1) && (length(indY) == 1) && ligneCoherente(FileName, NbLigEntete, Separator)
    [X, Y, V] = lectureDirecte(FileName, NbLigEntete, Separator, indX, indY, indV, nColumns);
    if isempty(X)
        [X, Y, V] = lectureIndirecte(FileName, NbLigEntete, Separator, indX, indY, indV, nColumns);
    end
else
    [X, Y, V] = lectureIndirecte(FileName, NbLigEntete, Separator, indX, indY, indV, nColumns);
end

%% Extraction si cela est demand�

if ~isempty(sub)
    X = X(sub);
    Y = Y(sub);
    V = V(sub,:);
end

%% Scale factors

if ScaleFactors(1) ~= 1
    X = X * ScaleFactors(1);
end
if ScaleFactors(2) ~= 1
    Y = Y * ScaleFactors(2);
end
if ScaleFactors(3) ~= 1
    V = V * ScaleFactors(3); % TODO ; diff�rencier V(:,k)
end

%% Suppression des NaN

if ~isempty(ValNaN)
    V(V == ValNaN) = NaN;
end
sub = ~isnan(sum(V,2));
X = X(sub);
Y = Y(sub);
V = V(sub,:);
clear sub

%% Supression des points dont les coordonn�es sont hors gabarit

if SigmaXY ~= 0
    S = stats(X);
    sub = find(X < (S.Mediane - SigmaXY*S.Variance));
    if ~isempty(sub)
        X(sub) = [];
        Y(sub) = [];
        V(sub,:) = [];
    end
    sub = find(X > (S.Mediane + SigmaXY*S.Variance));
    if ~isempty(sub)
        X(sub) = [];
        Y(sub) = [];
        V(sub,:) = [];
    end
    S = stats(Y);
    sub = find(Y < (S.Mediane - SigmaXY*S.Variance));
    if ~isempty(sub)
        X(sub) = [];
        Y(sub) = [];
        V(sub,:) = [];
    end
    sub = find(Y > (S.Mediane + SigmaXY*S.Variance));
    if ~isempty(sub)
        X(sub) = [];
        Y(sub) = [];
        V(sub,:) = [];
    end
end

flag = 1;


function [flag, ind1, ind2, ind3] = getColumnOrder(nom1, nom2, nom3, IndexColumns)

nomVar = {nom1, nom2, nom3};
for k=1:3
    value{k} = num2str(IndexColumns(k)); %#ok<AGROW>
end
[value, flag] = my_inputdlg(nomVar, value);
if ~flag
    ind1 = [];
    ind2 = [];
    ind3 = [];
    return
end
ind1 = str2num(value{1}); %#ok<ST2NM>
ind2 = str2num(value{2}); %#ok<ST2NM>
ind3 = str2num(value{3}); %#ok<ST2NM>


function [flag, scaleFactorX, scaleFactorY, scaleFactorV] = getScaleFactors

scaleFactorX = [];
scaleFactorY = [];
scaleFactorV = [];

p    = ClParametre('Name', 'Scale factor for X',      'MinValue', -Inf, 'MaxValue', Inf, 'Value', 1);
p(2) = ClParametre('Name', 'Scale factor for Y',      'MinValue', -Inf, 'MaxValue', Inf, 'Value', 1);
p(3) = ClParametre('Name', 'Scale factor for Values', 'MinValue', -Inf, 'MaxValue', Inf, 'Value', 1);
a = StyledParametreDialog('params', p, 'Title', 'ASCII file import');
%  a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [0 -1 0 0 0 -1 0 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
value = a.getParamsValue;
if ~flag
    return
end
scaleFactorX = value(1);
scaleFactorY = value(2);
scaleFactorV = value(3);


function flag = ligneCoherente(FileName, NbLigEntete, Separator)

fid = fopen(FileName, 'r');
for k=1:NbLigEntete
    fgetl(fid)
end
Ligne = fgetl(fid);
fclose(fid);
if isempty(Separator)
    Mots = strsplit(Ligne);
else
    Mots = strsplit(Ligne, Separator);
end
subNaN = strcmp(Mots', 'NaN');
Mots = Mots(~subNaN);
subEmpty = strcmp(Mots', '');
Mots = Mots(~subEmpty);
Valeurs = str2double(Mots);
if any(isnan(Valeurs))
    flag = 0;
    %     flag = 1; % Pour donn�es Nabil
else
    flag = 1;
end



function [X, Y, V] = lectureIndirecte(FileName, NbLigEntete, Separator, indX, indY, indV, nColumns)

fid = fopen(FileName, 'r');
for k=1:NbLigEntete
    Entete{k} = fgetl(fid); %#ok<AGROW,NASGU>
end

X = [];
Y = [];
V = [];
OK = 1;
Format ='';
% for k=1:max([indX indY, indV])
for k=1:nColumns
    Format = [Format '%s']; %#ok
end

while OK
    if isempty(Separator)
        C = textscan(fid, Format, 200000);
        % C = textscan(fid, Format, 1e6);
    else
        C = textscan(fid, Format, 200000, 'delimiter', Separator, 'MultipleDelimsAsOne', 1);
        % C = textscan(fid, Format, 1e6, 'delimiter', Separator);
    end
    if isempty(C{1})
        OK = 0;
    else
        N = length(C{1});
        Xk = NaN(N,1);
        Yk = NaN(N,1);
        Vk = NaN(N,length(indV));
        str1 = 'IFREMER - SonarScope : decodage du fichier par paquets de 200000 lignes.';
        str2 = 'IFREMER - SonarScope : decode data by blocks of 200000 lines.';
        hw = create_waitbar(Lang(str1,str2), 'N', N);
        for k=1:N
            my_waitbar(k, N, hw);
            
            if length(indX) == 1
                Xk(k) = str2double(C{indX}(k));
            else
                Xk(k) = str2lon([C{indX(1)}{k} ' ' C{indX(2)}{k}]);
            end
            if length(indY) == 1
                Yk(k) = str2double(C{indY}(k));
            else
                Yk(k) = str2lat([C{indY(1)}{k} ' ' C{indY(2)}{k}]);
            end
            for k2=1:length(indV)
                Vk(k,k2) = str2double(C{indV(k2)}{k});
            end
        end
        X = [X; Xk]; %#ok<AGROW>
        Y = [Y; Yk]; %#ok<AGROW>
        V = [V; Vk]; %#ok<AGROW>
        my_close(hw)
    end
end


function [X, Y, V] = lectureDirecte(FileName, NbLigEntete, Separator, indX, indY, indV, nColumns)

fid = fopen(FileName, 'r');
for k=1:NbLigEntete
    Entete{k} = fgetl(fid); %#ok<AGROW,NASGU>
end

X = {};
Y = {};
V = {};
OK = 1;
Format = '';
% for k=1:max([indX indY, indV])
%     if (k == indX) || (k == indY)
%         Format = [Format '%f']; %#ok
%     else
%         Format = [Format '%f']; %#ok
%     end
% end
for k=1:nColumns
    Format = [Format '%f']; %#ok<AGROW>
end

str1 = sprintf('Lecture des Data du fichier %s', FileName);
str2 = sprintf('Reading data from %s', FileName);
pppp = dir(FileName);
tailleFic = pppp.bytes;
hw = create_waitbar(Lang(str1,str2), 'N', tailleFic);
while OK
    tailleLue = ftell(fid);
    fprintf('%5.2f%%\n', 100*tailleLue/tailleFic);
    my_waitbar(tailleLue, tailleFic, hw);
    if isempty(Separator)
        C = textscan(fid, Format, 200000);
        % C = textscan(fid, Format, 1e6);
    else
        C = textscan(fid, Format, 200000, 'delimiter', Separator, 'MultipleDelimsAsOne', 1);
        % C = textscan(fid, Format, 1e6, 'delimiter', Separator);
    end
    if isempty(C{indY})
        OK = 0;
    else
        X{end+1} = C{indX}'; %#ok<AGROW>
        Y{end+1} = C{indY}'; %#ok<AGROW>
        V{end+1,1} = C{indV(1)}'; %#ok<AGROW>
        for k2=2:length(indV)
            V{end,k2} = C{indV(k2)}';
        end
    end
end
fclose(fid);
my_close(hw, 'MsgEnd');

X = [X{:}];
Y = [Y{:}];
V = [V{:}];

X = X(:);
Y = Y(:);
V = V(:);
V = reshape(V, length(X), length(indV));
