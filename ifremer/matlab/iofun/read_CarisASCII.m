% Reads a CARIS flag file for soundings
%
% Syntax
%   [flag, MaskCaris] = read_CarisASCII(nomFicTxt, nbPings, nbBeams)
%
% Input Arguments
%   nomFicTxt : Name of the text file
%   nbPings   : Number of pings of the sounder file
%   nbBeams   : Number of beams of the sounder file
%
% Output Arguments
%   flag      : 1=OK, 0=KO
%   MaskCaris : Matrix of booleans where true is for rejected soundings and
%   false for accepted soundings
%
% Remarks : Take care to the inversion of boolean values (true for rejected
% soundings)
%
% Examples
%   Create a text file that contains the lines :
% Lat (DD) Long (DD) Year Day Time Profile Beam Status
% -41.5439285 175.0286735 2015 143 06:58:23.019 2 1 Accept
% -41.5439336 175.0286687 2015 143 06:58:23.019 2 2 Accept
% -41.5439554 175.0286462 2015 143 06:58:23.019 2 3 Accept
% -41.5439681 175.0286334 2015 143 06:58:23.019 2 4 Accept
% -41.5439680 175.0286340 2015 143 06:58:23.019 2 5 Accept
% -41.5439775 175.0286246 2015 143 06:58:23.019 2 6 Accept
% -41.5439835 175.0286189 2015 143 06:58:23.019 2 7 Accept
% -41.5439883 175.0286144 2015 143 06:58:23.019 2 8 Accept
% -41.5439931 175.0286098 2015 143 06:58:23.019 2 9 Accept
% -41.5439996 175.0286035 2015 143 06:58:23.019 2 10 Accept
% -41.5440059 175.0285976 2015 143 06:58:23.019 2 11 Accept
% -41.5440154 175.0285881 2015 143 06:58:23.019 2 12 Accept
% -41.5440267 175.0285767 2015 143 06:58:23.019 2 13 Accept
% -41.5440261 175.0285779 2015 143 06:58:23.019 2 14 Accept
% -41.5440432 175.0285606 2015 143 06:58:23.019 2 15 Accept
% -41.5440477 175.0285563 2015 143 06:58:23.019 2 16 Accept
% -41.5440504 175.0285540 2015 143 06:58:23.019 2 17 Accept
% -41.5440548 175.0285500 2015 143 06:58:23.019 2 18 Reject (Swath Editor)
% -41.5440607 175.0285444 2015 143 06:58:23.019 2 19 Reject (Swath Editor)
% -41.5440729 175.0285322 2015 143 06:58:23.019 2 20 Accept
% -41.5440791 175.0285261 2015 143 06:58:23.019 2 21 Reject (Swath Editor)
% -41.5440909 175.0285143 2015 143 06:58:23.019 2 22 Accept
% -41.5441075 175.0284973 2015 143 06:58:23.019 2 23 Accept
% -41.5441120 175.0284931 2015 143 06:58:23.019 2 24 Accept
%
%   nomFicTxt = Your file name
%   nbPings = 533;
%   nbBeams = 432;
%   [flag, MaskCaris] = read_CarisASCII(nomFicTxt, nbPings, nbBeams)
%
% See also sonar_importBathyFromCaris_ALL Authors
% Authors  : JMA
%-------------------------------------------------------------------------------

function [flag, MaskCaris] = read_CarisASCII(nomFicTxt, nbPings, nbBeams)

flag      = 0;
MaskCaris = [];

%% Ouverture du fichier

fid = fopen(nomFicTxt);
if fid == -1
    messageErreurFichier(nomFicTxt, 'ReadFailure');
    return
end

%% Lecture et analyse de l'ent�te

entete = fgetl(fid); % // Year Day Time Profile Beam Status
entete = regexprep(entete, '\([^\(]*\)', ''); % Suppress the unit given in brackets

flagVirgule = contains(entete, ',');

if flagVirgule
    mots = strsplit(entete, ',');
else
    mots = strsplit(entete, ' ');
end

indicePing   = [];
indiceBeam   = [];
indiceStatus = [];
% indiceYear   = [];
% indiceDay    = [];
% indiceTime   = [];
if strcmp(mots{1}, '//')
    mots = mots(2:end);
end
for k=1:length(mots)
    if strcmp(mots{k}, 'Profile')
        indicePing = k;
    end
    if strcmp(mots{k}, 'Beam')
        indiceBeam = k;
    end
    if strcmp(mots{k}, 'Status')
        indiceStatus = k;
    end
%     if strcmp(mots{k}, 'Year')
%         indiceYear = k;
%     end
%     if strcmp(mots{k}, 'Day')
%         indiceDay = k;
%     end
%     if strcmp(mots{k}, 'Time')
%         indiceTime = k;
%     end
end
if isempty(indicePing) || isempty(indiceBeam) || isempty(indiceStatus)
    str1 = sprintf('L''ent�te du fichier "%s" ne contient pas les informations "Profile", "Beam" ou "Status".', nomFicTxt);
    str2 = sprintf('The header of "%s" does not contain "Profile", "Beam" ou "Status".', nomFicTxt);
    my_warndlg(Lang(str1,str2), 1);
    fclose(fid);
    return
end

ind = 1;


%% Lecture du fichier

if flagVirgule
    X = textscan(fid, '%s', 'Delimiter', '\n'); % , 'HeaderLines', 1) ;
    fclose(fid);
    X = X{1};
    n = length(X);
    iPing  = zeros(n,1);
    iBeam  = zeros(n,1);
    status = zeros(n,1);
    for k=1:n
        mots = strsplit(X{k}, ',');
        iPing(k)  = str2double(mots{indicePing});
        iBeam(k)  = str2double(mots{indiceBeam});
        status(k) = strncmpi('R', mots{indiceStatus}, 1);
    end
    
else
    
    Format = '';
    for k=1:max([indicePing indiceBeam indiceStatus])
        switch k
            %         case indiceYear
            %             Format = [Format ' %d']; %#ok<AGROW>
            %             indPing = ind;
            %             ind = ind + 1;
            %         case indiceDay
            %             Format = [Format ' %d']; %#ok<AGROW>
            %             indPing = ind;
            %             ind = ind + 1;
            %         case indiceTime
            %             Format = [Format ' %s']; %#ok<AGROW>
            %             indPing = ind;
            %             ind = ind + 1;
            case indicePing
                Format = [Format ' %d']; %#ok<AGROW>
                indPing = ind;
                ind = ind + 1;
            case indiceBeam
                Format = [Format ' %d']; %#ok<AGROW>
                indBeam = ind;
                ind = ind + 1;
            case indiceStatus
                Format = [Format ' %s']; %#ok<AGROW>
                indStatus = ind;
                ind = ind + 1;
            otherwise
                Format = [Format ' %*s']; %#ok<AGROW>
        end
    end
    Format = [Format  ' %*[^\n]'];
    X = textscan(fid, Format); % , 'HeaderLines', 1) ;
    fclose(fid);
    iPing  = X{indPing};
    iBeam  = X{indBeam};
    status = strncmpi('R', X{indStatus}, 1);
end
if isempty(iPing)
    flag = 0;
    return
end

%% Cr�ation du masque

% MaskCaris = ones(nbPings, nbBeams, 'uint8');
MaskCaris = false(nbPings, nbBeams);
for k=1:length(iPing)
    MaskCaris(iPing(k), iBeam(k)) = status(k);
end
flag = 1;

% 2013-283 10:25:32.039