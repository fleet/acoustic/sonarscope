function [flag, MaskMbSystem] = read_MbSystemASCII(nomFicTxt, nbPings, nbBeams, tPings)

flag = 0;
MaskMbSystem = [];

%% Ouverture du fichier

fid = fopen(nomFicTxt);
if fid == -1
    messageErreurFichier(nomFicTxt, 'ReadFailure');
    return
end

%% Lecture de l'ent�te

entete = fgetl(fid); % // Year Day Time Profile Beam Status
if isequal(entete, -1)
    return
end

mots = strsplit(entete);
indiceTime   = [];
indiceBeam   = [];
indiceStatus = [];
for k=1:length(mots)
    if (length(mots{k}) >= 5) && strcmp(mots{k}(1:5), 'time:')
        indiceTime = k + 1;
    end
    if (length(mots{k}) >= 5) && strcmp(mots{k}(1:5), 'beam:') 
        indiceBeam = k;
    end
    if (length(mots{k}) >= 7) && strcmp(mots{k}(1:7), 'action:')
        indiceStatus = k;
    end
end
if isempty(indiceTime) || isempty(indiceBeam) || isempty(indiceStatus)
    str1 = sprintf('L''ent�te du fichier "%s" ne contient pas les informations "Profile", "Beam" ou "Status".', nomFicTxt);
    str2 = sprintf('The header of "%s" does not contain "Profile", "Beam" ou "Status".', nomFicTxt);
    my_warndlg(Lang(str1,str2), 1);
    fclose(fid);
    return
end

%% D�codage du fichier

fseek(fid, 0, 'bof');
k = 0;
MaskMbSystem = zeros(nbPings, nbBeams, 'uint8');
while 1
    tline = fgetl(fid);
    if ~ischar(tline)
        break
    end
%     disp(tline)
    mots = strsplit(tline);

    t = cl_time('timeUnix', str2double(mots{indiceTime}));
    [~, iPing] = min(abs(tPings - t.timeMat));
 
    iBeam  = str2double(mots{indiceBeam}(6:end));
    status = str2double(mots{indiceStatus}(8:end));
    
    if (status == 1) || (status == 3) || (status == 4)
        MaskMbSystem(iPing, iBeam+1) = 1;
    end
    k = k + 1;    
    if mod(k, nbBeams*10) == 0
        disp(tline)
    end
end
fclose(fid);
flag = 1;
