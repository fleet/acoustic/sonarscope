% nomFicXls = 'F:\OTUS\att279.xls'
% [flag, DataNav] = read_NavXlsRov(nomFicXls);

function [flag, DataNav] = read_NavXlsRov(nomFicXls)

flag = exist(nomFicXls, 'file');
if ~flag
    return
end
[X, txt]= xlsread(nomFicXls);


N = size(X,1);
Date  = NaN(1, N);
Heure = NaN(1, N);
for i=1:N
    Date(i)  = dayStr2Ifr(txt{i+1,1});
    Heure(i) = hourStr2Ifr(txt{i+1,2});
end
DataNav.t = cl_time('timeIfr', Date, Heure);

DataNav.CAP         = X(:,1);
DataNav.GITE        = X(:,2);
DataNav.ASSIETTE    = X(:,3);
DataNav.IMMERSION   = X(:,4);
DataNav.ALTITUDE    = X(:,5);
DataNav.VITESSEX    = X(:,6);
DataNav.VITESSEY    = X(:,7);
DataNav.ANGLE_PAN   = X(:,8);
DataNav.ANGLE_TILT  = X(:,9);
DataNav.CAP_3CCD    = X(:,10);
DataNav.ASS_3CCD    = X(:,11);
DataNav.ZOOM        = X(:,12);
DataNav.OUVERTURE   = X(:,13);
DataNav.FOCUS       = X(:,14);
% DataNav.TEMP_1      = X(:,15);
% DataNav.TEMP_2      = X(:,16);
% DataNav.TEMP_RDI    = X(:,17);
% DataNav.SALINITE    = X(:,18);
% DataNav.CELERITE    = X(:,19);

for i=1:size(X,2)
    figure;
    haxe(i) = axes;  %#ok<AGROW>
    plot(DataNav.t, X(:,i)); grid on; title(txt{1,i+2}, 'Interpreter', 'none');
end
linkaxes(haxe, 'x');    
