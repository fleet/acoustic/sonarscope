%     nomFic = 'H:\Common_Data_Set\Geoacoustics\RDF\SBET\6-8-14\sbet_Mission 1.out';
%   [flag, Time, Latitude, Longitude, altitude, roll, pitch, heading,wander] = read_PospacSBET(nomFic);
%     Fig1 = plot_navigation_Figure([])
%     Fig1 = plot_navigation(nomFic, Fig1, Longitude, Latitude, Time, []);
%
%     figure; plot(Time);      grid on; title('Time')
%     figure; plot(Latitude);  grid on; title('Latitude')
%     figure; plot(Longitude); grid on; title('Longitude')
%     figure; plot(Longitude, Latitude); grid on; title('Nav')
% 
%     figure; plot(altitude); grid on; title('altitude')
%     figure; plot(roll);     grid on; title('roll')
%     figure; plot(pitch);    grid on; title('pitch')
%     figure; plot(heading);  grid on; title('heading')
%     figure; plot(wander);   grid on; title('wander')

function [flag, Time, Latitude, Longitude, altitude, roll, pitch, heading, wander] = read_PospacSBET(nomFic)

Time      = [];
Latitude  = [];
Longitude = [];
altitude  = [];

fid = fopen(nomFic, 'r');
if fid == -1
    return
end
X = fread(fid, [17 inf], 'double');
fclose(fid);

x         = X(1,:);
Latitude  = X(2,:);
Longitude = X(3,:);
altitude  = X(4,:);
roll      = X(8,:) * (180/pi);
pitch     = X(9,:) * (180/pi);
heading   = X(10,:) * (180/pi);
wander    = X(11,:);

nomDir = fileparts(nomFic);
[~, Date] = fileparts(nomDir);
mots = strsplit(Date, '-');
Jour  = str2double(mots{1});
Mois  = str2double(mots{2});
Annee = 2000 + str2double(mots{3});

% TODO  : A FAIRE
Sunday = Jour - 3; 

Date  = dayJma2Ifr(Sunday, Mois, Annee);
t = timeIfr2Mat(Date, x*1000);
Time = cl_time('timeMat', t);

flag = 1;

%{
public class SbetRecord
{
    public double Time;    //GPS seconds of week
    public double latRad;    //Latitude in radians
    public double lonRad;    //Longitude in radians
    public double alt;    //altitude
    public double xVel;    //velocity in x direction
    public double yVel;    //velocity in y direction
    public double zVel;    //velocity in z direction
    public double roll;    //roll angle
    public double pitch;    //pitch angle
    public double heading;    //heading angle
    public double wander;    //wander
    public double xForce;    //force in x direction
    public double yForce;    //force in y direction
    public double zForce;    //force in z direction

    public double xAngRate;    //angular rate in x direction
    public double yAngRate;    //angular rate in y direction
    public double zAngRate;    //angular rate in z direction
}

Then just use a BinaryReader to open up the file and read in the fields as doubles as shown in the code snippet below.

SbetRecord rec = new SbetRecord();

// Open an SBET file for reading...
BinaryReader reader = new BinaryReader(File.Open(@"C:\Temp\sbet1.out", FileMode.Open, FileAccess.Read));

// ...read a record...
rec.Time = reader.ReadDouble();
rec.latRad = reader.ReadDouble();
rec.lonRad = reader.ReadDouble();
rec.alt = reader.ReadDouble();
rec.xVel = reader.ReadDouble();
rec.yVel = reader.ReadDouble();
rec.zVel = reader.ReadDouble();
rec.roll = reader.ReadDouble();
rec.pitch = reader.ReadDouble();
rec.heading = reader.ReadDouble();
rec.wander = reader.ReadDouble();
rec.xForce = reader.ReadDouble();
rec.yForce = reader.ReadDouble();
rec.zForce = reader.ReadDouble();
rec.xAngRate = reader.ReadDouble();
rec.yAngRate = reader.ReadDouble();
rec.zAngRate = reader.ReadDouble();

// ... etc ...
%}