% Reads a Quimera flag file for soundings
%
% Syntax
%   [flag, MaskQuimera] = read_QuimeraASCII(nomFicTxt, nbPings, nbBeams)
%
% Input Arguments
%   nomFicTxt : Name of the text file
%   nbPings   : Number of pings of the sounder file
%   nbBeams   : Number of beams of the sounder file
%
% Output Arguments
%   flag      : 1=OK, 0=KO
%   MaskQuimera : Matrix of booleans where true is for rejected soundings and
%   false for accepted soundings
%
% Remarks : Take care to the inversion of boolean values (true for rejected
% soundings)
%
% Examples
%   Create a text file that contains the lines :
%   #Ping;Beam;Date Time
%   1;421;2016-09-14 04:12:55.541
%   1;422;2016-09-14 04:12:55.541
%   1;423;2016-09-14 04:12:55.541
%   1;424;2016-09-14 04:12:55.541
%   1;425;2016-09-14 04:12:55.541%
%   nomFicTxt = Your file name
%   nbPings = 533;
%   nbBeams = 432;
%   [flag, MaskQuimera] = read_QuimeraASCII(nomFicTxt, nbPings, nbBeams)
%
% See also sonar_importBathyFromQuimera_ALL Authors
% Authors  : JMA
%-------------------------------------------------------------------------------

function [flag, MaskQuimera] = read_QuimeraASCII(nomFicTxt, nbPings, nbBeams, timeMatDepth)

flag        = 0;
MaskQuimera = [];

%% Ouverture du fichier

fid = fopen(nomFicTxt);
if fid == -1
    messageErreurFichier(nomFicTxt, 'ReadFailure');
    return
end

%% Lecture et analyse de l'ent�te

entete = fgetl(fid); % // Year Day Time Profile Beam Status
entete = regexprep(entete, '\([^\(]*\)', ''); % Suppress the unit given in brackets

if contains(entete, ';')
    Separator = ';';
elseif contains(entete, ',')
    Separator = ',';
else
    Separator = ' ';
end

mots = strsplit(entete, Separator);

indicePing   = [];
indiceBeam   = [];
indiceDate   = [];
% indiceStatus = [];
% indiceYear   = [];
% indiceDay    = [];
% indiceTime   = [];
if strcmp(mots{1}, '//')
    mots = mots(2:end);
end
for k=1:length(mots)
    mots{k} = strrep(mots{k}, '#', '');
    if strcmp(mots{k}, 'Ping')
        indicePing = k;
    end
    if strcmp(mots{k}, 'Beam')
        indiceBeam = k;
    end
    if strcmp(mots{k}, 'Date Time')
        indiceDate = k;
    end
%     if strcmp(mots{k}, 'Status')
%         indiceStatus = k;
%     end
%     if strcmp(mots{k}, 'Year')
%         indiceYear = k;
%     end
%     if strcmp(mots{k}, 'Day')
%         indiceDay = k;
%     end
%     if strcmp(mots{k}, 'Time')
%         indiceTime = k;
%     end
end
if isempty(indicePing) || isempty(indiceBeam) || isempty(indiceDate)
    str1 = sprintf('L''ent�te du fichier "%s" ne contient pas les informations "Profile", "Beam" ou "Status".', nomFicTxt);
    str2 = sprintf('The header of "%s" does not contain "Profile", "Beam" ou "Status".', nomFicTxt);
    my_warndlg(Lang(str1,str2), 1);
    fclose(fid);
    return
end

ind = 1;


%% Lecture du fichier

if strcmp(Separator, ',')
    X = textscan(fid, '%s', 'Delimiter', '\n'); % , 'HeaderLines', 1) ;
    fclose(fid);
    X = X{1};
    n = length(X);
    iPing  = zeros(n,1);
    iBeam  = zeros(n,1);
    status = zeros(n,1);
    for k=1:n
        mots = strsplit(X{k}, ',');
        iPing(k)  = str2double(mots{indicePing});
        iBeam(k)  = str2double(mots{indiceBeam});
        status(k) = 1;
    end
    
else
    
    Format = '';
    for k=1:max([indicePing indiceBeam indiceDate])
        switch k
            case indicePing
                Format = [Format Separator '%d']; %#ok<AGROW>
                indPing = ind; %#ok<NASGU>
                ind = ind + 1;
            case indiceBeam
                Format = [Format Separator '%d']; %#ok<AGROW>
                indBeam = ind;
                ind = ind + 1;
            case indiceDate
                Format = [Format Separator '%s %s']; %#ok<AGROW>
                indDate = ind;
                ind = ind + 1;
            otherwise
                Format = [Format Separator '%*s']; %#ok<AGROW>
        end
    end
    Format = Format(2:end) ;
%     Format = [Format Separator '%*[^\n]'];
    X = textscan(fid, Format); % , 'HeaderLines', 1) ;
    fclose(fid);
%     iPing  = X{indPing};
    iBeam  = X{indBeam};
    sDate  = X{indDate};
    sTime  = X{indDate + 1};
end
nbPoints = length(iBeam);
t = datetime([cell2str(sDate) repmat(' ', nbPoints, 1) cell2str(sTime)], 'InputFormat', 'yyyy-MM-dd HH:mm:ss.SSS');

%% Cr�ation du masque

% MaskQuimera = ones(nbPings, nbBeams, 'uint8');
timeMatDepth = datetime(timeMatDepth, 'ConvertFrom', 'datenum');
MaskQuimera = false(nbPings, nbBeams);
for k=1:nbPoints
    timeMatQuimera = t(k);
    diff = timeMatDepth - timeMatQuimera;
    [tMin, kPing] = min(abs(diff)); %#ok<ASGLU>
    MaskQuimera(kPing, iBeam(k)) = 1;
end
flag = 1;

% 2013-283 10:25:32.039