function [flag, ABS] = read_SISabs(nomFic)

% try
%     T = readtable(nomFic, 'Format', '%14c  %f%f%f%f');
%     head(T)
%     
%     ABS.Datetime = datetime(T.Var1, 'InputFormat','yyyyMMddHHmmss');
%     ABS.Z      = -(T.Var3);
%     ABS.alpha1 = T.Var4;
%     ABS.alpha2 = T.Var5;
%     
%     ABS.Time = datenum(ABS.Datetime);
% 
% catch
    
    flag = 0;
    ABS  = [];
    
    fid = fopen(nomFic, 'r');
    if fid == -1
        messageErreurFichier(nomFic, 'ReadFailure');
        return
    end
    Header = fgetl(fid);
    fclose(fid);
    
    fid = fopen(nomFic, 'r');
    data = textscan(fid, '%f %f %f', 'HeaderLines', 1);
    fclose(fid);
    
    ABS.Filename = nomFic;
    ABS.Z = -data{1};
    ABS.alpha1 = data{2};
    ABS.alpha2 = data{3};
    % alpha3 = moyx0(ABS.Z, ABS.alpha1, 0);
    
    % FigUtils.createSScFigure; PlotUtils.createSScPlot(ABS.alpha1, ABS.Z, 'k');  grid on; hold on; PlotUtils.createSScPlot(ABS.alpha2, ABS.Z, 'b');  PlotUtils.createSScPlot(alpha3, ABS.Z, 'r');
    % FigUtils.createSScFigure; PlotUtils.createSScPlot(-ABS.Z, ABS.alpha1, 'k'); grid on; hold on; PlotUtils.createSScPlot(-ABS.Z, ABS.alpha2, 'b'); PlotUtils.createSScPlot(-ABS.Z, alpha3, '*r');
    
    [~, filename] = fileparts(nomFic);
    filename = strrep(filename, 'thinned_', '');
    mots = strsplit(filename, '_');
    ABS.Salinity  = str2double(mots{5}) / 100;
    ABS.Frequency = str2double(mots{6}(1:end-3));
    
    mots = strsplit(Header);
    X = sscanf(mots{5}, '%4d%2d%2d%2d%2d%2d');
    date  = dayJma2Ifr(X(3), X(2), X(1));
    heure = hourHms2Ifr(X(4), X(5), X(6));
    t = cl_time('timeIfr', date, heure);
    ABS.Time     = t.timeMat;
    ABS.Datetime = datetime(ABS.Time, 'ConvertFrom', 'datenum'); % datetime(X(1), X(2), X(3), X(3), X(4), X(5))
    
    ABS.Latitude  = str2double(mots{6});
    ABS.Longitude = str2double(mots{7});
% end
flag = 1;
