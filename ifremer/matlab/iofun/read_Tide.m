%   TideFile = getNomFicDatabase('ExKongsberg.tide')
%   TideFile = getNomFicDatabase('ExNiwa.tide')
%   TideFile = getNomFicDatabase('ExLibex.tid')
%   TideFile = getNomFicDatabase('ExCaraibes.tide')
%   TideFile = getNomFicDatabase('ExCidco.tide')
%   TideFile = 'D:\Temp\Herve\BugTide\maregraphe_brest.ttb';
%   [flag, Tide] = read_Tide(TideFile, 'CleanData', 1)

function [flag, Tide] = read_Tide(TideFile, varargin)

[varargin, CleanData] = getPropertyValue(varargin, 'CleanData', 0); %#ok<ASGLU>

flag = 1; % TODO

[nomDir, nomFic, ext] = fileparts(TideFile);
nomFicMat = fullfile(nomDir, [nomFic '_' ext(2:end) '.mat']);

UN = 1;
if UN && exist(nomFicMat, 'file') && checkFileCreation(nomFicMat, '25/08/2015') ...
        && checkFilePosterior(nomFicMat, TideFile)
    Tide = loadmat(nomFicMat, 'nomVar', 'Tide');
    % Pr�paration remplacement cl_time par datetime
    Tide.Datetime = datetime(Tide.Time.timeMat, 'ConvertFrom', 'datenum');
    return
    
else
    % TODO : refondre cette fonction tel que c'est fait dans lecFicNav_GenavirAUV
    
    TideTime = [];
    
    typeFormat = identFormatMaree(TideFile);
    [flag, nbHeaderLines, nbWordsExpected] = estimateNbHeaderLinesInAnASCIIFile(TideFile, 100);
    Format = '';
    for k=1:nbWordsExpected
        Format = [Format '%s']; %#ok<AGROW>
    end
    
    Tab = sprintf('\t');
    fid = fopen(TideFile, 'r');
    % TODO : textscan ne fonctionne plus comme avant !!! Les d�limiters ne sont plus pris en compte !!!
    data = textscan(fid, Format, 'HeaderLines', nbHeaderLines, 'delimiter', {' '; ';'; Tab}, 'MultipleDelimsAsOne', 1);
    fclose(fid);
    
    if length(data) == 1 % Cas du fichier ExCidco.tide : 2011-05-19;02:00;2.391
        X = cell2str(data{1});
        ind = strfind(X(1,:), ';');
        if length(ind) == 2
            clear data
            data{1} = str2cell(X(:,1:(ind(1)-1)));
            data{2} = str2cell(X(:,(ind(1)+1):(ind(2)-1)));
            data{3} = str2cell(X(:,(ind(2)+1):end));
        else
            clear data
            data{1} = str2cell(X(1:3:end,:));
            data{2} = str2cell(X(2:3:end,:));
            data{3} = str2cell(X(3:3:end,:));
        end
    end
    
    switch length(data)
        case 2 % 2 champs donc sans doute du type Kongsberg c.a.d YYYYMMDDHHMM XX.XXX
            TideValue = str2double(data{2});
            sub = isnan(TideValue);
            TideValue(sub) = [];
            data{1}(sub)   = [];
            
            s1 = data{1}{1};
            val1 = str2double(s1);
            if (length(s1) == 12) && ~isnan(val1) % Vraisemblablement Kongsberg c.a.d YYYYMMDDHHMM XX.XXX
                val1 = str2double(data{1});
                Minute = mod(val1, 100);
                val1 = floor(val1 / 100);
                Heure = mod(val1, 100);
                val1 = floor(val1 / 100);
                DD = mod(val1, 100);
                val1 = floor(val1 / 100);
                MM = mod(val1, 100);
                YYYY = floor(val1 / 100);
                DIfr = dayJma2Ifr(DD, MM, YYYY);
                hIfr = hourHms2Ifr(Heure, Minute, 0);
                TideTime = cl_time('timeIfr', DIfr, hIfr);
                
            elseif (length(s1) == 14) && ~isnan(val1) % NIWA c.a.d YYYYMMDDHHMMSS XX.XXX mission QUOI E:\QUOI_OUT\tan1806\MBES\tide\BoPlnty_tan1806_sis.tide : 20180701000000 -0.167
                val1 = str2double(data{1});
                Seconde = mod(val1, 100);
                val1 = floor(val1 / 100);
                Minute = mod(val1, 100);
                val1 = floor(val1 / 100);
                Heure = mod(val1, 100);
                val1 = floor(val1 / 100);
                DD = mod(val1, 100);
                val1 = floor(val1 / 100);
                MM = mod(val1, 100);
                YYYY = floor(val1 / 100);
                DIfr = dayJma2Ifr(DD, MM, YYYY);
                hIfr = hourHms2Ifr(Heure, Minute, Seconde);
                TideTime = cl_time('timeIfr', DIfr, hIfr);
           end
            
        otherwise % 3 champs donc sans doute date, heure, mar�e
            if strcmp(typeFormat, 'LIBEX')
                for k=1:length(data)
                    data{k} = data{k}(2:end-3);
                end
            end
            TideValue = str2double(data{3});
            sub = isnan(TideValue);
            TideValue(sub) = [];
            data{1}(sub)   = [];
            data{2}(sub)   = [];
            
            val1 = str2double(data{1}(1));
            val2 = str2double(data{2}(1));
            if ~isnan(val1) && ~isnan(val2) % Vraisemblablement date du genre YYYYMMDD et heure du genre HHMMSS
                'TODO';
                
            else % date pas d�cod�e donc il s'agit de string du genre 'YYYY/MM/DD' ou 'DD/MM/YYYY' et heure du genre 'HH:MM:SS'

                DIfr = dayStr2Ifr(data{1});
                hIfr = hourStr2Ifr(data{2});
                if ~isempty(DIfr) && ~isempty(hIfr)
                    TideTime = cl_time('timeIfr', DIfr, hIfr);
                end
            end
    end
    
    if ~isempty(TideTime) && ~isempty(TideValue)
        
        %% The convention of tide sign for SonarScope is positive. One check if it is ok and set the opposite if ever
        
        TideValue = setTidePositive(TideValue);
        
    else
        messageFileNotRecognizedAutomatically(TideFile)
        
        switch typeFormat
            case 'LIBEX'
                [flag, TideTime, TideValue] = read_TideLIBEX(TideFile, 'nbHeaderLines', nbHeaderLines);
                if ~flag
                    return
                end
                
            case 'CIDCO'
                [flag, TideTime, TideValue] = read_TideCIDCO(TideFile);
                if ~flag
                    return
                end
                
            case 'UnivGent'
                [flag, TideTime, TideValue] = read_TideUnivGent(TideFile);
                if ~flag
                    return
                end
                
            case 'Niwa'
                [flag, TideTime, TideValue] = read_TideNiwa(TideFile);
                if ~flag
                    return
                end
                
            case 'Caraibes'
                [flag, TideTime, TideValue] = read_TideCaraibes(TideFile);
                if ~flag
                    return
                end
                
            case 'ProvenanceInconnue'
                [flag, TideTime, TideValue] = read_TideUnknown(TideFile);
                if ~flag
                    return
                end
                
            case 'CaraibesHar'
                [flag, TideTime, TideValue] = read_TideCaraibesFromHar(TideFile);
                if ~flag
                    return
                end
                
            case 'MickaelVasquez'
                [flag, TideTime, TideValue] = read_TideVasquez(TideFile);
                if ~flag
                    return
                end
                
            case {'Simrad'; 'Simrad1'; 'Simrad2'}
                [flag, TideTime, TideValue] = read_TideSimrad(TideFile);
                
            case 'Caris'
                [flag, TideTime, TideValue] = read_TideCaris(TideFile);
                if ~flag
                    return
                end
                
            otherwise
                str1 = 'Le format du fichier de mar�e n''est pas reconnu, envoyez ce fichier � sonarscope@ifremer.fr SVP.';
                str2 = 'The tide file format is unknown. Please send this file to sonarscope@ifremer.fr';
                my_warndlg(Lang(str1,str2), 1);
                return
        end
    end
    
    %% One save the result in a .mat file
    
    Tide.Time  = TideTime;
    Tide.Value = TideValue(:)';
    
    % Pr�paration remplacement cl_time par datetime
    Tide.Datetime = datetime(Tide.Time.timeMat, 'ConvertFrom', 'datenum');
    
    save(nomFicMat, 'nomFic', 'Tide')
end

%% Nettoyage

if CleanData
    Title = ['Edit Tide - ' nomFic ext];
    T2 = Tide.Datetime;
    xSample = XSample('name', 'Pings',  'data', T2);
    ySample = YSample('Name', 'Tide', 'data', Tide.Value, 'marker', '.');
    signal = ClSignal('Name', Title, 'xSample', xSample, 'ySample', ySample);
    s = SignalDialog(signal, 'Title', 'Edit signal', 'cleanInterpolation', 1);
    s.openDialog();
    if s.okPressedOut
        Tide.Value = ySample.data;
    end
    
    save(nomFicMat, 'nomFic', 'Tide')
end

%% Auto-plot

if nargout == 0
    FigUtils.createSScFigure; PlotUtils.createSScPlot(Tide.Time, Tide.Value); grid on;
end


function messageFileNotRecognizedAutomatically(TideFile)

str1 = sprintf('Le fichier "%s" n''a pas pu �tre d�cod� automatiquement. Je tente le d�codage "ancienne formule". Pouvez-vous envoyer ce fichier � sonarscope@ifremer.fr SVP ?', TideFile);
str2 = sprintf('File "%s" could not be interprated automatically. I am tring to do it using the "old fashion way". Could you please send this file to sonarscope@ifremer.fr ?', TideFile);
my_warndlg(Lang(str1,str2), 1);
