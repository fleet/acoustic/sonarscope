% Lecture de donn�es de maregraphe au format utilis� par le CIDCO
%
% Syntax
%   [t, maree, flag] = read_TideCIDCO(nomFic)
% 
% INTPUT PARAMETERS :
%   nomFic : Nom du fichier de maree
% 
% Output Arguments
%   []    : Auto-plot activation
%   t     : Temps (instance de cl_time)
%   maree : Maree (m)
%   flag  : 1=OK, 0=KO
% 
% EXEMPLES :
%   TideFile = getNomFicDatabase('ExCidco.tide')
%   read_TideCIDCO(TideFile)
%
%   [t, maree, flag] = read_TideCIDCO(TideFile);
%
% See also read_Tide ecrFicMareeCar Authors
% Authors : JMA
%-----------------------------------------------------------------------

function  [flag, t, maree] = read_TideCIDCO(nomFic)

% TODO : refondre cette fonction tel que c'est fait dans lecFicNav_GenavirAUV

messageAboutTide('CIDCO')

flag  = 0;
t     = [];
maree = [];

N = countLines(nomFic);
if isnan(N)
    messageErreurFichier(nomFic, 'ReadFailure');
    return
end
N = N - 4;
date  = zeros(N,1);
heure = zeros(N,1);
maree = zeros(N,1);

fid = fopen(nomFic, 'r');
tline = fgetl(fid); %#ok<NASGU>
tline = fgetl(fid); %#ok<NASGU>
tline = fgetl(fid); %#ok<NASGU>
tline = fgetl(fid); %#ok<NASGU>
for k=1:N
    tline = fgetl(fid);
    mots = strsplit(tline, ';');
    mots{1} = strrep(mots{1}, '-', ' ');
    x = sscanf(mots{1}, '%04d%02d%02d');
    date(k)  = dayJma2Ifr(x(3), x(2), x(1));
    mots{2} = strrep(mots{2}, ':', ' ');
    x = sscanf(mots{2}, '%02d%02d');
    heure(k) = hourHms2Ifr(x(1), x(2), 0);
    maree(k) = str2double(mots{3});
end
fclose(fid);

%% Lecture de l'heure associ�e aux pings sonar

t = cl_time('timeIfr', date, heure);

%% Auto-plot

if nargout == 0
    FigUtils.createSScFigure; PlotUtils.createSScPlot(t, maree); grid on;
    title(['Tide : ' nomFic], 'Interpreter', 'None')
end

flag = 1;
