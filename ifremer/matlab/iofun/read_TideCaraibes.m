% Lecture de donnees de maregraphe au format Caraibes
%
% Syntax
%   [t, maree, flag] = read_TideCaraibes(nomFic)
% 
% INTPUT PARAMETERS :
%   nomFic : Nom du fichier de maree
% 
% Output Arguments
%   []    : Auto-plot activation
%   t     : Temps (instance de cl_time)
%   maree : Maree (m)
%   mareeFiltree : Maree filtree (m)
%   flag  : 1=OK, 0=KO
% 
% EXEMPLES :
%   TideFile = getNomFicDatabase('ExCaraibes.tide')
%   read_TideCaraibes(TideFile)
%
%   [t, maree, flag] = read_TideCaraibes(TideFile);
%
% See also read_Tide ecrFicMareeCar Authors
% Authors : JMA
%-----------------------------------------------------------------------

function  [flag, t, maree] = read_TideCaraibes(nomFic)

messageAboutTide('Caraibes')

flag  = 0;
t     = [];
maree = [];

nbHeaderLines = 0;
fid = fopen(nomFic, 'r');
if fid == -1
    return
end
% Ent�te ou pas ?
tline = fgetl(fid);
while isnan(str2double(tline(1:2)))
    nbHeaderLines = nbHeaderLines + 1;
    tline = fgetl(fid);
end
fclose(fid);

fid = fopen(nomFic, 'r');
data = textscan(fid, '%s %s %f %*[^\n]', 'HeaderLines', nbHeaderLines);
fclose(fid);
date  = dayStr2Ifr(data{1});
heure = hourStr2Ifr(data{2});
maree = data{3};

%% Lecture de l'heure associ�e aux pings sonar

t = cl_time('timeIfr', date, heure);

%% Auto-plot

if nargout == 0
    FigUtils.createSScFigure; PlotUtils.createSScPlot(t, maree); grid on;
    title(['Tide : ' nomFic], 'Interpreter', 'None')
end

flag = 1;
