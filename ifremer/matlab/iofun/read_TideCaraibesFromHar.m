% Lecture de donnees de maregraphe au format Caraibes
%
% Syntax
%   [t, maree, flag] = read_TideCaraibesFromHar(nomFic)
% 
% INTPUT PARAMETERS :
%   nomFic : Nom du fichier de maree
% 
% Output Arguments
%   []    : Auto-plot activation
%   t     : Temps (instance de cl_time)
%   maree : Maree (m)
%   mareeFiltree : Maree filtree (m)
%   flag  : 1=OK, 0=KO
% 
% EXEMPLES :
%   nomFic = '/home1/brekh/augustin/th013b/car/2001-03b-utc.dat'
%   read_TideCaraibesFromHar(nomFic)
%
%   [t, maree, flag] = read_TideCaraibesFromHar(nomFic);
%
% See also read_Tide ecrFicMareeCar Authors
% Authors : JMA
%-----------------------------------------------------------------------

function  [flag, t, maree] = read_TideCaraibesFromHar(nomFic)

% TODO : refondre cette fonction tel que c'est fait dans lecFicNav_GenavirAUV

messageAboutTide('CaraibesFromHar')

flag  = 0;
t     = [];
maree = [];

N = countLines(nomFic);
if isnan(N)
    messageErreurFichier(nomFic, 'ReadFailure');
    return
end

X = dlmread(nomFic);
date  = dayJma2Ifr(X(:,2), X(:,3), X(:,4));
heure = hourHms2Ifr(X(:,5), X(:,6), X(:,7));
maree = X(:,8);

%% Lecture de l'heure associ�e aux pings sonar

t = cl_time('timeIfr', date, heure);

%% Auto-plot

if nargout == 0
    FigUtils.createSScFigure; PlotUtils.createSScPlot(t, maree); grid on;
    title(['Tide : ' nomFic], 'Interpreter', 'None')
end

flag = 1;
