% Lecture de donnees de mar� au format CARIS
%
% Syntax
%   [t, maree, flag] = read_TideCaris(nomFic)
% 
% INTPUT PARAMETERS :
%   nomFic : Nom du fichier de maree
%
% Name-Value Pair Arguments
%   Offset : Offset a rajouter (m)
% 
% Output Arguments
%   []    : Auto-plot activation
%   t     : Temps (instance de cl_time)
%   maree : Maree (m)
%   mareeFiltree : Maree filtree (m)
%   flag  : 1=OK, 0=KO
% 
% EXEMPLES :
%   nomFic = 'TODO'
%   read_TideCaris(nomFic)
%
%   [t, maree, flag] = read_TideCaris(nomFic);
%
% See also read_Tide ecrFicMareeCar Authors
% Authors : JMA
%-----------------------------------------------------------------------

function  [flag, t, maree] = read_TideCaris(nomFic)

% TODO : refondre cette fonction tel que c'est fait dans lecFicNav_GenavirAUV

messageAboutTide('Caris')

flag  = 0;
t     = [];
maree = [];

N = countLines(nomFic);
if isnan(N)
    messageErreurFichier(nomFic, 'ReadFailure');
    return
end

N = N - 1;
date  = zeros(N,1);
heure = zeros(N,1);
maree = zeros(N,1);

fid = fopen(nomFic, 'r');
tline = fgetl(fid); %#ok<NASGU>
for k=1:N
    tline = fgetl(fid);
    mots = strsplit(tline);
    date(k)  = dayStrGB2Ifr(mots{1});
    heure(k) = hourStr2Ifr(mots{2});
    maree(k) = str2double(mots{3});
end
fclose(fid);

%% Lecture de l'heure associ�e aux pings sonar

t = cl_time('timeIfr', date, heure);

%% Auto-plot

if nargout == 0
    FigUtils.createSScFigure; PlotUtils.createSScPlot(t, maree); grid on;
    title(['Tide : ' nomFic], 'Interpreter', 'None')
end

flag = 1;
