%{
--------------------- LIBEX file dump ----------------------
----------------------- STN00905.OBS -----------------------
------------------ Time Zone:  GMT ( 0.0) ------------------

--------------------- Invariant Fields ---------------------
Name             Type Size    Units Value
------------------------------------------------------------
station_id       CHAR    5          00905
station_name     CHAR   16          St. John's, NFLD
data_product     CHAR    3          wlo
start_time       INTG    4  seconds 91/11/01 00:00:00
end_time         INTG    4  seconds 91/11/16 00:00:00
file_date        INTG    4  seconds 92/04/14 19:24:24
max_water_level  REAL    4   metres    1.640
min_water_level  REAL    4   metres    0.050
------------------------------------------------------------

---------------------- Variant Fields ----------------------
Name             Type Size    Units
------------------------------------------------------------
time             INTG    4  seconds 
water_level      REAL    4   metres 
std_dev          REAL    4   metres 
------------------------------------------------------------

1980/01/01 00:00:0.000  0.000000
2014/09/09 00:02:00.000  0.822
2014/09/09 00:03:00.000  0.835
2014/09/09 00:04:00.000  0.845
2014/09/09 00:05:00.000  0.859
...
2050/12/31 23:59:59.000  0.000000
-------------------- End Of LIBEX File ---------------------
%}

% TideFile = getNomFicDatabase('ExLibex.tid');
% [flag, t, maree] = read_TideLIBEX(TideFile)

function  [flag, t, maree] = read_TideLIBEX(nomFic)

messageAboutTide('LIBEX')

flag  = 0;
t     = [];
maree = [];

N = countLines(nomFic);
if isnan(N)
    messageErreurFichier(nomFic, 'ReadFailure');
    return
end

fid = fopen(nomFic, 'r');
for k=1:26
    tline = fgetl(fid); %#ok<NASGU>
end

N = N-26-2;
date  = zeros(N,1);
heure = zeros(N,1);
maree = zeros(N,1);
for k=1:N
    tline = fgetl(fid);
    mots = strsplit(tline);
    mots{1} = strrep(mots{1}, '/', ' ');
    x = sscanf(mots{1}, '%04d%02d%02d');
    date(k) = dayJma2Ifr(x(3), x(2), x(1));
    mots{2} = strrep(mots{2}, ':', ' ');
    x = sscanf(mots{2}, '%02d%02d');
    heure(k) = hourHms2Ifr(x(1), x(2), 0);
    maree(k) = str2double(mots{3});
end
fclose(fid);

%% Lecture de l'heure associ�e aux pings sonar

t = cl_time('timeIfr', date, heure);

%% Auto-plot

if nargout == 0
    FigUtils.createSScFigure; PlotUtils.createSScPlot(t, maree); grid on;
    title(['Tide : ' nomFic], 'Interpreter', 'None')
end

flag = 1;
