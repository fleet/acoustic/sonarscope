% Lecture de donnees de maregraphe au format utilis� au NIWA
%
% Syntax
%   [t, maree, flag] = read_TideNiwa(nomFic)
% 
% INTPUT PARAMETERS :
%   nomFic : Nom du fichier de maree
% 
% Output Arguments
%   []    : Auto-plot activation
%   t     : Temps (instance de cl_time)
%   maree : Maree (m)
%   mareeFiltree : Maree filtree (m)
%   flag  : 1=OK, 0=KO
% 
% EXEMPLES :
%   nomFic = getNomFicDatabase('ExNiwa.tide')
%   read_TideNiwa(nomFic)
%   [flag, t, maree] = read_TideNiwa(nomFic);
%
% See also read_Tide ecrFicMareeCar Authors
% Authors : JMA
%-----------------------------------------------------------------------

function  [flag, t, maree] = read_TideNiwa(nomFic)

% TODO : refondre cette fonction tel que c'est fait dans lecFicNav_GenavirAUV

messageAboutTide('NIWA')

flag  = 0;
t     = [];
maree = [];

N = countLines(nomFic);
if isnan(N)
    messageErreurFichier(nomFic, 'ReadFailure');
    return
end

N = N - 1;
date  = NaN(N,1);
heure = NaN(N,1);
maree = NaN(N,1);

fid = fopen(nomFic, 'r');
tline = fgetl(fid); %#ok<NASGU>
for k=1:N
    tline = fgetl(fid);
    if ~isempty(tline)
        mots = strsplit(tline);
        date(k)  = dayStrGB2Ifr(mots{1});
        heure(k) = hourStr2Ifr(mots{2});
        maree(k) = str2double(mots{3});
    end
end
fclose(fid);
subNaN = ~isnan(date);
date  = date(subNaN);
heure = heure(subNaN);
maree = maree(subNaN);

%% Lecture de l'heure associ�e aux pings sonar

t = cl_time('timeIfr', date, heure);

%% Auto-plot

if nargout == 0
    FigUtils.createSScFigure; PlotUtils.createSScPlot(t, maree); grid on;
    title(['Tide : ' nomFic], 'Interpreter', 'None')
end

flag = 1;
