% Lecture de donnees de maregraphe au format Simrad
%
% Syntax
%   [t, maree] = read_TideSimrad(nomFic)
% 
% INTPUT PARAMETERS :
%   nomFic      : Nom du fichier de maree
% 
% Output Arguments
%   []    : Auto-plot activation
%   t     : Temps (instance de cl_time)
%   maree : maree (m)
%
% EXEMPLES :
%   nomFic = getNomFicDatabase('ExKongsberg.tide');
%   read_TideSimrad(nomFic)
%
%   [t, maree] = read_TideSimrad(nomFic);
%   plot(t, maree); grid on;
%
% % Remarks : The format of this file is :
% ( Tide 1.00 Belgica 0001 200203041435 200203081308 )
% 200203041435 -2.63
% 200203041436 -2.66
% ...
%
% See also ecrFicMareeCar Authors
% Authors : JMA
%-----------------------------------------------------------------------

function  [flag, t, maree] = read_TideSimrad(nomFic)

% TODO : refondre cette fonction tel que c'est fait dans lecFicNav_GenavirAUV

messageAboutTide('Simrad')

flag  = 0;
t     = [];
maree = [];

N = countLines(nomFic);
if isnan(N)
    messageErreurFichier(nomFic, 'ReadFailure');
    return
end

fid = fopen(nomFic);
if fid == -1
    messageErreurFichier(nomFic, 'ReadFailure');
    return
end
L = fgetl(fid);

Slash = '/';
if (length(L) >= 26) && strcmp(L(3), Slash) && strcmp(L(6), Slash) && strcmp(L(15), ':') && strcmp(L(18), ':')
    Format = 'DD/MM/YYYY HH:MM:SS XX.XX';
elseif (length(L) >= 24) && strcmp(L(3), Slash) && strcmp(L(6), Slash) && strcmp(L(14), ':') && strcmp(L(17), ':')
    Format = 'DD/MM/YYYY HH:MM:SS XX.XX';
elseif strcmp(L(3), Slash) && strcmp(L(6), Slash) && strcmp(L(14), ':')
    Format = 'DD/MM/YYYY HH:MM XX.XX';
elseif (length(L) >= 24) && strcmp(L(5), Slash) && strcmp(L(8), Slash) && strcmp(L(14), ':') && strcmp(L(17), ':')
    Format = 'YYYY/MM/DD HH:MM:SS XX.XX';
elseif (length(L) >= 50) && strcmp(L(1), '(') && strcmp(L(end), ')')
    Pos = ftell(fid);
    line2 = fgetl(fid);
    fseek(fid, Pos, 'bof');
    mots = strsplit(line2);
    switch length(mots{1})
        case 14
            Format = 'YYYYMMDDHHMMSS XX.XX';
        case 12
            Format = 'YYYYMMDDHHMM XX.XX';
        otherwise
            str1 = 'Type de fichier de mar�e non reconnu.';
            str2 = 'Type of tide file not recognized.';
            my_warndlg(Lang(str1,str2), 1);
            return
    end
else
    Format = 'YYYYMMDDHHMMXX.XX';
end

switch Format
    case 'YYYYMMDDHHMM XX.XX' % Format le plus usuel chez Kongsberg 
        A = fscanf(fid, '%4d%2d%2d%2d%2d %f');
        fclose(fid);
        n = length(A) / 6;
        B = reshape(A, [6 n]);
        date  = dayJma2Ifr(B(3,:), B(2,:), B(1,:));
        heure = hourHms2Ifr(B(4,:), B(5,:), 0);
        maree = -B(6,:); % Attention : les valeurs sont donn�es en n�gatif dans le fichier. On inverse le signe car la nome de SSc est d'avoir des donn�es positives
        maree = setTidePositive(maree, 'read_TideSimrad', 'YYYYMMDDHHMM XX.XX');
        
    case 'DD/MM/YYYY HH:MM:SS XX.XX'
        fseek(fid, 0, 'bof');
        A = fscanf(fid, '%2d%*c%2d%*c%4d%*c%2d%*c%2d%*c%2d%f');
        fclose(fid);
        n = length(A) / 7;
        B = reshape(A, [7 n]);
        date  = dayJma2Ifr(B(1,:), B(2,:), B(3,:));
        heure = hourHms2Ifr(B(4,:), B(5,:), B(6,:));
        maree = B(7,:);
        maree = setTidePositive(maree, 'read_TideSimrad', 'DD/MM/YYYY HH:MM:SS XX.XX');
        
    case 'DD/MM/YYYY HH:MM XX.XX'
        fseek(fid, 0, 'bof');
        A = fscanf(fid, '%2d%*c%2d%*c%4d%*c%2d%*c%2d%*c%f');
        fclose(fid);
        n = length(A) / 6;
        B = reshape(A, [6 n]);
        date  = dayJma2Ifr(B(1,:), B(2,:), B(3,:));
        heure = hourHms2Ifr(B(4,:), B(5,:), zeros(1,n));
        maree = B(6,:);
        maree = setTidePositive(maree, 'read_TideSimrad', 'DD/MM/YYYY HH:MM XX.XX');
        
    case 'YYYYMMDDHHMMXX.XX'
        A = fscanf(fid, '%4d%2d%2d%2d%2d%f');
        fclose(fid);
        n = length(A) / 6;
        B = reshape(A, [6 n]);
        date  = dayJma2Ifr(B(3,:), B(2,:), B(1,:));
        heure = hourHms2Ifr(B(4,:), B(5,:), 0);
        maree = B(6,:);
        maree = setTidePositive(maree, 'read_TideSimrad', 'YYYYMMDDHHMMXX.XX');
        
    case 'YYYYMMDDHHMMSS XX.XX'
        A = fscanf(fid, '%4d%2d%2d%2d%2d%2d %f');
        fclose(fid);
        n = length(A) / 7;
        B = reshape(A, [7 n]);
        date  = dayJma2Ifr(B(3,:), B(2,:), B(1,:));
        heure = hourHms2Ifr(B(4,:), B(5,:), B(6,:));
        maree = B(7,:);
        maree = setTidePositive(maree, 'read_TideSimrad', 'YYYYMMDDHHMMSS XX.XX');
        
    case 'YYYY/MM/DD HH:MM:SS XX.XX'
        A = fscanf(fid, '%4d%*c%2d%*c%2d %2d%*c%2d%*c%2d %f');
        fclose(fid);
        n = length(A) / 7;
        B = reshape(A, [7 n]);
        date  = dayJma2Ifr(B(3,:), B(2,:), B(1,:));
        heure = hourHms2Ifr(B(4,:), B(5,:), B(6,:));
        maree = B(7,:);
        maree = setTidePositive(maree, 'read_TideSimrad', 'YYYY/MM/DD HH:MM:SS XX.XX');
end
t = cl_time('timeIfr', date, heure);

%% Auto-plot

if nargout == 0
    FigUtils.createSScFigure; PlotUtils.createSScPlot(t, maree); grid on;
    title(['Tide : ' nomFic], 'Interpreter', 'None')
end

flag = 1;
