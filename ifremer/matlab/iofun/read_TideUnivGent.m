% Lecture de donnees de maregraphe au format utilis� par l'universit� de Gent en Belgique
%
% Syntax
%   [t, maree, flag] = read_TideUnivGent(nomFic)
% 
% INTPUT PARAMETERS :
%   nomFic : Nom du fichier de maree
% 
% Output Arguments
%   []    : Auto-plot activation
%   t     : Temps (instance de cl_time)
%   maree : Maree (m)
%   mareeFiltree : Maree filtree (m)
%   flag  : 1=OK, 0=KO
% 
% EXEMPLES :
%   nomFic = 
%   read_TideUnivGent(nomFic)
%
%   [t, maree, flag] = read_TideUnivGent(nomFic);
%
% See also read_Tide ecrFicMareeCar Authors
% Authors : JMA
%-----------------------------------------------------------------------

function  [flag, t, maree] = read_TideUnivGent(nomFic)

% TODO : refondre cette fonction tel que c'est fait dans lecFicNav_GenavirAUV

messageAboutTide('UnivGent')

flag  = 0;
t     = [];
maree = [];

N = countLines(nomFic);
if isnan(N)
    messageErreurFichier(nomFic, 'ReadFailure');
    return
end

date  = zeros(N,1);
heure = zeros(N,1);
maree = zeros(N,1);

fid = fopen(nomFic, 'r');
for k=1:N
    tline = fgetl(fid);
    mots = strsplit(tline);
    x = sscanf(mots{1}, '%04d%02d%02d%02d%02d');
    date(k)  = dayJma2Ifr(x(3), x(2), x(1));
    heure(k) = hourHms2Ifr(x(4), x(5), x(6));
    maree(k) = str2double(mots{2});
end
fclose(fid);

%% Lecture de l'heure associ�e aux pings sonar

t = cl_time('timeIfr', date, heure);

%% Auto-plot

if nargout == 0
    FigUtils.createSScFigure; PlotUtils.createSScPlot(t, maree); grid on;
    title(['Tide : ' nomFic], 'Interpreter', 'None')
end

flag = 1;
