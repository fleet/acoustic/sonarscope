% (Tide)
% 20121010000000 -4.91
% 20121010000500 -4.89
% 20121010001000 -4.87

function  [flag, t, maree] = read_TideUnknown(nomFic)

% TODO : refondre cette fonction tel que c'est fait dans lecFicNav_GenavirAUV

messageAboutTide('UnknownOrigin')

flag  = 0;
t     = [];
maree = [];

fid = fopen(nomFic);
if fid == -1
     messageErreurFichier(nomFic, 'ReadFailure');
     return
end

L = fgetl(fid); %#ok<NASGU>

A = fscanf(fid, '%4d%2d%2d%2d%2d%2d %f');
fclose(fid);
n = length(A) / 7;
B = reshape(A, [7 n]);
date  = dayJma2Ifr( B(3,:), B(2,:), B(1,:));
heure = hourHms2Ifr( B(4,:), B(5,:), B(6,:));
maree = B(7,:);

%% Lecture de l'heure associ�e aux pings sonar

t = cl_time('timeIfr', date, heure);

%% Auto-plot

if nargout == 0
    FigUtils.createSScFigure; PlotUtils.createSScPlot(t, maree); grid on;
    title(['Tide : ' nomFic], 'Interpreter', 'None')
end

flag = 1;
