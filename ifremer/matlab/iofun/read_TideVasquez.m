% Lecture de donnees de maregraphe au format Caraibes
%
% Syntax
%   [t, maree, flag] = read_TideVasquez(nomFic, ...)
% 
% INTPUT PARAMETERS :
%   nomFic : Nom du fichier de maree
% 
% Output Arguments
%   []    : Auto-plot activation
%   t     : Temps (instance de cl_time)
%   maree : Maree (m)
%   mareeFiltree : Maree filtree (m)
%   flag  : 1=OK, 0=KO
% 
% EXEMPLES :
%   nomFic = getNomFicDatabase('TideBrehat.dat');
%   read_TideVasquez(nomFic)
%
%   [t, maree, flag] = read_TideVasquez(nomFic);
%
% See also read_Tide ecrFicMareeCar Authors
% Authors : JMA
%-----------------------------------------------------------------------

function  [flag, t, maree] = read_TideVasquez(nomFic)

% TODO : refondre cette fonction tel que c'est fait dans lecFicNav_GenavirAUV

messageAboutTide('Vasquez')

flag  = 0;
t     = [];
maree = [];

N = countLines(nomFic);
if isnan(N)
    messageErreurFichier(nomFic, 'ReadFailure');
    return
end

date  = zeros(N,1);
heure = zeros(N,1);
maree = zeros(N,1);

fid = fopen(nomFic, 'r');
tline = fgetl(fid); %#ok<NASGU>
for k=1:N
    tline = fgetl(fid);
    mots = strsplit(tline, ',');
    
    x = sscanf(mots{4}, '%d%c%d%c%d');
    Jour  = x(3);
    Mois  = x(1);
    Annee = x(5);
    date(k) = dayJma2Ifr(Jour, Mois, Annee);

    heure(k) = str2double(mots{2});
    maree(k) = str2double(mots{3});
end
fclose(fid);

%% Lecture de l'heure associ�e aux pings sonar

t = cl_time('timeIfr', date, heure*(60*60*1000));

%% Auto-plot

if nargout == 0
    FigUtils.createSScFigure; PlotUtils.createSScPlot(t, maree); grid on;
    title(['Tide : ' nomFic], 'Interpreter', 'None')
end

flag = 1;
