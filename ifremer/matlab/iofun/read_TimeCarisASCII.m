function [flag, TimePings] = read_TimeCarisASCII(nomFicTxt)

flag      = 0;
TimePings = [];

%% Ouverture du fichier

fid = fopen(nomFicTxt);
if fid == -1
    messageErreurFichier(nomFicTxt, 'ReadFailure');
    return
end

%% Lecture et analyse de l'ent�te

entete = fgetl(fid); % // Year Day Time Profile Beam Status
entete = regexprep(entete, '\([^\(]*\)', ''); % Suppress the unit given in brackets

mots = strsplit(entete);
indicePing   = [];
indiceBeam   = [];
indiceStatus = [];
indiceYear   = [];
indiceDay    = [];
indiceTime   = [];
if strcmp(mots{1}, '//')
    mots = mots(2:end);
end
for k=1:length(mots)
    if strcmp(mots{k}, 'Profile')
        indicePing = k;
    end
    if strcmp(mots{k}, 'Beam')
        indiceBeam = k;
    end
    if strcmp(mots{k}, 'Status')
        indiceStatus = k;
    end
    if strcmp(mots{k}, 'Year')
        indiceYear = k;
    end
    if strcmp(mots{k}, 'Day')
        indiceDay = k;
    end
    if strcmp(mots{k}, 'Time')
        indiceTime = k;
    end
end
if isempty(indicePing) || isempty(indiceBeam) || isempty(indiceStatus)
    str1 = sprintf('L''ent�te du fichier "%s" ne contient pas les informations "Profile", "Beam" ou "Status".', nomFicTxt);
    str2 = sprintf('The header of "%s" does not contain "Profile", "Beam" ou "Status".', nomFicTxt);
    my_warndlg(Lang(str1,str2), 1);
    fclose(fid);
    return
end

ind = 1;
Format = '';
for k=1:max([indicePing indiceBeam indiceStatus])
    switch k
        case indiceYear
            Format = [Format ' %d']; %#ok<AGROW>
            indYear = ind;
            ind = ind + 1;
        case indiceDay
            Format = [Format ' %d']; %#ok<AGROW>
            indDay = ind;
            ind = ind + 1;
        case indiceTime
            Format = [Format ' %s']; %#ok<AGROW>
            indTime = ind;
            ind = ind + 1;
        case indicePing
            Format = [Format ' %d']; %#ok<AGROW>
            indPing = ind;
            ind = ind + 1;
%         case indiceBeam
%             Format = [Format ' %d']; %#ok<AGROW>
%             indBeam = ind;
%             ind = ind + 1;
%         case indiceStatus
%             Format = [Format ' %s']; %#ok<AGROW>
%             indStatus = ind;
%             ind = ind + 1;
        otherwise
            Format = [Format ' %*s']; %#ok<AGROW>
    end
end
Format = [Format  ' %*[^\n]'];

%% Lecture du fichier

X = textscan(fid, Format); % , 'HeaderLines', 1) ;
fclose(fid);
iPing  = X{indPing};
% iBeam  = X{indBeam};

year = X{indYear};
Day  = X{indDay};
H    = X{indTime}; 

%% Supsampling for only one beam per ping

[~, ia] = unique(iPing);
year = year(ia);
Day = Day(ia);
H = H(ia);

%% Calcul de l'heure

H = cell2str(H);
heure   = str2num(H(:,1:2)); %#ok<ST2NM>
minute  = str2num(H(:,4:5)); %#ok<ST2NM>
seconde = str2num(H(:,7:12)); %#ok<ST2NM>

TimePings = datetime(year, 1, Day, heure, minute, seconde, 'Format', 'yyyy-MM-dd HH:mm:ss.SSS');

flag = 1;

% 2013-283 10:25:32.039