% Lecture de donnees de maregraphe au format utilis� par l'universit� de Gent en Belgique
%
% Syntax
%   [t, X, flag] = read_TideUnivGent(nomFic)
%
% INTPUT PARAMETERS :
%   nomFic : Nom du fichier de donn�es
%
% Output Arguments
%   []   : Auto-plot activation
%   t    : Temps (instance de cl_time)
%   X    : Valeurs
%   flag : 1=OK, 0=KO
%
% EXEMPLES :
%   nomFic =
%   read_yyyymmddhhmmssXxxx(nomFic)
%
%   [t, maree, flag] = read_yyyymmddhhmmssXxxx(nomFic);
%
% See also read_Tide ecrFicMareeCar Authors
% Authors : JMA
%-----------------------------------------------------------------------

function [flag, t, X] = read_yyyymmddhhmmssXxxx(nomFic)

flag = 0;
t    = [];
X    = [];

try
    T = readtable(nomFic, 'Format', '%14c  %f%f%f');
    Datetime = datetime(T.(1), 'InputFormat', 'yyyyMMddHHmmss');
    Datetime.Format = 'yyyy/MM/dd HH:mm:ss';
    
    X = [T.(2) T.(3) T.(4)];
    t = cl_time('timeMat',datenum(Datetime));

catch
    
    N = countLines(nomFic);
    if isnan(N)
        messageErreurFichier(nomFic, 'ReadFailure');
        return
    end
    
    date  = zeros(N,1);
    heure = zeros(N,1);
    X     = zeros(N,1);
    n = 0;
    
    fid = fopen(nomFic, 'r');
    for k=1:N
        tline = fgetl(fid);
        
        mots = strsplit(tline);
        x = sscanf(mots{1}, '%04d%02d%02d%02d%02d');
        if length(x) >= 6
            n = n + 1;
            date(n)  = dayJma2Ifr(x(3), x(2), x(1));
            heure(n) = hourHms2Ifr(x(4), x(5), x(6));
            for k2=2:length(mots)
                X(n,k2-1) = str2double(mots{k2});
            end
        end
    end
    fclose(fid);
    date  = date(1:n);
    heure = heure(1:n);
    X = X(1:n,:);
    
    %% Lecture de l'heure associ�e aux pings sonar
    
    t = cl_time('timeIfr', date, heure);
end

%% Auto-plot

if nargout == 0
    FigUtils.createSScFigure; PlotUtils.createSScPlot(t, X); grid on;
    title(['Signal : ' nomFic], 'Interpreter', 'None')
end

flag = 1;
