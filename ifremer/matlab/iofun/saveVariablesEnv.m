% Sauvegarde dans 2 fichiers .ini des variables de configuration d'exploitation
% de donn�es.
%
% Syntax
%   saveVariablesenv
%
% Input Arguments 
%   Sans objet (variables globales positionn�es) 
%
% Name-Value Pair Arguments 
%   Sans objet
%
% Output Arguments 
%   Sans objet 
% 
% Examples
%   saveVariablesEnv
%
% See also loadVariablesEnv Authors
% Authors : GLU
% -------------------------------------------------------------------------------

function saveVariablesEnv

global IfrTbx %#ok<GVMIS>
global IfrTbxResources %#ok<GVMIS>
global IfrTbxLang %#ok<GVMIS> %#ok<GVMIS> 
global IfrTbxGUISelectImage %#ok<GVMIS> %#ok<GVMIS> 
global IfrTbxTempDir %#ok<GVMIS> %#ok<GVMIS> 
global IfrTbxUseClickRightForFlag %#ok<GVMIS> %#ok<GVMIS> 
global IfrTbxLevelQuestions %#ok<GVMIS> %#ok<GVMIS> 
global IfrTbxGraphicsOnTop %#ok<GVMIS> %#ok<GVMIS> 
global IfrTbxUseLogFile %#ok<GVMIS> %#ok<GVMIS> 
global IfrTbxRepDefInput %#ok<GVMIS> 
global IfrTbxRepDefOutput %#ok<GVMIS> %#ok<GVMIS> 
global SizePhysTotalMemory %#ok<GVMIS> 
global NUMBER_OF_PROCESSORS %#ok<GVMIS>
global UserAcceptSendingMsg %#ok<GVMIS> 
global UserEmailServerAddress %#ok<GVMIS> 
global UserEmailPortNumber %#ok<GVMIS> 

%% Ouverture du fichier de de configuration de SonarScope.

nomFicConf = fullfile(IfrTbxResources, 'Config.txt');
fid = fopen(nomFicConf, 'wt');
if fid == -1
    str1 = sprintf('Impossible de cr�er le fichier "%s", v�rifiez ses droits d''�criture.', nomFicConf);
    str2 = sprintf('Impossible to create file "%s", check write permission on that directory.', nomFicConf);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'ImpossibleCreationNomFicConf');
    return
end
fprintf(fid, '[ENV]\n');
fprintf(fid, 'IfrTbx=%s\n',                     IfrTbx);
fprintf(fid, 'IfrTbxResources=%s\n',            IfrTbxResources);
fprintf(fid, 'IfrTbxLang=%s\n',                 IfrTbxLang);
fprintf(fid, 'IfrTbxGUISelectImage=%s\n',       IfrTbxGUISelectImage);
fprintf(fid, 'IfrTbxUseClickRightForFlag=%s\n', IfrTbxUseClickRightForFlag); 
fprintf(fid, 'IfrTbxLevelQuestions=%s\n',       IfrTbxLevelQuestions);
fprintf(fid, 'IfrTbxGraphicsOnTop=%s\n',        IfrTbxGraphicsOnTop);
fprintf(fid, 'IfrTbxUseLogFile=%s\n',           IfrTbxUseLogFile);
fprintf(fid, 'IfrTbxRepDefInput=%s\n',          IfrTbxRepDefInput);
fprintf(fid, 'IfrTbxRepDefOutput=%s\n',         IfrTbxRepDefOutput);
fprintf(fid, 'IfrTbxTempDir=%s\n',              IfrTbxTempDir);
fprintf(fid, '[SYSTEM]\n');
fprintf(fid, 'SizePhysTotalMemory=%d\n',    SizePhysTotalMemory);
fprintf(fid, 'NUMBER_OF_PROCESSORS=%d\n',   NUMBER_OF_PROCESSORS);
fprintf(fid, '[USER]\n');
fprintf(fid, 'AcceptSendingMsg=%d\n',       UserAcceptSendingMsg);
fprintf(fid, 'EmailServerAddress=%s\n',     UserEmailServerAddress);
fprintf(fid, 'EmailPortNumber=%d\n',        UserEmailPortNumber);
fclose(fid); % Fermeture du fichier d'exploitation de SonarScope.

