% Dans le cas de fichier de mar�e Simrad la valeur est n�gative. La
% convention dans SonarScope est d'avoir des valeurs de mar�e positives

function Maree = setTidePositive(Maree, varargin)

m = mean(Maree, 'omitnan');
if m < 0
    m1 = mean(Maree(Maree < 0), 'omitnan');
%     m2 = nanmean(Maree(Maree > 0))
    if (m1 / m) < 4 % Cas rencontr� avec fichier du NIWA Calypso formated_BOP_site3_2015.txt : la mar�ecentr�e autour de z�ro (m1 / m)= 313
        Maree = -Maree;
        if nargin == 3
            Tag1 = varargin{1};
            Tag2 = varargin{2};
            str1 = sprintf('Attention : la mar�e a �t� automatiquement invers�e par SonarScope dans la fonction %s, Tag=%s. Pouvez-vous envoyer ce fichier de mar�e � sonarscope@ifremer.fr SVP ?', Tag1, Tag2);
            str2 = sprintf('Warning : the tide values have been inversed by SonarScope in function %s, Tag=%s. Could you please send this file to sonarscope@ifremer.fr?', Tag1, Tag2);
            my_warndlg(Lang(str1,str2), 1);
        end
    end
end
