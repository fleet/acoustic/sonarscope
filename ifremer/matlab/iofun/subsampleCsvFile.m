% nomFicCsvIn  = 'D:\Temp\SPFE\20220516\aaaa\0052_20220319_113910_Belgica-WC-EI-Raw.csv';
% nomDirOut = 'D:\Temp\SPFE\20220516\aaaa';
% flag = subsampleCsvFile(nomFicCsvIn, nomDirOut, 20)

function flag = subsampleCsvFile(nomFicCsvIn, nomDirOut, timeStep)

flag = 0;

if ~iscell(nomFicCsvIn)
    nomFicCsvIn = {nomFicCsvIn};
end

N = length(nomFicCsvIn);
hw = create_waitbar('Reducing .csv files', 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    flag = subsampleCsvFile_unitaire(nomFicCsvIn{k}, nomDirOut, timeStep);
end
my_close(hw, 'MsgEnd')


function flag = subsampleCsvFile_unitaire(nomFicCsvIn, nomDirOut, timeStep)

flag = 0;

T = readtable(nomFicCsvIn);
% head(T)

[nbVal, nbVar] = size(T);
varNames = T.Properties.VariableNames;

if any(ismember(varNames, 'Time'))
    T.Time = datetime(T.Time, 'ConvertFrom', 'posixtime');
elseif any(ismember(varNames, 'strTime'))
    T.Time = datetime(T.strTime);
else
    return
end

delta = datenum(0, 0, 0, 0, 0, timeStep);
n = floor(datenum(T.Time - T.Time(1)) / delta);
% figure; plot(n, '.'); grid on;
[~,ia] = unique(n);
kMiddle = floor(mean([ia [ia(2:end); nbVal]], 2));

T2 = table;
for k=1:nbVar
    switch varNames{k}
        case 'Time'
            T2.(varNames{k}) = T.(varNames{k})(kMiddle);
        case 'strTime'
            T2.(varNames{k}) = T.(varNames{k})(kMiddle);
        case 'Heading'
            T2.(varNames{k}) = T.(varNames{k})(kMiddle);
        case {'Latitude'; 'Longitude'}
            X = T.(varNames{k});
            T2.(varNames{k}) = grpstats(X, n+1, 'mean');
        otherwise
            X = T.(varNames{k});
            switch class(X)
                case 'cell'
                    T2.(varNames{k}) = T.(varNames{k})(kMiddle);
                otherwise
                    X = grpstats(X, n+1, 'mean');
                    T2.(varNames{k}) = round(X, 5, 'significant');
            end
    end
end
% head(T2)

%% Création du fichier .csv de sortie

[~, nomFic] = fileparts(nomFicCsvIn);
nomFic = sprintf('%s-%ds.csv', nomFic, timeStep);
nomFicCsvOut = fullfile(nomDirOut, nomFic);
sep = getCSVseparator;
try
    writetable(T2, nomFicCsvOut, 'Delimiter', sep)
    flag = 1;
catch ME
    ME.getReport
end
