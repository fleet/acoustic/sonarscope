% function supprimeFiles : permet de supprimer l'ensemble des fichiers ayant l'extension (ext) �
% partir d'une racine (dirName) dans l'ensemble des r�pertoires et des
% sous-repertoires 
%
% Syntax
%  supprimeFiles(...)
%
% Name-Value Pair Arguments
%   ADU
%
% Name-only Arguments
% 
% Examples
% 
% See also FormationMemmapfile Authors
% Authors : JMA, GLU
%--------------------------------------------------------------------------

function supprimeFiles(dirName,extension)

pppp = fullfile(dirName,['*.' extension]);

delete(pppp);

dirs = dir(dirName);

for k=3:length(dirs)
    file = fullfile(dirName,dirs(k).name);
    supprimeFiles(file,extension);
    [stat, mess, id] = rmdir(file);
end
