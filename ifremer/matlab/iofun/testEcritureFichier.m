% Test d'�criture d'un fichier
%
% Syntax
%   flag = testEcritureFichier(nomFic)
%
% Input Arguments
%   nomFic : Nom du fichier
%
% Output Arguments
%   flag : 1 si oui, 0 si non
%
% Examples
%   nomFic = my_tempname
%   flag = testEcritureFichier(nomFic)
%
% See also Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function flag = testEcritureFichier(nomFic)

fid = fopen(nomFic, 'w+');
if fid == -1
    messageErreurFichier(nomFic, 'WriteFailure');
    flag = 0;
    return
end

fclose(fid);
S = warning;
warning('Off')
delete(nomFic)
warning(S)
flag = 1;
