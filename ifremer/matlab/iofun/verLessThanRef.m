% Function : verLessThanRef
% % Fonction de comparaison de version. Inspir� de verLessThan de MatLab.
% La version est declaree selon la norme SemRev.
%
% Syntax
%   result = verLessThanRef(verRef)
%
% INTPUT PARAMETERS : 
%   swVersion : version � tester.
%   verRef : version de r�ference (selon la norme Sem.Rev)
%
% Output Arguments 
%   result : boolean (0/1)
%
% Examples
%   globeVersion = '2.2.9';
%   verLessThanRef(globeVersion, '2.3.3')
%
%   globeVersion = '2.3.5';
%   verLessThanRef(globeVersion, '2.3.3')
%
% Authors : GLU
% ----------------------------------------------------------------------------

function result = verLessThanRef(swVersion, verRef)

    result = 0;

    swVersionParts = sscanf(swVersion, '%d.%d.%d')';
    if length(swVersionParts) < 3
        swVersionParts(3) = 0; % zero-fills to 3 elements
    end
    if swVersionParts(1) ~=  str2double(verRef(1))     % major version
        result = swVersionParts(1) < str2double(verRef(1));
    elseif swVersionParts(2) ~=  str2double(verRef(2)) % minor version
        result = swVersionParts(2) < str2double(verRef(3));
    else                                  % revision version
        result = swVersionParts(3) < str2double(verRef(5));
    end
