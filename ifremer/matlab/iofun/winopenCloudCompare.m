function winopenCloudCompare(flyFileName, varargin)

[varargin, GLOBAL_SHIFT] = getPropertyValue(varargin, 'GLOBAL_SHIFT', [0 0 0]); %#ok<ASGLU>

%% Recherche du nom de l'executable

try
    pppp = dir('C:\ProgramData\Microsoft\Windows\Start Menu\Programs\CloudCompare');
    pppp = pppp(end);
    aserver = actxserver('WScript.Shell');
    tmp = aserver.CreateShortcut(fullfile(pppp.folder, pppp.name));
    target = tmp.TargetPath;
    if ~exist(target, 'file')
        target = 'C:\Program Files\CloudCompare\CloudCompare.exe';
    end
catch
    target = 'C:\Program Files\CloudCompare\CloudCompare.exe';
end

if ~exist(target, 'file')
    str = 'SSc checked if CloudCompare is installed on your computer. If yes, it could not find where.';
    str = sprintf('%s\n\nIt could not find an icone on your desk and did not find the file "C:\\Program Files\\CloudCompare\\CloudCompare.exe"', str);
    str = sprintf('%s\n\nThis program plots the .ply files very efficiently, you should install it.', str);
    my_warndlg(str, 1)
    target = 'CloudCompare.exe';
end

%% Lancement de CloudCompare

cmd = sprintf('"%s"', target);

for k=1:length(flyFileName)
    cmd = sprintf('%s %s', cmd, flyFileName{k});
end

% ------------------------------------------------------------------------------------------------------
% TODO : voir s'il serait possible de passer les shifts en argument de la
% commande, �a �viterait de devoir le faire en dur dans la fonction ALL_exportPLY

% cmd = sprintf('%s -GLOBAL_SHIFT {%f % f %f}', cmd, GLOBAL_SHIFT(1), GLOBAL_SHIFT(2), GLOBAL_SHIFT(3));
% ------------------------------------------------------------------------------------------------------

cmd = [cmd '&'];
dos(cmd);
