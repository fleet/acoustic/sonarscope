function flag = writeVarXMLBin(nomDir, InfoSignal, Signal)

nomFic = fullfile(nomDir, InfoSignal.FileName);
fid = fopen(nomFic, 'w+');
if fid == -1
    messageErreurFichier(nomFic);
    flag = 0;
    return
end

n = fwrite(fid, Signal, InfoSignal.Storage);
if n ~= numel(Signal)
    messageErreurFichier(nomFic);
    flag = 0;
    fclose(fid);
    return
end
fclose(fid);
flag = 1;
