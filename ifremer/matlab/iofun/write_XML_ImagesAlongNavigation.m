function flag = write_XML_ImagesAlongNavigation(NomFicXML, DataSlices, nbPings, nbRows)

[nomDirRoot, nomDirBin] = fileparts(NomFicXML);
nbSlices = size(DataSlices.LatitudeTop, 1);

%% Definition of the structure

Info.Signature1      = 'SonarScope';
Info.Signature2      = 'ImagesAlongNavigation';
Info.FormatVersion   = 20100624;
Info.Name            = DataSlices.name;
Info.ExtractedFrom   = DataSlices.from;

% En commentaire pour l'instant car GLOBE ne reconnait pas ces donn�es
% Info.SliceKey        = 'Frequencies';

Info.Survey         = 'Unknown';
Info.Vessel         = 'Unknown';
Info.Sounder        = 'Unknown';
Info.ChiefScientist = 'Unknown';

Info.Dimensions.nbPings  = nbPings;
Info.Dimensions.nbSlices = nbSlices;
Info.Dimensions.nbRows   = nbRows;

Data.AcrossDistance = zeros(nbSlices,1); % TODO : � supprimer
Data.SliceExists    = true(nbSlices,1);
Data.SliceName      = DataSlices.SliceName;

Data.Date           = DataSlices.Date;
Data.Hour           = DataSlices.Hour;
Data.PingNumber     = DataSlices.PingNumber;
Data.Tide           = DataSlices.Tide;
Data.Heave          = DataSlices.Heave;

% Data.LatitudeNadir  = DataSlices.LatitudeNadir;
% Data.LongitudeNadir = DataSlices.LongitudeNadir;
Data.LatitudeTop     = DataSlices.LatitudeTop;
Data.LongitudeTop    = DataSlices.LongitudeTop;
Data.LatitudeBottom  = DataSlices.LatitudeBottom;
Data.LongitudeBottom = DataSlices.LongitudeBottom;
Data.DepthTop        = DataSlices.DepthTop;
Data.DepthBottom     = DataSlices.DepthBottom;
if isfield(DataSlices, 'ImageSegment')
    Data.ImageSegment = DataSlices.ImageSegment;
end
if isfield(DataSlices, 'ImageSegment')
    Data.EndData = DataSlices.EndData;
end

if nbSlices > 1
    if nbSlices == 2
        % Version de 3DV de Juillet 2013 ne fonctionne sur 2 slices si Info.MS == 1
        Info.MiddleSlice = 0;
    else
        Info.MiddleSlice = floor(nbSlices/2)-1; % TODO : c'est pas normal de mettre 0, �a devrait �tre 1
    end
end

Info.Signals(1).Name          = 'Date';
Info.Signals(end).Dimensions  = 'nbSlices, 1'; %'1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'Ifremer';
Info.Signals(end).FileName    = fullfile(nomDirBin,'Date.bin');

Info.Signals(end+1).Name      = 'Hour';
Info.Signals(end).Dimensions  = 'nbSlices, 1'; %'1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'nb milliseconds since midnight';
Info.Signals(end).FileName    = fullfile(nomDirBin,'Hour.bin');

Info.Signals(end+1).Name      = 'PingNumber';
Info.Signals(end).Dimensions  = 'nbSlices, 1'; %'1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirBin,'PingNumber.bin');

Info.Signals(end+1).Name      = 'Tide';
Info.Signals(end).Dimensions  = 'nbSlices, 1'; %'1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirBin,'Tide.bin');

Info.Signals(end+1).Name      = 'Heave';
Info.Signals(end).Dimensions  = 'nbSlices, 1'; %'1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirBin,'Heave.bin');

% Mis en commentaire mais devrait certainement y rester.

% Info.Signals(end+1).Name      = 'LatitudeNadir';
% Info.Signals(end).Dimensions  = '1, nbPings';
% Info.Signals(end).Storage     = 'double';
% Info.Signals(end).Unit        = 'deg';
% Info.Signals(end).FileName    = fullfile(nomDirBin, 'LatitudeNadir.bin');
% 
% Info.Signals(end+1).Name      = 'LongitudeNadir';
% Info.Signals(end).Dimensions  = '1, nbPings';
% Info.Signals(end).Storage     = 'double';
% Info.Signals(end).Unit        = 'deg';
% Info.Signals(end).FileName    = fullfile(nomDirBin, 'LongitudeNadir.bin');

Info.Signals(end+1).Name      = 'SliceExists';
Info.Signals(end).Dimensions  = 'nbSlices, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirBin,'SliceExists.bin');

% Info.Signals(end+1).Name      = 'DepthTop';
% Info.Signals(end).Dimensions  = '1, nbPings';
% Info.Signals(end).Storage     = 'single';
% Info.Signals(end).Unit        = 'm';
% Info.Signals(end).FileName    = fullfile(nomDirBin, 'DepthTop.bin');
% 
% Info.Signals(end+1).Name      = 'DepthBottom';
% Info.Signals(end).Dimensions  = '1, nbPings';
% Info.Signals(end).Storage     = 'single';
% Info.Signals(end).Unit        = 'm';
% Info.Signals(end).FileName    = fullfile(nomDirBin, 'DepthBottom.bin');

Info.Signals(end+1).Name      = 'AcrossDistance'; % TODO : � supprimer
Info.Signals(end).Dimensions  = 'nbSlices, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirBin, 'AcrossDistance.bin');


Info.Images(1).Name           = 'LatitudeTop';
Info.Images(end).Dimensions   = 'nbSlices, nbPings';
Info.Images(end).Storage      = 'double';
Info.Images(end).Unit         = 'deg';
Info.Images(end).FileName     = fullfile(nomDirBin, 'LatitudeTop.bin');

Info.Images(end+1).Name       = 'LongitudeTop';
Info.Images(end).Dimensions   = 'nbSlices, nbPings';
Info.Images(end).Storage      = 'double';
Info.Images(end).Unit         = 'deg';
Info.Images(end).FileName     = fullfile(nomDirBin,'LongitudeTop.bin');

% {
% En commentaire pour l'instant car 3DViewer ne reconnait pas ces donn�es
Info.Images(end+1).Name       = 'LatitudeBottom';
Info.Images(end).Dimensions   = 'nbSlices, nbPings';
Info.Images(end).Storage      = 'double';
Info.Images(end).Unit         = 'deg';
Info.Images(end).FileName     = fullfile(nomDirBin, 'LatitudeBottom.bin');

Info.Images(end+1).Name       = 'LongitudeBottom';
Info.Images(end).Dimensions   = 'nbSlices, nbPings';
Info.Images(end).Storage      = 'double';
Info.Images(end).Unit         = 'deg';
Info.Images(end).FileName     = fullfile(nomDirBin, 'LongitudeBottom.bin');

Info.Images(end+1).Name      = 'DepthTop';
Info.Images(end).Dimensions  = 'nbSlices, nbPings';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = 'm';
Info.Images(end).FileName    = fullfile(nomDirBin, 'DepthTop.bin');

Info.Images(end+1).Name      = 'DepthBottom';
Info.Images(end).Dimensions  = 'nbSlices, nbPings';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = 'm';
Info.Images(end).FileName    = fullfile(nomDirBin, 'DepthBottom.bin');

if isfield(DataSlices, 'ImageSegment')
    Info.Images(end+1).Name       = 'ImageSegment';
    Info.Images(end).Dimensions   = 'nbSlices, nbPings';
    Info.Images(end).Storage      = 'uint8';
    Info.Images(end).Unit         = '';
    Info.Images(end).FileName     = fullfile(nomDirBin, 'ImageSegment.bin');
end

if isfield(DataSlices, 'ImageSegment')
    Data.EndData = DataSlices.EndData;
    Info.Images(end+1).Name       = 'EndData';
    Info.Images(end).Dimensions   = 'nbSlices, nbPings';
    Info.Images(end).Storage      = 'uint16';
    Info.Images(end).Unit         = '';
    Info.Images(end).FileName     = fullfile(nomDirBin, 'EndData.bin');
end


% Info.Images(end+1).Name       = 'Reflectivity';
% Info.Images(end).Dimensions   = 'nbRows, nbPings, nbSlices';
% Info.Images(end).Storage      = 'single';
% Info.Images(end).Unit         = 'dB';
% Info.Images(end).FileName     = fullfile(nomDirBin,'Reflectivity.bin');

Info.Strings(1).Name            = 'SliceName';
Info.Strings(end).Dimensions    = 'nbSlices, 1';
% Info.Strings(end).Storage     = 'char';
% Info.Strings(end).Unit        = '';
Info.Strings(end).FileName      = fullfile(nomDirBin, 'SliceName.txt');

%% Cr�ation du fichier XML d�crivant la donn�e

xml_write(NomFicXML, Info);
flag = exist(NomFicXML, 'file');
if ~flag
    messageErreur(NomFicXML)
    return
end

%% Cr�ation des fichiers binaires des Strings

for k=1:length(Info.Strings)
    flag = writeStrings(nomDirRoot, Info.Strings(k), Data.(Info.Strings(k).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:length(Info.Signals)
    flag = writeSignal(nomDirRoot, Info.Signals(k), Data.(Info.Signals(k).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:length(Info.Images)
    flag = writeImage(nomDirRoot, Info.Images(k), Data.(Info.Images(k).Name));
    if ~flag
        return
    end
end

flag = 1;
