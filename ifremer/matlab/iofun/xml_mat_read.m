%XML_READ reads xml files and converts them into Matlab's struct tree.
%
% DESCRIPTION
% tree = xml_read(xmlfile) reads 'xmlfile' into data structure 'tree'
%
% tree = xml_read(xmlfile, Pref) reads 'xmlfile' into data structure 'tree'
% according to your preferences
%
% [tree, RootName, DOMnode] = xml_read(xmlfile) get additional information
% about XML file
%
% INPUT:
%  xmlfile	URL or filename of xml file to read
%  Pref     Preferences:
%    Pref.ItemName - default 'item' - name of a special tag used to itemize
%                    cell arrays
%    Pref.ReadAttr - default true - allow reading attributes
%    Pref.ReadSpec - default true - allow reading special nodes
%    Pref.Str2Num  - default 'smart' - convert strings that look like numbers
%                   to numbers. Options: "always", "never", and "smart"
%    Pref.KeepNS   - default true - keep or strip namespace info
%    Pref.NoCells  - default true - force output to have no cell arrays
%    Pref.Debug    - default false - show mode specific error messages
%    Pref.NumLevels- default infinity - how many recursive levels are
%      allowed. Can be used to speed up the function by prunning the tree.
%    Pref.RootOnly - default true - output variable 'tree' corresponds to
%      xml file root element, otherwise it correspond to the whole file.
%    Pref.CellItem - default 'true' - leave 'item' nodes in cell notation.
% OUTPUT:
%  tree         tree of structs and/or cell arrays corresponding to xml file
%  RootName     XML tag name used for root (top level) node.
%               Optionally it can be a string cell array storing: Name of
%               root node, document "Processing Instructions" data and
%               document "comment" string
%  DOMnode      output of xmlread
%
% DETAILS:
% Function xml_read first calls MATLAB's xmlread function and than
% converts its output ('Document Object Model' tree of Java objects)
% to tree of MATLAB struct's. The output is in format of nested structs
% and cells. In the output data structure field names are based on
% XML tags, except in cases when tags produce illegal variable names.
%
% Several special xml node types result in special tags for fields of
% 'tree' nodes:
%  - node.CONTENT - stores data section of the node if other fields are
%    present. Usually data section is stored directly in 'node'.
%  - node.ATTRIBUTE.name - stores node's attribute called 'name'.
%  - node.COMMENT - stores node's comment section (string). For global
%    comments see "RootName" output variable.
%  - node.CDATA_SECTION - stores node's CDATA section (string).
%  - node.PROCESSING_INSTRUCTIONS - stores "processing instruction" child
%    node. For global "processing instructions" see "RootName" output variable.
%  - other special node types like: document fragment nodes, document type
%   nodes, entity nodes, notation nodes and processing instruction nodes
%   will be treated like regular nodes
%
% Examples
%   MyTree = [];
%   MyTree.MyNumber = 13;
%   MyTree.MyString = 'Hello World';
%   nomFic = fullfile(my_tempdir, 'test.xml')
%   xml_mat_write(nomFic, MyTree);
%   [tree treeName] = xml_mat_read('test.xml');
%   disp(treeName)
%   gen_object_display()
%
% See also:
%   xml_write, xmlread, xmlwrite
%
% Written by Jarek Tuszynski, SAIC, jaroslaw.w.tuszynski_at_saic.com
% References:
%  - Function inspired by Example 3 found in xmlread function.
%  - Output data structures inspired by xml_toolbox structures.

function [tree, RootName, DOMnode] = xml_mat_read(xmlfile, varargin)

[nomDir, nomFic] = fileparts(xmlfile);
if isempty(nomDir)
    nomFicMat = [nomFic, '_xml.mat'];
else
    nomFicMat = fullfile(nomDir, [nomFic '_xml.mat']);
end

if exist(nomFicMat, 'file')
    SMat = dir(nomFicMat);
    SXML = dir(xmlfile);
    if ~isempty(SXML) && (SMat.datenum >= SXML.datenum)
        [tree, RootName, DOMnode] = loadmat(nomFicMat, 'nomVar', {'tree'; 'RootName'; 'DOMnode'});
    else
        [tree, RootName, DOMnode] = xml_read(xmlfile, varargin{:});
        try
            save(nomFicMat, 'tree', 'RootName', 'DOMnode')
        catch ME
            ErrorReport = getReport(ME) %#ok<NOPRT>
            fprintf('%s\n', ErrorReport);
        end
    end
else
    [tree, RootName, DOMnode] = xml_read(xmlfile, varargin{:});
    try
        save(nomFicMat, 'tree', 'RootName', 'DOMnode')
    catch ME
        ErrorReport = getReport(ME) %#ok<NOPRT>
        fprintf('%s\n', ErrorReport);
    end
end
