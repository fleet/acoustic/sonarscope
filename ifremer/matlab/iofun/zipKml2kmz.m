function flag = zipKml2kmz(nomFicKmz, DirTemp)

str = sprintf('Sipping "%s"', nomFicKmz);
WorkInProgress(str)
zip(nomFicKmz, fullfile(DirTemp, '*'))
while ~exist([nomFicKmz, '.zip'], 'file')
    pause(0.5)
end
flag = copyfile([nomFicKmz, '.zip'], nomFicKmz);
if ~flag
    str = sprintf('Impossible to copy file %s to %s.', [nomFicKmz, '.zip'], nomFicKmz);
    my_warndlg(str, 1);
    return
end
% pause(0.5)
delete([nomFicKmz, '.zip'])

flag = rmdir(DirTemp, 's');
if ~flag
    for k=1:60
        flag = rmdir(DirTemp, 's');
        if flag
            break
        end
        pause(1)
    end
    if ~flag
        str1 = sprintf('Attention, le r�pertoire "%s" n''a pas pu �tre supprim� automatiquement.', DirTemp);
        str2 = sprintf('Warning : folder "%s" could not be removed automaticaly.', DirTemp);
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'ExportGoolgleEarthDirNotRemoved');
        flag = 1; % Pour que le .kmz soit affich�
        return
    end
end
