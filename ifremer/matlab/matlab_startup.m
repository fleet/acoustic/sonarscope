% Initialisation de la toolbox "matlab"

function matlab_startup(IfrTbxIfr)

% Initialisation de la toolbox "matlab"
nomDir = fullfile(IfrTbxIfr, 'matlab', 'src');
path(path,  nomDir);
if exist(nomDir, 'dir')
    matlabSrc_startup(nomDir)
end

% disp('Initialisation de la Toolbox iofun');
path(path, fullfile(IfrTbxIfr, 'matlab', 'iofun'));

% disp('Initialisation de la Toolbox general');
path(path, fullfile(IfrTbxIfr, 'matlab', 'general'));
% path(path, fullfile(IfrTbxIfr, 'matlab', 'general', 'DevTools'));
path(path, fullfile(IfrTbxIfr, 'matlab', 'general', 'DiskTools'));
path(path, fullfile(IfrTbxIfr, 'matlab', 'general', 'GeoTools'));
path(path, fullfile(IfrTbxIfr, 'matlab', 'general', 'TimeTools'));
path(path, fullfile(IfrTbxIfr, 'matlab', 'general', 'NMEATools'));

% disp('Initialisation de la Toolbox graphics');
path(path, fullfile(IfrTbxIfr, 'matlab', 'graphics'));
path(path, fullfile(IfrTbxIfr, 'matlab', 'graphics', 'components'));
path(path, fullfile(IfrTbxIfr, 'matlab', 'graphics', 'componentsBase'));
path(path, fullfile(IfrTbxIfr, 'matlab', 'graphics', 'componentsNG'));
path(path, fullfile(IfrTbxIfr, 'matlab', 'graphics', 'applications'));
path(path, fullfile(IfrTbxIfr, 'matlab', 'graphics', 'applications', 'zoom'));
% path(path, fullfile(IfrTbxIfr, 'matlab', 'graphics', 'applications', 'MultiWaitbar'));

% disp('Initialisation de la Toolbox strfun');
path(path, fullfile(IfrTbxIfr, 'matlab', 'strfun'));

% disp('Initialisation de la Toolbox stats');
path(path, fullfile(IfrTbxIfr, 'matlab', 'stats'));

% disp('Initialisation de la Toolbox images');
path(path, fullfile(IfrTbxIfr, 'matlab', 'images'));
path(path, fullfile(IfrTbxIfr, 'matlab', 'images', 'my_imuitools'));
path(path, fullfile(IfrTbxIfr, 'matlab', 'images', 'texture'));
path(path, fullfile(IfrTbxIfr, 'matlab', 'images', 'speckle'));
path(path, fullfile(IfrTbxIfr, 'matlab', 'images', 'bathy'));
path(path, fullfile(IfrTbxIfr, 'matlab', 'images', 'movie'));
path(path, fullfile(IfrTbxIfr, 'matlab', 'images', 'carto'));

% disp('Initialisation de la Toolbox signal');
path(path, fullfile(IfrTbxIfr, 'matlab', 'signal'));

% disp('Initialisation de la Toolbox extern/mex/dll');
path(path, fullfile(IfrTbxIfr, 'extern', 'Mex-CUDA-OpenMP', 'dll'));
% disp('Initialisation de la Toolbox extern/mex/reson/dll');
path(path, fullfile(IfrTbxIfr, 'extern', 'Mex-CUDA-OpenMP', 'reson', 'dll'));
