function [Ex, Sigma] = CenterGravity(x,y)

sub = find(~isnan(y) & ~isnan(x));
xs = x(sub);
ys = y(sub);

% ps = px .^2;
px = ys / sum(ys);
Ex = sum(xs .* px);
M2 = sum(xs.^2 .* px);
V = M2 - Ex^2;
if V > 0
    Sigma = sqrt(V);
else
    Sigma = NaN;
end

