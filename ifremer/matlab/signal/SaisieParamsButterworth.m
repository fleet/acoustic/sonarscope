% Saisie des param�tres du filtre de Butterworth
%
% Syntax
%   [flag, Ordre, Wc] = SaisieParamsButterworth(Ordre, Wc)
%
% Input Arguments
%   Ordre : Ordre du filtre par defaut
%   Wc    : Pulsation de coupure normalis�e par defaut
%
% Output Arguments
%   flag  : 1=OK, 0=Cancel
%   Ordre : Ordre du filtre
%   Wc    : Pulsation de coupure normalis�e
%
% Examples
%   [flag, Ordre, Wc] = SaisieParamsButterworth(2, 0.02)
%
% See also filtfilt Authors
% Authors : JMA
%--------------------------------------------------------------------------

function [flag, Ordre, Wc] = SaisieParamsButterworth(Ordre, Wc)

str1 = sprintf('Param�tres du filtre de Butterworth\n(filtre sans d�calage temporel)');
str2 = sprintf('Butterworth filtre parametres\nZero-phase forward and reverse digital filtering)');

p    = ClParametre('Name', Lang('Ordre du filtre', 'Filter order'), ...
    'Value', Ordre, 'MinValue', 1, 'MaxValue', 10);
p(2) = ClParametre('Name', Lang('Fr�quence de coupure normalis�e', 'Normalized cutoff frequency'), ...
    'Value', Wc, 'MinValue', 0.0001, 'MaxValue', 0.25);
a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
rep = a.getParamsValue;   

Ordre = rep(1);
Wc = rep(2);
