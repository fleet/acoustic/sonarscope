% TODO : renommer cette fonction en controleAngle

function [Signal, Filtre, subRetour] = controleShipLatitude(NomSignal, Signal, varargin)

subBug = isnan(Signal);
subKO = find(subBug);
subOK = find(~subBug);
Signal(subKO) = interp1(subOK, Signal(subOK), subKO);
[Signal, Filtre, subRetour] = controleSignalNEW(NomSignal, Signal, varargin{:});
