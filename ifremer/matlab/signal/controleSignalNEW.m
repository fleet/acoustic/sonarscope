% [Signal, Filtre] = controleSignalNEW('Test', s);
%   figure; plot(Signal); grid on;
%   Filtre
%
% % -------------------------------------------------------------------------
%
% [Signal, Filtre, subRetour subTrous] = controleSignalNEW('Test', s, 'NoInterpolation', 1);
%   figure; plot(Signal); grid on;
%   Filtre
%   subRetour
%   subTrous
%
% [Signal, Filtre, subRetour, subTrous] = controleSignalNEW('Test', s, 'NoInterpolation', 1); % subTrous OK si pas d'interpolation
%   figure; plot(Signal); grid on;
%   Filtre
%   subRetour
%   subTrous % subTrous utile dans ce cas
%
% Filtre = struct('Type', 4, 'Ordre', 2, 'Wc', 1/10);
% [Signal, Filtre, subRetour, subTrous] = controleSignalNEW('Test', s, 'Filtre', Filtre, 'NoInterpolation', 1);
% % WC pas mis � jour dans GUI
%   figure; plot(Signal, '.-'); grid on;
%   Filtre
%   subRetour
%   subTrous
%
% Flags = true(size(Signal)); Flags([25:45 70:90]) = 0;
% [Signal, Filtre, subRetour, subTrous] = controleSignalNEW('Test', s, 'Flags', Flags, 'NoInterpolation', 1);
%   figure; plot(Signal, '.-'); grid on;
%   Filtre
%   subRetour
%   subTrous
%
% Flags = true(size(Signal)); Flags([25:45 70:90]) = 0;
% Filtre = struct('Type', 4, 'Ordre', 2, 'Wc', 1/10);
% [Signal, Filtre, subRetour, subTrous] = controleSignalNEW('Test', s, 'Filtre', Filtre, 'Flags', Flags, 'NoInterpolation', 1);
%   figure; plot(Signal, '.-'); grid on;
%   Filtre
%   subRetour
%   subTrous

% MHO : voir utilisation de flag dans getFlaggedBinsOfAnHistogram et voir ALL_RTK_Clean

function [signalSampleData, filtre, subRetour, subTrous] = controleSignalNEW(signalName, signalSampleData, varargin)

N = size(signalSampleData(:)',2);

[varargin, DataType]        = getPropertyValue(varargin, 'DataType',        SampleType.NORMAL);
[varargin, filtre]          = getPropertyValue(varargin, 'Filtre',          []);
[varargin, NoInterpolation] = getPropertyValue(varargin, 'NoInterpolation', 0);
[varargin, Flag]            = getPropertyValue(varargin, 'Flags',           ones(1,N)); %#ok<ASGLU>

% Creation du YSample & ClSignal
ySample = YSample('data', signalSampleData, 'dataType', DataType);
signal  = ClSignal('name', signalName, 'ySample', ySample);

% Conversion 'Filtre' en FilterDataParametre
if isempty(filtre)
    filterDataParametre = [];
else
    % FIXME filtre.WC ==> mettre valeur num�rique plutot qu'un string
    if ~isfield(filtre, 'Wc')
        filtre.Wc    = 0.1; % Ajout JMA le 05/02/2020
        filtre.Ordre = 2;   % Ajout JMA le 05/02/2020
    end
    filterDataParametre = FilterDataParametre('FilterType', FilterDataParametre.FilterNameList(1), 'ButterworthWc', num2str(filtre.Wc), 'ButterworthOrder', filtre.Ordre);
end

% Conversion 'Flags' end FlagDataParametre ==> TODO
% TODO

% Creation SignalDialog
% TODO flags
%     signalDialog = SignalDialog(signal, 'Title', signalName, 'CleanInterpolation', true, 'filterDataParametre', filterDataParam, 'flags', Flags);
signalDialog = SignalDialog(signal, 'Title', signalName, 'CleanInterpolation', ~NoInterpolation, 'filterDataParametre', filterDataParametre);
signalDialog.openDialog();

% Recup�ration du parametres de filtrage
filterDataParametre= signalDialog.filterDataParametre;

% Conversion FilterDataParametre en 'Filtre'
type = 1;
order =  filterDataParametre.butterworthOrderParam.Value;
wc = filterDataParametre.butterworthWcParam.Value;
filtre = struct('Type', type, 'Ordre', order, 'Wc', wc);


% Conversion FlagDataParametre en 'Flags' ==> TODO
% TODO

subRetour = signal.xSample.data;
signalSampleData = signal.ySample.data;
subTrous = find(isinf(signalSampleData));
signalSampleData(subTrous) = NaN;








% function [Signal, Filtre, subRetour, subTrous] = controleSignalNEW(NomSignal, Signal, varargin)
%
% [varargin, NoInterpolation] = getPropertyValue(varargin, 'NoInterpolation', 0);
%
% [SignalRetour, subRetour, Filtre] = clean_data(Signal(:)', 'YLabel', NomSignal, varargin{:});
% subTrous = 1:length(Signal);
% subTrous(subRetour) = [];
%
% if NoInterpolation
%     Signal(subTrous) = NaN;
%     return
% end
%
% Signal(subRetour) = SignalRetour;
%
% if contains(lower(NomSignal), 'heading') || contains(lower(NomSignal), 'cap')
%     Signal(subTrous) = my_interp1_headingDeg(subRetour, SignalRetour, subTrous, 'linear', 'extrap');
%     flagAngle = 1;
% elseif contains(lower(NomSignal), 'longitude') || contains(lower(NomSignal), 'latitude')
%     Signal(subTrous) = my_interp1_longitude(subRetour, SignalRetour, subTrous, 'linear', 'extrap');
%     flagAngle = 1;
% else
%     Signal(subTrous) = interp1(subRetour, SignalRetour, subTrous, 'linear', 'extrap');
%     flagAngle = 0;
% end
%
% if Filtre.Type ~= 1
%     if contains(lower(NomSignal), 'heading') || ...
%             contains(lower(NomSignal), 'cap') || ...
%             contains(lower(NomSignal), 'longitude')
%         Option = 'Angle';
%     else
%         Option = [];
%     end
%     if isfield(Filtre, 'Wc') && (Filtre.Wc == 0)
%         if flagAngle
%             Signal = filtrageButterSignal(Signal, Filtre.Wc, Filtre.Ordre, Option, 'Angle');
%         else
%             Signal = filtrageButterSignal(Signal, Filtre.Wc, Filtre.Ordre, Option);
%         end
%     end
% end







% % TypeComplete : {[]} | 'Nettoyees' | 'Interpolees' | 'Filtrees'
%
% function [SignalRetour, subRetour, Filtre] = clean_data(valOrig, varargin)
% % function [SignalRetour, subRetour, Filtre, Residu, flagRubber] = clean_data(valOrig, varargin)
%
% N = size(valOrig,2);
% [varargin, x]            = getPropertyValue(varargin, 'x',            1:N);
% [varargin, XLabel]       = getPropertyValue(varargin, 'XLabel',       'X');
% [varargin, YLabel]       = getPropertyValue(varargin, 'YLabel',       []);
% [varargin, TypeComplete] = getPropertyValue(varargin, 'TypeComplete', []);
% [varargin, Filtre]       = getPropertyValue(varargin, 'Filtre',       []);
% [varargin, Title]        = getPropertyValue(varargin, 'Title',        []);
% [varargin, flagFiltre]   = getPropertyValue(varargin, 'flagFiltre',   [1 0]);
% [varargin, Flag]         = getPropertyValue(varargin, 'Flags',        ones(1,N)); %#ok<ASGLU>
%
% if isempty(Filtre)
%     % TODO : cl_data : attendre que l'on puisse injecter Flag
%     am = cl_data('XData', x, 'YData', valOrig', 'XLabel', XLabel, 'YLabel', YLabel, 'Title', Title, ...
%         'Flag', Flag);
% else
%     % TODO : cl_data : attendre que l'on puisse injecter Flag et filtre
%     am = cl_data('XData', x, 'YData', valOrig', 'XLabel', XLabel, 'YLabel', YLabel, 'Title', Title, ...
%         'Filtre', Filtre, 'Flag', Flag);
% end
% am = editobj(am);
% Flag   = get(am, 'Flag');
% Filtre = get(am, 'Filtre');
%
% % flagRubber = any(Flag == 0);
% %
% % Residu = [];
% if ~isfield(Filtre, 'Wc') || (Filtre.Wc == 0)
%     SignalNettoye  = get(am, 'YData')';
% else
%     SignalNettoye  = get(am, 'YDataFiltre')';
%     if isempty(SignalNettoye)
%         SignalNettoye  = get(am, 'YData')';
%     else
% %         SignalNettoye = get(am, 'YData')';
%         SignalNettoyeFiltre = get(am, 'YDataFiltre')';
%         if size(SignalNettoyeFiltre, 1) > 1
%             if ~isdeployed
%                 str1 = 'Message pour JMA : Analyser pb dans clean-data : il s''agit sans doute d''une courbe dont le filtrage a foir�.';
%                 str2 = 'Message for JMA : Analyser pb dans clean-data : il s''agit sans doute d''une courbe dont le filtrage a foir�.';
%                 my_warndlg(Lang(str1,str2), 1);
%             end
%             SignalNettoyeFiltre = SignalNettoyeFiltre(1,:);
%         end
%         %         Residu = SignalNettoyeFiltre - SignalNettoye(1,:);
% %         Residu = SignalNettoye(1,:) - SignalNettoyeFiltre; %% Modif JMA le 21/03/2015 � bord du PP? Retour Pologne
%         SignalNettoye = SignalNettoyeFiltre;
%     end
% end
%
%
%
% subRetour = 1:N;
% PasDeSubscript = 0; % Bidouille pour corriger la maladresse de cl_data
% for k=1:size(valOrig,1)
%     if flagFiltre(k)
%         F = get(am, 'Flag');
%         Flag = (F(:))' & ~isnan(SignalNettoye(k, :)) & ~isinf(SignalNettoye(k, :));
%         subFlag0 = find(~Flag);
%         subFlag1 = find(Flag);
%
%         valInterpolee = SignalNettoye(k,:);
%         if ~isempty(subFlag0)
%             valInterpolee(subFlag0) = interp1(x(subFlag1), SignalNettoye(k, subFlag1), x(subFlag0), 'linear', 'extrap');
%         end
%
%         if length(valInterpolee) > 6
%             [B,A] = butter(2, 0.05);
%             Y = filtfilt(B, A, double(valInterpolee));
%         else
%             Y = valInterpolee;
%         end
%
%         if isempty(TypeComplete)
%             % SignalRetour(k,:) = valOrig(k,subFlag1);
%             SignalRetour(k,:) = SignalNettoye(k,subFlag1); %#ok<AGROW>
%             subRetour = subFlag1;
%         else
%             switch TypeComplete
%                 case 'Nettoyees'
%                     SignalRetour  = get(am, 'YData')'; % Rajout� par JMA le 14/04/2014
%                     SignalRetour = SignalRetour(:,subFlag1);
%                     SignalRetour(k,:) = valOrig(k,subFlag1);
%                     subRetour = subFlag1;
%                 case 'Interpolees'
%                     SignalRetour(k,:) = valInterpolee; %#ok<AGROW>
%                 case 'Filtrees'
%                     SignalRetour(k,:) = Y; %#ok<AGROW>
%             end
%         end
%     else
%         if PasDeSubscript
%             SignalRetour(k,:) = valOrig(k,:); %#ok<UNRCH>
%             subRetour = 1:size(valOrig,2);
%         else
%             subFlag1 = find(Flag);
%             SignalRetour(k,:) = valOrig(k,subFlag1); %#ok<AGROW>
%             subRetour = subFlag1;
%         end
%     end
% end
