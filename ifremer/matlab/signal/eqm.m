% Calcul de l'Erreur Quadratique Moyenne entre un signal et un modele
%
% Syntax
%   erreur = eqm(signal, modele)
% 
% Input Arguments 
%   signal : Signal experiemental
%   modele : Signal modelise
%
% Output Arguments 
%   erreur : Erreur quadratique moyenne.
%   indMin : Index du modele le plus proche
%
% Examples
%   modele = 1:100;
%   signal = modele + rand(size(modele));
%   eqm(signal, modele)
%
%   modele = rand(2,100);
%   signal = rand(4,100);
%   [erreur, indMin] = eqm(signal, modele)
%
% See also Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [erreur, indMin] = eqm(signal, modele)

[nbSignaux, N] = size(signal);
if (N == 1) && (size(modele, 1) == 1) && (size(modele, 2) > 1)
    modele = modele';
end
nbModeles = size(modele, 1);
not_nan = ~isnan(modele); % Modif VB 25/06/04

for i=1:nbSignaux
    for j=1:nbModeles
        d = signal(i,not_nan) - modele(j,not_nan);
        %             erreur(i,j) = sum(d .* d) / N;
        erreur(i,j) = sqrt(sum(d .* d)) / sum(not_nan);  %#ok   % Modif JMA 30/04/03
    end
    [~, ind] = min(erreur(i,:));
    indMin(i) = ind; %#ok
end
