% val = rand(1,10);
% Wc = 0.1;
% Ordre = 2;
% valFiltree = filtrageButterSignal(val, Wc, Ordre)

function  valFiltree = filtrageButterSignal(val, Wc, Ordre, varargin)

[varargin, flagAngle] = getPropertyValue(varargin, 'isAngle', false); %#ok<ASGLU>

[B,A] = butter(Ordre, Wc);

valFiltree = val;
sub = ~isnan(val);
if length(sub) < (3*Ordre+1)
    return
end

if flagAngle % Le signal est un cap, ou une longitude en degres
%     % Filtrage d'un cap en degres
%     val(sub) = val(sub) * (pi/180);
%     valFiltree(sub) = unwrap(val(sub));
%     
%     valFiltree(sub) = filtfilt(B, A, valFiltree(sub));
%     valFiltree(sub) = valFiltree(sub) * (180/pi);
%     valFiltree(sub) = mod(valFiltree(sub)+360, 360);
%     
%     if (min(val(sub)) < 0)
%         valFiltree = valFiltree - 360;
%     end

    % Filtrage d'un cap en degres
    x = filtfilt(B, A, double(cosd(val(sub))));
    y = filtfilt(B, A, double(sind(val(sub))));
    valFiltree(sub) = atan2(y,x) * (180/pi);
    valFiltree(sub) = mod(valFiltree(sub)+360, 360);
    if (min(val(sub)) < 0)
        valFiltree = valFiltree - 360;
    end

else
    valFiltree(sub) = filtfilt(B, A, double(val(sub)));
end

if nargout == 0
    FigUtils.createSScFigure;
    subplot(2,1,1); PlotUtils.createSScPlot(val, 'b'); grid on; hold on; plot(valFiltree, 'r'); hold off;
    subplot(2,1,2); PlotUtils.createSScPlot(val-valFiltree, 'b'); grid on;
end
