% N = 1000;
% valIn = sin(linspace(0,10*pi,N)) + rand(1,N);
% valIn(1:50:end) = 5;
% valIn(end) = -5;
% Wc = 0.01;
% Ordre = 2;
% valFiltree = filtrageButterSignalProgressif(valIn, Wc, Ordre);
% 
% FiltreNav.Type  = 5;
% FiltreNav.Ordre = 2;
% FiltreNav.Wc    = 0.01;
% [Longitude, FiltreNav] = controleSignalNEW('Test filtrageButterSignalProgressif', valIn, 'Filtre', FiltreNav);

function  valOut = filtrageButterSignalProgressif(valIn, Wc, Ordre, varargin)

[varargin, flagAngle] = getPropertyValue(varargin, 'isAngle', false); %#ok<ASGLU>

k = 0;
while 1
    k = k + 1;
    Y{k} = filtrageButterSignal(valIn, Wc, Ordre, 'isAngle', flagAngle); %#ok<AGROW>
    Wc = Wc * 1.1;
    T(k) = floor(1 /Wc); %#ok<AGROW>
    if Wc > 0.1
        break
    end
end
valOut = Y{1};

% figure; h = plot(valIn); grid on; hold on; set(h, 'Color', [0.5 0.5 0.5])

N = length(valIn);
for k=2:length(T)
    sub = [1:T(k) N-T(k)+1:N];
    valOut(sub) = Y{k}(sub);
%     plot(Y{k});
end

% h = plot(valOut, 'r'); grid on; set(h, 'LineWidth', 2);
