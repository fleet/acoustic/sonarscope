% Filtrage des valeurs fausses d'un signal
%
% Syntax
%   s = filtre_erreurs(signal, ...)
%
% Input Arguments
%   signal : Signal comportant des erreurs
%
% Name-Value Pair Arguments
%   EcartMax : Ecart maximum entre 2 echantillons
%
% Output Arguments
%   [] : Auto-plot activation
%   s  : Signal filtre.
%
% Remarks : On compare la valeur des echantillons a la valeur mediane,
%           si une valeur est superieure a l'ecart max alors c'est une erreur
%           on r�alise ensuite l'interpolation des erreurs.
%
% Examples
%   signal = load('/home/doppler/tmsias/gahagnon/Developpement/Solen.mat');
%   signal = load('Q:/Developpement/Solen.mat');
%   signal = signal.I;
%
%   filtre_erreurs(signal);
%
%   s = filtre_erreurs(signal, 'EcartMax', 10);
%   figure; plot(signal, 'b'); grid on; hold on; plot(s, 'r');
%
% See also Authors
% Authors : SG + JMA
%-------------------------------------------------------------------------------

function varargout = filtre_erreurs(signal, varargin)

% --------------------------------------
% Recuperation des parametres optionnels

[varargin, EcartMax] = getPropertyValue(varargin, 'EcartMax', []); %#ok<ASGLU>

% ---------------------------------
% Recherche automatique de EcartMax

if isempty(EcartMax)
    EcartMax = 3 * std(signal);
end

% Valeur mediane du signal

im = median(signal);

% Recherche des erreurs

sub = find(abs(signal-im) > EcartMax);

% Interpolation des erreurs

x = 1:length(signal);
x(sub) = [];
s = signal;
y = signal;
y(sub) = [];

yp = interp1(x, y, sub);
s(sub) = yp;

% ------
% Sortie

if nargout ~= 0
    varargout{1} = s;
else
    figure; plot(signal, 'b'); grid on; hold on; plot(s, 'r'); plot(sub, signal(sub), '*k');
    legend({'Signal brut';'Signal nettoye';'Erreurs detectees'})
end
