
%======================================================================
%
% Version 2.00
%
% This subroutine excludes spike noise from Acoustic Doppler
% Velocimetry (ADV) data using phasce-space method by Goring and
% Nikora (2002).
%
%======================================================================
%
% Input
%   fi  : input data with dimension (n,1)
%   Fig : Numero de la figure ([] sinon)
%
% Output
%   fo     : output (filterd) data
%   ip     : excluded array element number in xi and yi
%
% Example:
%   N = 300;
%   fi = 1:N;
%   sub = 2:7:N;
%   fi(sub) = fi(sub) + 100*(rand(size(sub))-0.5);
%   figure; plot(fi); grid on;
%   [fo, ip] = func_despike_phasespace(fi, 9, 'Test')
%
% for iTer=1:5
%     [Z2, subOK, subKO] = func_despike_phasespace(fi, 9, 'Test');
%     if ~isempty(subKO)
%         fi(subKO) = NaN;
%     end
% end
%
%======================================================================
% Terms:
%
%       Distributed under the terms of the terms of the
%       GNU General Public License
%
% Copyright: Nobuhito Mori, Osaka City University
%
%========================================================================
%
% Update:
%       2.00    2004/12/29 Major bug has been fixed
%       1.10    2004/09/06 Iteration is added
%       1.00    2004/09/01 Nobuhito Mori, Osaka City University
%
%========================================================================

function [fiOut, subOKOut, subKOOut] = func_despike_phasespace(fi, Fig, nomSignal, varargin)

fiOut = fi;
sub1 = ~isnan(fi) & ~isinf(fi);
[flag, y] = func_despike_phasespace2(fi(sub1), Fig, nomSignal, varargin{:});
if ~flag
    subKOOut = [];
    subOKOut = 1:length(fiOut);
    return
end
fiOut(sub1) = y;
subOKOut = find(~isnan(fiOut));
subKOOut = find(isnan(fiOut));


function [flag, fo, subOK, subKO] = func_despike_phasespace2(fi, Fig, nomSignal, varargin)

[varargin, Threshold] = getPropertyValue(varargin, 'Threshold', []); %#ok<ASGLU>

flag = 1;

%{
% Tentative pour d�celer si le signal est trop propre (ligne droite)
sub = find(~isnan(fi));
p = polyfit(sub, double(fi(sub)), 1);
YDataFitted = polyval(p, sub);
residu = fi(sub) - YDataFitted;
rangeResidu = range(residu)    
rangeFi = range(fi)
rangeFi / rangeResidu
%}

[fo, subKO] = despike(fi, Fig, nomSignal);
if isempty(fo) || all(isnan(fo))
    flag = 0;
    subOK = [];
    return
end

% figure; plot(fi(subNonNaN)); grid on; hold on; plot(fo, 'or');

if isempty(Threshold)
    Threshold = range(fi) / 100;
end
if ~isempty(Threshold)
    subOK2 = find(~subKO);
    subKO2 = find(subKO);
    if length(subOK2) < 2
        flag = 0;
        return
    end
%     figure; plot(subOK2, fi(subOK2), '*b'); grid on; hold on; plot(subKO2, fi(subKO2), '*r');
    
    y2 = interp1(subOK2, fi(subOK2), subKO2);
    subSupThreshold = (abs(y2 - fi(subKO2)) < Threshold);
    subKO(subKO2(subSupThreshold)) = 1;
    %{
figure; plot(fi); grid on; hold on;
plot(subSupThreshold, fi(subSupThreshold), 'or');
plot(subKO, fi(subKO), 'xg');
hold off;
    %}
end
subOK = setdiff(1:length(fi), subKO);


function [fo, ip] = despike(fi, Fig, nomSignal)

n      = length(fi);
f_mean = mean(fi, 'omitnan');
f      = fi - f_mean;
lambda = sqrt(2*log(n));


f = f - mean(f, 'omitnan');
s = std(f, 'omitnan');

% step 1: first and second derivatives
f_t  = gradient(f);
f_tt = gradient(f_t);

% step 2: calculation of std of f, ft, ftt
s_t  = std(f_t, 'omitnan');
s_tt = std(f_tt, 'omitnan');

% step 3: angle
theta = atan2(sum(f.*f_tt), sum(f.^2));

% step 4: estimation of a and b
f1  =   f * cos(theta) + f_tt * sin(theta);
f2  = - f * sin(theta) + f_tt * cos(theta);
s_1 = std(f1);
s_2 = std(f2);
a_3 = lambda * s_1;
b_3 = lambda * s_2;

% step 5: checking outlier in the phase space
if ~isnan(a_3) && ~isnan(b_3) && ~isinf(a_3) && ~isinf(b_3)
    a_1 = lambda * s;
    b_1 = lambda * s_t;
    a_2 = lambda * s_t;
    b_2 = lambda * s_tt;
    % f-f_ft
    [x1_p, y1_p, ip1] = func_excludeoutlier_ellipsoid(  f,  f_t, a_1, b_1, 0);
    % f-f_ft
    [x2_p, y2_p, ip2] = func_excludeoutlier_ellipsoid(f_t, f_tt, a_2, b_2, 0);
    % f_t-f_ft
    [x3_p, y3_p, ip3] = func_excludeoutlier_ellipsoid(  f, f_tt, a_3, b_3, theta);
    
    if isempty(x3_p)
        fo = [];
        ip = [];
        return
    end
else
    fo = [];
    ip = [];
    return
end

%% Excluding data

f(ip1) = NaN;
f(ip2) = NaN;
f(ip3) = NaN;

%{
% Test : au lieu de prendre la r�union, on prend l'intersection : r�sultat non concluent
N = length(f);
sub1 = false(1,N);
sub2 = false(1,N);
sub3 = false(1,N);
sub1(ip1) = true;
sub2(ip2) = true;
sub3(ip3) = true;
% f(sub1 & sub2 & sub3) = NaN;
f(sub1 | sub2 | sub3) = NaN;
%}

if all(isnan(f))
    fo = [];
    ip = [];
    return
end

%% For check

if ~isempty(Fig) && ~isnan(a_3) && ~isnan(b_3)
    t = 0:(pi/64):(2*pi);
    ip = find(isnan(f));
    x1_e = a_1 * cos(t);
    x2_e = a_2 * cos(t);
    x3_t = a_3 * cos(t);
    y1_e = b_1 * sin(t);
    y2_e = b_2 * sin(t);
    y3_t = b_3 * sin(t);
    x3_e = x3_t * cos(theta) - y3_t * sin(theta);
    y3_e = x3_t * sin(theta) + y3_t * cos(theta);
    
    figure(Fig);
    str1 = 'Filtrage de spike. (Algoritme provenant de Nobuhito Mori, Osaka City University)';
    str2 = 'Spike filter. (Algoritm coming from Nobuhito Mori, Osaka City University)';
    set(Fig, 'Name', Lang(str1,str2));
    subplot(2,2,1);
    plot(x1_e, y1_e, 'b-');
    grid on;
    hold on
    plot(f, f_t, 'k.');
    plot(x1_p, y1_p, 'r.');
    hold off
    xlabel('u');
    ylabel('\Delta u');
    
    subplot(2,2,3);
    plot(x3_e, y3_e, 'b-');
    hold on
    grid on;
    plot(f, f_tt, 'k.');
    plot(x3_p, y3_p, 'r.');
    hold off
    ylabel('\Delta u');
    ylabel('\Delta^2 u');
    
    subplot(2,2,2);
    plot(x2_e, y2_e, 'b-');
    hold on
    grid on;
    plot(f_t, f_tt, 'k.');
    plot(x2_p, y2_p, 'r.');
    hold off
    xlabel('\Delta u');
    ylabel('\Delta^2 u');
    
    subplot(2,2,4);
    plot(fi, 'k-');
    hold on
    grid on;
    plot(ip, fi(ip), 'ro', 'MarkerSize',5);
    hold off
    xlabel('Samples');
    if isempty(nomSignal)
        ylabel('u');
    else
        ylabel(['u : ' nomSignal]);
    end
    axis tight
    %         title(nomSignal, 'Interpreter', 'none')
    %pause
end

fo = f + f_mean;
ip = isnan(fo);




%======================================================================
%
% Version 2.11
%
% This program excludes the points outside of ellipsoid in two-
% dimensional domain
%
% Input
%   xi : input x data
%   yi : input y data
%   a  : the major axis
%   b  : the minor axis
%
% Output
%   xp : excluded x data
%   yp : excluded y data
%   ip : excluded array element number in xi and yi
%
% Example:
%   [xp, yp, ip] = func_excludeoutlier_ellipsoid(f, f_t, a, b);
%
%
%======================================================================
% Terms:
%
%       Distributed under the terms of the terms of the
%       GNU General Public License
%
% Copyright: Nobuhito Mori, Osaka City University
%
%========================================================================
%
% Update:
%       2.11    2005/01/14 Minor bug has been fixed
%       2.10    2005/01/12 Minor bug has been fixed
%       2.00    2004/12/29 Major bug has been fixed
%       1.01    2004/09/06 Bug fixed
%       1.00    2004/09/01 Nobuhito Mori, Osaka City University
%
%========================================================================

function [xp, yp, ip] = func_excludeoutlier_ellipsoid( xi, yi, a, b, theta )

n = max(size(xi));

xp = [];
yp = [];
ip = [];

%% Rotate data

if theta == 0
    X = xi;
    Y = yi;
else
    X =   xi*cos(theta) + yi*sin(theta);
    Y = - xi*sin(theta) + yi*cos(theta);
end

%% Main

m = 0;
for k=1:n
    x1 = X(k);
    y1 = Y(k);
    % point on the ellipsoid
    if x1 ~= 0
        x2 = 1 / sqrt(1/a^2 + 1/b^2*(y1^2/x1^2) );
    else
        x2 = a;
    end
    y2 = sqrt((1 - x2^2/a^2)*b^2 );
    if x1 < 0
        x2 = -x2;
    end
    if y1 < 0
        y2 = -y2;
    end
    dis = (x2^2+y2^2) - (x1^2+y1^2);
    if dis < 0
        m = m + 1;
        ip(m) = k; %#ok<AGROW>
        xp(m) = xi(k); %#ok<AGROW>
        yp(m) = yi(k); %#ok<AGROW>
    end
end
