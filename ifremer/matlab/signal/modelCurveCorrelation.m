function modelCurveCorrelation(x, C, Axe, flag_time)

deltax = x(2)-x(1);

n = length(C);
milieu = floor(n/2);
x = (-milieu:milieu) * deltax;
subDroite = milieu:n;

[MaxC, kMax] = max(abs(C));
ixMaxC = milieu - kMax;

PlotUtils.createSScPlot(Axe, x(subDroite), C(subDroite), '*'); grid on; axis xy; axis tight;
str1 = sprintf('Le d�calage trouv� est de %s', num2str(ixMaxC));
str2 = sprintf('Offset is %s', num2str(ixMaxC));
disp(Lang(str1,str2))

% sub = find(x > 0);
sub = kMax:length(x);

signekMax = sign(C(kMax));
C = C * (signekMax / MaxC);
% C = C / MaxC;

Params = optimCosinusAmorti(x(sub), C(sub), 'Edit', 1);
yModele = cosinusAmorti(x + x(milieu+ixMaxC), Params);

yModele = yModele * MaxC * signekMax;

hold on; PlotUtils.createSScPlot(x(subDroite), yModele(subDroite), 'k');

Amplitude   = Params(1);
Attenuation = Params(2);
Periode     = Params(3);

str = 'Cross-correlation';
S   = '$A{e^{-{\frac {x}{a}}}}\cos \left( 2\,{\frac {\pi \,x}{T}} \right) $';
if flag_time % L'axe des X est un temps (unite matlab)
    str = sprintf('%s : y=A*exp(-x/Alpha)*cos(2*pi*x/T) : A=%s Alpha=%s T=%s (%s s)', ...
        str, num2str(Amplitude), num2str(Attenuation), num2str(Periode), num2str(Periode * (24*3600)));
else
    str = sprintf('%s : %s : T=%s Alpha=%s A=%s', ...
        str, S, num2str(Periode), num2str(Attenuation), num2str(Amplitude));
end
h = title(str);
set(h, 'Interpreter', 'latex', 'fontsize', 14)

h = text(n/5, 0.6, S);
set(h, 'Interpreter', 'latex', 'fontsize', 12)
xlabel(str, 'Interpreter', 'latex')
