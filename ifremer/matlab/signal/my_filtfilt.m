function val = my_filtfilt(B, A, val, Wc)

N = floor((2*pi) / Wc);
N = min(N, floor(length(val)/2));

sub = ~isnan(val);
val = double(val');
y1 = filter(B, A, val(sub));
y1(1:N) = val(1:N);
y1 = fliplr(y1);
y2 = filter(B, A, y1);
y2(1:N) = y1(1:N);
val(sub) = fliplr(y2);
