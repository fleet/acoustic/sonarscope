% Estimation des parametres d'un signal compose d'une sommation de sinusoides amorties :
% S = S1 + S2 + ... avec Si = Ai * exp(-x/ai) * Acos(2*pi/Ti*x + phii)
%
% Syntax
%   parametres = optimExpCos(X, S, ordre)
%
% Input Arguments
%   S     : Signal inconnu a modeliser
%   ordre : Nombre de sinusoides amorties
%
% Name-Value Pair Arguments
%   DeltaT : Ecart temporel entre les echantillons
%
% Output Arguments
%   []     : auto-plot activation
%   parametres    : parametres du signal (ordre, 4) -> (Ai=amplitude, a=Amortissement, T=periode, phi=phase)
%
% Examples
%   pas = 0.01;
%   x = 0:pas:250;
%
%   p(1) = 1*2; parametres{1} = [1 50 20 0];
%   p(2) = 2*2; parametres{2} = [1 50 20 0 ; 0.5 70 15 0];
%   p(3) = 3*2; parametres{3} = [1 50 20 0 ; 0.5 70 15 0 ; 0.75 30 30 0]
%   p(4) = 4*2; parametres{4} = [1 50 20 0 ; 0.5 70 15 0 ; 0.25 10 10 0 ; 0.1 30 30 0]
%   p(5) = 1*1; parametres{5} = [1 50 Inf 0]
%   p(6) = 1*1; parametres{6} = [1 50 Inf 0 ; 0.5 20 Inf 0]
%   p(7) = 2*1; parametres{7} = [1 70 Inf 0 ; 0.5 10 Inf 0]
%
%   % Test de signaux purement theoriques : on impose le nombre de modes
%   for i=1:length(parametres)
%       s = syntheseProny(x, parametres{i});
%       params = optimExpCos(s, p(i), 'DeltaT', pas)
%         syntheseProny(x, parametres{i});
%         sModele = syntheseProny(x, params);
%         hold on; plot(x, sModele, 'r')
%         str = sprintf('Parametres de synthese : %s', num2strCode(parametres{i})); disp(str);
%         str = sprintf('Ordre recherch         : %d', p(i)); disp(str);
%         str = sprintf('Parametres trouves     : %s', num2strCode(params)); disp(str);
%         disp('Hit any key to continue'); pause;
%   end
%
%   % Test de theoriques. On impose le nombre de modes mais on fait varier le pas d'echantillonnage
%   pas = [0.01 0.1 1 2 5 10 20 50]
%   for i=1:length(pas)
%       x = 0:pas(i):250;
%       s = syntheseProny(x, parametres{4});
%       params = optimExpCos(s, p(4) ,'DeltaT', pas(i))
%         syntheseProny(x, parametres{4});
%         sModele = syntheseProny(x, params);
%         hold on; plot(x, sModele, 'r')
%         str = sprintf('Parametres de synthese : %s', num2strCode(parametres{4})); disp(str);
%         str = sprintf('Ordre recherch         : %d', p(4)); disp(str);
%         str = sprintf('Pas d''echantillonnage : %f', pas(i)); disp(str);
%         str = sprintf('Parametres trouves     : %s', num2strCode(params)); disp(str);
%         disp('Hit any key to continue'); pause;
%   end
%
%   % Test de signaux bruites. On impose le nombre de modes mais on fait varier le pas d'echantillonnage
%   pas = [0.01 0.1 1 2 5 10 20 50]
%   for i=1:length(pas)
%       x = 0:pas(i):250;
%       s = syntheseProny(x, parametres{1});
%       s = s + (rand(size(s)) - 0.5) * 0.01;
%       params = optimExpCos(s, p(1) ,'DeltaT', pas(i))
%         syntheseProny(x, parametres{1});
%         sModele = syntheseProny(x, params);
%         hold on; plot(x, sModele, 'r')
%         str = sprintf('Parametres de synthese : %s', num2strCode(parametres{1})); disp(str);
%         str = sprintf('Ordre recherch         : %d', p(1)); disp(str);
%         str = sprintf('Pas d''echantillonnage : %f', pas(i)); disp(str);
%         str = sprintf('Parametres trouves     : %s en presence de bruit', num2strCode(params)); disp(str);
%         disp('Hit any key to continue'); pause;
%   end
%
% See also Authors
% Authors : FM + LH + JMA
%--------------------------------------------------------------------------

function varargout = optimExpCos(S , ordre, varargin)

[varargin, DeltaT] = getPropertyValue(varargin, 'DeltaT', 1); %#ok<ASGLU>

str = warning;
warning off;

% -----------------------------
% Initialisation des parametres

parametres = [];
S          = S(:)';
P          = ordre ;
% X          = 1:length(S);
N          = length(S);            	% Nombre d'echantillons

% ------------------------------------------------------------------------
% Initialisation des matrices de covariance et d'une matrice de transition

% covariance = zeros(P+1,P+1);
Matrice_transitoire = zeros(N-P, P+1);

% -----------------------
% Calcul de la covariance

for i_col=1:P+1
    Matrice_transitoire(:, i_col) = S(P-i_col+2 : -i_col+1+N)' ;
end

covariance = Matrice_transitoire' * Matrice_transitoire ;

% On enleve la premiere ligne et premiere colonne

covariance_reduit = covariance(2:P+1,2:P+1);
C_inv = inv(covariance_reduit);
absC_inv = abs(C_inv(:));
Probleme = any(isinf(absC_inv)) | any(isnan(absC_inv)) | any((absC_inv > 1e8)); % Matrice de covariance non inversible

if Probleme
    if length(S) <= ordre
        parametres = [0 0 0 0];
    else
        parametres = optimExpCos(S(1:2:end) , ordre, 'DeltaT', DeltaT*2);
        disp('Downsize')
        varargout{1} = parametres;
        return
    end
end
ligne = - covariance(2:P+1,1);

% --------------------------------------
% Affichage utile pour comprendre l'algo

% Nl = size(covariance, 1);
% axeX = (0:Nl-1) * DeltaT;
% figure; imagesc(axeX, axeX, covariance); pixval;
% figure; (plot(axeX, covariance)); grid on;
% hold on; plot(axeX(2:P+1), -ligne, '+'); hold off

if ~Probleme
    
    % ------------------------------------
    % fin de l'estimation de la covariance
    
    coefficients_AR = [1 (C_inv*ligne)' ];
    Zi = roots(coefficients_AR);
    
    %----------------------------------
    % Amortissement et pulsation propre
    
    % indice de l amortissement
    
    Alphai = -DeltaT ./ log( abs(Zi) );
    
    % indice dune pulsation propre
    
    AngleZi = angle(Zi);
    
    
    % ---------------------------------------------
    % Phase et amplitude : matrice de Van der monde
    
    taille = length(S);
    Van_der_monde = zeros(taille,P);
    for i_ligne=1:taille
        Van_der_monde(i_ligne, :) = Zi' .^ (i_ligne-1);
    end
    
    B = ( inv( Van_der_monde' * Van_der_monde ) * Van_der_monde' ) * S';
    Ai = ( abs(B) ./ abs(Zi) );
    indice_complexe = find( imag(Zi) ~= 0);
    Ai( indice_complexe ) = 2* Ai( indice_complexe );            % Dans le cas de racines complexes conjuguees
    phase_Bi = -angle(B);
    Indice_valable = ones( 1, length(Zi) );
    
    % -------------------------------
    % Construction du Signal modelise
    
    taille = length(Zi);
    continu = 1;
    ii = 0;
    mode_significatif = 0;
    parametres=[];
    
    while continu
        ii = ii +1;
        
        if Indice_valable(ii) ~= 0
            
            % Determination des parametres du signal modelise
            
            C_complexe = ~isreal(Zi(ii));
            
            mode_significatif = mode_significatif +1;
            
            % On retourne les parametres du signal
            
            parametres(mode_significatif, :)  = [Ai(ii) Alphai(ii) 2*pi*DeltaT/AngleZi(ii) phase_Bi(ii)]; %#ok<AGROW>
            
            if C_complexe
                ii = ii + 1;
            end
        end
        
        if ii >= taille
            continu = 0;
        end
    end
end

if isempty(parametres)
    parametres = [0 0 0 0];
end

warning(str);

if isempty(nargout)
    x = (0:(length(S)-1)) * DeltaT;
    syntheseProny(x, parametres);
    hold on; plot(x, S, '*r')
else
    varargout{1} = parametres;
end
