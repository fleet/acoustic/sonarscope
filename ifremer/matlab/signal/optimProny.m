% Estimation des parametres d'un signal compose de plusieurs sinusoides amorties : 
% Signal = C + sum(Ai * cos(2*pi*t/Ti + phii) * exp(-t/alphai)) de i=1 a ordre=size(P,1)
% But: modelisation de Prony du signal d'entree, Y. 
% 
% Syntax
%   [P, C, correla] = optimProny(x, y)
% 
% Input Arguments
%   x : Abscisses
%   y : Signal inconnu a modeliser
%
% Output Arguments 
%   P       : Parametres du signal : [A1 a1 T1 phi1 ; A2 a2 ...]-> (Ai=amplitude, ai=Amortissement, Ti=periode, phii=phase)
%   C       : Constante a rajouter
%   correla : Correlation entre chaque mode et lee signal total synthetise.
%
% Examples
%   x = 0:250;
%
%   % Test avec un seul mode
%   s = syntheseProny(x, [1 100 20 0]);
%   [P, C, correla] = optimProny(x, s)
%   optimProny(x, s)
%   optimProny(x, s+10)
%
%   % Test avec deux modes
%   parametres = [1 100 20 0 ; 0.5 70 15 0];
%   s = syntheseProny(x, parametres);
%   optimProny(x, s)
%   [P, C, correla] = optimProny(x, s)
%
%   % Test en presence de bruit
%   parametres = [1 100 20 0 ; 0.5 70 15 0];
%   s = syntheseProny(x, parametres);
%   s = s + (rand(size(s)) - 0.5) * 0.01; 
%   optimProny(x, s)
%   [P, C, correla] = optimProny(x, s)
%
%   % test sur des exponentiels seuls
%   parametres = [1 100 Inf 0 ; 0.5 10 Inf 0];
%   s = syntheseProny(x, parametres);
%   optimProny(x, s)
%   [P, C, correla] = optimProny(x, s)
%    
% See also syntheseProny optimExpCos Authors
% Authors : FM + LH + JMA
% VERSION    : $Id: optimProny.m,v 1.6 2003/02/27 17:41:22 augustin Exp $
%-------------------------------------------------------------------------------

function varargout = optimProny(X, Y, varargin)

if nargin == 0
    return
end

if nargout == 0
    visu = 1;
else
    visu = 0;
end

[varargin, EstimationPeriode] = getFlag(varargin, 'EstimationPeriode'); %#ok<ASGLU>

X = X(:)';
Y = Y(:)';

% ----------------------------------------------------
% ordre maximun explore => lie a la longueur du signal

P_max = min(13, fix(length(Y) / 2) - 1);

% ---------------------------------------------------
% Calcul des parametres pour les "ordre" de 1 a P_max
% On sort de la boucle des qu'une modelisation rend des parametres = 0
% (toutes les modelisations d'ordre superieures rendraient 0)

DeltaT = X(2) - X(1);
for iP=1:P_max  
    
    subNaN = find(isnan(Y));
    if ~isempty(subNaN)
        Y(subNaN) = 0;
    end
    subInf = find(isinf(Y));
    if ~isempty(subInf)
        Y(subInf) = 0;
    end
    P = optimExpCos(Y, iP, 'DeltaT', DeltaT);
    if all(~P)
        break
    end
    parametres{iP} = P; %#ok<AGROW>
    Signal_reconstruit = syntheseProny(X-X(1) , parametres{iP});
    
    % ------------------
    % Calcul de l'erreur
    
    pppp = Signal_reconstruit - Y;
    erreur_P(iP) = sum(pppp .* pppp); %#ok<AGROW>
end

% --------------------------------------------------
% Recherche de l'ordre qui donne le meilleur resultat

[~, INDICE] =  min(erreur_P);
% Signal_reconstruit = syntheseProny(X-X(1), parametres{INDICE});

P = parametres{INDICE};
% ordre = size(P, 1);
if visu
    syntheseProny(X-X(1), parametres{INDICE});
    legende{1} = 'Modele reduit = Mode1 + Mode2 + ... + C';
    hold on; plot(X-X(1), Y, '+'); hold off;
    legende{2} = 'Donnees';
    legend(legende);
    str = sprintf('Synthese complete : %d modes', size(parametres{INDICE}, 1));
    title(str);
end

% -------------------------------------------------------
% On essaye d'eliminer les composantes non significatives
% On elimine les composante qui n'influent pas sur le coefficient
% de correlation a 4% pres

YModelise = syntheseProny(X-X(1), P);
nbModes = size(P, 1);
for iOrdre=1:nbModes
    sub = 1:nbModes;
    sub(iOrdre) = [];
    
    % On fait la synthese avec toutes les composantes SAUF la composante
    % d'ordre iOrdre
    
    Signal_ordre = syntheseProny(X-X(1) , P(sub,:));
    correla(iOrdre) = corr2(YModelise, Signal_ordre); %#ok<AGROW>
    
%     Signal_ordre = syntheseProny(X-X(1) , P(iOrdre,:));
%     correla2(iOrdre) = corr2(YModelise, Signal_ordre);
end

disp('-----------------------------------------')
fprintf('Le meilleur ajustement est obtenu pour une modelisation constituee de %d composantes\n', ...
                    size(parametres{INDICE},1));
disp('Parametres des composantes et correlation (k) : ')
disp('(amplitude, amortissement, periode, phase), cor(~k), cor(k)')
% P_Synthese = [P correla' correla2']

sub = find(correla > 0.96);
if length(sub) == nbModes
    % Dans le cas ou il n'y a aucune composante qui semble non significative,
    % on elimine quand-meme celle qui donne le minimum de correlation
    [~, indicemin] = min(correla);
    sub(indicemin) = [];
end


% Recherche des composantes qui sont non periodiques et calcul
% de la constante

subNonPeriodique = find(isinf(P(sub, 3)));
if isempty(subNonPeriodique)
    C = 0;
else
     C = mean(syntheseProny(X-X(1) , P(sub(subNonPeriodique),:)));
%    C = syntheseProny(1e10, P(sub(subNonPeriodique),:));
end

P(sub, :) = []; % On elimine les modes qui n'influent pas sur la correlation
correla(sub)  = [];
if visu
    disp('-----------------------------------------')
    fprintf('L''ajustement peut être reduit raisonnablement à %d composante(s)\n', ...
                    size(P,1));
    fprintf('et d''une constante C=%f\n', C);
    disp('Paramètres des composantes et corrélation: (amplitude, amortissement, periode, phase)')

    syntheseProny(X-X(1), P, 'constante', C)
    hold on; plot(X-X(1), Y, '+'); hold off;
end

% -------------------------------
% Affichage des differents ordres

clear correla
% ordre = size(P, 1);
couleurs = 'rgkycrgkycrgkycrgkyc';
for iOrdre=1:size(P, 1)
    if isinf(P(iOrdre,3))
        correla(iOrdre) = 0;    %#ok<AGROW> % ATTENTION BIDOUILLE
    else
        Signal_ordre = syntheseProny(X-X(1) , P(iOrdre,:));
        correla(iOrdre) = corr2(Y, Signal_ordre); %#ok<AGROW>
    end
    
%     pppp = Y - Signal_ordre;
%     EQM_P(iOrdre) = sum(pppp .* pppp);
end

% ------------------------
% Estimation de la periode

[correla_sort, indice_sort] = sort(correla);
correla_sort = fliplr(correla_sort);
indice_sort  = fliplr(indice_sort);
PeriodeEstimee = P(indice_sort(1),3);
P = P(indice_sort,:);

if visu
%     disp('')
%     str = sprintf('Coefficients de correlation des composantes / donnees : %f', correla_sort);
%     disp(str)
    
    for iOrdre=1:size(P, 1)
        Signal_ordre = syntheseProny(X-X(1) , P(iOrdre,:));
        hold on; plot(X-X(1), Signal_ordre, couleurs(iOrdre))
        legende{end+1} = ['Mode' num2str(iOrdre)]; %#ok<AGROW>
    end
end

% ---------------------------------------------------------------------------------
% On relance le processus d'identification sur un signal reduit a 2 fois la periode

sub = find((X-X(1)) < 2*PeriodeEstimee);
if length(sub) < length(X)
    disp('On relance le processus d''identification sur un signal reduit a 2 fois la periode')
    if ~EstimationPeriode
        [P2, C2, correla2] = optimProny(X(sub), Y(sub), 'EstimationPeriode');
%         YY = syntheseProny(X(sub), P, 'constante', C);  % EN TEST EN TEST EN TEST EN TEST
%         [P2, C2, correla2] = optimProny(X(sub), YY, 'EstimationPeriode'); % EN TEST EN TEST EN TEST EN TEST
        if ~isinf(P2(1,3))    % BIDOUILLE
            P = P2;
            C = C2;
            correla_sort = correla2;
        end
    end
end

if visu
    str = sprintf('Synthese reduite : %d modes et contante=%f', size(P, 1), C);
    title(str);
    
    if C ~= 0
        plot([0 X(end)-X(1)], [C C], couleurs(size(P, 1)+1));
        legende{end+1} = 'Constante';
    end
    if ~EstimationPeriode
        s1 = syntheseProny(X, P(1,:), 'constante', C);
        hold on; plot(X, s1, 'k'); hold off;
        legende{end+1} = 'Mode 1 + constante';
        disp('')
        fprintf('Periode estimee : %f\n', P(1,3));
    end
    legend(legende);
end

% -----------------
% Par ici la sortie 

switch nargout
% case 0
%     P
%     C
%     correla_sort
case 3
   varargout{1} = P;
   varargout{2} = C;
   varargout{3} = correla_sort;
end
