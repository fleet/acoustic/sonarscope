function [y, subOKOut, subKOOut] = signal_spike_filter(x, nomSignal, varargin)

[varargin, Fig]    = getPropertyValue(varargin, 'Fig', 4332841);
[varargin, Interp] = getPropertyValue(varargin, 'Interp', 1);

[y, subOKOut, subKOOut] = func_despike_phasespace(x, Fig, nomSignal, varargin{:});
if Interp
    y(subKOOut) = interp1(subOKOut, x(subOKOut), subKOOut);
end
