function H2 = spikeOut(H)

sub = find(~isnan(H) & ~isinf(H));
d = diff(H(sub));

s = filtre_erreurs(d);
subDiff = find(d ~= s);

subDiff = unique([subDiff subDiff+1 subDiff-1]);
subDiff(subDiff == 0) = [];
n = length(s);
subDiff(subDiff > n) = [];
subDiff = setdiff(1:n, subDiff);


% figure; plot(d, '-k'); grid on; hold on; plot(s(subDiff), 'or')
% figure; plot(sub, H(sub), '-k'); grid on; hold on; plot(sub(subDiff), H(sub(subDiff)), 'or')
H2 = NaN(size(H), class(H));
H2(sub(subDiff)) = H(sub(subDiff));