% Construction et affichage du Signal modelise par la methode de Prony
%
% Syntax
%   signal = syntheseProny(x, parametres, ...)
%
% Input Arguments
%    x          : Abscisse
%    parametres : parametres du signal [amplitude, amortissement, periode, phase]
%
% Name-Value Pair Arguments
%    constante : Constante a ajouter (0 par dedfaut)
%
% Output Arguments
%   []     : auto-plot activation
%   signal : signal synthetise
%
% Examples
%   syntheseProny(0:0.01:500, [1 100 20 0]);
%
%   parametres = [1 100 20 0 ; 0.5 70 15 0]
%   syntheseProny(0:0.01:500, parametres);
%
% See also Authors
% Authors : FM + LH + JMA
%-------------------------------------------------------------------------------

function varargout = syntheseProny(X, parametres, varargin)

[varargin, constante] = getPropertyValue(varargin, 'constante', 0); %#ok<ASGLU>

signal = X * 0;

Nombres_de_modes = size(parametres , 1);

for k=1:Nombres_de_modes
    
    % on recupere chaque parametre pour chaque mode
    
    amplitude       = parametres(k,1);
    amortissement   = parametres(k,2);
    periode         = parametres(k,3);
    phase           = parametres(k,4);
    
    % construction du signal modelise
    
    if amortissement ~= 0
        composante(k,:) = amplitude * cos(2 * pi / periode * X + phase) .* exp( - X / amortissement); %#ok<AGROW>
        signal = signal + composante(k,:);
    else
        composante(k,:) = amplitude * cos(2 * pi / periode * X + phase); %#ok<AGROW>
        signal = signal + composante(k,:);
    end
end

signal = signal(:)' + constante;

switch nargout
    case 0
        figure;
        plot(X, signal); grid on;
        title(num2strCode(parametres))
        
        %     for k=1:Nombres_de_modes
        %         hold on
        %         plot(X, composante(k,:), 'k')
        %     end
        %     hold off;
    otherwise
        varargout{1} = signal;
end




