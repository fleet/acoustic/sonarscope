function matlabSrc_startup(nomDirSrc)

path(path, fullfile(nomDirSrc, 'science'));
path(path, fullfile(nomDirSrc, 'ui'));
path(path, fullfile(nomDirSrc, 'unittest'));
path(path, fullfile(nomDirSrc, 'utils'));

path(path, fullfile(nomDirSrc, 'science', 'Model'));
path(path, fullfile(nomDirSrc, 'science', 'Optim'));
path(path, fullfile(nomDirSrc, 'science', 'Parametre'));
path(path, fullfile(nomDirSrc, 'science', 'Signal'));
path(path, fullfile(nomDirSrc, 'science', 'Image'));
path(path, fullfile(nomDirSrc, 'science', 'Layer'));
path(path, fullfile(nomDirSrc, 'science', 'utils'));

path(path, fullfile(nomDirSrc, 'science', 'Model', 'ExClModel'));
path(path, fullfile(nomDirSrc, 'science', 'Optim', 'ExClOptim'));

path(path, fullfile(nomDirSrc, 'ui', 'uiComponents'));
path(path, fullfile(nomDirSrc, 'ui', 'uiDialogs'));
path(path, fullfile(nomDirSrc, 'ui', 'uiUtils'));

path(path, fullfile(nomDirSrc, 'ui', 'uiDialogs', 'pamesDialog'));
path(path, fullfile(nomDirSrc, 'ui', 'uiDialogs', 'pamesDialog', 'parameters'));
path(path, fullfile(nomDirSrc, 'ui', 'uiDialogs', 'pamesDialog', 'swath'));
path(path, fullfile(nomDirSrc, 'ui', 'uiDialogs', 'pamesDialog', 'resolution'));
path(path, fullfile(nomDirSrc, 'ui', 'uiDialogs', 'pamesDialog', 'accuracy'));

path(path, fullfile(nomDirSrc, 'unittest', 'scienceTest'));
path(path, fullfile(nomDirSrc, 'unittest', 'uiTest'));

%     path(path, fullfile(nomDirSrc, 'unittest', 'scienceTest', 'ModelTest'));
%     path(path, fullfile(nomDirSrc, 'unittest', 'scienceTest', 'OptimTest'));
path(path, fullfile(nomDirSrc, 'unittest', 'scienceTest', 'ParametreTest'));
path(path, fullfile(nomDirSrc, 'unittest', 'scienceTest', 'SignalTest'));
path(path, fullfile(nomDirSrc, 'unittest', 'scienceTest', 'utilsTest'));

path(path, fullfile(nomDirSrc, 'unittest', 'uiTest', 'uiDemo'));

addpath(genpath(fullfile(nomDirSrc, 'unittest', 'xsfTest')));

