classdef ClImageData < handle
    % Description
    %   Class that describes ClImageData
    %
    % Syntax
    %   a = ClImageData(...)
    %
    % ClImageData properties :
    %   name     - Name of the image
    %   xData    - placement along x-axis (two-element vector or scalar)
    %   yData     - placement along y-axis (two-element vector or scalar)
    %   cData - image color data (vector or matrix)
    %   cLim - Max value of the parameter (default : Inf)
    %   colormapData   -
    %
    % Output Arguments
    %   a : One ClImageData instance
    %
    % Examples
    %    % Example 1 :
    %    imageData = ClImageData('cData', Lena);
    %    figure; image = imageData.imagesc();
    %
    %    % Example 2 :
    %    nomFic = 'Z:\private\ifremer\JMA\DemoImageEnBackground\DF1000_ExStr-grRawImage_PingRange_Reflectivity.xml';
    %    [flag, a] = cl_image.import_xml(nomFic);
    %
    %    H = get(a, 'SonarHeight');
    %    I = get(a, 'Image');
    %    x = get(a, 'x');
    %    y = get(a, 'y');
    %
    %    imageData= ClImageData('cData', I, 'xData', x, 'yData', y, 'colormapData', gray);
    %    imageData.imagesc
    %
    %   % TODO MHO : editProperties(imageData)
    %
    % Authors : MHO
    % See also ClLayer Authors
    % ----------------------------------------------------------------------------
    
    properties
        name  % name of image
        xData % placement along x-axis (two-element vector or scalar)
        yData % placement along y-axis (two-element vector or scalar)
        cData % image color data (vector or matrix)
        cLim  % Color limits (two-element vector)
        videoInverse % Video inverse
        % cDataMapping
        colormapData
    end
    
    methods
        function this = ClImageData(varargin)
            this = set(this, varargin{:});
        end
        
        function this = set(this,varargin)
            [varargin, this.name]         = getPropertyValue(varargin, 'name',         this.name);
            [varargin, this.xData]        = getPropertyValue(varargin, 'xData',        this.xData);
            [varargin, this.yData]        = getPropertyValue(varargin, 'yData',        this.yData);
            [varargin, this.cData]        = getPropertyValue(varargin, 'cData',        this.cData);
            [varargin, this.cLim]         = getPropertyValue(varargin, 'cLim',         this.cLim);
            [varargin, this.videoInverse] = getPropertyValue(varargin, 'videoInverse', false);
            [varargin, this.colormapData] = getPropertyValue(varargin, 'colormapData', this.colormapData); %#ok<ASGLU>
        end
        
        function im = image(this, varargin)
            im = image(varargin{:}, 'XData', this.xData, 'YData', this.yData, 'CData', this.cData);
            
            if ~isempty(this.colormapData)
                if this.videoInverse
                    map = flipud(this.colormapData);
                else
                    map = this.colormapData;
                end
                colormap(varargin{:}, map);
            end
        end
        
        function im = imagesc(this, varargin)
            if ~isempty(this.cLim)
                im = imagesc(varargin{:}, 'XData', this.xData, 'YData', this.yData, 'CData', this.cData, this.cLim);
            else
                im = imagesc(varargin{:}, 'XData', this.xData, 'YData', this.yData, 'CData', this.cData);
            end
            
            % Fix bug (implicite when imagesc(x,y,I);)
            axis tight; axis xy;
            
            if ~isempty(this.colormapData)
                if this.videoInverse
                    map = flipud(this.colormapData);
                else
                    map = this.colormapData;
                end
                colormap(varargin{:}, map);
            end
        end
    end
end
