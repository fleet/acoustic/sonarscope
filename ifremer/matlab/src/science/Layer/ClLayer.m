classdef ClLayer
    % Description
    %   Class that describes ClLayer
    %
    % Syntax
    %   a = ClLayer(...)
    %
    % ClImageData properties :
    %   xSignalList   - liste of signal on X axes
    %   ySignalList   - liste of signal on X axes
    %   imageDataList - liste of image data
    %
    % Output Arguments
    %   a : One ClLayer instance
    %
    % Examples
    %   ySample = YSample('data', sin(0:0.1*pi:10*pi-0.1*pi));
    %   ySample(2) = YSample('data', cos(0:0.1*pi:10*pi-0.1*pi));
    %   signal  = ClSignal('name', 'Simple sinus function', 'ySample', ySample);
    %
    %   C = [0 2 4 6; 8 10 12 14; 16 18 20 22];
    %   imageData = ClImageData('cData', C);
    %
    %  layer = ClLayer('ySignalList', signal, 'imageDataList', imageData);
    %  layer.plotY();
    %  layer.image();
    %  layer.imagePlot();
    %
    %   % TODO MHO : editProperties(layer)
    %
    % Authors : MHO
    % See also ClImageData Authors
    % ----------------------------------------------------------------------------
    
    properties
        xSignalList   % liste of signal on X axes
        ySignalList   % liste of signal on X axes
        imageDataList % liste of image data
        % statDataList
        % soundSpeedList
    end
    
    methods
        function this = ClLayer(varargin)
            this = set(this, varargin{:});
        end
        
        function this = set(this,varargin)
            [varargin, this.xSignalList]   = getPropertyValue(varargin, 'xSignalList',   this.xSignalList);
            [varargin, this.ySignalList]   = getPropertyValue(varargin, 'ySignalList',   this.ySignalList);
            [varargin, this.imageDataList] = getPropertyValue(varargin, 'imageDataList', this.imageDataList); %#ok<ASGLU>
        end
        
        function im = image(this, varargin)
            im = this.imageDataList.image(varargin{:});
        end
        
        function im = imagesc(this, varargin)
            im = this.imageDataList.imagesc(varargin{:});
        end
        
        function [fig, axesArray, curveArray] = plotX(this, varargin)
            [fig, axesArray, curveArray] = this.xSignalList.plot(varargin{:});
        end
        
        function [fig, axesArray, curveArray] = plotY(this, varargin)
            [fig, axesArray, curveArray] = this.ySignalList.plot(varargin{:});
        end
        
        function [fig, im, axesArray, curveArray] = imagePlot(this, varargin)
            [varargin, fig] = getPropertyValue(varargin, 'Fig', figure);
            
            % Nb of image
            imageNb = numel(this.imageDataList);
            
            %compute the max number of ySample for xSignal
            ySampleNbX = 0;
            for k=1:numel(this.xSignalList)
                ySampleNbX(k) = numel(this.xSignalList(k).ySample); %#ok<AGROW>
            end
            ySampleMaxNbX = max(ySampleNbX);
            
            %compute the max number of ySample for ySignal
            ySampleNbY = 0;
            for k=1:numel(this.ySignalList)
                ySampleNbY(k) = numel(this.ySignalList(k).ySample); %#ok<AGROW>
            end
            ySampleMaxNbY = max(ySampleNbY);
            
            % Compute the number of axes
            nbAxes = imageNb + ySampleMaxNbX + ySampleMaxNbY;
            
            % Create axes with subplot
            for k=1:nbAxes
                axesArray(k) = subplot(1, nbAxes, k); %#ok<AGROW>
            end
            
            % Assign axes
            imageAxesArray   = axesArray(1:imageNb);
            xSignalAxesArray = axesArray((imageNb + 1):(imageNb + ySampleMaxNbX));
            ySignalAxesArray = axesArray((imageNb + ySampleMaxNbX + 1):(imageNb + ySampleMaxNbX + ySampleMaxNbY));
            
            % Draw image
            if ~isempty(this.imageDataList)
                im = this.image(imageAxesArray, varargin{:});
            end
            
            % Plot x Signals
            curveArrayX = [];
            if ~isempty(this.xSignalList)
                [fig, ~, curveArrayX] = this.plotX('Fig', fig, 'axesArray', xSignalAxesArray, varargin{:});
            end
            
            % Plot Y Signal
            curveArrayY = [];
            if ~isempty(this.ySignalList)
                [fig, ~, curveArrayY] = this.plotY('Fig', fig, 'axesArray', ySignalAxesArray, varargin{:});
            end
            
            % Return curveArray
            curveArray = [curveArrayX curveArrayY];
        end
    end
end
