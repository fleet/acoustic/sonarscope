classdef ClModel < handle & Cloneable & AbstractDisplay & matlab.mixin.Heterogeneous
    % Description
    %   Class that deals with a physical model driven by parametres
    %
    % Syntax
    %   a = ClModel(...)
    %
    % ClModel properties :
    %   Name           - Name of the model
    %   Params         - ClParametre instances
    %   ParamsSup      - Additional parametres of the model {Psup1,Psup2, ...}
    %   ComponentsName - Name of the model components
    %   XData          - XData of experimental data
    %   YData          - YData of experimental data
    %   XDataFlags     - Array of booleans (size of XData) to allow data sampling
    %   WeightsType	   - Type of weighting
    %   WeightsData	   - Weights of experimental data
    %   WeightsTypeStr - {'none'; 'nb points'; 'variance'; 'std deviation'};
    %   xLabel         - Name of abscissa
    %   yLabel         - Name of ordinates
    %   xUnit          - Unit of abscissa
    %   yUnit          - Unit of ordinates
    %   Err            - Root Mean Sqare (rms) between experimental data and model taking into account the weights and XDataFlags
    %
    % Output Arguments
    %   a : One ClModel instance
    %
    % More About
    %   If the name of the model exists as a matlab function, and if you
    %   want to evaluate the model (y = compute(a)) or plot it (plot(a)),
    %   the function has to be written as is : [y, Components] = fonction(x, [parametress], ...)
    %
    % Examples
    %   T = ClParametre('Name', 'Temperature', 'Unit', 'deg', ...
    %                    'MinValue', -10, 'MaxValue', 50, 'Value', 20);
    %   S = ClParametre('Name', 'Salinite', 'Unit', '0/00', ...
    %                    'MinValue', 0, 'MaxValue', 50, 'Value', 38);
    %   a = ClModel('Name', 'Test', 'Params', [T S])
    %   Name = a.Name
    %   p = a.Params
    %   p(1).Value = 22;
    %   a.Params = p
    %   a.Name = 'Foo'
    %
    %   a = BSLurtonModel('XData', -80:80);
    %   plot(a);
    %   y = compute(a);
    %   set(a, 'YData', y + 5*(rand(size(y))-0.5))
    %   plot(a);
    %   ParamsName     = getParamsName(a)
    %   ParamsUnit     = getParamsUnit(a)
    %   ParamsEnable   = getParamsEnable(a)
    %   ParamsMinValue = getParamsMinValue(a)
    %   ParamsMaxValue = getParamsMaxValue(a)
    %   ParamsValue    = getParamsValue(a)
    %
    %   ParamsEnable(1:2:end) = 1;
    %   a = setParamsEnable(a, ParamsEnable)
    %   ParamsMinValue(1) = -60;
    %   a = setParamsMinValue(a, ParamsMinValue)
    %   ParamsMaxValue(1) = 20;
    %   a = setParamsMaxValue(a, ParamsMaxValue)
    %   ParamsValue(1) = -7;
    %   a = setParamsValue(a, ParamsValue)
    %   a = setParamsValueMinValueMaxValue(a, ParamsValue, ParamsMinValue, ParamsMaxValue)
    %
    %   char(a)
    %   char(a, 'hFunc', @char_short)
    %
    %   editProperties(a)
    %
    % Authors : MHO + JMA
    % See also ClParametre BSLurtonModel BSJacksonModel BSGuillonModel Authors
    % ----------------------------------------------------------------------------
    
    properties (Constant)
        WeightsTypeStr  = {'none'; 'nb points'; 'variance'; 'std deviation'}; % Type names of the weights
    end
    
    properties (Access = public)
        Name           = '';                % Name of the model
        functionName   = '';                % Name of the function
        Params         = ClParametre.empty; % ClParametre instances
        ParamsSup      = {};                % Additional parametres of the model
        ComponentsName = [];                % Name of model components
        XData          = [];                % XData of experimental data
        YData          = [];                % YData of experimental data
        XDataFlags     = [];                % Array of booleans (size of XData) to allow data sampling
        xLabel         = '';                % Name of abscissa
        yLabel         = '';                % Name of ordinates
        xUnit          = '';                % Unit of abscissa
        yUnit          = '';                % Unit of ordinates
        Dependencies   = [];                % Dependancies (cl_image instances)
        WeightsType	   = 1;                 % Type of weighting
        WeightsData	   = []                 % Weights of experimental data
        Err            = [];                % Root Mean Sqare (rms) between experimental data and model taking into account the weights and XDataFlags
    end
    
    methods
        
        function this = ClModel(varargin)
            % Constructor of the class
            
            %% Test if the user wants an empty instance
            if (nargin == 1) && isempty(varargin{1})
                this(1) = [];
                return
            end
            
            if ~isempty(varargin)
                this = set(this, varargin{:});
            end
        end
        
        %         function x = get.Err(this)
        %             % Getter of Err : Root Mean Sqare (rms) between experimental data and model taking into account the weights and XDataFlags
        %             x = rms(this);
        %         end
        
        
        function nbParams = get_nbParams(this)
            % Get the number of parametres of the model
            % Examples
            %   a = BSLurtonModel('XData', -80:80)
            %   nbParams = get_nbParams(a)
            
            if ~checkIfOnlyOneInstance(length(this))
                nbParams = NaN;
                return
            end
            nbParams = length(this.Params);
        end
        
        
        function ParamsName = getParamsName(this)
            % Get the names of the parametres in a cell array
            % Examples
            %   a = BSLurtonModel('XData', -80:80)
            %   ParamsName = getParamsName(a)
            
            ParamsName = {};
            if ~checkIfOnlyOneInstance(length(this))
                return
            end
            for k=length(this.Params):-1:1
                ParamsName{k} = this.Params(k).Name;
            end
        end
        
        
        function ParamsUnit = getParamsUnit(this)
            % Get the units of the parametres in a cell array
            % Examples
            %   a = BSLurtonModel('XData', -80:80)
            %   ParamsUnit = getParamsUnit(a)
            
            ParamsUnit = {};
            if ~checkIfOnlyOneInstance(length(this))
                return
            end
            for k=length(this.Params):-1:1
                ParamsUnit{k} = this.Params(k).Unit;
            end
        end
        
        
        function ParamsEnable = getParamsEnable(this)
            % Get the "Enable" properties of the parametres in an array
            % Examples
            %   a = BSLurtonModel('XData', -80:80)
            %   ParamsEnable = getParamsEnable(a)
            
            ParamsEnable = [];
            if ~checkIfOnlyOneInstance(length(this))
                return
            end
            for k=length(this.Params):-1:1
                ParamsEnable(k) = this.Params(k).Enable;
            end
        end
        
        
        function ParamsMinValue = getParamsMinValue(this)
            % Get the "MinValue" properties of the parametres in an array
            % Examples
            %   a = BSLurtonModel('XData', -80:80)
            %   ParamsMinValue = getParamsMinValue(a)
            
            ParamsMinValue = [];
            if ~checkIfOnlyOneInstance(length(this))
                return
            end
            for k=length(this.Params):-1:1
                ParamsMinValue(k) = this.Params(k).MinValue;
            end
        end
        
        
        function ParamsMaxValue = getParamsMaxValue(this)
            % Get the "MaxValue" properties of the parametres in an array
            % Examples
            %   a = BSLurtonModel('XData', -80:80)
            %   ParamsMaxValue = getParamsMaxValue(a)
            
            ParamsMaxValue = [];
            if ~checkIfOnlyOneInstance(length(this))
                return
            end
            for k=length(this.Params):-1:1
                ParamsMaxValue(k) = this.Params(k).MaxValue;
            end
        end
        
        
        function ParamsValue = getParamsValue(this)
            % Get the "Value" properties of the parametres in an array
            % Examples
            %   a = BSLurtonModel('XData', -80:80)
            %   ParamsValue = getParamsValue(a)
            
            ParamsValue = [];
            if ~checkIfOnlyOneInstance(length(this))
                return
            end
            
            for k=length(this.Params):-1:1
                % Comment� par JMA le 18/12/2015
                %                 X = this.Params(k).Value;
                %                 if ~isempty(X)
                %                     ParamsValue(k) = X;
                %                 end
                ParamsValue(k) = this.Params(k).Value;
            end
        end
        
        
        function this = set(this, varargin)
            % Setter of the form a = set(a, ...)
            % Examples
            %   T = ClParametre('Name', 'Temperature', 'Unit', 'deg', ...
            %                    'MinValue', -10, 'MaxValue', 50, 'Value', 20)
            %   S = ClParametre('Name', 'Salinite', 'Unit', '0/00', ...
            %                    'MinValue', 0, 'MaxValue', 50, 'Value', 38)
            %
            %   a = ClModel('Name', 'Test', 'Params', [T S])
            %
            %   Name   = get(a, 'Name')
            %   Params = get(a, 'Params')
            %
            %   Params(1) = set(Params(1), 'value', 33)
            %   a = set(a, 'Params', Params)
            %   set(a, 'Name', 'Toto')
            
            if ~checkIfOnlyOneInstance(length(this))
                return
            end
            
            %% Lecture des arguments
            
            [varargin, this.Name]           = getPropertyValue(varargin, 'Name',           this.Name);
            [varargin, this.functionName]   = getPropertyValue(varargin, 'functionName',   this.functionName);
            [varargin, this.ComponentsName] = getPropertyValue(varargin, 'ComponentsName', this.ComponentsName);
            [varargin, this.XData]          = getPropertyValue(varargin, 'XData',          this.XData);
            [varargin, this.YData]          = getPropertyValue(varargin, 'YData',          this.YData);
            [varargin, this.XDataFlags]     = getPropertyValue(varargin, 'XDataFlags',     this.XDataFlags);
            [varargin, this.Params]         = getPropertyValue(varargin, 'Params',         this.Params);
            [varargin, this.ParamsSup]      = getPropertyValue(varargin, 'ParamsSup',      this.ParamsSup);
            [varargin, this.xLabel]         = getPropertyValue(varargin, 'xLabel',         this.xLabel);
            [varargin, this.yLabel]         = getPropertyValue(varargin, 'yLabel',         this.yLabel);
            [varargin, this.xUnit]          = getPropertyValue(varargin, 'xUnit',          this.xUnit);
            [varargin, this.yUnit]          = getPropertyValue(varargin, 'yUnit',          this.yUnit);
            [varargin, this.Dependencies]   = getPropertyValue(varargin, 'Dependencies',   this.Dependencies);
            [varargin, this.WeightsType]    = getPropertyValue(varargin, 'WeightsType',    this.WeightsType);
            [varargin, this.WeightsData]    = getPropertyValue(varargin, 'WeightsData',    this.WeightsData);
            [varargin, this.Err]            = getPropertyValue(varargin, 'Err',            this.Err); %#ok<ASGLU>
            
            if isempty(this.functionName)
                this.functionName = this.Name;
            end
            
            %% Check if all the arguments have been interpretated
            
            %{
if checkIfVararginIsEmpty(varargin)
return
end
            %}
            
            %% Weights
            
            if isempty(this.WeightsData)
                this.WeightsData = ones(size(this.YData));
            end
            
        end
        
        
        function this = setParamsEnable(this, ParamsEnable)
            % Set the "Enable" properties of the parametres
            % Examples
            %   a = BSLurtonModel('XData', -80:80)
            %   ParamsEnable = getParamsEnable(a)
            %   a = setParamsEnable(a, ParamsEnable)
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc ClModel/setParamsEnable">ClModel/setParamsEnable</a>
            %   <a href="matlab:doc ClModel">ClModel</a>
            
            if ~checkIfOnlyOneInstance(length(this))
                return
            end
            for k=1:length(this.Params)
                this.Params(k).Enable = ParamsEnable(k);
            end
        end
        
        
        function this = setParamsMinValue(this, ParamsMinValue)
            % Set MinValue of the parametres
            % Examples
            %   a = BSLurtonModel('XData', -80:80)
            %   ParamsMinValue = getParamsMinValue(a)
            %   a = setParamsMinValue(a, ParamsMinValue)
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc ClModel/setParamsMinValue">ClModel/setParamsMinValue</a>
            %   <a href="matlab:doc ClModel">ClModel</a>
            
            if ~checkIfOnlyOneInstance(length(this))
                return
            end
            for k=1:length(this.Params)
                this.Params(k).MinValue = ParamsMinValue(k);
            end
        end
        
        
        function this = setParamsMaxValue(this, ParamsMaxValue)
            % Set MaxValue of the parametres
            % Examples
            %   a = BSLurtonModel('XData', -80:80)
            %   ParamsMaxValue = getParamsMaxValue(a)
            %   a = setParamsMaxValue(a, ParamsMaxValue)
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc ClModel/setParamsMaxValue">ClModel/setParamsMaxValue</a>
            %   <a href="matlab:doc ClModel">ClModel</a>
            
            if ~checkIfOnlyOneInstance(length(this))
                return
            end
            for k=1:length(this.Params)
                this.Params(k).MaxValue = ParamsMaxValue(k);
            end
        end
        
        
        function this = setParamsValue(this, ParamsValue)
            % Set Value of the parametres
            % Examples
            %   a = BSLurtonModel('XData', -80:80)
            %   ParamsValue = getParamsValue(a)
            %   a = setParamsValue(a, ParamsValue)
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc ClModel/setParamsValue">ClModel/setParamsValue</a>
            %   <a href="matlab:doc ClModel">ClModel</a>
            
            if ~checkIfOnlyOneInstance(length(this))
                return
            end
            for k=1:length(this.Params)
                this.Params(k).Value =  ParamsValue(k);
            end
        end
        
        
        function this = setParamsValueMinValueMaxValue(this, ParamsValue, ParamsMinValue, ParamsMaxValue)
            % Set Value, MinValue and MaxValue of the parametres
            % Examples
            %   a = BSLurtonModel('XData', -80:80)
            %   ParamsMinValue = getParamsMinValue(a)
            %   ParamsMaxValue = getParamsMaxValue(a)
            %   ParamsValue    = getParamsValue(a)
            %   a = setParamsValueMinValueMaxValue(a, ParamsValue, ParamsMinValue, ParamsMaxValue)
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc ClModel/setParamsValueMinValueMaxValue">ClModel/setParamsValueMinValueMaxValue</a>
            %   <a href="matlab:doc ClModel">ClModel</a>
            
            if ~checkIfOnlyOneInstance(length(this))
                return
            end
            for k=1:length(this.Params)
                this.Params(k) = setValueMinValueMaxValue(this.Params(k), ParamsValue(k), ParamsMinValue(k), ParamsMaxValue(k));
            end
        end
        
        
        function [err, x, y, xMin, yMin] = analyse(this, px, py, varargin)
            % Analysis of the RMS upon 2 parametres
            %
            % Syntax
            %   [eqm, x, y, xMin, yMin] = analyse(a, px, py)
            %
            % Input Arguments
            %   a  : a ClModel instance
            %   px : Index of the parametre to put in x axis
            %   py : Index of the parametre to put in y axis
            %
            % Name-Value Pair Arguments
            %   Nx : Number of cells in x (default 10)
            %   Ny : Number of cells in y (default 10)
            %
            % Output Arguments
            %   []   : Auto-plot activation
            %   eqm  : RMS of YData / Model
            %   x    : RMS abscissa
            %   y    : RMS ordinates
            %   xMin : Abscissa of the minimum RMS
            %   xMin : Ordinate of the minimum RMS
            %
            % Examples
            %   a = BSLurtonModel('XData', -80:80)
            %   a = noise(a, 'Coeff', 2);
            %   plot(a);
            %   analyse(a, 3, 4)
            %   [eqm, x, y, xMin, yMin] = analyse(a, 5, 6);
            %   figure; surf(x, y, eqm);
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc ClModel/analyse">ClModel/analyse</a>
            %   <a href="matlab:doc ClModel">ClModel</a>
            
            err  = [];
            x    = [];
            y    = [];
            xMin = [];
            yMin = [];
            
            if ~checkIfOnlyOneInstance(length(this))
                return
            end
            
            [varargin, Nx] = getPropertyValue(varargin, 'Nx', 10);
            [varargin, Ny] = getPropertyValue(varargin, 'Ny', 10);
            
            %% Check if all the arguments have been interpretated
            
            if checkIfVararginIsEmpty(varargin)
                return
            end
            
            %% Check if the processing can be done
            
            if ~CheckIfXDataAndYDataAreDefined(this)
                return
            end
            
            %% Compute the RMS
            
            ParamsValue    = getParamsValue(this);
            ParamsMinValue = getParamsMinValue(this);
            ParamsMaxValue = getParamsMaxValue(this);
            ParamsName     = getParamsName(this);
            
            err = zeros(Ny, Nx);
            y = linspace(ParamsMinValue(py), ParamsMaxValue(py), Ny);
            x = linspace(ParamsMinValue(px), ParamsMaxValue(px), Nx);
            hw = create_waitbar('Processing', 'N', Ny);
            for iy=1:Ny
                my_waitbar(iy, Ny, hw)
                ParamsValue(py) = y(iy);
                for ix=1:Nx
                    ParamsValue(px) = x(ix);
                    this = setParamsValue(this, ParamsValue);
                    yModele = compute(this);
                    difference = yModele - this.YData;
                    err(iy, ix) = sum(difference .^ 2);
                end
            end
            my_close(hw, 'MsgEnd');
            
            %% Look for the minimum RMS value
            
            [pppp, k] = min(err(:)); %#ok
            [iyMin,ixMin] = ind2sub(size(err), k);
            xMin = x(ixMin);
            yMin = y(iyMin);
            
            %% Autoplot
            
            if nargout == 0
                figure; imagesc(x, y, err); colorbar;
                title('Root Mean Square (rms)')
                xlabel(ParamsName{py}, 'Interpreter', 'none')
                ylabel(ParamsName{px}, 'Interpreter', 'none')
                
                [px,py] = gradient(err);
                hold on; quiver(x, y, -px, -py, 'k'); axis xy;
                plot([xMin xMin],   [y(1) y(end)],  'k')
                plot([x(1) x(end)], [yMin yMin], 'k')
            end
        end
        
        
        function Erreur = rms(this, useWeights) % TODO MHO from ClOptim
            % Compute the Root Mean Square (rms) between experimental data and model
            % taking into account the weights and XDataFlags
            %
            % Syntax
            %   RmsValue = rms(a, useWeights)
            %
            % Input Arguments
            %   a : ClOptim instances
            %   useWeights : true is use weights, false if not
            %
            % Output Arguments
            %   RmsValue : RMS of YData / Model
            %
            % Examples
            %   plot(optim);
            %   RmsValue = rms(optim, true)
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc ClOptim/rms">ClOptim/rms</a>
            %   <a href="matlab:doc ClOptim">ClOptim</a>
            
            % Calcul des erreurs avec prise en compte des poids et de la zone de calcul
            % (contrairement au calcul_erreur de ClModel)
            % Cette fonction est independante des algorithmes : cf. errFunction.m
            
            % R�cup�ration des informations
            oXData      = this.XData;
            oXData_size = size(oXData);
            oYData      = this.YData;
            oXDataFlags = this.XDataFlags;
            
            if isempty(oXDataFlags)
                oXDataFlags = ones(size(oXData));
            end
            
            oXDataFlags_indice = find(oXDataFlags == 1);
            
            N = length(oXData);
            
            % Test si valeurs exp�riementales sont pr�sentes
            if (N == length(oYData)) || (N == 0)
                
                % Calcul des YModel
                YModel = this.compute('x',  oXData);
                
                % Calcul de l'erreur sur la zone XDataFlags
                ym = YModel(oXDataFlags_indice);
                yd = oYData(oXDataFlags_indice);
                a_Err(oXDataFlags_indice) = ym(:)' - yd(:)';
                
                % Prise en compte de la ponderation
                if useWeights % Use weights
                    switch this.WeightsType
                        case 1 % none
                            a_WeightsData = ones(oXData_size);
                        case 2  % nb points
                            a_WeightsData = this.WeightsData;
                        case 3  % variance
                            a_WeightsData = 1 ./ sqrt(this.WeightsData);
                        case 4  % ecarts-type
                            a_WeightsData = 1 ./ this.WeightsData;
                    end
                else % No not use weights
                    a_WeightsData = ones(oXData_size);
                end
                
                % ----------------------------------------------------
                % Suppression des mauvais poids (infinis, NaN et nuls)
                oXDataFlags(~isfinite(a_WeightsData)) = 0;
                oXDataFlags(isnan(a_WeightsData))	  = 0;
                oXDataFlags(a_WeightsData==0)		  = 0;
                
                % -------------------------------------------------------------
                % Suppression des mauvaises valeurs (infines, NaN et comlexes)
                oXDataFlags(~isfinite(a_Err)) = 0;
                oXDataFlags(isnan(a_Err))	  = 0;
                oXDataFlags(imag(a_Err) ~= 0) = 0;	% Oui car ca arrive malheureusement : BSJackson rend des nombres complexes
                
                oXDataFlags_indice = find(oXDataFlags == 1);
                
                % Calcul de l'erreur pond�r�e sur la zone XDataFlags
                a_Err(oXDataFlags_indice) = a_Err(oXDataFlags_indice) .* a_WeightsData(oXDataFlags_indice);
                
                % Calcul de l'erreur quadratique moyenne sur la zone XDataFlags
                a_Err = sqrt(sum(a_Err(oXDataFlags_indice).^2) / sum(a_WeightsData(oXDataFlags_indice)));
                
                % Sauvegarde des erreurs
                Erreur = a_Err;
                
                this.Err = Erreur;
            else
                Erreur = NaN;
            end
        end
        
        
        %         function RmsValue = rms(this, varargin)
        %             % Compute the Root Mean Square (rms) between experimental data and model
        %             %
        %             % Syntax
        %             %   RmsValue = rms(a, ...)
        %             %
        %             % Input Arguments
        %             %   a : a ClModel instance
        %             %
        %             % Name-Value Pair Arguments
        %             %   sub : Sub-sampling in abscissa
        %             %
        %             % Output Arguments
        %             %   RmsValue  : RMS of YData / Model
        %             %
        %             % Examples
        %             %   a = BSLurtonModel('XData', -80:80)
        %             %   a = noise(a, 'Coeff', 2);
        %             %   plot(a);
        %             %   RmsValue = rms(a)
        %             %
        %             % Reference pages in Help browser
        %             %   <a href="matlab:doc ClModel/rms">ClModel/rms</a>
        %             %   <a href="matlab:doc ClModel">ClModel</a>
        %
        %             %% Check if the processing can be done
        %
        %             if ~checkIfOnlyOneInstance(length(this))
        %                 return
        %             end
        %             if ~CheckIfXDataAndYDataAreDefined(this)
        %                 RmsValue = NaN;
        %                 return
        %             end
        %
        %             %% Compute the RMS
        %
        % %             N = length(this.XData);
        % %             if (N == length(this.YData)) || (N == 0)
        % [varargin, sub] = getPropertyValue(varargin, 'sub', 1:length(this.XData));
        %
        %                 % Check if all the arguments have been interpretated
        %                 if checkIfVararginIsEmpty(varargin)
        %                     return
        %                 end
        %
        %                 YModel = compute(this, 'x', this.XData(sub));
        %                 RmsValue = eqm(this.YData(sub), YModel);
        % %             else
        % %                 RmsValue = NaN;
        % %             end
        %         end
        
        
        function str = char_short(this)
            % Short (and synthetic) summary of an instance.
            %
            % Syntax
            %   RmsValue = rms(a, ...)
            %
            % Input Arguments
            %   a : a ClModel instance
            %
            % Output Arguments
            %   str : Char array that describes the parametres of the model
            %
            % Examples
            %   a = BSLurtonModel('XData', -80:80)
            %   a = noise(a, 'Coeff', 2);
            %   char(a)
            %   char_short(a)
            %
            % More About
            %   If the instances is not unique, one can use the "char"
            %   method with the 'hFunc' proprty name to take advantage of
            %   the multi-instance as is :
            %   char([a a a], 'hFunc', @char_short)
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc ClModel/char_short">ClModel/char_short</a>
            %   <a href="matlab:doc ClModel">ClModel</a>
            
            %% Check the number of instances
            
            if ~checkIfOnlyOneInstance(length(this))
                return
            end
            
            %% Make the summary
            
            str = this.Name;
            for k=1:length(this.Params)
                a_nom = this.Params(k).Name;
                str(k+1, 1:length(a_nom)) = a_nom;
            end
            
            N = size(str, 2);
            str(1, N+3:N+7) = 'value';
            for k=1:length(this.Params)
                a_nom = sprintf('%f', this.Params(k).Value);
                str(k+1, N+3:N+2+length(a_nom)) = a_nom;
            end
            
            N = size(str, 2);
            str(1, N+3:N+10) = 'minvalue';
            for k=1:length(this.Params)
                a_nom = sprintf('%f', this.Params(k).MinValue);
                str(k+1, N+3:N+2+length(a_nom)) = a_nom;
            end
            
            N = size(str, 2);
            str(1, N+3:N+10) = 'maxvalue';
            for k=1:length(this.Params)
                a_nom = sprintf('%f', this.Params(k).MaxValue);
                str(k+1, N+3:N+2+length(a_nom)) = a_nom;
            end
            
            N = size(str, 2);
            str(1, N+3:N+7) = 'unite';
            for k=1:length(this.Params)
                a_nom = this.Params(k).Unit;
                str(k+1, N+3:N+2+length(a_nom)) = a_nom;
            end
        end
        
        
        function [y, Components] = compute(this, varargin)
            % Compute the y values of a model
            % Examples
            %   x = -80:80;
            %   a = BSLurtonModel('XData', x);
            %   y = compute(a);
            %   figure; plot(x, y); grid on;
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc ClModel/compute">ClModel/compute</a>
            %   <a href="matlab:doc ClModel">ClModel</a>
            
            y          = [];
            Components = [];
            
            if ~checkIfOnlyOneInstance(length(this))
                return
            end
            
            [varargin, a_XData] = getPropertyValue(varargin, 'x', []);
            
            %% Check if all the arguments have been interpretated
            
            if checkIfVararginIsEmpty(varargin)
                return
            end
            
            if isempty(a_XData)
                a_XData = this.XData;
            end
            if isempty(a_XData)
%                 str1 = 'XData est vide.';
                str2 = 'XData is empty.';
                my_warndlg(str2, 1);
                return
            end
            
            if ~exist(this.functionName, 'file')
%                 str1 = sprintf('ClModel/compute : La fonction "%s" n''est pas dans le path.', this.functionName);
                str2 = sprintf('ClModel/compute : Function "%s" was not found in the path.', this.functionName);
                my_warndlg(str2, 1);
                return
            end
            
            %% get the values of the parametres
            
            ParamsValue = getParamsValue(this);
            
            %% Evaluate the function
            
            if isempty(this.ComponentsName)
                try
                    % TODO MHO : est-ce qu'on pourrait faire y = @(this.Name)() ou quelque-chose comme �a ?
%                     y = feval(this.functionName, a_XData, ParamsValue, this.ParamsSup{:});
                    f = str2func(this.functionName);
                    y = f(a_XData, ParamsValue, this.ParamsSup{:});
                catch
%                     str1 = sprintf('ClModel/compute : La fonction "%s" doit r�pondre a :\ny = %s(x, parametres)\n(Cf. BSLurton pour exemple)', this.functionName, this.functionName);
                    str2 = sprintf('ClModel/compute : Function "%s" must be designed as :\ny = %s(x, parametres)\n(Cf. BSLurton for example)', this.functionName, this.functionName);
                    my_warndlg(str2, 1);
                end
            else
                try
%                     [y, Components] = feval(this.functionName, a_XData, ParamsValue, this.ParamsSup{:});
                    f = str2func(this.functionName);
                    [y, Components] = f(a_XData, ParamsValue, this.ParamsSup{:});
                    
                catch ME
                    %         str = sprintf('La fonction "%s" doit repondre a :\n[y, Components] = %s(x, parametres)\n(Cf. BSLurton pour exemple)', this.Name, this.Name);
                    my_warndlg(ME.message, 1);
                end
            end
        end
        
        function [res, x] = residuals(this, varargin)
            % Compute the residuals between experimental data and the model
            %
            % Syntax
            %   [res, x] = residuals(a, ...)
            %
            % Input Arguments
            %   a : a ClModel instance
            %
            % Name-Value Pair Arguments
            %   x : Abscissa where to compute the residuals (defauls XData)
            %   y : Ordinates where to compute the residuals (defauls XData)
            %
            % Output Arguments
            %   res : The residual values
            %   x   : Abscissa where the residuals have been computed
            %
            % Examples
            %   x = -80:80;
            %   a = BSLurtonModel('XData', x);
            %   a = noise(a, 'Coeff', 2);
            %   plot(a)
            %   [res, x] = residuals(a)
            %   figure; plot(x, res); grid on;
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc ClModel/residuals">ClModel/residuals</a>
            %   <a href="matlab:doc ClModel">ClModel</a>
            
            [varargin, x] = getPropertyValue(varargin, 'x', this.XData);
            [varargin, y] = getPropertyValue(varargin, 'y', this.YData);
            
            %% Check if all the arguments have been interpretated
            
            res = [];
            if ~checkIfOnlyOneInstance(length(this))
                return
            end
            
            if checkIfVararginIsEmpty(varargin)
                return
            end
            
            if isempty(y)
                return
            end
            
            yModel = compute(this, 'x', x);
            % figure; plot(x, y, '+k'); grid on; hold on;  plot(x, yModel, 'r');
            
            res = (y(:) - yModel(:))';
            % figure; plot(x, res, '+k'); grid on;
        end
        
        
        
        function y = interp(this, x)
            % Interpolate the experimental data on x abscissa
            %
            % Syntax
            %   y = interp(a, x)
            %
            % Input Arguments
            %   a : a ClModel instance
            %   x : Abscissa where to interpolate the data
            %
            % Output Arguments
            %   y : Interpolated values
            %
            % Examples
            %   a = BSLurtonModel('XData', -80:5:80);
            %   a = noise(a, 'Coeff', 10);
            %   plot(a)
            %   x = 0:70;
            %   y = interp(a, x)
            %   figure; plot(x, y); grid on;
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc ClModel/interp">ClModel/interp</a>
            %   <a href="matlab:doc ClModel">ClModel</a>
            
            if isempty(this.YData)
                y = [];
            else
                y = NaN(size(x), 'single');
                N = numel(x);
                for k=1:1e6:N
                    sub= k:(k+1e6)-1;
                    sub(sub > N) = [];
                    y(sub) = interp1(this.XData, this.YData, x(sub), 'linear', 'extrap');
                end
            end
        end
        
        function [Axe, fig, hCurves] = plot(this, varargin)
            % Plot the model with the experimental data if they exist
            %
            % Syntax
            %   [Axe, GCF] = plot(a, ...)
            %
            % Input Arguments
            %   a : ClModel instance(s)
            %
            % Name-Value Pair Arguments
            %    Axe            : Handle of the axis where to plot the model (default [])
            %    Erase          : Boolean to ask a RAZ of the figure (default true)
            %    DrawComponents : Supperimpose DrawComponents property for all instancies
            %    Legend         : Your own legend
            %    Title          : Title of the figure
            %
            % Name-Value Arguments
            %   NoData : Do not display the experimental data
            %
            % Output Arguments
            %   Axe     : Handle of the axis
            %   GCF     : Handle of the figure
            %   hCurves : Structure containing the handle of the curves.
            %
            % Examples
            %   a = BSLurtonModel('XData', -80:80);
            %   plot(a);
            %
            %   y = compute(a);
            %   a.YData = y + 5*(rand(size(y))-0.5);
            %   plot(a);
            %   plot(a, 'NoData', 1);
            %   plot(a, 'DrawComponents', 0);
            %   plot(a, 'DrawComponents', 0, 'NoData', 1, 'Title', 'Hello world', 'Legend', 'Holly curve');
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc ClModel/plot">ClModel/plot</a>
            %   <a href="matlab:doc ClModel">ClModel</a>

            [varargin, Axe]            = getPropertyValue(varargin, 'Axe',           []);
            [varargin, Erase]          = getPropertyValue(varargin, 'Erase',          true);
            [varargin, DrawComponents] = getPropertyValue(varargin, 'DrawComponents', 1);
%             [varargin, Legend]         = getPropertyValue(varargin, 'Legend',         []);
            [varargin, Title]          = getPropertyValue(varargin, 'Title',          []);
            [varargin, DefaultColor]   = getPropertyValue(varargin, 'DefaultColor',   false);
            [varargin, NoData]         = getPropertyValue(varargin, 'NoData',         false);
            
            hCurves.Data       = [];
            hCurves.Model      = [];
            hCurves.Components = [];
            fig = [];
            
            %% Check if all the arguments have been interpretated
            
            if checkIfVararginIsEmpty(varargin)
                return
            end
            
            if isempty(Axe)
                fig = FigUtils.createSScFigure;
                Axe = subplot(1,1,1);
            else
                if Erase
                    h = get(Axe, 'children');
                    delete(h)
                    axes(Axe)
                else
                    %                     axes(Axe)
                    %                     hold(Axe, 'on');
                end
            end
            %             Axe = gca;
            hold(Axe, 'on');
            %             hold on;
            couleurs = 'gmck';
            
            YLim = [Inf -Inf];
            
            LegendList{1} = 'Data';
            nbModels = length(this);
            for k=1:nbModels
                
                if ~DefaultColor
                    if  mod(nbModels,2) == 1 % Nombre impair : on est pas dans le cas d'un dual swath
                        ColorModel(k) = 'b'; %#ok<AGROW>
                    else % On est peut-�tre dans le cas d'un dual swath
                        if k <= (nbModels/2)
                            ColorModel(k) = 'b'; %#ok<AGROW>
                        else
                            ColorModel(k) = 'r'; %#ok<AGROW>
                        end
                    end
                end
                
                a = this(k);
                if ~isempty(a.YData)
                    if ~NoData
                        if ~DefaultColor
                            hCurves.Data(k) = PlotUtils.createSScPlot(Axe, a.XData, a.YData, [ColorModel(k) '+'], 'isLightContextMenu', true);
                        else
                            hCurves.Data(k) = PlotUtils.createSScPlot(Axe, a.XData, a.YData, 'isLightContextMenu', true);
                        end
                        grid(Axe, 'on');
                    end
                    hold(Axe, 'on');
                    %                     hold on;
                    YLim(1) = min([YLim(1); a.YData(:)]);
                    YLim(2) = max([YLim(2); a.YData(:)]);
                end
                
                [YModel, a_composantes] = compute(a);
                
                YLim(1) = min([YLim(1); YModel(:)]);
                YLim(2) = max([YLim(2); YModel(:)]);
                
                if DrawComponents
                    for k2=1:size(a_composantes, 1)
                        kCoul = 1 + mod(k2-1, length(couleurs));
                        hCurves.Components(k,k2) = PlotUtils.createSScPlot(Axe, a.XData, a_composantes(k2,:), couleurs(kCoul), 'isLightContextMenu', true); 
                         grid(Axe, 'on');
                        LegendList{end+1} = this.ComponentsName{k2}; %#ok<AGROW>
                    end
                end
                
                if ~isempty(YModel)
                    %         cmenu = uicontextmenu;
                    %         h = plot(a.XData, YModel, 'b', 'UIContextMenu', cmenu); grid on;
                    if ~DefaultColor
                        hCurves.Model(k) = PlotUtils.createSScPlot(Axe, a.XData, YModel, ColorModel(k), 'isLightContextMenu', true); 
                        grid(Axe, 'on');
                    else
                        hCurves.Model(k) = PlotUtils.createSScPlot(Axe, a.XData, YModel, 'isLightContextMenu', true); 
                        grid(Axe, 'on');
                    end
                    
                    str1 = sprintf('Courbe     : %d', k);
                    str2 = sprintf('Modele     : %s', a.Name);
                    str3 = sprintf('Parametres : %s', num2strCode(getParamsValue(a)));
                    cmenu = get(hCurves.Model(k), 'UIContextMenu');
                    uimenu(cmenu, 'Text', str1, 'Separator','on');
                    uimenu(cmenu, 'Text', str2);
                    uimenu(cmenu, 'Text', str3);
                end
                if ~isequal(YLim, [Inf -Inf])
                    set(Axe, 'YLim', YLim)
                end
            end
            hold(Axe, 'off');
            %             hold off;
            
%             if isempty(Legend)
%                 Legend = LegendList;
%             end
            %  legend(Axe, Legend, 'Interpreter', 'none', 'Location', 'north');
            
            %% xLabel
            
            if isempty(a.xUnit)
                str = a.xLabel;
            else
                str = sprintf('%s (%s)', a.xLabel, a.xUnit);
            end
            xlabel(Axe, str, 'Interpreter', 'none')
            
            %% yLabel
            
            if isempty(a.yUnit)
                str = a.yLabel;
            else
                str = sprintf('%s (%s)', a.yLabel, a.yUnit);
            end
            ylabel(Axe, str, 'Interpreter', 'none')
            
            %% Title
            
            if isempty(Title)
                Title = a.Name;
            else
                Title = sprintf('%s\n%s', Title, a.Name);
            end
            title(Axe, Title, 'Interpreter', 'none')
            
            % if nargout ~= 0
            %     varargout{1} = hAxe;    % Pour faire comme le plot de matlab (mis � part que ca rend le
            %     varargout{2} = gcf;    % Pour faire comme le plot de matlab (mis � part que ca rend le
            %     % handle de l'axe et non pas le handle de la courbe)
            % end
            
            
            % Get the ancestor axes and figure from the input (if present).
            if ~isempty(fig)
                obj = Axe;
                while ~isempty(obj)
                    if isgraphics(obj, 'figure')
                        fig = obj;
                        %                     elseif isgraphics(obj,'axes') || isgraphics(obj,'polaraxes') || isgraphics(obj,'geoaxes')
                        %                         ax = obj;
                    end
                    obj = obj.Parent;
                end
            end


            drawnow
        end
        
        
        function this = random(this)
            % Randomize the parametres that are enable
            %
            % Syntax
            %   a = random(a)
            %
            % Input Arguments
            %   a : ClModel instance(s)
            %
            % Output Arguments
            %   a : ClModel instance(s)
            %
            % Examples
            %   a = BSLurtonModel('XData', -80:80);
            %   b = random(a);
            %   plot([a b]);
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc ClModel/random">ClModel/random</a>
            %   <a href="matlab:doc ClModel">ClModel</a>
            
            if ~checkIfOnlyOneInstance(length(this))
                return
            end
            this.Params = random(this.Params);
        end
        
        function this = noise(this, varargin)
            % Add noise on YData
            %
            % Syntax
            %   a = noise(a, ...)
            %
            % Input Arguments
            %   a : a ClModel instance
            %
            % Name-Value Pair Arguments
            %   Coeff : Multiplicative noise (default 1)
            %
            % Output Arguments
            %   a : a ClModel instance
            %
            % Examples
            %   a = BSLurtonModel('XData', -80:80)
            %   a = noise(a, 'Coeff', 2);
            %   plot(a);
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc ClModel/noise">ClModel/noise</a>
            %   <a href="matlab:doc ClModel">ClModel</a>
            
            if ~checkIfOnlyOneInstance(length(this))
                return
            end
            if isempty(this.XData)
                return
            end
            
            [varargin, Coeff] = getPropertyValue(varargin, 'Coeff', 1);
            
            if checkIfVararginIsEmpty(varargin)
                return
            end
            
            X = rand(1, length(this.XData)) - 0.5;
            this.YData = compute(this) + Coeff * X;
        end
    end
    
    
    methods (Access = protected)
        
        function flag = CheckIfXDataAndYDataAreDefined(this)
            % Check if XData and YData are not empty
            % Syntax
            %   flag = CheckIfXDataAndYDataAreDefined(a)
            
            flag = 0;
            if isempty(this.XData) || isempty(this.YData)
%                 str1 = 'Pas de XData ou YData idans ce mod�le.';
                str2 = 'No XData or YData in this model.';
                my_warndlg(str2, 1);
                return
            end
            
            if length(this.XData) ~= length(this.YData)
%                 str1 = 'XData et YData n''ont pas la m�me taille.';
                str2 = 'XData and YData are not the same size.';
                my_warndlg(str2, 1);
                return
            end
            flag = 1;
        end
    end
end
