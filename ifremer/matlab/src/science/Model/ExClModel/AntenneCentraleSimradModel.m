classdef AntenneCentraleSimradModel < ClModel
    % Description
    %   Create a AntenneCentraleSimradModel instance (ClModel ineheritage)
    %
    % Syntax
    %   model = AntenneCentraleSimradModel(...)
    %
    % AntenneCentraleSimradModel properties :
    %   XData        - XData of experimental data
    %   YData        - YData of experimental data
    %   ValParams    - Values of the parametres     (default [-4.76188  23.7955 -30.1585   2.48292  3.09409 -30])
    %   ValMinParams - Min values of the parametres (default [-8         0      -50      -40       -1       -50])
    %   ValMaxParams - Max values of the parametres (default [ 0        40        0       40       10         0])
    %   Enable       - Enable randomization of the parametres (default [1 1 1 0])
    %
    % Output Arguments
    %   model : One AntenneCentraleSimradModel (ClModel ineheritage)
    %
    % Examples
    %   a = AntenneCentraleSimradModel('XData', -80:80)
    %   a = noise(a, 'Coeff', 5);
    %   plot(a);
    %
    % Authors : JMA
    % See also AntenneSincDep ClModel AntennePolyModel Authors
    %-------------------------------------------------------------------------------
    
    methods
        
        function this = AntenneCentraleSimradModel(varargin)
            
            % Superclass Constructor
            this = this@ClModel(varargin{:});
            
            [varargin, ValParams]    = getPropertyValue(varargin, 'ValParams',    [-4.76188  23.7955 -30.1585   2.48292  3.09409 -30]);
            [varargin, ValMinParams] = getPropertyValue(varargin, 'ValMinParams', [-8         0      -50      -40       -1       -50]);
            [varargin, ValMaxParams] = getPropertyValue(varargin, 'ValMaxParams', [ 0        40        0       40       10         0]);
            [varargin, Enable]       = getPropertyValue(varargin, 'Enable',       [1 1 1 1 1 1]);
            [varargin, XData]        = getPropertyValue(varargin, 'XData',        []);
            [varargin, YData]        = getPropertyValue(varargin, 'YData',        []); %#ok<ASGLU>
            
            if ~checkIfOnlyOneInstance(length(this))
                return
            end
            
            %% Create the parametres
            
            % Parameter 1
            Params(1) = ClParametre;
            Params(1).Name     = 'p(1)';
            Params(1).Unit     = 'dB';
            Params(1).MinValue = ValMinParams(1);
            Params(1).MaxValue = ValMaxParams(1);
            Params(1).Value    = ValParams(1);
            Params(1).Enable   = Enable(1);
            
            % Parameter 2
            Params(2) = ClParametre;
            Params(2).Name     = 'p(2)';
            Params(2).Unit     = 'dB/deg^2';
            Params(2).MinValue = ValMinParams(2);
            Params(2).MaxValue = ValMaxParams(2);
            Params(2).Value    = ValParams(2);
            Params(2).Enable   = Enable(2);
            
            % Parameter 3
            Params(3) = ClParametre;
            Params(3).Name     = 'p(3)';
            Params(3).Unit     = 'dB/deg^4';
            Params(3).MinValue = ValMinParams(3);
            Params(3).MaxValue = ValMaxParams(3);
            Params(3).Value    = ValParams(3);
            Params(3).Enable   = Enable(3);
            
            % Parameter 4
            Params(4) = ClParametre;
            Params(4).Name     = 'p(4)';
            Params(4).Unit     = 'dB/deg^6';
            Params(4).MinValue = ValMinParams(4);
            Params(4).MaxValue = ValMaxParams(4);
            Params(4).Value    = ValParams(4);
            Params(4).Enable   = Enable(4);
            
            % Parameter 5
            Params(5) = ClParametre;
            Params(5).Name     = 'p(5)';
            Params(5).Unit     = 'dB/deg^8';
            Params(5).MinValue = ValMinParams(5);
            Params(5).MaxValue = ValMaxParams(5);
            Params(5).Value    = ValParams(5);
            Params(5).Enable   = Enable(5);
            
            % Parameter 6
            Params(6) = ClParametre;
            Params(6).Name     = 'p(6)';
            Params(6).Unit     = 'dB/deg^10';
            Params(6).MinValue = ValMinParams(6);
            Params(6).MaxValue = ValMaxParams(6);
            Params(6).Value    = ValParams(6);
            Params(6).Enable   = Enable(6);
            
            %% Set the model
            
            this.set('Name', 'AntenneCentraleSimrad', 'functionName', 'AntenneCentraleSimrad', ...
                'Params', Params, ...
                'XData', XData, 'YData', YData, ...
                'xLabel', 'Angles', 'xUnit', 'deg', ...
                'yLabel', 'Gain', 'yUnit', 'dB');
        end
    end
end
