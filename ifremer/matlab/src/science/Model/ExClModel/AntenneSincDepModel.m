classdef AntenneSincDepModel < ClModel
    % Description
    %   Create a AntenneSincDepModel instance (ClModel ineheritage)
    %
    % Syntax
    %   model = AntenneSincDepModel(...)
    %
    % AntenneSincDepModel properties :
    %   XData        - XData of experimental data
    %   YData        - YData of experimental data
    %   ValParams    - Values of the parametres     (default [  0   0  3.0   0])
    %   ValMinParams - Min values of the parametres (default [-10 -45  0.5 -60])
    %   ValMaxParams - Max values of the parametres (default [ 10  45 40.0  60])
    %   Enable       - Enable randomization of the parametres (default [1 1 1 0])
    %
    % Output Arguments
    %   model : One AntenneSincDepModel (ClModel ineheritage)
    %
    % Examples
    %   a = AntenneSincDepModel('XData', -80:80)
    %   a = noise(a, 'Coeff', 5);
    %   plot(a);
    %
    % Authors : JMA
    % See also AntenneSincDep ClModel ModelDialog AntennePolyModel Authors
    %-------------------------------------------------------------------------------
    
    methods
        
        function this = AntenneSincDepModel(varargin)
            
            % Superclass Constructor
            this = this@ClModel(varargin{:});
            
            % [varargin, Name] = getPropertyValue(varargin, 'Name', this.Name);% TODO MHO
            [varargin, Name]         = getPropertyValue(varargin, 'Name',         'AntenneSincDep');
            [varargin, ValParams]    = getPropertyValue(varargin, 'ValParams',    [  0   0  3.0   0]);
            [varargin, ValMinParams] = getPropertyValue(varargin, 'ValMinParams', [-10 -45  0.5 -60]);
            [varargin, ValMaxParams] = getPropertyValue(varargin, 'ValMaxParams', [ 10  45 40.0  60]);
            [varargin, Enable]       = getPropertyValue(varargin, 'Enable',       [1 1 1 0]);
            [varargin, XData]        = getPropertyValue(varargin, 'XData',        []);
            [varargin, YData]        = getPropertyValue(varargin, 'YData',        []); %#ok<ASGLU>
            
            if ~checkIfOnlyOneInstance(length(this))
                return
            end
            
            %% Create the parametres
            
            % param Gain properties
            Params(1) = ClParametre;
            Params(1).Name     = 'Gain';
            Params(1).Unit     = 'dB';
            Params(1).MinValue = ValMinParams(1);
            Params(1).MaxValue = ValMaxParams(1);
            Params(1).Value    = ValParams(1);
            Params(1).Enable   = Enable(1);
            
            % param Centre properties
            Params(2) = ClParametre;
            Params(2).Name     = 'Centre';
            Params(2).Unit     = 'deg';
            Params(2).MinValue = ValMinParams(2);
            Params(2).MaxValue = ValMaxParams(2);
            Params(2).Value    = ValParams(2);
            Params(2).Enable   = Enable(2);
            
            % param Width properties
            Params(3) = ClParametre;
            Params(3).Name     = 'Width';
            Params(3).Unit     = 'deg';
            Params(3).MinValue = ValMinParams(3);
            Params(3).MaxValue = ValMaxParams(3);
            Params(3).Value    = ValParams(3);
            Params(3).Enable   = Enable(3);
            
            % param Inclinaison properties
            Params(4) = ClParametre;
            Params(4).Name     = 'Inclinaison';
            Params(4).Unit     = 'deg';
            Params(4).MinValue = ValMinParams(4);
            Params(4).MaxValue = ValMaxParams(4);
            Params(4).Value    = ValParams(4);
            Params(4).Enable   = Enable(4);
            
            %% Set the model
            
            this.set('Name', Name, 'functionName', 'AntenneSincDep', ...
                'Params', Params, ...
                'XData', XData, 'YData', YData, ...
                'xLabel', 'Angles', 'xUnit', 'deg', ...
                'yLabel', 'Gain', 'yUnit', 'dB');
        end
    end
end
