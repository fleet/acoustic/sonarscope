classdef AntenneSplineModel < ClModel
    % Description
    %   Create a AntenneSplineModel instance (ClModel ineheritage)
    %
    % Syntax
    %   model = AntenneSplineModel(...)
    %
    % AntenneSplineModel properties :
    %   XData        - XData of experimental data
    %   YData        - YData of experimental data
    %   ValParams    - Values of the parametres     (default [  0   0  3.0])
    %   ValMinParams - Min values of the parametres (default [-10 -45  0.5])
    %   ValMaxParams - Max values of the parametres (default [ 10  45 40.0])
    %   Enable       - Enable randomization of the parametres (default [1 1 1])
    %
    % Output Arguments
    %   model : One AntenneSplineModel (ClModel ineheritage)
    %
    % Examples
    %   XData  = -80:0.5:-30;
    %   xNodes = -80:10:-30;
    %   yNodes = [-9.8 -4.0 -0.7  1.0 -1.1 -8.0];
    %   a = AntenneSplineModel('XData', XData, 'xNodes', xNodes, 'yNodes', yNodes)
    %   a = noise(a, 'Coeff', 2);
    %   plot(a);
    %
    % Authors : JMA
    % See also AntenneSpline ModelDialog ClModel AntenneSincDepModel Authors
    %-------------------------------------------------------------------------------
    
    methods
        
        function this = AntenneSplineModel(varargin)
            % Constructor of the class
            
            % Superclass Constructor
            this = this@ClModel(varargin{:});
            
            % [varargin, Name] = getPropertyValue(varargin, 'Name', this.Name);
            [varargin, Name]   = getPropertyValue(varargin, 'Name',   'AntenneSpline');
            [varargin, xNodes] = getPropertyValue(varargin, 'xNodes', []);
            [varargin, yNodes] = getPropertyValue(varargin, 'yNodes', []);
            [varargin, Enable] = getPropertyValue(varargin, 'Enable', []);
            [varargin, XData]  = getPropertyValue(varargin, 'XData',  []);
            [varargin, YData]  = getPropertyValue(varargin, 'YData',  []); %#ok<ASGLU>
            
            if ~checkIfOnlyOneInstance(length(this))
                return
            end
            
            %% Create the parametres
            
            N = length(xNodes);
            if N == 0
                yNodes = [];
                Params = ClParametre.empty;
            end
            if length(xNodes) ~= length(yNodes)
                N = 0;
                xNodes = [];
                yNodes = [];
                Enable = [];
            end
            
            if isempty(yNodes) && (N ~= 0)
                yNodes = zeros(1,N) + mean(YData);
            end
            
            if isempty(Enable)
                Enable = ones(1,N);
            end
            
            for k=N:-1:1
                NameNode = sprintf('Node %s deg', num2str(xNodes(k)));
                k2 = max(2,k);
                k2 = min(k2,N-1);
                sub = (k2-1:k2+1);
                Maxy = max(yNodes(sub));
                Miny = min(yNodes(sub));
                
                if ~isempty(YData)
                    Maxy = max(Maxy, max(YData));
                    Miny = min(Miny, min(YData));
                end
                
                Marge = (Maxy-Miny)/10;
                Miny = Miny - Marge;
                Maxy = Maxy + Marge;
                
                Params(k) = ClParametre;
                Params(k).Name     = NameNode;
                Params(k).Unit     = 'dB';
                Params(k).MinValue = Miny;
                Params(k).MaxValue = Maxy;
                Params(k).Value    = yNodes(k);
                Params(k).Enable   = Enable(k);
            end
            
            %% Set the model
            
            this.set('Name', Name, 'functionName', 'AntenneSpline', ...
                'Params', Params, ...
                'XData', XData, 'YData', YData, 'ParamsSup', {xNodes}, ...
                'xLabel', 'Angles', 'xUnit', 'deg', ...
                'yLabel', 'Gain', 'yUnit', 'dB');
        end
    end
end
