classdef BSJacksonAPLModel < ClModel
    % Description
    %   Create a BSJacksonAPLModel instance (ClModel ineheritage)
    %
    % Syntax
    %   model = BSJacksonAPLModel(...)
    %
    % BSJacksonAPLModel properties :
    %   XData        - XData of experimental data
    %   YData        - YData of experimental data
    %   ValParams    - Values of the parametres     (default [ 300  5])
    %   ValMinParams - Min values of the parametres (default [ 100  1])
    %   ValMaxParams - Max values of the parametres (default [1000  9])
    %   Enable       - Enable randomization of the parametres (default [0 1])
    %
    % Output Arguments
    %   model : One BSJacksonAPLModel (ClModel ineheritage)
    %
    % Examples
    %   a = BSJacksonAPLModel('XData', -80:80)
    %   a = noise(a, 'Coeff', 5);
    %   plot(a);
    %
    %   a = BSJacksonAPLModel('XData', -80:80, 'ValParams', [100 2]);
    %   a = noise(a, 'Coeff', 5);
    %   plot(a);
    %
    % Authors : JMA
    % See also BSLurton ClModel BSJacksonModel BSGuillonModel Authors
    %-------------------------------------------------------------------------------
    
    methods
        function this = BSJacksonAPLModel(varargin)
            
            % Superclass Constructor
            this = this@ClModel(varargin{:});
            
            [varargin, XData]        = getPropertyValue(varargin, 'XData',        -80:80);
            [varargin, YData]        = getPropertyValue(varargin, 'YData',        []);
            [varargin, ValParams]    = getPropertyValue(varargin, 'ValParams',    [ 300 10]);
            [varargin, ValMinParams] = getPropertyValue(varargin, 'ValMinParams', [ 100  1]);
            [varargin, ValMaxParams] = getPropertyValue(varargin, 'ValMaxParams', [1000 23]);
            [varargin, Enable]       = getPropertyValue(varargin, 'Enable',       [0 1]); %#ok<ASGLU>
            
            %             %% Check if all the arguments have been interpretated
            %
            %             if checkIfVararginIsEmpty(varargin)
            %                 return
            %             end
            
            %% Create the parametres
            
            global SonarScopeDocumentation %#ok<GVMIS>
            
            nomHtml = fullfile(SonarScopeDocumentation, 'ADOC-HTML', 'PAMES-ADOC', 'target_strength.html');
            
            Params(1) = ClParametre(...
                'Name',     'Frequency', ...
                'Unit',     'kHz', ...
                'MinValue', ValMinParams(1), ...
                'MaxValue', ValMaxParams(1), ...
                'Value',    ValParams(1), ...
                'Help',     ['file://' nomHtml], ...
                'Enable',   Enable(1));
            
            Params(2) = ClParametre(...
                'Name',     'Index', ...
                'Unit',     'deg', ...
                'MinValue', ValMinParams(2), ...
                'MaxValue', ValMaxParams(2), ...
                'Value',    ValParams(2), ...
                'Help',     ['file://' nomHtml], ...
                'Enable',   Enable(2), ...
                'Format',  '%d');
            
            %% Create the model
            
            ComponentsName = {'Kirchhoff'; 'Rugosite composee'; 'medium Roughness';'large Roughness'; 'Surface';'Volume'; 'Total'};
            this.set('Name', 'BSJacksonAPL', 'Params', Params, 'ComponentsName', ComponentsName, ...
                'XData', XData, 'YData', YData, ...
                'xLabel', 'Angles', 'xUnit', 'deg', ...
                'yLabel', 'BS',     'yUnit', 'dB');
            
            % editProperties(model);
        end
    end
end
