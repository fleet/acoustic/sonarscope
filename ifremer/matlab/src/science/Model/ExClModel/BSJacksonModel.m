classdef BSJacksonModel < ClModel
    % Description
    %   Create a BSJacksonModel instance (ClModel ineheritage)
    %
    % Syntax
    %   model = BSJacksonModel(...)
    %
    % BSJacksonModel properties :
    %   XData        - XData of experimental data
    %   YData        - YData of experimental data
    %   ValParams    - Values of the parametres     (default [  0  3   -30 2 -25 15])
    %   ValMinParams - Min values of the parametres (default [-10  0.1 -50 0 -50 10])
    %   ValMaxParams - Max values of the parametres (default [ 10 10   -10 4 -10 25])
    %   Enable       - Enable randomization of the parametres (default [1 1 1 1 1 1 0])
    %
    % Output Arguments
    %   model : One BSJacksonModel (ClModel ineheritage)
    %
    % Examples
    %   a = BSJacksonModel('XData', -80:80, 'Freq', 100)
    %   a = noise(a, 'Coeff', 2);
    %   plot(a);
    %
    % See also : BSJackson ClModel refJackson refHamilton BSLurtonModel BSGuillonModel Authors
    % Authors  : MHO + VB + JMA
    %-------------------------------------------------------------------------------
    
    methods
        function this = BSJacksonModel(varargin)
            
            % Superclass Constructor
            this = this@ClModel(varargin{:});
            
            [varargin, Freq]         = getPropertyValue(varargin, 'Freq',         13);
            [varargin, ValParams]    = getPropertyValue(varargin, 'ValParams',    [Freq 1520 1.0  2.0  3.25 4e-2 -25 0.48]);
            [varargin, ValMinParams] = getPropertyValue(varargin, 'ValMinParams', [   6 1453 0.90 1.1  3.00 5e-4 -60 0.00]);
            [varargin, ValMaxParams] = getPropertyValue(varargin, 'ValMaxParams', [ 500 1550 2.5  2.50 3.50 0.2  -10 1.20]);
            [varargin, Enable]       = getPropertyValue(varargin, 'Enable',       [0 0 1 1 0 1 1 1]);
            [varargin, XData]        = getPropertyValue(varargin, 'XData',        []);
            [varargin, YData]        = getPropertyValue(varargin, 'YData',        []); %#ok<ASGLU>
            
            %             %% Check if all the arguments have been interpretated
            %
            %             if checkIfVararginIsEmpty(varargin)
            %                 return
            %             end
            
            
            %% Create the parametres
            
            global SonarScopeDocumentation %#ok<GVMIS> 
            
            nomHtml = fullfile(SonarScopeDocumentation, 'ADOC-HTML', 'PAMES-ADOC', 'target_strength.html');
            
            Params(1) = ClParametre('Name', 'Frequency', 'Unit', 'kHz', ...
                'MinValue', ValMinParams(1), ...
                'MaxValue', ValMaxParams(1), ...
                'Value',    ValParams(1), ...
                'Help',     ['file://' nomHtml], ...
                'Enable',   Enable(1));
            
            Params(2) = ClParametre('Name', 'Water celerity', 'Unit', 'm/s', ...
                'MinValue', ValMinParams(2), ...
                'MaxValue', ValMaxParams(2), ...
                'Value',    ValParams(2), ...
                'Help',     ['file://' nomHtml], ...
                'Enable',   Enable(2));
            
            Params(3) = ClParametre('Name', 'c_sed/c_water', ...
                'MinValue', ValMinParams(3), ...
                'MaxValue', ValMaxParams(3), ...
                'Value',    ValParams(3), ...
                'Help',     ['file://' nomHtml], ...
                'Enable',   Enable(3));
            
            Params(4) = ClParametre('Name', 'rho_sed/rho_water', ...
                'MinValue', ValMinParams(4), ...
                'MaxValue', ValMaxParams(4), ...
                'Value',    ValParams(4), ...
                'Help',     ['file://' nomHtml], ...
                'Enable',   Enable(4));
            
            Params(5) = ClParametre('Name', 'Spectral exponant', ...
                'MinValue', ValMinParams(5), ...
                'MaxValue', ValMaxParams(5), ...
                'Value',    ValParams(5), ...
                'Help',     ['file://' nomHtml], ...
                'Enable',   Enable(5));
            
            Params(6) = ClParametre('Name', 'Spectrale power', 'Unit', 'm-4', ...
                'MinValue', ValMinParams(6), ...
                'MaxValue', ValMaxParams(6), ...
                'Value',    ValParams(6), ...
                'Help',     ['file://' nomHtml], ...
                'Enable',   Enable(6));
            
            Params(7) = ClParametre('Name', 'Volumic index', 'Unit', 'dB/m3', ...
                'MinValue', ValMinParams(7), ...
                'MaxValue', ValMaxParams(7), ...
                'Value',    ValParams(7), ...
                'Help',     ['file://' nomHtml], ...
                'Enable',   Enable(7));
            
            Params(8) = ClParametre('Name', 'Attenuation', 'Unit', 'dB/lambda', ...
                'MinValue', ValMinParams(8), ...
                'MaxValue', ValMaxParams(8), ...
                'Value',    ValParams(8), ...
                'Help',     ['file://' nomHtml], ...
                'Enable',   Enable(8));
            
            %% Create the model
            
            ComponentsName = {'Kirchhoff'; 'Rugosite composee'; 'medium Roughness';'large Roughness'; 'Surface';'Volume'; 'Total'};
            this.set('Name', 'BSJackson', 'Params', Params, 'ComponentsName', ComponentsName, ...
                'XData', XData, 'YData', YData, ...
                'xLabel', 'Angles', 'xUnit', 'deg', ...
                'yLabel', 'BS',     'yUnit', 'dB');
            
            %% Create the dependancies
            
            % Celerity / Density
            
            XLabel = 'p4';
            YLabel = 'p3';
            densite_eau = 1.026; %sw_dens(s,t,p);%salinite temperature pression(db)
            %celerite_eau=1836/1.201; %= celeriteChen(Z, T, S);
            celerite_eau = ValParams(2);
            xmin = 1.20;
            xmax = 2.10;
            ymin = 1480;
            ymax = 1900;
            %densit� de probabilit�
            nomFicMat = getNomFicDatabase('Jackson.mat');
            sound_velocity = loadmat(nomFicMat, 'nomVar', 'I');
            %sound_velocity = sound_velocity/sum(sum(sound_velocity));
            %abscisses et ordonn�es
            [taillex, tailley] = size(sound_velocity);
            pasx = (xmax-xmin)/(taillex-1);
            pasy = (ymax-ymin)/(tailley-1);
            x = xmin:pasx:xmax;
            x = x/densite_eau;
            y = ymin:pasy:ymax;
            y = y/celerite_eau;
            
            d1 = cl_image('Name', 'BSJackson', 'Image', sound_velocity, 'XLabel', XLabel, 'x', x, 'YLabel', YLabel, 'y', y);
            
            this.Dependencies = d1;
        end
    end
end
