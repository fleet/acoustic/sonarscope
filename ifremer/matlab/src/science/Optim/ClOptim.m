classdef ClOptim < handle & AbstractDisplay & Cloneable & matlab.mixin.Heterogeneous
    %
    % Description
    %   Class that deals with the optimization of parametres of a model
    %
    % Syntax
    %   a = ClOptim(...)
    %
    % ClOptim properties :
    %   Name		   - Name of the optimization model
    %   xLabel		   - Name of abscissa
    %   yLabel		   - Name of ordinates
    %   XData		   - XData of experimental data
    %   YData		   - XData of experimental data
    %   AlgoType	   - Type of the optimization algorithm {1='Simplex'}; 2='Newton'
    %   UseWeights	   - Boolean that indicates if the weighting is enable
    %   UseDependences - Boolean that indicates if the dependance have to be taken into account
    %   UseSimAnneal   - Boolean that indicates if the simulation annealing is enable
    %   UseVisu		   - Boolean that indicates if the intermediate results are plotted
    %   Axe			   - Axe handle of the plot if UseVisu => Yes
    %   models		   - Agregation of ClModel instances
    %   currentModel   - Index of the current model
    %   TolF		   - Tolerance of the algorithm
    %   MaxIter		   - Maximum number of iterations
    %   AlgoTypeStr	   - {'Simplex'; 'Newton'};
    %
    % Output Arguments
    %   a : One ClOptim instance
    %
    % Examples
    %   XData = -80:80;
    %   a = BSLurtonModel('XData', XData);
    %   a.noise('Coeff', 5);                         % a = noise(a, 'Coeff', 5);
    %   a.plot;                                      % plot(a);
    %   YData = a.YData;
    %   a.setParamsValue([0 5 -30 3 -55 20 0]);      % a = set(a, [0 5 -30 3 -55 20 0]);
    %   a.plot;
    %
    %   optim = ClOptim('XData', XData, 'YData', YData, ...
    %         'xLabel', 'Incidence angles', ...
    %         'yLabel', 'Gain', ...
    %         'Name', 'Gain');
    %
    %   optim.set('models', a);                      % optim = set(optim, 'models', a);
    %   optim.plot;                                  % plot(optim)
    %
    %   optim.random;                                % optim = random(optim)
    %   optim.plot;                                  % plot(optim)
    %   optim.set('AlgoType', 1, 'UseSimAnneal', 0); % optim = set(optim, 'AlgoType', 1, 'UseSimAnneal', 0);
    %   optim.optimize;                              % optim = optimize(optim);
    %   optim.plot;                                  % plot(optim)
    %
    %   optim.Name                                   % % get(optim, 'Name')
    %   optim.Name = 'Hello world';                  % optim = set(optim, 'Name', 'Hello world');
    %   optim.Name
    %   optim = set(optim, 'Name', 'A second way to set Name');
    %   optim.Name
    %   optim.set('Name', 'A third way to set Name');
    %   optim.Name
    %
    %   optim.editProperties(); % editProperties(optim);
    %
    %   optim.plot;
    %   a = OptimDialog(optim, 'Title', 'Optim ExClOptimBS', 'windowStyle', 'normal');
    %   a.openDialog;
    %   a.optim.plot
    %   ParamsValue = a.optim.getParamsValue
    %   a.optim.editProperties;
    %
    % Authors : MHO + JMA
    % See also ClParametre BSLurtonModel OptimDialog Authors
    % ----------------------------------------------------------------------------
    
    properties (Constant)
        % WeightsTypeStr  = {'none'; 'nb points'; 'variance'; 'std deviation'}; % Type names of the weights
        AlgoTypeStr	= {'Simplex'; 'Newton'}; % Type names of the optimization algorithms
    end
    
    properties (Access = public)
        Name		   = '';    % Name of the optimization model
        xLabel		   = ''     % Name of abscissa
        yLabel		   = ''     % Name of ordinates
        XData		   = [];    % XData of experimental data
        YData		   = [];    % XData of experimental data
        UseWeights	   = true;  % Boolean that indicates if the weighting is enable
        UseDependences = true;  % Boolean that indicates if the dependance have to be taken into account
        AlgoType       = 2;     % Type of the optimization algorithm {1='Simplex'}; 2='Newton'
        UseSimAnneal   = false; % Boolean that indicates if the simulation annealing is enable
        UseVisu		   = false; % Boolean that indicates if the intermediate results are plotted
        Axe			   = [];    % Axe handle of the plot if UseVisu => Yes
        currentModel   = 1      % Index of the current model
        TolF		   = 0;     % Tolerance of the algorithm
        MaxIter		   = 100    % Maximum number of iterations
        models		   = ClModel.empty; % Agregation of ClModel instances
    end
    
    methods
        
        function this = ClOptim(varargin)
            % Constructor of the class
            
            %% Test if the user wants an empty instance
            if (nargin == 1) && isempty(varargin{1})
                this(1) = [];
                return
            end
            
            if ~isempty(varargin)
                this = set(this, varargin{:});
            end
        end
        
        function ParamsValue = getParamsValue(this, varargin)
            % Get the "Value" properties of the parametres of the current model
            % Examples
            %   ParamsValue = getParamsValue(optim)
            
            % Je ne me souviens plus pourquoi il y a varargin donc je mets
            % ce contr�le pour le d�couvrir
            if checkIfVararginIsEmpty(varargin)
                return
            end
            
            ParamsValue = getParamsValue(this.models(this.currentModel));
        end
        
        function this = set(this, varargin)
            % Setter of the form a = set(a, ...)
            % Examples
            %   optim = set(optim, 'Name', 'Hello world');
            %   optim.Name
            %   optim = optim.set('Name', 'A second way to set Name through the set method');
            %   optim.Name
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc ClOptim/set">ClOptim/set</a>
            %   <a href="matlab:doc ClOptim">ClOptim</a>
            
            [varargin, this.Name]           = getPropertyValue(varargin, 'Name',           this.Name);
            [varargin, this.xLabel]         = getPropertyValue(varargin, 'xLabel',         this.xLabel);
            [varargin, this.yLabel]         = getPropertyValue(varargin, 'yLabel',         this.yLabel);
            [varargin, this.XData]          = getPropertyValue(varargin, 'XData',          this.XData);
            [varargin, this.YData]          = getPropertyValue(varargin, 'YData',          this.YData);
            [varargin, this.UseWeights]     = getPropertyValue(varargin, 'UseWeights',     this.UseWeights);
            [varargin, this.UseDependences] = getPropertyValue(varargin, 'UseDependences', this.UseDependences);
            [varargin, this.AlgoType]       = getPropertyValue(varargin, 'AlgoType',       this.AlgoType);
            [varargin, this.UseSimAnneal]   = getPropertyValue(varargin, 'UseSimAnneal',   this.UseSimAnneal);
            [varargin, this.UseVisu]        = getPropertyValue(varargin, 'UseVisu',        this.UseVisu);
            [varargin, this.Axe]            = getPropertyValue(varargin, 'Axe',            this.Axe);
            [varargin, this.models]         = getPropertyValue(varargin, 'models',         this.models);
            [varargin, this.currentModel]   = getPropertyValue(varargin, 'currentModel',   this.currentModel);
            [varargin, this.TolF]           = getPropertyValue(varargin, 'TolF',           this.TolF);
            [varargin, this.MaxIter]        = getPropertyValue(varargin, 'MaxIter',        this.MaxIter);
            
            %% Check if all the arguments have been interpretated
            
            if checkIfVararginIsEmpty(varargin)
                return
            end
            
            %% Output processing
            
            if nargout == 0
                assignin('caller', inputname(1), this);
            end
        end
        
        function this = setParamsValue(this, ParamsValue)
            % Set Value of the parametres of the current model
            % Examples
            %   ParamsValue = getParamsValue(optim)
            %   ParamsValue(1) = -20
            %   optim = setParamsValue(optim, ParamsValue);
            %   plot(optim);
            
            CurrentModel = this.models(this.currentModel);
            CurrentModel = setParamsValue(CurrentModel, ParamsValue);
            this.models(this.currentModel) = CurrentModel;
        end
        
        function [y, composantes] = compute(this, varargin)
            % Compute the values of the current model on XData abscissa
            % Examples
            %   x = optim.XData;
            %   y = compute(optim);
            %   figure; plot(x, y); grid on;
            %
            %   x = 0:0.5:70;
            %   y = compute(a, 'x', x);
            %   figure; plot(x, y); grid on;
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc ClOptim/compute">ClOptim/compute</a>
            %   <a href="matlab:doc ClOptim">ClOptim</a>
            
            [varargin, a_XData] = getPropertyValue(varargin, 'x', this.XData); %#ok<ASGLU>
            
            y           = [];
            composantes = [];
            
            if isempty(a_XData)
                return
            end
            
            tmp = this.models(this.currentModel);
            
            [y, composantes] = compute(tmp, 'x', a_XData);
        end
        
        function this = optimize(this)
            % Description
            %   Optimize the parameters of the model in order to have a minimum Root Mean Square value between the experimental data and the model
            %
            % Syntax
            %   a = optimize(a)
            %
            % Input Arguments
            %   a : One ClModel instance
            %
            % Output Arguments
            %   a : The updated ClModel instance
            %
            % Example
            %   a = ExClOptimBS;
            %
            %   a = random(a);
            %   plot(a);
            %   a = set(a, 'AlgoType', 1, 'UseSimAnneal', 0);
            %   a = optimize(a);
            %   plot(a);
            %
            %   a = random(a);
            %   plot(a);
            %   a = set(a, 'AlgoType', 1, 'UseSimAnneal', 1);
            %   a = optimize(a);
            %   plot(a);
            %
            %   % Change the maximum number of iterations for Newton because
            %   a.MaxIter = 500;
            %
            %   a = random(a);
            %   plot(a);
            %   a = set(a, 'AlgoType', 2, 'UseSimAnneal', 0);
            %   a = optimize(a);
            %   plot(a);
            %
            %   a = random(a);
            %   plot(a);
            %   a = set(a, 'AlgoType', 2, 'UseSimAnneal', 1);
            %   a = optimize(a);
            %   plot(a);
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc ClOptim/optimize">ClOptim/optimize</a>
            %   <a href="matlab:doc ClOptim">ClOptim</a>
            
            %% Test if XData not empty
            
            oXData = double(this.XData);
            if isempty(oXData)
                return
            end
            
            %% Test si on a la toolbox optimisation au cas o� on demande l'algo de newton
            
            if this.AlgoType == 2
                if ~exist('lsqnonlin', 'file')
                    this.AlgoType = 1; % On impose le simplex si on a pas la toolbox Optimization
                    % TODO MHO : remettre le graphique � jour (bouton
                    % Algorithme Type
                end
            end
            
            %% Appel de la fonction d'optimisation desir�e
            
            switch this.AlgoType
                case 1  % Simplex
                    this = optimizeSimplex(this);
                    
                case 2  % Newton
                    if this.UseSimAnneal % Recuit simul� : Yes
                        this = optimizeNewtonSA(this);
                    else % Recuit simul� : No
                        try
                            this = optimizeNewton(this);
                        catch ME
                            fprintf('%s\n', getReport(ME));
                        end
                    end
                    
                otherwise
%                     str1 = 'M�thode d''optimisation non d�finie.';
                    str2 = 'This optimization method is not defined.';
                    my_warndlg(str2, 0);
            end
        end
        
        function this = optimizeNewton(this)
            % Description
            %   Optimize the parameters of the model in order to have a
            %   minimum Root Mean Square value between the experimental
            %   data and the model with the Newton algorithm without simulated
            %   annealing
            %
            % Syntax
            %   a = optimizeNewton(a)
            %
            % Input Arguments
            %   a : One ClModel instance
            %
            % Output Arguments
            %   a : The updated ClModel instance
            %
            % Example
            %   a = ExClOptimBS;
            %
            %   a = random(a);
            %   plot(a);
            %   a = optimizeNewton(a);
            %   plot(a);
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc ClOptim/optimizeNewton">ClOptim/optimizeNewton</a>
            %   <a href="matlab:doc ClOptim">ClOptim</a>
            
            a_AlgoType = this.AlgoType;
            this.AlgoType = 2;
            
            %% Test XData non vide
            
            oXData = double(this.XData);
            
            if isempty(oXData)
                return
            end
            
            %% Param�tres de l'algorithme d'optimisation
            
            OPTIONS = optimset('Display', 'off', 'TolFun', double(this.TolF), 'MaxIter', double(this.MaxIter), ...
                'Jacobian', 'off'); % , 'UseParallel', true % TODO JMA: demander si // Tbx dans l'IHM
            
            %% R�cup�ration du modele
            
            CurrentModel = this.models(this.currentModel);
            
            %% S�paration des parametres devant �tre optimis�s et ceux qui ne doivent pas l'�tre
            
            paramsDdl = getParamsEnable(CurrentModel);
            paramsIni = getParamsValue(CurrentModel);
            paramsMin = getParamsMinValue(CurrentModel);
            paramsMax = getParamsMaxValue(CurrentModel);
            
            ind_paramsOpt = find(paramsDdl);
            ind_paramsFix = find(~paramsDdl);
            
            paramsOpt = paramsIni(ind_paramsOpt);
            paramsFix = paramsIni(ind_paramsFix);
            
            paramsMin = paramsMin(ind_paramsOpt);
            paramsMax = paramsMax(ind_paramsOpt);
            
            %% Appel de la fonction matlab d'optimisation
            
            % [paramsOpt, resnorm, residual, exitflag, output] = ...
            paramsOpt = lsqnonlin('errFunction', double(paramsOpt), double(paramsMin), double(paramsMax), OPTIONS, this);
            
            %% Sauvegarde des parametres optimises dans l'instance
            
            params(ind_paramsFix) = paramsFix;
            params(ind_paramsOpt) = paramsOpt;
            this = setParamsValue(this, params);
            
            this.AlgoType = a_AlgoType;
        end
        
        function this = optimizeNewtonSA(this, varargin)
            % Description
            %   Optimize the parameters of the model in order to have a
            %   minimum Root Mean Square value between the experimental
            %   data and the model with the Newton algorithm with simulated
            %   annealing
            %
            % Syntax
            %   a = optimizeNewtonSA(a)
            %
            % Input Arguments
            %   a : One ClModel instance
            %
            % Output Arguments
            %   a : The updated ClModel instance
            %
            % Example
            %   a = ExClOptimBS;
            %
            %   a = random(a);
            %   plot(a);
            %   a = optimizeNewtonSA(a);
            %   plot(a);
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc ClOptim/optimizeNewtonSA">ClOptim/optimizeNewtonSA</a>
            %   <a href="matlab:doc ClOptim">ClOptim</a>
            
            
            a_AlgoType = this.AlgoType;
            this.AlgoType = 2;
            
            %% Test si XData non vide
            
            oXData = this.XData;
            
            if isempty(oXData)
                return
            end
            
            %% Initialisation des parametres du recuit simul�
            
            Newt_min   = 7;   % borne minimale du nombre maximal d'iterations de Newton.
            Newt_max   = 100; % 25; % borne maximale du nombre maximal d'iterations de Newton.
            Newt_plus  = 3;   % variation � la hausse nombre maximal d'iterations de Newton.
            Newt_moins = 1;   % variation � la baisse nombre maximal d'iterations de Newton.
            
            Rec_min  = 0.01;  % ratio temperature finale / temp initiale.
            Rec_pas  = 0.95;  % raison de la progression geometrique de refroidissement.
            Rec_dp   = 0.97;  % variation relative de la largeur d'intervalle des parametres.
            n_tirage = 100;   % nombre de tirages aleatoires pour calculer une energie mediane.
            
            %% R�cup�ration du mod�le
            
            CurrentModel = this.models(this.currentModel);
            
            %% S�paration des param�tres devant �tre optimis�s et ceux qui ne doivent pas l'�tre
            
            paramsDdl = getParamsEnable(CurrentModel);
            paramsIni = getParamsValue(CurrentModel);
            paramsMin = getParamsMinValue(CurrentModel);
            paramsMax = getParamsMaxValue(CurrentModel);
            
            ind_paramsOpt = find(paramsDdl);
            ind_paramsFix = find(~paramsDdl);
            
            %% D�but de l'algo
            
            paramsOpt = paramsIni(ind_paramsOpt);
            paramsFix = paramsIni(ind_paramsFix);
            
            paramsMin = paramsMin(ind_paramsOpt);
            paramsMax = paramsMax(ind_paramsOpt);
            
            NbreOfParams  = length(ind_paramsOpt);
            
            paramsBest    = paramsOpt;
            paramsCourant = paramsOpt;
            % paramsTry   = paramsOpt;
            
            a_Err      = errFunction(paramsOpt, this);
            ErrBest    = sqrt(sum(a_Err .* a_Err)) / length(a_Err);
            ErrCourant = ErrBest;
            
            %% Param�tres de l'algorithme d'optimisation
            
            Newt    = Newt_min;
            OPTIONS = optimset('Display', 'off', 'TolFun', this.TolF, 'MaxIter', Newt, 'Jacobian', 'off');
            
            % Calcul de la temperature initiale du recuit simule.
            % Regle emprique qui permet d'avoir une probabilite d'augmentation d'environ 0.5 au debut.
            % Recherche sur ntirage des variations d'energie lorsque les parametres varient initialement dans l'intervalle.
            
            p_randInit = ones(n_tirage,1) * paramsIni(ind_paramsOpt);
            p_min      = ones(n_tirage,1) * paramsMin;
            p_max      = ones(n_tirage,1) * paramsMax;
            sub        = [];
            l_interv   = 1.;
            
            while isempty(sub)
                arecom   = 1;
                nbArecom = 0;
                
                while arecom
                    p_rand   = p_randInit + l_interv * (rand(n_tirage,NbreOfParams) - 0.5) .* (p_max - p_min);
                    arecom   = any(p_rand < p_min) | any(p_rand > p_max);
                    nbArecom = nbArecom +1;
                    if nbArecom > 100
                        break
                    end
                end
                
                subOutOfRange         = find(p_rand < p_min);
                p_rand(subOutOfRange) = p_min(subOutOfRange);
                
                subOutOfRange         = find(p_rand > p_max);
                p_rand(subOutOfRange) = p_max(subOutOfRange);
                
                % % Calcul de l'erreur
                
                a_Err   = errFunction(p_rand, this);
                erreurs = sqrt(sum(a_Err .* a_Err, 2)) / size(a_Err, 2);
                
                [Errmin, Imin] = min(erreurs);
                ErrBest        = Errmin;
                paramsBest     = p_rand(Imin,:);
                
                erreurs  = erreurs - ErrCourant;
                sub      = find(erreurs > 0);
                l_interv = l_interv / 10;
            end
            
            erreurs = erreurs(sub); % on ne consid�re que les augmentations
            erreurs = sort(erreurs);
            
            M = erreurs(ceil(length(erreurs)/2));  % m�diane, m�me en cas une seule valeur
            
            % Parametres de calcul
            temp_debut = 1.44 * M;
            
            % Permet d'avoir une probabilit� d'augmentation d'environ 0.5 au d�but.
            temp_fin = Rec_min * temp_debut;
            
            Boucle     = 0;
            iter       = 0;
            temp       = temp_debut;
            ErrCourant = ErrBest;
            
            paramsTry  = paramsBest;
            
            % CORRECTION BUG DANS LE CAS D'UN RECUIT SIMULE
            if isempty(this.TolF)
                this.TolF = 0;
            end
            % CORRECTION BUG DANS LE CAS D'UN RECUIT SIMULE
            
            while (temp > temp_fin) && (ErrBest > this.TolF) && (Boucle < this.MaxIter)
                
                [paramsOpt, resnorm, residual, ~, output] = ...
                    lsqnonlin('errFunction', paramsTry, paramsMin, paramsMax, OPTIONS, this);
                
                ErrNewt = resnorm / length(residual);
                dE      = ErrBest - ErrNewt;
                
                if dE > 0
                    ErrBest       = ErrNewt;
                    ErrCourant    = ErrNewt;
                    paramsBest    = paramsOpt;
                    paramsCourant = paramsOpt;
                else
                    dE = ErrCourant - ErrNewt;
                    
                    % % On accepte de cro�tre en fonction de la temperature
                    
                    if rand(1) < exp(dE / temp)
                        paramsCourant = paramsTry;
                        ErrCourant    = ErrNewt;
                        %                 fprintf('Ca rechauffe : ');
                    else
                        %                 fprintf('Ca refroidit : ');
                    end
                end
                
                %% Mise � jour
                
                Boucle = Boucle + output.iterations;
                if output.iterations == Newt
                    Newt = min(Newt + Newt_plus, Newt_max);
                else
                    Newt = max(Newt - Newt_moins, Newt_min);
                end
                OPTIONS = optimset(OPTIONS, 'MaxIter', Newt);
                temp = Rec_pas .* temp;
                iter = iter + 1;
                
                coef = Rec_dp ^ Boucle;
                for k=1:NbreOfParams
                    arecom   = 1;
                    nbArecom = 0;
                    while arecom
                        x1 = rand(1);
                        x2 = rand(1);
                        P  = paramsCourant(k) + (paramsMax(k) - paramsMin(k)) ...
                            * (sqrt(-2*log(x1)) .* cos(2*pi*x2)) .* coef;
                        
                        % Recalcul les param�tres pour �tre dans l'intervalle parammin parammax
                        arecom = (P < paramsMin(k)) | (P > paramsMax(k));
                        nbArecom = nbArecom +1;
                        if nbArecom > 100
                            break
                        end
                    end
                    paramsTry(k) = P;
                end
                
                % On remet les param�tres recalcitrants dans l'intervalle parammin parammax
                
                subOutOfRange = find(paramsTry < paramsMin);
                paramsTry(subOutOfRange) = paramsMin(subOutOfRange);
                
                subOutOfRange = find(paramsTry > paramsMax);
                paramsTry(subOutOfRange) = p_max(subOutOfRange);
            end
            
            %% Sauvegarde des param�tres optimis�s dans l'instance
            
            params(ind_paramsOpt) = paramsBest;
            params(ind_paramsFix) = paramsFix;
            this = setParamsValue(this, params);
            
            this.AlgoType = a_AlgoType;
        end
        
        function this = optimizeSimplex(this, varargin)
            % Description
            %   Optimize the parameters of the model in order to have a
            %   minimum Root Mean Square value between the experimental
            %   data and the model with the Simplex algorithm
            %
            % Syntax
            %   a = optimizeSimplex(a)
            %
            % Input Arguments
            %   a : One ClModel instance
            %
            % Output Arguments
            %   a : The updated ClModel instance
            %
            % Example
            %   a = ExClOptimBS;
            %
            %   a = random(a);
            %   plot(a);
            %   a = optimizeSimplex(a);
            %   plot(a);
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc ClOptim/optimizeSimplex">ClOptim/optimizeSimplex</a>
            %   <a href="matlab:doc ClOptim">ClOptim</a>
            
            
            a_AlgoType = this.AlgoType;
            %             this.AlgoType = 1;
            
            %% Test XData non vide
            oXData = this.XData;
            if isempty(oXData)
                return
            end
            
            %% R�cup�ration du mod�le
            
            CurrentModel = this.models(this.currentModel);
            CurrentModel = set(CurrentModel, 'XData', oXData, 'YData', this.YData);
            
            if this.UseVisu
                figure;
                ha(1) = subplot(3,1,1); grid on; hold on; xlabel('Iteration'); ylabel('Tolerance');
                ha(2) = subplot(3,1,2); grid on; hold on; xlabel('Iteration'); ylabel('ErrBas');
                xh  = 1:this.MaxIter;
                yh1 = NaN(1,this.MaxIter);
                yh2 = NaN(1,this.MaxIter);
                hc(1) = plot(ha(1), xh, yh1, '-+b');
                hc(2) = plot(ha(2), xh, yh2, '-+b');
                if this.TolF ~= 0
                    plot(ha(1), [1 this.MaxIter], [this.TolF this.TolF], 'k');
                end
                a_Axe = subplot(3,1,3);
            else
                a_Axe = this.Axe;
            end
            % if isempty(Axe)
            %     fig = figure;
            %     Axe = subplot(1,1,1);
            % end
            
            %% S�paration des param�tres devant �tre optimis�s et ceux qui ne doivent pas l'�tre
            
            paramsDdl = getParamsEnable(CurrentModel);
            paramsIni = getParamsValue(CurrentModel);
            paramsMin = getParamsMinValue(CurrentModel);
            paramsMax = getParamsMaxValue(CurrentModel);
            
            ind_paramsOpt = find(paramsDdl); % param�tres � optimiser
            if isempty(ind_paramsOpt)
%                 str1 = 'Tous les param�tres sont block�s.';
                str2 = 'All the parametres are pin up.';
                my_warndlg(str2, 1);
                return
            end
            
            ind_paramsFix = find(~paramsDdl); % parametres fixes
            
            NbreOfParams = length(ind_paramsOpt);
            
            paramsOpt = paramsIni(ind_paramsOpt);
            paramsFix = paramsIni(ind_paramsFix);
            
            paramsMin = paramsMin(ind_paramsOpt);
            paramsMax = paramsMax(ind_paramsOpt);
            
            %% Initialisation des param�tres de calcul
            
            Err_best   = Inf;
            paramsBest = paramsOpt;
            
            %% Initialisation du vertex
            
            vertex(1,:) = paramsOpt;
            u = rand(NbreOfParams, NbreOfParams);
            vertex(2:(NbreOfParams+1),:) = u .* repmat((paramsMax-paramsMin), [NbreOfParams 1]) + repmat(paramsMin, [NbreOfParams 1]);
            
            %% Temp�rature au cas o� on fasse du recuit simul�
            
            vertex_size = size(vertex);
            paramsMin_matrix = repmat(paramsMin,vertex_size(1),1);
            paramsMax_matrix = repmat(paramsMax,vertex_size(1),1);
            
            if this.UseSimAnneal % Recuit simul�
                temp_debut = (paramsMax_matrix - paramsMin_matrix) / 10; % debatement max pour chaque parametre
                temp_fin   = eps;
                pas_temp   = temp_debut / this.MaxIter; % pas tel que temp < temp_fin coincide avec MaxIter
            else  % Pas de recuit simul�
                temp_debut = 0;
                temp_fin   = 0;
                pas_temp   = 0;
            end
            
            temp = temp_debut;
            all_vertex_mod  = 1;
            
            %% Boucle principale
            
            % OnContinue = 1;
            Iter = 0;
            
            while (temp >= temp_fin) %& OnContinue %#ok
                
                Iter = Iter + 1;
                
                % ------------------------------------------------
                % Perturbation au cas ou on fasse du recuit simule
                vertex = vertex - temp .* random('unif', -1, 1, vertex_size(1), vertex_size(2));
                
                % ----------------------------------------------------------
                % Test pour que les valeurs soient toujours dans les limites
                vertex = min(vertex, paramsMax_matrix); % test valeurs max
                vertex = max(vertex, paramsMin_matrix); % test valeurs min
                
                % --------------------------------------------
                % Calcul de la somme des vertex sur les points
                vertex_sum = sum(vertex, 1);
                
                % ---------------------------------------------------------------
                % Calcul des valeurs de la fonction d'erreur aux points du vertex
                if all_vertex_mod == 1
                    a_Err = errFunction(vertex, this);
                    all_vertex_mod = 0;
                else
                    a_Err(ind_haut,:) = errFunction(vertex(ind_haut,:), this);
                end
                
                % ---------------------
                % Classement des points
                ErrTemp = a_Err;
                [ErrTemp, ind] = sort(ErrTemp);
                
                Err_bas = ErrTemp(1);
                ind_bas = ind(1);
                
                Err_haut = ErrTemp(NbreOfParams+1);
                ind_haut = ind(NbreOfParams+1);
                
                Err_avhaut = ErrTemp(NbreOfParams);
                
                TolR = 2 * abs(Err_bas - Err_haut) / (Err_haut + Err_bas);
                
                % ----------------------------------------------------------------
                % Diminution de la temp�rature au cas ou on fasse du recuit simul�
                temp = temp - pas_temp;
                
                % ---------------------
                % Affichage du resultat
                
                params = vertex(ind_bas,:);
                
                if this.UseVisu
                    yh1(Iter) = TolR;
                    yh2(Iter) = Err_bas;
                    set(hc(1), 'YData', yh1);
                    set(hc(2), 'YData', yh2);
                    drawnow
                end
                
                if Err_bas < Err_best
                    Err_best = Err_bas;
                    paramsBest = params;
                    
                    ParamsValue(ind_paramsOpt) = paramsBest; %#ok
                    ParamsValue(ind_paramsFix) = paramsFix; %#ok
                    
                    CurrentModel = setParamsValue(CurrentModel, ParamsValue);
                    
                    if this.UseVisu
                        str = sprintf('Iter=%d    Err_best=%f    paramsBest=[%s]    temp=%f', Iter, Err_best, num2str(ParamsValue), temp);
                        fprintf('%s\n', str);
                    end
                    
                    if this.UseVisu
                        %                         echelle = axis(a_Axe);
                        plot(CurrentModel, 'Axe', a_Axe);
                        drawnow
                        
                        % Affichage zone de calcul
                        oXDataFlags = CurrentModel.XDataFlags;
                        if ~isempty(oXDataFlags)
                            
                            Xmin_plot = oXData(1);
                            Xmax_plot = oXData(length(oXData));
                            not_zero  = find(oXDataFlags == 1);
                            Xmin_algo = oXData(min(not_zero));
                            Xmax_algo = oXData(max(not_zero));
                            
                            if Xmin_plot ~= Xmin_algo
                                h1 = patch([Xmin_plot Xmin_plot Xmin_algo Xmin_algo], [-1e3 1e3 1e3 -1e3], [0.7 0.7 0.7]);
                                set(h1, 'FaceAlpha', 0.5);
                            end
                            if Xmax_plot ~= Xmax_algo
                                h2 = patch([Xmax_plot Xmax_plot Xmax_algo Xmax_algo], [-1e3 1e3 1e3 -1e3], [0.7 0.7 0.7]);
                                set(h2, 'FaceAlpha', 0.5);
                            end
                        end
                        %                         axis(echelle);
                        drawnow;
                    end
                end
                
                % ----------------------------
                % Test: Optimisation termin�e?
                
                if (Iter >= this.MaxIter) || ( TolR < this.TolF )
                    %         OnContinue = 0;
                    break
                end
                
                % ----------------------------------------------------------------
                % Exploration de l'espace des param�tres par d�formation du vertex
                % ----------------------------------------------------------------
                
                % -------------------------------
                % Reflexion et calcul de l'erreur
                
                [Err_Refl, vertex_Refl] = deform_vertex(this, -1, paramsMin, paramsMax, vertex, vertex_sum, ind_haut);
                
                if Err_Refl < Err_bas
                    
                    % -------------------------------
                    % Expansion (avec reflexion) et calcul de l'erreur
                    
                    [Err_expn, vertex_expn] = deform_vertex(this, -2, paramsMin, paramsMax, vertex, vertex_sum, ind_haut);
                    
                    if Err_expn < Err_haut
                        vertex(ind_haut,:) = vertex_expn;
                    else
                        vertex(ind_haut,:) = vertex_Refl;
                    end
                    
                else
                    
                    if Err_Refl > Err_avhaut
                        
                        if Err_Refl < Err_haut
                            vertex(ind_haut,:) = vertex_Refl;
                            vertex_sum = sum(vertex,1);
                        end
                        
                        % ---------------------------------
                        % Contraction et calcul de l'erreur
                        
                        [Err_cont, vertex_cont] = deform_vertex(this, 0.5, paramsMin, paramsMax, vertex, vertex_sum, ind_haut);
                        
                        if Err_cont > Err_haut
                            
                            % --------------------
                            % Contraction generale
                            
                            vertex_bas = repmat(vertex(ind_bas,:), [(NbreOfParams + 1) 1]);
                            vertex = (vertex + vertex_bas) ./ 2;
                            all_vertex_mod = 1;
                        else
                            vertex(ind_haut,:) = vertex_cont;
                        end
                    else
                        vertex(ind_haut,:) = vertex_Refl;
                    end
                end
            end
            
            if this.UseVisu
                linkaxes(ha, 'x')
            end
            
            %% Sauvegarde des parametres optimises dans l'instance
            
            ParamsValue(ind_paramsOpt) = paramsBest;
            ParamsValue(ind_paramsFix) = paramsFix;
            this = setParamsValue(this, ParamsValue);
            
            this.AlgoType = a_AlgoType;
        end
        
        function [Axe, GCF, hCurves] = plot(this, varargin)
            % Description
            %   Plot the current model with the experimental data if they exist
            %
            % Syntax
            %   [Axe, GCF] = plot(a, ...)
            %
            % Input Arguments
            %   a : ClModel instance(s)
            %
            % Name-Value Pair Arguments
            %    Axe            : Handle of the axis where to plot the model (default [])
            %    Erase          : Boolean to ask a RAZ of the figure (default true)
            %    DrawComponents : Supperimpose DrawComponents property for all instancies
            %    Legend         : Your own legend
            %    Name           : Name of the figure
            %
            % Name-Value Arguments
            %   NoData : Do not display the experimental data
            %
            % Output Arguments
            %   Axe : Handle of the axis
            %   GCF : Handle of the figure
            %
            % Examples
            %   a = optim;
            %   plot(a);
            %
            %   y = compute(a);
            %   a.YData = y + 5*(rand(size(y))-0.5);
            %   plot(a);
            %   plot(a, 'NoData', 1);
            %   plot(a, 'DrawComponents', 0);
            %   plot(a, 'DrawComponents', 0, 'NoData', 1, 'Name', 'Hello world', 'Legend', 'Holly curve');
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc ClOptim/plot">ClOptim/plot</a>
            %   <a href="matlab:doc ClOptim">ClOptim</a>
            
            if ~checkIfOnlyOneInstance(length(this))
                return
            end
            
            [varargin, X] = getPropertyValue(varargin, 'Name', []);
            if isempty(X)
                X = sprintf('%s - %s - SimAnneal : %s - MaxIter : %d', this.Name, this.AlgoTypeStr{this.AlgoType}, num2strCode(this.UseSimAnneal == 1), this.MaxIter);
            end
            CurrentModel = this.models(this.currentModel);
            CurrentModel = set(CurrentModel, 'XData', this.XData, 'YData', this.YData);
            [Axe, GCF, hCurves] = plot(CurrentModel, 'Title', X, varargin{:});
        end
        
        function this = random(this, varargin)
            % Description
            %   Randomize the parametres of the current model
            %
            % Syntax
            %   b = random(a)
            %
            % Input Arguments
            %   a : One ClOptim instance
            %
            % Name-Value Pair Arguments
            %   identModel : Index of the model (default  : current model)
            %
            % Output Arguments
            %   b : One ClOptim instance
            %
            % Examples
            %   plot(optim);
            %   b = random(optim);
            %   plot(b);
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc ClOptim/random">ClOptim/random</a>
            %   <a href="matlab:doc ClOptim">ClOptim</a>
            
            if ~checkIfOnlyOneInstance(length(this))
                return
            end
            
            [varargin, sub] = getPropertyValue(varargin, 'identModel', this.currentModel); %#ok<ASGLU>
            
            for k=1:length(sub)
                m = this.models(sub(k));
                m = random(m);
                this.models(sub(k)) = m;
            end
        end
        
        function Err = errFunction(paramsTransmis, this)
            % Fonction d'erreur utilis�e par les algorithmes
            % Compute the eror function between experimental data and model
            % that is used to run the optimiztion tools. This function is
            % different from the "rms" method because some  modifications
            % are done on the error value to take into account the
            % dependencies.
            % TODO JMA : this method could be simplified using the rms method : TODO TODO TODO
            %
            % Syntax
            %   Err = errFunction(paramsTransmis, a)
            %
            % Input Arguments
            %   paramsTransmis : Parameters of the model for N evaluations of the model
            %   a              : ClOptim instances
            %
            % Output Arguments
            %   Err : Error between YData and Model for the N evaluations
            %
            % Examples
            %   plot(optim);
            %   Err = rms(pppp, a) % TODO JMA d�finir pppp
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc ClOptim/errFunction">ClOptim/errFunction</a>
            %   <a href="matlab:doc ClOptim">ClOptim</a>
            
            % R�cup�ration du mod�le
            CurrentModel = this.models(this.currentModel);
            
            % Recherche des valeurs sur lesquelles l'algo doit �tre appliqu�
            % (ce qui revient � calculer l'erreur juste sur la zone de travail)
            oXDataFlags = CurrentModel.XDataFlags;
            
            % Comment� par JMA le 19/04/2017 car pas la m�me taille entre
            oXData = this.XData;
            oYData = this.YData;
            
            % Modif JMA le 19/04/2017 : Ca ne change rien et �a peut avoir
            % des effets de bords
            %             oXData      = CurrentModel.XData;
            %             oYData      = CurrentModel.YData;
            
            sizeBeforeSub = size(oXData);
            
            sub = ~isnan(oYData);
            oXData = oXData(sub);
            oYData = oYData(sub);
            
            oXData_size = size(oXData);
            if isempty(oXDataFlags)
                oXDataFlags = ones(oXData_size); % Toute la zone si aucune zone precis�e
            else
                try
                    oXDataFlags = oXDataFlags(sub);
                catch
                    messageForDebugInspection 19/04/2017
                end
            end
            oXDataFlags_indice = find(oXDataFlags == 1);
            
            paramsDdl = getParamsEnable(CurrentModel);
            paramsOpt = getParamsValue(CurrentModel);
            
            ind_paramsOpt = find(paramsDdl);
            ind_paramsFix = find(~paramsDdl);
            
            for k=1:size(paramsTransmis, 1)
                params(ind_paramsOpt) = paramsTransmis(k,:); %#ok
                params(ind_paramsFix) = paramsOpt(ind_paramsFix); %#ok
                
                %% Mod�lisation
                
                CurrentModel = setParamsValue(CurrentModel, params);
                Simulees = compute(CurrentModel);
                Simulees = Simulees(sub);
                
                %% Erreur : donn�es - valeurs modelis�es
                
                ys = Simulees(oXDataFlags_indice);
                yd = oYData(oXDataFlags_indice);
                Err(k,oXDataFlags_indice) = ys(:)' - yd(:)'; %#ok<AGROW>
                Err = abs(Err);
            end
            
            %% Prise en compte de la pond�ration
            
            if this.UseWeights % Poids actives
                switch CurrentModel.WeightsType
                    case 1  % none
                        a_WeightsData = ones(sizeBeforeSub);
                    case 2  % nb points
                        a_WeightsData = CurrentModel.WeightsData;
                    case 3  % variance
                        a_WeightsData = 1 ./ sqrt(CurrentModel.WeightsData);
                    case 4  % ecarts-type
                        a_WeightsData = 1 ./ CurrentModel.WeightsData;
                end
            else % Poids d�sactiv�s
                a_WeightsData = ones(sizeBeforeSub);
            end
            a_WeightsData = a_WeightsData(sub)';
            
            %% Suppression des mauvais poids (infinis, NaN et nuls)
            
            oXDataFlags(~isfinite(a_WeightsData)) = 0;
            oXDataFlags(isnan(a_WeightsData))	  = 0;
            oXDataFlags(a_WeightsData == 0)	      = 0;
            
            %% Suppression des mauvaises valeurs (infines, NaN et comlexes)
            
            oXDataFlags(~isfinite(Err)) = 0;
            oXDataFlags(isnan(Err))	    = 0;
            oXDataFlags(imag(Err) ~= 0) = 0; % Oui car ca arrive malheureusement : BSJackson rend des nombres complexes
            
            oXDataFlags_indice = find(oXDataFlags == 1);
            
            for k=1:size(Err, 1)
                W = a_WeightsData(oXDataFlags_indice);
                W = reshape(W, size(Err(k,oXDataFlags_indice)));
                Err(k,oXDataFlags_indice) = Err(k,oXDataFlags_indice) .* W;
            end
            
            %% Calcul de l'erreur quadratique moyenne (que dans le cas du Simplex car fait auto par MatLab avec Newton)
            
            if this.AlgoType == 1
                Err = Err(:,oXDataFlags_indice);
                a_WeightsData = a_WeightsData(oXDataFlags_indice);
                subNaN = find(isnan(sum(Err)));
                Err(:,subNaN) = [];
                a_WeightsData(subNaN) = [];
                Err = sqrt(sum(Err .^ 2, 2) / sum(a_WeightsData));
                % Err = sum(abs(Err(:,oXDataFlags_indice)), 2); % Non EQM
            end
            
            %% Suppression des mauvaises valeurs (infines, NaN et comlexes)
            
            Err(~isfinite(Err))	= 0;
            Err(isnan(Err))		= 0;
            Err(imag(Err) ~= 0)	= 0;
            
            %% Prise en compte des d�pendances entre param�tres
            
            ClOptim_Dependences_number = length(CurrentModel.Dependencies);
            if this.UseDependences && (ClOptim_Dependences_number > 0)
                %                 proba = ones(size(Err));
                proba = 1;
                
                % Cr�ation de la matrice (compl�te, ie avec param�tres locked) des param�tres
                for k=1:size(paramsTransmis, 1)
                    params_all(k,ind_paramsOpt) = paramsTransmis(k,:); %#ok
                    params_all(k,ind_paramsFix) = paramsOpt(ind_paramsFix); %#ok
                end
                
                % Recup�ration infos model courant
                m_title = CurrentModel.Name;
                
                % Test sur toutes les d�pendances saisies
                for k=1:ClOptim_Dependences_number
                    Dependences = CurrentModel.Dependencies(k);
                    
                    % R�cup�ration infos dependance courante
                    d_title	= Dependences.Name;
                    
                    if strcmp(m_title, d_title) % Test si dependance courante correspond au modele courant
                        
                        % R�cup�ration des indices des param�tres du couple (dans la liste de parametres du modele)
                        XLabel = Dependences.XLabel;
                        px_num = str2num(XLabel(2)); %#ok
                        YLabel = Dependences.YLabel;
                        py_num = str2num(YLabel(2)); %#ok
                        
                        % R�cup�ration des valeurs des param�tres du vertex
                        px_value = params_all(:, px_num);
                        py_value = params_all(:, py_num);
                        
                        % R�cup�ration des indices des param�tres du vertex (dans la matrice de dependance)
                        x_value = Dependences.x;
                        y_value = Dependences.y;
                        x_dist	= abs(ones(length(px_value),1)*x_value-px_value*ones(1,length(x_value)));
                        y_dist	= abs(ones(length(py_value),1)*y_value-py_value*ones(1,length(y_value)));
                        [x_dist_min, x_indice] = min(x_dist,[],2); %#ok<ASGLU>
                        [y_dist_min, y_indice] = min(y_dist,[],2); %#ok<ASGLU>
                        
                        % R�cup�ration de la probabilit� li�e au couple
                        val	= get_Image(Dependences); % toutes les probabilit�s saisies
                        p	= ones(size(Err));
                        for j=1:size(x_indice,1) % Boucle n�cessaire lorsque cette fonction est appel�e par l'algo de Newton avec Recuitsimul�
                            tmp	 = val(y_indice(j), x_indice(j));    % probabilites liees aux couples courants
                            p(j) = sum(sum(tmp)) / sum(ones(size(tmp)))*p(j);  % calcul de la probabilite moyenne si besoin (cas ou l'on dans plusieurs intervalles)
                        end											% ex. : si X = [0, 0.5, 1] alors 0.25 est aussi proche de 0 et 0.5, d'ou moyennage
                        %                         proba = p .* proba; % calcul de la probabilit� globale
                        proba = p .* proba / max(val(:)); % calcul de la probabilit� globale
                        
                        
                        %                         val	= get_Image(Dependences); % toutes les probabilit�s saisies
                        %                         p = val(y_indice, x_indice);    % probabilites liees aux couples courants
                        %                         proba = proba .* p; % / max(val(:)); % calcul de la probabilit� globale
                    end
                end
                
                % %               figure; hr(1) = subplot(2,1,1); plot(Err); grid on; hr(1) = subplot(2,1,2); plot(proba); grid on; linkaxes(hr);
                %                 subProba0 = (proba == 0);
                %                 Err( subProba0) = Err( subProba0) - log(eps);
                %                 Err(~subProba0) = Err(~subProba0) - log(proba(~subProba0));
                
                %{
% TODO JMA : la prise en compte des d�pendances ne fonctionne pas. Voir comment faire : El�onore

if proba == 0
Err(:) = abs(double(Err)); % - log10(eps);
else
Err = abs(double(Err)) .* (1 + proba);
end
                %}
                
            end
            Err = double(Err);
        end
    end
    
    methods (Access = protected)
        
        function [Err_try, vertex_try] = deform_vertex(this, facteur, paramsMin, paramsMax, vertex, vertex_sum, ind_haut)
            % Description
            %   Vertex deformation (Simplex algorithm)
            %
            % Syntax
            %   [Err_try, vertex_try] = deform_vertex(this, facteur, paramsMin, paramsMax, vertex, vertex_sum, ind_haut)
            %
            % Input Arguments
            %   this        : Instance de la classe ClOptim
            %   facteur     : facteur de d�formation
            %   ParamsMin   : Valeurs minimales des param�tres
            %   ParamsMax   : Valeurs maximales des param�tres
            %   vertex      :
            %   vertex_sum  :
            %   ind_haut    : Indice du point haut du vertex
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc ClOptim/deform_vertex">ClOptim/deform_vertex</a>
            %   <a href="matlab:doc ClOptim">ClOptim</a>
            
            
            NbreOfParams = size(vertex,2);
            
            % ------------------------
            % Calcul du vertex deform�
            
            fact1      = (1 - facteur) / NbreOfParams;
            fact2      = fact1 - facteur;
            vertex_try = vertex_sum * fact1 - vertex(ind_haut,:) * fact2;
            
            % --------------------------------------------------------
            % Verification si les param�tres du vertex deform� restent
            % dans les limites de l'espace born� de recherche
            
            ind_Min = find(vertex_try < paramsMin);
            ind_Max = find(vertex_try > paramsMax);
            
            vertex_try(ind_Min) = paramsMin(ind_Min);
            vertex_try(ind_Max) = paramsMax(ind_Max);
            
            % -----------------------------------
            % Calcul de l'erreur au nouveau point
            
            Err_try = errFunction(vertex_try, this);
        end
    end
end
