classdef ExClOptimBS < ClOptim
    % Description
    %   Create a ExClOptimBS instance (ClOptim ineheritage)
    %   made of a BSLurtonModel, a BSJacksonModel, a BSGuillonModel,
    %   a BSHamiltonModel and a BSJacksonAPLModel
    %
    % Syntax
    %   a = ExClOptimBS(...)
    %
    % ExClOptimBS properties :
    %   See ClOptim
    %
    % Output Arguments
    %   a : One ExClOptimBS (ClOptim ineheritage)
    %
    % Examples
    %   a = ExClOptimBS;
    %   editProperties(a);
    %   plot(a);
    %   a.Algo
    %   a.UseSimAnneal
    %   % a.UseVisu = 1;
    %   a.MaxIter = 100;
    %
    %   a = random(a);
    %   plot(a, 'Name', 'Random');
    %   a = set(a, 'Algo', 1, 'UseSimAnneal', 0);
    %   a = optimize(a);
    %   plot(a);
    %
    %   a = random(a);
    %   plot(a, 'Name', 'Random');
    %   a = set(a, 'Algo', 1, 'UseSimAnneal', 1);
    %   a = optimize(a);
    %   plot(a);
    %
    %   % Change the maximum number of iterations for Newton because
    %   a.MaxIter = 500;
    %
    %   a = random(a);
    %   plot(a, 'Name', 'Random');
    %   a = set(a, 'Algo', 2, 'UseSimAnneal', 0);
    %   a = optimize(a);
    %   plot(a);
    %
    %   a = random(a);
    %   plot(a, 'Name', 'Random');
    %   a = set(a, 'Algo', 2, 'UseSimAnneal', 1);
    %   a = optimize(a);
    %   plot(a);
    %
    %   b = OptimDialog(a, 'Title', 'Optim ExClOptimBS', 'windowStyle', 'normal');
    %   b.openDialog();
    %
    % Authors : JMA
    % See also ClOptim ClModel BSLurtonModel BSJacksonModel BSGuillonModel OptimDialog Authors
    %-------------------------------------------------------------------------------
    
    methods
        function this = ExClOptimBS(varargin)
            
            % Superclass Constructor
            this = this@ClOptim(varargin{:});
            
            [varargin, this.XData]  = getPropertyValue(varargin, 'XData',  -80:80);
            [varargin, this.Name]   = getPropertyValue(varargin, 'Name',   'AngularBackscater');
            [varargin, this.xLabel] = getPropertyValue(varargin, 'xLabel', 'Indidenceangles');
            [varargin, this.yLabel] = getPropertyValue(varargin, 'yLabel', 'BS');
            [varargin, Coeff]       = getPropertyValue(varargin, 'Coeff',  5);
            
            %% Push the remaining parametres to the setter
            
            this = set(this, varargin{:});
            
            %% Create the models
            
            weightsData = rand(size(this.XData));
            modeleBSLurton     = BSLurtonModel(    'XData', this.XData, 'WeightsData', weightsData);
            modeleBSJackson    = BSJacksonModel(   'XData', this.XData, 'WeightsData', weightsData);
            modeleBSGuillon    = BSGuillonModel(   'XData', this.XData, 'WeightsData', weightsData);
            modeleBSHamilton   = BSHamiltonModel(  'XData', this.XData, 'WeightsData', weightsData);
            modeleBSJacksonAPL = BSJacksonAPLModel('XData', this.XData, 'WeightsData', weightsData);
            
            %% Define the experimental data (YData)
            
            modeleBSLurton = noise(modeleBSLurton, 'Coeff', Coeff);
            this.YData = modeleBSLurton.YData;
            
            %% Set the models in the ClOptim instance
            
            this.models = [modeleBSLurton, modeleBSJackson, modeleBSGuillon, modeleBSHamilton modeleBSJacksonAPL];
        end
    end
end
