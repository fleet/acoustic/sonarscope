classdef ClParametre < handle & Cloneable & AbstractDisplay
    % Description
    %   Class that deals with a parameter with its Value, Unit, MinValue, ...
    %
    % Syntax
    %   a = ClParametre(...)
    %
    % ClParametre properties :
    %   Name     - Name of the parameter
    %   Value    - Value of the parameter
    %   Unit     - Unit of the parameter
    %   MinValue - value of the parameter (default : -Inf)
    %   MaxValue - Max value of the parameter (default : Inf)
    %   Format   - Format to be used to display the value : {'%f'} | '%d'
    %   Enable   - Boolean that indicates it the value can be changed by the "random" method
    %   Help     - Path or URL giving explanation for the parameter
    %   Mute     - Boolean that ndicates if a message must be displayed if the value is set out of the min and max values
    %
    % Output Arguments
    %   a : One ClParametre instance
    %
    % Examples
    %   a = ClParametre('Name', 'Temperature', 'Unit', 'deg', 'MinValue', -10, 'MaxValue', 50, 'Value', 20);
    %   a
    %   methods(a)
    %   editProperties(a)
    %
    %   a.Name
    %   a.Value
    %   a.Value = 30
    %   a.Value = 60
    %   a.Mute  = 2
    %   a.Value = 60
    %   a.MaxValue = 20
    %
    %   nomHtml = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope'
    %   a.Help = nomHtml
    %   web(a)
    %
    %   a(1) = ClParametre('Name', 'Temperature', 'Unit', 'deg', 'MinValue', -10, 'MaxValue', 50, 'Value', 20);
    %   a(2) = ClParametre('Name', 'Salinity', 'Unit', '0/00', 'MinValue', 25, 'MaxValue', 35, 'Value', 30);
    %   a
    %   a(2).Value = 31
    %   editProperties(a)
    %
    % Authors : MHO + JMA
    % See also ParametreComponent ParametreDialog ClModel Authors
    % ----------------------------------------------------------------------------
    
    properties (Access = public)
        Name     = '';   % Name of the parameter
        Value            % Value of the parameter
        Unit     = '';   % Unit of the parameter
        MinValue = -Inf; % Min value of the parameter (default : -Inf)
        MaxValue = Inf;  % Max value of the parameter (default : Inf)
        Format   = '%f'; % Format to be used to display the value : {'%f'} | '%d'
        Enable   = true; % Boolean that indicates it the value can be changed by the "random" method
        Help     = '';   % Path or URL giving explanation for the parameter
        Mute     = true; % Boolean that ndicates if a message must be displayed if the value is set out of the min and max values
        valueSignificant
    end
    
    
    methods
        
        function this = ClParametre(varargin)
            % Constructor of the class
            
                this = set(this, varargin{:});
        end
        
        
        function this = set(this, varargin)
            % Setter of the form a = set(a, ...)
            % Examples
            %   a = ClParametre;
            %   set(a, 'Enable', 1, 'Name', 'Temperature', 'MinValue', -10, 'MaxValue', 50, 'Value', 20);
            %   a
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc ClParametre/set">ClParametre/set</a>
            %   <a href="matlab:doc ClParametre">ClParametre</a>
            
            if ~checkIfOnlyOneInstance(length(this))
                return
            end
            
            %% Lecture des arguments
            
            [varargin, this.Name]             = getPropertyValue(varargin, 'Name',             this.Name);
            [varargin, this.Unit]             = getPropertyValue(varargin, 'Unit',             this.Unit);
            [varargin, this.Enable]           = getPropertyValue(varargin, 'Enable',           this.Enable);
            [varargin, this.MinValue]         = getPropertyValue(varargin, 'MinValue',         this.MinValue);
            [varargin, this.MaxValue]         = getPropertyValue(varargin, 'MaxValue',         this.MaxValue);
            [varargin, this.Format]           = getPropertyValue(varargin, 'Format',           this.Format);
            [varargin, this.Help]             = getPropertyValue(varargin, 'Help',             this.Help);
            [varargin, this.Mute]             = getPropertyValue(varargin, 'Mute',             this.Mute);
            [varargin, this.Value]            = getPropertyValue(varargin, 'Value',            this.Value);
            [varargin, this.valueSignificant] = getPropertyValue(varargin, 'valueSignificant', this.valueSignificant);
            
            
            %% Check if all the arguments have been interpretated
            
            if checkIfVararginIsEmpty(varargin)
                return
            end
            
            %% Output processing
            
            if nargout == 0 % TODO MHO : should not be used but kept for analogy to graphic setter
                assignin('caller', inputname(1), this);
            end
        end
        
        
        function set.Name(this, Name)
            % Setter of "Name" property
            
            if ~checkIfOnlyOneInstance(length(this))
                return
            end
            
            if ischar(Name)
                this.Name = Name;
            else
                my_warndlg('Class ClParametre, method set.Name : Invalid argument type (char is expected)', 1);
            end
        end
        
        
        function set.Enable(this, Enable)
            % Setter of "Enable" property
            
            if ~checkIfOnlyOneInstance(length(this))
                return
            end
            
            if isa(Enable, 'boolean') || any(Enable == [0 1])
                this.Enable = (Enable == true);
            else
                my_warndlg('Class ClParametre, method set.Enable : Invalid argument value (true, false, 0 or 1 are expected)', 1);
            end
        end
        
        
        function set.MinValue(this, MinValue)
            % Setter of "MinValue" property
            
            if ~checkIfOnlyOneInstance(length(this))
                return
            end
            
            if isempty(MinValue) || ~isnumeric(MinValue) || isnan(MinValue)
                my_warndlg('Class ClParametre, method set.MinValue : Invalid argument type (numeric value expected)', 1);
                return
            end
            if MinValue > this.Value %#ok<MCSUP>
                % my_warndlg(['ClParametre/set.MinValue : Value set to ', num2str(MinValue)], 0);
                this.Value = MinValue; %#ok<MCSUP>
            end
            this.MinValue = MinValue;
        end
        
        
        function set.MaxValue(this, MaxValue)
            % Setter of "MaxValue" property
            
            if ~checkIfOnlyOneInstance(length(this))
                return
            end
            
            if isempty(MaxValue) || ~isnumeric(MaxValue) || isnan(MaxValue)
                my_warndlg('Class ClParametre, method set.MaxValue : Invalid argument type (numeric value expected)', 1);
                return
            end
            if MaxValue < this.Value %#ok<MCSUP>
                % my_warndlg(['ClParametre/set.MaxValue : Value set to ', num2str(MaxValue)], 0);
                this.Value = MaxValue; %#ok<MCSUP>
            end
            this.MaxValue = MaxValue;
        end
        
        
        function set.Value(this, Value)
            % Setter of "Value" property
            
            %             if ~checkIfOnlyOneInstance(length(this)) % Comment� par JMA le 18/01/2018 car appel� un tr�s grand nombre de fois
            %                 return
            %             end
            
            %             if isempty(Value) || ~isnumeric(Value)
            if  ~isnumeric(Value)
                if ~this.Mute %#ok<MCSUP>
                    my_warndlg('Class ClParametre, method set.Value : Invalid argument type (numeric value expected)', 1);
                    return
                else
                    return
                end
            elseif (isempty(Value))
                % empty value allowed
                this.Value = Value;
                return
            end
            
            
            % round with significant number if exist
            if ~isempty(this.valueSignificant) %#ok<MCSUP>
                Value = round(Value, this.valueSignificant, 'significant'); %#ok<MCSUP>
            end
            
            
            if (Value < this.MinValue) || (Value > this.MaxValue) %#ok<MCSUP>
                if ~this.Mute && (Value < this.MinValue) %#ok<MCSUP>
%                     str1 = sprintf('La valeur ne peut pas �tre inf�rieure � %s.', num2str(this.MinValue)); %#ok<MCSUP>
                    str2 = sprintf('The Value cannot be less than %s.', num2str(this.MinValue)); %#ok<MCSUP>
                    my_warndlg(str2, 0, 'TimeDelay', 10);
                elseif ~this.Mute && (Value > this.MaxValue) %#ok<MCSUP>
%                     str1 = sprintf('La valeur ne peut pas �tre sup�rieure � %s.', num2str(this.MaxValue)); %#ok<MCSUP>
                    str2 = sprintf('The Value cannot be more than %s.', num2str(this.MaxValue)); %#ok<MCSUP>
                    my_warndlg(str2, 0, 'TimeDelay', 10);
                end
                
                % set the value to min or max
                if (Value < this.MinValue) %#ok<MCSUP>
                    this.Value =  this.MinValue; %#ok<MCSUP>
                elseif (Value > this.MaxValue) %#ok<MCSUP>
                    this.Value =  this.MaxValue; %#ok<MCSUP>
                end
            else
                this.Value = Value;
            end
            
            if strcmp(this.Format, '%d') %#ok<MCSUP>
                this.Value = round(this.Value);
            end
        end
        
        
        function this = setValueMinValueMaxValue(this, Value, MinValue, MaxValue)
            % Set the properties "Value", "MinValue" and "MaxValue" in a single shot
            % Examples
            %   a = ClParametre
            %   a = set_allVal(a, 10, 0, 100)
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc ClParametre/setValueMinValueMaxValue">ClParametre/setValueMinValueMaxValue</a>
            %   <a href="matlab:doc ClParametre">ClParametre</a>
            
            if ~checkIfOnlyOneInstance(length(this))
                return
            end
            
            this.MinValue = MinValue;
            this.MaxValue = MaxValue;
            if (Value < this.MinValue) || (Value > this.MaxValue)
                if ~this.Mute && (Value < this.MinValue)
%                     str1 = sprintf('La valeur ne peut pas �tre inf�rieure � %s.', num2str(this.MinValue));
                    str2 = sprintf('The Value cannot be less than %s.',           num2str(this.MinValue));
                    my_warndlg(str2, 0, 'TimeDelay', 10);
                elseif ~this.Mute && (Value > this.MaxValue)
%                     str1 = sprintf('La valeur ne peut pas �tre sup�rieure � %s.', num2str(this.MaxValue));
                    str2 = sprintf('The Value cannot be more than %s.',           num2str(this.MaxValue));
                    my_warndlg(str2, 0, 'TimeDelay', 10);
                end
            else
                this.Value = Value;
            end
        end
        
        
        function flag = eq(a, b)
            % Test of equality of two instances
            % Examples
            %   a = ClParametre('Name', 'Temperature', 'Unit', 'deg', 'MinValue', -10, 'MaxValue', 50, 'Value', 20);
            %   b = a;
            %   flag = (a == b)
            %   b.Value = 0;
            %   flag = (a == b)
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc ClParametre/eq">ClParametre/eq</a>
            %   <a href="matlab:doc ClParametre">ClParametre</a>
            
            if ~checkIfOnlyOneInstance(length(a))
                return
            end
            if ~checkIfOnlyOneInstance(length(b))
                return
            end
            
            flag(1) = (a.Value    == b.Value);
            flag(2) = (a.MinValue == b.MinValue);
            flag(3) = (a.MaxValue == b.MaxValue);
            flag(4) = (a.Enable   == b.Enable);
            flag(5) = strcmp(a.Name,   b.Name);
            flag(6) = strcmp(a.Unit,   b.Unit);
            flag(7) = strcmp(a.Format, b.Format);
            if isempty(a.Help) && isempty(b.Help)
                flag(8) = 1;
            else
                flag(8) = strcmp(a.Help, b.Help);
            end
            flag = all(flag);
        end
        
        
        function flag = ne(a, b)
            % Test of inequality of two instances
            % Examples
            %   a = ClParametre('Name', 'Temperature', 'Unit', 'deg', 'MinValue', -10, 'MaxValue', 50, 'Value', 20);
            %   b = a;
            %   flag = (a ~= b)
            %   b.Value = 0;
            %   flag = (a ~= b)
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc ClParametre/ne">ClParametre/ne</a>
            %   <a href="matlab:doc ClParametre">ClParametre</a>
            
            if ~checkIfOnlyOneInstance(length(this))
                return
            end
            
            flag = ~(a == b);
        end
        
        
        function this = random(this)
            % Randomize "Value" property
            %
            % Syntax
            %   a = random(a)
            %
            % Input Arguments
            %   a : ClParametre instance(s)
            %
            % Output Arguments
            %   a : ClModel instance(s)
            %
            % Examples
            %   a = ClParametre('Name', 'Temperature', 'Unit', 'deg', ...
            %                    'MinValue', -10, 'MaxValue', 50, 'Value', 20);
            %   a.Value
            %   a = random(a);
            %   a.Value
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc ClParametre/random">ClParametre/random</a>
            %   <a href="matlab:doc ClParametre">ClParametre</a>
            
            for k=1:length(this)
                if this(k).Enable
                    value = this(k).MinValue + rand(1,1) * (this(k).MaxValue - this(k).MinValue);
                    if strcmp(this(k).Format, '%d')
                        value = floor(value);
                    end
                    this(k).Value = value;
                end
            end
        end
        
        
        function web(this)
            % Open the web browser on the URL defined in "Help"
            % Examples
            %   a = ClParametre('Enable', 1, 'Name', 'Temperature', 'Unit', 'deg', 'MinValue', -10, 'MaxValue', 50, 'Value', 20)
            %   nomHtml = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope'
            %   a.Help = nomHtml;
            %   web(a)
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc ClParametre/web">ClParametre/web</a>
            %   <a href="matlab:doc ClParametre">ClParametre</a>
            
            for k=1:length(this)
                if ischar(this(k).Help) && ~isempty(this(k).Help)
                    my_web(this(k).Help)
                else
%                     str1 = 'ClParametre/web : Help non defini';
                    str2 = 'ClParametre/web : Help is not defined';
                    my_warndlg(str2, 1);
                end
            end
        end
    end
end
