classdef FilterDataParametre < handle & Cloneable & AbstractDisplay
  % FilterDataParametre Description
    %   Class that describes a filter data parametre
    %
    % FilterDataParametre Syntax
    %   filterParam = FilterDataParametre(...)
    %
    % Properties :
    %   filterNameList            
    %   selectedFilterIndex        
    %   butterworthWcParam      
    %   wcNameList       
    %   selectedWcIndex  
    %   butterworthOrderParam  
    %   gaussianSigmaParam            
    %   gaussianSigmaParam            
    %
    % Examples
    %
    %   %Exemple 1 :
    %   FilterDataParametre.FilterNameList
    %   FilterDataParametre.WcNameList
    %   filterDataParam = FilterDataParametre('FilterType', FilterDataParametre.FilterNameList(1), 'ButterworthWc', FilterDataParametre.WcNameList(3), 'ButterworthOrder', 2)
    %   filterDataParam.butterworthWcParam.Value
    %
    %   %Exemple 2 :
    %   filterDataParam = FilterDataParametre('FilterType', "Gaussian", 'GaussianSigma', 2)
    % ----------------------------------------------------------------------------
    
    properties (Constant)
        FilterNameList = ["Butterworth", "Gaussian", "Polynome"];
        WcNameList = ["No Filter", "1/2", "1/5", "1/10", "1/20", "1/50", "1/100", "1/200", "1/500", "1/1000"];
    end
    
    properties
        selectedFilterIndex = 1;
        
        % Butterworth parameters
        butterworthWcParam     = ClParametre.empty % ClParametre list :  wc
        butterworthWcIndex     = 1;
        butterworthOrderParam  = ClParametre.empty % ClParametre list : ordre
        butterworthProgressive = false;

        % Gaussian
        gaussianSigmaParam = ClParametre.empty

        % Polynome
        polynomeOrderParam = ClParametre.empty
    end
    

    methods
        function this = FilterDataParametre(varargin)
            [varargin, filterType]             = getPropertyValue(varargin, 'FilterType',             FilterDataParametre.FilterNameList(1));
            [varargin, butterworthWc]          = getPropertyValue(varargin, 'ButterworthWc',          FilterDataParametre.WcNameList(1));
            [varargin, butterworthOrder]       = getPropertyValue(varargin, 'ButterworthOrder',       2);
            [varargin, butterworthProgressive] = getPropertyValue(varargin, 'ButterworthProgressive', false);
            [varargin, gaussianSigma]          = getPropertyValue(varargin, 'GaussianSigma',          2);
            [varargin, polynomeOrder]          = getPropertyValue(varargin, 'PolynomeOrder',          1);  %#ok<ASGLU>
            
            this.selectedFilterIndex = find(contains(FilterDataParametre.FilterNameList, filterType, 'IgnoreCase', true));
            
            % Create ClParametre for butterworth
            this.butterworthWcParam = ClParametre('Name', 'Normalized cutoff frequency', ...
                'MinValue', 0, 'MaxValue', 1, 'Value', str2num(butterworthWc)); %#ok<ST2NM>
            this.butterworthOrderParam = ClParametre('Name', 'Order', ...
                'MinValue', 1, 'MaxValue', 1000, 'Value', butterworthOrder, 'Format', '%d');
            
            % Create ClParametre for gaussian
            this.gaussianSigmaParam = ClParametre('Name', 'Sigma', 'Unit', ' ', ...
                'MinValue', 0, 'MaxValue', 1000, 'Value', gaussianSigma, 'Format', '%d');
            
            % Create ClParametre for polynome order
            this.polynomeOrderParam = ClParametre('Name', 'Order', 'Unit', ' ', ...
                'MinValue', 1, 'MaxValue', 6, 'Value', polynomeOrder, 'Format', '%d');
        end
    end
end
