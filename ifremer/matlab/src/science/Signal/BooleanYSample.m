classdef BooleanYSample < YSample
    % BooleanYSample Description
    %   Class that describes a sample on Y axe with boolean options
    %
    % Properties :
    %
    % Examples
    %   booleanYSample1 = BooleanYSample('data', [1 0 1 1 0 1])
    %   booleanYSample2 = BooleanYSample('data', [1 0 1 1 0 1], 'name', 'Test BooleanYSample')
    % ----------------------------------------------------------------------------
    
    properties (Constant)
        strBoolList = {'Off'; 'On'};
    end
    
    methods
        function this = BooleanYSample(varargin)
            % Superclass Constructor
            this = this@YSample(varargin{:});
            
            this = set(this, varargin{:});
        end
        
        function this = set(this, varargin)
            this = set@YSample(this, varargin{:});
        end
        
        function str = summary(this, varargin)
            str = sprintf('%s : %s : Indexed by %s "%s"', ...
                this.summaryWithoutUnit(varargin), ...
                BooleenDansListe2str(mode(this.data), this.strBoolList, 'Num'));
        end
    end
end
