classdef ClHistory < handle & Cloneable & AbstractDisplay
    % History of the commands applied to an instance of a class
    %   It should be instanciated in the constructor of a class
    %
    % Syntax
    %   history = ClHistory(...)
    %
    % Properties :
    %   nbDisplay       - Number of commands that should be displayed
    %   historyStruct   - Structur of history,
    %   index           - Index of the history array. Increases by 1 every
    %   time the history of the calling class is updated.
    
    
    properties
        nbDisplay     = 5;
        index         = 0;
        historyStruct = struct('Process', {}, 'Parameters', {});
    end
    
    methods
        function this = ClHistory(varargin)
            % Constructor of the class
            
            [varargin, this.nbDisplay]     = getPropertyValue(varargin, 'nbDisplay',     this.nbDisplay);
            [varargin, this.index]         = getPropertyValue(varargin, 'index',         this.index);
            [varargin, this.historyStruct] = getPropertyValue(varargin, 'historyStruct', this.historyStruct); %#ok<ASGLU>
            
            this.updateHistory('Creation');
        end
        
        function updateHistory(this, process, varargin)
            %Updates the history of the calling class
            
            this.index = this.index +1;
            
            this.historyStruct(this.index).Process = process;
            par = 1;
            while ~isempty(varargin)
                this.historyStruct(this.index).Parameters(par).Name  = varargin{1};
                [varargin, this.historyStruct(this.index).Parameters(par).Value] = getPropertyValue(varargin, varargin{1}, '');
                par = par + 1;
            end
        end
    end
end
