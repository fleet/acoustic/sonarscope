classdef ClNavigation < SignalContainer
    % ClNavigation Description
    %   Class that describes Navigation Data
    %
    % Examples
    %   latSignal = ClSignal('ySample', YSample('data', sin(0:0.1*pi:10*pi-0.1*pi)));
    %   lonSignal = ClSignal('ySample', YSample('unit', 'Deg', 'data', sin(0:0.2*pi:20*pi-0.1*pi)));
    %   nav  = ClNavigation(latSignal, lonSignal);
    %   nav.plot;
    %   nav.plotNav;
    %
    %
    % % Create navigation in 1 navigation and n samples
    %    fileName{1} = 'DataForDemo/NavAUV/usblRepeater09Modif.txt';
    %    fileName{2} = 'DataForDemo/NavAUV/usblRepeater10Modif.txt';
    %    fileName{3} = 'DataForDemo/NavAUV/usblRepeater11Modif.txt';
    %    fileName{4} = 'DataForDemo/NavAUV/usblRepeater12Modif.txt';
    %    FileNameOut = FtpUtils.importDataForDemoFromSScFTP(fileName);
    %    for k=1:length(FileNameOut)
    %         [flag, Latitude, Longitude, SonarTime, Heading, SonarSpeed, Immersion] = lecFicNav(FileNameOut{k});
    %         T = datetime(SonarTime.timeMat, 'ConvertFrom', 'datenum');
    %         timeSample(k) = XSample('name', 'time', 'data', T);
    %         latSample(k) = YSample('data', Latitude);
    %         lonSample(k) = YSample('data', Longitude);
    %         immersionSample(k) = YSample('name', 'Immersion', 'unit', 'm', 'data', Immersion);
    %     end
    %     latSignal =  ClSignal('ySample', latSample, 'xSample', timeSample);
    %     lonSignal =  ClSignal('ySample', lonSample, 'xSample', timeSample);
    %     immersionSignal =  ClSignal('ySample', immersionSample, 'xSample', timeSample);
    %     nav  = ClNavigation(latSignal, lonSignal, 'immersionSignal', immersionSignal, 'name', 'navigation');
    %     nav.plot;
    %     nav.plotNav;
    %
    %
    %  % Create n navigation and 1 sample in each
    %     fileName{1} = 'DataForDemo/NavAUV/usblRepeater09Modif.txt';
    %     fileName{2} = 'DataForDemo/NavAUV/usblRepeater10Modif.txt';
    %     fileName{3} = 'DataForDemo/NavAUV/usblRepeater11Modif.txt';
    %     fileName{4} = 'DataForDemo/NavAUV/usblRepeater12Modif.txt';
    %     FileNameOut = FtpUtils.importDataForDemoFromSScFTP(fileName);
    %     for k=1:length(FileNameOut)
    %          [flag, Latitude, Longitude, SonarTime, Heading, SonarSpeed, Immersion] = lecFicNav(FileNameOut{k});
    %          T = datetime(SonarTime.timeMat, 'ConvertFrom', 'datenum');
    %          timeSample = XSample('name', 'time', 'data', T);
    %          latSample = YSample('data', Latitude);
    %          latSignal =  ClSignal('ySample', latSample, 'xSample', timeSample);
    %          lonSample = YSample('data', Longitude);
    %          lonSignal =  ClSignal('ySample', lonSample, 'xSample', timeSample);
    %          immersionSample = YSample('name', 'Immersion', 'unit', 'm', 'data', Immersion);
    %          immersionSignal =  ClSignal('ySample', immersionSample, 'xSample', timeSample);
    %          nav(k)  = ClNavigation(latSignal, lonSignal, 'immersionSignal', immersionSignal, 'name', 'navigation');
    %      end
    %      nav.plot;
    %      nav.plotNav;
    % ----------------------------------------------------------------------------
    
    properties
        latitudeSignalIndex      = 1;
        longitudeSignalIndex     = 2;
        immersionSignalIndex     = 0;
        headingVesselSignalIndex = 0;
    end
    
    methods
        function this = ClNavigation(latitudeSignal, longitudeSignal, varargin)
            % Superclass Constructor
            this = this@SignalContainer(varargin{:});
            
            [varargin, immersionSignal]     = getPropertyValue(varargin, 'immersionSignal',     ClSignal.empty);
            [varargin, headingVesselSignal] = getPropertyValue(varargin, 'headingVesselSignal', ClSignal.empty);
            [varargin, otherSignal]         = getPropertyValue(varargin, 'otherSignal',         ClSignal.empty); %#ok<ASGLU>
            
            navSignal(this.latitudeSignalIndex) = latitudeSignal;
            % Set default latitude infos if needed
            if isempty(navSignal(this.latitudeSignalIndex).name)
                navSignal(this.latitudeSignalIndex).name = 'Latitude';
            end
            % Set DataType Angle
            for k=1:numel(latitudeSignal.ySample)
                latitudeSignal.ySample(k).dataType = SampleType.ANGLE;
            end
            
            navSignal(this.longitudeSignalIndex) = longitudeSignal;
            % Set default longitude infos if needed
            if isempty(navSignal(this.longitudeSignalIndex).name)
                navSignal(this.longitudeSignalIndex).name = 'Longitude';
            end
            % Set DataType Angle
            for k=1:numel(longitudeSignal.ySample)
                longitudeSignal.ySample(k).dataType = SampleType.ANGLE;
            end
            
            % Check if immersion Sample
            if ~isempty(immersionSignal)
                navSignal(end+1) = immersionSignal;
                this.immersionSignalIndex = numel(navSignal); % update index immerserion Sample
                % Set default immersion infos if needed
                if isempty(navSignal(this.immersionSignalIndex).name)
                    navSignal(this.immersionSignalIndex).name = 'Immersion';
                end
            end
            % Check if heading Sample
            if ~isempty(headingVesselSignal)
                % Set DataType Angle
                for k=1:numel(headingVesselSignal.ySample)
                    headingVesselSignal.ySample(k).dataType = SampleType.ANGLE;
                end
                navSignal(end+1) = headingVesselSignal;
                this.headingVesselSignalIndex = numel(navSignal); % update index headingVessel Sample
                % Set default heading infos if needed
                if isempty(navSignal(this.headingVesselSignalIndex).name)
                    navSignal(this.headingVesselSignalIndex).name = 'Heading';
                end
            end
            
            this.signalList = [navSignal otherSignal];
            
            %             this = set(this, 'signalList' , signalList);
            
            if isempty(this.name)
                this.name = 'Navigation';
            end
        end
        
        %% Setters
        function this = set(this,varargin)
            set@SignalContainer(this,varargin{:});
        end
        
        %% Getters
        function TimeSignal = getTimeSignalSample(this)
            TimeSignal = this.signalList(this.latitudeSignalIndex).xSample;
        end
        
        function latitudeSignalSample = getLatitudeSignalSample(this)
            latitudeSignalSample = this.signalList(this.latitudeSignalIndex).ySample;
        end
        
        function longitudeSignalSample = getLongitudeSignalSample(this)
            longitudeSignalSample = this.signalList(this.longitudeSignalIndex).ySample;
        end
        
        function immersionSignalSample = getImmersionSignalSample(this)
            if this.immersionSignalIndex == 0
                immersionSignalSample = [];
            else
                immersionSignalSample = this.signalList(this.immersionSignalIndex).ySample;
            end
        end
        
        function headingVesselSignalSample = getHeadingVesselSignalSample(this)
            if this.headingVesselSignalIndex ~= 0
                headingVesselSignalSample = this.signalList(this.headingVesselSignalIndex).ySample;
            else
                headingVesselSignalSample = [];
            end
        end
        
        function otherSignalSample = getOtherSignalSample(this, index) % Ajout JMA le 08/02/2021 : TODO MHO : faire cela proprement
        	otherSignalSample = this.signalList(index).ySample;
        end
        
        %% Plot Nav
        function [fig, lineArray] = plotNav(this, varargin)
            [varargin, fig]     = getPropertyValue(varargin, 'Fig', []);
            [varargin, navAxes] = getPropertyValue(varargin, 'navAxes', matlab.graphics.axis.Axes.empty);
            [varargin, is3D]    = getPropertyValue(varargin, '3d', false); %#ok<ASGLU>
            
            if isempty(fig) && isempty(navAxes)
                % Create figure & axeArray
                if ~isempty(this(1).name)
                    fig = FigUtils.createSScFigure('Name', this(1).name);
                else
                    fig = FigUtils.createSScFigure('Name', this.signalList(1).name);
                end
                
            end
            
            if  isempty(navAxes)
                navAxes = findobj(fig, 'Type', 'Axes');
                if isempty(navAxes)
                    % Create axes
                    navAxes = axes(fig);
                end
            end
            
            %init lineArray: nbRow = nbSignalContainers, nbColumn = nbsamples
            lineArray = matlab.graphics.chart.primitive.Line.empty(0 ,0);
            for k=1:numel(this)
                if is3D
                    lineIstanceArray = this(k).oneInstancePlotNav3D(navAxes);
                else
                    lineIstanceArray = this(k).oneInstancePlotNav(navAxes);
                end
                lineArray(k,1:numel(lineIstanceArray)) = lineIstanceArray;
            end
            % axe configuration
            hold('off');
            grid('on');
            axis('tight')
            hold('on')
        end
    end
    
    methods (Access = protected)
        
        function lineArray = oneInstancePlotNav(this, navAxes)
            
            lineArray = matlab.graphics.chart.primitive.Line.empty(numel(this.signalList(this.longitudeSignalIndex).ySample),0);
            
            for k=1:numel(this.signalList(this.longitudeSignalIndex).ySample)
                xData = this.signalList(this.longitudeSignalIndex).ySample(k).data;
                yData = this.signalList(this.latitudeSignalIndex).ySample(k).data;
                userData = this.signalList(this.latitudeSignalIndex).xSample(k).data;
                lineArray(k,1) = PlotUtils.createSScPlot(navAxes, xData, yData, 'userData', userData, 'DisplayName', 'Navigation', 'isLightContextMenu', true);
                % set style to line
                this.signalList(this.latitudeSignalIndex).ySample(k).setStyleToLine(lineArray(k));
                % xlabel
                xlabel([this.signalList(this.longitudeSignalIndex).name ' (' this.signalList(this.longitudeSignalIndex).ySample(k).unit ')']);
                ylabel([this.signalList(this.latitudeSignalIndex).name ' (' this.signalList(this.latitudeSignalIndex).ySample(k).unit ')']);
                % title
                title('Navigation', 'Interp', 'none');
                hold('on');
            end
        end
        
        function lineArray = oneInstancePlotNav3D(this, navAxes)
            
            lineArray = matlab.graphics.chart.primitive.Line.empty(numel(this.signalList(this.longitudeSignalIndex).ySample),0);
            
            for k=1:numel(this.signalList(this.longitudeSignalIndex).ySample)
                xData = this.signalList(this.longitudeSignalIndex).ySample(k).data;
                yData = this.signalList(this.latitudeSignalIndex).ySample(k).data;
                zData = this.signalList(this.immersionSignalIndex).ySample(k).data;
                userData = this.signalList(this.latitudeSignalIndex).xSample(k).data;
                lineArray(k,1) = PlotUtils.createSScPlot3(navAxes, yData, xData, zData, 'userData', userData, 'DisplayName', 'Navigation3D', 'isLightContextMenu', true);
                % Add DragnDrop on figure
                FigUtils.addDragDrop(ancestor(lineArray(k),'figure'));
                % add ContextMenu to line
                PlotUtils.addContextMenu(lineArray(k));
                
                % set style to line
                this.signalList(this.latitudeSignalIndex).ySample(k).setStyleToLine(lineArray(k));
                % xlabel
                xlabel([this.signalList(this.longitudeSignalIndex).name ' (' this.signalList(this.longitudeSignalIndex).ySample(k).unit ')']);
                ylabel([this.signalList(this.latitudeSignalIndex).name ' (' this.signalList(this.latitudeSignalIndex).ySample(k).unit ')']);
                zlabel([this.signalList(this.immersionSignalIndex).name ' (' this.signalList(this.immersionSignalIndex).ySample(k).unit ')']);
                % title
                title('Navigation3D', 'Interp', 'none');
                hold('on');
            end
        end
    end
end
