classdef (Abstract) ClSample < handle & AbstractOperator & AbstractDisplay
    % Description
    %   Abstract Class that is inheritated in the XSample and YSample classes
    %
    % ClSample properties :
    %   data     - Data of sample
    %   name     - Name of sample
    %   unit     - Unit of sample
    %   dataType - Type of data described on the abscissa axis
    %
    % Output Arguments
    %   a : One ClSample instance
    %
    % Examples
    %   a = YSample('data', linspace(0,1,100).', 'dataType', SampleType.NORMAL);
    %
    % Authors : MHO
    % See also XSample YSample ClSignal Authors
    % ----------------------------------------------------------------------------
    
    properties
        data     % Data of sample
        name     % Name of sample
        unit     % Unit of sample
        dataType % Type of data containing by SampleType TODO MHO : expliciter les valeurs que �a peut prendre : NORMAL, INDEX, etc de SampleType
        comment  % Comment to be associated to the data
    end
    
    methods
        function this = ClSample(varargin)
            % Constructor of the class
            
            this = set(this, varargin{:});
        end
        
        function this = set(this,varargin)
            [varargin, this.data]     = getPropertyValue(varargin, 'data',     this.data);
            [varargin, this.name]     = getPropertyValue(varargin, 'name',     this.name);
            [varargin, this.unit]     = getPropertyValue(varargin, 'unit',     this.unit);
            [varargin, this.dataType] = getPropertyValue(varargin, 'dataType', this.dataType);
            [varargin, this.comment]  = getPropertyValue(varargin, 'comment',  this.comment); %#ok<ASGLU>
        end
        
        function set.data(this, data)
            if isempty(data)
                this.data = data;
            else
                if isvector(data)
                    this.data = data;
                else
%                     str1 = 'Un object sample ne peut contenir qu''une donn�e de type vecteur ([1 n] ou [n 1]).'; %#ok<NASGU>
                    str2 = 'A sample can only have a data vector [1 n] or [n 1].';
                    my_warndlg(str2, 1);
                    return
                end
            end
        end
        
        function set.dataType(this, dataType)
            if ~isempty(dataType) &&  any(ismember(SampleType.LIST, dataType))
                this.dataType = dataType;
            else
                this.dataType = SampleType.NORMAL;
            end
        end
           
        function val = getDataUnit(this, unitOut)
            %   a = YSample('data', [1 100 1000], 'unit', 'm')
            %   a.getDataUnit('km')
            
            if isempty(unitOut)
                val = this.data;
                return
            end
            
            unitOutSetToMeters = unitOut;
            unitInSetToMeters  = this.unit;
            
            % TODO : unitsratio ne connait que les longueurs. On triche
            % pour pouvoir traiter les Hertz et secondes.
            unitOutSetToMeters = strrep(unitOutSetToMeters, 'Hz', 'm');
            unitInSetToMeters  = strrep(unitInSetToMeters,  'Hz', 'm');
            unitOutSetToMeters = strrep(unitOutSetToMeters, 's', 'm');
            unitInSetToMeters  = strrep(unitInSetToMeters,  's', 'm');
            
            coef = unitsratio(unitOutSetToMeters, unitInSetToMeters);
            val = this.data * coef;
        end
        
        % Summary without unit(name, subx, suby)
        function str = summaryWithoutUnit(this, varargin)
            [varargin, subx] = getPropertyValue(varargin, 'subx', 1:size(this.data,2));
            [varargin, suby] = getPropertyValue(varargin, 'suby', 1:size(this.data,1)); %#ok<ASGLU>
            str = sprintf('%s : %s', ...
                this.name, num2strCode(this.data(suby,subx)));
        end
        
        % Summary (name, subx, suby, unit)
        function str = summary(this, varargin)
            str = sprintf('%s (%s)', this.summaryWithoutUnit(varargin), this.unit);
        end
 
    end
    
    methods (Access = protected)
        
        function value = getOperationValue(this)
            % implement AbstractOperator
            for k=numel(this):-1:1
                value(k,:) = this(k).data;
            end
        end
        
        function setOperationValue(this, newValue)
            % implement AbstractOperator
            for k=numel(this):-1:1
                this(k).data = newValue(k, :);
            end
        end
    end
end
