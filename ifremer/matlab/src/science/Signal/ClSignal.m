classdef ClSignal < handle & AbstractDisplay & AbstractOperator & matlab.mixin.Heterogeneous
    % ClSignal Description
    %   Class that describes different types of signals (Reflectivity,
    %   Bathymetry, Latitude, Longitude, other...).
    %
    % ClSignal Syntax
    %   s = ClSignal(...)
    %
    % Properties :
    %   name            - Name of the signal
    %   xSample         - Sample object(s) for x data
    %   ySample         - Sample object(s) for y data
    %   comment         - Comments on the signal
    %   originFilename  - The name of the file from which the signal was extracted
    %   samplesDimNum   - Whether the number of samples is in the 1st or 2nd column of the result of the function 'size'
    %   flag            - Indicates whether the signal was automatically transposed
    %
    % Examples
    %
    %   % Exemple 1 :
    %   ySample = YSample('data', sin(0:0.1*pi:10*pi-0.1*pi));
    %   signal  = ClSignal('name', 'Simple sinus function','ySample',ySample);
    %   methods(signal)
    %   editProperties(signal)
    %   plot(signal)
    %
    %   % Exemple 2 :
    %   ySample    = YSample('data', sin(0:0.1*pi:10*pi-0.1*pi), 'color', 'k');
    %   ySample(2) = YSample('data', sin(0:0.1*pi:10*pi-0.1*pi), 'color', 'b');
    %   ySample(3) = YSample('data', sin(0:0.1*pi:10*pi-0.1*pi), 'color', 'r');
    %   ySample(4) = YSample('data', cos(0:0.1*pi:10*pi-0.1*pi), 'color', 'k');
    %   ySample(5) = YSample('data', cos(0:0.1*pi:10*pi-0.1*pi), 'color', 'b');
    %   ySample(6) = YSample('data', cos(0:0.1*pi:10*pi-0.1*pi), 'color', 'r');
    %   ySample(7) = YSample('data', sin(0:0.1*pi:10*pi-0.1*pi), 'color', 'k');
    %   ySample(8) = YSample('data', sin(0:0.2*pi:20*pi-0.2*pi), 'color', 'b');
    %   ySample(9) = YSample('name', 'test', 'data', sin(0:0.2*pi:20*pi-0.2*pi), 'color', 'r');
    %   signal  = ClSignal('name', 'Simple sinus function','ySample',ySample);
    %   plot(signal)
    %
    %   % Exemple 3 : Plot several signals in different axes :
    %   ySample1    = YSample('data', sin(0:0.1*pi:10*pi-0.1*pi));
    %   ySample1(2) = YSample('data', cos(0:0.1*pi:10*pi-0.1*pi));
    %   signalTest(1)  = ClSignal('name', 'signal1','ySample',ySample1);
    %
    %   ySample2    = YSample('data', sin(0:0.1*pi:10*pi-0.1*pi)+0.1);
    %   ySample2(2) = YSample('data', cos(0:0.1*pi:10*pi-0.1*pi)+0.1);
    %   ySample2(3) = YSample('data', sin(0:0.1*pi:10*pi-0.1*pi)+0.2, 'name', 'ysample2(3)');
    %   signalTest(2) = ClSignal('name', 'signal2', 'ySample', ySample2);
    %
    %   signalTest.plot
    %   signalTest.plot('subAxes', [1 2 0; 1 2 3]);
    %
    %   % Exemple 4 (vertical signal):
    %   ySample = YSample('data', sin(0:0.1*pi:10*pi-0.1*pi));
    %   signal  = ClSignal('name', 'Simple sinus function','ySample',ySample, 'isVertical', true);
    %   signalTest.plot
    %
    %   % Exemple 5 (image in background using CLImageData):
    %   nomFic = 'Z:\private\ifremer\JMA\DemoImageEnBackground\DF1000_ExStr-grRawImage_PingRange_Reflectivity.xml';
    %   [flag, a] = cl_image.import_xml(nomFic);
    %   H = get(a, 'SonarHeight');I = get(a, 'Image');x = get(a, 'x');y = get(a, 'y');
    %
    %   xSample = XSample('data', H);
    %   ySample = YSample('data', 1:length(H), 'name', 'test', 'color', 'r');
    %   signal  = ClSignal('name', 'Test', 'xSample', xSample, 'ySample', ySample);
    %
    % TODO MHO : comme faire le plot ?
    %   imageData = ClImageData('cData', I, 'xData', x, 'yData', y, 'colormapData', gray);
    %   signal.plot('imageData', imageData);
    %
    %   % Exemple 6 (image in background using an other figure with imagesc):
    %   nomFic = 'Z:\private\ifremer\JMA\DemoImageEnBackground\DF1000_ExStr-grRawImage_PingRange_Reflectivity.xml';
    %   [flag, a] = cl_image.import_xml(nomFic);
    %   H = get(a, 'SonarHeight');I = get(a, 'Image');x = get(a, 'x');y = get(a, 'y');
    %
    %   xSample = XSample('data', H);
    %   ySample = YSample('data', 1:length(H), 'name', 'test', 'color', 'r');
    %   signal  = ClSignal('name', 'Test', 'xSample', xSample, 'ySample', ySample);
    %
    %   FigUtils.createSScFigure;
    %   im = imagesc(x,y,I); colormap(im.Parent, gray);
    %   signal.plot('axesArray', im.Parent);
    % ----------------------------------------------------------------------------
        
    properties (Access = public)
        name           = '';              % Name of the signal
        tag                               % Tag to be associated to the data
        xSample        = XSample.empty;
        ySample        = YSample.empty;
        comment        = [];              % Comments of the signal
        originFilename = [];              % Name of the file from which the signal was extracted
        history        = ClHistory.empty;
        isVertical     = false;           % true if signal must be vertical plot
        linkType       = 1;               % 1="None" 2="Signal", 3="Layer"
        link           = 'None';          % name of layer of signal linked
    end
    
    methods
        function this = ClSignal(varargin)
            this = set(this, varargin{:});
        end
        
        function this = set(this,varargin)
            [varargin, this.name]           = getPropertyValue(varargin, 'name',           this.name);
            [varargin, this.xSample]        = getPropertyValue(varargin, 'xSample',        this.xSample);
            [varargin, this.ySample]        = getPropertyValue(varargin, 'ySample',        this.ySample);
            [varargin, this.comment]        = getPropertyValue(varargin, 'comment',        this.comment);
            [varargin, this.originFilename] = getPropertyValue(varargin, 'originFilename', this.originFilename);
            [varargin, this.history]        = getPropertyValue(varargin, 'history',        ClHistory);
            [varargin, this.tag]            = getPropertyValue(varargin, 'tag',            this.tag);
            [varargin, this.isVertical]     = getPropertyValue(varargin, 'isVertical',     this.isVertical);
            [varargin, this.linkType]       = getPropertyValue(varargin, 'linkType',       this.linkType);
            [varargin, this.link]           = getPropertyValue(varargin, 'link',           this.link); %#ok<ASGLU>
            
            % set default xSample if ySample and no xSample
            this.setDefaultXSampleIfNeeded();
        end
        
        
        function setDefaultXSampleIfNeeded(this)
            % Set default xData if needed
            
            if ~isempty(this.ySample)
                if isempty(this.xSample)
                    
                    % Check if all ySample have same data length
                    isSameYSampleDataLength = 1;
                    ySampledataLength = length(this.ySample(1).data);
                    for k=1:numel(this.ySample)
                        if length(this.ySample(k).data) ~= ySampledataLength
                            isSameYSampleDataLength = 0;
                        end
                        ySampledataLength = length(this.ySample(k).data);
                    end
                    
                    if isSameYSampleDataLength
                        % if isSameYSampleDataLength ==> create one xSample
                        xData = (1:length(this.ySample(1).data)).';
                        xsample = XSample('data', xData);
                        for k=1:numel(this.ySample)
                            this.xSample(k) = xsample;
                        end
                    else  % if not ==> create k xSample
                        for k=1:numel(this.ySample)
                            xData = (1:length(this.ySample(k).data)).';
                            xsample = XSample('data', xData);
                            this.xSample(k) = xsample;
                        end
                    end
                elseif numel(this.xSample) == 1 && numel(this.ySample) > 1
                    for k=2:numel(this.ySample)
                        this.xSample(k) = this.xSample(1);
                    end
                end
            end
        end
        
        
        %% PLOT FUNCTIONS
        function [fig, axesArray, lineArray] = plot(this, varargin)
            %plot ClSignal
            %
            % Example :
            % ySample(1) = YSample('data', sin(0:0.1*pi:10*pi-0.1*pi));
            % ySample(2) = YSample('data', cos(0:0.1*pi:10*pi-0.1*pi));
            % signal  = ClSignal('name', 'Simple sinus function','ySample',ySample);
            % signal.plot;
            %
            % Plot in different axes :
            % ySample(1) = YSample('data', sin(0:0.1*pi:10*pi-0.1*pi));
            % ySample(2) = YSample('data', cos(0:0.1*pi:10*pi-0.1*pi));
            % signal  = ClSignal('name', 'Simple sinus function','ySample',ySample);
            % signal.plot('subAxes', [1 2]);
            %
            % Plot sever signals in different axes :
            % ySample1(1) = YSample('data', sin(0:0.1*pi:10*pi-0.1*pi));
            % ySample1(2) = YSample('data', cos(0:0.1*pi:10*pi-0.1*pi));
            % signalTest(1)  = ClSignal('name', 'signal1','ySample',ySample1);
            %
            % ySample2(1) = YSample('data', sin(0:0.1*pi:10*pi-0.1*pi));
            % ySample2(2) = YSample('data', cos(0:0.1*pi:10*pi-0.1*pi));
            % ySample2(3) = YSample('data', cos(0:0.1*pi:10*pi-0.1*pi)+0.1);
            % signalTest(2)  = ClSignal('name', 'signal2','ySample',ySample2);
            %
            % signalTest.plot
            % signalTest.plot('subAxes', [1 2 0; 1 2 3]);
            %
            % With image :
            % nomFic = 'Z:\private\ifremer\JMA\DemoImageEnBackground\DF1000_ExStr-grRawImage_PingRange_Reflectivity.xml';
            % [flag, a] = cl_image.import_xml(nomFic);
            % H = get(a, 'SonarHeight');
            % I = get(a, 'Image');
            % x = get(a, 'x');
            % y = get(a, 'y');
            %
            % xSample = XSample('data', H);
            % ySample = YSample('data', 1:length(H), 'color', 'r');
            % signal  = ClSignal('name', 'Test','xSample',xSample,'ySample',ySample);
            %
            % im = imagesc(x,y,I);
            % signal.plot('axesArray', im.Parent);
            
            [varargin, fig]             = getPropertyValue(varargin, 'Fig',             []);
            [varargin, axesArray]       = getPropertyValue(varargin, 'axesArray',       matlab.graphics.axis.Axes.empty);
            [varargin, subAxes]         = getPropertyValue(varargin, 'subAxes',         []);
            [varargin, isVerticalArray] = getPropertyValue(varargin, 'isVerticalArray', []); %#ok<ASGLU>
            [varargin, imageDataList]   = getPropertyValue(varargin, 'imageDataList',   []); %#ok<ASGLU>
            
            % Check if several Signals
            nbSignals = numel(this);
            
            % Init var
            if isempty(subAxes)
                for k=1:nbSignals
                    for k2=1:numel(this(k).ySample)
                        subAxes(k,k2) = k;
                    end
                end
            end
            
            % Create figure or check if signal is plottable
            if isempty(fig) && isempty(axesArray)
                % Create figure & axesArray
                fig = FigUtils.createSScFigure('Name', this(1).name);
            elseif ~isempty(fig) && isempty(axesArray)
                % Check signal is plottable in the figure
                if ~this.isPlottable(fig, subAxes)
                    return
                end
            end
            
            % Check if axes creation is needed
            if  isempty(axesArray)
                axesArray = findobj(fig, 'Type', 'Axes');
                if isempty(axesArray)
                    % Create axesArray
                    nbAxes = max(subAxes(:));
                    for k=1:nbAxes
                        try
                            isVert = ~this(k).isVertical;
                        catch
                            isVert = 1;
                        end
                        if isVert % horizontal : n lines, 1 column
                            axesArray(k) = subplot(nbAxes, 1, k);
                        else % vertical : 1 line, n columns
                            axesArray(k) = subplot(1, nbAxes, k);
                            % if vertical, rotate each axes
                            camroll(axesArray(k), -90);
                        end
                    end
                end
            end
            
            % Config axesArray
            for k=1:numel(axesArray)
                % hold on
                hold(axesArray(k), 'on');
            end
            
            % Display imageData background
            if ~isempty(imageDataList)
                if length(imageDataList) == 1 % if only juste 1 imageData, plot on first axe
                     imageDataList.imagesc(axesArray(1));
                else  % else, plot on each axe
                    for k=1:numel(axesArray)
                        imageDataList(k).imagesc(axesArray(k));
                    end
                end
            end
            
            %init lineArray: nbRow = nbSignals/axes, nbColumn = nbsamples
            lineArray = matlab.graphics.chart.primitive.Line.empty(nbSignals ,0);
            % Plot for each signal
            for k=1:nbSignals
%                 [pppp, lineInstanceArray] = this(k).oneInstancePlot(axesArray, subAxes(k,:));
%                 axesArray = pppp;
% Modif JMA le 16/03/2023 : pourquoi �tre pass� par pppp ?
                [axesArray, lineInstanceArray] = this(k).oneInstancePlot(axesArray, subAxes(k,:));
                lineArray(k,1:numel(lineInstanceArray)) = lineInstanceArray;
            end
            
            if ~isempty(axesArray)
                % Link axes & hold off
                axesToLink = matlab.graphics.axis.Axes.empty;
                for k1=1:nbSignals
                    nbYSample  = numel(this(k).ySample);
                    for k2=1:nbYSample
                        if subAxes(k1,k2)~=0
                            axesToHoldAndLink = axesArray(subAxes(k1,k2));
                            hold(axesToHoldAndLink, 'off');
                            
                            % check if already linked
                            KEY = 'graphics_linkaxes';
                            hlink = getappdata(axesToHoldAndLink,KEY);
                            if isempty(hlink)
                                axesToLink(end+1) = axesToHoldAndLink; %#ok<AGROW>
                            end
                        end
                    end
                end
                if ~isempty(axesToLink)
                    linkaxes(axesToLink, 'x');
                end
            end
            
        end
         
        function str = taggedName(this)
            if isempty(this.tag)
                str = this.name;
            else
                str = [this.tag ' / ' this.name];
            end
        end
        
        
        function signalList = getSignalByTag(this, tag)
            % Return a list of signal givin the tag
            
            signalList = ClSignal.empty();
            for signalIndex=1:numel(this)
                if strcmp(this(signalIndex).tag, tag)
                    signalList(end+1) = this(signalIndex); %#ok<AGROW>
                end
                if length(signalList) > 1
                    my_breakpoint
                    % TODO : A priori on ne devrait pas avoir plusieurs signaux avec le m�me tab
                    % Si cela se confirme on peut mettre un return apr�s
                    % avoir touv� le premier et d�placer le empty en fin de
                    % fonction (pas de tag trouv�)
                    % Cette fonction est appell�e de tr�s nombreuses fois,
                    % �a serait bien de r�duire son temps d'ex�cution 
                end
            end
        end
        
        function [flag, val, Unit] = getValueMatrix(this, varargin)
            % Return the value matrix of signal (or signalList)
            [varargin, unitOut] = getPropertyValue(varargin, 'unitOut', []);
            [varargin, subl]    = getPropertyValue(varargin, 'subl',    []); %#ok<ASGLU>
            
            flag = 0;
            val  = [];
            Unit = '';
            
            %% Signaux verticaux nouvelle g�n�ration
            
            for k1=1:length(this)
                X = [];
                for k2=1:numel(this(k1).ySample)
                    X(:,end+1) = this(k1).ySample(k2).getDataUnit(unitOut); %#ok<AGROW>
                    if isempty(unitOut)
                        Unit = this(k1).ySample(k2).unit;
                    else
                        Unit = unitOut;
                    end
                    if isempty(subl)
                        val = X;
                    else
                        val = X(subl,:);
                    end
                    flag = 1;
                end
                if ~isempty(X)
                    return
                end
            end
        end
        
        
        function isLinkedSignal = isLinkedSignal(this)
            % Check if signal is linked
            isLinkedSignal = 0;
            for k1=1:length(this)
                isLinkedSignal = isLinkedSignal || (this(k1).linkType ~= 1);
            end
        end
        
        %% Operations
        % Signal Addition
        function c = plus(a,b)
            %Override AbstractOperator
            %  c = a+b;
            c = plus@AbstractOperator(a,b);
            
            % Update history
            c.history.updateHistory('Addition', 'a', a, 'b', b);
        end
        
        % Signal Subtraction
        function c = minus(a,b)
            %Override AbstractOperator
            % c = a-b;
            c = minus@AbstractOperator(a,b);
            
            % Update history
            c.history.updateHistory('Subtraction', 'a', a, 'b', b);
        end
        
        % Signal Multiplication
        function c = mtimes(a,b)
            %Override AbstractOperator a*b ==> a.b
            c = a.*b;
            % Update history
            c.history.updateHistory('Multiplication', 'a', a, 'b', b);
        end
        
        % Signal division
        function c = mrdivide(a,b)
            %Override AbstractOperator a/b ==> a./b
            c = a./b;
            % Update history
            c.history.updateHistory('Division', 'a', a, 'b', b);
        end
        
        %%
        
    end
    
    methods (Access = protected)
        
        function isSameNbrOfYSample = isSameNbrOfYSample(this)
            % Check if several Signals & same number of Y Samples
            
            isSameNbrOfYSample= true;
            nbSignals = numel(this);
            for k=1:nbSignals-1
                if numel(this(k).ySample) ~= numel(this(k+1).ySample)
                    isSameNbrOfYSample = false;
%                     str1 = sprintf('Les instances de ClSignal n''ont pas le m�me nombre de Y Samples');
                    str2 = sprintf('Instances of ClSignal haven''t the same number of Y Samples');
                    my_warndlg(str2, 1);
                    return
                end
            end
        end
        
        function isPlottable = isPlottable(this, fig, subAxes)
            % Check signal is plotable in figure
            
            isPlottable = true;
            
            axesArray = findobj(fig, 'Type', 'Axes');
            if ~isempty(axesArray)
                nbSignals = numel(this);
                for k1=1:nbSignals
                    
                    nbAxes = nnz(unique(subAxes(:))); % compute nbr of axes with non zero of unique elements of subAxes
                    if length(axesArray) ~= nbAxes
%                         str1 = 'Le nombre de signaux n''est pas compatible avec le nombre d''axes';
                        str2 = 'The number of signal is incompatible with the number os axes';
                        my_warndlg(str2, 1);
                        return
                    end
                    
                    nbYSample = numel(this(k1).ySample);
                    for k2=1:nbYSample
                        if ~isempty(this(k1).ySample(k2).unit)
                            title = [this(k1).ySample(k2).name ' (' this(k1).ySample(k2).unit ')'];
                        else
                            title = this(k1).ySample(k2).name;
                        end
                        
                        if ~strcmp(axesArray(subAxes(k1,k2)).Title.String, title)
%                             str1 = sprintf('Le nom de l''axe (%s) ne correspond pas au nom du signal (%s)',         axesArray(subAxes(k1,k2)).Title.String, title);
                            str2 = sprintf('The name of the axes (%s) does not correspond to the signal name (%s)', axesArray(subAxes(k1,k2)).Title.String, title);
                            my_warndlg(str2, 1);
                            return
                        end
                    end
                end
            end
        end
        
        function [axesArray, lineArray] = oneInstancePlot(this, axesArray, subAxes)
            % plot one instance of ClSignal
            lineArray = matlab.graphics.chart.primitive.Line.empty(numel(this.ySample),0);
            
            for k=1:numel(this.ySample)
                if subAxes(k)~=0
                    % Line Name
                    if ~isempty(this.ySample(k).name)
                        lineName = [this.name ' - ' this.ySample(k).name];
                    else
                        lineName = this.name;
                    end

                    lineArray(k,1) = PlotUtils.createSScPlot(axesArray(subAxes(k)), this.xSample(k).data, this.ySample(k).data, 'DisplayName', lineName, 'isLightContextMenu', true);

                    % set style to line
                    this.ySample(k).setStyleToLine(lineArray(k));
                    
                    % isAngle to line
                    if strcmp( this.ySample(k).dataType, SampleType.ANGLE)
                       setappdata( lineArray(k,1), 'isAngle', true);
                    end
                    
                    % xlabel
                    if isempty(this.xSample(k).unit)
                        
                        xlabel(axesArray(subAxes(k)), this.xSample(k).name);
                    else
                        xlabel(axesArray(subAxes(k)), [this.xSample(k).name ' (' this.xSample(k).unit ')']);
                    end
                    
                    % title
                    if isempty(axesArray(subAxes(k)).Title.String) % if no title
                        strTitle = lineName;
                    else
                        strTitle = this.name;
                    end
                    
                    if ~isempty(this.ySample(k).unit) % add unit to title
                        strTitle = [strTitle ' (' this.ySample(k).unit ')']; %#ok<AGROW>
                    end
                    
                    if this.isVertical % if vertical, add an empty line to see ticks labels
                        strTitle = {strTitle; ''}; %#ok<AGROW>
                    end
                    
                    title(axesArray(subAxes(k)),strTitle, 'Interp', 'none')
                    
                    % axe configuration
                    grid(axesArray(subAxes(k)), 'on');
                    axis(axesArray(subAxes(k)), 'tight')
                    hold(axesArray(subAxes(k)), 'on')
                end
            end
        end
        
        function value = getOperationValue(this)
            % implement AbstractOperator
            
            for k=numel(this.ySample):-1:1
                value(k,:) = this.ySample(k).data;
            end
        end
        
        function setOperationValue(this, newValue)
            % implement AbstractOperator
            
            for k=numel(this.ySample):-1:1
                this.ySample(k).data = newValue(k, :);
            end
        end
    end
end
