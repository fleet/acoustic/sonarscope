classdef IndexYSample < YSample
    % YSampleIndex Description
    %   Class that describes a sample on Y axe with index options
    %
    % Properties :
    %
    % Examples
    % indexYSample1 = IndexYSample('data', [1 3 1 4 2 3 3], 'strIndexList', {'A'; 'BB'; 'CCC'; 'DDDD'})
    % indexYSample2 = IndexYSample('data', 1:4, 'strIndexList', {'A'; 'BB'; 'CCC'; 'DDDD'}, 'name', 'Test IndexYSample')
    %
    % ----------------------------------------------------------------------------
    
    properties
        strIndexList
    end
    
    methods
        function this = IndexYSample(varargin)
            % Superclass Constructor
            this = this@YSample(varargin{:});
            
            this = set(this, varargin{:});
        end
        
        function this = set(this, varargin)
            this = set@YSample(this, varargin{:});
            [varargin, this.strIndexList] = getPropertyValue(varargin, 'strIndexList', this.strIndexList); %#ok<ASGLU>
        end
        
        function str = summary(this, varargin)
            str = sprintf('%s : %s', ...
                this.summaryWithoutUnit(varargin), ...
                SelectionDansListe2str(mode(this.data), this.strIndexList, 'Num'));
        end
    end
end
