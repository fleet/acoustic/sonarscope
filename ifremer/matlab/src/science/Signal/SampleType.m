classdef SampleType
    % SampleType Description
    %   Class that contains the available type for a Sample
    %
    % Properties :
    %
    % Examples
    %   ysample = YSample('data', linspace(0,1,100).', 'dataType', SampleType.NORMAL);
    % ----------------------------------------------------------------------------
    
    properties (Constant)
        NORMAL  = 'Normal';
        ANGLE   = 'Angle';
        INDEX   = 'Index';
        
        LIST = {SampleType.NORMAL, ...
            SampleType.ANGLE, ...
            SampleType.INDEX};
    end
end
