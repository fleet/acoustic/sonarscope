classdef SignalContainer < handle & AbstractDisplay & Cloneable &  matlab.mixin.Heterogeneous
    % SignalContainer Description
    %   Class that contains a list of signals (Reflectivity,
    %   Bathymetry, Latitude, Longitude, other...).
    %
    % SignalContainer Syntax
    %   s = SignalContainer(...)
    %
    % Properties :
    %   name       - Name of the signalContainer
    %   signalList - list of signal
    %
    % Examples
    %
    %   %Exemple 1 :
    %   ySample = YSample('data', sin(0:0.1*pi:10*pi-0.1*pi));
    %   signal  = ClSignal('name', 'Simple sinus function','ySample',ySample);
    %   signalContainer = SignalContainer('signalList', signal);
    %   signalContainer.plot
    %
    %   %Exemple 2 :
    %   ySample(1) = YSample('data', sin(0:0.1*pi:10*pi-0.1*pi), 'color', 'k');
    %   ySample(2) = YSample('data', sin(0:0.1*pi:10*pi-0.1*pi), 'color', 'b');
    %   ySample(3) = YSample('data', sin(0:0.1*pi:10*pi-0.1*pi), 'color', 'r');
    %   ySample(4) = YSample('data', cos(0:0.1*pi:10*pi-0.1*pi), 'color', 'k');
    %   ySample(5) = YSample('data', cos(0:0.1*pi:10*pi-0.1*pi), 'color', 'b');
    %   ySample(6) = YSample('data', cos(0:0.1*pi:10*pi-0.1*pi), 'color', 'r');
    %   ySample(7) = YSample('data', sin(0:0.1*pi:10*pi-0.1*pi), 'color', 'k');
    %   ySample(8) = YSample('data', sin(0:0.2*pi:20*pi-0.2*pi), 'color', 'b');
    %   ySample(9) = YSample('name', 'test', 'data', sin(0:0.2*pi:20*pi-0.2*pi), 'color', 'r');
    %   signal  = ClSignal('name', 'Simple sinus function', 'ySample', ySample);
    %   signalContainer = SignalContainer('signalList', signal);
    %   signalContainer.plot
    %
    %   %Exemple 3 : Plot several signals in different axes :
    %   ySample1(1) = YSample('data', sin(0:0.1*pi:10*pi-0.1*pi));
    %   ySample1(2) = YSample('data', cos(0:0.1*pi:10*pi-0.1*pi));
    %   signalTest(1)  = ClSignal('name', 'signal1', 'ySample', ySample1);
    %
    %   ySample2(1) = YSample('data', sin(0:0.1*pi:10*pi-0.1*pi)+0.1);
    %   ySample2(2) = YSample('data', cos(0:0.1*pi:10*pi-0.1*pi)+0.1);
    %   ySample2(3) = YSample('data', sin(0:0.1*pi:10*pi-0.1*pi)+0.2, 'name', 'ysample2(3)');
    %   signalTest(2)  = ClSignal('name', 'signal2', 'ySample', ySample2);
    %
    %   signalContainerTest = SignalContainer('signalList', signalTest);
    %   signalContainerTest.plot
    %   signalContainerTest.plot('subAxes', [1 2 0; 1 2 3]);
    % ----------------------------------------------------------------------------
    
    properties (Access = public)
        name
        signalList
        tag
        originFilename = []; % Name of the file from which the signal was extracted
    end
    
    methods
        function this = SignalContainer(varargin)
            this = set(this, varargin{:});
        end
        
        function this = set(this,varargin)
            [varargin, this.name]           = getPropertyValue(varargin, 'name',           this.name);
            [varargin, this.signalList]     = getPropertyValue(varargin, 'signalList',     this.signalList);
            [varargin, this.tag]            = getPropertyValue(varargin, 'tag',            this.tag);
            [varargin, this.originFilename] = getPropertyValue(varargin, 'originFilename', this.originFilename); %#ok<ASGLU>
        end
        
        
        %% PLOT FUNCTIONS
        function [fig, axesArray, lineArray] = plot(this, varargin)
            
            [varargin, fig]           = getPropertyValue(varargin, 'Fig',           []);
            [varargin, axesArray]     = getPropertyValue(varargin, 'axesArray',     matlab.graphics.axis.Axes.empty);
            [varargin, subAxes]       = getPropertyValue(varargin, 'subAxes',       []);
            [varargin, imageDataList] = getPropertyValue(varargin, 'imageDataList', []); %#ok<ASGLU>
            
            % Check if several Signals
            nbSignalContainers = numel(this);
            
            %init the lineArray : nb signalContainer, nb signals, nb ysample
            lineArray = matlab.graphics.chart.primitive.Line.empty(0, 0, 0);
            % Plot for each signalContainer
            for k=1:nbSignalContainers
                [fig, axesArrayReturn, lineIstanceArray] = this(k).oneInstancePlot(fig, axesArray, subAxes, imageDataList);
                  axesArray = axesArrayReturn;
                if ~isempty(lineIstanceArray)
                    lineArray(k, :, :) = lineIstanceArray;
                else
                    [~, nbSignal, nbYSample] = size(lineArray);
                    lineArray(k, :, :) = gobjects(nbSignal,nbYSample); %empty array for graphics objects
                end
            end
            
            % link lines properties of same signals/samples
            this.linkPropertiesOfLines(lineArray);
        end
        
        
        function signalList = getSignalByTag(this, tag)
            signalList = ClSignal.empty();
            for signalContainerIndex=1:numel(this)
                for signalIndex=1:numel(this(signalContainerIndex).signalList)
                    if strcmp(this(signalContainerIndex).signalList(signalIndex).tag, tag)
                        signalList(end+1) = this(signalContainerIndex).signalList(signalIndex); %#ok<AGROW>
                    end
                end
            end
        end
    end
    
    
    methods (Access = protected)
        
        function [fig, axesArray, lineArray] = oneInstancePlot(this, fig, axesArray, subAxes, imageDataList)
            % plot one instance of SignalContainer
            if ~isempty(this.signalList)
                [fig, axesArray, lineArray] = this.signalList.plot('Fig', fig, ...
                    'axesArray', axesArray, 'subAxes', subAxes, 'imageDataList', imageDataList);
            else
                 lineArray = matlab.graphics.chart.primitive.Line.empty(0);
            end
        end
        
        function linkPropertiesOfLines(this, lineArray)
            % link lines properties of same signals/samples
            
            % compute the sample max number depending dimension of lineArray.
            lineArraySize = size(lineArray);
            if size(lineArraySize) == 3
                % If dim = 3 (container/signal/sample), sampleMaxNb = lineArraySize(3)
                sampleMaxNb = lineArraySize(3);
            else
                % If not, (container/signal), sampleMaxNb = 1
                sampleMaxNb = 1;
            end
            
            for containerIndex=1:numel(this)
                for sampleIndex=1:sampleMaxNb
                    % the raw list can contains non-line object (if sample
                    % nb differents or signals nb different
                    lineRawList = lineArray(containerIndex, :, sampleIndex);
                    %  construct a list with only line objects
                    lineTolink = matlab.graphics.chart.primitive.Line.empty(0);
                    for k=1:numel(lineRawList)
                        if isa(lineRawList(k), SignalDialog.lineClassName)
                            lineTolink(end+1) = lineRawList(k); %#ok<AGROW>
                        end
                    end
                    
                    % add listner to link properties (linkprop does'nt work)
                    for k=1:numel(lineTolink)
                        addlistener(lineTolink(k),'Color',           'PostSet',@(~,~)set(lineTolink(:),'Color',          get(lineTolink(k),'Color')));
                        addlistener(lineTolink(k),'LineStyle',       'PostSet',@(~,~)set(lineTolink(:),'LineStyle',      get(lineTolink(k),'LineStyle')));
                        addlistener(lineTolink(k),'LineWidth',       'PostSet',@(~,~)set(lineTolink(:),'LineWidth',      get(lineTolink(k),'LineWidth')));
                        addlistener(lineTolink(k),'Marker',          'PostSet',@(~,~)set(lineTolink(:),'Marker',         get(lineTolink(k),'Marker')));
                        addlistener(lineTolink(k),'MarkerSize',      'PostSet',@(~,~)set(lineTolink(:),'MarkerSize',     get(lineTolink(k),'MarkerSize')));
                        addlistener(lineTolink(k),'MarkerEdgeColor', 'PostSet',@(~,~)set(lineTolink(:),'MarkerEdgeColor',get(lineTolink(k),'MarkerEdgeColor')));
                    end
                end
            end
        end
    end
end
