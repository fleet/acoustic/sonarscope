classdef SignalUtils
    % Class SignalUtils provides static methods for operations on Signals
    %
    % Examples
    %   sgNaN = ClSignal('ySample', YSample('data', rand(200, 1)), 'Name', '200 random values');
    %
    %   k1 = length(sgNaN.ySample.data);
    %   r = randi([1 k1], 1, randi([1 50], 1, 1));
    %   sgNaN.ySample.data(r) = NaN;
    %   sgNaN.name = '200 random values with missing data';
    %   SignalUtils.plotNaN(sgNaN)
    %
    %   window = 2;
    %   sgNew = sgNaN.clone();
    %   SignalUtils.fillNaN(sgNew, window);
    %   SignalUtils.plotNaN(sgNew)
    % ----------------------------------------------------------------------------
    
    properties
    end
    
    methods (Static)
        
        function plotNaN(signal, varargin)
            % Plot ClSignal with nan detection
            [~, axesArray, ~] = signal.plot(varargin{:});
            
            % Retrieve subAxes
            [varargin, subAxes] = getPropertyValue(varargin, 'subAxes', []); %#ok<ASGLU>
            
            nbYSample = numel(signal(1).ySample);
            if isempty(subAxes)
                subAxes = 1:nbYSample;
            end
            
            for k1=1:numel(signal)
                for k2=1:numel(signal(k1).ySample)
                    sub = isnan(signal(k1).ySample(k2).data); % detect NaN
                    sub = sub(:); % set sub horizontal
                    sub = sub | [1; sub(1:end-1)] | [sub(2:end); 1]; % transform sub
                    
                    x = signal(k1).xSample(k2).data;
                    y = signal(k1).ySample(k2).data;
                    hold(axesArray(subAxes(k2)), 'on');
                    h = plot(axesArray(subAxes(k2)), x(sub), y(sub), '*');
                    if ~isempty(signal.ySample(k2).color)
                        set(h, 'Color', signal(k1).ySample(k2).color)
                    end
                    hold(axesArray(subAxes(k2)), 'off')
                end
            end
        end
        
        
        %% Filters & Algorithms
        function signal = fillNaN(signal, windowSize)
            % Description
            %   The "fillNaN" function replaces the NaN values with the mean value of
            %   the data located within the window size chosen by the user
            %
            % Syntax
            %   signal = SignalUtils.fillNaN(signal, windowSize)
            %
            % Input arguments
            %   signal       : the object on which the NaN have to be changed
            %   windowSize : the range around the NaN value.
            %
            % Output arguments
            %  The modified object, signal.
            %
            % Remarks : If no output is given, the modifications will occur on the
            % object that was input during the call of the function.
            %
            % Examples
            %   sgNaN = ClSignal('ySample', YSample('data', rand(200, 1)), 'Name', '200 random values');
            %
            %   k1 = length(sgNaN.ySample.data);
            %   r = randi([1 k1], 1, randi([1 50], 1, 1));
            %   sgNaN.ySample.data(r) = NaN;
            %   sgNaN.name = '200 random values with missing data';
            %   SignalUtils.plotNaN(sgNaN)
            %
            %   window = 2;
            %   sgNew = sgNan.clone();
            %   SignalUtils.fillNaN(sgNew, window);
            %   SignalUtils.plotNaN(sgNew)
            %
            % Authors : ED + JMA
            % See also ClSignal Authors
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc ClSignal/fillNaN">ClSignal/fillNaN</a>
            % ----------------------------------------------------------------------------
            
            for k1=1:numel(signal.ySample)
                n = size(signal.ySample(k1).data, 2);
                for k2=1:n
                    y      = signal.ySample(k1).data(:,k2);
                    toFill = find(isnan(y));
                    
                    for k3=1:length(toFill)
                        % Testing if the array indexes corresponding to the window size around
                        % NaN are inside the array to process
                        if ((toFill(k3)-windowSize) > 0) && ((toFill(k3)+windowSize) <= length(y)) % Yes they are
                            subMat = [y(toFill(k3)-windowSize:toFill-1) ; y(toFill(k3)+1:toFill(k3)+windowSize)];
                            
                        elseif ((toFill(k3)-windowSize) <= 0) && ((toFill(k3)+windowSize) <= length(y)) % No they aren't, the resulting index is < 0
                            subMat = [y(1:toFill(k3)-1) ; y(toFill(k3)+1:toFill(k3)+windowSize)];  % The subMat starts in the first index of the array to process
                            
                        elseif ((toFill(k3)-windowSize) > 0) && ((toFill(k3)+windowSize) > length(y))
                            subMat = [y(toFill(k3)-windowSize:toFill(k3)-1) ; y(toFill(k3)+1:length(y))];
                        else
                            subMat = y;
                        end
                        y(toFill(k3)) = mean(subMat, 'omitnan');
                        signal.ySample(k1).data(:,k2) = y;
                    end
                end
            end
            % Update history
            signal.history.updateHistory('fillNaN', 'windowSize', windowSize);
        end
        
        
        function signal = interp1(signal, x2, varargin)
            %% interp1
            % Overloading of the existing interp1 function in matlab
            %
            % Syntax
            %   signal = SignalUtils.interp1(signal, x2, varargin)
            %
            % Input Arguments
            %   signal : the current object
            %   varargin : the finer abscissa,
            %   optional : the interpolation method ('linear' by default)
            %
            % Output Arguments
            %  The modified object, signal.
            %
            % Remarks : If no output is given, the modifications will occur on the
            % object that was input during the call of the function.
            %
            % Examples
            %   ySample = YSample('data',  rand(50,1), 'color', 'r');
            %   sg1  = ClSignal('name', '50 random values', 'ySample',ySample);
            %
            %      finerAbsc = linspace(1,50,1000); %1000 values between 1 and 50
            %
            %      sg1 = SignalUtils.interp1(sg1, finerAbsc, 'spline')
            %
            % Authors : JMA
            
            isAllSameXSample = true;
            for k=1:numel(signal.xSample)
                if (signal.xSample(k) ~= signal.xSample(1))
                    isAllSameXSample = false;
                    break;
                end
            end
            
            for k=1:numel(signal.ySample)
                x = signal.xSample(k).data;
                YData = signal.ySample(k).data;
                signal.ySample(k).data = [];
                y = interp1(x, YData, x2, varargin{:});
                signal.ySample(k).data = y;
                if ~isAllSameXSample
                    signal.xSample(k).data = x2;
                    % Update history
                    signal.history.updateHistory('interp1', 'Finer definition', x2, 'Method', 'extrapolation');
                end
                
            end
            if isAllSameXSample
                signal.xSample(1).data = x2;
            end
            
        end
        
        
        function isNaN = isnan(signal)
            % isnan Checks if there are NaNs in each ySample
            for k=numel(signal.ySample):-1:1
                isNaN(k,:) = isnan(signal.ySample(k).data);
            end
        end %fin isnan
    end
end
