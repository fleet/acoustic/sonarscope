classdef XSample < ClSample
    % XSample Description
    %   Class that describes samples on X axe
    %
    % Properties :
    %
    % Examples
    %   xSample = XSample('data', sin(0:0.1*pi:10*pi-0.1*pi));
    % ----------------------------------------------------------------------------
    
    properties
    end
    
    methods
        function this = XSample(varargin)
            % Constructor of the class
            
            % Superclass Constructor
            this = this@ClSample(varargin{:});
            
            this = set(this, varargin{:});
        end
        
        function this = set(this,varargin)
            set@ClSample(this,varargin{:});
        end
    end
end
