classdef YSample < ClSample & LineSpec & matlab.mixin.Heterogeneous
    % YSample Description
    %   Class that describes a sample on Y axe
    %
    % Properties :
    %
    % Examples
    %   ySample = YSample('data', sin(0:0.1*pi:10*pi-0.1*pi));
    %   ySample = YSample('data', sin(0:0.1*pi:10*pi-0.1*pi), 'color', 'k');
    % ----------------------------------------------------------------------------
    
    properties
    end
    
    methods
        function this = YSample(varargin)
            % Superclass Constructor
            this = this@ClSample(varargin{:});
            this = this@LineSpec(varargin{:});
            
            this = set(this, varargin{:});
        end 
        
        function this = set(this, varargin)
            this = set@ClSample(this, varargin{:});
            this = set@LineSpec(this, varargin{:});
        end
    end
end
