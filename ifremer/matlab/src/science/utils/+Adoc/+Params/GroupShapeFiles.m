% Description
%   Params for GroupShapeFiles
%
% Syntax
%    [flag, listShpFileNames, fileNameShpCoverage, fileNameShpNavigation, repImport, repExport] = Adoc.Params.GroupShapeFiles(repImport, repExport)
%
% Intput Arguments
%   repImport : 
%   repExport : 
%
% Output Arguments
%   flag                  : 
%   listShpFileNames      : 
%   fileNameShpCoverage   : 
%   fileNameShpNavigation : 
%   repImport             : 
%   repExport             : 
%
% Examples
%    [flag, listShpFileNames, fileNameShpCoverage, fileNameShpNavigation, repImport, repExport] = Adoc.Params.GroupShapeFiles(repImport, repExport)
%
% Author : JMA
% See also Authors

function [flag, listShpFileNames, fileNameShpCoverage, fileNameShpNavigation, repImport, repExport] ...
    = GroupShapeFiles(repImport, repExport)

fileNameShpCoverage   = [];
fileNameShpNavigation = [];

%% Fichier SHP des "Coverage"

[flag, listShpFileNames, repImport] = uiSelectFiles('ExtensionFiles', '.shp', ...
    'RepDefaut', repImport, 'MaxLevel', 4, 'Entete', 'Select the coverage shp files');
if ~flag
    return
end

%% Ecriture du fichier ASCII

[flag, fileNameShpCoverage] = my_uiputfile('.shp', 'Define the output coverage shp file', fullfile(repExport, 'TotalCoverage.shp'));
if ~flag
    return
end
repExport = fileparts(fileNameShpCoverage);

%% Ecriture du fichier ASCII

[flag, fileNameShpNavigation] = my_uiputfile('.shp', 'Define the output navigation shp file', fullfile(repExport, 'TotalNavigation.shp'));
if ~flag
    return
end
repExport = fileparts(fileNameShpNavigation);
