% Description
%   Params for SurveyReport_AddFigures
%
% Syntax
%    [flag, nomFicAdoc, Title, Comment, hFig, repImport] = Adoc.Params.SurveyReport_AddFigures(repImport)
%
% Intput Arguments
%   repImport : 
%
% Output Arguments
%   flag       : 
%   nomFicAdoc : 
%   Title      : 
%   Comment    : 
%   hFig       : 
%   repImport  : 
%
% Examples
%    [flag, nomFicAdoc, Title, Comment, hFig, repImport] = Adoc.Params.SurveyReport_AddFigures(repImport)
%
% Author : JMA
% See also Authors


function [flag, nomFicAdoc, Title, Comment, hFig, repImport] = SurveyReport_AddFigures(repImport)

persistent persistent_Title persistent_Comment

Title   = [];
Comment = [];
hFig    = [];

%% Nom du survey

nomFicCompens = fullfile(repImport, '*.adoc');
[flag, nomFicAdoc] = my_uigetfile(nomFicCompens, 'Survey Report file name');
if ~flag
    return
end
repImport = fileparts(nomFicAdoc);

%% S�lection des figures

hFig = findobj(0, 'Type', 'Figure');
N = length(hFig);
InitialValue = true(1, N);
for k=N:-1:1
    FigureNameForSurveyReport = getappdata(hFig(k), 'FigureNameForSurveyReport');
    if isempty(FigureNameForSurveyReport)
        hAxes = findobj(hFig(k), 'Type', 'Axe');
        if isempty(hAxes)
            str{k,1} = sprintf('%d - %s', hFig(k).Number, hFig(k).Name);
        elseif isempty(hAxes(end).Title.String)
            str{k,1} = sprintf('%d - %s', hFig(k).Number, hFig(k).Name);
        elseif iscell(hAxes(end).Title.String)
            str{k,1} = sprintf('%d - %s', hFig(k).Number, hAxes(end).Title.String{1});
        else
            str{k,1} = sprintf('%d - %s', hFig(k).Number, hAxes(end).Title.String);
        end
    else
        str{k,1} = sprintf('%d - %s', hFig(k).Number, FigureNameForSurveyReport);
    end
    if contains(hFig(k).Name, 'SonarScope Work Window')
        InitialValue(k) = false;
    end
end

[rep, flag] = my_listdlgMultiple('Liste of items', str, 'InitialValue', find(InitialValue));
if ~flag
    return
end
hFig = hFig(rep);

%% Title & Comment

if isempty(persistent_Title)
    Title   = '';
    Comment = '';
else
    Title   = persistent_Title;
    Comment = persistent_Comment;
end

nomVar = {'Title' ; 'Comment'};
value = {Title ; Comment};
[X, flag] = my_inputdlg(nomVar, value, 'numlines', [1 50; 10 50]);
if ~flag
    return
end
Title   = X{1};
Comment = X{2};

persistent_Title   = Title;
persistent_Comment = Comment;
