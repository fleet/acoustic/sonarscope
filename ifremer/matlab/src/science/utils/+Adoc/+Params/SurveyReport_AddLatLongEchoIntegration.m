% Description
%   Params for SurveyReport_AddLatLongEchoIntegration
%
% Syntax
%    [flag, nomFicAdoc, Title, Comment, Thresholds, repImport] = Adoc.Params.SurveyReport_AddLatLongEchoIntegration(this, repImport)
%
% Intput Arguments
%   this      : Instance de cl_image
%   repImport : 
%
% Output Arguments
%   flag       : 
%   nomFicAdoc : 
%   Title      : 
%   Comment    : 
%   Thresholds : 
%   repImport  : 
%
% Examples
%    [flag, nomFicAdoc, Title, Comment, Thresholds, repImport] = Adoc.Params.SurveyReport_AddLatLongEchoIntegration(this, repImport)
%
% Author : JMA
% See also Authors


function [flag, nomFicAdoc, Title, Comment, Thresholds, repImport] = SurveyReport_AddLatLongEchoIntegration(this, repImport)

persistent persistent_Thresholds_OverSpecular persistent_Thresholds_OversSeafloor

Thresholds = [];

[flag, nomFicAdoc, Title, Comment, repImport] = Adoc.Params.SurveyReport_AddLatLongImage(this, repImport);
if ~flag
    return
end

flagOverSpecular = contains(this.Name, 'OverSpecular');
if flagOverSpecular
    if isempty(persistent_Thresholds_OverSpecular)
        Thresholds = [-50 -40 -30 -25];
    else
        Thresholds = persistent_Thresholds_OverSpecular;
    end
else
    if isempty(persistent_Thresholds_OversSeafloor)
        Thresholds = [10 20 25];
    else
        Thresholds = persistent_Thresholds_OversSeafloor;
    end
end

nomVar = 'Enter the threshold values';
value = num2str(Thresholds(:)');
[str, flag] = my_inputdlg(nomVar, value);
if ~flag
    return
end
str = str{1};
if isempty(str)
    Thresholds = [];
else
    Thresholds = sscanf(str, '%f');
    if isempty(Thresholds)
        flag = 0;
        return
    end
end

if flagOverSpecular
    persistent_Thresholds_OverSpecular = Thresholds;
else
    persistent_Thresholds_OversSeafloor = Thresholds;
end
