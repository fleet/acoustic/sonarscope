% Description
%   Params for SurveyReport_AddLatLongImage
%
% Syntax
%    [flag, nomFicAdoc, Title, Comment, repImport] = Adoc.Params.SurveyReport_AddLatLongImage(this, repImport)
%
% Intput Arguments
%   this      : Instance de cl_image
%   repImport : 
%
% Output Arguments
%   flag       : 
%   nomFicAdoc : 
%   Title      : 
%   Comment    : 
%   repImport  : 
%
% Examples
%    [flag, nomFicAdoc, Title, Comment, repImport] = Adoc.Params.SurveyReport_AddLatLongImage(this, repImport)
%
% Author : JMA
% See also Authors


function [flag, nomFicAdoc, Title, Comment, repImport] = SurveyReport_AddLatLongImage(this, repImport)

persistent persistent_Title persistent_Comment persistent_Title_OverSpecular persistent_Title_OversSeafloor

nomFicAdoc = [];
Title      = [];
Comment    = [];

%% Test d'ÚligibilitÚ de l'image

flag = testSignature(this, 'GeometryType', cl_image.indGeometryType('LatLong'));
if ~flag
    return
end

%% Nom du survey

nomFicCompens = fullfile(repImport, '*.adoc');
[flag, nomFicAdoc] = my_uigetfile(nomFicCompens, 'Survey Report file name');
if ~flag
    return
end
repImport = fileparts(nomFicAdoc);

%% Title & Comment

if contains(this.Name, 'OverSpecular')
    typeData = 2;
elseif contains(this.Name, 'OverSeafloor') || contains(this.Name, 'OverSeabed')
    typeData = 3;
else
    typeData = 1;
end

switch typeData
    case 1
        if isempty(persistent_Title)
            Title   = '';
            Comment = '';
        else
            Title   = persistent_Title;
            Comment = persistent_Comment;
        end
    case 2 % OverSpecular
        if isempty(persistent_Title_OverSpecular)
            Title   = '';
            Comment = '';
        else
            Title   = persistent_Title_OverSpecular;
            Comment = persistent_Comment;
        end
    case 3 % OverSeafloor
        if isempty(persistent_Title_OversSeafloor)
            Title   = '';
            Comment = '';
        else
            Title   = persistent_Title_OversSeafloor;
            Comment = persistent_Comment;
        end
end

nomVar = {'Title' ; 'Comment'};
value = {Title ; Comment};
[X, flag] = my_inputdlg(nomVar, value, 'numlines', [1 50; 10 50]);
if ~flag
    return
end
Title   = X{1};
Comment = X{2};

switch typeData
    case 1
%         if isempty(persistent_Title)
            persistent_Title   = Title;
            persistent_Comment = Comment;
%         else
%             Title   = persistent_Title;
%             Comment = persistent_Comment;
%         end
    case 2 % OverSpecular
%         if isempty(persistent_Title_OverSpecular)
            persistent_Title_OverSpecular   = Title;
            persistent_Comment              = Comment;
%         else
%             Title   = persistent_Title_OverSpecular;
%             Comment = persistent_Comment;
%         end
    case 3 % OverSeafloor
%         if isempty(persistent_Title_OversSeafloor)
            persistent_Title_OversSeafloor = Title;
            persistent_Comment             = Comment;
%         else
%             Title   = persistent_Title_OversSeafloor;
%             Comment = persistent_Comment;
%         end
end
