% Description
%   SurveyReport_Create
%
% Syntax
%    [flag, nomFicAdoc, WebSiteName, LogFileName, repExport] = Adoc.Params.SurveyReport_Create(repImport)
%
% Intput Arguments
%   repImport : 
%
% Output Arguments
%   flag        : 
%   nomFicAdoc  : 
%   WebSiteName : 
%   LogFileName : 
%   repExport   : 
%
% Examples
%    [flag, nomFicAdoc, WebSiteName, LogFileName, repExport] = Adoc.Params.SurveyReport_Create(repImport)
%
% Author : JMA
% See also Authors

function [flag, nomFicAdoc, WebSiteName, LogFileName, repExport] = SurveyReport_Create(repImport)

nomFicAdoc  = [];
WebSiteName = [];
LogFileName = [];
repExport   = [];

QL = get_LevelQuestion;

%% Nom du survey

str1 = 'Saisissez le r�pertoire o� le Survey Report sera cr��.';
str2 = 'Select or create the directory where the Survey Report will be copied';
[flag, nomDirAdoc] = my_uigetdir(repImport, Lang(str1,str2));
if ~flag
    return
end
repExport = nomDirAdoc;

%% Nom du survey

while 1
    nomVar = {'Name of the survey'};
    [SurveyName, flag] = my_inputdlg(nomVar, {''});
    if ~flag
        return
    end
    SurveyName = SurveyName{1};
    if isempty(SurveyName)
        str1 = 'Le nom de survey ne peut pas �tre vide';
        str2 = 'The survey name cannot be emply.';
        my_warndlg(Lang(str1,str2), 1);
    else
        SurveyName = strrep(SurveyName, ' ', '');
        break
    end
end

%% V�rification

listing = dir(nomDirAdoc);
nomFicAdoc = fullfile(nomDirAdoc, [SurveyName '.adoc']);
if (length(listing) > 2) && exist(nomFicAdoc, 'file')
    str1 = sprintf('Le fichier "%s" exite d�j�, voulez-vous l''�craser ?', nomFicAdoc);
    str2 = sprintf('"%s" already exists, do you want to overwite it ?', nomFicAdoc);
    [rep, flag] = my_questdlg(Lang(str1,str2));
    if ~flag
        return
    end
    if rep == 2
        flag = 0;
        return
    end
end

%% Nom du site internet

if QL > 1
    nomVar = {'Name of the website if any (let if empty otherwise)'};
    [WebSiteName, flag] = my_inputdlg(nomVar, {''});
    if ~flag
        return
    end
    WebSiteName = WebSiteName{1};
end

%% LogFileName

if QL > 1
    str1 = 'Voulez-vous copier et r�f�rencer un fichier log de la campagne dans l''arborescence du rapport ?';
    str2 = 'Do you want to copy a log file of the survey into the survey report ?';
    [rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
    if ~flag || (rep == 2)
        return
    end
    [flag, LogFileName] = uiSelectFiles('RepDefaut', repImport);
    if flag
        LogFileName = LogFileName{1};
    end
end
