% Description
%   Params for SurveyReport_Extract
%
% Syntax
%    [flag, nomFicAdoc, nomDirOut, params, flagZip, repImport, repExport] = Adoc.Params.SurveyReport_Extract(repImport, repExport)
%
% Intput Arguments
%   repImport : 
%   repExport : 
%
% Output Arguments
%   flag       : 
%   nomFicAdoc : 
%   nomDirOut  : 
%   params     : 
%   flagZip    : 
%   repImport  : 
%   repExport  : 
%
% Examples
%    [flag, nomFicAdoc, nomDirOut, params, flagZip, repImport, repExport] = Adoc.Params.SurveyReport_Extract(repImport, repExport)
%
% Author : JMA
% See also Authors


function [flag, nomFicAdoc, nomDirOut, params, flagZip, repImport, repExport] ...
    = SurveyReport_Extract(repImport, repExport)

nomDirOut = [];
flagZip   = [];

%% Nom du survey

nomFicCompens = fullfile(repImport, '*.adoc');
[flag, nomFicAdoc] = my_uigetfile(nomFicCompens, 'Survey Report file name');
if ~flag
    params = [];
    return
end
nomDirIn = fileparts(nomFicAdoc);
repImport = nomDirIn;

%% nomDirOut

while 1
    [flag, nomDirOut] = my_uigetdir(repExport, 'Name of the directory where the shortened version will be created');
    if ~flag
        return
    end
    
    if strcmp(nomDirIn(end), '\')
        nomDirIn(end) = [];
    end
    if strcmp(nomDirOut, nomDirIn)
        str1 = 'Le r�pertoire de la version courte ne peut pas �tre le m�me que celui de d�part.';
        str2 = 'The directory of the shortened version cannot be the same as the source one.';
        my_warndlg(Lang(str1,str2), 1);
    else
        break
    end
end

repExport = nomDirOut;

%% S�lection des r�pertoires � copier

params.DirNames{1} = 'KMZ';
params.DirNames{2} = 'ERS';
params.DirNames{3} = 'FIG';
params.DirNames{4} = 'GEOTIF';
[rep, flag] = my_listdlgMultiple('List of directories to export', params.DirNames, 'InitialValue', []);
if ~flag 
    return
end
params.DirFlags = rep;

%% Zip ou pas zip ?

str1 = 'Voulez-vous cr�er un fichier .zip ?';
str2 = 'Do you wand to create a .zip file ?';
[rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
if ~flag
    return
end
flagZip = (rep == 1);
