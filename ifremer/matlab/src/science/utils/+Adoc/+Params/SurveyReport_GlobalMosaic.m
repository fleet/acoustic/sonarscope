% Description
%   Params for SurveyReport_GlobalMosaic
%
% Syntax
%    [flag, nomFicAdoc, gridSize, InfoCompensationCurveReflectivity, AreaName, repImport] = Adoc.Params.SurveyReport_GlobalMosaic(repImport)
%
% Intput Arguments
%   repImport : 
%
% Output Arguments
%   flag                              : 
%   nomFicAdoc                        : 
%   gridSize                          : 
%   InfoCompensationCurveReflectivity : 
%   AreaName                          : 
%   repExport                         : 
%
% Examples
%    [flag, nomFicAdoc, gridSize, InfoCompensationCurveReflectivity, AreaName, repImport] = Adoc.Params.SurveyReport_GlobalMosaic(repImport)
%
% Author : JMA
% See also Authors


function [flag, nomFicAdoc, gridSize, InfoCompensationCurveReflectivity, AreaName, repImport] = SurveyReport_GlobalMosaic(repImport)

nomFicAdoc = [];
gridSize   = [];
AreaName   = [];
InfoCompensationCurveReflectivity = [];

%% Warning

str1 = sprintf('Il est pr�f�rable d''avoir import� la mar�e avant de lancrer ce processus (si cela s''y pr�te). Le MNT sera alors calcul� correctement.\n\nVoulez-vous poursuivre ?');
str2 = sprintf('The DTM will be OK if you have imported the tide before launching this processing (unless the MBES is a deep water one).\n\nIt is also preferable to import flags from Caris, Globe, ... (if they are available). You can also clean up the bathymetry using SonarScope.\n\nDo you want to continue ?');
[rep, flag] = my_questdlg(Lang(str1,str2));
if ~flag || (rep == 2)
    flag = 0;
    return
end

%% Nom du survey

nomFicCompens = fullfile(repImport, '*.adoc');
[flag, nomFicAdoc] = my_uigetfile(nomFicCompens, 'Survey Report file name');
if ~flag
    return
end
repImport = fileparts(nomFicAdoc);

%% Nom de la zone

nomVar = {'Name of the area if any (let if empty otherwise)'};
[AreaName, flag] = my_inputdlg(nomVar, {''});
if ~flag
    return
end
AreaName = AreaName{1};

%% Grid size

[flag, gridSize] = get_gridSize();
if ~flag
    return
end

%% Si Mosaique de R�flectivit�, on propose de faire une compensation

str1 = 'Voulez-vous r�aliser une mosa�que suppl�mentaire avec des fichiers de compensation ?';
str2 = 'Do you want to do an additional reflectivity mosaic with compensations files ?';
strQuestion = Lang(str1,str2);
[flag, CorFile, ModeDependant, UseModel, PreserveMeanValueIfModel] = QuestionCompensationCurve(repImport, 'strQuestion', strQuestion);
if ~flag
    return
end
if ~isempty(CorFile)
    InfoCompensationCurveReflectivity.FileName                 = CorFile;
    InfoCompensationCurveReflectivity.ModeDependant            = ModeDependant;
    InfoCompensationCurveReflectivity.UseModel                 = UseModel;
    InfoCompensationCurveReflectivity.PreserveMeanValueIfModel = PreserveMeanValueIfModel;
end
