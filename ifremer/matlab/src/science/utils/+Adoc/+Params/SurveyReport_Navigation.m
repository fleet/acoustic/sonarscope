% Description
%   Params for SurveyReport_Navigation
%
% Syntax
%    [flag, nomFicAdoc, ListSignals, repExport] = Adoc.Params.SurveyReport_Navigation(repExport)
%
% Input Arguments
%   repExport : The SonarScope default directory for file exports
%
% Output Arguments
%   flag        : 0=KO, 1=OK
%   nomFicAdoc  : 
%   ListSignals : 
%   repExport   : The directory name of the .xml file
%
% Examples
%    [flag, nomFicAdoc, ListSignals, repExport] = Adoc.Params.SurveyReport_Navigation(repExport)
%
% Author : JMA
% See also Authors

function [flag, nomFicAdoc, ListSignals, repExport] = SurveyReport_Navigation(repExport) 

persistent persistent_subListSignals

ListSignals = [];

if isempty(persistent_subListSignals)
    subListSignals = 1:16;
else
    subListSignals = persistent_subListSignals;
end

%% Nom du survey

nomFicCompens = fullfile(repExport, '*.adoc');
[flag, nomFicAdoc] = my_uigetfile(nomFicCompens, 'Survey Report file name');
if ~flag
    return
end
repExport = fileparts(nomFicAdoc);

%% S�lection des signaux

ListSignals = ALL.getKongsbergListSignals;
[subListSignals, flag] = my_listdlgMultiple('List of signals to be displayed over the navigation', ListSignals, 'InitialValue', subListSignals);
if ~flag
    return
end
ListSignals = ListSignals(subListSignals);
persistent_subListSignals = subListSignals;
