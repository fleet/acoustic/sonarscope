% Description
%   Params for SurveyReport_PlotImageSignals
%
% Syntax
%    [flag, nomFicAdoc, indexConfigs, repExport] = Adoc.Params.SurveyReport_PlotImageSignals(repExport)
%
% Intput Arguments
%   repExport : 
%
% Output Arguments
%   flag         : 
%   nomFicAdoc   : 
%   indexConfigs : 
%   repImport    : 
%
% Examples
%    [flag, nomFicAdoc, indexConfigs, repExport] = Adoc.Params.SurveyReport_PlotImageSignals(repExport)
%
% Author : JMA
% See also Authors


function [flag, nomFicAdoc, indexConfigs, repExport] = SurveyReport_PlotImageSignals(repExport)

indexConfigs = [];

%% Nom du survey

nomFiltre = fullfile(repExport, '*.adoc');
[flag, nomFicAdoc] = my_uigetfile(nomFiltre, 'Survey Report file name');
if ~flag
    return
end
repExport = fileparts(nomFicAdoc);

%% indexConfigs

str{1} = 'Bathy and Attitude';
str{2} = 'Reflectivity and Modes';
str{3} = 'Reflectivity and Specular';
str{4} = 'Reflectivity and Pulse length';
str{5} = 'Reflectivity and Absorption coefficient';
[indexConfigs, flag] = my_listdlgMultiple('Selection', str, 'InitialValue', 1:5);
if ~flag
    return
end
