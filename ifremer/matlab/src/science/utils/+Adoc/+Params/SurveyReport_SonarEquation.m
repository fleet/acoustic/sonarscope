% Description
%   Params for SurveyReport_PlotImageSignals
%
% Syntax
%    [flag, nomFicAdoc, NomFicMNT, flagCreateCSV, repImport, repExport] = Adoc.Params.SurveyReport_SonarEquation(repImport, repExport)
%
% Intput Arguments
%   repImport : 
%   repExport : 
%
% Output Arguments
%   flag          : 
%   nomFicAdoc    : 
%   NomFicMNT     : 
%   flagCreateCSV : 
%   repImport     : 
%   repExport     : 
%
% Examples
%    [flag, nomFicAdoc, NomFicMNT, flagCreateCSV, repImport, repExport] = Adoc.Params.SurveyReport_SonarEquation(repImport, repExport)
%
% Author : JMA
% See also Authors


function [flag, nomFicAdoc, NomFicMNT, flagCreateCSV, repImport, repExport] = SurveyReport_SonarEquation(repImport, repExport)

NomFicMNT     = [];
flagCreateCSV = [];

%% Nom du survey

nomFicCompens = fullfile(repExport, '*.adoc');
[flag, nomFicAdoc] = my_uigetfile(nomFicCompens, 'Survey Report file name');
if ~flag
    return
end
repExport = fileparts(nomFicAdoc);

%% MNT de r�f�rence

[flag, NomFicMNT, repImport] = uiSelectFiles('ExtensionFiles', {'.tif'; '.ers'; '.bag'}, 'AllFilters', 1, ...
    'RepDefaut', repImport, 'ChaineExclue', 'SunColor', 'Entete', 'Select the reference DTM from which insonified area and incidence angles will be computed');
if ~flag
    return
end
NomFicMNT = NomFicMNT{1};

%% Ecriture du fichier ASCII

[rep, flag] = my_questdlg('Create the .csv file for BS intercomparison project ?', 'Init', 2);
if ~flag
    return
end
flagCreateCSV = (rep == 1);
