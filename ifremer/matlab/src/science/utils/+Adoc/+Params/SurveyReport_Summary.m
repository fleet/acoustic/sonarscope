% Description
%   SurveyReport_Summary
%
% Syntax
%    [flag, nomFicAdoc, repExport] = Adoc.Params.SurveyReport_Summary(repExport)
%
% Intput Arguments
%   repExport : 
%
% Output Arguments
%   flag        : 
%   nomFicAdoc  : 
%   repExport   : 
%
% Examples
%    [flag, nomFicAdoc, repExport] = Adoc.Params.SurveyReport_Summary(repExport)
%
% Author : JMA
% See also Authors


function [flag, nomFicAdoc, repExport] = SurveyReport_Summary(repExport)

%% Nom du survey

nomFicCompens = fullfile(repExport, '*.adoc');
[flag, nomFicAdoc] = my_uigetfile(nomFicCompens, 'Survey Report file name');
if ~flag
    return
end
repExport = fileparts(nomFicAdoc);

flag = 1;
