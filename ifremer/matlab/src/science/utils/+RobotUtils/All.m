classdef All
    % Collection of static functions TODO
    %
    % Authors  : JMA Authors
    % See also : TODO
    %
    % Reference pages in Help browser
    %   <a href="matlab:doc XMLBinUtils" >XMLBinUtils</a>
    %  -------------------------------------------------------------------------------
    
    methods (Static)
        
        % TODO :
        %         logFileId = getLogFileId;
        %         msg = sprintf('Begin %s', nomFicAll{k1});
        %         logFileId.info('RobotUtils.All', msg);
        
        
        function launchProcess(paramsGeneraux, paramsWC, paramsDTM, paramsMOS, flagParallelTbx, varargin)
            if flagParallelTbx
                % Mauvaise id�e si survey report mis en place ?
                RobotUtils.All.launchProcessWithParalelTbx(paramsGeneraux, paramsWC, paramsDTM, paramsMOS);
            else
                RobotUtils.All.launchProcessWithoutParalelTbx(paramsGeneraux, paramsWC, paramsDTM, paramsMOS);
            end
        end
        
        
        function launchProcessWithoutParalelTbx(paramsGeneraux, paramsWC, paramsDTM, paramsMOS, varargin)
            
            % paramsGeneraux.nomDirIn = '\\md-nas\missioncourante\EQUIPEMENTS\EM122\DONNEES\CARTOGRAPHIE';
            % paramsGeneraux.nomDirOut = 'E:\MAYOBS15\EM122\TransitA';
            % paramsGeneraux.PreProcessSeabedimage = 0;
            % paramsGeneraux.nomDirNav = 'E:\MAYOBS15\EM122\TransitA\Navigation';
            % paramsGeneraux.flagGoogleEarth = 1;
            % paramsWC.nomDirOut = 'E:\MAYOBS15\EM122\TransitA\WC-DepthAcrossDist';
            % paramsWC.typeAlgo  = 2;
            % paramsWC.CLimRaw   = [-64 10];
            % paramsDTM.nomDir = fullfile(nomDirOut, 'DTM-MOS');
            % paramsDTM.TideFile = 'E:\MAYOBS15\EM122\TransitA\MAYOBS15.ttb';
            % paramsMOS.nomFicCompensationCurves = {'E:\MAYOBS15\EM122\TransitA\Mode_4_Swath_Double_Signal_CW_CompensSingle_Whole_image_on_TransitA_0027_20201004_151433_EM122_Marion_Dufresne_Clean_1_Reflectivity_BeamPointingAngle_TxBeamIndexSwath.mat'};
            % paramsMOS.ReflecVideoInverse = 1;
            % RobotUtils.All.createAndExportWCD(paramsGeneraux, paramsWC, paramsDTM, paramsMOS)
            
            nomDirIn              = paramsGeneraux.nomDirIn;
            nomDirOut             = paramsGeneraux.nomDirOut;
            PreProcessSeabedimage = paramsGeneraux.PreProcessSeabedimage;
            nomDirNav             = paramsGeneraux.nomDirNav;
            flagGoogleEarth       = paramsGeneraux.flagGoogleEarth;
            FirstFileNumber       = paramsGeneraux.FirstFileNumber;
            NbPendingFiles = 1; % A modifier apr�s validation pour S7K
%             NbPendingFiles        = paramsGeneraux.NbPendingFiles;
            
            Fig1               = [];
            nbSecondes         = 5;
            flagMessageWaiting = 1;
            listNomFicForWCDProcessing = {};
            
            %% D�finition de la carto
            
            Carto = RobotUtils.Common.DefineCarto(nomDirOut);
            
            %% Test si nomDirOut identique � nomDirIn
            
            % if strcmp(nomDirIn, nomDirOut)
            %     str1 = 'Le r�pertoire de travail ne peut pas �tre le m�me que le r�pertoire de d�pos des fichiers.';
            %     str2 = 'The two directories cannot be the same.';
            %     my_warndlg(Lang(str1,str2), 1);
            %     return
            % end
            SameDirectory = strcmp(nomDirIn, nomDirOut);
            
            %% Cr�ation du r�pertoire nomDirIn s'il n'existe pas
            
            if ~SameDirectory && ~exist(nomDirOut, 'dir')
                flag = mkdir(nomDirOut);
                if ~flag
                    RobotUtils.Common.messageMkdirFailed(nomDirOut);
                    return
                end
            end
            
            %% Etablissement de la liste des fichiers d�j� trait�s
            
            listeDejaTraites = {};
            
            %% Lancement de la surveillance du r�pertoire de nomDirIn
            
            [hwControl, TagControlWindow] = RobotUtils.Common.createShutdownControlWindow(nomDirIn, nomDirOut);
            RobotUtils.Common.printStart(nomDirIn);
            RobotUtils.Common.printWaiting(TagControlWindow);
            
            while ishandle(hwControl)
                
                %% Lecture de la liste des fichiers qui ont �t� d�plac�s
                
                [listeFichiersDeplaces, nomFicInfo] = RobotUtils.Common.readListOfMovedFiles(nomDirIn, nomDirOut, '.all');
                
                %% Lecture des fichiers .all pr�sents sur nomDirIn
                
                listeFic = listeFicOnDir(nomDirIn, '.all');
                for k=1:length(listeFic)
                    listeFic{k} = windows_replaceVolumeLetter(listeFic{k});
                end
                
                %% Etablissement de la liste des fichiers qui sont sur nomDirIn mais qui ne sont pas sur nomDirOut
                
                listeAutreTemp = listeFicOnDir(nomDirIn, '.all', 'Filtre', '9999_');
                for k=1:length(listeAutreTemp)
                    listeAutreTemp{k} = windows_replaceVolumeLetter(listeAutreTemp{k});
                end
                nomFicTemp = fullfile(nomDirIn, '9999.all');
                nomFicTemp = windows_replaceVolumeLetter(nomFicTemp);
                listeFic = setdiff(listeFic, [listeDejaTraites; listeFichiersDeplaces; {nomFicTemp}; listeAutreTemp]);
                
                if FirstFileNumber ~=  0
                    for k=length(listeFic):-1:1
                        [~, nomFicSeul] = fileparts(listeFic{k});
                        number = str2double(nomFicSeul(1:4));
                        if number < FirstFileNumber
                            listeFic(k) = [];
                        end
                    end
                end
                
                %% Traitement des fichiers qui n'ont pas encore �t� trait�s
                
                nbFicInLoop = length(listeFic) - NbPendingFiles;
                for k=1:nbFicInLoop
                    
                    %% Test si la fen�tre a �t� ferm�e
                    
                    [flag, hwControl, TagControlWindow] = RobotUtils.Common.checkShutdownControlFigure(hwControl, nomDirIn, nomDirOut, TagControlWindow);
                    if ~flag
                        break
                    end
                    
                    %% Copie du fichier individuel
                    
                    [flag, nomFicIn, nomFic, ~, nbSecondes, flagAnyProcessingHasBeenDone] = RobotUtils.Common.copieDataFile(listeFic{k}, k, nbFicInLoop, ...
                        3, SameDirectory, '.all', nomDirOut);
                    if ~flag
                        continue
                    end
                    
                    %% Lecture du fichier
                    
                    aKM = cl_simrad_all('nomFic', nomFic);
                    if isempty(aKM)
                        appendTxtFile(nomFicInfo, nomFic);
                        continue
                    end
                    
                    %% Import de la mar�e
                    
                    if ~isempty(paramsDTM.TideFile)
                        %             [flag, DataAttitude] = read_attitude_bin(aKM);
                        %             if flag
                        %                 if ~isfield(DataAttitude, 'Tide')
                        ALL_TideImport(aKM, nomFic, paramsDTM.TideFile, 'Tide', 'Mute', 1);
                        %                 end
                        %             end
                    end
                    
                    %% Traitement de l'imagerie
                    
                    if PreProcessSeabedimage == 1
                        flag = read_PingAcrossSample(aKM, 'ListeLayers', [], 'Carto', Carto);
                        if ~flag
                            return
                        end
                        flagAnyProcessingHasBeenDone = 1;
                    end
                    
                    %% Cr�ation d'un DTM et d'une mosa�que de r�flectivit�
                    
                    if ~isempty(paramsDTM.nomDir) || ~isempty(paramsMOS.nomDir)
                        flagProcessingHasBeenDone = RobotUtils.All.createAndExportDTM(nomFic, paramsDTM.nomDir, paramsMOS.nomDir, paramsMOS.ReflecVideoInverse, paramsMOS.nomFicCompensationCurves, flagGoogleEarth);
                        flagAnyProcessingHasBeenDone = flagAnyProcessingHasBeenDone || flagProcessingHasBeenDone;
                    end
                    
                    %% Affichage de la navigation dans Google Earth
                    
                    if flagAnyProcessingHasBeenDone
                        Fig1 = RobotUtils.All.plotAndExportNavigation(aKM, nomFic, nomDirNav, Fig1, flagGoogleEarth);
                    end
                    
                    %% Ajout du fichier dans la liste des fichiers d�j� trait�s
                    
                    listeDejaTraites{end+1,1} = nomFicIn; %#ok<AGROW>
                    
                    msg = sprintf('%s - End processing "%s"', datetime, nomFic);
                    RobotUtils.Common.printInfo(msg);
                    
                    %% Colonne d'eau non prioritaire
                    
                    listNomFicForWCDProcessing{end+1} = nomFic; %#ok<AGROW>
                    
                    flagMessageWaiting = 1;
                end
                
                %% Traitement de la colonne d'eau
                
                if (nbFicInLoop == 0) && ~isempty(listNomFicForWCDProcessing)
                    nomFic = listNomFicForWCDProcessing{1};
                    RobotUtils.All.WCDProcessing(nomFic, paramsWC);
                    listNomFicForWCDProcessing(1) = [];
                end
                
                %% Message d'attente
                
                if (nbFicInLoop == 0) && flagMessageWaiting
                    RobotUtils.Common.printWaiting(TagControlWindow);
                    flagMessageWaiting = 0;
                end
                
                %% Test si la fen�tre a �t� ferm�e
                
                [flag, hwControl, TagControlWindow] = RobotUtils.Common.checkShutdownControlFigure(hwControl, nomDirIn, nomDirOut, TagControlWindow);
                if flag
                    pause(nbSecondes)
                end
            end
            RobotUtils.Common.printShutdown;
        end
        
        
        function launchProcessWithParalelTbx(paramsGeneraux, paramsWC, paramsDTM, paramsMOS, varargin)
            
            % paramsGeneraux.nomDirIn = '\\md-nas\missioncourante\EQUIPEMENTS\EM122\DONNEES\CARTOGRAPHIE';
            % paramsGeneraux.nomDirOut = 'E:\MAYOBS15\EM122\TransitA';
            % paramsGeneraux.PreProcessSeabedimage = 0;
            % paramsGeneraux.nomDirNav = 'E:\MAYOBS15\EM122\TransitA\Navigation';
            % paramsGeneraux.flagGoogleEarth = 1;
            % paramsWC.nomDirOut = 'E:\MAYOBS15\EM122\TransitA\WC-DepthAcrossDist';
            % paramsWC.typeAlgo  = 2;
            % paramsWC.CLimRaw   = [-64 10];
            % paramsDTM.nomDir = fullfile(nomDirOut, 'DTM-MOS');
            % paramsDTM.TideFile = 'E:\MAYOBS15\EM122\TransitA\MAYOBS15.ttb';
            % paramsMOS.nomFicCompensationCurves = {'E:\MAYOBS15\EM122\TransitA\Mode_4_Swath_Double_Signal_CW_CompensSingle_Whole_image_on_TransitA_0027_20201004_151433_EM122_Marion_Dufresne_Clean_1_Reflectivity_BeamPointingAngle_TxBeamIndexSwath.mat'};
            % paramsMOS.ReflecVideoInverse = 1;
            % RobotUtils.All.createAndExportWCD(paramsGeneraux, paramsWC, paramsDTM, paramsMOS)
            
            useParallel = get_UseParallelTbx;
%             logFileId   = getLogFileId;
            
            nomDirIn              = paramsGeneraux.nomDirIn;
            nomDirOut             = paramsGeneraux.nomDirOut;
            PreProcessSeabedimage = paramsGeneraux.PreProcessSeabedimage;
            nomDirNav             = paramsGeneraux.nomDirNav;
            flagGoogleEarth       = paramsGeneraux.flagGoogleEarth;
            FirstFileNumber       = paramsGeneraux.FirstFileNumber;
            
            Fig1                       = [];
            nbSecondes                 = 1;
            flagMessageWaiting         = 1;
            listNomFicForWCDProcessing = {};
            
            %% D�finition de la carto
            
            Carto = RobotUtils.Common.DefineCarto(nomDirOut);
            
            %% Test si nomDirOut identique � nomDirIn
            
            % if strcmp(nomDirIn, nomDirOut)
            %     str1 = 'Le r�pertoire de travail ne peut pas �tre le m�me que le r�pertoire de d�pos des fichiers.';
            %     str2 = 'The two directories cannot be the same.';
            %     my_warndlg(Lang(str1,str2), 1);
            %     return
            % end
            SameDirectory = strcmp(nomDirIn, nomDirOut);
            
            %% Cr�ation du r�pertoire nomDirIn s'il n'existe pas
            
            if ~SameDirectory && ~exist(nomDirOut, 'dir')
                flag = mkdir(nomDirOut);
                if ~flag
                    RobotUtils.Common.messageMkdirFailed(nomDirOut);
                    return
                end
            end
            
            %% Etablissement de la liste des fichiers d�j� trait�s
            
            listeDejaTraites       = {};
            nomFicTideDejaImportee = {};
            
            %% Lancement de la surveillance du r�pertoire de nomDirIn
            
            [hwControl, TagControlWindow] = RobotUtils.Common.createShutdownControlWindow(nomDirIn, nomDirOut);
            RobotUtils.Common.printStart(nomDirIn);
            RobotUtils.Common.printWaiting(TagControlWindow);
            
            while ishandle(hwControl)
                
                %% Lecture de la liste des fichiers qui ont �t� d�plac�s
                
                [listeFichiersDeplaces, nomFicInfo] = RobotUtils.Common.readListOfMovedFiles(nomDirIn, nomDirOut, '.all');
                
                %% Lecture des fichiers .all pr�sents sur nomDirIn
                
                listeFic = listeFicOnDir(nomDirIn, '.all');
                for k=1:length(listeFic)
                    listeFic{k} = windows_replaceVolumeLetter(listeFic{k});
                end
                
                %% Etablissement de la liste des fichiers qui sont sur nomDirIn mais qui ne sont pas sur nomDirOut
                
                listeAutreTemp = listeFicOnDir(nomDirIn, '.all', 'Filtre', '9999_');
                for k=1:length(listeAutreTemp)
                    listeAutreTemp{k} = windows_replaceVolumeLetter(listeAutreTemp{k});
                end
                nomFicTemp = fullfile(nomDirIn, '9999.all');
                nomFicTemp = windows_replaceVolumeLetter(nomFicTemp);
                listeDejaTraites = unique(listeDejaTraites);
                listeFic = setdiff(listeFic, [listeDejaTraites; listeFichiersDeplaces; {nomFicTemp}; listeAutreTemp]);
                
                if FirstFileNumber ~=  0
                    for k=length(listeFic):-1:1
                        [~, nomFicSeul] = fileparts(listeFic{k});
                        number = str2double(nomFicSeul(1:4));
                        if number < FirstFileNumber
                            listeFic(k) = [];
                        end
                    end
                end
                
                %% Traitement des fichiers qui n'ont pas encore �t� trait�s
                
                listeFic = listeFic(1:end-1);
                nbFicInLoop = length(listeFic);
                if nbFicInLoop == 0
                    [flag, hwControl, TagControlWindow] = RobotUtils.Common.checkShutdownControlFigure(hwControl, nomDirIn, nomDirOut, TagControlWindow);
                    if flag
                        pause(nbSecondes)
                    else
                        break
                    end
                else
                    msg = sprintf('%s - Beginning the copy of .all &.wcd files', datetime);
                    RobotUtils.Common.printInfoCentered(msg);
                    hw = create_waitbar('Copy of new .all & .wcd files', 'N', nbFicInLoop);
                    for k=1:nbFicInLoop
                        my_waitbar(k, nbFicInLoop, hw)
                        
                        %% Test si la fen�tre a �t� ferm�e
                        
                        [flag, hwControl, TagControlWindow] = RobotUtils.Common.checkShutdownControlFigure(hwControl, nomDirIn, nomDirOut, TagControlWindow);
                        if ~flag
                            break
                        end
                        
                        %% Copie du fichier individuel
                        
                        [flag, ~, nomFic, ~, nbSecondes, flagAnyProcessingHasBeenDone] = RobotUtils.Common.copieDataFile(listeFic{k}, k, nbFicInLoop, ...
                            3, SameDirectory, '.all', nomDirOut);
                        if ~flag
                            continue
                        end
                        nomFicWork{k} = nomFic; %#ok<AGROW>
                    end
                    my_close(hw);
                    msg = sprintf('%s - End the copy of .all &.wcd files', datetime);
                    RobotUtils.Common.printInfoCentered(msg);
                    
                    [flag, hwControl, TagControlWindow] = RobotUtils.Common.checkShutdownControlFigure(hwControl, nomDirIn, nomDirOut, TagControlWindow);
                    if ~flag
                        break
                    end
                    
                    %% Lecture du fichier
                    
                    msg = sprintf('%s - Beginning of uncoding .all &.wcd files', datetime);
                    RobotUtils.Common.printInfoCentered(msg);
                    [flag, aKM] = ALL.checkFiles(nomFicWork, 'ShowProgressBar', 1);
                    if ~flag
                        RobotUtils.Common.printError('Exit because an error occured');
                        return
                    end
                    msg = sprintf('%s - End of uncoding .all &.wcd files', datetime);
                    RobotUtils.Common.printInfoCentered(msg);
                    
                    %% Import de la mar�e
                    
                    % TODO : modifier le code pour �viter de r�importer la
                    % mar�e � chaque d�but : v�rifier si une mar�e a d�j� �t�
                    % introduite. Cela permet de changer de fichier de mar�e au
                    % fur et � mesure de la campagne
                    
                    fprintf('%s - Beginning of tide import in cache directories\n', datetime);
                    if ~isempty(paramsDTM.TideFile)
                        nomFicWorkTide = setdiff(nomFicWork, nomFicTideDejaImportee);
                        ALL_TideImport(aKM, nomFicWorkTide, paramsDTM.TideFile, 'Tide', 'Mute', 1);
                        nomFicTideDejaImportee = [nomFicTideDejaImportee; nomFicWorkTide(:)]; %#ok<AGROW>
                    end
                    msg = sprintf('%s - End of tide import in cache directories', datetime);
                    RobotUtils.Common.printInfoCentered(msg);
                    
                    %% Traitement de l'imagerie
                    
                    if PreProcessSeabedimage == 1
                        fprintf('%s - Beginning of seabed full resolution preprocessing\n', datetime);
                        flagAnyProcessingHasBeenDoneHere = false(1,nbFicInLoop);
                        parfor (k=1:nbFicInLoop, useParallel)
                            flag = read_PingAcrossSample(aKM, 'ListeLayers', [], 'Carto', Carto); % 'logFileId', logFileId
                            if flag
                                flagAnyProcessingHasBeenDoneHere(k) = 1;
                            end
                        end
                        flagAnyProcessingHasBeenDoneHere = any(flagAnyProcessingHasBeenDoneHere);
                        flagAnyProcessingHasBeenDone = flagAnyProcessingHasBeenDone || flagAnyProcessingHasBeenDoneHere;
                        msg = sprintf('%s - End of seabed full resolution preprocessing', datetime);
                        RobotUtils.Common.printInfoCentered(msg);
                        if flagAnyProcessingHasBeenDone
                            flagMessageWaiting = 1;
                        end
                    end
                    
                    %% Cr�ation d'un DTM et d'une mosa�que de r�flectivit�
                    
                    msg = sprintf('%s - Beginning of DTM and reflectivity images', datetime);
                    RobotUtils.Common.printInfoCentered(msg);
                    hw = create_waitbar('Processing DTM & Reflectivity', 'N', nbFicInLoop);
                    for k=1:nbFicInLoop
                        my_waitbar(k, nbFicInLoop, hw)
                        if ~isempty(paramsDTM.nomDir) || ~isempty(paramsMOS.nomDir)
                            flagProcessingHasBeenDone = RobotUtils.All.createAndExportDTM(nomFicWork{k}, paramsDTM.nomDir, paramsMOS.nomDir, paramsMOS.ReflecVideoInverse, paramsMOS.nomFicCompensationCurves, flagGoogleEarth);
                            flagAnyProcessingHasBeenDone = flagAnyProcessingHasBeenDone || flagProcessingHasBeenDone;
                            
                            if flagAnyProcessingHasBeenDone
                                Fig1 = RobotUtils.All.plotAndExportNavigation(aKM(k), nomFicWork{k}, nomDirNav, Fig1, flagGoogleEarth);
                            end
                        end
                    end
                    my_close(hw);
                    msg = sprintf('%s - End of DTM and reflectivity images', datetime);
                    RobotUtils.Common.printInfoCentered(msg);
                    if flagAnyProcessingHasBeenDone
                        flagMessageWaiting = 1;
                    end
                    
                    %% Affichage de la navigation dans Google Earth
                    
                    for k=1:nbFicInLoop
                        listeDejaTraites{end+1,1} = nomFicWork{k}; %#ok<AGROW>
                        listNomFicForWCDProcessing{end+1} = nomFicWork{k}; %#ok<AGROW>
                    end
                    
                    listeDejaTraites = unique([listeDejaTraites; listeFic]);
                    
                    %% Test si la fen�tre a �t� ferm�e
                    
                    [flag, hwControl, TagControlWindow] = RobotUtils.Common.checkShutdownControlFigure(hwControl, nomDirIn, nomDirOut, TagControlWindow);
                    if flag
                        pause(nbSecondes)
                    else
                        break
                    end
                end
                
                %% Traitement de la colonne d'eau
                
                if (nbFicInLoop == 0) && ~isempty(listNomFicForWCDProcessing)
                    nbProc = min(length(listNomFicForWCDProcessing), max(1,useParallel));
                    parfor kproc=1:nbProc
                        nomFicProc = listNomFicForWCDProcessing{kproc};
                        RobotUtils.All.WCDProcessing(nomFicProc, paramsWC, 'Mute', 1);
                    end
                    for kproc=nbProc:-1:1
                        nomFic = listNomFicForWCDProcessing{kproc};
                        listNomFicForWCDProcessing(kproc) = [];
                        appendTxtFile(nomFicInfo, nomFic);
                    end
                end
                
                %% Test si la fen�tre de contr�le a �t� ferm�e
                
                [flag, hwControl, TagControlWindow] = RobotUtils.Common.checkShutdownControlFigure(hwControl, nomDirIn, nomDirOut, TagControlWindow);
                if flag
                    pause(nbSecondes)
                else
                    break
                end
                
                if flagMessageWaiting
                    RobotUtils.Common.printWaiting(TagControlWindow);
                    flagMessageWaiting = 0;
                end
            end
            RobotUtils.Common.printShutdown;
        end
        
        
        function flagProcessingHasBeenDone = createAndExportWCD(nomFicAll, paramsWC, varargin)
            
            global logFileId %#ok<GVMIS> %#ok<GVMIS> 
            
            [varargin, Mute]      = getPropertyValue(varargin, 'Mute',      0);
            [varargin, logFileId] = getPropertyValue(varargin, 'logFileId', logFileId); %#ok<ASGLU>
            
            flagProcessingHasBeenDone = 0;
            
            aKM = cl_simrad_all('nomFic', nomFicAll);
            flag = read_WaterColumn_bin(aKM, 'XMLOnly', 1);
            if ~flag
                return
            end
            
            nomDirOut        = paramsWC.nomDirOut;
            typeAlgo         = paramsWC.typeAlgo;
            CLimRaw          = paramsWC.CLimRaw;
            TypeOutputFormat = paramsWC.TypeOutputFormat;
                        
            if ~exist(nomDirOut, 'dir')
                mkdir(nomDirOut)
                if ~exist(nomDirOut, 'dir')
                    return
                end
            end
            
            [~, filename] = fileparts(nomFicAll);
            nomFicWCRaw  = [filename '_Raw_PolarEchograms.xml'];
            nomFicWCComp = [filename '_Comp_PolarEchograms.xml'];
            nomFicWCNc   = [filename '_PolarEchograms.g3d.nc'];
            Names.nomFicXmlRaw  = fullfile(nomDirOut, nomFicWCRaw);
            Names.nomFicXmlComp = fullfile(nomDirOut, nomFicWCComp);
            Names.nomFicNetcdf  = fullfile(nomDirOut, nomFicWCNc);
            if ~my_isdeployed % TODO : Le temps de confirmer que �a n'a pas d'effet de bord
                nomFicEchoes = [filename '_Echoes.xml'];
                Names.nomFicEchoes  = fullfile(nomDirOut, nomFicEchoes);
            end
            Names.nomFicKML     = [];
            Names.AreaName      = [];
            
            MaxImageSize = 1000;
            StepDisplay  = 0;
            
            if (exist(Names.nomFicXmlRaw, 'file') && exist(Names.nomFicXmlComp, 'file')) ...
                    || exist(Names.nomFicNetcdf, 'file')
                return
            end
            
            TypeWCOutput = 1;
            if TypeOutputFormat == 1
                ALL.Process.ComputeEchogramsXMLBin(aKM, TypeWCOutput, Names, ...
                    'StepDisplay', StepDisplay, ...
                    'CorrectionTVG', 30, 'CLimRaw', CLimRaw, ...
                    'TideCorrection', 1, 'typeAlgoWC', typeAlgo, ...
                    'skipAlreadyProcessedFiles', 1, ...
                    'MaxImageSize', MaxImageSize, 'Mute', Mute);
            else
                ALL.Process.ComputeEchogramsGlobe(aKM, TypeWCOutput, Names, ...
                    'StepDisplay', StepDisplay, ...
                    'CorrectionTVG', 30, 'CLimRaw', CLimRaw, ...
                    'TideCorrection', 1, 'typeAlgoWC', typeAlgo, ...
                    'skipAlreadyProcessedFiles', 1, ...
                    'MaxImageSize', MaxImageSize, 'Mute', Mute);
            end
            flagProcessingHasBeenDone = 1;
        end
        
        
        function flagProcessingHasBeenDone = createAndExportWCD_3DMatrix(nomFicAll, paramsWC, varargin)
            
            global logFileId %#ok<GVMIS> %#ok<GVMIS> 
            
            [varargin, Mute]      = getPropertyValue(varargin, 'Mute',      0);
            [varargin, logFileId] = getPropertyValue(varargin, 'logFileId', logFileId); %#ok<ASGLU>
            
            flagProcessingHasBeenDone = 0;

            aKM = cl_simrad_all('nomFic', nomFicAll);
            if isempty(aKM)
                return
            end
            
            nomDirOut        = paramsWC.NomDirOut_3DMatrix;
            TypeFileWCOutput = paramsWC.TypeOutputFormat_3DMatrix;
            typeAlgoWC       = paramsWC.typeAlgo;
            CLimRaw          = paramsWC.CLimRaw;
            CLimComp         = [-10 25];
            %             TypeOutputFormat = paramsWC.TypeOutputFormat;
            
            flag = RobotUtils.Common.mkdirIfNecessary(nomDirOut);
            if ~flag
                return
            end
            
            [~, filename] = fileparts(nomFicAll);
            nomFicWCRaw   = [filename '_WCCube_Raw_PolarEchograms.xml'];
            nomFicWCComp  = [filename '_WCCube_Comp_PolarEchograms.xml'];
            nomFicWCNc    = [filename '_WC3DMatrix.nc'];
            
            Names.nomFicXmlRaw  = fullfile(nomDirOut, nomFicWCRaw);
            Names.nomFicXmlComp = fullfile(nomDirOut, nomFicWCComp);
            Names.nomFicNetcdf  = fullfile(nomDirOut, nomFicWCNc);
            Names.nomFicKML     = [];
            Names.AreaName      = [];
            %             MaxImageSize = 1000;
            
            if (exist(Names.nomFicXmlRaw, 'file') && exist(Names.nomFicXmlComp, 'file')) ...
                    || exist(Names.nomFicNetcdf, 'file')
                return
            end
            
            skipAlreadyProcessedFiles = 0;
            MaskBeyondDetection = 0;
            
            TypeWCOutput       = 2; % 1 = echogrammes, 2 = 3DMatrix
            flagTideCorrection = 1;
            CorrectionTVG      = 30;
            flagProcessingHasBeenDone = ALL.Process.ComputeEchogramsXMLBin(aKM, TypeWCOutput, Names, ...
                'CorrectionTVG', CorrectionTVG, 'CLimRaw', CLimRaw, 'CLimComp', CLimComp, ...
                'TideCorrection', flagTideCorrection, 'typeAlgoWC', typeAlgoWC, ...
                'MaskBeyondDetection', MaskBeyondDetection, 'skipAlreadyProcessedFiles', skipAlreadyProcessedFiles, ...
                'Mute', Mute);
            
            if TypeFileWCOutput == 2 % Netcdf
                flag = WC_3DMatrix_XML2Netcdf(Names);
                if flag
                    flag = WC_3DMatrix_RemoveXMLBin(Names); %#ok<NASGU>
                end
            end
        end
        
        
        function flagProcessingHasBeenDone = createAndExportDTM(nomFic, nomDirDTM, nomDirMOS, ReflecVideoInverse, nomFicCompensationCurves, flagGoogleEarth)
            
            flagProcessingHasBeenDone = 0;
            
            [~, filename] = fileparts(nomFic);
            
            %% R�pertoires DTM
            
            flag = RobotUtils.Common.mkdirIfNecessary(nomDirDTM);
            if ~flag
                return
            end
            
            nomDirDTMGLOBE = fullfile(nomDirDTM, 'GLOBE');
            flag = RobotUtils.Common.mkdirIfNecessary(nomDirDTMGLOBE);
            if ~flag
                return
            end
            
            nomDirDTMGoogle = fullfile(nomDirDTM, 'Google');
            flag = RobotUtils.Common.mkdirIfNecessary(nomDirDTMGoogle);
            if ~flag
                return
            end
            
            nomDirDTMErs = fullfile(nomDirDTM, 'ERS');
            flag = RobotUtils.Common.mkdirIfNecessary(nomDirDTMErs);
            if ~flag
                return
            end
            
            %% R�pertoires MOS
            
            flag = RobotUtils.Common.mkdirIfNecessary(nomDirMOS);
            if ~flag
                return
            end
            
            nomDirMOSGLOBE = fullfile(nomDirMOS, 'GLOBE');
            flag = RobotUtils.Common.mkdirIfNecessary(nomDirMOSGLOBE);
            if ~flag
                return
            end
            
            nomDirMOSGoogle = fullfile(nomDirMOS, 'Google');
            flag = RobotUtils.Common.mkdirIfNecessary(nomDirMOSGoogle);
            if ~flag
                return
            end
            
            nomDirMOSErs = fullfile(nomDirMOS, 'ERS');
            flag = RobotUtils.Common.mkdirIfNecessary(nomDirMOSErs);
            if ~flag
                return
            end
            
            %% Initialisation communes � DTM et MOS
            
            InfoMos.TypeMos            = 2;
            InfoMos.TypeGrid           = 2;
            InfoMos.flagMasquageDual   = 0;
            InfoMos.LimitAngles        = [];
            InfoMos.gridSize           = [];
            InfoMos.skipExistingMosaic = 0;
            
            InfoMos.Import.MasqueActif  = 1;
            InfoMos.Import.SlotFilename = [];
            InfoMos.Import.SonarTime    = [];
            
            InfoMos.MasqueActif  = 1;
            InfoMos.SlotFilename = [];
            InfoMos.SonarTime    = [];
            
            InfoMos.Covering.Priority           = 1;
            InfoMos.Covering.SpecularLimitAngle = [];
            
            I0 = cl_image.empty;
            
            %% Cr�ation de la mosa�que de r�flectivit�
            
            nomFicMOS = fullfile(nomDirMOSErs,    [filename '_Reflectivity_LatLong.ers']);
            nomFicKmz = fullfile(nomDirMOSGoogle, [filename '_Reflectivity_LatLong.kmz']);
            if ~exist(nomFicMOS, 'file') || ~exist(nomFicKmz, 'file')
                if ~iscell(nomFicCompensationCurves)
                    nomFicCompensationCurves = {nomFicCompensationCurves};
                end
                InfoCompensationCurve.FileName = nomFicCompensationCurves;
                InfoCompensationCurve.ModeDependant = 1;
                InfoCompensationCurve.UseModel = 0;
                InfoCompensationCurve.PreserveMeanValueIfModel = 0;
                
                InfoMos.DirName = nomDirMOSErs;
                flag = SimradAllMosaiqueParProfil(I0, 1, nomFic, 0, 'ReflectivityFromSnippets', [5 5], 0, InfoCompensationCurve, InfoMos, 'Mute', 1);
                if flag
                    RobotUtils.Common.printExportedFilename(nomFicMOS)
                end
                
                if flag && exist(nomFicMOS, 'file')
                    [flag, b] = cl_image.import_ermapper(nomFicMOS);
                    if flag
                        flagProcessingHasBeenDone = 1;
                        b.CLim = '0.5%';
                        
                        nomLayer = [filename '_Reflectivity_texture_v2'];
                        nomFic3D = nomLayer;
                        flagExportTexture   = 1;
                        flagExportElevation = 0;
                        flag = export_3DViewer_InGeotifTiles(b, nomLayer, nomFic3D, nomDirMOSGLOBE, flagExportTexture, flagExportElevation);
                        if flag
                            RobotUtils.Common.printExportedFilename(nomFic3D)
                        end
                        
                        if ReflecVideoInverse
                            b.Video = 2;
                        end
                        
                        flag = export_GoogleEarth(b, 'nomFic', nomFicKmz);
                        if flag
                            RobotUtils.Common.printExportedFilename(nomFicKmz)
                        end
                        
                        if flagGoogleEarth
                            if flag
                                winopen(nomFicKmz)
                            end
                        else
                            imagesc(b);
                            drawnow
                        end
                    end
                end
            end
            
            %% Cr�ation du DTM
            
            nomFicDTM = fullfile(nomDirDTMErs,    [filename '_Bathymetry_LatLong.ers']);
            nomFicKmz = fullfile(nomDirDTMGoogle, [filename '_Bathymetry_LatLong.kmz']);
            
            if ~exist(nomFicDTM, 'file') || ~exist(nomFicKmz, 'file')
                InfoMos.DirName = nomDirDTMErs;
                flag = SimradAllMosaiqueParProfil(I0, 1, nomFic, 0, 'Depth+Draught-Heave', [5 5], 0, [], InfoMos, 'Mute', 1, 'TideCorrection', 1);
                if flag
                    RobotUtils.Common.printExportedFilename(nomFicMOS)
                end
                
                if flag && exist(nomFicDTM, 'file')
                    [flag, b] = cl_image.import_ermapper(nomFicDTM);
                    if flag
                        flagProcessingHasBeenDone = 1;
                        b.CLim = '0.5%';
                        b.ColormapIndex = 20;
                        
                        nomLayer = [filename '_bathy_elevation_v2'];
                        nomFic3D = [filename '_bathy'];
                        flagExportTexture   = 0;
                        flagExportElevation = 1;
                        export_3DViewer_InGeotifTiles(b, nomLayer, nomFic3D, nomDirDTMGLOBE, flagExportTexture, flagExportElevation);
                        if flag
                            RobotUtils.Common.printExportedFilename(nomFic3D)
                        end
                        
                        c = sunShadingGreatestSlope(b, 0.5);
                        flag = export_GoogleEarth(c, 'nomFic', nomFicKmz);
                        if flag
                            RobotUtils.Common.printExportedFilename(nomFicKmz)
                        end
                        
                        if flagGoogleEarth
                            if flag
                                winopen(nomFicKmz)
                            end
                        else
                            imagesc(c);
                            drawnow
                        end
                    end
                end
            end
        end
        
        
        function Fig1 = plotAndExportNavigation(aKM, nomFic, nomDirNav, Fig1, flagGoogleEarth)
            
            try
                Colororder = colororder;
            catch % Car la fonction colororder n'est pas reconnue en standalone !!!
                Colororder = [0   0.447000000000000   0.741000000000000
                    0.850000000000000   0.325000000000000   0.098000000000000
                    0.929000000000000   0.694000000000000   0.125000000000000
                    0.494000000000000   0.184000000000000   0.556000000000000
                    0.466000000000000   0.674000000000000   0.188000000000000
                    0.301000000000000   0.745000000000000   0.933000000000000
                    0.635000000000000   0.078000000000000   0.184000000000000];
            end
            nbColors = size(Colororder, 1);
            [~, nomFicSeul] = fileparts(nomFic);
            LineName = ['Nav-' nomFicSeul];
            
            %% Cr�ation du r�pertoire Navigation
            
            flag = RobotUtils.Common.mkdirIfNecessary(nomDirNav);
            if ~flag
                return
            end
            
            %% Cr�ation du r�pertoire Google
            
            nomDirNavGoogle = fullfile(nomDirNav, 'Google');
            flag = RobotUtils.Common.mkdirIfNecessary(nomDirNavGoogle);
            if ~flag
                return
            end
            nomFicGE = fullfile(nomDirNavGoogle, [LineName '.kmz']);
            flagExistGE = exist(nomFicGE, 'file');
            
            %% Cr�ation du r�pertoire GLOBE
            
            nomDirNavGlobe = fullfile(nomDirNav, 'GLOBE');
            flag = RobotUtils.Common.mkdirIfNecessary(nomDirNavGlobe);
            if ~flag
                return
            end
            NomFicXml = fullfile(nomDirNavGlobe, [LineName '.xml']);
            flagExistXml = exist(NomFicXml, 'file');
            
            %% Lecture de la navigation
            
            % TODO : je crois qu'il y a une fonction qui fait �a directement, mais la quelle ?
            
            if ~flagExistGE || ~flagExistXml
                Depth = get_Layer(aKM, 'Depth+Draught-Heave', 'CartoAuto', 1, 'Mute', 1);
                
                Lat = get(Depth, 'SonarFishLatitude');
                Lon = get(Depth, 'SonarFishLongitude');
                Datetime = get(Depth, 'Datetime');
                
                kCoul = 1 + mod(str2double(nomFicSeul(1:4)), nbColors);
                Color = Colororder(kCoul,:);
                
                Label = [char(Datetime(1)) ' - ' char(Datetime(end))];
                
                %% Cr�ation de la navigation dans Google Earth
                
                flagNewFile = RobotUtils.Common.plotNavGoogleGlobe(Lat, Lon, Color, LineName, Label, ...
                    flagExistGE, nomFicGE, flagGoogleEarth, ...
                    flagExistXml, Datetime, NomFicXml);
                if flagNewFile
                    Fig1 = ALL_PlotNav(aKM, 'Fig', Fig1);
                end
            end
        end
        
        
        function WCDProcessing(nomFic, paramsWC, varargin)
            
            [varargin, Mute] = getPropertyValue(varargin, 'Mute', 0); %#ok<ASGLU>
            
            [~, nomFicSeul] = fileparts(nomFic);
            
            %% Cr�ation des �chogrammes polaires
            
            if ~isempty(paramsWC) && ~isempty(paramsWC.nomDirOut)
                RobotUtils.All.createAndExportWCD(nomFic, paramsWC, 'Mute', Mute);
            end
            
            %% Cr�ation des �chogrammes polaires au format 3D-Matrix
            
            nomFicNetcdf = fullfile(paramsWC.NomDirOut_3DMatrix, [nomFicSeul '_WCCube_PolarEchograms.nc']);
            if ~isempty(paramsWC) && ~isempty(paramsWC.NomDirOut_3DMatrix) ...
                    && ~exist(nomFicNetcdf, 'file')
                RobotUtils.All.createAndExportWCD_3DMatrix(nomFic, paramsWC, 'Mute', Mute);
            end
            
            %% Cr�ation des WC-AlongNav
            
            if ~isempty(paramsWC) && ~isempty(paramsWC.NomDirOut_AlongNav)
                RobotUtils.Common.createAndExportWCD_AlongNav(nomFic, paramsWC, 'Mute', Mute);
            end
        end
    end
end