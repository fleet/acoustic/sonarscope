classdef Common
    % Collection of static functions TODO
    %
    % Authors  : JMA Authors
    % See also : TODO
    %
    % Reference pages in Help browser
    %   <a href="matlab:doc XMLBinUtils" >XMLBinUtils</a>
    %  -------------------------------------------------------------------------------
    
    methods (Static)
        
        function printInfo(msg)
            logFileId = getLogFileId;
            logFileId.info('S7k', msg);
        end
        
        
        function printInfoCentered(str)
            n = floor((120 - length(str)) / 2);
            tirets = repmat('-', 1, n);
            fprintf('%s %s %s\n\n', tirets, str, tirets);
        end
        
        
        function printStart(nomDirIn)
            msg = sprintf('Checking "%s"', nomDirIn);
            logFileId = getLogFileId;
            logFileId.info('S7k', msg);
        end
        
        
        function printShutdown
            logFileId = getLogFileId;
            logFileId.info('S7k', 'Robot shutdown');
        end
        
        
        function printError(msg)
            logFileId = getLogFileId;
            logFileId.info('S7k', msg);
        end
        
        
        function printExportedFilename(nomFic, varargin)
            [varargin, Success] = getPropertyValue(varargin, 'Success', 1); %#ok<ASGLU>
            if Success
                msg = sprintf('  --> Exported : %s', nomFic);
            else
                msg = sprintf('  --> Could not be exported : %s', nomFic);
            end
            logFileId = getLogFileId;
            logFileId.info('S7k', msg);
        end
        
        
        function printWaiting(TagControlWindow)
            msg = sprintf('Waiting for new files -  Close the Robot control window "%s" to shutdown the robot', TagControlWindow);
            RobotUtils.Common.printInfoCentered(msg);
        end
        
        
        function messageMkdirFailed(nomDirOut)
            str1 = sprintf('Il est impossible de cr�er le r�pertoire "%s"', nomDirOut);
            str2 = sprintf('It is impossible to create directory "%s"',     nomDirOut);
            my_warndlg(Lang(str1,str2), 1);
        end
        
        
        function messageCopyfileFailed(nomFicIn, nomFicOut)
            str1 = sprintf('Il est impossible de copier "%s" sur "%s"', nomFicIn, nomFicOut);
            str2 = sprintf('It is impossible to copy "%s" on "%s"',     nomFicIn, nomFicOut);
            my_warndlg(Lang(str1,str2), 1);
        end
        
        
        function [flag, hwControl, TagControlWindow] = checkShutdownControlFigure(hwControl, nomDirIn, nomDirOut, TagControlWindow)
            if ishandle(hwControl)
                flag = 1;
            else
                str1Exit = sprintf('La fen�tre de surveillance des fichiers a �t� ferm�e.\nConfirmez-vous que vous d�sirez stopper le robot ?');
                str2Exit = sprintf('The window checking the files has been closed.\nDo you confirm you want to stop the robot ?');
                [rep, flag] = my_questdlg(Lang(str1Exit,str2Exit));
                if flag && (rep == 2)
                    [hwControl, TagControlWindow] = RobotUtils.Common.createShutdownControlWindow(nomDirIn, nomDirOut);
                    flag = 1;
                else
                    flag = 0;
                end
            end
        end
        
        
        function [hwControl, TagControlWindow] = createShutdownControlWindow(nomDirIn, nomDirOut)
            str1 = sprintf('Cette fen�tre surveille les fichiers .s7k d�pos�s sur "%s", les copies sur "%s" et les pr�trait�s.\n\nLa fermeture de cette fen�tre (bouton OK ) stoppera le robot.', ...
                nomDirIn, nomDirOut);
            str2 = sprintf('This window is checking the .s7k file posted on "%s", it copies them on %s" and process them.\n\nWARNING : Pressing on the OK button will  stop the robot.', ...
                nomDirIn, nomDirOut);
            MessageFenetre = Lang(str1,str2);
            t = datetime;
            t.Format='yyyy-MM-dd HH:mm:ss';
            TagControlWindow = char(t);
            FigName = sprintf('Shutdown control window : %s', TagControlWindow);
            hwControl = my_warndlg(MessageFenetre, 0, 'displayItEvenIfSSc', 1, 'FigName', FigName);
        end
        
        
        function moveGeneric(filename, In, Out)
            
            flag = RobotUtils.Common.mkdirIfNecessary(Out);
            if ~flag
                return
            end
            
            %% Recherche des fichiers .nc
            
            nomFicStar = fullfile(In, ['*' filename '*']);
            strCmd = sprintf('movefile(''%s'', ''%s''', nomFicStar, Out);
            [flag, msg] = movefile(nomFicStar, Out, 'f');
            if flag == 1
                try
                    delete(nomFicStar)
                    fprintf('%s succeded !\n', strCmd);
                catch ME
                    fprintf('%s failed !\n%s\n', strCmd, ME.getReport);
                end
            else
                fprintf('%s failed !\n%s\n', strCmd, msg);
            end
        end
        
        
        function flag = mkdirIfNecessary(nomDir)
            flag = 1;
            if ~exist(nomDir, 'dir')
                mkdir(nomDir)
                if ~exist(nomDir, 'dir')
                    RobotUtils.Common.messageMkdirFailed(nomDir);
                    flag = 0;
                    return
                end
            end
        end
        
        
        function  Carto = DefineCarto(nomDirOut)
            C0 = cl_carto([]);
            nomFicCartoDefault = fullfile(nomDirOut, 'Carto_Directory.def');
            if exist(nomFicCartoDefault, 'file')
                nomFicCartoDefault = strrep(nomFicCartoDefault, '.def', '.xml');
                Carto = import_xml(C0, nomFicCartoDefault);
            else
                while ~exist(nomFicCartoDefault, 'file')
                    [~, Carto] = define(C0, nomDirOut, [], []);
                    if ~exist(nomFicCartoDefault, 'file')
                        str = sprintf('Sorry, you have to export the cartographic parameters into %s', nomFicCartoDefault);
                        my_warndlg(str, 1);
                    end
                end
            end
        end
        
        
        function writeConfigurationFile(nomFicConfig, paramsGeneraux, paramsWC, paramsDTM, paramsMOS, flagParallelTbx)
            paramsRobot.paramsGeneraux  = paramsGeneraux;
            paramsRobot.paramsWC        = paramsWC;
            paramsRobot.paramsDTM       = paramsDTM;
            paramsRobot.paramsMOS       = paramsMOS;
            paramsRobot.flagParallelTbx = flagParallelTbx;
            str = jsonencode(paramsRobot, 'PrettyPrint', true);
            fid = fopen(nomFicConfig, 'w+');
            fprintf(fid, '%s', str);
            fclose(fid);
        end
        
        
        function [paramsGeneraux, paramsWC, paramsDTM, paramsMOS, flagParallelTbx] = readConfigurationFile(nomFicConfig)
%             fid = fopen(nomFicConfig, 'r');
%             str = fgetl(fid);
%             fclose(fid);
            str = readlines(nomFicConfig);
            str = char(str);
            str = str';
            str = str(:)';
            paramsRobot = jsondecode(str);
            paramsGeneraux  = paramsRobot.paramsGeneraux;
            paramsWC        = paramsRobot.paramsWC;
            paramsDTM       = paramsRobot.paramsDTM;
            paramsMOS       = paramsRobot.paramsMOS;
            flagParallelTbx = paramsRobot.flagParallelTbx;
        end
        
        
        function moveFiles(listFileNames, nomDirOut, DirNames)
            
            nomDirIn = fileparts(listFileNames{1});
            nomFicInfo = fullfile(nomDirIn, 'ListOfMovedFiles.txt');
            if ~exist(nomFicInfo, 'file')
                fid = fopen(nomFicInfo, 'w');
                fclose(fid);
            end
            
            N = length(listFileNames);
            str1 = 'D�placement des fichiers';
            str2 = 'Move files';
            hw = create_waitbar(Lang(str1,str2), 'N', N);
            for k=1:N
                my_waitbar(k, N, hw)
                
                [~, filename] = fileparts(listFileNames{k});
                
                %% D�placement du MNT
                
                RobotUtils.Common.moveGeneric(filename, fullfile(DirNames.DTMIn, 'ERS'),    fullfile(DirNames.DTMOut, 'ERS'))
                RobotUtils.Common.moveGeneric(filename, fullfile(DirNames.DTMIn, 'Google'), fullfile(DirNames.DTMOut, 'Google'))
                RobotUtils.Common.moveGeneric(filename, fullfile(DirNames.DTMIn, 'GLOBE'),  fullfile(DirNames.DTMOut, 'GLOBE'))
                
                %% D�placement de l'image de r�flectivit�
                
                RobotUtils.Common.moveGeneric(filename, fullfile(DirNames.MOSIn, 'ERS'),    fullfile(DirNames.MOSOut, 'ERS'))
                RobotUtils.Common.moveGeneric(filename, fullfile(DirNames.MOSIn, 'Google'), fullfile(DirNames.MOSOut, 'Google'))
                RobotUtils.Common.moveGeneric(filename, fullfile(DirNames.MOSIn, 'GLOBE'),  fullfile(DirNames.MOSOut, 'GLOBE'))
                
                %% D�placement de la navigation
                
                RobotUtils.Common.moveGeneric(filename, fullfile(DirNames.NavIn, 'Google'), fullfile(DirNames.NavOut, 'Google'))
                RobotUtils.Common.moveGeneric(filename, fullfile(DirNames.NavIn, 'GLOBE'),  fullfile(DirNames.NavOut, 'GLOBE'))
                
                %% D�placement de la colonne d'eau
                
                RobotUtils.Common.moveGeneric(filename, DirNames.WCIn, DirNames.WCOut)
                
                %% D�placement de la WC-3DMatrix
                
                RobotUtils.Common.moveGeneric(filename, DirNames.WC3DMatrixIn, DirNames.WC3DMatrixOut)
                
                %% D�placement de la WC-AlongDist
                
                RobotUtils.Common.moveGeneric(filename, DirNames.WCAlongNavIn, DirNames.WCAlongNavOut)
                
                %% D�placement du .s7k
                
                RobotUtils.Common.moveGeneric(filename, nomDirIn, nomDirOut)
                RobotUtils.Common.moveGeneric(filename, fullfile(nomDirIn, 'SonarScope'), fullfile(nomDirOut, 'SonarScope'))
                %  ALL.Process.MoveFiles(listFileNames{k}, nomDirOut)
                
                %% Append du fichier d'info
                
                appendTxtFile(nomFicInfo, listFileNames{k});
                
            end
            my_close(hw, 'MsgEnd')
        end
        
        
        function [listeFichiersDeplaces, nomFicInfo] = readListOfMovedFiles(nomDirIn, nomDirOut, Ext)
            % Lecture de la liste des fichiers qui ont �t� d�plac�s
            
            nomFicInfo = fullfile(nomDirOut, 'ListOfMovedFiles.txt');
            if exist(nomFicInfo, 'file')
                listeFichiersDeplaces = {};
                fid = fopen(nomFicInfo);
                while 1
                    tline = fgetl(fid);
                    if ~ischar(tline)
                        break
                    end
                    [~, filename] = fileparts(tline);
                    nomFic = fullfile(nomDirIn, [filename Ext]);
                    listeFichiersDeplaces{end+1,1} = windows_replaceVolumeLetter(nomFic); %#ok<AGROW>
                end
                fclose(fid);
            else
                listeFichiersDeplaces = {};
            end
        end
        
        
        function flagProcessingHasBeenDone = createAndExportWCD_AlongNav(nomFicAll, paramsWC, varargin)
            
            global logFileId %#ok<GVMIS> %#ok<GVMIS> 
            
            [varargin, Mute]      = getPropertyValue(varargin, 'Mute',      0);
            [varargin, logFileId] = getPropertyValue(varargin, 'logFileId', logFileId); %#ok<ASGLU>
            
            flagProcessingHasBeenDone = 0;
            
            nomDirOut = paramsWC.NomDirOut_AlongNav;
            
            if ~exist(nomDirOut, 'dir')
                flag = RobotUtils.Common.mkdirIfNecessary(nomDirOut);
                if ~flag
                    return
                end
            end
            
            [~, filename] = fileparts(nomFicAll);
            nomFicWCRaw  = [filename '_WCCube_Raw_PolarEchograms.xml'];
            nomFicWCComp = [filename '_WCCube_Comp_PolarEchograms.xml'];
            %             nomFicWCNc   = [filename '_WC3DMatrix.nc'];
            nomFicWCNc   = [filename '_WCCube_PolarEchograms.nc'];
            
            nomFicXmlRaw  = fullfile(paramsWC.NomDirOut_3DMatrix, nomFicWCRaw);
            nomFicXmlComp = fullfile(paramsWC.NomDirOut_3DMatrix, nomFicWCComp);
            nomFicNetcdf  = fullfile(paramsWC.NomDirOut_3DMatrix, nomFicWCNc);
            
            if exist(nomFicXmlRaw, 'file') && exist(nomFicXmlComp, 'file')
                nomFic3DMatrix{1} = nomFicXmlRaw;
                nomFic3DMatrix{2} = nomFicXmlComp;
            elseif exist(nomFicNetcdf, 'file')
                nomFic3DMatrix{1} = nomFicNetcdf;
            else
                return
            end
            
            [~, filename] = fileparts(nomFicAll);
            nomFicWCRaw  = [filename '_WCSliceRaw.xml'];
            nomFicWCComp = [filename '_WCSliceComp.xml'];
            nomFicWCNc   = [filename '_WCSlice.nc'];
            
            Names.nomFicXmlRaw  = fullfile(nomDirOut, nomFicWCRaw);
            Names.nomFicXmlComp = fullfile(nomDirOut, nomFicWCComp);
            Names.nomFicNetcdf  = fullfile(nomDirOut, nomFicWCNc);
            
            if (exist(Names.nomFicXmlRaw, 'file') && exist(Names.nomFicXmlComp, 'file')) ...
                    || exist(Names.nomFicNetcdf, 'file')
                return
            end
            
            type3DFileFormat = paramsWC.AlongNavTypeOutputFormat;
            
            % [~, ~, Ext] = fileparts(nomFic3DMatrix{1});
            if (type3DFileFormat == 1) % && ~strcmp(Ext, '.nc') % Format de sortie en XML-Bin mais on interdit si format d'entr�e en Netcdf
                WC_SlicesAlongNavigation(nomFic3DMatrix, nomDirOut, 'Step', paramsWC.AlongNavStep, 'Mute', Mute);
            else % Format de sortie en Netcdf
                WC_SlicesAlongNavFlyTexture(nomFic3DMatrix, nomDirOut, 'Step', paramsWC.AlongNavStep, ...
                    'ncFileName', Names.nomFicNetcdf, 'Mute', Mute);
            end
        end
        
        
        function flagNewFile = plotNavGoogleGlobe(Lat, Lon, Color, LineName, Label, ...
                flagExistGE, nomFicGE, flagGoogleEarth, ...
                flagExistXml, Datetime, NomFicXml)
            
            %% Cr�ation de la navigation dans Google Earth
            
            flagNewFile = 0;
            if ~flagExistGE
                plot_navigationOnGoogleEarth(Lat, Lon, Color, [LineName '.kmz'], Label, nomFicGE, ...
                    'Display', flagGoogleEarth, 'PinsVisibility', 0);
                flagNewFile = 1;
                RobotUtils.Common.printExportedFilename(nomFicGE)
            end
            
            %% Cr�ation du fichier de navigation pour GLOBE
            
            if ~flagExistXml
                Immersion = [];
                flag = plot_navigationExportXML({Lat}, {Lon}, {Datetime}, {Color}, {LineName}, {Label}, NomFicXml, 'Immersion', Immersion);
                if flag
                    flagNewFile = 1;
                    RobotUtils.Common.printExportedFilename(NomFicXml)
                end
            end
        end
        
                            
        function [flag, nomFicIn, nomFic, nomFicSeul, nbSecondes, flagAnyProcessingHasBeenDone] ...
                = copieDataFile(listeFic, k, nbFicInLoop, nbFicWait, SameDirectory, Ext, nomDirOut)
            
            flag = 1;
            
            if k < (nbFicInLoop - nbFicWait)
                nbSecondes = 0;
            else
                nbSecondes = 10;
            end
            flagAnyProcessingHasBeenDone = 0;
            
            nomFicIn = windows_replaceVolumeLetter(listeFic);
            [~, nomFicSeul] = fileparts(nomFicIn);
            nomFic = fullfile(nomDirOut, [nomFicSeul Ext]);
            
            msg = sprintf('%s - Beginning processing "%s"', datetime, nomFic);
            RobotUtils.Common.printInfo(msg);
            
            %% Copie du fichier sur nomDirOut
            
            while 1
                nbBytesAvant = sizeFic(nomFic);
                pause(nbSecondes)
                nbBytesApres = sizeFic(nomFic);
                if nbBytesAvant == nbBytesApres
                    break
                end
            end
            
            if ~SameDirectory
                if sizeFic(nomFic) ~= sizeFic(nomFicIn)
                    flag = copyfile(nomFicIn, nomFic, 'f');
                    if ~flag
                        RobotUtils.Common.messageCopyfileFailed(nomFicIn, nomFic);
                        return
                    end
                    flagAnyProcessingHasBeenDone = 1;
                end
                
                nomFicWCDIn = strrep(nomFicIn, Ext, '.wcd');
                if exist(nomFicWCDIn, 'file')
                    nomFicWCD = strrep(nomFic, Ext, '.wcd');
                    if sizeFic(nomFicWCD) ~= sizeFic(nomFicWCDIn)
                        flag = copyfile(nomFicWCDIn, nomFicWCD, 'f');
                        if ~flag
                            RobotUtils.Common.messageCopyfileFailed(nomFicIn, nomFicWCD);
                            return
                        end
                        flagAnyProcessingHasBeenDone = 1;
                    end
                end
            end
        end
        
        
        function PlotKMZ(listeFic, classement, filtreData, nbSeconds)
            
            k = 0;
            if classement == 1 % Par ordre de fichiers
                PlotKMZFilter(listeFic, nbSeconds, '', '')
            else % Par type de donn�es
                if filtreData.MOS
                    PlotKMZFilter(listeFic, nbSeconds, '', '_Reflectivity_LatLong');
                end
                if filtreData.DTM
                    PlotKMZFilter(listeFic, nbSeconds, '', '_Bathymetry_LatLong');
                end
                if filtreData.NAV
                    PlotKMZFilter(listeFic, nbSeconds, 'Nav-', '');
                end
            end
            
            %% Trac� WC
            
            if filtreData.WC
                N = length(listeFic);
                listWC = {};
                for k3=1:N
                    [pathname, filename] = fileparts(listeFic{k3});
                    
                    strFiltre = [filename '_PolarEchograms.g3d'];
                    listKmzFiles = listeFicOnDepthDir(pathname, '.nc', 'Filtre', strFiltre);
                    listWC = [listWC; listKmzFiles(:)]; %#ok<AGROW>
                    
                    strFiltre = [filename '_Raw_PolarEchograms']; 
                    listKmzFiles = listeFicOnDepthDir(pathname, '.xml', 'Filtre', strFiltre);
                    listWC = [listWC; listKmzFiles(:)]; %#ok<AGROW>
                end
                I0 = cl_image.empty;
            	WC_InspectPolarEchogramDepthAcrossDist(I0, listWC);
            end
            
            
            function PlotKMZFilter(listeFic, nbSeconds, prefix, suffix)
                N = length(listeFic);
                for k1=1:N
                    [pathname, filename] = fileparts(listeFic{k1});
                    strFiltre = [prefix filename suffix];
                    listKmzFiles = listeFicOnDepthDir(pathname, '.kmz', 'Filtre', strFiltre);
                    for k2=1:length(listKmzFiles)
                        winopen(listKmzFiles{k2});
                        k = k + 1;
                        if k == 1
                            ind = 2;
                            while ind == 2
                                str = sprintf('Wait for the first file to be fully displayed, otherwise Google Earth can crash.\n\nIs Google Earth now ready to display the next files ?');
                                [ind, flag] = my_questdlg(str);
                                if ~flag
                                    return
                                end
                            end
                        else
                            pause(nbSeconds);
                        end
                    end
                end
            end
        end
    end
end
