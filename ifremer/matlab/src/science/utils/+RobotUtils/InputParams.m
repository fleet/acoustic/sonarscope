classdef InputParams
    
    methods (Static)
        
        function [flag, paramsGeneraux, paramsWC, paramsDTM, paramsMOS, flagParallelTbx, repImport, repExport] = ...
                CopyAndProcessSounderOutputFiles(Ext, repImport, repExport)
            
            persistent persistent_CLimRawAll persistent_CLimRawS7k 
            
            flagParallelTbx = 0;
            FirstFileNumber = 0;
            
            paramsGeneraux.nomDirIn              = [];
            paramsGeneraux.nomDirOut             = [];
            paramsGeneraux.PreProcessSeabedimage = 0;
            paramsGeneraux.nomDirNav             = [];
            paramsGeneraux.flagGoogleEarth       = 1;
            paramsGeneraux.FirstFileNumber       = 0;
            paramsGeneraux.NbPendingFiles        = 0;
            
            paramsWC.nomDirOut        = [];
            paramsWC.typeAlgo         = [];
            paramsWC.CLimRaw          = [];
            paramsWC.TypeOutputFormat = 2;
            
            paramsWC.NomDirOut_3DMatrix        = [];
            paramsWC.TypeOutputFormat_3DMatrix = 2;

            paramsWC.NomDirOut_AlongNav       = [];
            paramsWC.AlongNavStep             = [];
            paramsWC.AlongNavTypeOutputFormat = 2;
            
            paramsDTM.nomDir   = [];
            paramsDTM.TideFile = [];
            
            paramsMOS.nomDir                   = [];
            paramsMOS.ReflecVideoInverse       = 0;
            paramsMOS.nomFicCompensationCurves = {};
            
            QL = get_LevelQuestion;
            
            %% Param�tres manuels ou fichier de configuration ?
            
            [typeInputParameters, flag] = my_listdlg('How do you want to enter the input parameters ?', ...
                {'Manually'; 'Using a configuration file'}, ...
                'SelectionMode', 'Single');
            if ~flag
                return
            end
            if typeInputParameters == 1
                
                %% Nom du r�pertoire des .all ou .s7k d'origine
                
                str1 = ['Nom du r�pertoire o� sont d�pos�s les fichiers ' Ext];
                str2 = ['Name of the directory where the ' Ext ' files are available.'];
                [flag, paramsGeneraux.nomDirIn] = my_uigetdir(repImport, Lang(str1,str2));
                if ~flag
                    return
                end
                repImport = paramsGeneraux.nomDirIn;
                
                %% Nom du r�pertoie de destination (l� o� seront copi�s les .all et .wcd ou .s7k)
                
                str1 = ['Nom du r�pertoire o� les ' Ext ' seront copi�s'];
                str2 = ['Name of the directory where the ' Ext ' files will be copied'];
                [flag, paramsGeneraux.nomDirOut] = my_uigetdir(repExport, Lang(str1,str2));
                if ~flag
                    return
                end
                repExport = paramsGeneraux.nomDirOut;
                
                % if strcmp(paramsGeneraux.nomDirIn, paramsGeneraux.nomDirOut)
                %     str1 = 'Le r�pertoire de travail ne peut pas �tre le m�me que le r�pertoire de d�pos des fichiers.';
                %     str2 = 'The two directories cannot be the same.';
                %     my_warndlg(Lang(str1,str2), 1);
                %     flag = 0;
                %     return
                % end
                
                %% Fichier de d�part
                
                if strcmp(Ext, '.all')
                    [flag, FirstFileNumber] = RobotUtils.InputParams.questionFirstFileNumber(Ext);
                    if ~flag
                        return
                    end
                end
                paramsGeneraux.FirstFileNumber = FirstFileNumber;
                
                %% Nombre de fichiers en attente
                
                if strcmp(Ext, '.all')
                    paramsGeneraux.NbPendingFiles = 1;
                else
                    paramsGeneraux.NbPendingFiles = 0;
                end
%                 if QL > 2
                    p = ClParametre('Name', 'Number of pending files in real time backup system (O if RSYNC, 2 if PDS)', 'MinValue', 0, 'MaxValue', 5, 'Value', paramsGeneraux.NbPendingFiles);
                    a = StyledSimpleParametreDialog('params', p, 'Title', 'SonarScope Robot settings');
                    a.openDialog;
                    flag = a.okPressedOut;
                    if ~flag
                        return
                    end
                    paramsGeneraux.NbPendingFiles = a.getParamsValue;
%                 end
                
                %% Question pr�traitement des donn�es d'imagerie
                
                if (QL > 2) && strcmp(Ext, '.all')
                    str1 = 'Pr�traitement de la "Seabed Image" ?';
                    str2 = 'Preprocessing the "Seabed Image" datagrams ?';
                    [rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
                    if ~flag
                        return
                    end
                    paramsGeneraux.PreProcessSeabedimage = (rep == 1);
                end
                
                %% R�pertoires par d�faut
                
                %     str1 = 'Comment voulez-vous organiser les r�pertoires pour les donn�es MNT, WC, ... ?';
                %     str2 = 'How do you want to organize the output directories (DTM, WC, ...) ?';
                %     [ind, flag] = my_questdlg(Lang(str1,str2), 'Auto', 'Manual');
                %     if ~flag
                %         return
                %     end
                %     flagDefault = (ind == 1);
                
                %% R�pertoire Navigation
                
                %     if flagDefault
                %         paramsGeneraux.nomDirNav = fullfile(repExport, 'Navigation');
                %         fprintf('The default output directory for the DTM is %s\n', paramsGeneraux.nomDirNav);
                %     else
                str1 = 'Nom du r�pertoire o� la navigation sera cr��.';
                str2 = 'Name of the directory where the Navigation will be created.';
                [flag, paramsGeneraux.nomDirNav] = my_uigetdir(repExport, Lang(str1,str2));
                if ~flag
                    return
                end
                %     end
                
                %% Cr�ation des DTM et mosa�ques
                
                str1 = 'Voulez-vous cr�er des MNT et mosa�ques individuels par faisceaux ?';
                str2 = 'Do you want to create individual DTM and Reflectivity per file ?';
                [ind, flag] = my_questdlg(Lang(str1,str2));
                if ~flag
                    return
                end
                
                if ind == 1
                    %         if flagDefault
                    %             paramsDTM.nomDir = fullfile(repExport, 'DTM');
                    %             fprintf('The default output directory for the DTM is %s\n', paramsDTM.nomDir);
                    %         else
                    str1 = 'Nom du r�pertoire o� les MNT seront cr��s.';
                    str2 = 'Name of the directory where the DTM will be created.';
                    [flag, paramsDTM.nomDir] = my_uigetdir(repExport, Lang(str1,str2));
                    if ~flag
                        return
                    end
                    %         end
                    
                    [flag, paramsDTM.TideFile] = inputTideFile(1, repExport);
                    if ~flag
                        return
                    end
                    
                    %         if flagDefault
                    %             paramsMOS.nomDir = fullfile(repExport, 'Reflec');
                    %             fprintf('The default output directory for the reflectivity is %s\n', paramsMOS.nomDir);
                    %         else
                    str1 = 'Nom du r�pertoire o� les images de r�flectivit� seront cr��s.';
                    str2 = 'Name of the directory where the reflectivity images will be created.';
                    [flag, paramsMOS.nomDir] = my_uigetdir(repExport, Lang(str1,str2));
                    if ~flag
                        return
                    end
                    %         end
                    
                    str1 = 'Voulez-vous appliquer des courbes de compensation de la r�flectivit� ?';
                    str2 = 'Do you want to apply compensation curves for the reflectivity ?';
                    [ind, flag] = my_questdlg(Lang(str1,str2));
                    if ~flag
                        return
                    end
                    if ind == 1
                        [flag, paramsMOS.nomFicCompensationCurves] = uiSelectFiles('ExtensionFiles', '.mat', 'RepDefaut', repExport, ...
                            'ENTETE', 'IFREMER - SonarScope - Select the reflectivity compensation files');
                        if ~flag
                            return
                        end
                    end
                    
                    str1 = 'Table de couleur pour l''affichage de la r�flectivit� dans Google Earth';
                    str2 = 'Colortable for the reflectivity displayed in Google Earth';
                    str{1} = 'Normal  : Black=Low reflectivity,  White=High reflectivity';
                    str{2} = 'Inverse : Black=High reflectivity, White=Low  reflectivity';
                    [rep, flag] = my_listdlg(Lang(str1,str2), str, 'SelectionMode', 'Single');
                    if ~flag
                        return
                    end
                    paramsMOS.ReflecVideoInverse = (rep == 2);
                end
                
                %% Affichage dans Google Earth
                
                str1 = 'Voulez-vous afficher les donn�es dans Google Earth (Navigation, MNT, ...) ?';
                str2 = 'Do you want to display the data in Google Earth (Navigation, DTM, ...) ?';
                [ind, flag] = my_questdlg(Lang(str1,str2));
                if ~flag
                    return
                end
                paramsGeneraux.flagGoogleEarth = (ind == 1);
                
                %% Cr�ation de WC
                
                str1 = 'Voulez-vous traiter la colonne d''eau (�chogrammes polaires) ?';
                str2 = 'Do you want to process the WC data (polar echograms) ?';
                [ind, flag] = my_questdlg(Lang(str1,str2));
                if ~flag
                    return
                end
                
                if ind == 1
                    
                    %% Nom du r�pertoire o� seront cr��s les �chogrammes polaires
                    
                    %         if flagDefault
                    %             paramsWC.nomDirOut = fullfile(repExport, 'WC-DepthAcrossDist');
                    %             fprintf('The default output directory for WCD is %s\n', paramsWC.nomDirOut);
                    %         else
                    str1 = 'Nom du r�pertoire o� les �chogrammes polaires seront cr��s.';
                    str2 = 'Name of the directory where the WC polar echograms will be created.';
                    [flag, paramsWC.nomDirOut] = my_uigetdir(repExport, Lang(str1,str2));
                    if ~flag
                        return
                    end
                    %         end
                    
                    %% Type d'algorithme
                    
                    if strcmp(Ext, '.all')
                        InitialValue = 2;
                    else
                        InitialValue = 1;
                    end
                    [flag, paramsWC.typeAlgo] = question_WCRaytracingAlgorithm('InitialValue', InitialValue);
                    if ~flag
                        return
                    end
                    
                    %% Rehaussement de contraste des donn�es brutes
                    
                    switch Ext
                        case '.all'
                            if isempty(persistent_CLimRawAll)
                                CLimRaw = [-64 10];
                            else
                                CLimRaw = persistent_CLimRawAll;
                            end
                        case '.s7k'
                            if isempty(persistent_CLimRawS7k)
                                CLimRaw = [20 60]; % TODO Carla doir r�fl�chir au truc
                            else
                                CLimRaw = persistent_CLimRawS7k;
                            end
                    end
                    
                    str1 = 'IFREMER - SonarScope : Bornes du rehaussement de contraste';
                    str2 = 'IFREMER - SonarScope : Color limits (contrast enhancement)';
                    p    = ClParametre('Name', Lang('Valeur min', 'Min value'), ...
                        'Unit', 'dB',  'MinValue', -100, 'MaxValue', 100, 'Value', CLimRaw(1));
                    p(2) = ClParametre('Name', Lang('Valeur max', 'Max value'), ...
                        'Unit', 'dB',  'MinValue', -100, 'MaxValue', 100, 'Value', CLimRaw(2));
                    a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
                    % a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
                    a.sizes = [0 -3 -1 -2 -1 -1 -1 0];
                    a.openDialog;
                    flag = a.okPressedOut;
                    if ~flag
                        return
                    end
                    paramsWC.CLimRaw = a.getParamsValue;
                    
                    switch Ext
                        case '.all'
                            persistent_CLimRawAll = CLimRaw;
                        case '.s7k'
                            persistent_CLimRawS7k = CLimRaw ;
                    end
                    
                    %% Format des fichiers WC
                    
                    str    = {'XML-Bin'};
                    str{2} = 'Netcdf';
                    [paramsWC.TypeOutputFormat, flag] = my_listdlg('Format of the WCD output files', str, 'InitialValue', 2, 'SelectionMode', 'Single');
                    if ~flag
                        return
                    end
                    
                    %% WC-3DMatrix
                    
                    str1 = 'Voulez-vous traiter la colonne d''eau (�chogrammes polaires) ?';
                    str2 = 'Do you want to create the WC 3D-Matrix ?';
                    [rep2, flag] = my_questdlg(Lang(str1,str2));
                    if ~flag
                        return
                    end
                    
                    if rep2 == 1
                        str1 = 'Nom du r�pertoire o� les �chogrammes matrices WC-3D seront cr��s.';
                        str2 = 'Name of the directory where the WC 3D matrices will be created.';
                        [flag, paramsWC.NomDirOut_3DMatrix] = my_uigetdir(repExport, Lang(str1,str2));
                        if ~flag
                            return
                        end
                        
                        %% Format des fichiers WC
                        
                        str    = {'XML-Bin'};
                        str{2} = 'Netcdf';
                        [paramsWC.TypeOutputFormat_3DMatrix, flag] = my_listdlg('Format of the WC-3DMatrix output files', str, 'InitialValue', 2, 'SelectionMode', 'Single');
                        if ~flag
                            return
                        end
                        
                        %% WC-AlongNav
                        
                        str1 = 'Voulez-vous traiter les coupes WC parall�les � la navigation ?';
                        str2 = 'Do you want to create the WC-AlongNav slices ?';
                        [rep3, flag] = my_questdlg(Lang(str1,str2));
                        if ~flag
                            return
                        end
                        
                        if rep3 == 1
                            str    = {'Auto'};
                            str{2} = 'Manual';
                            [rep4, flag] = my_listdlg('Step between the slices', str, 'SelectionMode', 'Single');
                            if ~flag
                                return
                            end
                            if rep4 == 2
                                p = ClParametre('Name', 'Step', 'Unit', 'm',  'MinValue', 0, 'MaxValue', 200, 'Value', 25);
                                a = StyledSimpleParametreDialog('params', p, 'Title', 'Step between the WC AlongNav slices');
                                a.openDialog;
                                flag = a.okPressedOut;
                                if ~flag
                                    return
                                end
                                paramsWC.AlongNavStep = a.getParamsValue;
                            end
                            
                            str1 = 'Nom du r�pertoire o� les coupes WC le long de la navigation seront cr��s.';
                            str2 = 'Name of the directory where the WC slices along the navigation will be created.';
                            [flag, paramsWC.NomDirOut_AlongNav] = my_uigetdir(repExport, Lang(str1,str2));
                            if ~flag
                                return
                            end
                            
                            %% Format des fichiers WC
                            
                            str    = {'XML-Bin'};
                            str{2} = 'Netcdf';
                            [paramsWC.AlongNavTypeOutputFormat, flag] = my_listdlg('Format of the WC-AlongNav output files', str, 'InitialValue', 2, 'SelectionMode', 'Single');
                            if ~flag
                                return
                            end
                        end
                    end
                end
                
                %% Traitement parallel
                
                if strcmp(Ext, '.all')
                    [flag, flagParallelTbx] = RobotUtils.InputParams.questionParallelTbx;
                    if ~flag
                        return
                    end
                end
                paramsGeneraux.flagParallelTbx = flagParallelTbx;
                
                %% Sauvegarde du fichier de configuration
                
                str1 = 'Voulez-vous sauvegarder les param�tres dans un fichier de configuration ?';
                str2 = 'Do you want to save the parameters in a configuration file ?';
                [ind, flag] = my_questdlg(Lang(str1,str2));
                if ~flag
                    return
                end
                
                if ind == 1
                    [flag, nomFicConfig] = my_uiputfile('*.json', 'Define the configuration file', fullfile(repExport, 'ConfigRobot.json'));
                    if ~flag
                        return
                    end
                    RobotUtils.Common.writeConfigurationFile(nomFicConfig, paramsGeneraux, paramsWC, paramsDTM, paramsMOS, flagParallelTbx);
                end
                
            else
                
                [flag, nomFicConfig] = my_uigetfile('*.json', 'Pick a configuration file', repImport);
                if ~flag
                    return
                end
                [paramsGeneraux, paramsWC, paramsDTM, paramsMOS, flagParallelTbx] = RobotUtils.Common.readConfigurationFile(nomFicConfig);
                repImport = fileparts(nomFicConfig);
                
                %% Fichier de d�part
                
                if strcmp(Ext, '.all')
                    [flag, FirstFileNumber] = RobotUtils.InputParams.questionFirstFileNumber(Ext);
                    if ~flag
                        return
                    end
                end
                paramsGeneraux.FirstFileNumber = FirstFileNumber;
                
                %% Traitement parallel
                
                if strcmp(Ext, '.all')
                    [flag, flagParallelTbx] = RobotUtils.InputParams.questionParallelTbx;
                    if ~flag
                        return
                    end
                end
                paramsGeneraux.flagParallelTbx = flagParallelTbx;
            end
        end
        
        
        function [flag, FirstFileNumber] = questionFirstFileNumber(Ext)
            
            persistent persistent_FirstFileNumber
            
            if isempty(persistent_FirstFileNumber)
                FirstFileNumber = 0;
            else
                FirstFileNumber = persistent_FirstFileNumber;
            end
            
            p  = ClParametre('Name', 'Number (0 to start from the beginning)', 'MinValue', 0, 'MaxValue', 9998, 'Value', FirstFileNumber);
            a = StyledSimpleParametreDialog('params', p, 'Title', ['Number of the first ' Ext ' file to check']);
            a.openDialog;
            flag = a.okPressedOut;
            if ~flag
                return
            end
            FirstFileNumber = a.getParamsValue;
            persistent_FirstFileNumber = FirstFileNumber;
        end
        
        
        function [flag, flagParallelTbx] = questionParallelTbx
            str1 = 'Voulez-vous utiliser la parall�le toolbox ?';
            str2 = 'Do you want to use the parallel toolbox ?';
            [ind, flag] = my_questdlg(Lang(str1,str2));
            if ~flag
                return
            end
            flagParallelTbx = (ind == 1);
        end
        
        
        function [flag, nomDirIn, repExport, nomDirOut, DirNames] = MoveFiles(listFileNames, repExport)
            
            DirNames.DTMIn         = [];
            DirNames.DTMOut        = [];
            DirNames.MOSIn         = [];
            DirNames.MOSOut        = [];
            DirNames.WCIn          = [];
            DirNames.WCOut         = [];
            DirNames.NavIn         = [];
            DirNames.NavOut        = [];
            DirNames.WC3DMatrixIn  = [];
            DirNames.WC3DMatrixOut = [];
            DirNames.WCAlongNavIn  = [];
            DirNames.WCAlongNavOut = [];
            
            %% Nom du r�pertoire des .all d'origine
            
            [nomDirIn, ~, Ext] = fileparts(listFileNames{1});
            
            %% Nom du r�pertoire de destination (l� o� seront copi�s les .all, .wcd et autres)
            
            str1 = sprintf('Nom du r�pertoire o� les %s seront d�plac�s.', Ext);
            str2 = sprintf('Name of the directory where the %s files will be moved.', Ext);
            [flag, nomDirOut] = my_uigetdir(repExport, Lang(str1,str2));
            if ~flag
                return
            end
            repExport = nomDirOut;
            
            if strcmp(nomDirIn, nomDirOut)
                RobotUtils.InputParams.messagrSameDir(nomDirIn, nomDirOut)
                flag = 0;
                return
            end
            
            %%
            
            str1 = 'Avez-vous cr�� les donn�es avec un fichier de navigation ?';
            str2 = 'Have you processed the data with a configuration file ?';
            [indRep, flag] = my_questdlg(Lang(str1,str2));
            if ~flag
                return
            end
            if indRep == 1
                [flag, nomFicConfig] = my_uigetfile('*.json', 'Pick a configuration file', nomDirIn);
                if ~flag
                    return
                end
                [paramsGeneraux, paramsWC, paramsDTM, paramsMOS] = RobotUtils.Common.readConfigurationFile(nomFicConfig);
                
                nomDirIn = paramsGeneraux.nomDirOut;
                
                DirNames.DTMIn        = paramsDTM.nomDir;
                DirNames.MOSIn        = paramsMOS.nomDir;
                DirNames.WCIn         = paramsWC.nomDirOut;
                DirNames.NavIn        = paramsGeneraux.nomDirNav;
                DirNames.WC3DMatrixIn = paramsWC.NomDirOut_3DMatrix;
                DirNames.WCAlongNavIn = paramsWC.NomDirOut_AlongNav;
                
                DirNames.DTMOut        = [nomDirOut strrep(DirNames.DTMIn,  nomDirIn, '')];
                DirNames.MOSOut        = [nomDirOut strrep(DirNames.MOSIn,  nomDirIn, '')];
                DirNames.WCOut         = [nomDirOut strrep(DirNames.WCIn,   nomDirIn, '')];
                DirNames.NavOut        = [nomDirOut strrep(DirNames.NavIn,  nomDirIn, '')];
                DirNames.WC3DMatrixOut = [nomDirOut strrep(DirNames.WC3DMatrixIn,   nomDirIn, '')];
                DirNames.WCAlongNavOut = [nomDirOut strrep(DirNames.WCAlongNavIn,  nomDirIn, '')];
            else
                
                %% R�pertoires des DTM et mosa�ques
                
                str1 = 'Avez-vous cr�� des MNT et images de r�flectivit� ?';
                str2 = 'Have you processed DTM and reflectivity images ?';
                [ind, flag] = my_questdlg(Lang(str1,str2));
                if ~flag
                    return
                end
                
                if ind == 1
                    str1 = 'Nom du r�pertoire o� les MNT ont �t� cr��s.';
                    str2 = 'Name of the directory where the DTM have been created.';
                    [flag, DirNames.DTMIn] = my_uigetdir(nomDirIn, Lang(str1,str2));
                    if ~flag
                        return
                    end
                    
                    str1 = 'Nom du r�pertoire o� les MNT seront d�plac�s.';
                    str2 = 'Name of the directory where the DTM will be moved.';
                    [flag, DirNames.DTMOut] = my_uigetdir(repExport, Lang(str1,str2));
                    if ~flag
                        return
                    end
                    
                    str1 = 'Nom du r�pertoire o� les images de r�flectivit� ont �t� cr��s.';
                    str2 = 'Name of the directory where the reflectivity images have been created.';
                    [flag, DirNames.MOSIn] = my_uigetdir(nomDirIn, Lang(str1,str2));
                    if ~flag
                        return
                    end
                    
                    str1 = 'Nom du r�pertoire o� les images de r�flectivit� seront d�plac�es.';
                    str2 = 'Name of the directory where the reflectivity images will be moved.';
                    [flag, DirNames.MOSOut] = my_uigetdir(repExport, Lang(str1,str2));
                    if ~flag
                        return
                    end
                    
                    str1 = 'Nom du r�pertoire o� les fichiers de navigation ont �t� cr��s.';
                    str2 = 'Name of the directory where the navigation files have been created.';
                    [flag, DirNames.NavIn] = my_uigetdir(nomDirIn, Lang(str1,str2));
                    if ~flag
                        return
                    end
                    
                    str1 = 'Nom du r�pertoire o� les fichiers de navigation seront d�plac�s.';
                    str2 = 'Name of the directory where the navigation files will be moved.';
                    [flag, DirNames.NavOut] = my_uigetdir(repExport, Lang(str1,str2));
                    if ~flag
                        return
                    end
                end
                
                %% Cr�ation de WC
                
                str1 = 'La colonne d''eau a t''elle �t� cr�e ?';
                str2 = 'Have you processed Water Column Data ?';
                [ind, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
                if ~flag
                    return
                end
                
                if ind == 1
                    
                    %% Nom du r�pertoie o� seront cr��s les �chogrammes polaires
                    
                    str1 = 'Nom du r�pertoire o� les �chogrammes polaires ont �t� cr��s.';
                    str2 = 'Name of the directory where the polar echograms have been created.';
                    [flag, DirNames.WCIn] = my_uigetdir(nomDirIn, Lang(str1,str2));
                    if ~flag
                        return
                    end
                    
                    str1 = 'Nom du r�pertoire o� les �chogrammes polaires seront d�plac�s.';
                    str2 = 'Name of the directory where the polar echograms will be moved.';
                    [flag, DirNames.WCOut] = my_uigetdir(repExport, Lang(str1,str2));
                    if ~flag
                        return
                    end
                end
            end
        end
        
        function messagrSameDir
            str1 = 'Les r�pertoires In et Out sont identiques.';
            str2 = 'The two directories cannot be the same.';
            my_warndlg(Lang(str1,str2), 1);
        end
        
        
        function [flag, filtreData, classement, nbSeconds] = PlotKMZ

            filtreData.DTM = 1;
            filtreData.MOS = 1;
            filtreData.NAV = 1;
            filtreData.WC  = 1;
            nbSeconds      = [];
            
            %% Ordre des trac�s
            
            [classement, flag] = my_listdlg('Classify data in Google Earth accoring to :', {'File names'; 'Data types'}, 'SelectionMode', 'Single');
            if ~flag
                return
            end
            
            %% Filtre des DataType si trac� par type
            
            if classement == 2
                [ind, flag] = my_listdlg('Select the data types you want to display in Google Earth', {'DTM'; 'MOS'; 'NAV'; 'WC (in Matlab figure, not in GE)'});
                if ~flag
                    return
                end
                filtreData.DTM = ismember(1, ind);
                filtreData.MOS = ismember(2, ind);
                filtreData.NAV = ismember(3, ind);
                filtreData.WC  = ismember(4, ind);
                
                %% Nombre de secondes d'attente entre 2 envois � GE
                
                if any(ind < 4)
                    p = ClParametre('Name', 'Delay between the file exports', 'Unit', 's',  'MinValue', 0, 'MaxValue', 20, 'Value', 1);
                    a = StyledSimpleParametreDialog('params', p, 'Title', 'Upload .kmz file to Google Earth');
                    a.openDialog;
                    flag = a.okPressedOut;
                    if ~flag
                        return
                    end
                    nbSeconds = a.getParamsValue;
                end
            end
        end
    end
end