classdef S7k
    % Collection of static functions TODO
    %
    % Authors  : JMA Authors
    % See also : TODO
    %
    % Reference pages in Help browser
    %   <a href="matlab:doc XMLBinUtils" >XMLBinUtils</a>
    %  -------------------------------------------------------------------------------
    
    methods (Static)
        
        % TODO :
        %         logFileId = getLogFileId;
        %         msg = sprintf('Begin %s', nomFicS7K{k1});
        %         logFileId.info('S7k', msg);
        
        function launchProcess(paramsGeneraux, paramsWC, paramsDTM, paramsMOS, varargin)
            
            % paramsGeneraux.nomDirIn = '\\md-nas\missioncourante\EQUIPEMENTS\EM122\DONNEES\CARTOGRAPHIE';
            % paramsGeneraux.nomDirOut = 'E:\MAYOBS15\EM122\TransitA';
            % paramsGeneraux.PreProcessSeabedimage = 0;
            % paramsGeneraux.nomDirNav = 'E:\MAYOBS15\EM122\TransitA\Navigation';
            % paramsGeneraux.flagGoogleEarth = 1;
            % paramsWC.nomDirOut = 'E:\MAYOBS15\EM122\TransitA\WC-DepthAcrossDist';
            % paramsWC.typeAlgo  = 2;
            % paramsWC.CLimRaw   = [-64 10];
            % paramsDTM.nomDir = fullfile(nomDirOut, 'DTM-MOS');
            % paramsDTM.TideFile = 'E:\MAYOBS15\EM122\TransitA\MAYOBS15.ttb';
            % paramsMOS.nomFicCompensationCurves = {'E:\MAYOBS15\EM122\TransitA\Mode_4_Swath_Double_Signal_CW_CompensSingle_Whole_image_on_TransitA_0027_20201004_151433_EM122_Marion_Dufresne_Clean_1_Reflectivity_BeamPointingAngle_TxBeamIndexSwath.mat'};
            % paramsMOS.ReflecVideoInverse = 1;
            % RobotUtils.S7k.createAndExportWCD(paramsGeneraux, paramsWC, paramsDTM, paramsMOS)
            
            nomDirIn        = paramsGeneraux.nomDirIn;
            nomDirOut       = paramsGeneraux.nomDirOut;
            nomDirNav       = paramsGeneraux.nomDirNav;
            flagGoogleEarth = paramsGeneraux.flagGoogleEarth;
            FirstFileNumber = paramsGeneraux.FirstFileNumber;
            NbPendingFiles  = paramsGeneraux.NbPendingFiles;
            
            Fig                        = [];
            nbSecondes                 = 5;
            flagMessageWaiting         = 1;
            listNomFicForWCDProcessing = {};
            
            %% D�finition de la carto
            
            Carto = RobotUtils.Common.DefineCarto(nomDirOut);
            
            %% Test si nomDirOut identique � nomDirIn
            
            SameDirectory = strcmp(nomDirIn, nomDirOut);
            
            %% Cr�ation du r�pertoire nomDirIn s'il n'existe pas
            
            if ~SameDirectory && ~exist(nomDirOut, 'dir')
                flag = RobotUtils.Common.mkdirIfNecessary(nomDirOut);
                if ~flag
                    return
                end
            end
            
            %% Etablissement de la liste des fichiers d�j� trait�s
            
            listeDejaTraites = {};
            
            %% Lancement de la surveillance du r�pertoire de nomDirIn
            
            [hwControl, TagControlWindow] = RobotUtils.Common.createShutdownControlWindow(nomDirIn, nomDirOut);
            RobotUtils.Common.printStart(nomDirIn);
            RobotUtils.Common.printWaiting(TagControlWindow);
            
            while 1
                while ishandle(hwControl)
                    
                    %% Lecture de la liste des fichiers qui ont �t� d�plac�s
                    
                    [listeFichiersDeplaces, nomFicInfo] = RobotUtils.Common.readListOfMovedFiles(nomDirIn, nomDirOut, '.s7k');
                    
                    %% Lecture des fichiers .s7k pr�sents sur nomDirIn
                    
                    listeFic = listeFicOnDir(nomDirIn, '.s7k');
                    for k=1:length(listeFic)
                        listeFic{k} = windows_replaceVolumeLetter(listeFic{k});
                    end
                    
                    %% Etablissement de la liste des fichiers qui sont sur nomDirIn mais qui ne sont pas sur nomDirOut
                    
                    listeFic = setdiff(listeFic, [listeDejaTraites; listeFichiersDeplaces]);
                    
                    if FirstFileNumber ~=  0
                        for k=length(listeFic):-1:1
                            [~, nomFicSeul] = fileparts(listeFic{k});
                            number = str2double(nomFicSeul(1:4));
                            if number < FirstFileNumber
                                listeFic(k) = [];
                            end
                        end
                    end
                    
                    %% Copie des fichiers qui n'ont pas encore �t� copi�s
                    
                    nbFicInLoop = length(listeFic) - NbPendingFiles;
                    nomFicCopied = {};
                    str = sprintf('Check / Copy files from\n%s\nto\n%s', nomDirIn, nomDirOut);
                    hw = create_waitbar(str, 'N', nbFicInLoop);
                    for k=1:nbFicInLoop
                        my_waitbar(k, nbFicInLoop, hw)
                        
                        %% Test si la fen�tre a �t� ferm�e
                        
                        [flag, hwControl, TagControlWindow] = RobotUtils.Common.checkShutdownControlFigure(hwControl, nomDirIn, nomDirOut, TagControlWindow);
                        if ~flag
                            break
                        end
                        
                        %% Copie du fichier de donn�es
                        
                        [flag, nomFicIn, nomFic, nomFicSeul, nbSecondes, flagAnyProcessingHasBeenDone] = RobotUtils.Common.copieDataFile(listeFic{k}, ...
                            k, nbFicInLoop, 2, SameDirectory, '.s7k', nomDirOut); %#ok<ASGLU>
                        if ~flag
                            continue
                        end
                    end
                    my_close(hw)
                    
                    %% Cr�ation du cache en utilisant la //Tbx
                    
                    if ~isempty(nomFicCopied)
                        WorkInProgress('Creating cache for new files');
                    end
                    [~, Carto] = prepare_lectures7k_step1(nomFicCopied, 'Carto', Carto);
                    
                    %% Lancement des processings
                    
                    hw = create_waitbar('Processing files', 'N', nbFicInLoop);
                    for k=1:nbFicInLoop
                        my_waitbar(k, nbFicInLoop, hw)
                        
                        %% Test si la fen�tre a �t� ferm�e
                        
                        [flag, hwControl, TagControlWindow] = RobotUtils.Common.checkShutdownControlFigure(hwControl, nomDirIn, nomDirOut, TagControlWindow);
                        if ~flag
                            break
                        end
                        
                        %% D�but du traitement du fichier individuel
                        
                        if k < (nbFicInLoop-NbPendingFiles-1)
                            nbSecondes = 0;
                        else
                            nbSecondes = 5;
                        end
                        flagAnyProcessingHasBeenDone = 0;
                        
                        nomFicIn = windows_replaceVolumeLetter(listeFic{k});
                        [~, nomFicSeul] = fileparts(nomFicIn);
                        nomFic = fullfile(nomDirOut, [nomFicSeul '.s7k']);
                        
                        msg = sprintf('%s - Beginning processing "%s"', datetime, nomFic);
                        RobotUtils.Common.printInfo(msg);
                        
                        %% Lecture du fichier
                        
                        [flag, Carto] = prepare_lectures7k_step1(nomFic, 'Carto', Carto);
                        if ~flag
                            appendTxtFile(nomFicInfo, nomFic);
                            continue
                        end
                        a = cl_reson_s7k('nomFic', nomFic, 'KeepNcID', true);
                        if isempty(a)
                            appendTxtFile(nomFicInfo, nomFic);
                            continue
                        end
                        
                        %% Import de la mar�e
                        
                        if ~isempty(paramsDTM.TideFile)
                            % % ALL_TideImport(A0, nomFic, paramsDTM.TideFile, 'Tide', 'Mute', 1);
                            % TODO :Comment� pour le moment car l'import de mar�e reste � faire pour les .s7k
                            % S7K_TideImport(S0, nomFic, paramsDTM.TideFile, 'Tide', 'Mute', 1, 'CheckAll', 0);
                        end
                        
                        %% Cr�ation d'un DTM et d'une mosa�que de r�flectivit�
                        
                        if ~isempty(paramsDTM.nomDir) || ~isempty(paramsMOS.nomDir)
                            flagProcessingHasBeenDone = RobotUtils.S7k.createAndExportDTM(nomFic, paramsDTM.nomDir, paramsDTM.TideFile, paramsMOS.nomDir, paramsMOS.ReflecVideoInverse, paramsMOS.nomFicCompensationCurves, flagGoogleEarth);
                            flagAnyProcessingHasBeenDone = flagAnyProcessingHasBeenDone || flagProcessingHasBeenDone;
                        end
                        
                        %% Affichage de la navigation dans Google Earth
                        
                        %                     if flagAnyProcessingHasBeenDone
                        if flagAnyProcessingHasBeenDone || (~isempty(nomDirNav) && ~exist(fullfile(fullfile(nomDirNav, 'Google'), [nomFic '.kmz']), 'file')) % Modif JM2A le 15/03/2021
                            Fig = RobotUtils.S7k.plotAndExportNavigation(a, nomFic, nomDirNav, Fig, flagGoogleEarth);
                        end
                        
                        %% Ajout du fichier dans la liste des fichiers d�j� trait�s
                        
                        listeDejaTraites{end+1,1} = nomFicIn; %#ok<AGROW>
                        
                        msg = sprintf('%s - End processing "%s"', datetime, nomFic);
                        RobotUtils.Common.printInfo(msg);
                        
                        %% Colonne d'eau non prioritaire
                        
                        listNomFicForWCDProcessing{end+1} = nomFic; %#ok<AGROW>
                        flagMessageWaiting = 1;
                    end
                    my_close(hw)
                    
                    %% Traitement de la colonne d'eau
                    
                    if (nbFicInLoop == 0) && ~isempty(listNomFicForWCDProcessing)
                        nomFic = listNomFicForWCDProcessing{1};
                        RobotUtils.S7k.WCDProcessing(nomFic, paramsWC);
                        listNomFicForWCDProcessing(1) = [];
                    end
                    
                    %% Message d'attente
                    
                    if (nbFicInLoop == 0) && flagMessageWaiting
                        RobotUtils.Common.printWaiting(TagControlWindow);
                        flagMessageWaiting = 0;
                    end
                    
                    %% Test si la fen�tre a �t� ferm�e
                    
                    [flag, hwControl, TagControlWindow] = RobotUtils.Common.checkShutdownControlFigure(hwControl, nomDirIn, nomDirOut, TagControlWindow);
                    if flag
                        pause(nbSecondes)
                    end
                end
                [flag, hwControl, TagControlWindow] = RobotUtils.Common.checkShutdownControlFigure(hwControl, nomDirIn, nomDirOut, TagControlWindow);
                if flag
                    pause(nbSecondes)
                else
                    break
                end
            end
            RobotUtils.Common.printShutdown;
        end
        
        
        function  flagProcessingHasBeenDone = createAndExportWCD(nomFicS7K, paramsWC, varargin)
            
            global logFileId %#ok<GVMIS> %#ok<GVMIS> 
            
            [varargin, logFileId] = getPropertyValue(varargin, 'logFileId', logFileId); %#ok<ASGLU>
            
            flagProcessingHasBeenDone = 0;
            
            nomDirOut  = paramsWC.nomDirOut;
            typeAlgoWC = paramsWC.typeAlgo;
            CLimRaw    = paramsWC.CLimRaw;
            CLimComp   = [-10 40];
            TypeOutputFormat = paramsWC.TypeOutputFormat;
            
            flag = RobotUtils.Common.mkdirIfNecessary(nomDirOut);
            if ~flag
                return
            end
            
            [~, filename] = fileparts(nomFicS7K);
            nomFicWCRaw  = [filename '_Raw_PolarEchograms.xml'];
            nomFicWCComp = [filename '_Comp_PolarEchograms.xml'];
            nomFicWCNc   = [filename '_PolarEchograms.g3d.nc'];
            
            Names.nomFicXmlRaw  = fullfile(nomDirOut, nomFicWCRaw);
            Names.nomFicXmlComp = fullfile(nomDirOut, nomFicWCComp);
            Names.nomFicNetcdf  = fullfile(nomDirOut, nomFicWCNc);
            Names.nomFicKML     = [];
            Names.AreaName      = [];
            
            MaxImageSize = 1000;
            
            if (exist(Names.nomFicXmlRaw, 'file') && exist(Names.nomFicXmlComp, 'file')) ...
                    || exist(Names.nomFicNetcdf, 'file')
                return
            end
            
            nomFicNav       = [];
            nomFicIP        = [];
            OrigineWC       = 1; % 1=WC, 2=Mag&Phase
            ChoixNatDecibel = 2; % 1=Val nat, 2=dB
            skipAlreadyProcessedFiles = 0;
            TagName         = [];
            MaskBeyondDetection = 0;
            [~, Carto] = prepare_lectures7k_step1(nomFicS7K);% , 'Carto', Carto);
            
            TypeWCOutput = 1; % 1 = echogrammes, 2 = 3DMatrix
            if TypeOutputFormat == 1 % XML-Bin
                flagProcessingHasBeenDone = process_ResonS7k_Echograms(nomFicS7K, nomFicNav, nomFicIP, TypeWCOutput, Names, ...
                    OrigineWC, ChoixNatDecibel, CLimRaw, CLimComp, TagName, MaskBeyondDetection, ...
                    'typeAlgoWC', typeAlgoWC, 'MaxImageSize', MaxImageSize, ...
                    'skipAlreadyProcessedFiles', skipAlreadyProcessedFiles, 'Carto', Carto);
            else % GLOBE
                flagProcessingHasBeenDone = process_ResonS7k_EchogramsGlobeFlyTexture(nomFicS7K, nomFicNav, nomFicIP, TypeWCOutput, Names, ...
                    OrigineWC, ChoixNatDecibel, CLimRaw, CLimComp, TagName, MaskBeyondDetection, ...
                    'typeAlgoWC', typeAlgoWC, 'MaxImageSize', MaxImageSize, ...
                    'skipAlreadyProcessedFiles', skipAlreadyProcessedFiles, 'Carto', Carto);
            end
        end
        
        
        function flagProcessingHasBeenDone = createAndExportWCD_3DMatrix(nomFicS7K, paramsWC, varargin)
            
            global logFileId %#ok<GVMIS> %#ok<GVMIS> 
            
            [varargin, logFileId] = getPropertyValue(varargin, 'logFileId', logFileId); %#ok<ASGLU>
            
            flagProcessingHasBeenDone = 0;
            
            nomDirOut        = paramsWC.NomDirOut_3DMatrix;
            TypeFileWCOutput = paramsWC.TypeOutputFormat_3DMatrix;
            typeAlgoWC       = paramsWC.typeAlgo;
            CLimRaw          = paramsWC.CLimRaw;
            CLimComp         = [-10 40];
            
            flag = RobotUtils.Common.mkdirIfNecessary(nomDirOut);
            if ~flag
                return
            end
            
            [~, filename] = fileparts(nomFicS7K);
            nomFicWCRaw   = [filename '_WCCube_Raw_PolarEchograms.xml'];
            nomFicWCComp  = [filename '_WCCube_Comp_PolarEchograms.xml'];
            nomFicWCNc    = [filename '_WC3DMatrix.nc'];
            
            Names.nomFicXmlRaw  = fullfile(nomDirOut, nomFicWCRaw);
            Names.nomFicXmlComp = fullfile(nomDirOut, nomFicWCComp);
            Names.nomFicNetcdf  = fullfile(nomDirOut, nomFicWCNc);
            Names.nomFicKML     = [];
            Names.AreaName      = [];
            
            MaxImageSize = 1000;
            
            if (exist(Names.nomFicXmlRaw, 'file') && exist(Names.nomFicXmlComp, 'file')) ...
                    || exist(Names.nomFicNetcdf, 'file')
                return
            end
            
            nomFicNav       = [];
            nomFicIP        = [];
            OrigineWC       = 1; % 1=WC, 2=Mag&Phase
            ChoixNatDecibel = 2; % 1=Val nat, 2=dB % TODO : passer cela en PN/PV
            skipAlreadyProcessedFiles = 0;
            TagName         = [];
            MaskBeyondDetection = 0;
            [~, Carto] = prepare_lectures7k_step1(nomFicS7K);% , 'Carto', Carto);
            
            TypeWCOutput = 2; % 1 = echogrammes, 2 = 3DMatrix
            flagProcessingHasBeenDone = process_ResonS7k_Echograms(nomFicS7K, nomFicNav, nomFicIP, TypeWCOutput, Names, ...
                OrigineWC, ChoixNatDecibel, CLimRaw, CLimComp, TagName, MaskBeyondDetection, ...
                'typeAlgoWC', typeAlgoWC, 'MaxImageSize', MaxImageSize, ...
                'skipAlreadyProcessedFiles', skipAlreadyProcessedFiles, 'Carto', Carto);
            %                 'statusDepthAndAcrossDistLimits', statusDepthAndAcrossDistLimits);
            
%             TypeFileWCOutput = 2;
            if TypeFileWCOutput == 2 % Netcdf
                flag = WC_3DMatrix_XML2Netcdf(Names);
                if flag
                    flag = WC_3DMatrix_RemoveXMLBin(Names); %#ok<NASGU>
                end
            end
        end
        
        
        function flagProcessingHasBeenDone = createAndExportDTM(nomFic, nomDirDTM, TideFile, nomDirMOS, ReflecVideoInverse, nomFicCompensationCurves, flagGoogleEarth)
            
            flagProcessingHasBeenDone = 0;
            
            [~, filename] = fileparts(nomFic);
            
            %% R�pertoires DTM
            
            flag = RobotUtils.Common.mkdirIfNecessary(nomDirDTM);
            if ~flag
                return
            end
            
            nomDirDTMGLOBE = fullfile(nomDirDTM, 'GLOBE');
            flag = RobotUtils.Common.mkdirIfNecessary(nomDirDTMGLOBE);
            if ~flag
                return
            end
            
            nomDirDTMGoogle = fullfile(nomDirDTM, 'Google');
            flag = RobotUtils.Common.mkdirIfNecessary(nomDirDTMGoogle);
            if ~flag
                return
            end
            
            nomDirDTMErs = fullfile(nomDirDTM, 'ERS');
            flag = RobotUtils.Common.mkdirIfNecessary(nomDirDTMErs);
            if ~flag
                return
            end
            
            %% R�pertoires MOS
            
            flag = RobotUtils.Common.mkdirIfNecessary(nomDirMOS);
            if ~flag
                return
            end
            
            nomDirMOSGLOBE = fullfile(nomDirMOS, 'GLOBE');
            flag = RobotUtils.Common.mkdirIfNecessary(nomDirMOSGLOBE);
            if ~flag
                return
            end
            
            nomDirMOSGoogle = fullfile(nomDirMOS, 'Google');
            flag = RobotUtils.Common.mkdirIfNecessary(nomDirMOSGoogle);
            if ~flag
                return
            end
            
            nomDirMOSErs = fullfile(nomDirMOS, 'ERS');
            flag = RobotUtils.Common.mkdirIfNecessary(nomDirMOSErs);
            if ~flag
                return
            end
            
            %% Initialisation communes � DTM et MOS
            
            %             InfoMos.TypeMos            = 2;
            %             InfoMos.TypeGrid           = 2;
            %             InfoMos.flagMasquageDual   = 0;
            %             InfoMos.LimitAngles        = [];
            %             InfoMos.gridSize           = [];
            %             InfoMos.skipExistingMosaic = 0;
            %
            %             InfoMos.Import.MasqueActif  = 1;
            %             InfoMos.Import.SlotFilename = [];
            %             InfoMos.Import.SonarTime    = [];
            %
            %             InfoMos.MasqueActif  = 1;
            %             InfoMos.SlotFilename = [];
            %             InfoMos.SonarTime    = [];
            %
            %             InfoMos.Covering.Priority           = 1;
            %             InfoMos.Covering.SpecularLimitAngle = [];
            
            I0 = cl_image.empty;
            
            %% Cr�ation de la mosa�que de r�flectivit�
            
            nomFicMOS = fullfile(nomDirMOSErs,    [filename '_Reflectivity_LatLong.ers']);
            nomFicKmz = fullfile(nomDirMOSGoogle, [filename '_Reflectivity_LatLong.kmz']);
            if ~exist(nomFicMOS, 'file') || ~exist(nomFicKmz, 'file')
                if iscell(nomFicCompensationCurves)
                    if ~isempty(nomFicCompensationCurves) && (length(nomFicCompensationCurves) == 1) && isempty(nomFicCompensationCurves{1})
                        nomFicCompensationCurves = {}; % TODO : r�soudre le pb en amaont
                    end
                else
                    if ischar(nomFicCompensationCurves)
                        nomFicCompensationCurves = {nomFicCompensationCurves};
                    end
                end
                %                 InfoCompensationCurve.FileName                 = nomFicCompensationCurves;
                %                 InfoCompensationCurve.ModeDependant            = 1;
                %                 InfoCompensationCurve.UseModel                 = 0;
                %                 InfoCompensationCurve.PreserveMeanValueIfModel = 0;
                
                %                 InfoMos.DirName = nomDirMOSErs;
                
                selectionData      = 1; % Reflectivity
                skipExistingMosaic = 0;
                %                 nomFicCompensationCurves = [];
                flag = ResonMosaiqueParProfil(nomDirMOSErs, nomFic, [], nomFicCompensationCurves, 0, selectionData, [5 5], skipExistingMosaic);
                RobotUtils.Common.printExportedFilename(nomFicMOS, 'Success', flag)
                
                if flag && exist(nomFicMOS, 'file')
                    [flag, b] = cl_image.import_ermapper(nomFicMOS);
                    if flag
                        flagProcessingHasBeenDone = 1;
                        b.CLim = '0.5%';
                        
                        nomLayer = [filename '_Reflectivity_texture_v2'];
                        nomFic3D = nomLayer;
                        flagExportTexture   = 1;
                        flagExportElevation = 0;
                        flag = export_3DViewer_InGeotifTiles(b, nomLayer, nomFic3D, nomDirMOSGLOBE, flagExportTexture, flagExportElevation);
                        if flag
                            RobotUtils.Common.printExportedFilename(nomFic3D)
                        end
                        
                        if ReflecVideoInverse
                            b.Video = 2;
                        end
                        
                        flag = export_GoogleEarth(b, 'nomFic', nomFicKmz);
                        if flag
                            RobotUtils.Common.printExportedFilename(nomFicKmz)
                        end
                        
                        if flagGoogleEarth
                            if flag
                                winopen(nomFicKmz)
                            end
                        else
                            imagesc(b);
                            drawnow
                        end
                    end
                end
            end
            
            %% Cr�ation du DTM
            
            nomFicDTM = fullfile(nomDirDTMErs,    [filename '_Bathymetry_LatLong.ers']);
            nomFicKmz = fullfile(nomDirDTMGoogle, [filename '_Bathymetry_LatLong.kmz']);
            
            if ~exist(nomFicDTM, 'file') || ~exist(nomFicKmz, 'file')
                %                 InfoMos.DirName = nomDirDTMErs;
                
                selectionData            = 2; % Bathy
                skipExistingMosaic       = 0;
                nomFicCompensationCurves = [];
                
                flag = ResonMosaiqueParProfil(nomDirDTMErs, nomFic, [], nomFicCompensationCurves, 0, selectionData, [5 5], skipExistingMosaic, ...
                    'TideFile', TideFile); % Modif JMA le 12/03/2021 'TideFile', [] avant
                if flag
                    RobotUtils.Common.printExportedFilename(nomFicMOS)
                end
                
                if flag && exist(nomFicDTM, 'file')
                    [flag, b] = cl_image.import_ermapper(nomFicDTM);
                    if flag
                        flagProcessingHasBeenDone = 1;
                        b.CLim = '0.5%';
                        b.ColormapIndex = 20;
                        
                        nomLayer = [filename '_bathy_elevation_v2'];
                        nomFic3D = [filename '_bathy'];
                        flagExportTexture   = 0;
                        flagExportElevation = 1;
                        export_3DViewer_InGeotifTiles(b, nomLayer, nomFic3D, nomDirDTMGLOBE, flagExportTexture, flagExportElevation);
                        if flag
                            RobotUtils.Common.printExportedFilename(nomFic3D)
                        end
                        
                        c = sunShadingGreatestSlope(b, 0.5);
                        flag = export_GoogleEarth(c, 'nomFic', nomFicKmz);
                        if flag
                            RobotUtils.Common.printExportedFilename(nomFicKmz)
                        end
                        
                        if flagGoogleEarth
                            if flag
                                winopen(nomFicKmz)
                            end
                        else
                            imagesc(c);
                            drawnow
                        end
                    end
                end
            end
        end
        
        
        function Fig = plotAndExportNavigation(a, nomFic, nomDirNav, Fig, flagGoogleEarth)
            
            try
                Colororder = colororder;
            catch % Car la fonction colororder n'est pas reconnue en standalone !!!
                Colororder = [0   0.447000000000000   0.741000000000000
                    0.850000000000000   0.325000000000000   0.098000000000000
                    0.929000000000000   0.694000000000000   0.125000000000000
                    0.494000000000000   0.184000000000000   0.556000000000000
                    0.466000000000000   0.674000000000000   0.188000000000000
                    0.301000000000000   0.745000000000000   0.933000000000000
                    0.635000000000000   0.078000000000000   0.184000000000000];
            end
            nbColors = size(Colororder, 1);
            [~, nomFicSeul] = fileparts(nomFic);
            LineName = ['Nav-' nomFicSeul];
            
            %% Cr�ation du r�pertoire Navigation
            
            flag = RobotUtils.Common.mkdirIfNecessary(nomDirNav);
            if ~flag
                return
            end
            
            %% Cr�ation du r�pertoire Google
            
            nomDirNavGoogle = fullfile(nomDirNav, 'Google');
            flag = RobotUtils.Common.mkdirIfNecessary(nomDirNavGoogle);
            if ~flag
                return
            end
            nomFicGE = fullfile(nomDirNavGoogle, [LineName '.kmz']);
            flagExistGE = exist(nomFicGE, 'file');
            
            %% Cr�ation du r�pertoire GLOBE
            
            nomDirNavGlobe = fullfile(nomDirNav, 'GLOBE');
            flag = RobotUtils.Common.mkdirIfNecessary(nomDirNavGlobe);
            if ~flag
                return
            end
            NomFicXml = fullfile(nomDirNavGlobe, [LineName '.xml']);
            flagExistXml = exist(NomFicXml, 'file');
            
            %% Lecture de la navigation
            
            % TODO : je crois qu'il y a une fonction qui fait �a directement, mais la quelle ?
            
            if ~flagExistGE ||~flagExistXml
                identSondeur = get(a, 'SounderName');
                Data = read_navigation(a, identSondeur);
                if isempty(Data) || isempty(Data.Latitude)
                    return
                end
                
                [flag, DataDepth] = read_depth_s7k(a);
                if ~flag
                    return
                end
                
                S0 = cl_reson_s7k.empty();
                [SonarTime, Lat, Lon, Heading, CourseVessel, Immersion, provenance] = getNavFromVariousDatagrams(S0, Data, DataDepth); %#ok<ASGLU>
                Datetime = datetime(SonarTime.timeMat, 'ConvertFrom', 'datenum');
                
                kCoul = randi(nbColors);
                Color = Colororder(kCoul,:);
                
                Label = [char(Datetime(1)) ' - ' char(Datetime(end))];
                
                %% Cr�ation de la navigation dans Google Earth
                
                flagNewFile = RobotUtils.Common.plotNavGoogleGlobe(Lat, Lon, Color, LineName, Label, ...
                    flagExistGE, nomFicGE, flagGoogleEarth, ...
                    flagExistXml, Datetime, NomFicXml);
                if flagNewFile
                    Fig = plot_nav_S7K(a, 'Fig', Fig);
                end
            end
        end
        
        
        function WCDProcessing(nomFic, paramsWC)
            
            [~, nomFicSeul] = fileparts(nomFic);
            
            %% Cr�ation des �chogrammes polaires
            
            if ~isempty(paramsWC) && ~isempty(paramsWC.nomDirOut)
                RobotUtils.S7k.createAndExportWCD(nomFic, paramsWC);
            end
            
            %% Cr�ation des �chogrammes polaires au format 3D-Matrix
            
            nomFicNetcdf = fullfile(paramsWC.NomDirOut_3DMatrix, [nomFicSeul '_WCCube_PolarEchograms.nc']);
            if ~isempty(paramsWC) && ~isempty(paramsWC.NomDirOut_3DMatrix) ...
                    && ~exist(nomFicNetcdf, 'file')
                RobotUtils.S7k.createAndExportWCD_3DMatrix(nomFic, paramsWC);
            end
            
            %% Cr�ation des WC-AlongNav
            
            if ~isempty(paramsWC) && ~isempty(paramsWC.NomDirOut_AlongNav)
                RobotUtils.Common.createAndExportWCD_AlongNav(nomFic, paramsWC);
            end
        end
    end
end
