classdef InputParams
    
    methods (Static)
        
        function [flag, nomDirMovie, CLim, fps, quality, profileVideo, repExport] = matrixMovie(repExport)
                        
            CLim         = [];
            fps          = [];
            quality      = [];
            profileVideo = [];
            
            QL = get_LevelQuestion;
            
            %% Selection du fichier de sortie
            
            [flag, nomDirMovie] = my_uigetdir(repExport, 'Select the output movies folder');
            if ~flag
                return
            end
            repExport = nomDirMovie;
            
            %% Bornes de rehaussement de contraste
            
            [flag, CLim] = WCDUtils.InputParams.getCLim();
            if ~flag
                return
            end
            
            %% Fr�quence de rafraichissement des images
            
            str1 = 'TODO';
            str2 = 'Movie maker parametres';
            p(1) = ClParametre('Name', Lang('Frames par seconde','Frames per second'),...
                'Unit', 'Hz', 'MinValue', 1, 'MaxValue', 25,  'Value', 2);
            p(2) = ClParametre('Name', Lang('Qualit� de la compression','Compression Quality'), ...
                'Unit', 'Hz', 'MinValue', 1, 'MaxValue', 100, 'Value', 75);
            a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
            % a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
            a.sizes = [-2 -2 -1 0];
            a.openDialog;
            flag = a.okPressedOut;
            if ~flag
                return
            end
            X = a.getParamsValue;
            fps     = X(1);
            quality = X(2);
            
            %% Encodeur
            
            pppp = 4;
            str = {'Archival:  Motion JPEG 2000 file with lossless compression'; ...
                'Motion JPEG AVI: Compressed AVI file using Motion JPEG codec'; ...
                'Motion JPEG 2000: Compressed Motion JPEG 2000 file'; ...
                'MPEG-4: Compressed MPEG-4 file with H.264 encoding (Windows 7 systems only)'; ...
                'Uncompressed AVI: Uncompressed AVI file with RGB24 video'};
            if QL >= 3
                str1 = 'Codec Profile ?';
                str2 = 'Type de Codec ?';
                [pppp, flag] = my_listdlg(Lang(str1,str2), str, 'SelectionMode', 'Single', 'InitialValue', pppp);
                if ~flag
                    return
                end
            end
            [profileVideo, ~] = strtok(str{pppp}, ':');
        end
        
        
        function [flag, nomDirMovie, CLim, fps, quality, profileVideo, subPing, repExport] ...
                = matrixMoviePings(listeFic, repExport)
            
            subPing = [];
            
            [flag, nomDirMovie, CLim, fps, quality, profileVideo, repExport] ...
                = WCDUtils.InputParams.matrixMovie(repExport);
            if ~flag
                return
            end
            
            if ischar(listeFic)
                listeFic = {listeFic};
            end
            
            if length(listeFic) == 1
                [flag, Data] = NetcdfUtils.readGrpData(listeFic{1}, 'WaterColumnCube3D', 'XMLOnly', 1);
                if flag
                    subPing = 1:Data.Dimensions.nbPings;
                else
                    subPing = 1:100000;
                end
                [subPing, flag] = saisieBinsAxe(subPing, 'Ping numbers', 'Pings');
                if ~flag
                    return
                end
            end
        end
        
        
        function [flag, CLim, flagUnderAntenna, subPing, AngleWidth] = matrixDisplayNadir(listeFic)
            
            subPing          = [];
            flagUnderAntenna = 1;
            AngleWidth       = [];

            %% Bornes de rehaussement de contraste
            
            [flag, CLim] = WCDUtils.InputParams.getCLim();
            if ~flag
                return
            end

            %% Type de repr�sentation

            str{1} = 'Top is set to antenna';
            str{2} = 'Top is set to Immersion';
            [rep, flag] = my_listdlg('Top of the images', str, 'SelectionMode', 'Single');
            if ~flag
                return
            end
            flagUnderAntenna = (rep == 1);

            %% Limites angulaires au contact de fond marin

            p  = ClParametre('Name', Lang('Valeur maximale', 'Max value'), 'Unit', 'deg', 'Value', 15);
            a = StyledSimpleParametreDialog('params', p, 'Title', 'Angular limit on seabed');
            a.openDialog;
            flag = a.okPressedOut;
            if ~flag
                return
            end
            AngleWidth = a.getParamsValue;

            %% Sous-�chantillonnage des pings

            if ischar(listeFic)
                listeFic = {listeFic};
            end
            
            if length(listeFic) == 1
                [flag, Data] = NetcdfUtils.readGrpData(listeFic{1}, 'WaterColumnCube3D', 'XMLOnly', 1);
                if flag
                    subPing = 1:Data.Dimensions.nbPings;
                else
                    subPing = 1:100000;
                end
                [subPing, flag] = saisieBinsAxe(subPing, 'Ping numbers', 'Pings');
                if ~flag
                    return
                end
            end
        end


        function [flag, listFig, showLimits] = matrixSPFEStep2
            listFig    = matlab.ui.Figure.empty();
            showLimits = true;

            names = {};
            h = findobj('Type', 'figure');
            for k=1:length(h)
                x = getappdata(h(k), 'WCDNadirThroughDatetime');
                if ~isempty(x)
                    listFig(end+1) = h(k); %#ok<AGROW>
                    names{end+1} = x; %#ok<AGROW> 
                end
            end
            if isempty(listFig)
                str1 = 'TODO';
                str2 = 'No figure of WCDNadirThroughDatetime available';
                my_warndlg(Lang(str1,str2), 1);
                flag = 0;
                return
            end
            [rep, flag] = my_listdlg('Liste of items', names, 'InitialValue', 1:length(names));
            if ~flag
                return
            end
            if isempty(rep)
                flag = 0;
                return
            end
            listFig = listFig(rep);

            [ind, flag] = my_questdlg('Draw lines limits ?');
            if ~flag
                return
            end
            showLimits = (ind == 1);
        end
        
        
        function [flag, CLim] = getCLim
            
            persistent persistent_CLim
            
            %% Bornes de rehaussement de contraste
            
            if isempty(persistent_CLim)
                Clim = [-84 10];
            else
                Clim = persistent_CLim;
            end
            [flag, CLim] = saisie_CLim(Clim(1), Clim(2), 'dB');
            if ~flag
                return
            end
            persistent_CLim = CLim;
        end
        
        
        function [flag, flagPlotCross] = questionCrossOverImages
            [ind, flag] = my_questdlg('Plot cross over the images ?', 'Init', 2);
            if ~flag
                flagPlotCross = [];
                return
            end
            flagPlotCross = (ind == 1);
        end
        
        
        
        function [flag, listFileNames, backgroundImage, repImport] = getDepthAcrossDistCheck2(...
                this, repImport, ChaineIncluse)
            
            backgroundImage = cl_image.empty;
                        
            [flag, listFileNames, repImport] = uiSelectFiles('ExtensionFiles', {'.xml'; '.nc'}, 'AllFilters', 1, ...
                'RepDefaut', repImport, 'ChaineIncluse', ChaineIncluse);
            if ~flag
                return
            end
            
            if this.GeometryType == cl_image.indGeometryType('LatLong')
                str1 = 'Voulez-vous visualiser l''image courante en arri�re plan ?';
                str2 = 'Do you want to put the current image in background ?';
                [rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
                if ~flag
                    return
                end
                flagImageInBackground = (rep == 1);
            else
                flagImageInBackground = 0;
                str1 = 'Si la g�om�trie de l''image courante �tait de type "LatLong", alors il serait possible d''afficher cette image en background de la navigation.';
                str2 = 'If the current image was a "LatLong" geometry type, it could be possible to display it in background of the navigation display.';
                my_warndlg(Lang(str1,str2), 1);
            end
            
            if flagImageInBackground
                backgroundImage = this;
            end
        end
        
        
        function[flag, listeFic, LayerType, backgroundImage, CLim, muteUnderSeabed, repImport] = getMatrixCheck(this, repImport)
            
            LayerType       = 'Raw';
            CLim            = [];
            muteUnderSeabed = [];
            
            [flag, listeFic, backgroundImage, repImport] = WCDUtils.InputParams.getDepthAcrossDistCheck2(...
                this, repImport, '_WCCube');
            if flag
                strLayerType = {'Raw'; 'Comp'};
                [rep, flag] = my_listdlg('Selet the type of data', strLayerType, 'SelectionMode', 'Single');
                if ~flag
                    return
                end
                LayerType = strLayerType{rep};
                [flag, CLim] = WCDUtils.InputParams.getCLim();
                if flag
                    [ind, flag] = my_questdlg('Do you want to remove data under the seafloor in case it has not been already done in the data ? (answer No if you are checking passive mode WCD)');
                    if ~flag
                        return
                    end
                    muteUnderSeabed = (ind == 1);
                end
            end
        end
        
        
        function[flag, listeFic, LayerType, backgroundImage, CLim, muteUnderSeabed, repImport] = getSampleBeamCheck(this, repImport)
            
            LayerType       = 'Raw';
            CLim            = [];
            muteUnderSeabed = [];
            
            [flag, listeFic, backgroundImage, repImport] = WCDUtils.InputParams.getDepthAcrossDistCheck2(...
                this, repImport, 'SampleBeam');
            if flag
                strLayerType = {'Raw'; 'Comp'};
                [rep, flag] = my_listdlg('Selet the type of data', strLayerType, 'SelectionMode', 'Single');
                if ~flag
                    return
                end
                LayerType = strLayerType{rep};
                [flag, CLim] = WCDUtils.InputParams.getCLim();
                if flag
                    [ind, flag] = my_questdlg('Do you want to remove data under the seafloor in case it has not been already done in the data ? (answer No if you are checking passive mode WCD)');
                    if ~flag
                        return
                    end
                    muteUnderSeabed = (ind == 1);
                end
            end
        end
    end
end
