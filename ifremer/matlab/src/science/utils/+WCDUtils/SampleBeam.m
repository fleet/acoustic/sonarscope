classdef SampleBeam
    % Collection of static functions TODO
    %
    % Authors  : JMA Authors
    % See also : TODO
    %
    %   nomFic = 'C:\Temp\0003_20210822_085431_raw_Raw_SampleBeam.xml';
    %   WCDUtils.SampleBeam.movie('Pings',            nomFic, my_tempdir, [10 60], 'fps', 5, 'AngleWidth', 5)
    %   WCDUtils.SampleBeam.movie('VerticalSlices',   nomFic, my_tempdir, [10 60], 'PingWidth', 5)
    %   WCDUtils.SampleBeam.movie('HorizontalSlices', nomFic, my_tempdir, [10 60], 'PingWidth', 5)
    %   WCDUtils.SampleBeam.check(nomFic, [10 60])
    %
    %   nomFic = 'C:\Temp\0003_20210822_085431_raw_Raw_SampleBeam.xml';
    %   WCDUtils.SampleBeam.check(nomFic, [-65 -10])
    %   WCDUtils.SampleBeam.check(nomFic, [-10  25], 'LayerType', 'Comp')
    %   WCDUtils.SampleBeam.check(nomFic, [-65 -10], 'muteUnderSeabed', 0)
    %
    %   nomFic = 'C:\Temp\0003_20210822_085431_raw_Raw_SampleBeam.xml';
    %   WCDUtils.SampleBeam.check(nomFic, [-85 10])
    %
    %   nomFic = 'C:\Temp\0003_20210822_085431_raw_Raw_SampleBeam.xml';
    %   WCDUtils.SampleBeam.check(nomFic, [-85 -10])
    %
    % Reference pages in Help browser
    %   <a href="matlab:doc XMLBinUtils" >WC3DSampleBeam</a>
    %  -------------------------------------------------------------------------------
    
    methods (Static)
        
        function movie(direction, nomFicXml, nomDirMovie, CLim, varargin)
            
            [varargin, fps]          = getPropertyValue(varargin, 'fps',          25);
            [varargin, quality]      = getPropertyValue(varargin, 'quality',      75);
            [varargin, profileVideo] = getPropertyValue(varargin, 'profileVideo', 'MPEG-4');
            
            if ~iscell(nomFicXml)
                nomFicXml = {nomFicXml};
            end
            
            for k=1:length(nomFicXml)
                WCDUtils.SampleBeam.movie_unit(direction, nomFicXml{k}, nomDirMovie, CLim, fps, quality, profileVideo, varargin{:});
            end
        end
        
        
        function movie_unit(direction, nomFicXml, nomDirMovie, CLim, fps, quality, profileVideo, varargin)
            
            %% Lecture du fichier XML d�crivant la donn�e
            
            [flag, Data, Reflectivity, nomFic] = WCDUtils.SampleBeam.getMatrix(nomFicXml);
            if ~flag
                return
            end
            
            %% Cr�ation du film
            
            filename = strrep(nomFic, '_WCCube_PolarEchograms', '');
            % filename = strrep(filename, '_7150', '');
            switch direction
                case 'Pings'
                    Tag = 'Pgs';
                case 'VerticalSlices'
                    Tag = 'Vert';
                case 'HorizontalSlices'
                    Tag = 'Horz';
                otherwise % 'Pings'
                    return
            end
            filename = [filename '-' Tag];
            
            nomFicMovie = fullfile(nomDirMovie, filename);
            mov = VideoWriter(nomFicMovie, profileVideo);
            mov.FrameRate = fps;
            if ~strcmp(profileVideo, 'Uncompressed AVI')
                mov.Quality = quality;
            end
            
            open(mov);
            switch direction
                case 'Pings'
                    WCDUtils.SampleBeam.loopOnPings(mov, Reflectivity, Data, CLim, varargin{:})
                case 'VerticalSlices'
                    WCDUtils.SampleBeam.loopOnVerticalSlices(mov, Reflectivity, Data, CLim, varargin{:})
                case 'HorizontalSlices'
                    WCDUtils.SampleBeam.loopOnHorizontalSlices(mov, Reflectivity, Data, CLim, varargin{:})
            end
            close(mov);
            
            %% Affichage du film
            
            nomFicMovie = fullfile(nomDirMovie, mov.Filename);
            winopen(nomFicMovie)
            % if isdeployed
            %     winopen(nomFicMovie)
            % else
            %     try
            %         my_implay(nomFicMovie)
            %     catch %#ok<CTCH>
            %         winopen(nomFicMovie)
            %     end
            % end
            
            % info = mmfileinfo(nomFicMovie)
            % audio = info.Audio
            % video = info.Video
        end
        
        
        function loopOnPings(mov, Reflectivity, Data, CLim, varargin)
            
            [varargin, subPing]    = getPropertyValue(varargin, 'subPing',    []);
            [varargin, AngleWidth] = getPropertyValue(varargin, 'AngleWidth', 15); %#ok<ASGLU>
            
            %% Lecture de la matrice
            
            if isempty(subPing)
                subPing = 1:Data.Dimensions.nbPings;
            else
                subPing(subPing > Data.Dimensions.nbPings) = [];
                subPing(subPing < 1) = [];
                Reflectivity = Reflectivity(:,:,subPing);
            end
            [nbDepth, nbAcrossDist, nbPings] = size(Reflectivity); %#ok<ASGLU>

            %% Calcul de l'�chelle des Z
            
            ZMin = min(Data.Depth(subPing));
            Z = linspace(ZMin, 0, Data.Dimensions.nbRows);
            [~, nomSimple] = fileparts(mov.Filename);
            
            %% Slices along nav
            
            AcrossDistance = Data.AcrossDistance(:);
            Fig = figure;
            Fig.WindowState = 'maximized';
            drawnow
            pause(1)
            figPosition = Fig.Position;
            
            h1 = subplot(2, 1, 1);
            pings = subPing;
            y = Z;
            
            widthAcrossDist = ceil((abs(ZMin) * tand(AngleWidth/2)) / (AcrossDistance(2) - AcrossDistance(1)));
            
            kAcrossDist = find((Data.AcrossDistance >= 0), 1, 'first');
            if widthAcrossDist == 0
                I = squeeze(Reflectivity(:,kAcrossDist,:));
            else
                subAcrossDist = max(1,kAcrossDist-widthAcrossDist):min(nbAcrossDist,kAcrossDist+widthAcrossDist);
                I = squeeze(mean(Reflectivity(:,subAcrossDist,:), 2, 'omitnan'));
            end
            if ~all(isnan(I(:)))
                ADMin = Data.AcrossDistance(kAcrossDist-widthAcrossDist);
                ADMax = Data.AcrossDistance(kAcrossDist+widthAcrossDist);
                strMin = num2str(ADMin, '%0.2f');
                strMax = num2str(ADMax, '%0.2f');
                if widthAcrossDist == 0
                    Title = sprintf('IFREMER - SonarScope - %s\nSlices %d, Across distances %s m', ...
                        nomSimple,  kAcrossDist, strMin);
                else
                    Title = sprintf('IFREMER - SonarScope - %s\nSlices %d:%d, Across distances from %s m to %s m', ...
                        nomSimple,  kAcrossDist-widthAcrossDist, kAcrossDist+widthAcrossDist, strMin, strMax);
                end
                imagesc(h1, pings, y', I, CLim); axis(h1, 'xy'); title(h1, Title, 'Interpreter', 'none'); colormap(jet(256)); colorbar;
                xlabel('Ping #'); ylabel('Depth (m)'); hold on;
                hv = plot([subPing(1) subPing(1)], [ZMin 0], 'w');
            end
            
            %% Echogrammes polaires
            
            h2 = subplot(2, 1, 2);
            y = Z;
            step = 1;
            firstSlice = 1;
            hw = create_waitbar('Display Pings', 'N', nbPings);
            for kPing=1:step:nbPings
                my_waitbar(kPing, nbPings, hw)
                set(hv, 'XData', [subPing(kPing) subPing(kPing)]);
                
                I = squeeze(Reflectivity(:,:,kPing));
                if all(isnan(I(:)))
                    continue
                end
                
                Title = sprintf('IFREMER - SonarScope - %s : Ping %d / %d', nomSimple, kPing, nbPings);
                if firstSlice
                    firstSlice = 0;
                    hi = imagesc(h2, AcrossDistance, y, I, CLim);
                    axis(h2, 'xy'); axis(h2, 'equal', 'tight');
                    colormap(h2, jet(256)); colorbar(h2);
                    xlabel('Across distance (m)'); ylabel('Depth (m)');
                    ht = title(h2, Title, 'Interpreter', 'none');
                    hold on;
                    plot([ADMin ADMin], [ZMin 0], 'w');
                    plot([ADMax ADMax], [ZMin 0], 'w');
                    hold off;
                else
                    set(hi, 'CData', I);
                    ht.String = Title;
                end
                
                Fig.Position = figPosition;
                frame = getframe(Fig);
                writeVideo(mov, frame);
            end
            my_close(hw);
        end
        
        
        function loopOnVerticalSlices(mov, Reflectivity, Data, CLim, varargin)

            [varargin, PingWidth] = getPropertyValue(varargin, 'PingWidth', 0); %#ok<ASGLU>
            
            [nbDepth, nbAcrossDist, nbPings] = size(Reflectivity); %#ok<ASGLU>
            
            %% Calcul de l'�chelle des Z
            
            ZMin = min(Data.Depth);
            Z = linspace(ZMin, 0, Data.Dimensions.nbRows);
            [~, nomSimple] = fileparts(mov.Filename);
            
            %% Echograme du ping du milieu
            
            AcrossDistance = Data.AcrossDistance(:);
            Fig = figure;
            Fig.WindowState = 'maximized';
            drawnow
            pause(1)
            figPosition = Fig.Position;

            h1 = subplot(2, 1, 1);
            kPing = ceil(nbPings/2);
            if PingWidth == 0
                I = squeeze(Reflectivity(:,:,kPing));
            else
                subPings = max(1,kPing-PingWidth):min(nbPings,kPing+PingWidth);
                I = squeeze(mean(Reflectivity(:,:,subPings), 2, 'omitnan'));
            end
            
            if PingWidth == 0
                Title = sprintf('IFREMER - SonarScope - %s : Ping %d / %d', nomSimple, kPing, nbPings);
            else
                Title = sprintf('IFREMER - SonarScope - %s  -  Mean value of Pings %d:%d / %d', ...
                    nomSimple, subPings(1), subPings(end), nbPings);
            end

            y = Z;
            imagesc(h1, AcrossDistance, y, I, CLim); axis(h1, 'xy'); axis(h1, 'equal', 'tight'); title(h1, Title, 'Interpreter', 'none'); colormap(h1, jet(256)); colorbar(h1);
            xlabel('Across dist (m)'); ylabel('Depth (m)');
            hold on;
            hv = plot([1 1], [ZMin 0], 'w');
            hold off
            drawnow
            
            %% Vertical slices
            
            h2 = subplot(2, 1, 2);
            y = Z;
            step = 1;
            firstSlice = 1;
            hw = create_waitbar('Display vertical slices', 'N', nbAcrossDist);
            for kSliceVert=1:step:nbAcrossDist
                my_waitbar(kSliceVert, nbAcrossDist, hw)
                
                pings = 1:nbPings;
                I = squeeze(Reflectivity(:,kSliceVert,:));
                if all(isnan(I(:)))
                    continue
                end
                AcrossDist = Data.AcrossDistance(kSliceVert);
                strAcrossDist = num2str(AcrossDist, '%0.2f');
                Title = sprintf('IFREMER - SonarScope - %s\nSlice %d / %d, Across distance %s m', ...
                    nomSimple, kSliceVert, nbAcrossDist, strAcrossDist);
                if firstSlice
                    firstSlice = 0;
                    hi = imagesc(h2, pings, y', I, CLim);
                    axis(h2, 'xy');
                    colormap(jet(256)); colorbar;
                    xlabel('Ping #'); ylabel('Depth (m)');
                    ht = title(h2, Title, 'Interpreter', 'none');
                    
                    %                         ht = title(h2, setTitle2(kSliceVert), 'Interpreter', 'none'); % TODO
                    
                    % TODO : utiliser la visu de la m�thode check ...
                    
                    hold on;
                    subPings = max(1,kPing-PingWidth):min(nbPings,kPing+PingWidth);
                    plot([subPings(1)   subPings(1)],   [ZMin 0], 'w');
                    plot([subPings(end) subPings(end)], [ZMin 0], 'w');
                    hold off;
                else
                    set(hi, 'CData', I);
                    ht.String = Title;
                end
                set(hv, 'XData', [AcrossDist AcrossDist]);
                
            	Fig.Position = figPosition;
                frame = getframe(Fig);
                writeVideo(mov, frame);
            end
            my_close(hw);
        end
        
        
        function loopOnHorizontalSlices(mov, Reflectivity, Data, CLim, varargin)

            [varargin, PingWidth] = getPropertyValue(varargin, 'PingWidth', 0); %#ok<ASGLU>

            [nbDepth, nbAcrossDist, nbPings] = size(Reflectivity); %#ok<ASGLU>

            %% Calcul de l'�chelle des Z
            
            ZMin = min(Data.Depth);
            Z = linspace(ZMin, 0, Data.Dimensions.nbRows);
            [~, nomSimple] = fileparts(mov.Filename);
            
            %% Echograme du ping du milieu
            
            AcrossDistance = Data.AcrossDistance(:);
            Fig = figure;
            Fig.WindowState = 'maximized';
            drawnow
            pause(1)
            figPosition = Fig.Position;
            
            h1 = subplot(2, 1, 1);
            kPing = ceil(nbPings/2);
            if PingWidth == 0
                I = squeeze(Reflectivity(:,:,kPing));
            else
                subPings = max(1,kPing-PingWidth):min(nbPings,kPing+PingWidth);
                I = squeeze(mean(Reflectivity(:,:,subPings), 2, 'omitnan'));
            end
                        
            if PingWidth == 0
                Title = sprintf('IFREMER - SonarScope - %s : Ping %d / %d', nomSimple, kPing, nbPings);
            else
                Title = sprintf('IFREMER - SonarScope - %s  -  Mean value of Pings %d:%d / %d', ...
                    nomSimple, subPings(1), subPings(end), nbPings);
            end

            y = Z;
            imagesc(h1, AcrossDistance, y, I, CLim); axis(h1, 'xy'); axis(h1, 'equal', 'tight'); title(h1, Title, 'Interpreter', 'none'); colormap(h1, jet(256)); colorbar(h1);
            xlabel('Across dist (m)'); ylabel('Depth (m)');
            hold on;
            hv = plot(h1, [min(AcrossDistance) max(AcrossDistance)], [0 0], 'w');
            hold off;
            drawnow
            
            %% Horizontal slices
            
            h2 = subplot(2, 1, 2);
            y = AcrossDistance;
            pings = 1:nbPings;
            step = 1;
            firstSlice = 1;
            hw = create_waitbar('Display horizontal slices', 'N', nbDepth);
            for kSliceHorz=1:step:nbDepth
                my_waitbar(kSliceHorz, nbDepth, hw)
                
                I = squeeze(Reflectivity(kSliceHorz,:,:));
                if all(isnan(I(:)))
                    continue
                end
                Title = sprintf('IFREMER - SonarScope - %s\nSlice %d / %d, %s m', ...
                    nomSimple, kSliceHorz, nbDepth, num2str(Z(kSliceHorz), '%0.2f'));
                if firstSlice
                    firstSlice = 0;
                    hi = imagesc(h2, pings, y', I, CLim); axis(h2, 'xy');
                    ht = title(h2, Title, 'Interpreter', 'none');
                    colormap(jet(256)); colorbar;
                    xlabel('Ping #'); ylabel('Across distance (m)');
                    hold on;
                    subPings = max(1,kPing-PingWidth):min(nbPings,kPing+PingWidth);
                    plot([subPings(1)   subPings(1)],   [min(AcrossDistance) max(AcrossDistance)], 'w');
                    plot([subPings(end) subPings(end)], [min(AcrossDistance) max(AcrossDistance)], 'w');
                    hold off;
                else
                    set(hi, 'CData', I);
                    ht.String = Title;
                end
                set(hv, 'YData', [Z(kSliceHorz) Z(kSliceHorz)]);
                
            	Fig.Position = figPosition;
                frame = getframe(Fig);
                writeVideo(mov, frame);
            end
            my_close(hw);
        end
        
                    
        function [flag, Data, Reflectivity, nomFic, ReflectivityRaw, ReflectivityComp] = getMatrix(nomFicXml, varargin)
            
            [varargin, LayerType] = getPropertyValue(varargin, 'LayerType', 'Raw'); %#ok<ASGLU>
            
            Reflectivity     = [];
            ReflectivityRaw  = [];
            ReflectivityComp = [];
            
            [nomDir, nomFic] = fileparts(nomFicXml);
            nomFicNc = fullfile(nomDir, [nomFic '.nc']);
            str1 = sprintf('Lecture du fichier "%s"', nomFicNc);
            str2 = sprintf('Reading file "%s"', nomFicNc);
            WorkInProgress(Lang(str1,str2))
            if exist(nomFicNc, 'file')
                %         [flag, Data3DMatrix] = WC_readImageCube3D(nomFicXml, 'Memmapfile', 1); % Retester cet appel
                [flag, Data] = NetcdfUtils.readGrpData(nomFicNc, 'WaterColumnCube3D'); % 'PolarEchograms');
                if ~flag
                    return
                end
                ReflectivityRaw  = Data.ReflectivityRaw(:,:,:);
                ReflectivityComp = Data.ReflectivityComp(:,:,:);
                switch LayerType
                    case 'Raw'
                        Reflectivity = ReflectivityRaw;
                    case 'Comp'
                        Reflectivity = ReflectivityComp;
                    otherwise
                        flag = 0;
                        return
                end
            else
                [flag, Data] = XMLBinUtils.readGrpData(nomFicXml, 'Memmapfile', 0); % Reflectivity: [446�1437�871 cl_memmapfile]
                if ~flag
                    return
                end
                
                coefCompression = ceil(Data.Dimensions.nbRows / 1000);
                Data.Dimensions.nbRows = ceil(Data.Dimensions.nbRows / coefCompression);
                Reflectivity = NaN(Data.Dimensions.nbRows, Data.Dimensions.nbPings, Data.Dimensions.nbSlices, 'single');
                hw = create_waitbar('Reading .tif files', 'N', Data.Dimensions.nbSlices);
                for kPing=1:Data.Dimensions.nbSlices
                    my_waitbar(kPing, Data.Dimensions.nbSlices, hw)
                    [nomDr1, nomFic1] = fileparts(Data.SliceName{kPing});
                    nomDr1 = fileparts(nomDr1);
                    [IRaw, CLim, nomFicImage] = WCDUtils.SampleBeam.readPingWCDXML(nomDr1, nomFic1, kPing);
                    IRaw = downsize(IRaw, 'pasy', coefCompression, 'pasx', 1);
                    [n1IRaw,n2IRaw] = size(IRaw);
                    [n1IRef,n2IRef] = size(Reflectivity);
                    if n1IRaw > n1IRef
                        Reflectivity((n1IRef+1):n1IRaw,:,:) = single(NaN);
                    end
                    if n2IRaw > n2IRef
                        Reflectivity(:,(n2IRef+1):n2IRaw,:) = single(NaN);
                    end
                    Reflectivity(1:n1IRaw,1:n2IRaw,kPing) = IRaw;
                end
                my_close(hw)
                Reflectivity      = my_flipud(Reflectivity);
                ReflectivityRaw   = Reflectivity;
                ReflectivityComp  = [];
                Data.Reflectivity = Reflectivity;
            end
        end
        
        
        function check(nomFicXml, CLim, varargin)
            
            [varargin, LayerType]       = getPropertyValue(varargin, 'LayerType',      'Raw');
            [varargin, backgroundImage] = getPropertyValue(varargin, 'backgroundImage', []);
            [varargin, muteUnderSeabed] = getPropertyValue(varargin, 'muteUnderSeabed', 1); %#ok<ASGLU>
            
            if ~iscell(nomFicXml)
                nomFicXml = {nomFicXml};
            end
            
            CLimRaw  = CLim;
            CLimComp = [-10 25];
            WCDUtils.SampleBeam.InfoShortcuts;
            N = length(nomFicXml);
            hw = create_waitbar('Check 3DMatrix files', 'N', N);
            for k=1:N
                my_waitbar(k, N, hw)
                [CLimRaw, CLimComp] = WCDUtils.SampleBeam.check_unit(nomFicXml{k}, LayerType, CLimRaw, CLimComp, backgroundImage, muteUnderSeabed, 'Modal', N > 1);
                
                % Attente que la figure soit ferm�e car BOOM si lancement d'une s�rie de fichiers
                flag = question_ContinueNextFile('N', N, 'k', k);
                if ~flag
                    break
                end
            end
            my_close(hw);
        end
                
        
        function fig = InfoShortcuts
            str = {};
            str{end+1} = 'Right arrow : Next ping';
            str{end+1} = 'Left arrow  : Previous ping';
            str{end+1} = 'Up   arrow  : Depth up';
            str{end+1} = 'Down arrow  : Depth down';
            str{end+1} = 'Page down   : First Ping';
            str{end+1} = 'Page up     : Last Ping';
            str{end+1} = '+ : Zoom x2 of pings';
            str{end+1} = '- : Zoom /2 of pings';
            str{end+1} = 'c : Contrast 0.5% from current image';
            str{end+1} = 'Mouse left click on figures   : Set current point';
            str{end+1} = 'Mouse left click on colorbars : Contrast enhancement GUI';
            str{end+1} = 'Shift - Mouse left click on figures : Store the current point values in file "SSc-WCD-3DSampleBeam-Pointing.csv"';
            str{end+1} = 'f : Create figures';
            str{end+1} = '>   : Search max value and set the current point on this voxel';
            str{end+1} = 'Tab : Permutes Raw / Comp';
            fig = edit_infoPixel(str, [], 'Size', [100, 300], ...
                'PromptString', 'SonarScope Shortcuts for 3DSampleBeam WCD checking.');
        end
  
        
        function [CLimRaw, CLimComp] = check_unit(nomFicXml, LayerType, CLimRaw, CLimComp, backgroundImage, muteUnderSeabed, varargin)
            
            [varargin, Modal] = getPropertyValue(varargin, 'Modal', 0); %#ok<ASGLU>

            %% Lecture du fichier XML d�crivant la donn�e
            
            msg = ['Reading the matrix "' LayerType '", please wait.'];
            hw = my_warndlg(msg, 0, 'displayItEvenIfSSc', 1);
            [flag, Data, Reflectivity, nomFic, ReflectivityRaw, ReflectivityComp] = WCDUtils.SampleBeam.getMatrix(nomFicXml, 'LayerType', LayerType);
            if ~flag
                return
            end
            my_close(hw);
            
            muteUnderSeabed = muteUnderSeabed && isfield(Data, 'Mask');
                            
            [nbDepth, nbAcrossDist, nbPings] = size(Reflectivity);
            nomSimple = strrep(nomFic, '_WCCube_PolarEchograms', '');
            csvSep    = getCSVseparator;
            
            switch LayerType
                case 'Raw'
                    CLim = CLimRaw;
                case 'Comp'
                    CLim = CLimComp;
            end
            
            %% Calcul des valeurs des axes
            
%           figure; plot(Data.Depth, 'k'); grid on; hold on; plot(Data.Immersion, 'b'); 
            
            Data.Dimensions.nbRows = size(Reflectivity, 1);

            Z              = -Data.Dimensions.nbRows:-1;
            AcrossDistance = 1:Data.Dimensions.nbPings;
            pings          = 1:Data.Dimensions.nbSlices;
            
            Data.AcrossDistance = AcrossDistance;
            
            minAcross = AcrossDistance(1);
            maxAcross = AcrossDistance(end);
            minZ      = Z(1);
            maxZ      = Z(end);
            minPing   = pings(1);
            maxPing   = pings(end);
            
            %% Cr�ation de la figure
            
            Fig = FigUtils.createSScFigure;
            Fig.WindowState = 'maximized';
            hInfo = uicontrol(Fig, 'Style', 'text', 'Position', [10, 0, 1500, 20], 'HorizontalAlignment', 'left');
            
            kPingCurrent   = floor(nbPings / 2);
            kDepthCurrent  = floor(nbDepth / 2);
            kAcrossCurrent = floor(nbAcrossDist / 2);
            
            %% Vue DepthAcrossDist
            
            h1 = subplot(2, 2, 1);
            [h1Image, hc1, h1Title, h1LineDepth, h1LineAcross] = createImage1(h1, kPingCurrent, kDepthCurrent);
            
            %% Vue DepthPings
            
            h2 = subplot(2, 2, 2);
            [h2Image, hc2, h2Title, h2LineDepth, h2LinePing] = createImage2(h2, kAcrossCurrent, kDepthCurrent);

            %% Vue AcrossDistPings
            
            h3 = subplot(2, 2, 4);
            [h3Image, hc3, h3Title, h3LineAcross, h3LinePing] = createImage3(h3, kPingCurrent, kAcrossCurrent);
            linkaxes([h2 h3], 'x');
            linkaxes([h1 h2], 'y');
            
            %% Vue g�ographique
            
            h4 = subplot(2, 2, 3);
            [h4Latitude, h4Longitude, h4Swath, h4Across, hi4, EIRaw, EIComp, CLim4Raw, CLim4Comp] = createImage4(h4, kPingCurrent, kAcrossCurrent);
            
            %% Callbacks
            
            set(Fig, 'WindowButtonMotionFcn', @windowButtonMotion, 'BusyAction', 'cancel', 'Interruptible', 'Off')
            set(Fig, 'WindowButtonUpFcn',     @windowButtonDown,   'BusyAction', 'cancel', 'Interruptible', 'Off')
            set(Fig, 'KeyPressFcn',           @pressKey);

            set(hc1, 'ButtonDownFcn',         {@clbkColorbar; h1}, 'BusyAction', 'cancel', 'Interruptible', 'Off')
            set(hc2, 'ButtonDownFcn',         {@clbkColorbar; h2}, 'BusyAction', 'cancel', 'Interruptible', 'Off')
            set(hc3, 'ButtonDownFcn',         {@clbkColorbar; h3}, 'BusyAction', 'cancel', 'Interruptible', 'Off')
            
            %% Sortie
            
            if Modal
                uiwait(Fig);
            end
            
            
            function Title = setTitle1(k3)
                Title = sprintf('IFREMER - SonarScope - %s : Ping %d / %d', nomSimple, k3, nbPings);
            end
            
            
            function Title = setTitle2(k2)
                AcrossDist = Data.AcrossDistance(k2);
                strAcrossDist = num2str(AcrossDist, '%0.2f');
                Title = sprintf('IFREMER - SonarScope - %s\nSlice %d / %d, Across distance %s m', ...
                    nomSimple, k2, nbAcrossDist, strAcrossDist);
            end
            
            
            function Title = setTitle3(k1)
                Title = sprintf('IFREMER - SonarScope - %s\nSlice %d / %d, Z %s m', ...
                    nomSimple, k1, nbDepth, num2str(Z(k1), '%0.2f'));
            end
            
            
            function [h1Image, hc1, h1Title, h1LineDepth, h1LineAcross] = createImage1(hAxe, kPing, kDepth, varargin)
                
                [varargin, flagPlotCross] = getPropertyValue(varargin, 'flagPlotCross', 1); %#ok<ASGLU>
                
                if isempty(hAxe)
                    figure;
                    hAxe = subplot(1,1,1);
                else
                    flagPlotCross = 1;
                end
                
                I = squeeze(Reflectivity(:,:,kPing));
                if muteUnderSeabed
                    Masque = squeeze(Data.Mask(:,:,kPing));
                    I(Masque == 1) = NaN;
                end
                
                h1Image = imagesc(hAxe, AcrossDistance, Z, I, CLim);
                axis(hAxe, 'xy'); axis(hAxe, 'tight');
                colormap(hAxe, jet(256));
                hc1 = colorbar(hAxe);
                xlabel('Across distance (m)'); ylabel('Z (m)');
                h1Title = title(hAxe, setTitle1(kPing), 'Interpreter', 'none');
                
                if flagPlotCross
                    hold on;
                    h1LineDepth  = plot([minAcross maxAcross], [Z(kDepth) Z(kDepth)], 'w');
                    h1LineAcross = plot([kPing kPing], [minZ maxZ], 'w');
                    hold off;
                end
            end
            
            
            function [h2Image, hc2, h2Title, h2LineDepth, h2LinePing] = createImage2(hAxe, kAcross, kDepth, varargin)
                                
                [varargin, flagPlotCross] = getPropertyValue(varargin, 'flagPlotCross', 1); %#ok<ASGLU>
                
                if isempty(hAxe)
                    figure;
                    hAxe = subplot(1,1,1);
                else
                    flagPlotCross = 1;
                end
                
                I = squeeze(Reflectivity(:,kAcross,:));
                if muteUnderSeabed
                    Masque = squeeze(Data.Mask(:,kAcross,:));
                    I(Masque == 1) = NaN;
                end
                h2Image = imagesc(hAxe, pings, Z', I, CLim);
                axis(hAxe, 'xy');
                colormap(jet(256));
                hc2 = colorbar;
                xlabel('Ping #'); ylabel('Z (m)');
                h2Title = title(hAxe, setTitle2(kAcross), 'Interpreter', 'none');
                
                if flagPlotCross
                    hold on;
                    h2LineDepth = plot([minPing maxPing], [Z(kDepth) Z(kDepth)], 'w');
                    h2LinePing  = plot([kPingCurrent kPingCurrent], [minZ maxZ], 'w');
                    hold off;
                end
            end
            
            function [h3Image, hc3, h3Title, h3LineAcross, h3LinePing] = createImage3(hAxe, kPing, kAcross, varargin)
                
                [varargin, flagPlotCross] = getPropertyValue(varargin, 'flagPlotCross', 1); %#ok<ASGLU>
                
                if isempty(hAxe)
                    figure;
                    hAxe = subplot(1,1,1);
                else
                    flagPlotCross = 1;
                end                

                I = squeeze(Reflectivity(kDepthCurrent,:,:));
                if muteUnderSeabed
                    Masque = squeeze(Data.Mask(kDepthCurrent,:,:));
                    I(Masque == 1) = NaN;
                end
                
                h3Image = imagesc(hAxe, pings, AcrossDistance, I, CLim); axis(hAxe, 'xy');
                h3Title = title(hAxe, setTitle3(kDepthCurrent), 'Interpreter', 'none');
                colormap(jet(256));
                hc3 = colorbar;
                xlabel('Ping #'); ylabel('Across distance (m)');
                
                if flagPlotCross
                    hold on;
                    h3LineAcross = plot([minPing maxPing], [AcrossDistance(kAcross) AcrossDistance(kAcross)], 'w');
                    h3LinePing   = plot([kPing kPing], [minAcross maxAcross], 'w');
                    hold off;
                end
            end
            
            
            
            
            function [h4Latitude, h4Longitude, h4Swath, h4Across, hi4, EIRaw, EIComp, CLim4Raw, CLim4Comp] = createImage4(h4, kPingCurrent, kAcrossCurrent)
                EIComp    = [];
                CLim4Raw  = [];
                CLim4Comp = [];
                if isempty(backgroundImage)
                    hw = my_warndlg('Computing the geographic vue, please wait.', 0, 'displayItEvenIfSSc', 1);
                    
                    if ~isempty(ReflectivityRaw)
                        EIComp = ReflectivityRaw;
                        if muteUnderSeabed
                            S = squeeze(sum(int32(Data.Mask), 1));
                            EIComp(Data.Mask == 1) = NaN;
                        end
                        EIComp = squeeze(mean(EIComp, 1, 'omitnan'));
                        J = EIComp;
                        if muteUnderSeabed
                            J(S > (nbDepth * 0.75)) = NaN;
                        end
                        ValStats = stats(J);
                        CLim4Comp = ValStats.Quant_03_97;
                    end
                    
                    EIRaw = ReflectivityRaw;
                    if muteUnderSeabed
                        S = squeeze(sum(int32(Data.Mask), 1));
                        EIRaw(Data.Mask == 1) = NaN;
                    end
                    EIRaw = squeeze(mean(EIRaw, 1, 'omitnan'));
                    J = EIRaw;
                    
                    hi4 = pcolor(Data.LongitudeBottom', Data.LatitudeBottom', EIRaw); shading flat;
                    colormap(jet(256));
                    hc4 = colorbar;
                    set(hc4, 'ButtonDownFcn', @clbkColorbarH4EI, 'BusyAction', 'cancel', 'Interruptible', 'Off')
                    if muteUnderSeabed
                        J(S > (nbDepth * 0.75)) = NaN;
                    end
                    ValStats = stats(J);
                    CLim4Raw = ValStats.Quant_03_97;
                    set(h4, 'CLim', CLim4Raw)
                    my_close(hw);
                else
                    h4Image = imagesc(backgroundImage, 'Axe', h4); hold on; %#ok<NASGU> % TODO : provoque un bug � plot3
                    hc4 = colorbar;
                    set(hc4, 'ButtonDownFcn', @clbkColorbarH4, 'BusyAction', 'cancel', 'Interruptible', 'Off')
                    hi4       = [];
                    EIRaw     = [];
                    EIComp    = [];
                    CLim4Raw  = [];
                    CLim4Comp = [];
                end
                
                hold on;
                minLat = min(Data.LatitudeTop(:));
                maxLat = max(Data.LatitudeTop(:));
                minLon = min(Data.LongitudeTop(:));
                maxLon = max(Data.LongitudeTop(:));
                lonCurrent = Data.LongitudeTop(kPingCurrent, kAcrossCurrent);
                latCurrent = Data.LatitudeTop( kPingCurrent, kAcrossCurrent);
                h4Latitude  = plot([minLon maxLon], [latCurrent latCurrent], 'k');
                h4Longitude = plot([lonCurrent lonCurrent], [minLat maxLat], 'k');
                h4Swath     = plot(Data.LongitudeTop(kPingCurrent,:),   Data.LatitudeTop(kPingCurrent,:),   'w');
                h4Across    = plot(Data.LongitudeTop(:,kAcrossCurrent), Data.LatitudeTop(:,kAcrossCurrent), 'w');
                hold off;
                axis normal
                h4.XLim = [minLon maxLon];
                h4.YLim = [minLat maxLat];
                axisGeo(h4);
            end
            
            
            function updataImage1(AcrossMouse, DepthMouse)
                kAcrossDist = find((Data.AcrossDistance >= AcrossMouse), 1, 'first');
                if isempty(kAcrossDist)
                    return
                end
                kDepth = find((Z >= DepthMouse), 1, 'first');
                if isempty(kDepth)
                    return
                end
                
                set(h1LineDepth,  'YData', [DepthMouse  DepthMouse]);
                set(h1LineAcross, 'XData', [AcrossMouse AcrossMouse]);
                set(h2LineDepth,  'YData', [DepthMouse  DepthMouse]);
                set(h3LineAcross, 'YData', [AcrossMouse AcrossMouse]);
                
                I = squeeze(Reflectivity(:,kAcrossDist,:));
                if muteUnderSeabed
                    Masque = squeeze(Data.Mask(:,kAcrossDist,:));
                    I(Masque == 1) = NaN;
                end
                set(h2Image, 'CData', I);
                
                I = squeeze(Reflectivity(kDepth,:,:));
                if muteUnderSeabed
                    Masque = squeeze(Data.Mask(kDepth,:,:));
                    I(Masque == 1) = NaN;
                end
                set(h3Image, 'CData', I);
                
                h2Title.String = setTitle2(kAcrossDist);
                h3Title.String = setTitle3(kDepth);
                
                LongitudeMouse = Data.LongitudeTop(kPingCurrent, kAcrossDist);
                LatitudeMouse  = Data.LatitudeTop( kPingCurrent, kAcrossDist);
                set(h4Latitude,  'YData', [LatitudeMouse  LatitudeMouse]);
                set(h4Longitude, 'XData', [LongitudeMouse LongitudeMouse]);
                set(h4Swath,  'XData', Data.LongitudeTop(kPingCurrent,:), 'YData', Data.LatitudeTop(kPingCurrent,:));
                set(h4Across, 'XData', Data.LongitudeTop(:,kAcrossDist),  'YData', Data.LatitudeTop(:,kAcrossDist));
                
                setInfo(kPingCurrent, kAcrossDist, kDepth);
            end
            
            
            function updataImage2(PingMouse, DepthMouse)
                kDepth = find((Z     >= DepthMouse) , 1, 'first');
                kPing  = find((pings >= PingMouse) ,  1, 'first');
                if isempty(kPing)
                    return
                end
                
                set(h2LineDepth, 'YData', [DepthMouse DepthMouse]);
                set(h2LinePing,  'XData', [PingMouse  PingMouse]);
                set(h1LineDepth, 'YData', [DepthMouse DepthMouse]);
                set(h3LinePing,  'XData', [PingMouse  PingMouse]);
                
                I = squeeze(Reflectivity(:,:,kPing));
                if muteUnderSeabed
                    Masque = squeeze(Data.Mask(:,:,kPing));
                    I(Masque == 1) = NaN;
                end
                set(h1Image, 'CData', I);
                
                I = squeeze(Reflectivity(kDepth,:,:));
                if muteUnderSeabed
                    Masque = squeeze(Data.Mask(kDepth,:,:));
                    I(Masque == 1) = NaN;
                end
                set(h3Image, 'CData', I);
                
                h1Title.String = setTitle1(kPing);
                h3Title.String = setTitle3(kDepth);
                
                LongitudeMouse = Data.LongitudeTop(kPing, kAcrossCurrent);
                LatitudeMouse  = Data.LatitudeTop( kPing, kAcrossCurrent);
                set(h4Latitude,  'YData', [LatitudeMouse  LatitudeMouse]);
                set(h4Longitude, 'XData', [LongitudeMouse LongitudeMouse]);
                set(h4Swath,  'XData', Data.LongitudeTop(kPing,:),          'YData', Data.LatitudeTop(kPing,:));
                set(h4Across, 'XData', Data.LongitudeTop(:,kAcrossCurrent), 'YData', Data.LatitudeTop(:,kAcrossCurrent));
                
                setInfo(kPing, kAcrossCurrent, kDepth);
            end
            
            
            function updataImage3(PingMouse, AcrossMouse)
                set(h3LineAcross, 'YData', [AcrossMouse AcrossMouse]);
                set(h3LinePing,   'XData', [PingMouse   PingMouse]);
                set(h1LineAcross, 'XData', [AcrossMouse AcrossMouse]);
                set(h2LinePing,   'XData', [PingMouse   PingMouse]);
                
                kAcrossDist = find((Data.AcrossDistance >= AcrossMouse), 1, 'first');
                if isempty(kAcrossDist)
                    return
                end
                kPing  = find((pings >= PingMouse), 1, 'first');
                if isempty(kPing)
                    return
                end
                
                I = squeeze(Reflectivity(:,:,kPing));
                if muteUnderSeabed
                    Masque = squeeze(Data.Mask(:,:,kPing));
                    I(Masque == 1) = NaN;
                end
                set(h1Image, 'CData', I);
                
                I = squeeze(Reflectivity(:,kAcrossDist,:));
                if muteUnderSeabed
                    Masque = squeeze(Data.Mask(:,kAcrossDist,:));
                    I(Masque == 1) = NaN;
                end
                set(h2Image, 'CData', I);
                
                h1Title.String = setTitle1(kPing);
                h2Title.String = setTitle2(kAcrossDist);
                
                LongitudeMouse = Data.LongitudeTop(kPing, kAcrossDist);
                LatitudeMouse  = Data.LatitudeTop( kPing, kAcrossDist);
                
                set(h4Latitude,  'YData', [LatitudeMouse  LatitudeMouse]);
                set(h4Longitude, 'XData', [LongitudeMouse LongitudeMouse]);
                set(h4Swath,  'XData', Data.LongitudeTop(kPing,:),       'YData', Data.LatitudeTop(kPing,:));
                set(h4Across, 'XData', Data.LongitudeTop(:,kAcrossDist), 'YData', Data.LatitudeTop(:,kAcrossDist));
                
                setInfo(kPing, kAcrossDist, kDepthCurrent);
            end
            
            
            function updataImage4(LongitudeMouse, LatitudeMouse, PingMouse, AcrossMouse)
                set(h4Latitude,   'YData', [LatitudeMouse  LatitudeMouse]);
                set(h4Longitude,  'XData', [LongitudeMouse LongitudeMouse]);
                
                set(h1LineAcross, 'XData', [AcrossMouse AcrossMouse]);
                set(h3LineAcross, 'YData', [AcrossMouse AcrossMouse]);
                set(h2LinePing,   'XData', [PingMouse   PingMouse]);
                set(h3LinePing,   'XData', [PingMouse   PingMouse]);
                
                kAcrossDist = find((Data.AcrossDistance >= AcrossMouse), 1, 'first');
                if isempty(kAcrossDist)
                    return
                end
                kPing = find((pings >= PingMouse), 1, 'first');
                if isempty(kPing)
                    return
                end
                
                I = squeeze(Reflectivity(:,:,kPing));
                if muteUnderSeabed
                    Masque = squeeze(Data.Mask(:,:,kPing));
                    I(Masque == 1) = NaN;
                end
                set(h1Image, 'CData', I);
                
                I = squeeze(Reflectivity(:,kAcrossDist,:));
                if muteUnderSeabed
                    Masque = squeeze(Data.Mask(:,kAcrossDist,:));
                    I(Masque == 1) = NaN;
                end
                set(h2Image, 'CData', I);
                
                h1Title.String = setTitle1(kPing);
                h2Title.String = setTitle2(kAcrossDist);
                
                set(h4Swath,  'XData', Data.LongitudeTop(kPing,:),       'YData', Data.LatitudeTop(kPing,:));
                set(h4Across, 'XData', Data.LongitudeTop(:,kAcrossDist), 'YData', Data.LatitudeTop(:,kAcrossDist));
                
                setInfo(kPing, kAcrossDist, kDepthCurrent);
            end

            
            function windowButtonMotion(varargin)
                [flag, C] = isOverAxis(h1);
                if flag
                    AcrossMouse = C(1);
                    DepthMouse  = C(2);
                    updataImage1(AcrossMouse, DepthMouse);
                    return
                end
                
                [flag, C] = isOverAxis(h2);
                if flag
                    PingMouse  = C(1);
                    DepthMouse = C(2);
                    updataImage2(PingMouse, DepthMouse);
                    return
                end
                
                [flag, C] = isOverAxis(h3);
                if flag
                    PingMouse   = C(1);
                    AcrossMouse = C(2);
                    updataImage3(PingMouse, AcrossMouse);
                    return
                end
                
                [flag, C] = isOverAxis(h4);
                if flag
                    LongitudeMouse = C(1);
                    LatitudeMouse  = C(2);
                    
                    Dist = ((Data.LongitudeTop - LongitudeMouse) .^ 2 + (Data.LatitudeTop - LatitudeMouse) .^ 2);
                    [~, ind] = min(Dist(:));
                    [kPing, kAcrossDist] = ind2sub(size(Dist), ind);
                    
                    if (kPing < 1) || (kPing > nbPings) || (kAcrossDist < 1) || (kAcrossDist > nbAcrossDist)
                        return
                    end
                    
                    updataImage4(LongitudeMouse, LatitudeMouse, pings(kPing), Data.AcrossDistance(kAcrossDist));
                    return
                end
                
                % On revient au point courant
                
                DepthMouse  = Z(kDepthCurrent);
                AcrossMouse = AcrossDistance(kAcrossCurrent);
                PingMouse   = kPingCurrent;
                LongitudeMouse = Data.LongitudeTop(kPingCurrent, kAcrossCurrent);
                LatitudeMouse  = Data.LatitudeTop( kPingCurrent, kAcrossCurrent);
                updataImage1(AcrossMouse, DepthMouse);
                updataImage2(PingMouse,   DepthMouse);
                updataImage3(PingMouse,   AcrossMouse);
                updataImage4(LongitudeMouse, LatitudeMouse, PingMouse, AcrossMouse)
                
                setInfo(kPingCurrent, kAcrossCurrent, kDepthCurrent);
            end

            
            function setInfo(kPing, kAcrossDist, kDepth)
                LonMouse    = Data.LongitudeTop(kPing,kAcrossDist);
                LatMouse    = Data.LatitudeTop( kPing,kAcrossDist);
                valPixel    = Reflectivity(kDepth,kAcrossDist,kPing);
                
                AcrossMouse = AcrossDistance(kAcrossDist);
                DepthMouse  = Z(kDepth);

                strInfo = sprintf('Val=%5.1f | Ping=%05d | Across=%8.2f | Z=%8.2f | Lat=%f | Lon=%f | Lat=%s | Lon=%s | Time=%s', ...
                    valPixel, kPing, AcrossMouse, DepthMouse, ...
                    LatMouse, LonMouse, lat2str(LatMouse), lon2str(LonMouse), ...
                    char(Data.Datetime(kPing)));
                
                hInfo.String = strInfo;
            end
            
            
            function windowButtonDown(varargin)
                
                %% Test si on a cliqu� sur un des 3 axes
                
                [flag, C] = isOverAxis(h1);
                if flag
                    AcrossMouse = C(1);
                    DepthMouse  = C(2);
                    kDepthCurrent  = find((Z                   >= DepthMouse) , 1, 'first');
                    kAcrossCurrent = find((Data.AcrossDistance >= AcrossMouse), 1, 'first');
                    if isempty(kAcrossCurrent)
                        return
                    end
                    updataImage1(AcrossMouse, DepthMouse);
                    printPointPosition;
                    printGlobePointage;
                    return
                end
                
                [flag, C] = isOverAxis(h2);
                if flag
                    PingMouse  = C(1);
                    DepthMouse = C(2);
                    kDepthCurrent = find((Z     >= DepthMouse) , 1, 'first');
                    kPingCurrent  = find((pings >= PingMouse),   1, 'first');
                    updataImage2(PingMouse, DepthMouse);
                    printPointPosition;
                    printGlobePointage;
                    return
                end
                
                [flag, C] = isOverAxis(h3);
                if flag
                    PingMouse   = C(1);
                    AcrossMouse = C(2);
                    kAcrossCurrent = find((Data.AcrossDistance >= AcrossMouse), 1, 'first');
                    if isempty(kAcrossCurrent)
                        return
                    end
                    kPingCurrent = find((pings >= PingMouse), 1, 'first');
                    updataImage3(PingMouse, AcrossMouse);
                    printPointPosition;
                    printGlobePointage;
                    return
                end
                
                [flag, C] = isOverAxis(h4);
                if flag
                    LongitudeMouse = C(1);
                    LatitudeMouse  = C(2);
                    
                    Dist = ((Data.LongitudeTop - LongitudeMouse) .^ 2 + (Data.LatitudeTop - LatitudeMouse) .^ 2);
                    [~, ind] = min(Dist(:));
                    [kPing, kAcrossDist] = ind2sub(size(Dist), ind);
                    
                    if (kPing < 1) || (kPing > nbPings) || (kAcrossDist < 1) || (kAcrossDist > nbAcrossDist)
                        return
                    end
                    kAcrossCurrent = kAcrossDist;
                    kPingCurrent   = kPing;
                    updataImage4(LongitudeMouse, LatitudeMouse, pings(kPing), Data.AcrossDistance(kAcrossDist));
                    printPointPosition;
                    printGlobePointage;
                    return
                end
                
                %% On revient au point courant
                
                DepthMouse  = Z(kDepthCurrent);
                AcrossMouse = AcrossDistance(kAcrossCurrent);
                PingMouse   = kPingCurrent;
                updataImage1(AcrossMouse, DepthMouse);
                updataImage2(PingMouse,   DepthMouse);
                updataImage3(PingMouse,   AcrossMouse);
            end
            
            
            function printPointPosition
                SelectionType = get(Fig, 'SelectionType');
                if strcmp(SelectionType, 'normal')
                    pathname = fileparts(nomFicXml);
                    pointFilename = fullfile(pathname, 'SSc-WCD-3DSampleBeam-Pointing.csv');
                    if exist(pointFilename, 'file')
                        fid = fopen(pointFilename, 'a+t');
                    else
                        strInfo = sprintf('Val (dB) ; Ping ; Across (m) ; Z (m) ; Depth (m); Lat (deg) ; Lon (deg) ; Lat2 ; Lon2 ; Ping time ; File ; User pointing time');
                        strInfo = strrep(strInfo, ';', csvSep);
                        fid = fopen(pointFilename, 'w+');
                        if fid ~= -1
                            fprintf(fid, '%str\n', strInfo);
                        end
                    end
                    DepthMouse  = Z(kDepthCurrent);
                    AcrossMouse = AcrossDistance(kAcrossCurrent);
                    LonMouse    = Data.LongitudeTop(kPingCurrent,kAcrossCurrent);
                    LatMouse    = Data.LatitudeTop( kPingCurrent,kAcrossCurrent);
                    valPixel    = Reflectivity(kDepthCurrent,kAcrossCurrent,kPingCurrent);
                    DepthReel   = DepthMouse;
                    fprintf('Pointed : Val (dB) %5.1f - Ping  %05d - Across (m) %8.2f - Z (m) %8.2f - Depth (m) %8.2f - Lat (deg) %s - Lon (deg) %s - %s - %s - Ping time %s - File %s - User pointing time %s\n', ...
                        valPixel, kPingCurrent, AcrossMouse, DepthMouse, DepthReel, ...
                        num2strPrecis(LatMouse), num2strPrecis(LonMouse), lat2str(LatMouse), lon2str(LonMouse), ...
                        char(Data.Datetime(kPingCurrent)), Data.ExtractedFrom, char(datetime));
                    
                    strInfo = sprintf('%5.1f ; %05d ; %8.2f ; %8.2f ; %8.2f ; %s ; %s ; %s ; %s ; %s ; %s ; %s', ...
                        valPixel, kPingCurrent, AcrossMouse, DepthMouse, DepthReel, ...
                        num2strPrecis(LatMouse), num2strPrecis(LonMouse), lat2str(LatMouse), lon2str(LonMouse), ...
                        char(Data.Datetime(kPingCurrent)), Data.ExtractedFrom, char(datetime));
                    strInfo = strrep(strInfo, ';', csvSep);
                    if fid ~= -1
                        fprintf(fid, '%s\n', strInfo);
                    end
                    my_fclose(fid);
                end
            end
            
            
            function printGlobePointage
                SelectionType = get(Fig, 'SelectionType');
                
                if strcmp(SelectionType, 'extend') || strcmp(SelectionType, 'alt') %|| strcmp(SelectionType, 'normal')
                    pathname = fileparts(nomFicXml);
                    pointFilename = fullfile(pathname, 'SScPointingForGlobe.csv');
                    if exist(pointFilename, 'file')
                        ID  = getNbLinesAsciiFile(pointFilename);
                        fid = fopen(pointFilename, 'a+t');
                    else
                        strInfo = sprintf('"ID";"LAYER";"PING";"LATITUDE_DEG";"LONGITUDE_DEG";"LATITUDE_DMD";"LONGITUDE_DMD";"HEIGHT_ABOVE_SEA_SURFACE";"HEIGHT_ABOVE_SEA_FLOOR";"SEA_FLOOR_ELEVATION";"SEA_FLOOR_LAYER";"DATE";"TIME";"MARKER_COLOR";"MARKER_SIZE";"MARKER_SHAPE";"GROUP";"CLASS";"COMMENT"');
                        fid = fopen(pointFilename, 'w+t');
                        if fid == -1
                            messageErreurFichier(pointFilename, 'WriteFailure');
                        else
                            fprintf(fid, '%s\n', strInfo);
                        end
                        ID = 1;
                    end
                    DepthMouse  = Z(kDepthCurrent);
                    AcrossMouse = AcrossDistance(kAcrossCurrent);
                    LonMouse    = Data.LongitudeTop(kPingCurrent,kAcrossCurrent);
                    LatMouse    = Data.LatitudeTop( kPingCurrent,kAcrossCurrent);
                    valPixel    = Reflectivity(kDepthCurrent,kAcrossCurrent,kPingCurrent);
                    fprintf('Pointed : Val (dB) %5.1f - Ping  %05d - Across (m) %8.2f - Z (m) %8.2f - Lat (deg) %s - Lon (deg) %s - %s - %s - Ping time %s - File %s - User pointing time %s\n', ...
                        valPixel, kPingCurrent, AcrossMouse, DepthMouse, ...
                        num2strPrecis(LatMouse), num2strPrecis(LonMouse), lat2str(LatMouse), lon2str(LonMouse), ...
                        char(Data.Datetime(kPingCurrent)), Data.ExtractedFrom, char(datetime));
                    
                    [~, LAYER] = fileparts(Data.ExtractedFrom);
                    LAYER = strrep(LAYER, 'Raw-', '');
                    TPing = Data.Datetime(kPingCurrent);
                    TPing.Format = 'yyyy-MM-dd  hh:mm:ss.SSS';
                    strPing = char(TPing);
                    strDate = strPing(1:10);
                    strHour = strPing(13:end);
                    strLat  = lat2str(LatMouse);
                    strLon  = lon2str(LonMouse);
                    strLat = strrep(strLat, '.', ',');
                    strLon = strrep(strLon, '.', ',');
                    strLat = strrep(strLat, 'S ', '-');
                    strLon = strrep(strLon, 'S ', '-');
                    strLat = strrep(strLat, 'W ', '-');
                    strLon = strrep(strLon, 'W ', '-');
                    strLat = strrep(strLat, 'N ', '');
                    strLon = strrep(strLon, 'N ', '');
                    strLat = strrep(strLat, 'E ', '');
                    strLon = strrep(strLon, 'E ', '');
                    strLat = strrep(strLat, ' ', '  ');
                    strLon = strrep(strLon, ' ', '  ');
                    
                    if isfield(Data, 'Mask')
                        vm = Data.Mask(:,kAcrossCurrent,kPingCurrent);
                        subVal = find(vm == 0);
                        if isempty(subVal)
                            my_fclose(fid);
                            return
                        end
                        kDepthLast = subVal(1);
                    else
                        try
                            vm = Reflectivity(:,kAcrossCurrent,kPingCurrent);
                            subVal = find(~isnan(vm), 1, 'last');
                            if isempty(subVal)
                                my_fclose(fid);
                                return
                            end
                            kDepthLast = subVal(1);
                        catch
                            kDepthLast = kDepthCurrent;
                        end
                    end
                    SEA_FLOOR_ELEVATION      = Z(kDepthLast);
                    HEIGHT_ABOVE_SEA_SURFACE = DepthMouse;
                    HEIGHT_ABOVE_SEA_FLOOR   = HEIGHT_ABOVE_SEA_SURFACE - SEA_FLOOR_ELEVATION;
                    
                    switch SelectionType
                        case 'extend'
                            MARKER_COLOR = '#ff8000ff';
                        case 'alt'
                            MARKER_COLOR = '#ff0080ff';
                        case 'normal'
                            MARKER_COLOR = '#ffff8000';
                        otherwise % Ne devrait pas arriver en principe
                            my_breakpoint
                            MARKER_COLOR = '#ff8000ff';
                    end
                                            
                    strInfo = '';
                    strInfo = [strInfo sprintf('"%03d"%c',  ID,                       csvSep)]; % ID
                    strInfo = [strInfo sprintf('"%s"%c',    LAYER,                    csvSep)]; % LAYER
                    strInfo = [strInfo sprintf('"%d"%c',    kPingCurrent,             csvSep)]; % PING
                    strInfo = [strInfo sprintf('"%s"%c',    num2strPrecis(LatMouse),  csvSep)]; % LATITUDE_DEG
                    strInfo = [strInfo sprintf('"%s"%c',    num2strPrecis(LonMouse),  csvSep)]; % LONGITUDE_DEG
                    strInfo = [strInfo sprintf('"%s"%c',    strLat,                   csvSep)]; % LATITUDE_DMD
                    strInfo = [strInfo sprintf('"%s"%c',    strLon,                   csvSep)]; % LONGITUDE_DMD
                    strInfo = [strInfo sprintf('"%0.2f"%c', HEIGHT_ABOVE_SEA_SURFACE, csvSep)]; % HEIGHT_ABOVE_SEA_SURFACE
                    strInfo = [strInfo sprintf('"%0.2f"%c', HEIGHT_ABOVE_SEA_FLOOR,   csvSep)]; % HEIGHT_ABOVE_SEA_FLOOR
                    strInfo = [strInfo sprintf('"%0.2f"%c', SEA_FLOOR_ELEVATION,      csvSep)]; % SEA_FLOOR_ELEVATION
                    strInfo = [strInfo sprintf('"%s"%c',    'DTM-filename',           csvSep)]; % SEA_FLOOR_LAYER
                    strInfo = [strInfo sprintf('"%s"%c',    strDate,                  csvSep)]; % DATE
                    strInfo = [strInfo sprintf('"%s"%c',    strHour,                  csvSep)]; % TIME
                    strInfo = [strInfo sprintf('"%s"%c',    MARKER_COLOR,             csvSep)]; % MARKER_COLOR
                    strInfo = [strInfo sprintf('"%d"%c',    20,                       csvSep)]; % MARKER_SIZE
                    strInfo = [strInfo sprintf('"%s"%c',    'Sphere',                 csvSep)]; % MARKER_SHAPE
                    strInfo = [strInfo sprintf('"%s"%c',    'Water column echo',      csvSep)]; % GROUP
                    strInfo = [strInfo sprintf('"%s"%c',    SelectionType,            csvSep)]; % CLASS
                    strInfo = [strInfo sprintf('"%s"',      'Comment')]; % COMMENT
                    if fid ~= -1
                        fprintf(fid, '%s\n', strInfo);
                    end
                    my_fclose(fid);
                end
            end
            
            
            function clbkColorbar(~, ~, hAxe, varargin)
                hImcontrast = my_imcontrast(hAxe); % TODO : le Adjust ne fonctionne pas
                waitfor(hImcontrast)
                CLim = get(hAxe, 'CLim'); % TODO : voir comment r�cup�rer l'axe de la colorbar
                set(h1, 'CLim', CLim);
                set(h2, 'CLim', CLim);
                set(h3, 'CLim', CLim);
            end
            
            
            function clbkColorbarH4(~, ~, varargin)
                hImcontrast = my_imcontrast(h4); % TODO : le Adjust ne fonctionne pas
                waitfor(hImcontrast)
                CLim = get(h4, 'CLim'); % TODO : voir comment r�cup�rer l'axe de la colorbar
                set(h4, 'CLim', CLim);
            end
            
            
            function clbkColorbarH4EI(~, ~, varargin)
                CLim = get(h4, 'CLim'); % TODO : voir comment r�cup�rer l'axe de la colorbar
                [flag, CLim] = saisie_CLim(CLim(1), CLim(2), 'dB');
                if ~flag
                    return
                end
                set(h4, 'CLim', CLim);
                switch LayerType
                    case 'Raw'
                        CLim4Raw = CLim;
                    case 'Comp'
                        CLim4Comp = CLim;
                end
            end
            
            
            function pressKey(~, kData, varargin)
                switch kData.Key
                    case 'tab' % Permutation Raw / Comp et inversement
                        if ~isempty(LayerType)
                            if isempty(ReflectivityComp)
                                return
                            end
                            switch LayerType
                                case 'Raw'
                                    CLimRaw = CLim;
                                    LayerType = 'Comp';
                                    CLim = CLimComp;
                                    Reflectivity = ReflectivityComp;
                                    
                                    if ~isempty(hi4)
                                       set(hi4, 'CData', EIComp);
                                    end
                                   set(h4,  'CLim', CLim4Comp)
                                case 'Comp'
                                    CLimComp = CLim;
                                    LayerType = 'Raw';
                                    CLim = CLimRaw;
                                    Reflectivity = ReflectivityRaw;
                                    
                                    if ~isempty(hi4)
                                        set(hi4, 'CData', EIRaw);
                                    end
                                    set(h4,  'CLim', CLim4Raw)
                            end
%                             msg = ['Reading the matrix "' LayerType '", please wait.'];
%                             hw = my_warndlg(msg, 0, 'displayItEvenIfSSc', 1);
%                             [flag, Data, Reflectivity, nomFic] = WCDUtils.SampleBeam.getMatrix(nomFicXml, 'LayerType', LayerType);
                            my_close(hw);
                            set(h1, 'CLim', CLim);
                            set(h2, 'CLim', CLim);
                            set(h3, 'CLim', CLim);
                            updateImages();
                        end
                        
                    case 'rightarrow'
                        kPingCurrent = kPingCurrent + 1;
                        updateImages();
                        
                    case 'leftarrow'
                        kPingCurrent = kPingCurrent - 1;
                        updateImages();
                        
                    case 'uparrow'
%                         kPingCurrent = kPingCurrent + 10;
%                         updateImages();
                        kDepthCurrent = kDepthCurrent + 1;
                        updateImages();
                        
                    case 'downarrow'
%                         kPingCurrent = kPingCurrent - 10;
%                         updateImages();
                        kDepthCurrent = kDepthCurrent - 1;
                        updateImages();
                        
                    case 'pagedown'
                        kPingCurrent = 1;
                        updateImages();
                        
                    case 'pageup'
                        kPingCurrent = Inf;
                        updateImages();
                        
                    case 'f'
                        [flag, flagPlotCross] = WCDUtils.InputParams.questionCrossOverImages();
                        if ~flag
                            return
                        end
                        createImage1([], kPingCurrent,   kDepthCurrent,  'flagPlotCross', flagPlotCross);
                        createImage2([], kAcrossCurrent, kDepthCurrent,  'flagPlotCross', flagPlotCross);
                        createImage3([], kPingCurrent,   kAcrossCurrent, 'flagPlotCross', flagPlotCross);
                        
                    case 'c'
                        hImageCourante = findobj(gca, 'Type', 'Image');
                        if isempty(hImageCourante)
                            return
                        end
                        iColDeb = find(hImageCourante.XData > hImageCourante.Parent.XLim(1), 1, 'first');
                        iColFin = find(hImageCourante.XData < hImageCourante.Parent.XLim(2), 1, 'last');
                        iLigDeb = find(hImageCourante.YData > hImageCourante.Parent.YLim(1), 1, 'first');
                        iLigFin = find(hImageCourante.YData < hImageCourante.Parent.YLim(2), 1, 'last');
                        CData = get(hImageCourante, 'CData');
                        CData = CData(iLigDeb:iLigFin, iColDeb:iColFin); % Extraction de la donn�e sur le zoom
                        ValStats = stats(CData);
                        CLim = ValStats.Quant_03_97;
                        if isequal(hImageCourante.Parent, h4) % Pas s�r que ce teste fonctionne !
                            set(h4, 'CLim', CLim);
                        else
                            set(h1, 'CLim', CLim);
                            set(h2, 'CLim', CLim);
                            set(h3, 'CLim', CLim);
                        end
                        
                    otherwise
%                         Key = kData.Key
                        
                        CurrentCharacter = get(Fig, 'CurrentCharacter');
                        switch CurrentCharacter
                            case '+'
                                XLim = h2.XLim;
                                dXlim = floor(XLim(2) - XLim(1)) / 4;
                                XLim = [XLim(1) + dXlim, XLim(1) + 3*dXlim];
                                h2.XLim = XLim;
                                h3.XLim = XLim;
                                
                            case '-'
                                XLim = h2.XLim;
                                dXlim = floor(XLim(2) - XLim(1)) / 4;
                                XLim = [XLim(1) - dXlim, XLim(2) + dXlim];
                                XLim(1) = max(1, XLim(1));
                                XLim(2) = min(nbPings, XLim(2));
                                h2.XLim = XLim;
                                h3.XLim = XLim;
                                
                            case '>' % Recherche du pixel le plus �lev� pour se positionner automatiquement dessus
                                hImageCourante = [];
                                identAxe = [];
                                
                                flag = isOverAxis(h1);
                                if flag
                                    hImageCourante = h1Image;
                                    identAxe = 1;
                                end
                                
                                flag = isOverAxis(h2);
                                if flag
                                    hImageCourante = h2Image;
                                    identAxe = 2;
                                end
                                
                                flag = isOverAxis(h3);
                                if flag
                                    hImageCourante = h3Image;
                                    identAxe = 3;
                                end

                                if isempty(hImageCourante)
                                    return
                                end
                                
                                CData = get(hImageCourante, 'CData');
                                [~, ind] = max(CData(:));
                                [ind1, ind2] = ind2sub(size(CData), ind);
                                
                                switch identAxe
                                    case 1
                                        kDepthCurrent  = ind1;
                                        kAcrossCurrent = ind2;
                                    case 2
                                        kDepthCurrent  = ind1;
                                        kPingCurrent   = ind2;
                                    case 3
                                        kAcrossCurrent = ind1;
                                        kPingCurrent   = ind2;
                                end
                                updateImages();
                        end
                end
                
                
                function updateImages()
                    kPingCurrent = max(1, min(kPingCurrent, nbPings));
                    
                    DepthMouse  = Z(kDepthCurrent);
                    AcrossMouse = AcrossDistance(kAcrossCurrent);
                    LongitudeMouse = Data.LongitudeTop(kPingCurrent, kAcrossCurrent);
                    LatitudeMouse  = Data.LatitudeTop( kPingCurrent, kAcrossCurrent);
                    
                    updataImage1(AcrossMouse, DepthMouse);
                    updataImage2(kPingCurrent, DepthMouse);
                    updataImage3(kPingCurrent, AcrossMouse);
                    updataImage4(LongitudeMouse, LatitudeMouse, kPingCurrent, AcrossMouse)
                end
            end
        end
        
        
        
        
        function [IRaw, CLim, nomFicImage] = readPingWCDXML(nomDr1, nomFic1, kPing)
            iDir = floor(kPing/100);
            nomDir2 = num2str(iDir, '%03d');
            nomFicImage = fullfile(nomDr1, nomDir2, sprintf('%05d.tif', kPing));
            flagImageTrouvee = 1;
            if ~exist(nomFicImage, 'file')
                nomFicImage = fullfile(nomDr1, nomFic1, nomDir2, sprintf('%05d.png', kPing));
                if ~exist(nomFicImage, 'file')
                    flagImageTrouvee = 0;
                end
            end
            Info = imfinfo(nomFicImage);
            if flagImageTrouvee
                IRaw = imread(nomFicImage);
                IRaw((IRaw < Info.SMinSampleValue) | (IRaw > Info.SMaxSampleValue)) = NaN;
            else
                IRaw = [];
            end
            
            mots = strsplit(nomFicImage, filesep);
            if  contains(mots{end}, '.tif')
                if contains(mots{end-2}, '_Raw_SampleBeam')
                    %  CLim = [-64 10];
                    CLim = [];
                elseif contains(mots{end-2}, '_Comp_SampleBeam')
                    CLim = [-5 30];
                else
                    CLim = [];
                end
            else
                CLim = [];
            end
        end    
    end
end