classdef (Abstract) AbstractOperator < handle & Cloneable
    % AbstractOperator
    %   The class which implement this Abstract class have to implement
    %       the protected method getOperationData(this),
    %       setOperationData(this) and clone(this)
    
    methods (Access = public)
        %% Arithmetic (Addition, subtraction, multiplication, division, power, rounding)
        
        % Addition
        function c = plus(a,b)
            [c, aValue, bValue] = initOperation(a,b);
            
            cValue = aValue + bValue;
            
            performOperation(c, cValue);
        end
        
        % Subtraction
        function c = minus(a,b)
            [c, aValue, bValue] = initOperation(a,b);
            
            cValue = aValue - bValue;
            
            performOperation(c, cValue);
        end
        
        % Element-wise multiplication
        function c = times(a,b)
            [c, aValue, bValue] = initOperation(a,b);
            
            cValue = aValue .* bValue;
            
            performOperation(c, cValue);
        end
        
        % Right array division
        function c = rdivide(a,b)
            [c, aValue, bValue] = initOperation(a,b);
            
            cValue = aValue ./ bValue;
            
            performOperation(c, cValue);  
        end
        
        % Left array division
        function c = ldivide(a,b)
            [c, aValue, bValue] = initOperation(a,b);
            
            cValue = aValue .\ bValue;
            
            performOperation(c, cValue)
            
        end
        
        % Element-wise power
        function c = power(a,b)
            [c, aValue, bValue] = initOperation(a,b);
            
            cValue = aValue .^ bValue;
            
            performOperation(c, cValue);
        end
        
        % Matrix Multiplication
        function c = mtimes(a,b)
            [c, aValue, bValue] = initOperation(a,b);
            
            cValue = aValue * bValue;
            
            performOperation(c, cValue);
        end
        
        % Solve systems of linear equations xA = B for x
        function c = mrdivide(b,a)
            [c, aValue, bValue] = initOperation(a,b);
            
            cValue = bValue / aValue;
            
            performOperation(c, cValue);
        end
        
        % Solve systems of linear equations Ax = B for x
        function c = mldivide(a,b)
            [c, aValue, bValue] = initOperation(a,b);
            
            cValue = aValue \ bValue;
            
            performOperation(c, cValue);
        end
        
        %% Relational (Value comparisons)
        % TODO MHO
        
        %% Logical (True or false (Boolean) conditions)
        % TODO MHO
    end
    
    
    methods (Access = protected)
        function [c, aValue, bValue] = initOperation(a,b)
            % Initialize values for comming operation. Return c, the new object
            % creating from operation, aValue and bValue, the values who
            % will be operated.
            if isobject(a) && isobject(b)
                c = a.clone();
                aValue = a.getOperationValue();
                bValue = b.getOperationValue();
            elseif isobject(a) && isnumeric(b)
                c = a.clone();
                aValue = a.getOperationValue();
                bValue = b;
            elseif isnumeric(a) && isobject(b)
                c = b.clone();
                aValue = a;
                bValue = b.getOperationValue();
            else
%                 str1 = 'Impossible de faire l''operation';
                str2 = 'Can''t done the operation';
                my_warndlg(str2, 1);
                return
            end
        end
        
        function performOperation(c, cValue)
            % Perform the operation. Set the computed value to the new
            % created object.
            c.setOperationValue(cValue);
        end
    end
    
    methods (Access = protected, Abstract)
        %% function to implement by child
        
        value = getOperationValue(this)
        
        setOperationValue(this, newValue)
    end
end
