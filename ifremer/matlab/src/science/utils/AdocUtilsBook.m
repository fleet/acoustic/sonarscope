classdef AdocUtilsBook
    % Collection of static functions to create .adoc files used for Survey Reports
    %
    % Authors  : JMA Authors
    % See also : checkSurveyReport
    %
    % Reference pages in Help browser
    %   <a href="matlab:doc AdocUtilsBook" >AdocUtilsBook</a>
    %  -------------------------------------------------------------------------------
    
    methods (Static)
        
        function flag = createSurveyReportFramework(adocFileName, varargin)
            % Create a Survey Report framework (Ex "MySurveyReport.adoc" file + "MySurveyReport" directory)
            %
            % Syntax
            %   AdocUtilsBook.createSurveyReportFramework(adocFileName, ...)
            %
            % Input Arguments
            %   adocFileName : Name of the .adoc file (Ascii Doctor)
            %
            % Name-Value Pair Arguments
            %   webSiteName : Name of a valid website describing the survey
            %   logFileName : Name of a log file of the survey
            %
            % Output Arguments
            %   flag : 0=KO, 1=OK
            %
            % Examples
            %   [flag, adocFileName, dataFileName] = AdocUtilsBook.getExampleDataSimonStevin;
            %   if flag
            %       flag = AdocUtilsBook.createSurveyReportFramework(adocFileName)
            %   end
            %
            % Authors  : JMA
            % See also : checkSurveyReport
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc AdocUtilsBook.createSurveyReportFramework" >AdocUtilsBook.createSurveyReportFramework</a>
            %   <a href="matlab:doc AdocUtilsBook"                             >AdocUtilsBook</a>
            %  -------------------------------------------------------------------------------
            
            global SonarScopeDocumentation %#ok<GVMIS> 
            
            [varargin, webSiteName] = getPropertyValue(varargin, 'WebSiteName', []);
            [varargin, logFileName] = getPropertyValue(varargin, 'LogFileName', []); %#ok<ASGLU>
            
            [nomDir, surveyName] = fileparts(adocFileName);
            summaryPathName = fullfile(nomDir, surveyName);
            
            %% Creation fichier .adoc
            
            fid = fopen(adocFileName, 'w+t');
            if fid == - 1
                messageErreurFichier(adocFileName, 'writeFailure')
                flag = 0;
                return
            end
            
            %% Cr�ation des r�pertoires
            
            flag = AdocUtilsBook.createDirectories(summaryPathName);
            if ~flag
                my_warndlg(msg, 1);
                return
            end
            
            %% Copie du bandeau Ifremer
            
            nomFicImageIn = fullfile(SonarScopeDocumentation, 'images', 'SScHelp-Images', 'Bandeau_entete.jpg');
            if exist(nomFicImageIn, 'file')
                copyfile(nomFicImageIn, fullfile(summaryPathName, 'PNG'));
            end
            
            %% Ecriture de l'ent�te du .adoc
            
            fprintf(fid, '= %s \n\n', surveyName);
            fprintf(fid, ':doctype: book\n');
            fprintf(fid, ':imagesdir: ./%s\n', surveyName);
            fprintf(fid, ':sectnums:\n');
            fprintf(fid, ':toc: macro\n');
            fprintf(fid, ':toc-title: Table of contents\n');
            fprintf(fid, ':author: SonarScope\n');
            fprintf(fid, ':email: sonarscope@ifremer.fr\n');
            fprintf(fid, ':revnumber: 1.0\n');
            fprintf(fid, ':revdate: Created %s, Updated %s\n', date, date);
            fprintf(fid, ':revremark: Not reviewed\n\n');
            fprintf(fid, 'image:PNG/Bandeau_entete.jpg[]\n\n');            
            %fprintf(fid, 'This survey report was created automatically by SonarScope\n\n');
            fprintf(fid, 'toc::[]\n\n');
            %fprintf(fid, '---\n\n');
            
            if ~isempty(webSiteName)
                fprintf(fid, 'Website of the survey : %s\n\n', webSiteName);
            end
            
            if ~isempty(logFileName) && exist(logFileName, 'file')
                [~, nomFic, ext] = fileparts(logFileName);
                nomFic = strrep(nomFic, ' ', '-');
                nomFic = [nomFic ext];
                nomFicDir = fullfile(summaryPathName, nomFic);
                [flag, message, messageId] = copyfile(logFileName, nomFicDir, 'f'); %#ok<ASGLU>
                if flag
                    fprintf(fid, 'Log file : link:./%s/%s[Open]\n\n', surveyName, nomFic);
                end
                
            end
            if ~isempty(webSiteName) || ~isempty(logFileName)
                fprintf(fid, '---\n\n');
            end
            
            fclose(fid);
            
            flag = 1;
            
            str = sprintf('"%s" was successfully created.', adocFileName);
            logFileId = getLogFileId;
            logFileId.info('AdocUtilsBook.createSurveyReportFramework', str);
        end
        
        
        function [figFileName, pngFileName] = exportFigAndPng(figID, summaryPathName, surveyName, tag, strTitle)
            % Export a Matlab figure into a .fig and a .png files
            %
            % Syntax
            %   [figFileName, pngFileName] = exportFigAndPng(figID, summaryPathName, surveyName, tag, strTitle)
            %
            % Input Arguments
            %   figID           : Matlab figure handle
            %   summaryPathName : Pathname of the Survey Report .adoc file
            %   surveyName      : SurveyName (name of the .adoc file, without path and extension)
            %   tag             : Any string that will avoid conflicts of file names in the SurveyReport subdirectories
            %   strTitle        : Name of the section in the Survey Report
            %
            % Output Arguments
            %   figFileName : Name of the saved .fig file
            %   pngFileName : Name of the saved .png file
            %
            % Examples
            %   [figFileName, pngFileName] = AdocUtilsBook.exportFigAndPng(figID, summaryPathName, surveyName, tag, strTitle)
            %
            % Authors  : JMA
            % See also : checkSurveyReport, ALL_SurveyReport_Summary
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc AdocUtilsBook.exportFigAndPng" >AdocUtilsBook.exportFigAndPng</a>
            %   <a href="matlab:doc AdocUtilsBook"                 >AdocUtilsBook</a>
            %  -------------------------------------------------------------------------------
            
            figFileName = fullfile(summaryPathName, surveyName, 'FIG', [surveyName '-' strTitle '-' tag '.fig']);
            % N = get(0, 'RecursionLimit');
            % set(0, 'RecursionLimit', 10000);
            
            oldWarn = warning('off', 'MATLAB:class:customSaveRecursionLimit'); % Exception rajout�e par JMA le 24/06/2019
            % pour trac�s de navigation lorsqu'il y a beaucoup de lignes (Ex : EM2040 QUOI)
            
            savefig(figID, figFileName)
            warning(oldWarn)
            
            % set(0,'RecursionLimit', N);
            
            pngFileName = fullfile(summaryPathName, surveyName, 'PNG', [surveyName '-' strTitle '-' tag '.png']);
            fig2fic(figID, pngFileName)
        end
        
        
        function addSummary(adocFileName, summaryFileName, figFileName, configurationFileName, shortSummary, fileNameFigHistoDatagrams, fileNamePngHistoDatagrams)
            % Add a summary of files into the Survey Report
            %
            % Syntax
            %   addSummary(adocFileName, summaryFileName, figFileName, configurationFileName, shortSummary, fileNameFigHistoDatagrams, fileNamePngHistoDatagrams)
            %
            % Input Arguments
            %   adocFileName              : Survey Report file name (.adoc)
            %   summaryFileName           : Summary file (.csv)
            %   figFileName               : Matlab saved figure of the summary uitable (.fig)
            %   configurationFileName     : Configuration files (list of data files that share th same settings) (.txt)
            %   shortSummary              : Short summary
            %   fileNameFigHistoDatagrams : Name of the saved .fig file of the summary plot (.fig)
            %   fileNamePngHistoDatagrams : Name of the saved .png file of the summary plot (.png)
            %
            % Examples
            %   AdocUtilsBook.addSummary(adocFileName, summaryFileName, figFileName, configurationFileName, shortSummary, fileNameFigHistoDatagrams, fileNamePngHistoDatagrams)
            %
            % Authors  : JMA
            % See also : checkSurveyReport, ALL_SurveyReport_Summary
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc AdocUtilsBook.addSummary" >AdocUtilsBook.addSummary</a>
            %   <a href="matlab:doc AdocUtilsBook"            >AdocUtilsBook</a>
            %  -------------------------------------------------------------------------------
            
%             [~, surveyName] = fileparts(adocFileName);
            
            %% Open file
            
            fid = fopen(adocFileName, 'a');
            fprintf(fid, '== Summary of lines ==\n\n');
            
            %% Summary of lines
            
            % [Heure, Minute, Seconde] = hourIfr2Hms(shortSummary.SurveyDuration);
            % strDuration = sprintf('%02d:%02d:%02d', Heure, Minute, round(Seconde));
            strDuration = AdocUtilsBook.hours2str(shortSummary.SurveyDuration/60);
            
            dt = shortSummary.SurveyTimeEnd - shortSummary.SurveyTimeBegin;
            strDurationSurvey = char(dt);
            % [Heure, Minute, Seconde] = hourIfr2Hms(dt*24*3600000);
            % strDurationSurvey = sprintf('%02d:%02d:%02d', Heure, Minute, round(Seconde));
            
            fprintf(fid, '\n==== Short summary ====\n\n');
            
            fprintf(fid, '* Sounder : *%s%d*\n', shortSummary.SurveySounderCie, shortSummary.SurveySounderName);
            fprintf(fid, '* Survey from *%s* to *%s*  - Total survey duration *%s*\n', ...
                char(shortSummary.SurveyTimeBegin), ...
                char(shortSummary.SurveyTimeEnd), ...
                strDurationSurvey);
            fprintf(fid, '* Total duration of stored data : *%s*\n', strDuration);
            fprintf(fid, '* Total number of pings : *%s*\n', num2strMoney(shortSummary.SurveyNbPings));
            
            fprintf(fid, '* Min depth : *%s m*\n', num2str(shortSummary.SurveyDepthMin, '%.2f'));
            fprintf(fid, '* Max depth : *%s m*\n', num2str(shortSummary.SurveyDepthMax, '%.2f'));
            
            fprintf(fid, '* Total navigation length : *%d m*\n' ,  round(shortSummary.SurveyLineLength));
            fprintf(fid, '* Total coverage of lines : *%s km2*\n', num2str(shortSummary.SurveyLineCoverage, '%.2f'));
            
%             fprintf(fid, '\n==== Access to the full summary ====\n\n');
%             
%             [~, nomFic, ext] = fileparts(summaryFileName);
%             fprintf(fid, 'link:./%s/%s%s[Excel file]\n\n', surveyName, nomFic, ext);
%             
%             AdocUtilsBook.addFigureMatlab(fid, figFileName, surveyName, 'table', 'FIG')
%             
%             nbConfigFiles = length(configurationFileName);
%             fprintf(fid, '=== List of files per configuration ===\n');
%             for k=1:nbConfigFiles
%                 [~, nomFic, ext] = fileparts(configurationFileName{k});
%                 fprintf(fid, '\n* %s%s link:./%s/%s%s[ Display]\n', nomFic, ext, surveyName, nomFic, ext);
%             end
            AdocUtilsBook.addTopOfThePage(fid);
            
%             %% Histogram of datagrams
%             
%             if ~isempty(fileNameFigHistoDatagrams) && ~isempty(fileNamePngHistoDatagrams)
%                 fprintf(fid, '=== Histogram of datagrams ===\n\n');
%                 [~, nomFic, ext] = fileparts(fileNamePngHistoDatagrams);
%                 fprintf(fid, 'image:PNG/%s%s[Matlab figure (.png)]\n\n', nomFic, ext);
%                 AdocUtilsBook.addFigureMatlab(fid, fileNameFigHistoDatagrams, surveyName, 'Histogram of datagrams', 'FIG')
%                 AdocUtilsBook.addTopOfThePage(fid);
%             end
            
            %% End of section
            
            fclose(fid);
        end
        
        
        function flag = openAdoc(adocFileName, varargin)
            % Open a Survey Report
            %   (open the .html file by default or the .adoc file if the .html was not found)
            %
            % Syntax
            %   flag = AdocUtilsBook.openAdoc(adocFileName, ...)
            %
            % Input Arguments
            %   adocFileName : Name of the .adoc file (Ascii Doctor)
            %
            % Name-Value Pair Arguments
            %   Mute : 0=Authorize messagess, 1=No messages(Default 0)
            %
            % Output Arguments
            %   flag : 0=KO, 1=OK
            %
            % Examples
            %   flag = AdocUtilsBook.getExampleDataSimonStevin;
            %   if flag
            %       flag = AdocUtilsBook.openAdoc(adocFileName)
            %   end
            %
            % Authors  : JMA
            % See also : checkSurveyReport
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc AdocUtilsBook.openAdoc" >AdocUtilsBook.openAdoc</a>
            %   <a href="matlab:doc AdocUtilsBook"          >AdocUtilsBook</a>
            %  -------------------------------------------------------------------------------
            
            global IfrTbxResources %#ok<GVMIS>
            
            [varargin, Mute] = getPropertyValue(varargin, 'Mute', 0); %#ok<ASGLU>
            
            nomDir = fileparts(IfrTbxResources);
            
            nomFicHtml = strrep(adocFileName, '.adoc', '.html');
            if isdeployed
                nomExe = fullfile(nomDir, 'extern', 'AsciiDoctor4Ssc', 'bin', 'asciidoctorj.exe');
            else
                nomExe = fullfile(nomDir, 'ifremer', 'extern', 'AsciiDoctor4Ssc', 'bin', 'asciidoctorj.exe');
            end
            cmd = sprintf('"%s" -b html "%s" -o "%s"', nomExe, adocFileName, nomFicHtml);
            [flag, msg] = dos(cmd);
            if flag == 0
                winopen(nomFicHtml)
            else % Erreur !!!
                try
                    my_warndlg(msg, 0);
                    if ~Mute
                        str = sprintf('SonarScope will open "%s" using AsciidocFX (normally installed with SetUpSonarScope_External_yyyymmdd9_Raaaab.exe).\n\nPlease click on button "HTML" then menu "Save". You can then open the .html file with your favorite internet browser.', ...
                            adocFileName);
                        my_warndlg(str, 0, 'displayItEvenIfSSc', 1, 'TimeDelay', 300);
                    end
                    winopen(adocFileName)
                catch
                    str = 'AsciidocFX does not seem to be installed.';
                    my_warndlg(str, 1);
                end
            end
        end
        
        
        function flag = addNavigation(adocFileName, figFileName, pngFileName, strTitle, varargin)
            % Open a Survey Report
            %
            % Syntax
            %   flag = addNavigation(adocFileName, figFileName, pngFileName, strTitle, ...)
            %
            % Input Arguments
            %   adocFileName : Name of the .adoc file (Ascii Doctor)
            %   figFileName  : Name of the outout .fig file
            %   pngFileName  : Name of the outout .png file
            %   strTitle     : TODO
            %
            % Name-Value Pair Arguments
            %   InfoSignal          : TODO (Default [])
            %   SectionName         : TODO (Default [])
            %   GoogleEarthFilename : TODO (Default [])
            %   Globe3DVFilename    : TODO (Default [])
            %   Comment             : TODO (Default [])
            %   listDataFiles       : TODO (Default [])
            %
            % Output Arguments
            %   flag : 0=KO, 1=OK
            %
            % Examples
            %   see ALL_SurveyReport_Navigation
            %
            % Authors  : JMA
            % See also : ALL_SurveyReport_Navigation
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc AdocUtilsBook.openAdoc" >AdocUtilsBook.openAdoc</a>
            %   <a href="matlab:doc AdocUtilsBook"          >AdocUtilsBook</a>
            %  -------------------------------------------------------------------------------
                        
            [varargin, InfoSignal]    = getPropertyValue(varargin, 'InfoSignal',          []);
            [varargin, SectionName]   = getPropertyValue(varargin, 'SectionName',         []);
            [varargin, kmzFileName]   = getPropertyValue(varargin, 'GoogleEarthFilename', []);
            [varargin, nomFicXml3DV]  = getPropertyValue(varargin, 'Globe3DVFilename',    []);
            [varargin, strComment]    = getPropertyValue(varargin, 'Comment',             []);
            [varargin, listDataFiles] = getPropertyValue(varargin, 'listDataFiles',       []); %#ok<ASGLU>
            
            %             flag = 0;
            
            [nomDir, surveyName] = fileparts(adocFileName);
            tag = num2str(randi(100000));
            
            fid = fopen(adocFileName, 'a');
            if ~isempty(SectionName)
                fprintf(fid, '\n== %s ==\n\n', SectionName);
            end
            
            if ~isempty(listDataFiles)
                nomFic = AdocUtilsBook.createListOfFiles(nomDir, surveyName, tag, listDataFiles);
                fprintf(fid, '* List of plotted files. link:./%s[ Display]\n', nomFic);
            end
            
            fprintf(fid, '\n==== %s ====\n\n', strTitle);
            [~, nomFic, ext] = fileparts(pngFileName);
            fprintf(fid, 'image::PNG/%s%s[Matlab figure (.fig)]\n\n', nomFic, ext);
            
            if ~isempty(InfoSignal)
                if isfield(InfoSignal, 'strIndData')
                    N = length(InfoSignal.strIndData);
                    fprintf(fid, '\n* Legend : ');
                    for k=1:N
                        fprintf(fid, '%d = %s', InfoSignal.strIndData(k), InfoSignal.strData{k});
                        if k < N
                            fprintf(fid, ', ');
                        end
                    end
                    fprintf(fid, '\n\n');
                elseif isfield(InfoSignal, 'DataUnique') && ~isempty(InfoSignal.DataUnique)
                    N = length(InfoSignal.DataUnique);
                    fprintf(fid, '\n* Only few values : [');
                    for k=1:N
                        fprintf(fid, '%f', InfoSignal.DataUnique(k));
                        if k < N
                            fprintf(fid, ', ');
                        end
                    end
                    if isempty(InfoSignal.Unit)
                        fprintf(fid, ']\n\n');
                    else
                        fprintf(fid, '] (%s)\n\n', InfoSignal.Unit);
                    end
                end
            end
            
            if ~isempty(strComment)
                fprintf(fid, '%s\n\n', strComment);
            end
            
            AdocUtilsBook.addFigureMatlab(fid, figFileName, surveyName, 'figure', 'FIG')
            
            if ~isempty(kmzFileName)
                [~, nomFic, ext] = fileparts(kmzFileName);
                fprintf(fid, 'link:./%s/KMZ/%s%s[GoogleEarth]\n', surveyName, nomFic, ext);
            end
            
            if ~isempty(nomFicXml3DV)
                % Pas d'ouverture automatique dans 3DV pour un fichier XML
                %     [~, nomFic, ext] = fileparts(nomFicXml3DV);
                %     fprintf(fid, '\nlink:./%s/3DV/%s%s[Globe]\n\n', surveyName, nomFic, ext);
                nomFic = AdocUtilsBook.createListOfFiles(nomDir, surveyName, tag, nomFicXml3DV, 'NomFicList', 'Globe3DVFilename');
                fprintf(fid, ' -  link:./%s[Globe]\n', nomFic);
            end
            
            AdocUtilsBook.addTopOfThePage(fid);
            fclose(fid);
            flag = 1;
        end
        
        
        function addMosaic(adocFileName, figFileName, pngFileName, kmzFileName, geotifFileName, typeGeotif, imageName, tag, varargin)
            % See also ALL.Process.SurveyReport_GlobalMosaic,
            % SurveyReport_AddLatLongBathymetry, ...
            
            [varargin, strTitle]              = getPropertyValue(varargin, 'Title',                 []);
            [varargin, AreaName]              = getPropertyValue(varargin, 'AreaName',              []);
            [varargin, strComment]            = getPropertyValue(varargin, 'Comment',               []);
            [varargin, listDataFiles]         = getPropertyValue(varargin, 'listDataFiles',         []);
            [varargin, InfoCompensationCurve] = getPropertyValue(varargin, 'InfoCompensationCurve', []);
            [varargin, nomFicPngInverseVideo] = getPropertyValue(varargin, 'nomFicPngInverseVideo', []);
            [varargin, nomFicKmzInverseVideo] = getPropertyValue(varargin, 'nomFicKmzInverseVideo', []); %#ok<ASGLU>
            
            [nomDir, surveyName] = fileparts(adocFileName);
            
            fid = fopen(adocFileName, 'a');
            if ~isempty(strTitle)
                if isempty(AreaName)
                    fprintf(fid, '\n== %s ==\n\n', strTitle);
                else
                    fprintf(fid, '\n== %s - %s ==\n\n', strTitle, AreaName);
                end
                
                if ~isempty(listDataFiles)
                    nomFic = AdocUtilsBook.createListOfFiles(nomDir, surveyName, tag, listDataFiles);
                    fprintf(fid, '* List of files used for these mosaics. link:./%s[ Display]\n', nomFic);
                end
                
                if ~isempty(strComment)
                    strComment = strComment';
                    fprintf(fid, ' %s\n', strComment(:)');
                end
            end
            fprintf(fid, '\n==== %s ====\n\n', imageName);
            
            if ~isempty(InfoCompensationCurve)
                fprintf(fid, 'Applied compensation files :\n\n');
                for k=1:length(InfoCompensationCurve.FileName)
                    [~, nomFic, ext] = fileparts(InfoCompensationCurve.pngFileName{k});
                    
                    fprintf(fid, '* %s\n', nomFic);
                    
                    fprintf(fid, '\nimage:PNG/%s%s[Curve (.png)]\n\n', nomFic, ext);
                    
                    %         [~, nomFic, ext] = fileparts(InfoCompensationCurve.figFileName{k});
                    %         fprintf(fid, 'link:./%s/FIG/%s%s[File]\n\n', surveyName, nomFic, ext);
                    AdocUtilsBook.addFigureMatlab(fid, InfoCompensationCurve.figFileName{k}, surveyName, 'figure', 'FIG')
                end
                fprintf(fid, '\n\nResult :\n\n');
            end
            
            if isempty(nomFicPngInverseVideo)
                [~, nomFic, ext] = fileparts(pngFileName);
                fprintf(fid, 'image:PNG/%s%s[Matlab figure (.png)]\n\n', nomFic, ext);
            else
                [~, nomFic1, ext1] = fileparts(pngFileName);
                [~, nomFic2, ext2] = fileparts(nomFicPngInverseVideo);
                
                fprintf(fid, '[width="100%%",options="header,footer"]\n');
                fprintf(fid, '|====================\n');
                fprintf(fid, '| Video normal | Video inverse\n');
                fprintf(fid, '| image:PNG/%s%s[Matlab figure (.png)] | image:PNG/%s%s[Matlab figure (.png)]\n', ...
                    nomFic1, ext1, nomFic2, ext2);
                fprintf(fid, '|====================\n\n');
            end
            
            AdocUtilsBook.addFigureMatlab(fid, figFileName, surveyName, 'figure', 'FIG')
            
            if isempty(nomFicKmzInverseVideo)
                [~, nomFic, ext] = fileparts(kmzFileName);
                fprintf(fid, 'link:./%s/KMZ/%s%s[GoogleEarth file]\n\n', surveyName, nomFic, ext);
            else
                [~, nomFic1, ext1] = fileparts(kmzFileName);
                [~, nomFic2, ext2] = fileparts(nomFicKmzInverseVideo);
                
                fprintf(fid, 'GoogleEarth files : link:./%s/KMZ/%s%s[Video normal] | link:./%s/KMZ/%s%s[Video inverse]\n\n', ...
                    surveyName, nomFic1, ext1, surveyName, nomFic2, ext2);
            end
            
            [~, nomFic, ext] = fileparts(geotifFileName);
            fprintf(fid, 'link:./%s/GEOTIF/%s%s[%s]\n', surveyName, nomFic, ext, typeGeotif);
            
            AdocUtilsBook.addTopOfThePage(fid);
            fclose(fid);
        end
        
        
        function addFigures(adocFileName, figFileName, pngFileName, kmzFileName, tag, varargin)
            % See also addSomeDisplayedFigures, S7K_SurveyReport_PlotSignalsVsTime
            
            [varargin, strTitle]      = getPropertyValue(varargin, 'Title',         []);
            [varargin, AreaName]      = getPropertyValue(varargin, 'AreaName',      []);
            [varargin, strComment]    = getPropertyValue(varargin, 'Comment',       []);
            [varargin, listDataFiles] = getPropertyValue(varargin, 'listDataFiles', []); %#ok<ASGLU>
            
            [nomDir, surveyName] = fileparts(adocFileName);
            
            if ischar(figFileName)
                figFileName = {figFileName};
            end
            
            if ischar(pngFileName)
                pngFileName = {pngFileName};
            end
            
            fid = fopen(adocFileName, 'a');
            if ~isempty(strTitle)
                if isempty(AreaName)
                    fprintf(fid, '\n== %s ==\n\n', strTitle);
                else
                    fprintf(fid, '\n== %s - %s ==\n\n', strTitle, AreaName);
                end
                
                if ~isempty(listDataFiles)
                    nomFic = AdocUtilsBook.createListOfFiles(nomDir, surveyName, tag, listDataFiles);
                    fprintf(fid, '* List of files used for these mosaics. link:./%s[ Display]\n', nomFic);
                end
                
                if ~isempty(strComment)
                    strComment = strComment';
                    fprintf(fid, ' %s\n', strComment(:)');
                end
            end
            
            for k=1:length(pngFileName)
                if isempty(pngFileName{k})
                    continue
                end
                [~, nomFic, ext] = fileparts(pngFileName{k});
                
                strTitle = nomFic;
                if ~isempty(tag)
                    strTitle = strrep(strTitle, ['-' tag], '');
                end
                
                fprintf(fid, '\n==== %s ====\n\n', strTitle);
                
                fprintf(fid, 'image:PNG/%s%s[Matlab figure (.png)]\n\n', nomFic, ext);
                
                AdocUtilsBook.addFigureMatlab(fid, figFileName{k}, surveyName, 'figure', 'FIG')
            end
            
            if ~isempty(kmzFileName)
                if ischar(kmzFileName)
                    kmzFileName = {kmzFileName}; % Au cas o� !
                end
                fprintf(fid, '\n==== Location of these images ====\n\nGoogleEarth : ');
                for k=1:length(kmzFileName)
                    if exist(kmzFileName{k}, 'file')
                        [~, nomFic, ext] = fileparts(kmzFileName{k});
%                         fprintf(fid, 'link:./%s/KMZ/%s%s[GoogleEarth %s file]\n\n', surveyName, nomFic, ext, nomFic);
                        if contains(nomFic, 'Raw')
                            code = 'Raw';
                        else
                            code = 'Comp';
                        end
                        fprintf(fid, '\tlink:./%s/KMZ/%s%s[%s]', surveyName, nomFic, ext, code);
                        if (length(kmzFileName) == 2) && (k == 1)
                            fprintf(fid, '\tand');
                        end
                    end
                end
                fprintf(fid, '\n');
            end
            
            AdocUtilsBook.addTopOfThePage(fid);
            fclose(fid);
        end
        
        
        function addImageAndSignals(adocFileName, figFileName, pngFileName, varargin)
            % See also ALL_SurveyReport_PlotImageSignals
            
            [varargin, Title1] = getPropertyValue(varargin, 'Title1', []);
            [varargin, Title2] = getPropertyValue(varargin, 'Title2', []);
            [varargin, Title3] = getPropertyValue(varargin, 'Title3', []); %#ok<ASGLU>
            % [varargin, Intro]  = getPropertyValue(varargin, 'Intro',  []); %#ok<ASGLU>
            
            [~, surveyName] = fileparts(adocFileName);
            
            fid = fopen(adocFileName, 'a');
            
            if ~isempty(Title1)
                fprintf(fid, '\n== %s ==\n\n', Title1);
            end
            
            if ~isempty(Title2)
                fprintf(fid, '\n==== %s ====\n\n', Title2);
            end
            
            if ~isempty(Title3)
                fprintf(fid, '\n==== %s ====\n\n', Title3);
            end
            
            % if ~isempty(Intro)
            %     fprintf(fid, '%s\n\n', Intro);
            % end
            
            [nomDir1, nomFic, ext] = fileparts(pngFileName);
            [~, nomDir2] = fileparts(nomDir1);
            nomFicPng1 = [nomFic ext];
            fprintf(fid, 'image:PlotImageSignals/%s/%s[]\n\n', nomDir2, nomFicPng1);
            
            AdocUtilsBook.addFigureMatlab(fid, figFileName, surveyName, 'figure', 'PlotImageSignals')
            
            AdocUtilsBook.addTopOfThePage(fid);
            fclose(fid);
        end
        
        
        function flag = cleanDirectories(adocFileName)
            % See also QUOI_CleanDirectories
            
            if ischar(adocFileName)
                adocFileName = {adocFileName};
            end
            
            flag = 0;
            for k=1:length(adocFileName)
                [surveyReportDirectory, surveyReportName] = fileparts(adocFileName{k});
                
                %% Lecture du fichier .adoc
                
                fileID = fopen(adocFileName{k});
                if fileID == -1
                    messageErreurFichier(adocFileName{k}, 'ReadFailure')
                    return
                end
                adocStrings = textscan(fileID, '%s', 'Delimiter', newline);
                fclose(fileID);
                adocStrings = adocStrings{1};
                
                %% Nettoyage des r�pertoires du Survey Report
                
                flag(1) = AdocUtilsBook.cleanDirectory(adocStrings, surveyReportDirectory, surveyReportName, 'FIG',    '.fig');
                flag(2) = AdocUtilsBook.cleanDirectory(adocStrings, surveyReportDirectory, surveyReportName, 'GEOTIF', '.tif');
                flag(3) = AdocUtilsBook.cleanDirectory(adocStrings, surveyReportDirectory, surveyReportName, 'KMZ',    '.kmz');
                flag(4) = AdocUtilsBook.cleanDirectory(adocStrings, surveyReportDirectory, surveyReportName, 'PNG',    '.png');
                % Warning : pas de nettoyage des r�pertoires ERS et SHP car pas de liens dans le .adoc
                
                %% Reg�n�re le .html pour assurer le coup
                
                if any(flag)
                    AdocUtilsBook.openAdoc(adocFileName{k});
                end
                
                %% The End
                
                fprintf('\nCleaning directories for "%s" is over\n\n', adocFileName{k});
            end
            flag = 1;
        end
        
        
        function flag = extractShortVersion(adocFileNameIn, surveyReportDirectoryOut, params, flagZip)
            params.DirNames{end+1} = 'PNG';
            params.DirFlags(end+1) = length(params.DirNames);
            
            [nomDirIn, nomDir2] = fileparts(adocFileNameIn);
            summaryFileNameIn  = fullfile(nomDirIn,  nomDir2);
            summaryFileNameOut = fullfile(surveyReportDirectoryOut, nomDir2);
            
            %% Copie du fichier .html
            
            adocFileNameIn = strrep(adocFileNameIn, '.adoc', '.html');
            [flag, msg] = copyfile(adocFileNameIn, surveyReportDirectoryOut, 'f');
            if ~flag
                AdocUtilsBook.messageCopyFile(adocFileNameIn, surveyReportDirectoryOut, msg);
                return
            end
            
            %% Suppression du r�pertoire summaryFileNameOut s'il existe d�j�
            
            if exist(summaryFileNameOut, 'dir')
                [flag, msg] = rmdir(summaryFileNameOut, 's');
                if ~flag
                    my_warndlg(msg, 1);
                    return
                end
            end
            
            %% Cr�ation du r�pertoire summaryFileNameOut
            
            flag = AdocUtilsBook.createOneDirectory(summaryFileNameOut);
            if ~flag
                return
            end
            
            %% Cr�ation des r�pertoires
            
            flag = AdocUtilsBook.createOneDirectory(fullfile(summaryFileNameOut, 'PNG'));
            if ~flag
                return
            end
            
            flag = AdocUtilsBook.createOneDirectory(fullfile(summaryFileNameOut, 'ERS'));
            if ~flag
                return
            end
            
            flag = AdocUtilsBook.createOneDirectory(fullfile(summaryFileNameOut, 'KMZ'));
            if ~flag
                return
            end
            
            flag = AdocUtilsBook.createOneDirectory(fullfile(summaryFileNameOut, 'FIG'));
            if ~flag
                return
            end
            
            flag = AdocUtilsBook.createOneDirectory(fullfile(summaryFileNameOut, 'GEOTIF'));
            if ~flag
                return
            end
            
            % R�pertoire SonarEquation
            
            nomDirSonarEq = fullfile(summaryFileNameIn, 'SonarEquation');
            if exist(nomDirSonarEq, 'dir')
                flagSonarEquation = 1;
                flag = AdocUtilsBook.createOneDirectory(fullfile(summaryFileNameOut, 'SonarEquation'));
                if ~flag
                    return
                end
                
                listeDirSonarEquation = listeDirOnDir(nomDirSonarEq);
                for k=1:length(listeDirSonarEquation)
                    flag = AdocUtilsBook.createOneDirectory(fullfile(summaryFileNameOut, 'SonarEquation', listeDirSonarEquation(k).name));
                    if ~flag
                        return
                    end
                end
            else
                flagSonarEquation = 0;
            end
            
            % R�pertoire PlotImageSignals
            
            nomDirPlotImageSignals = fullfile(summaryFileNameIn, 'PlotImageSignals');
            if exist(nomDirPlotImageSignals, 'dir')
                flagPlotImageSignals = 1;
                flag = AdocUtilsBook.createOneDirectory(fullfile(summaryFileNameOut, 'PlotImageSignals'));
                if ~flag
                    return
                end
                
                listeDirPlotImageSignals = listeDirOnDir(nomDirPlotImageSignals);
                for k=1:length(listeDirPlotImageSignals)
                    flag = AdocUtilsBook.createOneDirectory(fullfile(summaryFileNameOut, 'PlotImageSignals', listeDirPlotImageSignals(k).name));
                    if ~flag
                        return
                    end
                end
            else
                flagPlotImageSignals = 0;
            end
            
            %% Copie des fichiers summary , conf ...
            
            listeFiles = listeFicOnDir(summaryFileNameIn);
            for k=1:length(listeFiles)
                [flag, msg] = copyfile(listeFiles{k}, summaryFileNameOut, 'f');
                if ~flag
                    AdocUtilsBook.messageCopyFile(listeFiles{k}, summaryFileNameOut, msg);
                end
            end
            
            %% Copie du r�pertoire PNG
            
            nomDir4In  = fullfile(summaryFileNameIn,  'PNG');
            nomDir4Out = fullfile(summaryFileNameOut, 'PNG');
            if exist(nomDir4In, 'dir')
                [flag, msg] = copyfile(nomDir4In, nomDir4Out, 'f');
                if ~flag
                    AdocUtilsBook.messageCopyFile(nomDir4In, nomDir4Out, msg);
                end
            end
            
            %% Copie des contenus des sous-r�pertoires
            
            for k=1:length(params.DirFlags)
                nomDir3 = params.DirNames{params.DirFlags(k)};
                nomDir4In  = fullfile(summaryFileNameIn,  nomDir3);
                nomDir4Out = fullfile(summaryFileNameOut, nomDir3);
                if exist(nomDir4In, 'dir')
                    [flag, msg] = copyfile(nomDir4In, nomDir4Out, 'f');
                    if ~flag
                        AdocUtilsBook.messageCopyFile(nomDir4In, nomDir4Out, msg);
                        return
                    end
                end
            end
            
            %% Copie des contenus des sous-r�pertoires de SonarEquation
            
            if flagSonarEquation
                for k=1:length(listeDirSonarEquation)
                    flag = AdocUtilsBook.conditionalCopy(params, ...
                        fullfile(summaryFileNameIn,  'SonarEquation', listeDirSonarEquation(k).name), ...
                        fullfile(summaryFileNameOut, 'SonarEquation', listeDirSonarEquation(k).name));
                    if ~flag
                        return
                    end
                end
            end
            
            %% Copie des contenus des sous-r�pertoires de PlotImageSignals
            
            if flagPlotImageSignals
                for k=1:length(listeDirPlotImageSignals)
                    flag = AdocUtilsBook.conditionalCopy(params, ...
                        fullfile(summaryFileNameIn,  'PlotImageSignals', listeDirPlotImageSignals(k).name), ...
                        fullfile(summaryFileNameOut, 'PlotImageSignals', listeDirPlotImageSignals(k).name));
                    if ~flag
                        return
                    end
                end
            end
            
            %% Zip
            
            if flagZip
                zipfilename  = [summaryFileNameOut '.zip'];
                filenames{1} = [summaryFileNameOut '.html'];
                filenames{2} = summaryFileNameOut;
                zip(zipfilename, filenames);
                if exist(zipfilename, 'file')
                    delete(filenames{1});
                    [flag, msg] = rmdir(filenames{2}, 's');
                    if ~flag
                        my_warndlg(msg, 1);
                        return
                    end
                end
            end
        end
        
        
        function addPingBeamAndLatLongImages(adocFileName, figFileName, pngFileName, nomFicImageTif,...
                typeTifImagePingBeam, typeTifImageLatLong, pngHistoFileName, ...
                figFileNameLatLong, pngFileNameLatLong, kmzFileName, geotifFileName, varargin)
            % see also ALL_SurveyReport_SonarEquation
            
            [varargin, Title1] = getPropertyValue(varargin, 'Title1', []);
            [varargin, Title2] = getPropertyValue(varargin, 'Title2', []);
            [varargin, Title3] = getPropertyValue(varargin, 'Title3', []);
            [varargin, Intro]  = getPropertyValue(varargin, 'Intro',  []); %#ok<ASGLU>
            
            [~, surveyName] = fileparts(adocFileName);
            
            fid = fopen(adocFileName, 'a');
            
            if ~isempty(Title1)
                fprintf(fid, '\n== %s ==\n\n', Title1);
            end
            
            if ~isempty(Title2)
                fprintf(fid, '\n==== %s ====\n\n', Title2);
            end
            
            if ~isempty(Title3)
                fprintf(fid, '\n==== %s ====\n\n', Title3);
            end
            
            if ~isempty(Intro)
                fprintf(fid, '%s\n\n', Intro);
            end
            
            [nomDir1, nomFic, ext] = fileparts(pngFileName);
            [~, nomDir2] = fileparts(nomDir1);
            nomFicPng1 = [nomFic ext];
            % if isempty(pngHistoFileName)
            %     fprintf(fid, 'image:SonarEquation/%s/%s[]\n\n', nomDir2, nomFicPng1);
            % else
            [~, nomFic, ext] = fileparts(pngHistoFileName);
            nomFicPng2 = [nomFic ext];
            [~, nomFic, ext] = fileparts(pngFileNameLatLong);
            nomFicPng3 = [nomFic ext];
            fprintf(fid, '\n[width="100%%",options="header"]\n');
            fprintf(fid, '|====================\n');
            fprintf(fid, '| Image | Histogram | Mos\n');
            fprintf(fid, '| image:SonarEquation/%s/%s[] | image:SonarEquation/%s/%s[] | image:SonarEquation/%s/%s[]\n', ...
                nomDir2, nomFicPng1, nomDir2, nomFicPng2, nomDir2, nomFicPng3);
            fprintf(fid, '|====================\n\n');
            % end
            
            AdocUtilsBook.addFigureMatlab(fid, figFileName,        surveyName, 'PingBeam figure', 'SonarEquation/ReferenceDTM')
            AdocUtilsBook.addFigureMatlab(fid, figFileNameLatLong, surveyName, 'LatLong figure',  'SonarEquation/ReferenceDTM')
            
            [~, nomFic, ext] = fileparts(nomFicImageTif);
            fprintf(fid, 'PingBeam image : link:./%s/SonarEquation/%s/%s%s[%s]\n\n', surveyName, nomDir2, nomFic, ext, typeTifImagePingBeam);
            
            [~, nomFic, ext] = fileparts(geotifFileName);
            fprintf(fid, 'LatLong image : link:./%s/SonarEquation/%s/%s%s[%s]', surveyName, nomDir2, nomFic, ext, typeTifImageLatLong);
            
            [~, nomFic, ext] = fileparts(kmzFileName);
            fprintf(fid, '  and  link:./%s/SonarEquation/%s/%s%s[GoogleEarth file]\n', surveyName, nomDir2, nomFic, ext);
            
            AdocUtilsBook.addTopOfThePage(fid);
            fclose(fid);
        end
        
        
        function addReferenceDTM(adocFileName, figFileName, pngFileName, kmzFileName, geotifFileName, TypeData, typeGeotif)
            % see also ALL_SurveyReport_SonarEquation
                        
            [~, surveyName] = fileparts(adocFileName);
            
            fid = fopen(adocFileName, 'a');
            
            if TypeData == cl_image.indDataType('Bathymetry')
                fprintf(fid, '\n== Reference DTM ==\n\n');
                fprintf(fid, 'This reference DTM is used to compute these layers :\n\n');
                fprintf(fid, ' * SlopeAcross and SlopeAlong \n\n');
                fprintf(fid, ' * IncidenceAngles \n\n');
                fprintf(fid, ' * IA_SSc(Insonified Area) \n\n');
            end
            
            fprintf(fid, '\n==== %s ====\n\n', cl_image.strDataType{TypeData});
            
            [~, nomFic, ext] = fileparts(pngFileName);
            fprintf(fid, 'image:SonarEquation/ReferenceDTM/%s%s[Matlab figure (.fig)]\n\n', nomFic, ext);
            
            AdocUtilsBook.addFigureMatlab(fid, figFileName, surveyName, 'figure', 'SonarEquation/ReferenceDTM')
            
            % if ~isempty(kmzFileName)
            [~, nomFic, ext] = fileparts(kmzFileName);
            fprintf(fid, 'link:./%s/SonarEquation/ReferenceDTM/%s%s[GoogleEarth file]\n\n', surveyName, nomFic, ext);
            % end
            
            [~, nomFic, ext] = fileparts(geotifFileName);
            fprintf(fid, 'link:./%s/SonarEquation/ReferenceDTM/%s%s[%s]\n', surveyName, nomFic, ext, typeGeotif);
            
            AdocUtilsBook.addTopOfThePage(fid);
            fclose(fid);
        end
        
        
        function addAsciiFile(adocFileName, asciiFileName, surveyName)
            % see also ALL_SurveyReport_SonarEquation
            
            fid = fopen(adocFileName, 'a');
            [~, nomFic, ext] = fileparts(asciiFileName);
            fprintf(fid, '\n==== ASCII file ====\n\n');
            fprintf(fid, 'All the data illustrated in the previous section are exported in this  link:./%s/ASCII/%s%s[ASCII file (.csv)]\n', ...
                surveyName, nomFic, ext);
            AdocUtilsBook.addTopOfThePage(fid);
            fclose(fid);
        end
        
        
        function addNetcdfFile(adocFileName, filename, ncFileNamePingBeam, ncFileNameLatLong, surveyName)
            % see also ALL_SurveyReport_SonarEquation
            
            fid = fopen(adocFileName, 'a');
            [~, filenamePingBeam, ext] = fileparts(ncFileNamePingBeam);
            [~, filenameLatLong]       = fileparts(ncFileNameLatLong);
            fprintf(fid, '\n==== NetCDF files ====\n\n');
            fprintf(fid, '\nAll the data illustrated in the previous section are exported in these NetCDF files :\n\n');
            fprintf(fid, 'link:./%s/SonarEquation/%s/%s%s[%s.nc] and link:./%s/SonarEquation/%s/%s%s[%s.nc)]\n', ...
                surveyName, filename, filenamePingBeam, ext, filenamePingBeam, ...
                surveyName, filename, filenameLatLong, ext, filenameLatLong);
            fprintf(fid, '\nYou can use "Panoply" to inspect this file and visualize the data\n');
            AdocUtilsBook.addTopOfThePage(fid);
            fclose(fid);
        end
        
        
        function addBackscatterStatus(adocFileName, str, this) % TODO JMA: sortir this
            % see also ALL_SurveyReport_SonarEquation
            
            [~, strBackscatterStatus] = get_BS_Status(this);
            fid = fopen(adocFileName, 'a');
            fprintf(fid, '\n*%s*\n\n', strtrim(str));
            for k=1:size(strBackscatterStatus, 1)
                fprintf(fid, ' %s\n', deblank(strBackscatterStatus(k,:)));
            end
            fprintf(fid, '\n');
            fclose(fid);
        end
        
        
        function InfoCompensationCurve = exportCompensationCurve(InfoCompensationCurve, summaryPathName, surveyName, tag)
            % see also ALL.Process.SurveyReport_GlobalMosaic
            
            if ischar(InfoCompensationCurve.FileName)
                InfoCompensationCurve.FileName = {InfoCompensationCurve.FileName};
            end
            for k=1:length(InfoCompensationCurve.FileName)
                [~, FileName] = fileparts(InfoCompensationCurve.FileName{k});
                InfoCompensationCurve.figFileName{k} = fullfile(summaryPathName, surveyName, 'FIG', [FileName tag '.fig']);
                InfoCompensationCurve.pngFileName{k} = fullfile(summaryPathName, surveyName, 'PNG', [FileName tag '.png']);
                
                [CourbesStatistiques, flag] = load_courbesStats(InfoCompensationCurve.FileName{k});
                if ~flag
                    return
                end
                
                [flag, figID] = plot_CourbesStats(CourbesStatistiques, 'Stats', 0);
                if ~flag
                    return
                end
                legend off
                
                savefig(figID, InfoCompensationCurve.figFileName{k})
                
                fig2fic(figID, InfoCompensationCurve.pngFileName{k})
                close(figID);
            end
        end
        
        
        function flag = addSomeDisplayedFigures(adocFileName, strTitle, strComment, hFig, varargin)
            % see also ALL_SurveyReport_SonarEquation
            
            [varargin, flagQuestion] = getPropertyValue(varargin, 'Question', 1); %#ok<ASGLU>
            
            tag = num2str(randi(100000));
            
            %% Nom du fichier ADOC
            
            [summaryPathName, surveyName] = fileparts(adocFileName);
            
            %% Extraction �ventuelle et renommage de l'image
            
            [kmzFileName, figFileName, pngFileName] = AdocUtilsBook.exportFig2PngAndFigFiles(summaryPathName, surveyName, hFig, tag);
            
            %% Insertion des figures
            
            AdocUtilsBook.addFigures(adocFileName, figFileName, pngFileName, kmzFileName, tag, 'Title', strTitle, 'Comment', strComment);
            
            %% R�ouverture du fichier Adoc
            
            AdocUtilsBook.openAdoc(adocFileName);
            
            %% Suppression des fen�tres
            
            if flagQuestion
                str = 'Do you want to close the figures ?';
                [rep, flag] = my_questdlg(str);
                if ~flag
                    return
                end
                if rep == 1
                    for k=1:length(hFig)
                        my_close(hFig(k));
                    end
                end
            end
            
            flag = 1;
        end
        
        
        function str = importantMessage(str1, str2)
            % See also ALL_SurveyReport_SonarEquation
            str = sprintf('%s\n[IMPORTANT]\n====', strtrim(str1));
            for k=1:size(str2, 1)
                str = sprintf('%s\n%s\n', str, deblank(str2(k,:)));
            end
            str = sprintf('%s\n====\n', str);
        end
        
        
        function checkSurveyReport
            % AdocUtilsBook.checkSurveyReport
            
            I0 = cl_image_I0;
            
            %% Get example data (EM2040 files with WC from Simon Stevin)
            
            %             [flag, adocFileName, dataFileName, compFileName, gridSize] = AdocUtilsBook.getExampleDataSimonStevin;
            [flag, adocFileName, dataFileName, paramsCompReflec, gridSize] = AdocUtilsBook.getExampleDataMarmesonet;
            if ~flag
                return
            end
            
            %% Create a Survey Report framework
            
            flag = AdocUtilsBook.createSurveyReportFramework(adocFileName);
            if ~flag
                return
            end
            
            %% Preprocess the .all files
            
            [flag, All] = ALL.checkFiles(dataFileName);
            if ~flag
                return
            end
            
            %% Add the summary
            
            flag = ALL_SurveyReport_Summary(All, adocFileName);
            if ~flag
                return
            end
            
            %% Add Navigation, coverage and signals over navigation
            
            listSignals = {'VertDepth'; 'Heading'; 'InterPingsDistance'; 'SurfaceSoundSpeed'; 'BSN'};
            flag = ALL_SurveyReport_Navigation(All, adocFileName, listSignals);
            if ~flag
                return
            end
            
            %% Add Sound Speed Profiles
            
            flag = ALL_SurveyReport_SoundSpeedProfile(All, adocFileName);
            if ~flag
                return
            end
            
            %% Add MNT, Mosaic ans compensated Mosaic
            
            flag = ALL.Process.SurveyReport_GlobalMosaic(adocFileName, dataFileName, gridSize, ...
                'InfoCompensationCurveReflectivity', paramsCompReflec);
            if ~flag
                return
            end
            
            %% Add figures
            
            strTitle   = 'Add displayed figures';
            strComment = 'All the figures that still exist are catched here for the demonstration.';
            hFig = findobj(0, 'Type', 'Figure');
            flag = AdocUtilsBook.addSomeDisplayedFigures(adocFileName, strTitle, strComment, hFig, 'Question', 0);
            if ~flag
                return
            end
            
            %% Add Images and signals
            
            flag = ALL_SurveyReport_PlotImageSignals(All(1), adocFileName);
            if ~flag
                return
            end
            
            %% Add a LatLong Bathymetry image
            
            [ersFileName, flag] = getNomFicDatabase('EM12D_PRISMED_125m_LatLong_Bathymetry.ers', 'NoMessage');
            if ~flag
                return
            end
            [flag, a] = import(I0, ersFileName);
            if ~flag
                return
            end
            
            strTitle   = 'Add LatLong images';
            strComment = 'It is possible to add Bathymetry image in geometry LatLong. SSc will display automaticaly the shaded image and the greatest slopes.';
            
            flag = SurveyReport_AddLatLongBathymetry(a, adocFileName, strTitle, strComment);
            if ~flag
                return
            end
            
            %% Add a LatLong Reflectivity image
            
            
            
            %% Add a LatLong Echo-integration image
            
            
            
            %% Clean directory
            
            flag = AdocUtilsBook.cleanDirectories(adocFileName);
            if ~flag
                return
            end
            
            %% Extract a shortened Survey Report
            
            params.DirNames = {'KMZ'  'ERS'  'FIG'  'GEOTIF'};
            params.DirFlags = 1;
            flag = AdocUtilsBook.extractShortVersion(adocFileName, my_tempdir, params, false);
            if ~flag
                return
            end
            
        end
    end
    
    
    methods (Static, Access = private)
        
        function flag = createDirectories(summaryDirName)
            
            flag = AdocUtilsBook.createOneDirectory(summaryDirName);
            if ~flag
                return
            end
            
            flag = AdocUtilsBook.createOneDirectory(fullfile(summaryDirName, 'PNG'));
            if ~flag
                return
            end
            
            flag = AdocUtilsBook.createOneDirectory(fullfile(summaryDirName, 'ERS'));
            if ~flag
                return
            end
            
            flag = AdocUtilsBook.createOneDirectory(fullfile(summaryDirName, 'KMZ'));
            if ~flag
                return
            end
            
            flag = AdocUtilsBook.createOneDirectory(fullfile(summaryDirName, '3DV'));
            if ~flag
                return
            end
            
            flag = AdocUtilsBook.createOneDirectory(fullfile(summaryDirName, 'SHP'));
            if ~flag
                return
            end
            
            flag = AdocUtilsBook.createOneDirectory(fullfile(summaryDirName, 'FIG'));
            if ~flag
                return
            end
            
            flag = AdocUtilsBook.createOneDirectory(fullfile(summaryDirName, 'GEOTIF'));
            if ~flag
                return
            end
        end
        
        
        function flag = createOneDirectory(grpDirName)
            if exist(grpDirName, 'dir')
                flag = 1;
            else
                [flag, msg] = mkdir(grpDirName);
                if ~flag
                    my_warndlg(msg, 1);
                    return
                end
            end
        end
        
        
        function strDurationSurvey = hours2str(nbHours)
            if nbHours > 24
                nbJours = floor(nbHours / 24);
                nbHours = nbHours - nbJours*24;
                strDurationSurvey = sprintf('%d days and %3.1f hours', nbJours, nbHours);
            elseif nbHours > 1
                strDurationSurvey = sprintf('%3.1f hours', nbHours);
            else
                nbMinutes = nbHours * 60;
                strDurationSurvey = sprintf('%3.1f minutes', nbMinutes);
            end
        end
        
        
        function addTopOfThePage(fid)
            fprintf(fid, '\n<<Top,Top of the page>>\n\n');
            fprintf(fid, '// ------------------------------------------\n\n');
        end
        
        
        function nomFicHelp = getHelpHowToOpenMatlabFigures
            global SonarScopeDocumentation %#ok<GVMIS> 
            nomFicHelp = fullfile('file:\\\\', SonarScopeDocumentation, 'ADOC-HTML', 'Tutorials-ADOC', 'SScDoc-Tutorial-HowToOpenMatlabFigures.html');
        end
        
        
        function str = getStringHelpHowToOpenMatlabFigures
            str = sprintf('_............. (this link:%s[tutorial] explains how to open Matlab figures)_', AdocUtilsBook.getHelpHowToOpenMatlabFigures);
        end
        
        
        function addFigureMatlab(fid, figFileName, surveyName, strTypeFigure, pathname)
            [~, nomFic, ext] = fileparts(figFileName);
            fprintf(fid, 'link:./%s/%s/%s%s[Matlab %s (.fig)] %s\n\n', ...
                surveyName, pathname, nomFic, ext, strTypeFigure, AdocUtilsBook.getStringHelpHowToOpenMatlabFigures);
        end
        
        
        function nomFic = createListOfFiles(pathname, surveyName, tag, listDataFiles, varargin)
            [varargin, NomFicList] = getPropertyValue(varargin, 'NomFicList', 'listAllFilesForMosaics'); %#ok<ASGLU>
            
            if ~iscell(listDataFiles)
                listDataFiles = {listDataFiles};
            end
            
            nomFic = fullfile(surveyName, [NomFicList '-' tag '.txt']);
            nomFicList = fullfile(pathname, nomFic);
            fidList = fopen(nomFicList, 'w+');
            for k=1:length(listDataFiles)
                fprintf(fidList, '* %s\n', listDataFiles{k});
            end
            fclose(fidList);
        end
        
        
        function [kmzFileName, figFileName, pngFileName] = exportFig2PngAndFigFiles(summaryPathName, surveyName, hFig, tag)
            kmzFileName = [];
            
            InfoImage = {};
            N = length(hFig);
            for k=1:N
                figID = hFig(k);
                FigNumber(k) = figID.Number; %#ok<AGROW>
            end
            [~, subOrdre] = sort(FigNumber);
            hFig = hFig(subOrdre);
            
            hw = create_waitbar('Catching figures', 'N', N);
            for k=1:N
                my_waitbar(k, N, hw)
                fprintf('Catching figure %d / %d\n', k, N);
                
                figID = hFig(k);
                hAxes = findobj(figID, 'Type', 'Axe');
                if isempty(hAxes)
                    continue
                end
                clear py
                for k2=1:length(hAxes)
                    p = get(hAxes(k2), 'Position');
                    py(k2) = p(2); %#ok<AGROW>
                end
                [~, ind] = sort(py); % on prendra le titre sur l'axe plac� le plus en haut de la figure
                FileName = hAxes(ind(end)).Title.String;
                if isempty(FileName)
                    FileName = figID.Name;
                end
                FileName = str2FileNameCompatible(FileName);
                
                %     figID.WindowState = 'maximized';
                %     figID.WindowState = 'normal';
                
                FigureNameForSurveyReport = getappdata(figID, 'FigureNameForSurveyReport');
                if isempty(FigureNameForSurveyReport)
                    figFileName{k} = fullfile(summaryPathName, surveyName, 'FIG', [FileName '-' tag '.fig']); %#ok<AGROW>
                    pngFileName{k} = fullfile(summaryPathName, surveyName, 'PNG', [FileName '-' tag '.png']); %#ok<AGROW>
                else
                    FigureNameForSurveyReport = strrep(FigureNameForSurveyReport, ' ', '');
                    figFileName{k} = fullfile(summaryPathName, surveyName, 'FIG', [FigureNameForSurveyReport '-' tag '.fig']); %#ok<AGROW>
                    pngFileName{k} = fullfile(summaryPathName, surveyName, 'PNG', [FigureNameForSurveyReport '-' tag '.png']); %#ok<AGROW>
                end
                try
                    savefig(figID, figFileName{k})
                    fig2fic(figID, pngFileName{k})
                catch
                    str = sprintf('"%s" could not be captured.', figFileName{k});
                    my_warndlg(str, 0);
                end
                
                Info = getappdata(figID, 'InfoImage');
                if ~isempty(Info)
                    if isfield(Info, 'tag') && ~isempty(Info.tag)
                        listeTag{k} = Info.tag; %#ok<AGROW>
                    elseif isfield(Info, 'Tag') && ~isempty(Info.Tag) % Rattrapage d'une maladresse tag or Tag ? TODO corriger � la source
                        listeTag{k} = Info.Tag; %#ok<AGROW>
                    else
                        listeTag{k} = 'NoTag'; %#ok<AGROW>
                    end
                    InfoImage{k} = Info; %#ok<AGROW>
                end
                % close(figID);
            end
            my_close(hw, 'MsgEnd')
            
            %%
            
            if ~isempty(InfoImage)
                tmp.NoTag = [];
                for k=1:length(InfoImage)
                    if isempty(InfoImage{k})
                        continue
                    end
                    if isempty(listeTag{k})
                        tmp.NoTag = [tmp.NoTag k];
                    else
                        if isfield(tmp, InfoImage{k}.Tag)
                            tmp.(InfoImage{k}.Tag) = [tmp.(InfoImage{k}.Tag) k];
                        else
                            tmp.(InfoImage{k}.Tag) = k;
                        end
                    end
                end
                fields = fieldnames(tmp);
                kmzFileName = {};
                for k=1:length(fields)
                    sub = tmp.(fields{k});
                    if isempty(sub)
                        continue
                    end
                    if strcmp(fields{k}, 'NoTag')
                        kmzFileName{end+1} = fullfile(summaryPathName, surveyName, 'KMZ', [surveyName '-' tag '.kmz']); %#ok<AGROW>
                    else
                        kmzFileName{end+1} = fullfile(summaryPathName, surveyName, 'KMZ', [surveyName '-' fields{k} '-' tag '.kmz']); %#ok<AGROW>
                    end
                    export_GoogleEarth_GeolocatedImage(kmzFileName{end}, pngFileName(sub), InfoImage(sub));
                    winopen(kmzFileName{end})
                end
            end
        end
        
        
        function flag = cleanDirectory(adocStrings, surveyReportDirectory, surveyReportName, subDirectoryName, Ext)
            flag = 0;
            
            nomDirFIG = fullfile(surveyReportDirectory, surveyReportName, subDirectoryName);
            listeFicFig = listeFicOnDir(nomDirFIG, Ext);
            
            for k=1:length(listeFicFig)
                [~, nomFic] = fileparts(listeFicFig{k});
                nomFic = [nomFic Ext]; %#ok<AGROW>
                tmp = strfind(adocStrings, nomFic);
                ind = [tmp{:}];
                if isempty(ind)
                    fprintf('Deleting : %s\n', nomFic);
                    try
                        delete(listeFicFig{k});
                        flag = 1;
                    catch ME
                        ME.getReport
                        my_warndlg(ME.message, 1);
                    end
                end
            end
        end
        
        
        function messageCopyFile(adocFileNameIn, surveyReportDirectoryOut, msg)
            str = sprintf('The copy of "%s" into "%s" failed :\n%s', adocFileNameIn, surveyReportDirectoryOut, msg);
            my_warndlg(str, 1);
        end
        
        
        function flag = conditionalCopy(params, summaryFileNameIn, summaryFileNameOut)
            flag = 1;
            for k=1:length(params.DirFlags)
                nomDir3 = params.DirNames{params.DirFlags(k)};
                listeFic = listeFicOnDir(summaryFileNameIn, ['.' lower(nomDir3)]);
                for k2=1:length(listeFic)
                    [flag, msg] = copyfile(listeFic{k2}, summaryFileNameOut, 'f');
                    if ~flag
                        AdocUtilsBook.messageCopyFile(listeFic{k}, summaryFileNameOut, msg);
                        return
                    end
                end
            end
        end
        
        
        function [flag, adocFileName, dataFileName, paramsCompReflec, gridSize] = getExampleDataMarmesonet
            adocFileName = [];
            dataFileName = [];
            gridSize     = 5;
            
            % R�cup�ration des .wcd
            flag = FtpUtils.getSScDemoFile('0306_20091113_160950_Lesuroit.wcd', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            flag = FtpUtils.getSScDemoFile('0307_20091113_163950_Lesuroit.wcd', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            
            % R�cup�ration des .all
            [flag, dataFileName{1}] = FtpUtils.getSScDemoFile('0306_20091113_160950_Lesuroit.all', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            [flag, dataFileName{2}] = FtpUtils.getSScDemoFile('0307_20091113_163950_Lesuroit.all', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            
            % R�cup�ration du fichier de compensation de la r�flectivit�
            [flag, compFileName] = FtpUtils.getSScDemoFile('Mode_3_Swath_Single_Signal_CW_Compens_Reflectivity_BeamPointingAngle_TxBeamIndexSwath.mat', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            paramsCompReflec.FileName                 = compFileName;
            paramsCompReflec.ModeDependant            = 1;
            paramsCompReflec.UseModel                 = 0;
            paramsCompReflec.PreserveMeanValueIfModel = 0;
            
            % Nom du fichier .adoc
            pathname = fileparts(dataFileName{1});
            surveyReportPathname = fullfile(pathname, 'SurveyReportMarmesonetUnitest');
            %             if exist(surveyReportPathname, 'dir')
            %                 flag = rmdir(surveyReportPathname, 's');
            %                 if ~flag
            %                     return
            %                 end
            %             end
            if ~exist(surveyReportPathname, 'dir')
                flag = mkdir(surveyReportPathname);
                if ~flag
                    return
                end
            end
            adocFileName = fullfile(surveyReportPathname, 'TestSR.adoc');
        end
        
        
        function [flag, adocFileName, dataFileName, paramsCompReflec, gridSize] = getExampleDataSimonStevin
            adocFileName = [];
            dataFileName = [];
            gridSize     = 0.2;
            
            % R�cup�ration des .wcd
            flag = FtpUtils.getSScDemoFile('0029_20180306_213505_SimonStevin.wcd', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            flag = FtpUtils.getSScDemoFile('0030_20180306_213905_SimonStevin.wcd', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            flag = FtpUtils.getSScDemoFile('0031_20180306_214305_SimonStevin.wcd', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            
            % R�cup�ration des .all
            [flag, dataFileName{1}] = FtpUtils.getSScDemoFile('0029_20180306_213505_SimonStevin.all', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            [flag, dataFileName{2}] = FtpUtils.getSScDemoFile('0030_20180306_213905_SimonStevin.all', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            [flag, dataFileName{3}] = FtpUtils.getSScDemoFile('0031_20180306_214305_SimonStevin.all', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            
            % R�cup�ration du fichier de compensation de la r�flectivit�
            [flag, compFileName] = FtpUtils.getSScDemoFile('Mode_2_Swath_Single_Signal_CW_CompensSingle_Reflectivity_BeamPointingAngle_TxBeamIndexSwath.mat', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            paramsCompReflec.FileName                 = compFileName;
            paramsCompReflec.ModeDependant            = 1;
            paramsCompReflec.UseModel                 = 0;
            paramsCompReflec.PreserveMeanValueIfModel = 0;
            
            % Nom du fichier .adoc
            pathname = fileparts(dataFileName{1});
            surveyReportPathname = fullfile(pathname, 'SurveyReportUnitest');
            %             if exist(surveyReportPathname, 'dir')
            %                 flag = rmdir(surveyReportPathname, 's');
            %                 if ~flag
            %                     return
            %                 end
            %             end
            if ~exist(surveyReportPathname, 'dir')
                flag = mkdir(surveyReportPathname);
                if ~flag
                    return
                end
            end
            adocFileName = fullfile(surveyReportPathname, 'TestSR.adoc');
        end
    end
end
