classdef BucCtdScampiUtils
    % Collection of static functions reading SCAMPI, BUC and CTD data (needed during MAYOBS15 survey)
    %
    % Authors  : JMA Authors
    % See also : TODO
    %
    % Reference pages in Help browser
    %   <a href="matlab:doc XMLBinUtils" >XMLBinUtils</a>
    %  -------------------------------------------------------------------------------
    
    methods (Static)
        
        function [Datetime, Latitude, Longitude, Altitude, Heading, Immersion, Gite, assietteBucScampi, ...
                LongueurFilee, Effort, VitesseTreuil] = readAttitudeBucScampi(nomFicBucScampi)
            % Reads technical parameters of the SCAMPI (position, attitude,etc ...)
            %
            % Syntax
            %   [Datetime, Latitude, Longitude, Altitude, Heading, Immersion, Gite, assietteBucScampi, ...
            %   LongueurFilee, Effort, VitesseTreuil] = BucCtdScampiUtils.readAttitudeBucScampi(nomFicBucScampi)
            %
            % Input Arguments
            %   nomFicBucScampi : Name of the SCAMPI file that contains the technical parameters
            %
            % Output Arguments
            %   Datetime          : 
            %   Latitude          : 
            %   Longitude         : 
            %   Altitude          : 
            %   Heading           : 
            %   Immersion         : 
            %   Gite              : 
            %   assietteBucScampi : 
            %   LongueurFilee     : 
            %   Effort            : 
            %   VitesseTreuil     : 
            %
            % Examples
            %   nomFicBucScampi = 'U:\MAYOBS15\Utilisateurs\Carla\pour_JMA\TEST_routine_buc\NAV_MD228_MAYOBS15-135-01.csv';
            %   [Datetime, Latitude, Longitude, Altitude, Heading, Immersion, Gite, assietteBucScampi, ...
            %      LongueurFilee, Effort, VitesseTreuil] = BucCtdScampiUtils.readAttitudeBucScampi(nomFicBucScampi)
            %
            % Authors  : JMA
            % See also : plot_BUC_SCAMPI_CTD
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc BucCtdScampiUtils.readNavigationBucNetcdf" >BucCtdScampiUtils.readNavigationBucNetcdf</a>
            %   <a href="matlab:doc BucCtdScampiUtils.readCTD"                 >BucCtdScampiUtils.readCTD</a>
            %   <a href="matlab:doc BucCtdScampiUtils.editSignals"             >BucCtdScampiUtils.editSignals</a>
            %   <a href="matlab:doc BucCtdScampiUtils"                         >BucCtdScampiUtils</a>
            %  -------------------------------------------------------------------------------
            
            %{
            DATE        HEURE        LATITUDE        LONGITUDE       CAP     GITE    ASSIETTE    IMMERSION    ALTITUDE    L_FILEE    EFFORT    VITESSE_TREUIL
            __________    ________    _____________    ____________    _____    ____    ________    _________    ________    _______    ______    ______________
            19/10/2020    23:09:44    -12.845238667    45.681603333    322.7    -4.2      2.2         50.9         2.8        50.2       640            0
            19/10/2020    23:09:45    -12.845238733    45.681603222    324.1    -4.2      2.4         50.9         2.8        50.2       570            0
            19/10/2020    23:09:46      -12.8452388    45.681603111    324.8    -4.3      2.5         50.9         2.8        50.2       560            0
            %}
                
                TableBucScampi = readtable(nomFicBucScampi);
                head(TableBucScampi)
                
                Date  = TableBucScampi.(1);
                Heure = TableBucScampi.(2);
                
                Datetime = Date + Heure;
                Datetime.Format = 'yyyy/MM::dd  hh:mm:ss.SSS';
                
                Latitude          =  TableBucScampi.(3);
                Longitude         =  TableBucScampi.(4);
                Heading           =  TableBucScampi.(5);
                Gite              =  TableBucScampi.(6);
                assietteBucScampi =  TableBucScampi.(7);
                Immersion         = -TableBucScampi.(8);
                Altitude          =  TableBucScampi.(9);
                LongueurFilee     =  TableBucScampi.(10);
                Effort            =  TableBucScampi.(11);
                VitesseTreuil     =  TableBucScampi.(12);
        end
        
        
        function [Datetime, Immersion, Temp, Neph, ORP] = readCTD(nomFicCTD)
            % Reads data from the CTD
            %
            % Syntax
            %   [Datetime, Immersion, Temp, Neph, ORP] = readCTD(nomFicCTD)
            %
            % Input Arguments
            %   nomFicCTD : Name of the CTD data file
            %
            % Output Arguments
            %   Datetime  : 
            %   Latitude  : 
            %   Immersion : 
            %   Temp      : 
            %   Neph      : 
            %   ORP       : 
            %
            % Examples
            %   nomFicCTD = 'U:\MAYOBS15\Utilisateurs\Carla\pour_JMA\TEST_routine_buc\Cecile_pour_Carla\MAY15-AUV02-JMA.xlsx';
            %   [Datetime, Immersion, Temp, Neph, ORP] = BucCtdScampiUtils.readCTD(nomFicCTD)
            %
            % Authors  : JMA
            % See also : plot_BUC_SCAMPI_CTD
            %
            % Reference pages in Help browser
            % Reference pages in Help browser
            %   <a href="matlab:doc BucCtdScampiUtils.readNavigationBucNetcdf" >BucCtdScampiUtils.readNavigationBucNetcdf</a>
            %   <a href="matlab:doc BucCtdScampiUtils.readAttitudeBucScampi"   >BucCtdScampiUtils.readAttitudeBucScampi</a>
            %   <a href="matlab:doc BucCtdScampiUtils.editSignals"             >BucCtdScampiUtils.editSignals</a>
            %   <a href="matlab:doc BucCtdScampiUtils"                         >BucCtdScampiUtils</a>
            %  -------------------------------------------------------------------------------
            
            %{
    date_time_dd_mm_yyyyHh_mm_ss_    Press_dB_    Temp__C_    Depth_m_     Neph_volts_    Press_counts_    Temp_counts_    Temp_resistance_    ORP_counts_    Neph_counts_    Logic_volts_    Sensor_volts_    ORP_mv_    CorrectedDepth_m_    Var15    Var16    Var17    Var18    Var19    Var20    Var21    Var22    Var23    Var24    Var25    Var26    date_time_dd_mm_yyyyHh_mm_ss__1    Press_dB__1    Temp__C__1    Depth_m__1    Neph_volts__1    Press_counts__1    Temp_counts__1    Temp_resistance__1    Cond_counts_    Neph_counts__1    Logic_volts__1    Sensor_volts__1    Eh_volts_
    _____________________________    _________    ________    _________    ___________    _____________    ____________    ________________    ___________    ____________    ____________    _____________    _______    _________________    _____    _____    _____    _____    _____    _____    _____    _____    _____    _____    _____    _____    _______________________________    ___________    __________    __________    _____________    _______________    ______________    __________________    ____________    ______________    ______________    _______________    _________
         12/10/2020 02:30:01           -9.554     27.38408     -9.49974      0.08691          1093            21819            9386.406           42472           1528            8.56            8.64         142.173       -2.21655624        NaN      NaN      NaN      NaN      NaN      NaN      NaN      NaN      NaN      NaN      NaN      NaN                   NaN                      NaN           NaN           NaN             NaN               NaN               NaN                 NaN                NaN              NaN               NaN                NaN             NaN
         12/10/2020 02:30:06           -7.773     27.37976     -7.72842       0.0882          1103            21822            9387.954           42486           1543             NaN             NaN         142.389      -0.455412948        NaN      NaN      NaN      NaN      NaN      NaN      NaN      NaN      NaN      NaN      NaN      NaN                   NaN                      NaN           NaN           NaN             NaN               NaN               NaN                 NaN                NaN              NaN               NaN                NaN             NaN
         12/10/2020 02:30:11           -7.951     27.37401     -7.90555      0.08734          1102            21826            9390.019           42501           1533             NaN             NaN         142.621      -0.631525911        NaN      NaN      NaN      NaN      NaN      NaN      NaN      NaN      NaN      NaN      NaN      NaN                   NaN                      NaN           NaN           NaN             NaN               NaN               NaN                 NaN                NaN              NaN               NaN                NaN             NaN
         12/10/2020 02:30:16           -6.882     27.36969     -6.84277      0.08777          1108            21829            9391.567           42510           1538             NaN             NaN          142.76       0.425147312        NaN      NaN      NaN      NaN      NaN      NaN      NaN      NaN      NaN      NaN      NaN      NaN                   NaN                      NaN           NaN           NaN             NaN               NaN               NaN                 NaN                NaN              NaN               NaN                NaN             NaN
            %}
            
            TableCtd = readtable(nomFicCTD);
            head(TableCtd)
            
            Datetime  =  TableCtd.(1);
            Temp      =  TableCtd.(3);
            Immersion = -TableCtd.(14);
            Neph      =  TableCtd.Neph_counts_;
            ORP       =  TableCtd.ORP_counts_;
        end
         
        
        function [Datetime, Latitude, Longitude, Immersion, Signals, Units] = readNavigationBucNetcdf(ncFileName)
            % Reads data from a BUC
            %
            % Syntax
            %   [Datetime, Latitude, Longitude, Immersion, Signals, Units] = readNavigationBucNetcdf(ncFileName)
            %
            % Input Arguments
            %   ncFileName : Name of the BUC file
            %
            % Output Arguments
            %   Datetime  : 
            %   Latitude  : 
            %   Longitude : 
            %   Immersion : 
            %   Signals   : 
            %   Units     : 
            %
            % Examples
            %   ncFileName = 'U:\MAYOBS15\Utilisateurs\Carla\pour_JMA\TEST_routine_buc\POS\xxxxx';
            %   [Datetime, Latitude, Longitude, Immersion, Signals, Units] = readNavigationBucNetcdf(ncFileName)
            %
            % Authors  : JMA
            % See also : plot_BUC_SCAMPI_CTD
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc BucCtdScampiUtils.readAttitudeBucScampi"   >BucCtdScampiUtils.readAttitudeBucScampi</a>
            %   <a href="matlab:doc BucCtdScampiUtils.readCTD"                 >BucCtdScampiUtils.readCTD</a>
            %   <a href="matlab:doc BucCtdScampiUtils.editSignals"             >BucCtdScampiUtils.editSignals</a>
            %   <a href="matlab:doc BucCtdScampiUtils"                         >BucCtdScampiUtils</a>
            %  -------------------------------------------------------------------------------
             
            finfo = ncinfo(ncFileName);
            for k=1:length(finfo.Variables)
                varName{k} = finfo.Variables(k).Name; %#ok<AGROW>
            end
            
            indTime = strcmp(varName, 'time');
            varName(indTime) = [];
            N = length(varName);
            
            ncID = netcdf.open(ncFileName);
            
            varID = netcdf.inqVarID(ncID, 'time');
            time = netcdf.getVar(ncID, varID);
            Datetime = datetime(time, 'ConvertFrom', 'excel');
            
            for k=1:N
                varID      = netcdf.inqVarID(ncID, varName{k});
                Signals{k} = netcdf.getVar(ncID, varID); %#ok<AGROW>
                Units{k}   = netcdf.getAtt(ncID, varID, 'units'); %#ok<AGROW>
            end
            
            netcdf.close(ncID);
            
            %%
            
            indLat    = find(strcmp(varName, 'lat'));
            Latitude  = Signals{indLat} ;
            indLon    = find(strcmp(varName, 'long'));
            Longitude = Signals{indLon};
            indDepth  = find(strcmp(varName, 'depth_immersion'));
            Immersion = Signals{indDepth};
            
            Signals([indLat indLon indDepth]) = [];
            Units([indLat indLon indDepth])   = [];
            
            sub = (Latitude == 0) | (Longitude == 0);
            Datetime(sub)  = [];
            Latitude(sub)  = [];
            Longitude(sub) = [];
            Immersion(sub) = [];
            
            for k=1:length(Signals)
                Signals{k}(sub) = [];
            end
        end       
        
        
        function editSignals(filename, Datetime, Latitude, Longitude, varargin)
            % Edit some signals with navigation
            %
            % Syntax
            %   editSignals(filename, Datetime, Latitude, Longitude, ...)
            %
            % Input Arguments
            %   filename  : 
            %   Datetime  : 
            %   Latitude  : 
            %   Longitude : 
            %
            % Name-Value Pair Arguments
            %   Heading             : 
            %   Immersion           : 
            %   displayedSignalList : 
            %
            % Examples
            %   nomFicBucScampi = 'U:\MAYOBS15\Utilisateurs\Carla\pour_JMA\TEST_routine_buc\NAV_MD228_MAYOBS15-135-01.csv';
            %   [timeBucScampi, latBucScampi, lonBucScampi, altitudeBucScampi, capBucScampi, immersionBucScampi, giteBucScampi, ...
            %       assietteBucScampi, lfileeBucScampi, Effort, VitesseTreuil] = BucCtdScampiUtils.readAttitudeBucScampi(nomFicBucScampi);
            %
            %   BucCtdScampiUtils.editSignals(nomFicBucScampi, timeBucScampi, latBucScampi, lonBucScampi, altitudeBucScampi, ...
            %       giteBucScampi, assietteBucScampi, lfileeBucScampi, Effort, VitesseTreuil, ...
            %       'Heading', capBucScampi, 'Immersion', immersionBucScampi, 'displayedSignalList', [1 1 1 0 1 0 0 0 0 0]);
            %
            % Authors  : JMA
            % See also : plot_BUC_SCAMPI_CTD
            %
            % Reference pages in Help browser
            % Reference pages in Help browser
            %   <a href="matlab:doc BucCtdScampiUtils.readNavigationBucNetcdf" >BucCtdScampiUtils.readNavigationBucNetcdf</a>
            %   <a href="matlab:doc BucCtdScampiUtils.readAttitudeBucScampi"   >BucCtdScampiUtils.readAttitudeBucScampi</a>
            %   <a href="matlab:doc BucCtdScampiUtils.readCTD"                 >BucCtdScampiUtils.readCTD</a>
            %   <a href="matlab:doc BucCtdScampiUtils"                         >BucCtdScampiUtils</a>
            %  -------------------------------------------------------------------------------
 
            [varargin, Heading]             = getPropertyValue(varargin, 'Heading',             []);
            [varargin, Immersion]           = getPropertyValue(varargin, 'Immersion',           []);
            [varargin, displayedSignalList] = getPropertyValue(varargin, 'displayedSignalList', []);
            
            timeSample = XSample('name', 'time', 'data', Datetime);
            
            latSample = YSample('data', Latitude, 'marker', '.');
            latSignal = ClSignal('ySample', latSample, 'xSample', timeSample);
            
            lonSample = YSample('data', Longitude, 'marker', '.');
            lonSignal = ClSignal('ySample', lonSample, 'xSample', timeSample);
            
            headingSample       = YSample('data', Heading, 'marker', '.');
            headingVesselSignal = ClSignal('ySample', headingSample,   'xSample', timeSample);
            
            immersionSample = YSample('data', Immersion, 'marker', '.');
            immersionSignal = ClSignal('ySample', immersionSample, 'xSample', timeSample);
            
            for k=1:length(varargin)
                VarName = inputname(4+k);
                Val = varargin{k};
                
                ySample = YSample('Name', VarName, 'data', Val);
                otherSignals(k)  = ClSignal('Name', VarName, 'xSample', timeSample, 'ySample', ySample); %#ok<AGROW>
            end
            
            [~, name] = fileparts(filename);
            nav = ClNavigation(latSignal, lonSignal, 'name', name, ...
                'immersionSignal', immersionSignal,  'headingVesselSignal', headingVesselSignal, ...
                'otherSignal', otherSignals);
            
            a = NavigationDialog(nav, 'Title', 'Navigation', 'CleanInterpolation', true, 'waitAnswer', false);
            
            if ~isempty(displayedSignalList)
                a.displayedSignalList = displayedSignalList;% [1   1   1   0   1   0   0   0   0   0];
            end
            pause(0.5)
            a.openDialog();
        end

    end
    
    
    methods (Static, Access = private)
        
        %         function uuuu(xx)
        %         end
    end
end
