classdef FtpUtils
    % Collection of static functions to download SonarScope demo files
    %
    % Authors  : JMA Authors
    % See also : ALL_Cache2Netcdf 
    %
    % Reference pages in Help browser
    %   <a href="matlab:doc FtpUtils" >FtpUtils</a>
    %  -------------------------------------------------------------------------------
    
    methods (Static)
        
        function ftpId = ftpSSc
            % Open the SonarScope ftp site
            %
            % Syntax
            %   ftpId = FtpUtils.ftpSSc
            %
            % Output Arguments
            %   ftpId : Id of the SSc ftp site
            %
            % Examples
            %   ftpId = FtpUtils.ftpSSc;
            %   dataFileName = '/sonarscope/public/DataForDemo/EM122/0003_20151025_112343_Atalante.all';
            %   FtpUtils.mget(ftpId, dataFileName, my_tempdir);
            %   close(ftpId);
            %
            % Authors  : JMA
            % See also : ftp
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc FtpUtils.ftpSSc"  >FtpUtils.ftpSSc</a>
            %   <a href="matlab:doc FtpUtils"         >FtpUtils</a>
            %  -------------------------------------------------------------------------------
            
            persistent persistent_Username persistent_Password
            
            ftpId      = [];
            flag       = 0;
            kIteration = 0;
            while ~flag
                kIteration = kIteration + 1;
                if kIteration == 4
                    break
                end
                
                if isempty(persistent_Username)
                    persistent_Username = '';
                end
                
                if isempty(persistent_Password)
                    [passWord, userName] = passwordEntryDialog('enterUserName', true, ...
                        'DefaultUserName' , persistent_Username, ...
                        'CheckPasswordLength', false, ...
                        'WindowName', 'Ifremer extranet login');
                    
                    if isempty(userName) && isempty(passWord)
                        return
                    else
                        persistent_Username = userName;
                        persistent_Password = passWord;
                    end
                end
                
                try
                    ftpId = ftp('eftp.ifremer.fr', persistent_Username, persistent_Password);
                    flag = 1;
                catch
                    persistent_Password = [];
                    flag = 0;
                end
            end
        end
        
        
        function localFileName = mget(ftpId, dataFileName, OutputDir)
            % Write an XML structure and its associated data into a SSc Netcdf file
            %
            % Syntax
            %   localFileName = FtpUtils.mget(ftpId, dataFileName, OutputDir)
            %
            % Input Arguments
            %   ftpId        : ftp site ID
            %   dataFileName : Name of the data file
            %   OutputDir    : Output directory name
            %
            % Output Arguments
            %   localFileName : Name of the downloaded file
            %
            % Examples
            %   dataFileName = '/sonarscope/public/DataForDemo/EM122/0003_20151025_112343_Atalante.all';
            %   dataFileName = '/sonarscope/public/DataForDemo/Geoswath/ESSTECH19020.rdf';
            %   ftpId = FtpUtils.ftpSSc;
            %   FtpUtils.mget(ftpId, dataFileName, my_tempdir);
            %   close(ftpId);
            %
            % Authors  : JMA
            % See also : ftpSSc mget
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc FtpUtils.ftpSSc"  >FtpUtils.ftpSSc</a>
            %   <a href="matlab:doc FtpUtils.mget"    >FtpUtils.mget</a>
            %   <a href="matlab:doc FtpUtils"         >FtpUtils</a>
            %  -------------------------------------------------------------------------------
            
            if ~iscell(dataFileName)
                dataFileName = {dataFileName};
            end
            
            localFileName = {};
            for k1=1:length(dataFileName)
                fileName1 = strrep(dataFileName{k1}, '\', '/');
                try
                    location = mget(ftpId, fileName1, OutputDir);
                catch ME
                    my_warndlg(ME.message, 1);
                    return
                end
                
                nomDir = fileparts(fileName1);
                
                for k2=1:length(location)
                    [flag, MESSAGE] = movefile(location{k2}, OutputDir);
                    if ~flag
                        my_warndlg(MESSAGE, 1);
                    end
                end
                
                while ~isempty(nomDir)
                    nomDir2 = fullfile(OutputDir, nomDir);
                    if isempty(listeFicOnDir(nomDir2)) && isempty(listeDirOnDir(nomDir2)) && exist(nomDir2, 'dir')
                        rmdir(nomDir2)
                        pause(0.5)
                    else
                        break
                    end
                    nomDir = fileparts(nomDir);
                end
                nomFic2 = {};
                for k2=1:length(location)
                    [~, nomFic, Ext] = fileparts(location{k2});
                    nomFic2{k2} = fullfile(OutputDir, [nomFic Ext]); %#ok<AGROW>
                end
                localFileName = [localFileName; nomFic2(:)]; %#ok<AGROW>
            end
        end
        
        
        function localFileName = importDataForDemoFromSScFTP(dataFileName, varargin)
            % Import a file from the SonarScope ftp site
            %
            % Syntax
            %   localFileName = FtpUtils.importDataForDemoFromSScFTP(dataFileName, ...)
            %
            % Input Arguments
            %   localFileName : xxxxxxxxxxxxxxxxxxxxxx
            %
            % Name-Value Pair Arguments
            %   OutputDir : Output directory name
            %
            % Output Arguments
            %   OutputDir       : Output directory (Default : your tempdir, C:\Temp, D\Temp ?))
            %   SScFtpDirectory : Ftp site directory (Default '/sonarscope/public')
            %
            % Examples
            %   dataFileName    = {'DataForDemo/NavAUV/usblRepeater09Modif.txt'};
            %   dataFileName{2} = 'DataForDemo/NavAUV/usblRepeater10Modif.txt';
            %   dataFileName{3} = 'DataForDemo/NavAUV/usblRepeater11Modif.txt';
            %   dataFileName{4} = 'DataForDemo/NavAUV/usblRepeater12Modif.txt';
            %   localFileName = FtpUtils.importDataForDemoFromSScFTP(dataFileName)
            %
            %   dataFileName = 'DataForDemo/Geoswath/ESSTECH19020.rdf';
            %   localFileName = FtpUtils.importDataForDemoFromSScFTP(dataFileName, 'OutputDir', my_tempdir)
            %
            % Authors  : JMA
            % See also : checkGetFromFtpSScDemoFiles ftpSSc mget
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc FtpUtils.ftpSSc"                       >FtpUtils.ftpSSc</a>
            %   <a href="matlab:doc FtpUtils.checkGetFromFtpSScDemoFiles"  >FtpUtils.checkGetFromFtpSScDemoFiles</a>
            %   <a href="matlab:doc FtpUtils"                              >FtpUtils</a>
            %  -------------------------------------------------------------------------------
            
            [varargin, SScFtpDirectory] = getPropertyValue(varargin, 'SScFtpDirectory', '/sonarscope/public');
            [varargin, OutputDir]       = getPropertyValue(varargin, 'OutputDir',       []); %#ok<ASGLU>
            
            if isempty(OutputDir)
                OutputDir = my_tempdir;
            end
            
            if ~exist(OutputDir, 'dir')
                [flag, msg] = mkdir(OutputDir);
                if ~flag
                    localFileName = [];
                    messageErreurFichier(OutputDir, 'WriteFailure', 'Message', msg);
                end
            end
            
            if ~iscell(dataFileName)
                dataFileName = {dataFileName};
            end
            
            hasAWildcard = 0;
            for k=1:length(dataFileName)
                hasAWildcard = ~isempty(strfind(dataFileName{k}, '*'));
                break
                % TODO JMA l'autre wilcard pour 1 caract�re ? � ???
            end
            
            if hasAWildcard % One have given the exact names, one can look if they already exist on OutputDir
                ftpId = FtpUtils.ftpSSc;
                localFileName = FtpUtils.mget(ftpId, fullfile(SScFtpDirectory, dataFileName), OutputDir);
                close(ftpId);
            else
                for k=1:length(dataFileName)
                    [~, nomFic, Ext] = fileparts(dataFileName{k});
                    nomFic = fullfile(OutputDir, [nomFic Ext]);
                    if exist(nomFic, 'file') % The file is already here
                        localFileName{k} = nomFic; %#ok<AGROW>
                    else % Access to the SonarScope ftp site
                        ftpId = FtpUtils.ftpSSc;
                        if isempty(ftpId)
                            localFileName = [];
                            return
                        end
                        nomFic2 = FtpUtils.mget(ftpId, fullfile(SScFtpDirectory, dataFileName{k}), OutputDir);
                        close(ftpId);
                        if isempty(nomFic2)
                            localFileName = [];
                            return
                        end
                        localFileName{k} = nomFic2{1}; %#ok<AGROW>
                    end
                end
            end
            
            if length(localFileName) == 1
                localFileName = localFileName{1};
            end
        end
        
        
        function [flag, localFileName] = getSScDemoFile(dataFileName, varargin)
            % Retreive a file on the local output directory. If not found, it will be downloaded from the SonarScope ftp site
            %
            % Syntax
            %   [flag, localFileName] = FtpUtils.getSScDemoFile(dataFileName, ...)
            %
            % Input Arguments
            %   dataFileName : Name of the demo file
            %
            % Name-Value Pair Arguments
            %   OutputDir : Output directory name
            %
            % Output Arguments
            %   flag          : 1=the file was found, 0 the file was not found
            %   localFileName : Name of the downloaded file 
            %
            % Examples
            %   [flag, localFileName] = FtpUtils.getSScDemoFile('usblRepeater09Modif.txt', 'OutputDir', 'D:\Temp\ExData')
            %
            % Authors  : JMA
            % See also : checkGetFromFtpSScDemoFiles importDataForDemoFromSScFTP
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc FtpUtils.checkGetFromFtpSScDemoFiles" >FtpUtils.checkGetFromFtpSScDemoFiles</a>
            %   <a href="matlab:doc FtpUtils"                             >FtpUtils</a>
            %  -------------------------------------------------------------------------------
            
            [varargin, OutputDir] = getPropertyValue(varargin, 'OutputDir', []); %#ok<ASGLU>
            
            flag = 0;
            if isempty(OutputDir)
                OutputDir = my_tempdir;
            else
                if ~exist(OutputDir, 'dir')
                    str1 = sprintf('Le r�pertoire "%s" n''existe pas.',  OutputDir);
                    str2 = sprintf('The directory "%s" does not exist.', OutputDir);
                    my_warndlg(Lang(str1,str2), 1);
                    localFileName = [];
                    return
                end
            end
            
            switch dataFileName
                case 'ESSTECH19020.rdf'
                    OutputDir = fullfile(OutputDir, 'ExRDF');
                    if ~exist(OutputDir, 'dir')
                        mkdir(OutputDir);
                    end
                    nomSubDir = 'DataForDemo/Geoswath/';
                    
                case {'usblRepeater09Modif.txt'; 'usblRepeater10Modif.txt'; 'usblRepeater11Modif.txt'; 'usblRepeater12Modif.txt'}
                    OutputDir = fullfile(OutputDir, 'ExNavAUV');
                    if ~exist(OutputDir, 'dir')
                        mkdir(OutputDir);
                    end
                    nomSubDir = 'DataForDemo/NavAUV/';
                    
                case '0014.04.im'
                    OutputDir = fullfile(OutputDir, 'ExSAR');
                    if ~exist(OutputDir, 'dir')
                        mkdir(OutputDir);
                    end
                    nomSubDir = 'DataForDemo/SAR/';
                    
                case 'Syp_001_180507104600.sdf'
                    OutputDir = fullfile(OutputDir, 'ExSDF');
                    if ~exist(OutputDir, 'dir')
                        mkdir(OutputDir);
                    end
                    nomSubDir = 'DataForDemo/SDF/';
                    
                case 'Syp_001_180507104600.xtf'
                    OutputDir = fullfile(OutputDir, 'ExXTF');
                    if ~exist(OutputDir, 'dir')
                        mkdir(OutputDir);
                    end
                    nomSubDir = 'DataForDemo/XTF/';
                    
                case 'tan1505-D20150428-T013331.raw'
                    OutputDir = fullfile(OutputDir, 'ExRawEK60');
                    if ~exist(OutputDir, 'dir')
                        mkdir(OutputDir);
                    end
                    nomSubDir = 'DataForDemo/EK60/';
                    
                case 'EST190019_D20190214_T090030.SEG'
                    OutputDir = fullfile(OutputDir, 'ExSEGY');
                    if ~exist(OutputDir, 'dir')
                        mkdir(OutputDir);
                    end
                    nomSubDir = 'DataForDemo/SBP/';
                    
                case 'Mobydick_025_20180315_002209.hac'
                    OutputDir = fullfile(OutputDir, 'ExHAC');
                    if ~exist(OutputDir, 'dir')
                        mkdir(OutputDir);
                    end
                    nomSubDir = 'DataForDemo/HAC/';
                    
                case {'0056_20150615_234546_ATL_EM710.all'; '0056_20150615_234546_ATL_EM710.wcd'}
                    OutputDir = fullfile(OutputDir, 'ALL-WithMagAndPhase');
                    if ~exist(OutputDir, 'dir')
                        mkdir(OutputDir);
                    end
                    nomSubDir = 'DataForDemo/ALL-WithMagAndPhase/';
                    
                case {'0029_20180306_213505_SimonStevin.all'; '0029_20180306_213505_SimonStevin.wcd';
                        '0030_20180306_213905_SimonStevin.all'; '0030_20180306_213905_SimonStevin.wcd'
                        '0031_20180306_214305_SimonStevin.all'; '0031_20180306_214305_SimonStevin.wcd'
                        'Mode_2_Swath_Single_Signal_CW_CompensSingle_Reflectivity_BeamPointingAngle_TxBeamIndexSwath.mat'}
                    OutputDir = fullfile(OutputDir, 'ALL-EM2040SimonsStevin');
                    if ~exist(OutputDir, 'dir')
                        mkdir(OutputDir);
                    end
                    nomSubDir = 'DataForDemo/ALL-EM2040SimonsStevin/';
                    
                case {'0306_20091113_160950_Lesuroit.all'; '0306_20091113_160950_Lesuroit.wcd';
                        '0307_20091113_163950_Lesuroit.all'; '0307_20091113_163950_Lesuroit.wcd'
                        'Mode_3_Swath_Single_Signal_CW_Compens_Reflectivity_BeamPointingAngle_TxBeamIndexSwath.mat'}
                    OutputDir = fullfile(OutputDir, 'Marmesonet-306-307');
                    if ~exist(OutputDir, 'dir')
                        mkdir(OutputDir);
                    end
                    nomSubDir = 'DataForDemo/Marmesonet-306-307/';
                    
                    
                case {'0000_20160310_115008_Belgica.all'; '0001_20160310_115615_Belgica.all'; '0002_20160310_120246_Belgica.all'; 'AbsC1610.txt'}
                    OutputDir = fullfile(OutputDir, 'ALL-EM3002WithUserAbsorption');
                    if ~exist(OutputDir, 'dir')
                        mkdir(OutputDir);
                    end
                    nomSubDir = 'DataForDemo/ALL-EM3002WithUserAbsorption/';
                    
                case '0059_20130203_134006_Thalia.all'
                    OutputDir = fullfile(OutputDir, 'ALL-WithStaveData');
                    if ~exist(OutputDir, 'dir')
                        mkdir(OutputDir);
                    end
                    nomSubDir = 'DataForDemo/ALL-WithStaveData/';
                    
                case '0013_20191022_173330_SimonStevin.all'
                    OutputDir = fullfile(OutputDir, 'ALL-WithRTK');
                    if ~exist(OutputDir, 'dir')
                        mkdir(OutputDir);
                    end
                    nomSubDir = 'DataForDemo/ALL-WithRTK/';
                    
                case '0000_20150309_200643_Belgica.all'
                    OutputDir = fullfile(OutputDir, 'ALL-EM3002');
                    if ~exist(OutputDir, 'dir')
                        mkdir(OutputDir);
                    end
                    nomSubDir = 'DataForDemo/ALL-EM3002/';
                    
                case '0000_20130802_152942_Europe.all'
                    OutputDir = fullfile(OutputDir, 'ALL-WithQFIfremer');
                    if ~exist(OutputDir, 'dir')
                        mkdir(OutputDir);
                    end
                    nomSubDir = 'DataForDemo/ALL-WithQFIfremer/';
                    
                case 'D20190625-T100713.raw'
                    OutputDir = fullfile(OutputDir, 'EK80');
                    if ~exist(OutputDir, 'dir')
                        mkdir(OutputDir);
                    end
                    nomSubDir = 'DataForDemo/EK80/';
                    
                case 'ADCP_38khz_Malaga_Brest__006_allPofil.odv'
                    OutputDir = fullfile(OutputDir, 'ODV');
                    if ~exist(OutputDir, 'dir')
                        mkdir(OutputDir);
                    end
                    nomSubDir = 'DataForDemo/ODV/';
                    
                otherwise
                    str1 = sprintf('"%s" n''a pas �t� trouv� dans les sous-r�pertoires de %s', dataFileName, '\\iota1\sonarscope\public\DataForDemo');
                    str2 = sprintf('"%s" was not found in %s sub-directories.', dataFileName, '\\iota1\sonarscope\public\DataForDemo');
                    my_warndlg(Lang(str1,str2), 1);
                    localFileName = [];
                    return
            end
            
            dataFileNameFtp = fullfile(nomSubDir, dataFileName);
            localFileName = FtpUtils.importDataForDemoFromSScFTP(dataFileNameFtp, 'OutputDir', OutputDir);
            if isempty(localFileName)
                return
            end
            if exist(localFileName, 'file')
                flag = 1;
            end
        end
        
        
        function flag = checkGetFromFtpSScDemoFiles
            % Check the import of a set of demo files from the SSc ftp site
            %
            % Syntax
            %   flag = FtpUtils.checkGetFromFtpSScDemoFiles
            %
            % Output Arguments
            %   flag : 1=OK, 0=KO
            %
            % Examples
            %   dataFileName = FtpUtils.checkGetFromFtpSScDemoFiles
            %
            % Authors  : JMA
            % See also : getSScDemoFile
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc FtpUtils.getSScDemoFile" >FtpUtils.getSScDemoFile</a>
            %   <a href="matlab:doc FtpUtils"                >FtpUtils</a>
            %  -------------------------------------------------------------------------------

            [flag, localFileName] = FtpUtils.getSScDemoFile('ESSTECH19020.rdf', 'OutputDir', 'D:\Temp\ExData');  %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, localFileName] = FtpUtils.getSScDemoFile('usblRepeater09Modif.txt', 'OutputDir', 'D:\Temp\ExData'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, localFileName] = FtpUtils.getSScDemoFile('usblRepeater10Modif.txt', 'OutputDir', 'D:\Temp\ExData'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, localFileName] = FtpUtils.getSScDemoFile('usblRepeater11Modif.txt', 'OutputDir', 'D:\Temp\ExData'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, localFileName] = FtpUtils.getSScDemoFile('usblRepeater12Modif.txt', 'OutputDir', 'D:\Temp\ExData'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, localFileName] = FtpUtils.getSScDemoFile('0014.04.im', 'OutputDir', 'D:\Temp\ExData'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, localFileName] = FtpUtils.getSScDemoFile('Syp_001_180507104600.sdf', 'OutputDir', 'D:\Temp\ExData'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, localFileName] = FtpUtils.getSScDemoFile('Syp_001_180507104600.xtf', 'OutputDir', 'D:\Temp\ExData'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, localFileName] = FtpUtils.getSScDemoFile('tan1505-D20150428-T013331.raw', 'OutputDir', 'D:\Temp\ExData'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, localFileName] = FtpUtils.getSScDemoFile('EST190019_D20190214_T090030.SEG', 'OutputDir', 'D:\Temp\ExData'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, localFileName] = FtpUtils.getSScDemoFile('Mobydick_025_20180315_002209.hac', 'OutputDir', 'D:\Temp\ExData'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, localFileName] = FtpUtils.getSScDemoFile('0059_20130203_134006_Thalia.all', 'OutputDir', 'D:\Temp\ExData'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, localFileName] = FtpUtils.getSScDemoFile('D20190625-T100713.raw', 'OutputDir', 'D:\Temp\ExData'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, localFileName] = FtpUtils.getSScDemoFile('ADCP_38khz_Malaga_Brest__006_allPofil.odv', 'OutputDir', 'D:\Temp\ExData'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, localFileName] = FtpUtils.getSScDemoFile('0013_20191022_173330_SimonStevin.all', 'OutputDir', 'D:\Temp\ExData'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, localFileName] = FtpUtils.getSScDemoFile('0000_20150309_200643_Belgica.all', 'OutputDir', 'D:\Temp\ExData'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, localFileName] = FtpUtils.getSScDemoFile('0000_20130802_152942_Europe.all', 'OutputDir', 'D:\Temp\ExData'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, localFileName] = FtpUtils.getSScDemoFile('0029_20180306_213505_SimonStevin.all', 'OutputDir', 'D:\Temp\ExData'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, localFileName] = FtpUtils.getSScDemoFile('0029_20180306_213505_SimonStevin.wcd', 'OutputDir', 'D:\Temp\ExData'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, localFileName] = FtpUtils.getSScDemoFile('0030_20180306_213905_SimonStevin.all', 'OutputDir', 'D:\Temp\ExData'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, localFileName] = FtpUtils.getSScDemoFile('0030_20180306_213905_SimonStevin.wcd', 'OutputDir', 'D:\Temp\ExData'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, localFileName] = FtpUtils.getSScDemoFile('0031_20180306_214305_SimonStevin.all', 'OutputDir', 'D:\Temp\ExData'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, localFileName] = FtpUtils.getSScDemoFile('0031_20180306_214305_SimonStevin.wcd', 'OutputDir', 'D:\Temp\ExData'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            flag = 1;
        end
    end
end
