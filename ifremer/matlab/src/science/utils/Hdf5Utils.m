classdef Hdf5Utils
    % Collection of static functions to read/write data into SonarScope Hdf5 files.
    %   Some conventions are defined for SonarScope HDF5 files as :
    %     - Statistics for all variables (or datasets)
    %     - etc
    % Authors  : GLU Authors
    % See also : ALL_Cache2HDF5, netcdf
    %
    % Reference pages in Help browser
    %   <a href="matlab:doc Hdf5Utils" >Hdf5Utils</a>
    %   <a href="matlab:doc netcdf"    >netcdf</a>
    %  -------------------------------------------------------------------------------
   
    methods (Static)
        
        function [flag, Att] = readAtt(ncFileName, grp)
            %--------------------
            % Function readAtt : Reading attributes from a group.
            %
            % -------------------
            
            flag = 0; %#ok<NASGU>
            Att = [];
            
            % Attributs globaux
            for a=1:numel(grp.Attributes)
                % Suppression de caract�res sp�ciaux.
                attName     = grp.Attributes(a).Name;
                attVal      = h5readatt(ncFileName, grp.Name, attName);
                
                if strcmp(attName(1), '_')
                    attName = attName(2:end);
                end
                if iscell(attVal)
                    Att.(attName) = attVal{:};
                else
                    if Hdf5Utils.stringIsNumeric(attVal)
                        Att.(attName) = str2double(attVal);
                    else
                        Att.(attName) = attVal;
                    end
                end
            end
            flag = 1;
        end
        
        
        function [flag, Data, tableDim] = readVar(ncFileName, grp, tableDim, varargin)
            %--------------------
            % Function readVar : Reading variables from a group.
            % -------------------
            
            [varargin, ArboOnly]   = getPropertyValue(varargin, 'ArboOnly',   0);
            [varargin, Memmapfile] = getPropertyValue(varargin, 'Memmapfile', 0); %#ok<ASGLU>
            
            flag = 0; %#ok<NASGU>
            Data = [];
            % Lecture des datasets(variables)
            nDataset = 0;
            
            % Ouverture du fichier commune a la lecture des attributs et des datasetss
            fid = H5F.open (ncFileName, 'H5F_ACC_RDONLY', 'H5P_DEFAULT');
            % Enumeration reading
            if ~isempty(grp.Datatypes)
                fieldEnum = {grp.Datatypes(:).Name};
                if numel(fieldEnum)
                    for k=1:numel(fieldEnum)
                        % if fieldEnum is a type of not
                        if strcmpi(grp.Datatypes(k).Class, 'H5T_ENUM')
                            type_id = H5T.open(fid,fieldEnum{k});
                            % class_id = H5T.get_class(type_id);
                            num_members = H5T.get_nmembers(type_id);
                            for j = 1:num_members
                                nameEnum = regexp(fieldEnum{k}, '/', 'split');
                                nameEnum = nameEnum{end};
                                Data.Enum.(nameEnum).name{j} = H5T.get_member_name(type_id,j-1);
                                Data.Enum.(nameEnum).value(j) = H5T.enum_valueof(type_id, ...
                                    Data.Enum.(nameEnum).name{j});
                            end
                            
                            % H5T.close(class_id);
                            H5T.close(type_id);
                        end
                    end
                end
            end % End of Enumeration identification.
            
            for d=1:numel(grp.Datasets)
                dsName = [grp.Name '/' grp.Datasets(d).Name];
                dset_id    = H5D.open(fid,dsName);
                type_id    = H5D.get_type(dset_id);
                dsVal      = H5D.read(dset_id); % H5D.read(dset_id, 'H5ML_DEFAULT', 'H5S_ALL', 'H5S_ALL', 'H5P_DEFAULT');
                isDimScale = H5DS.is_scale(dset_id);
                if strcmpi(grp.Datasets(d).Name, 'seabed_image_samples_r') == 1
                    % Identification des dimensions.
                    % D�finir la s�lection pour le sous-ensemble
                    dspace_id        = H5D.get_space(dset_id);
                    [ndims, h5_dims] = H5S.get_simple_extent_dims(dspace_id); %#ok<ASGLU> 
                    sizeOfType       = H5T.get_size(type_id);
                    sizeOfVar        = prod(h5_dims) * sizeOfType;
                    if sizeOfVar / 1024^3 > 0.5 % 0.5 GBytes max
                        continue
                    end
                end
                                
                % Dataset is Enumeration type : reading for the dataset.
                if H5T.detect_class(type_id, 'H5T_ENUM')
                    num_members = H5T.get_nmembers(type_id);                   
                    for j = 1:num_members
                        % Data.Enum.(grp.Datasets(d).Name).name{j} = H5T.get_member_name(type_id,j-1);
                        % Data.Enum.(grp.Datasets(d).Name).value(j) = H5T.enum_valueof(type_id, ...
                        %     Data.Enum.(grp.Datasets(d).Name).name{j});
                        % A d�faut de r�cup�rer le nom du type Enum, on
                        % r�cup�re localement ses valeurs dans la
                        % description de la variable.
                        Data.(grp.Datasets(d).Name).Enum.name{j} = H5T.get_member_name(type_id,j-1); 
                        Data.(grp.Datasets(d).Name).Enum.value(j) = H5T.enum_valueof(type_id, ...
                            Data.(grp.Datasets(d).Name).Enum.name{j});
                    end
                end

                if isDimScale
                    [~, ia, ~]             = intersect({grp.Datasets(d).Attributes(:).Name},'_Netcdf4Dimid');
                    tableDim(end+1).Name    = grp.Datasets(d).Name;
                    tableDim(end).Grp       = grp.Name;
                    tableDim(end).Id        = grp.Datasets(d).Attributes(ia).Value;
                    
                    % Data.Dim.(grp.Datasets(d).Name) = numel(dsVal);
                    if isfield(Data, 'Dim')
                        Data.Dim(end+1).name    = grp.Datasets(d).Name;
                        Data.Dim(end).value     = numel(dsVal);
                        Data.Dim(end).id        = grp.Datasets(d).Attributes(ia).Value;
                    else
                        Data.Dim(1).name    = grp.Datasets(d).Name;
                        Data.Dim(1).value   = numel(dsVal);
                        Data.Dim(1).id      = grp.Datasets(d).Attributes(ia).Value;
                    end
                end

                datasetIsTime = 0;
                nAtt = numel(grp.Datasets(d).Attributes);
                if nAtt > 0
                    [~, ia, ~] = intersect({grp.Datasets(d).Attributes(:).Name}, 'standard_name');
                    if ia
                        if strcmpi(grp.Datasets(d).Attributes(ia).Value, 'time')
                            datasetIsTime = 1;
                        end
                    end
                end
                
                % Cas de dataset time qui sont des dimensions et des datasets.
                % if datasetIsTime || (~isDimScale && numel(dsVal))
                if datasetIsTime || (numel(dsVal))
                    for a=1:nAtt
                        nameAtt = grp.Datasets(d).Attributes(a).Name;
                        if nameAtt(1) == '_'
                            nameAtt = nameAtt(2:end);
                        end
                        attVal = h5readatt(ncFileName, [grp.Name '/' grp.Datasets(d).Name], grp.Datasets(d).Attributes(a).Name);
                        if Hdf5Utils.stringIsNumeric(attVal)
                            Data.(grp.Datasets(d).Name).(nameAtt) = str2double(attVal);
                        else
                            Data.(grp.Datasets(d).Name).(nameAtt) = attVal;
                        end
                    end
                    
                    % Effacement des champs propres au Hdf.
                    if isfield(Data.(grp.Datasets(d).Name), 'DIMENSION_LIST')
                        Data.(grp.Datasets(d).Name) = rmfield(Data.(grp.Datasets(d).Name), 'DIMENSION_LIST');
                    end
                    
                    % Conversion in datetime if dataset is a time vector.
                    if datasetIsTime
                        [~, ia, ~] = intersect({grp.Datasets(d).Attributes(:).Name}, 'units');
                        if ia
                            strUnits = grp.Datasets(d).Attributes(ia).Value;
                            dsVal = Hdf5Utils.convertFromTimeUnit(dsVal, strUnits);
                        end
                    end
                    
                    % Check pour contr�ler le type de stockage en MatLab
                    % d'une dataset de type Variable Length.
                    strTypeId	= Hdf5Utils.identifyDataType(type_id);
                    Data.(grp.Datasets(d).Name).ClassId = strTypeId;                  
                    
                    Variable            = Data.(grp.Datasets(d).Name);
                    Variable.Datatype   = class(dsVal);
                    Variable.Dimensions = size(dsVal);
                    [~, TypeMemmapfile] = Hdf5Utils.CanVarBeMemmapfile(Variable, Memmapfile); 
                    
                    if ~ArboOnly
                        if H5T.detect_class(type_id, 'H5T_VLEN')
%                             strWrn  = sprintf('%s : Variable de type Vlen, voir avec JMA pour le stockage', dsName);
%                             pppp    = dbstack(1).name;
%                             fprintf('==> %s : %s\n', pppp, strWrn);
                            Data.(grp.Datasets(d).Name).value = dsVal; % 'Voir JMA';
%                             maxSamples  = max(arrayfun(@(x) numel(x{:}), dsVal(:)));
                            % dummy       = NaN(size(rdata,2), maxSamples);
                            
                        else
                            % TODO JMA : g�rer TypeMemmapfile=3 pour demander une copie par morceaux
                            switch TypeMemmapfile
                                case 1 % Transfert de la donn�e en Memmapfile
                                    % TODO GLU : faire un m�chanisme plus sophistiqu�
                                    % (m�morisation du nom du groupe, du nom de la
                                    % variable, etc et ne cr�er r�ellement le fichier
                                    % memmapfile que si on demande la lecture des valeurs .
                                    % Attention, on fait souvent class(X(1)) dans SSc, il
                                    % faut d�tecter ce cas pour ne pas cr�er de fichiers
                                    % inutilement
                                    
                                    Data.(grp.Datasets(d).Name).value = cl_memmapfile('Value', dsVal);
                                case 2 % Pas de transfert de la donn�e (appel si par exemple on veut lire les signaux mais pas les images :
                                    % OK, c'est pas top, il faudrait plut�t faire une fonction sp�cifique. Appel dans ALL_writePingFlagsWaterColumn
                                    % my_breakpoint
                                otherwise % Transfert int�gral de la donn�e
                                    Data.(grp.Datasets(d).Name).value = dsVal;
                            end
                        end
                    end
                    nDataset = nDataset + 1;
                    
                    % Close dataset.
                    H5D.close(dset_id);

                end
                
            end
            
            % Close file.
            H5F.close(fid);

            flag = 1;
        end % ---End readVar
        
        
        function [flag, HdfData] = readGrpData(ncFileName, rootName, varargin)
            % Reads a HDF5 file
            %
            % Syntax
            %   [flag, Hdf5Data] = Hdf5Utils.readGrpData(ncFileName, ...)
            %
            % Input Arguments
            %   ncFileName : Name of the Data file
            %   grpName    : Name of the HDF5 group (Ex : Runtime)
            %
            % Name-Value Pair Arguments
            %   ArboOnly    : true=only the XML file information is returned, not the data (Default : false)
            %   BinInData  : 0 : Data contains only the Data structure and Bin contains the data
            %                1 : Data contains both the Data and the Bin structure
            %   Memmapfile : Parameter used to optimize the memory (Default : 0)
            %                 0 : data is returned in memory (Ram)
            %                 1 : data is returned in virtual memory (cl_memmapfile objetcs)
            %                -1 : Signals are returned in memory, Images (Matices) are returned in virtual memory
            %
            % Output Arguments
            %   Data : Data structure of the file
            %
            % Examples
            %   nomFicXsf       = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\From JMA\EM304\0023_20180628_122559_ShipNameFromAll.xsf.nc';
            %
            %   [flag, Hdf5Data] = Hdf5Utils.readGrpData(nomFicXsf, '/');
            %   [flag, Hdf5Data] = Hdf5Utils.readGrpData(nomFicXsf, '/Environment');
            %   [flag, Hdf5Data] = Hdf5Utils.readGrpData(nomFicXsf, '/Environment/Dynamic_draught');
            %   % Only arborescence
            %   [flag, Hdf5Data] = Hdf5Utils.readGrpData(nomFicXsf1, '/', 'ArboOnly', 0);
            %
            %   % Output huge variables in Memmapfile
            %   [flag, Hdf5Data1] = Hdf5Utils.readGrpData(nomFicXsf1, '/', 'Memmapfile', -1);
            %   [flag, Hdf5Data1] = Hdf5Utils.readGrpData(nomFicXsf1, '/', 'Memmapfile', 0);
            %
            % See also SScCacheHdf5Utils.readWholeData
            % Authors : JMA
            %
            % Reference pages in Help browser
            %   TODO
            %  -------------------------------------------------------------------------------
            
            [varargin, ArboOnly]   = getPropertyValue(varargin, 'ArboOnly',   0);
            [varargin, Memmapfile] = getPropertyValue(varargin, 'Memmapfile', 0); %#ok<ASGLU>
            
            HdfData = [];
            
            flag = exist(ncFileName, 'file');
            if ~flag
                return
            end
            
            h5Info = h5info(ncFileName);
            
            fieldName = 'xsf_convention_version';
%             idxField  = find(arrayfun(@(x) strcmpi(x.Name, fieldName), h5Info.Attributes));
            idxField  = find(arrayfun(@(x) strcmpi(x.Name, fieldName), h5Info.Attributes), 1);
            if isempty(idxField)
                    strWrn  = sprintf(  ['Issue in XSF Format. Impossible to identify xsf Convetion Version.\n'...
                                        'Please, check the field %s in global attributes'], fieldName);
                    my_warndlg(strWrn, 0, 'Tag', 'MessageHDF5'); 
%             else
%                 releaseVersion = str2double(h5Info.Attributes(idxField).Value);
% %                 if releaseVersion < 0.2
% %                     strWrn  = sprintf('XSF Format version (%s) is too old. Please, regenerate the XSF with a newer version of Globe', ...
% %                                         h5Info.Attributes(idxField).Value);
% %                     my_warndlg(strWrn, 0, 'Tag', 'MessageHDF5'); 
% %                     flag = 0;
% %                     return
% %                 end
            end
            
            
            %% Open file
            
            for kBug=1:1 % 10
                if kBug > 1
                    fprintf('>-------------- readGrpData kBug=%d --------------<\n', kBug);
                end
                
                %% Analyse du fichier
                
                strWrn  = sprintf('Reading HDF5 file %s', ncFileName);
                hWrn = my_warndlg(strWrn, 0, 'Tag', 'MessageHDF5');
                
                if strcmpi(rootName, '/')
                    %% Lecture des attributs significatifs.
                    [flag, Att] = Hdf5Utils.readAtt(ncFileName, h5Info);
                    if flag
                        HdfData.Att = Att;
                    end
                    if isempty(HdfData)
                        strWrn  = sprintf('Problem in HDF5 attributes reading:%s', ncFileName);
%                         titreWrn = 'HDF5 reading';
%                         hWrn    = warndlg(strWrn, titreWrn) ; %#ok<NASGU>
                        my_warndlg(strWrn, 0, 'Tag', 'MessageHDF5');
                        return
                    end
                    subInfo = h5Info;
                else
                    [flag, subInfo] = Hdf5Utils.extractGrpInfo(h5Info, rootName);
                    if ~flag || isempty(subInfo)
                        strWrn = sprintf('Problem in HDF5 subgroup identification. Please, check your request.');
                        pppp   = dbstack;
                        fprintf('==> %s : %s\n', pppp.name, strWrn);
%                         titreWrn = 'HDF5 reading';
%                         hWrn     = warndlg(strWrn, titreWrn) ; %#ok<NASGU>
                        my_warndlg(strWrn, 0, 'Tag', 'MessageHDF5');
                        flag     = 0;
                        return
                    end
                    [flag, Att] = Hdf5Utils.readAtt(ncFileName, subInfo);
                    if flag
                        HdfData.Att = Att;
                    else
                        strWrn = sprintf('Problem in HDF5 subgroup identification. Please, check your request.');
                        pppp   = dbstack;
                        fprintf('==> %s : %s\n', pppp.name, strWrn);
%                         titreWrn = 'HDF5 reading';
%                         hWrn = warndlg(strWrn, titreWrn) ; %#ok<NASGU>
                        my_warndlg(strWrn, 0, 'Tag', 'MessageHDF5');
                        flag = 0;
                        return
                    end
                end
                
                %% Datasets reading from the current group
                
                tableDim = [];
                [flag, Data, tableDim] = Hdf5Utils.readVar(ncFileName, subInfo, tableDim);
                if ~flag
                    strWrn = sprintf('Problem in HDF5 dataset reading. Please check the file.');
                    pppp   = dbstack;
                    fprintf('==> %s : %s\n', pppp.name, strWrn);
%                     titreWrn = 'HDF5 reading';
%                     hWrn     = warndlg(strWrn, titreWrn) ; %#ok<NASGU>
                    my_warndlg(strWrn, 0, 'Tag', 'MessageHDF5');
                    flag     = 0;
                    return
                elseif ~isempty(Data)
                    fNames  = fieldnames(Data);
                    for f=1:numel(fNames)
                        HdfData.(fNames{f}) = Data.(fNames{f});
                    end
                    if isfield(Data, 'Enum')
                        fNames  = fieldnames(Data.Enum);
                        for f=1:numel(fNames)
                            HdfData.Enum.(fNames{f}) = Data.Enum.(fNames{f});
                        end
                    end
                end               
                
                %% Groups reading
                
                for g=1:numel(subInfo.Groups)
                    grpName = regexp(subInfo.Groups(g).Name, '\/', 'split');
                    grpName = grpName{end};
                    [flag, Data, tableDim] = Hdf5Utils.readGrpRecursif(ncFileName, subInfo.Groups(g), tableDim, ...
                                            'ArboOnly', ArboOnly, ...
                                            'Memmapfile', Memmapfile);
                    if ~flag
                        strWrn = sprintf('Problem in HDF5 group reading');
                        pppp   = dbstack(1).name;
                        fprintf('==> %s : %s\n', pppp, strWrn);
%                         titreWrn  = 'HDF5 reading';
%                         [flag, ~] = warndlg(htmlStr, titreWrn) ;
                        my_warndlg(strWrn, 0, 'Tag', 'MessageHDF5');
                        return
                    end
                    
                    % Datasets assignation in output structure.
                    fNames  = fieldnames(Data);
                    for f=1:numel(fNames)
                        % Test only on 1st char.
                        if isstrprop(grpName(1),'digit')
                            grpName = ['Sensor' grpName];
                        end
                        if contains(fNames{f}, 'Dim')
                            HdfData.(grpName).Dim = Data.Dim;
                        else
                            if isfield(Data.(fNames{f}), 'Netcdf4Coordinates')
                                for j=1:numel(Data.(fNames{f}).Netcdf4Coordinates)
                                    % Identify concerned dimension
                                    idDim = [tableDim(:).Id] == Data.(fNames{f}).Netcdf4Coordinates(j);
                                    Data.(fNames{f}).Dim(j).name    = tableDim(idDim).Name;
                                    Data.(fNames{f}).Dim(j).grp     = tableDim(idDim).Grp;
                                    Data.(fNames{f}).Dim(j).id      = tableDim(idDim).Id;
                                end
                            end
                            if ~isfield(Data.(fNames{f}), 'Dim')
                                if isfield(Data.(fNames{f}), 'Netcdf4Dimid')
                                    % Identify concerned dimension
                                    idDim = [tableDim(:).Id] == Data.(fNames{f}).Netcdf4Dimid;
                                    Data.(fNames{f}).Dim.name    = tableDim(idDim).Name;
                                    Data.(fNames{f}).Dim.grp     = tableDim(idDim).Grp;
                                    Data.(fNames{f}).Dim.id      = tableDim(idDim).Id;
                                end
                            end
                            % Dataset creation if not a dimension.
                            HdfData.(grpName).(fNames{f}) = Data.(fNames{f});
                        end
                        % end
                    end
                    
                    
                    % Datasets(variables) dimension attribution once they are known
                    %{
                    if isfield(Data, 'Dim') && nDataset > 0
                        datasetName     = fieldnames(Data);
                        [C, ia , ib]    = intersect(datasetName, 'Dim');
                        datasetName(ia) = [];
                        for d=1:nDataset
                            if isfield(Data.(datasetName{d}), 'Netcdf4Coordinates')
                                Data.(datasetName{d}).Dim     = {};
                                for j=1:numel(Data.(datasetName{d}).Netcdf4Coordinates)
                                    % Identification de la dimension concern�e
                                    idDim = find([tableDim(:).Id] == Data.(datasetName{d}).Netcdf4Coordinates(j));
                                    Data.(datasetName{d}).Dim{end+1} = tableDim(idDim).Name;
                                end
                            end
                        end
                    end
                    %}
                    
                end
                
                HdfData.fileName    = ncFileName;
                
                if ishandle(hWrn)
                    delete(hWrn);
                end
                
                %% End
                flag = 1;
            end
            if ~flag
                return
            end
        end % ---End readGrpData
        
        
        function [flag, subInfo] = extractGrpInfo(rootInfo, grpName)
            %--------------------
            % Function extractGrpInfo : recursive function to extract h5Info only for
            % subgroup
            %
            % -------------------
            
            flag    = 0; %#ok<NASGU>
            subInfo = [];
            
            % Substructure extraction for requested subgroup.
            if strcmpi(grpName, rootInfo.Name)
                subInfo = rootInfo;
            elseif numel(rootInfo.Groups) > 0
                if strcmpi(grpName, rootInfo.Name)
                    subInfo = rootInfo;
                else
                    for g=1:numel(rootInfo.Groups)
                        [flag, subInfo] = Hdf5Utils.extractGrpInfo(rootInfo.Groups(g), grpName);
                        if ~flag
                            return
                        elseif ~isempty(subInfo)
                            break
                        end
                    end
                end
            end
            
            flag = 1;
        end % --- End extractGrpInfo
        
        
        function [flag, sRootGrp, tableDim] = readGrpRecursif(ncFileName, grp, tableDim, varargin)
            %--------------------
            % Function readGrpRecursif : recursive reading of a group (with its
            % attributs, dimensions and datasets).
            % -------------------
            
            [varargin, ArboOnly]   = getPropertyValue(varargin, 'ArboOnly',   0);
            [varargin, Memmapfile] = getPropertyValue(varargin, 'Memmapfile', 0); %#ok<ASGLU>
        
            flag     = 0; %#ok<NASGU>
            sRootGrp = [];
            
            % Lecture des attributs
            [flag, Att] = Hdf5Utils.readAtt(ncFileName, grp);
            if ~isempty(Att)
                % Assignation des attributs en sortie.
                fNames = fieldnames(Att);
                for f=1:numel(fNames)
                    sRootGrp.Att.(fNames{f}) = Att.(fNames{f});
                end
            elseif ~flag
                return
            end
            
            [flag, Datasets, tableDim] = Hdf5Utils.readVar(ncFileName, grp, tableDim,  ...
                                                                'ArboOnly', ArboOnly, ...
                                                                'Memmapfile', Memmapfile);
            if ~isempty(Datasets)
                % Assignation des attributs en sortie.
                fNames = fieldnames(Datasets);
                for f=1:numel(fNames)
                    sRootGrp.(fNames{f}) = Datasets.(fNames{f});
                end
            elseif ~flag
                return
            end
            
            % Lecture des sous-groupes par r�cursivit�
            sSubGrp = [];
            if ~isempty(grp.Groups)
                for g=1:numel(grp.Groups)
                    subGrp1Name = regexp(grp.Groups(g).Name, '/', 'split');
                    subGrp1Name = subGrp1Name{end};
                    % Test only on 1st char .
                    if isstrprop(subGrp1Name(1),'digit')
                        subGrp1Name = ['Sensor' subGrp1Name];
                    end
                    [flag, subGrp, tableDim] = Hdf5Utils.readGrpRecursif(ncFileName, grp.Groups(g), tableDim, ...
                                                                                                'ArboOnly', ArboOnly, ...
                                                                                                'Memmapfile', Memmapfile);
                    if ~isempty(subGrp)
                        % Assignation des attributs en sortie.
                        fNames = fieldnames(subGrp);
                        for f=1:numel(fNames)
                            sSubGrp.(subGrp1Name).(fNames{f}) = subGrp.(fNames{f});
                            if ~contains(fNames{f}, 'Dim')
                                % Les deux attributs peuvent cohabiter : cas de beamwidth_receive_major
                                if isfield(sSubGrp.(subGrp1Name).(fNames{f}), 'Netcdf4Coordinates')
                                    sSubGrp.(subGrp1Name).(fNames{f}).Dim = [];
                                    for j=1:numel(sSubGrp.(subGrp1Name).(fNames{f}).Netcdf4Coordinates)
                                        % Identification de la dimension concern�e
                                        idDim = [tableDim(:).Id] == sSubGrp.(subGrp1Name).(fNames{f}).Netcdf4Coordinates(j);
                                        sSubGrp.(subGrp1Name).(fNames{f}).Dim(end+1).name   = tableDim(idDim).Name;
                                        sSubGrp.(subGrp1Name).(fNames{f}).Dim(end).grp      = tableDim(idDim).Grp;
                                        sSubGrp.(subGrp1Name).(fNames{f}).Dim(end).id       = tableDim(idDim).Id;
                                    end
                                end
                                if ~isfield(sSubGrp.(subGrp1Name).(fNames{f}), 'Dim')
                                    if isfield(sSubGrp.(subGrp1Name).(fNames{f}), 'Netcdf4Dimid')
                                        % Identification de la dimension concern�e
                                        idDim = [tableDim(:).Id] == sSubGrp.(subGrp1Name).(fNames{f}).Netcdf4Dimid;
                                        sSubGrp.(subGrp1Name).(fNames{f}).Dim.name   = tableDim(idDim).Name;
                                        sSubGrp.(subGrp1Name).(fNames{f}).Dim.grp    = tableDim(idDim).Grp;
                                        sSubGrp.(subGrp1Name).(fNames{f}).Dim.id     = tableDim(idDim).Id;
                                    end
                                end
                            end
                        end
                    elseif ~flag
                        return
                    end
                end
            end
            
            % Assignation des sous-groupes en sortie.
            if ~isempty(sSubGrp)
                fNames = fieldnames(sSubGrp);
                for f=1:numel(fNames)
                    sRootGrp.(fNames{f}) = sSubGrp.(fNames{f});
                end
            end
            flag = 1;
        end
        
        % ---End readGrpRecursif
        
        
        function [flag, DataAttitude, indSystemUsed] = readAttitude(DataPlatform, varargin)
            % Function : read attitude from xsf file.
            %
            % Example :
            % ---------
            % nomFicXsf = TODO
            % [flag, Hdf5Data] = Hdf5Utils.readGrpData(nomFicXsf, '/');
            %
            % [flag, DataAttitude, nbSensors, indSystemUsed] = Hdf5Utils.readAttitude(Data.Platform, 'Memmapfile', -1);
            % ###################################################################################
            
            [varargin, ApplyCalibrationOffsets] = getPropertyValue(varargin, 'ApplyCalibrationOffsets', false);
            [varargin, sensorID]                = getPropertyValue(varargin, 'SensorId',                0); %#ok<ASGLU>
            
            flag          = 0;
            indSystemUsed = []; % TODO, identify those are used.
            
            DataAttitude            = Hdf5Utils.getMetadata('Attitude');
            
            % Motion Reference Unit linked to the platform
            MRU.X                  = DataPlatform.MRU_offset_x;
            MRU.Y                  = DataPlatform.MRU_offset_y;
            MRU.Z                  = DataPlatform.MRU_offset_z;
            MRU.RollCalibration    = DataPlatform.MRU_rotation_x;
            MRU.PitchCalibration   = DataPlatform.MRU_rotation_y;
            MRU.HeadingCalibration = DataPlatform.MRU_rotation_z;

            MRU.TimeDelay = 0; % GLU : Non identifi� dans le Xsf.

            %% Identify number of Sensors
            
            % [DataSensor, nbSensors]     = Hdf5Utils.identifySensor(DataPlatform.Attitude);
            strSensor = sprintf('Sensor%03d', sensorID);
            if isfield(DataPlatform.Attitude, strSensor)
                DataSensor = DataPlatform.Attitude.(strSensor);
            else
                strMsg = sprintf('Sensor %03d does not exist.\nPlease, check your input.', sensorID);
                pppp   = dbstack;
                strWrn = sprintf('==> %s/%s %s.', mfilename, pppp.name, strMsg);
                fprintf('%s\n', strWrn);
                return                
            end
            
            if isfield(DataSensor.Vendor_specific, 'sensor_system_descriptor')
                SensorSystemDescriptor = Hdf5Utils.read_value(DataSensor.Vendor_specific.sensor_system_descriptor);
                DataAttitude.SensorSystemDescriptor = SensorSystemDescriptor;
            end

            %% Assignation Data for Ssc from Xsf structure.

            T = DataSensor.time.value;
            [T, ~]      = sort(T);
            sub         = (diff(T)> 0);
            sub(end+1)  = 1;
            DataAttitude.Time    = T(sub);
            DataAttitude.Roll    = Hdf5Utils.read_value(DataSensor.roll, 'SubIndex', sub);
            DataAttitude.Pitch   = Hdf5Utils.read_value(DataSensor.pitch, 'SubIndex', sub);
            DataAttitude.Heading = Hdf5Utils.read_value(DataSensor.heading, 'SubIndex', sub);
            DataAttitude.Heave   = Hdf5Utils.read_value(DataSensor.vertical_offset, 'SubIndex', sub);
            if isfield(DataSensor.Vendor_specific, 'sensor_status')
                DataAttitude.Sensorstatus   = Hdf5Utils.read_value(DataSensor.Vendor_specific.sensor_status, 'SubIndex', sub);
            end

            if ApplyCalibrationOffsets

                %% D�calage temporel
                DataAttitude.Time = DataAttitude.Time + MRU.TimeDelay; % TODO : + en attendant confirmation Herv�

                %% Application des offsets
                % Champs non identifi�s dans le xsf
                DataAttitude.Roll    = DataAttitude.Roll(:,:)    + Hdf5Utils.read_value(MRU.RollCalibration);
                DataAttitude.Pitch   = DataAttitude.Pitch(:,:)   + Hdf5Utils.read_value(MRU.PitchCalibration);
                DataAttitude.Heading = DataAttitude.Heading(:,:) + Hdf5Utils.read_value(MRU.HeadingCalibration);

            end
            
            DataAttitude.ApplyCalibrationOffsets = ApplyCalibrationOffsets;
            DataAttitude.Dimensions.NbSamples    = length(DataAttitude.Heading);

            flag = 1;
        end % End of readAttitude
         
        
        function [flag, DataRuntime] = readRuntime(DataPlatform, varargin)
            % Function : read position from xsf file.
            %
            % Example :
            % ---------
            % nomFicXsf = TODO
            % [flag, Hdf5Data] = Hdf5Utils.readGrpData(nomFicXsf, '/');
            %
            % [flag, DataRuntime] = Hdf5Utils.readRuntime(Hdf5Data.Platform);
            % ###################################################################################
            
            flag        = 0;  %#ok<NASGU>
            DataRuntime = [];
                        
            % Identification of fields
            runtime = DataPlatform.Vendor_specific.runtime;
            
            % Case of kmall
            if isfield(runtime, 'runtime_txt')
                % TODO 
            end
            
            fNames  = fieldnames(runtime);
            % explore and split Dim if exist. 
            [~, ia] = intersect(fNames, 'Dim');
            if ia
                DataRuntime.Dimension.NbSamples = runtime.(fNames{ia}).value;
                fNames(ia) = [];
            end
            
            for f=1:numel(fNames)
                %% Assignation Data for Ssc from Xsf structure.
                DataRuntime.(fNames{f}) = Hdf5Utils.read_value(runtime.(fNames{f}));                
            end
            
            flag = 1;
        end % End of readRuntime
        
        
        function Metadata = getMetadata(nameDg, varargin)
            
            % Recovery of metadata, for compliance with SonarScope netCDF
            % Cache format.
            Metadata.Title         = nameDg; % Datagram
            % Metadata.EmModel       = 'ToBeFilled';
            % Metadata.Constructor   = 'Kongsberg'; % S7K TODO
            Metadata.TimeOrigin    = '01/01/-4713';
            Metadata.Comments      = 'ToBeFilled';
            Metadata.FormatVersion = 20210331;

        end

        
        function [flag, DataSSP] = readSoundSpeedProfile(DataEnv, varargin)
            % Function : read Sound Speed Profile from xsf file.
            %
            % Example :
            % ---------
            % nomFicXsf = TODO
            % [flag, Hdf5Data] = Hdf5Utils.readGrpData(nomFicXsf, '/');
            %
            % [flag, DataSSP]     = Hdf5Utils.readSoundSpeedProfile(Hdf5Data.Environment);
            % 
            % ###################################################################################
            
            [varargin, profileID] = getPropertyValue(varargin, 'ProfileId', 1); %#ok<ASGLU>
            
            flag = 0;  %#ok<NASGU>
                         
            try
                DataSSP = Hdf5Utils.getMetadata('SoundSpeedProfile');
                
                DataSSP.PingCounter            = 'Unknown';
                if isfield(DataEnv.Sound_speed_profile, 'sample_depth')
                    DataSSP.Depth                  = Hdf5Utils.read_value(DataEnv.Sound_speed_profile.sample_depth);
                else
                    DataSSP.Depth                  = Hdf5Utils.read_value(DataEnv.Sound_speed_profile.depth);
                end
                if isfield(DataEnv.Sound_speed_profile, 'salinity')
                    DataSSP.Salinity  = Hdf5Utils.read_value(DataEnv.Sound_speed_profile.salinity);
                else
                    DataSSP.Salinity  = {[]};
                end
                if isfield(DataEnv.Sound_speed_profile, 'temperature')
                    DataSSP.Temperature  = Hdf5Utils.read_value(DataEnv.Sound_speed_profile.temperature);
                else
                    DataSSP.Temperature  = {[]};
                end
                SoundSpeed    = Hdf5Utils.read_value(DataEnv.Sound_speed_profile.sound_speed);
                if iscell(DataSSP.Depth(1))
                    DataSSP.Depth = cellfun(@(x) -single(x'), DataSSP.Depth, 'Un', 0);
                    DataSSP.Depth = DataSSP.Depth{profileID};
                    DataSSP.SoundSpeed = cellfun(@(x) single(x'), SoundSpeed, 'Un', 0);
                    DataSSP.SoundSpeed = DataSSP.SoundSpeed{profileID};
                    DataSSP.Salinity = cellfun(@(x) single(x'), DataSSP.Salinity, 'Un', 0);
                    DataSSP.Salinity = DataSSP.Salinity{profileID};
                    DataSSP.Temperature = cellfun(@(x) single(x'), DataSSP.Temperature, 'Un', 0);
                    DataSSP.Temperature = DataSSP.Temperature{profileID};
                else
                    DataSSP.Depth       = single(-DataSSP.Depth');
                    DataSSP.SoundSpeed  = SoundSpeed(:, profileID);
                    DataSSP.SoundSpeed  = single(DataSSP.SoundSpeed');
                    DataSSP.Salinity    = single(DataSSP.Salinity');
                    DataSSP.Temperature = single(DataSSP.Temperature');
                end
                DataSSP.DepthResolution        = NaN; % mean(diff(DataSSP.Depth));
                if isfield(DataEnv.Sound_speed_profile, 'profile_time')
                    DataSSP.TimeProfileMeasurement = Hdf5Utils.read_value(DataEnv.Sound_speed_profile.profile_time);
                else
                    DataSSP.TimeProfileMeasurement = Hdf5Utils.read_value(DataEnv.Sound_speed_profile.time);
                end
                if isfield(DataEnv.Sound_speed_profile, 'measure_time')
                    DataSSP.TimeMeasurement   = Hdf5Utils.read_value(DataEnv.Sound_speed_profile.measure_time);
                end
                if isfield(DataEnv.Sound_speed_profile, 'datagram_time')
                    DataSSP.TimeStartOfUse  = datenum(Hdf5Utils.read_value(DataEnv.Sound_speed_profile.Vendor_specific.datagram_time));
                else
                    DataSSP.TimeStartOfUse  = datenum(DataSSP.TimeMeasurement(1));
                end
                DataSSP.NbEntries              = uint16(Hdf5Utils.read_value(DataEnv.Sound_speed_profile.sample_count));

            catch ME
                my_breakpoint
                ErrorReport = getReport(ME); %#ok<NASGU>
                pause(0.2);
            end
            
            flag = 1;
        end % End of readPosition

        
        function [flag, DataPosition, indSystemUsed] = readPosition(DataPlatform, varargin)
            % Function : read position from xsf file.
            %
            % Example :
            % ---------
            % nomFicXsf = TODO
            % [flag, Hdf5Data] = Hdf5Utils.readGrpData(nomFicXsf, '/');
            %
            % [flag, DataPosition, nbSensors, indSystemUsed] = Hdf5Utils.readPosition(Hdf5Data.Platform);
            % fileName = Data.fileName;
            % plot_navigation(fileName, [], DataPosition.Longitude, DataPosition.Latitude, DataPosition.Time, ...
            % DataPosition.CourseVessel);
            % ###################################################################################
            
            [varargin, ApplyCalibrationOffsets] = getPropertyValue(varargin, 'ApplyCalibrationOffsets', false); %#ok<ASGLU>
            [varargin, sensorID]                = getPropertyValue(varargin, 'SensorId',                0); %#ok<ASGLU>
            
            flag          = 0; 
            indSystemUsed = [];
                        
            DataPosition = Hdf5Utils.getMetadata('Position');

            % Identification of sensor substructure
            % [DataSensor, nbSensors]     = Hdf5Utils.identifySensor(DataPlatform.Position);
            strSensor = sprintf('Sensor%03d', sensorID);
            if isfield(DataPlatform.Position, strSensor)
                DataSensor = DataPlatform.Position.(strSensor);
            else
                strMsg = sprintf('Sensor %03d does not exist.\nPlease, check your input.', sensorID);
                pppp   = dbstack;
                strWrn = sprintf('==> %s/%s %s.', mfilename, pppp.name, strMsg);
                fprintf('%s\n', strWrn);
                return                
            end
            
            % Case of some S7K
            isSNOk = isfield(DataSensor, 'system_serial_number') || ...
                        isfield(DataSensor.Vendor_specific, 'system_serial_number');
            % Case of some S7K
            isSOGOk = isfield(DataSensor, 'speed_over_ground');
            % Case of some S7K
            isCOGOk = isfield(DataSensor, 'course_over_ground');
            % Case of some kmall
            isHeadingOk = isfield(DataSensor, 'Heading');
            % Case of some kmall
            isHAEOk = isfield(DataSensor, 'height_above_reference_ellipsoid');
            
            %% Assignation Data for Ssc from Xsf structure.
            if isSNOk
                pppp          = Hdf5Utils.read_value(DataSensor.Vendor_specific.system_serial_number);
                DataPosition.SystemSerialNumber = unique(pppp); %#ok<*AGROW>
                if isfield(DataSensor.Vendor_specific, 'position_sensor_descriptor')
                    pppp  = Hdf5Utils.read_value(DataSensor.Vendor_specific.position_sensor_descriptor);
                    DataPosition.PositionDescriptor = pppp; %#ok<*AGROW>
                end
                pppp  = Hdf5Utils.read_value(DataSensor.Vendor_specific.measure_in_fix_quality);
                DataPosition.MeasureInFixQuality = pppp; %#ok<*AGROW>
            end

            T                 = Hdf5Utils.read_value(DataSensor.time);
            [T, ~]            = sort(T);
            subTime           = (diff(T)> 0);
            subTime(end+1)    = 1;
            DataPosition.Time = cl_time('timeMat', datenum(T));

            if isfield(DataSensor.Vendor_specific, 'position_raw_count')
                DataPosition.PingCounter          = Hdf5Utils.read_value(DataSensor.Vendor_specific.position_raw_count, 'SubIndex', subTime);
                % DataPosition.Dimensions.NbSamples = length(DataPosition.PingCounter);
            end
            DataPosition.Latitude  = Hdf5Utils.read_value(DataSensor.latitude, 'SubIndex', subTime);
            DataPosition.Longitude = Hdf5Utils.read_value(DataSensor.longitude, 'SubIndex', subTime);
            if isSOGOk
                DataPosition.Speed  = Hdf5Utils.read_value(DataSensor.speed_over_ground, 'SubIndex', subTime);
            end
            if isCOGOk
                DataPosition.CourseVessel = Hdf5Utils.read_value(DataSensor.course_over_ground, 'SubIndex', subTime);
            end
            if isHeadingOk
                DataPosition.Heading = Hdf5Utils.read_value(DataSensor.heading, 'SubIndex', subTime);
            end
            DataPosition.GPSTime = datenum(T);
            if isHAEOk
                DataPosition.HeightAboveEll = Hdf5Utils.read_value(DataSensor.height_above_reference_ellipsoid, 'SubIndex', subTime);
            end
            flag = 1;
        end % End of readPosition
    
    
        function [flag, DataInstallParams] = readInstallParams(DataPlatform, varargin)
            % Function : read Depth from xsf file.
            %
            % Example :
            % ---------
            % nomFicXsf = TODO
            % [flag, Hdf5Data] = Hdf5Utils.readGrpData(nomFicXsf, '/');
            %
            % [flag, DataInstallParams] = readInstallParams(Hdf5Data1.Platform, 'Memmapfile', -1);
            % ###################################################################################

            flag              = 0;
            DataInstallParams = [];
            
            if isfield(DataPlatform.Vendor_specific.Att, 'installation')
                % Cas du S7k.
                % strInstallParams = DataPlatform.Vendor_specific.installation;
                fNames = fieldnames(DataPlatform.Vendor_specific.installation);
                typeMB = 'Reson'; 
            
            elseif isfield(DataPlatform.Vendor_specific.Att, 'installation_kmall_raw_data')
                % Cas du KmAll
                strInstallParams  = DataPlatform.Vendor_specific.Att.installation_kmall_raw_data;
                DataInstallParams = 'TODO'; % TODO
                typeMB            = 'Simrad'; 
                return
                
            elseif isfield(DataPlatform.Vendor_specific.Att, 'installation_raw_data_kmall') % TODO GLU : ajout JMA le 03/06/2021
                % Cas du KmAll
                strInstallParams  = DataPlatform.Vendor_specific.Att.installation_raw_data_kmall;
                DataInstallParams = 'TODO'; % TODO
                typeMB            = 'Simrad'; 
                return
                
            elseif isfield(DataPlatform.Vendor_specific.Att, 'rawInstallation')
                % Cas du All.
                strInstallParams    = DataPlatform.Vendor_specific.Att.rawInstallation;
                % S�paration des �l�ments sur la base du sch�ma : "xxx=*"
                pppp        = regexp(strInstallParams, '\w{3,}=[-a-zA-Z0-9. _]*', 'match');
                pppp2       = regexp(pppp, '\w{3,}=', 'split');
                nameField   = regexp(pppp, '\w{3,}=', 'match');
                % Suppression du '=' pour avoir les noms de champs sous forme "xxx"
                nameField   = cellfun(@(x) (regexprep(x, '=', '')), nameField);
                typeMB      = 'Simrad'; 
            end     
            
            if strcmpi(typeMB, 'Reson')
                DataInstallParams.Model = 'TODO'; % TODO
                dataFromSubGroup = DataPlatform.Vendor_specific.installation;
                for iF=1:numel(fNames)
                    if strcmpi(fNames{iF}, 'Dim')
                        DataInstallParams.Dimensions = dataFromSubGroup.(fNames{iF}); 
                    elseif strcmpi(fNames{iF}, 'Att')
                        DataInstallParams.Attributs = dataFromSubGroup.(fNames{iF}); 
                    elseif strcmpi(fNames{iF}, 'Enum')
                        DataInstallParams.Enumerations = dataFromSubGroup.(fNames{iF}); 
                    else
                        DataInstallParams.(fNames{iF}) = Hdf5Utils.read_value(dataFromSubGroup.(fNames{iF}));
                    end
               end
               return
            else
                % S�paration des �l�ments identifi�s sur la base du sch�ma et r�cup�ration de la cha�ne *: "xxx=*"
                for k=1:numel(nameField)
                    Value = pppp2{k}{2};
                    numVal = str2double(Value);
                    if isnan(numVal)
                        DataInstallParams.(nameField{k}) = Value;
                    else
                        DataInstallParams.(nameField{k}) = numVal;
                    end
                end
            end
                
            DataInstallParams.EmModel = DataPlatform.Vendor_specific.Att.kongsbergModelNumber;
            
            % Cf Fct. 
            % isDual = defineIsDual(DataInstallParams) de cl_simrad_all/read_installationParameters_bin;
            % D�termination de isDual
            switch DataInstallParams.EmModel
                case {300; 710; 712; 302; 122; 1002; 120; 2000; 304}
                    isDual = 0;
                    
                case 2040
                    if isfield(DataInstallParams, 'Line')
                        if ~isfield(DataInstallParams.Line, 'S3R') || isempty(DataInstallParams.Line.S3R)
                            isDual = 0;
                        else
                            isDual = 1;
                        end
                    else
                        if ~isfield(DataInstallParams, 'S3R') || isempty(DataInstallParams.S3R)
                            isDual = 0;
                        else
                            isDual = 1;
                        end
                    end
                    
                case 2045 % 2040c
                    % Test pour conna�tre l'existence en mode Single ou Dual en se
                    % basant sur l'existence de R2S (on pourrait �ventuellement se baser sur SystemSerialNumber et
                    % SystemSerialNumberSecondHead).
                    if isfield(DataInstallParams, 'R2S') && ~isempty(DataInstallParams.R2S) && (DataInstallParams.R2S ~= 0)
                        if DataInstallParams.R2S ~= DataInstallParams.R1S
                            isDual = 1;
                        else
                            isDual = 0; % Etrange cas pas encore rencontr�.
                        end
                    else
                        isDual = 0;
                    end
                    
                case 3002
                    if isfield(DataInstallParams, 'S2R') && ~isempty(DataInstallParams.S2R) && (DataInstallParams.S2R ~= 0)
                        isDual = 1;
                    else
                        isDual = 0;
                    end
                case 3020
                    if isfield(DataInstallParams, 'S2R') && ~isempty(DataInstallParams.S2R) && (DataInstallParams.S2R ~= 0)
                        isDual = 1;
                    else
                        isDual = 0;
                    end
                    
                case 850 % ME70
                    isDual = 0;
                    
                otherwise
                    if ~isdeployed
                        str = sprintf('read_installationParameters_bin : Model=%d : completer le switch', DataInstallParams.EmModel);
                        my_warndlg(str, 1);
                    end
                    if isfield(DataInstallParams, 'S2R') && ~isempty(DataInstallParams.S2R) && (DataInstallParams.S2R ~= 0)
                        isDual = 1;
                    else
                        isDual = 0;
                    end
            end
            DataInstallParams.isDual = isDual;

            flag = 1;
        end % reaDInstallParams
        
        
        function [flag, DataDepth] = readDepth(DataBeam_group1, varargin)
            % Function : read Depth from xsf file.
            %
            % Example :
            % ---------
            % nomFicXsf = TODO
            % [flag, Hdf5Data] = Hdf5Utils.readGrpData(nomFicXsf, '/');
            %
            % [flag, DataDepth] = Hdf5Utils.readDepth(Hdf5Data1.Sonar.Beam_group1.Bathymetry, 'Memmapfile', -1);
            % ###################################################################################
            
            flag      = 0; %#ok<NASGU>
            DataDepth = [];

            %% Assignation Data for Ssc from Xsf structure.
            fNames          = fieldnames(DataBeam_group1);
            DataDepth.Dimensions = [];
            if isfield(DataBeam_group1, 'Dim')
                idx         = strcmpi(fNames, 'Dim');
                for iD=1:numel(DataBeam_group1.Dim)
                    nameDim = DataBeam_group1.Dim(iD).name;
                    DataDepth.Dimensions.(nameDim) = DataBeam_group1.Dim(iD).value;
                end
                fNames(idx) = []; % Just processed
            end
            DataDepth.Attributs = [];
            if isfield(DataBeam_group1, 'Att')
                idx         = strcmpi(fNames, 'Att');
                fNamesAtt   = fieldnames(DataBeam_group1.Att);
                for iA=1:numel(fNamesAtt)
                    DataDepth.Attributs.(fNamesAtt{iA}) = DataBeam_group1.Att.(fNamesAtt{iA});
                end
                fNames(idx) = []; % Just processed
            end
            DataDepth.Enumerations = [];
            if isfield(DataBeam_group1, 'Enum')
                idx         = strcmpi(fNames, 'Enum');
                fNamesEnum  = fieldnames(DataBeam_group1.Enum);
                for iE=1:numel(fNamesEnum)
                    DataDepth.Enumerations.(fNamesEnum{iE}) = DataBeam_group1.Enum.(fNamesEnum{iE});
                end
                fNames(idx) = []; % Just processed
            end      
            
            for iF=1:numel(fNames)
                if strcmpi(fNames{iF}, 'Att')
                   fNamesAtt      = fieldnames(DataBeam_group1.(fNames{iF}));
                   for iFA=1:numel(fNamesAtt)
                        DataDepth.Attributs.(fNamesAtt{iFA}) = DataBeam_group1.(fNames{iF}).(fNamesAtt{iFA});
                   end
                elseif strcmpi(fNames{iF}, 'Enum')
                    % DataDepth.Enumerations = DataBeam_group1.(fNames{iF}); 
                    fNamesEnum     = fieldnames(DataBeam_group1.(fNames{iF}));
                    for iE=1:numel(fNamesEnum)
                        DataDepth.Enumerations.(fNamesEnum{iE}) = DataBeam_group1.(fNames{iF}).(fNamesEnum{iE});
                    end
                end
            end
            
            % Exploration of bathymetry sub-group.
            DataBathy = DataBeam_group1.Bathymetry;
            fNames    = fieldnames(DataBathy);
            for iF=1:numel(fNames)
                if strcmpi(fNames{iF}, 'Dim')
                    % Process different for Dimensions with has (name,
                    % value, id) subfields
                    for iD=1:numel(DataBathy.Dim)
                        nameDim = DataBathy.Dim(iD).name;
                        DataDepth.Dimensions.(nameDim) = DataBathy.Dim(iD).value;
                    end
                elseif strcmpi(fNames{iF}, 'Att')
                    fNamesAtt      = fieldnames(DataBathy.(fNames{iF}));
                    for iFA=1:numel(fNamesAtt)
                        DataDepth.Attributs.(fNamesAtt{iFA}) = DataBathy.(fNames{iF}).(fNamesAtt{iFA});
                    end

                elseif strcmpi(fNames{iF}, 'Enum')
                    fNamesEnum     = fieldnames(DataBathy.(fNames{iF}));
                    for iE=1:numel(fNamesEnum)
                        DataDepth.Enumerations.(fNamesEnum{iE}) = DataBathy.(fNames{iF}).(fNamesEnum{iE});
                    end

                    % DataDepth.Enumerations = DataBathy.(fNames{iF}); 
                else
                    if strcmpi(fNames{iF}, 'Vendor_specific')
                        fVSNames = fieldnames(DataBathy.Vendor_specific);
                        for iFVS=1:numel(fVSNames)
                            if strcmpi(fVSNames{iFVS}, 'Dim')
                                DataDepth.Vendor_specific.Dimensions = DataBathy.Vendor_specific.(fVSNames{iFVS}); 
                            elseif strcmpi(fVSNames{iFVS}, 'Att')
                                DataDepth.Vendor_specific.Attributs = DataBathy.Vendor_specific.(fVSNames{iFVS}); 
                            elseif strcmpi(fVSNames{iFVS}, 'Enum')
                                DataDepth.Vendor_specific.Enumerations = DataBathy.Vendor_specific.(fVSNames{iFVS}); 
                            else
                                DataDepth.Vendor_specific.(fVSNames{iFVS}) = Hdf5Utils.read_value(DataBathy.Vendor_specific.(fVSNames{iFVS}));
                            end
                        end
                    else
                        DataDepth.(fNames{iF}) = Hdf5Utils.read_value(DataBathy.(fNames{iF}));
                    end
                end
            end
            flag = 1;
        end % End of readDepth
    

        function valDateTime = convertFromTimeUnit(val, strTimeUnit)
            % Function unit itme (convention UDUNITS) in dateref and unit
            %
            % Example :
            % ---------
            %   valTime = Hdf5Utils.convertFromTimeUnit(20e9, 'nanoseconds since 2020-01-01 00:00:00Z');
            %   valTime = Hdf5Utils.convertFromTimeUnit(3, 'minutes since 2020-01-01 00:00:00Z');
            %   valTime = Hdf5Utils.convertFromTimeUnit(3, 'years since 2020-01-01 00:00:00Z');
            %
            % ##########################################################
            
            isNanoSeconds   = ~isempty(regexpi(strTimeUnit, '\<nanoseconds\>', 'match'));
            val             = double(val);
            if isNanoSeconds
                val = seconds(val *1e-9);
            end
            isMicroSeconds  = ~isempty(regexpi(strTimeUnit, '\<microseconds\>', 'match'));
            if isMicroSeconds
                val = seconds(val*1e-6);
            end
            isMilliSeconds  = ~isempty(regexpi(strTimeUnit, '\<milliseconds\>', 'match'));
            if isMilliSeconds
                val = seconds(val*1e-3);
            end
            isSeconds       = ~isempty(regexpi(strTimeUnit, '\<seconds\>', 'match'));
            if isSeconds
                val = seconds(val);
            end
            isMinutes       = ~isempty(regexpi(strTimeUnit, '\<minutes\>', 'match'));
            if isMinutes
                val = minutes(val);
            end
            isHours         = ~isempty(regexpi(strTimeUnit, '\<hours\>', 'match'));
            if isHours
                val = hours(val);
            end
            isDays          = ~isempty(regexpi(strTimeUnit, '\<days\>', 'match'));
            if isDays
                val = days(val);
            end
            isMonths        = ~isempty(regexpi(strTimeUnit, '\<months\>', 'match'));
            if isMonths
                val = calmonths(val);
            end
            isYear          = ~isempty(regexpi(strTimeUnit, '\<years\>', 'match'));
            if isYear
                val = years(val);
            end
            
            [a, b] =regexp(strTimeUnit, '\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}Z');
            if ~isempty(a) && ~isempty(b)
                strDateRef      = strTimeUnit(a:b);
                dateRef         = datetime(strDateRef(1:end-1), 'InputFormat', 'yyyy-MM-dd HH:mm:ss', 'TimeZone', 'Z');
%                 [y,m,d,H,M,S]   = datevec(strDateRef, 'yyyy-mm-dd HH:MM:SSZ');
%                 dateRef         = datetime(y,m,d,H,M,S);
            end
            
            % Finale conversion
            valDateTime = val + dateRef;
        end % End of convertFromTimeUnit
        
        
        function [DataSensor, nbSensors] = identifySensor(Data)
            % Function : identify sensors of a pacja ge in xsf (Position /
            % Attitude)
            %
            % Example :
            % ---------
            % [DataSensor, nbSensors] = Hdf5Utils.identifySensor(Data.Position);
            % ####################################################
            
            fieldNames = fieldnames(Data);
            a           = regexp(fieldNames, 'Sensor');
            nbSensors   = numel(a);
            for k=1:nbSensors
                DataSensor(k) = Data.(fieldNames{k});
            end
        end % End of identifySensor
        
        
        function isNum = stringIsNumeric(S)
            % Function : detect if string could be converted into numeric value
            % Example :
            % ---------
            % isNum = Hdf5Utils.stringIsNumeric('0123.5');
            % ######################################
            hdlFcnStringIsNum = @(S) ~isnan(str2double(S));
            isNum = hdlFcnStringIsNum(S);
        end % end of stringIsNumeric
        
        
        function units = read_unit(Data, varargin)
            % Function : read unit attribut from dataset if exists
            %
            % Example :
            % ---------
            % unit = Hdf5Utils.read_unit(DataSensor.roll);
            % ######################################           
            
            units = [];
            
            if isfield(Data, 'units')
                units = Data.units;
            elseif isfield(Data, 'unit')
                units = Data.unit;
            end
            
        end
        
        function classIdStr = identifyDataType(type_id)
            class_id = H5T.get_class(type_id);
            switch(class_id)
                case H5ML.get_constant_value('H5T_INTEGER')
                    classIdStr = 'H5T_INTEGER';
                    % fprintf('Integer\n');
                case H5ML.get_constant_value('H5T_FLOAT')
                    classIdStr = 'H5T_FLOAT';
                    % fprintf('Floating point\n');
                case H5ML.get_constant_value('H5T_STRING')
                    classIdStr = 'H5T_STRING';
                    % fprintf('String\n');
                case H5ML.get_constant_value('H5T_BITFIELD')
                    classIdStr = 'H5T_BITFIELD';
                    % fprintf('Bitfield\n');
                case H5ML.get_constant_value('H5T_OPAQUE')
                    classIdStr = 'H5T_OPAQUE';
                    % fprintf('Opaque\n');
                case H5ML.get_constant_value('H5T_COMPOUND')
                    classIdStr = 'H5T_COMPOUND';
                    % fprintf('Compound'\n');
                case H5ML.get_constant_value('H5T_ENUM')
                    classIdStr = 'H5T_ENUM';
                    % fprintf('Enumerated\n');
                case H5ML.get_constant_value('H5T_VLEN')
                    classIdStr = 'H5T_VLEN';
                    % fprintf('Variable length\n');
                case H5ML.get_constant_value('H5T_ARRAY')
                    classIdStr = 'H5T_ARRAY';
                    % fprintf('Array\n');
            end
        end
        
        function val = read_value(Data, varargin)
            % Function : read value from dataset of file with following operations:
            % - applying scale_factor & add_offset
            % - applying quality control
            %
            % Example :
            % ---------
            % val = Hdf5Utils.read_value(DataSensor.roll, 'SubIndex', sub);
            % ######################################
            
            [varargin, subIndex] = getPropertyValue(varargin, 'SubIndex', []); %#ok<ASGLU>
            
            % Function to convert value in correct values with attributs add_offset
            % and scale_factor.
            
            valid_min = -Inf;
            valid_max = Inf;
            
            % Quality control (before appplication of scale_factor & add_offset)
            if isfield(Data, 'valid_range')
                valid_min = min(Data.valid_range(:));
                valid_max = max(Data.valid_range(:));
            else
                if isfield(Data, 'valid_min')
                    valid_min = Data.valid_min;
                end
                if isfield(Data, 'valid_max')
                    valid_max = Data.valid_max;
                end
            end
            
            if isfield(Data, 'FillValue')
                if iscell(Data.FillValue)
                    fprintf('==> Merci de controler la valeur de l''attribut FillValue de la variable : %s\n', ...
                        Data.long_name)
                else
                    if isnumeric(Data.value) % Ajout JMA le 16/11/2023
                        Data.value = double(Data.value); % Pour �viter disfonctionnement si Data.value est un entier 
                        Data.value(Data.value(:) == Data.FillValue) = NaN; % Modif JMA le 18/11/2021
                    elseif isa(Data.value, 'cl_memmapfile')
                        Data.value = double(Data.value(:,:,:,:,:,:,:,:));
                        Data.value(Data.value(:) == Data.FillValue) = NaN; 
                    else
                        my_breakpoint
                        Data.value(Data.value(:) == Data.FillValue) = NaN; % TODO
                    end
                end
            end
            
            if ~isinf(valid_min)
                if iscell(Data.value)
                    for k=1:length(Data.value)
%                         if min(Data.value{k}) < valid_min
%                             my_breakpoint
%                             strWrn   = sprintf('Value for dataset %s inferior to minimal bound.', Data.long_name);
%                             titreWrn = 'HDF5 reading value';
%                             hWrn = warndlg(strWrn, titreWrn); %#ok<NASGU>
%                             return
%                         end
                        Data.value{k}(Data.value{k}(:) < valid_min) = NaN; % Modif JMA le 18/11/2021
                    end
                else
%                     if min(Data.value(:)) < valid_min
%                         my_breakpoint
%                         strWrn      = sprintf('Value for dataset %s inferior to minimal bound.', Data.long_name);
%                         titreWrn    = 'HDF5 reading value';
%                         hWrn = warndlg(strWrn, titreWrn); %#ok<NASGU>
%                         return
%                     end
                    Data.value(Data.value(:) < valid_min) = NaN; % Modif JMA le 18/11/2021
                end
            end
            if ~isinf(valid_max)
                if iscell(Data.value)
                    for k=1:length(Data.value)
%                         if max(Data.value{k}) > valid_max
%                             my_breakpoint
%                             strWrn   = sprintf('Value for dataset %s inferior to maximal bound.', Data.long_name);
%                             titreWrn = 'HDF5 reading value';
%                             hWrn = warndlg(strWrn, titreWrn); %#ok<NASGU>
%                             return
%                         end
                        Data.value{k}(Data.value{k}(:) > valid_max) = NaN; % Modif JMA le 18/11/2021
                    end
                else
%                     if max(Data.value(:)) > valid_max
%                         strWrn      = sprintf('Value for dataset %s inferior to maximal bound.', Data.long_name);
%                         titreWrn    = 'HDF5 reading value';
%                         hWrn = warndlg(strWrn, titreWrn); %#ok<NASGU>
%                         return
%                     end   
                    Data.value(Data.value(:) > valid_max) = NaN; % Modif JMA le 18/11/2021
                end
            end
            
            add_offset   = 0;
            scale_factor = 1;
            % Conversion of data into valid space.
            if isfield(Data, 'add_offset')
                add_offset = Data.add_offset;
            end
            if isfield(Data, 'scale_factor')
                scale_factor = Data.scale_factor;
            end
            
            % Apply only if field is numeric.
            if isnumeric(Data.value)
                val = (scale_factor * double(Data.value)) + add_offset;
                if ~isa(Data.value, 'integer')
                    % Cast to conserve original data type.
                    val = cast(val, class(Data.value));
                end
            elseif iscell(Data.value)
                if isa(Data.value{1,1}, 'char')
                    val = Data.value;
                else
                    % val = cellfun(@(x) (scale_factor .* (single(x)) + add_offset), Data.value, 'Un', 0);
                    % Tentative d'acc�l�ration mais le pb vient de la quantit� de donn�es WC
                    if scale_factor == 1
                        if add_offset == 0
                            val = cellfun(@(x) single(x), Data.value, 'Un', 0);
                        else
                            val = cellfun(@(x) (single(x) + add_offset), Data.value, 'Un', 0);
                        end
                    else
                        if add_offset == 0
                            val = cellfun(@(x) (scale_factor .* single(x)), Data.value, 'Un', 0);
                        else
                            val = cellfun(@(x) (scale_factor .* (single(x)) + add_offset), Data.value, 'Un', 0);
                        end
                    end
                    % Cast to conserve original data type.
                    % val = cellfun(@(x) cast(x, class(Data.value{1,1})), val, 'un', 0);
                end
            else
                val = Data.value;
            end
            
            % Convert data in explicit values from Enumeratoin
            if isfield(Data, 'Enum')
                % Map from the values to a matlab index.
                idx = zeros(size(val));

                memberName  = Data.Enum.name;
                memberValue = Data.Enum.value;
                for j = 1:numel(memberValue)
                    idx(val==memberValue(j)) = j;
                end
                val = memberName(idx);
            end
            
            % Filtering on subIndex
            if subIndex
                val = val(subIndex);
            end
        end % End of read_value
        
        
        function [Type, TypeMemmapfile] = CanVarBeMemmapfile(Variable, Memmapfile)
            switch Variable.Datatype
                case 'char'
                    Type = 'Strings';
                    TypeMemmapfile = 0;
                case 'cell'
                    Type = 'Strings';
                    TypeMemmapfile = 0;
                case 'datetime'
                    Type = 'datetime';
                    TypeMemmapfile = 0;
                otherwise
                    if (length(Variable.Dimensions) >= 2) && (Variable.Dimensions(2) > 10)
                        Type = 'Images';
                        TypeMemmapfile = abs(Memmapfile);
                    elseif (length(Variable.Dimensions) == 1) && (Variable.Dimensions(1) > 1e6)
                        Type = 'Signals'; % Pour Water Column
                        TypeMemmapfile = abs(Memmapfile);
                        % TODO JMA : cr�er TypeMemmapfile=3 pour demander une copie par morceaux
                    else
                        Type = 'Signals';
                        TypeMemmapfile = (Memmapfile == 1);
                    end
            end
        end % CanVarBeMemmapfile       
    end % End of static methods
end
