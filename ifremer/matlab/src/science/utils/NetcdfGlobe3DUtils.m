classdef NetcdfGlobe3DUtils
    % Collection of static functions to write data into Globe3D Netcdf files.
    %
    % Authors  : JMA Authors
    % See also : netcdf
    %
    % Reference pages in Help browser
    %   <a href="matlab:doc NetcdfGlobe3DUtils" >NetcdfGlobe3DUtils</a>
    %   <a href="matlab:doc netcdf"             >netcdf</a>
    %  -------------------------------------------------------------------------------
    
    methods (Static)
        
        function flag = createGlobeFlyTexture(ncFileName, Data, groupNames, sliceNames, layerUnits, varargin)
            % TODO : Help
            
            [varargin, CLimRaw]           = getPropertyValue(varargin, 'CLimRaw',           []);
            [varargin, CLimComp]          = getPropertyValue(varargin, 'CLimComp',          []);
            [varargin, MaskSignification] = getPropertyValue(varargin, 'MaskSignification', {}); %#ok<ASGLU>
            
            if ischar(layerUnits)
                layerUnits = {layerUnits};
            end
            if ischar(groupNames)
                groupNames = {groupNames};
            end
            if ischar(sliceNames)
                sliceNames = {sliceNames};
            end
            %             nbLayers = length(sliceNames);
            
            % Create empty netcdf file
            ncID = netcdf.create(ncFileName, 'NETCDF4');
            
            % Create global attributes and slice names
            NetcdfGlobe3DUtils.defGlobalAttAndSliceNames(ncID, sliceNames, ...
                'CLimRaw', CLimRaw, 'CLimComp', CLimComp); % Data.CLimRaw, Data.CLimComp
            
            % Define datetime signal
            if isfield(Data, 'Date')
                t = cl_time('timeIfr', Data.Date, Data.Hour);
                Datetime = datetime(t.timeMat, 'ConvertFrom', 'datenum');
                t = posixtime(Datetime);
                t = int64(t*1000);
            else
                t = [];
            end
            
            % Define the group variables
            nbGroups = Data.Dimensions.nbGroups;
            nbSlices = Data.Dimensions.nbSlices;
            grdID           = NaN(nbGroups,1);
            varID_Elevation = NaN(nbGroups,1);
            varID_Latitude  = NaN(nbGroups,1);
            varID_Longitude = NaN(nbGroups,1);
            varID_Layer     = NaN(nbGroups,nbSlices);
            varID_Time      = NaN(nbGroups,1);
            attGlobal = -1; % netcdf.getConstant('GLOBAL');
            for kGroup=1:nbGroups
                grdID(kGroup) = netcdf.defGrp(ncID, sprintf('%03d', kGroup));
                
                % netcdf.putAtt(grdID(kGroup), attGlobal, 'is_valid', 1);
                if isempty(groupNames)
                    netcdf.putAtt(grdID(kGroup), attGlobal, 'long_name', sprintf('Slice %03d', kGroup));
                else
                    netcdf.putAtt(grdID(kGroup), attGlobal, 'long_name', groupNames{kGroup});
                end
                
                if isfield(Data, 'AcrossDistance')
                    netcdf.putAtt(grdID(kGroup), attGlobal, 'across_distance',  sprintf('%sm', num2str(Data.AcrossDistance(kGroup))));
                end
                
                % Define group variable dimensions
                dimIDLength   = netcdf.defDim(grdID(kGroup), 'length',   Data.Dimensions.nbPings);
                dimIDHeight   = netcdf.defDim(grdID(kGroup), 'height',   Data.Dimensions.nbRows);
                dimIDPosition = netcdf.defDim(grdID(kGroup), 'position', Data.Dimensions.nbPings);
                dimIDVector   = netcdf.defDim(grdID(kGroup), 'vector',   2);
                
                % Define group variables
                [varID_Elevation(kGroup), varID_Latitude(kGroup), varID_Longitude(kGroup), varID_Time(kGroup)] = ...
                    NetcdfGlobe3DUtils.defVarPosition(grdID(kGroup), [dimIDPosition dimIDVector], t, dimIDLength);
                
                % Define the layers
                varID_Layer(kGroup,:) = NetcdfGlobe3DUtils.defVarLayers(grdID(kGroup), sliceNames, layerUnits, ...
                    [dimIDLength dimIDHeight], MaskSignification);
            end
            
            % Write group variables
            for kGroup=1:nbGroups
                X = [Data.DepthTop Data.DepthBottom];
                netcdf.putVar(grdID(kGroup), varID_Elevation(kGroup), X);
                
                X = Data.LatitudeTop(kGroup,:);
                netcdf.putVar(grdID(kGroup), varID_Latitude(kGroup),  [X;X]');
                
                X = Data.LongitudeTop(kGroup,:);
                netcdf.putVar(grdID(kGroup), varID_Longitude(kGroup), [X;X]');
                
                if ~isempty(t)
                    netcdf.putVar(grdID(kGroup), varID_Time(kGroup), t);
                end
                
                % Write the layers
                for kSlice=1:nbSlices
                    X = Data.Reflectivity(:,:,kGroup,kSlice);
                    layerName = lower(sliceNames{kSlice});
                    switch layerName
                        case 'mask'
                            netcdf.putVar(grdID(kGroup), varID_Layer(kGroup,kSlice), uint8(X'));
                        otherwise
                            netcdf.putVar(grdID(kGroup), varID_Layer(kGroup,kSlice), single(X'));
                    end
                end
            end
            
            % Close the netcdf file
            netcdf.close(ncID);
            
            flag = 1;
        end
        
        
        function defGlobalAttAndSliceNames(ncID, sliceNames, varargin)
            % TODO : Help
            
            [varargin, CLimRaw]        = getPropertyValue(varargin, 'CLimRaw',        []);
            [varargin, CLimComp]       = getPropertyValue(varargin, 'CLimComp',       []);
            [varargin, SourceFileName] = getPropertyValue(varargin, 'SourceFileName', []); %#ok<ASGLU>
            
            nbLayers = length(sliceNames);
            ind = find(contains(lower(sliceNames), 'raw'));
            if isempty(ind)
                ind = 1;
            else
                ind = ind(1);
            end
            DefaultDisplayedLayer = lower(sliceNames{ind});
            
            % Write global attibutes
            attGlobal = -1; % netcdf.getConstant('GLOBAL');
            netcdf.putAtt(ncID, attGlobal, 'dataset_type',          'FlyTexture');
            netcdf.putAtt(ncID, attGlobal, 'history',               'Created by SonarScope');
            netcdf.putAtt(ncID, attGlobal, 'default_selected_layer', DefaultDisplayedLayer);
            
            if ~isempty(SourceFileName)
                netcdf.putAtt(ncID, attGlobal, 'SourceFileName', SourceFileName);
            end
            
            % Define some dimensions
            dimID    = netcdf.defDim(ncID, 'stringLength',    1024);
            dimID(2) = netcdf.defDim(ncID, 'datalayer_count', nbLayers);
            
            % Write layer names
            S = cell2str(sliceNames(:));
            S = lower(S);
            S(:,(size(S,2)+1):1024) = ' ';
            NC_Storage = NetcdfUtils.getNetcdfClassname('char');
            varID = netcdf.defVar(ncID, 'datalayer_variable_name', NC_Storage, dimID);
            netcdf.putVar(ncID, varID, S');
            
            varID = netcdf.defVar(ncID, 'datalayer_display_name',  NC_Storage, dimID); % Variable optionnelle
            netcdf.putVar(ncID, varID, S');
            
            if isempty(CLimRaw) && ~isempty(CLimComp)
                CLimRaw = CLimComp;
            elseif ~isempty(CLimRaw) && isempty(CLimComp)
                CLimComp = CLimRaw;
            end
            if ~isempty(CLimRaw) && ~isempty(CLimComp)
                S = 'GMT_jet';
                S = lower(S);
                S(:,(size(S,2)+1):1024) = ' ';
                S = repmat(S, nbLayers, 1);
                
                if nbLayers == 3
                    contrast_min = [CLimRaw(1) CLimComp(1) NaN];
                    contrast_max = [CLimRaw(2) CLimComp(2) NaN];
                else
                    contrast_min = [CLimRaw(1) CLimComp(1)];
                    contrast_max = [CLimRaw(2) CLimComp(2)];
                end
                
                varID = netcdf.defVar(ncID, 'datalayer_color_palette',  NC_Storage, dimID); % Variable optionnelle
                netcdf.putVar(ncID, varID, S');
                
                NC_Storage = NetcdfUtils.getNetcdfClassname('single');
                varID = netcdf.defVar(ncID, 'datalayer_contrast_min',  NC_Storage, dimID(2)); % Variable optionnelle
                netcdf.putVar(ncID, varID, contrast_min);
                                
                varID = netcdf.defVar(ncID, 'datalayer_contrast_max',  NC_Storage, dimID(2)); % Variable optionnelle
                netcdf.putVar(ncID, varID, contrast_max);
            else
%                 my_breakpoint
            end
        end
        
        
        function [flag, sliceNames] = readGlobalAttAndSliceNames(ncID, ncFileName)
            % TODO : Help
            
            sliceNames = {};
            
            % Write global attibutes
            attGlobal = -1; % netcdf.getConstant('GLOBAL');
            dataset_type = netcdf.getAtt(ncID, attGlobal, 'dataset_type'); %, 'FlyTexture');
            flag = strcmp(dataset_type, 'FlyTexture');
            if ~flag
                str = sprintf('dataset_type is not "FlyTexture" for %s', ncFileName);
                my_warndlg(str, 1);
                return
            end
            %             netcdf.putAtt(ncID, attGlobal, 'history',      'Created by SonarScope');
            
            varID = netcdf.inqVarID(ncID, 'datalayer_variable_name');
            X = netcdf.getVar(ncID, varID);
            X = X';
            for k=1:size(X,1)
                sliceNames{k,1} = rmblank(X(k,:)); %#ok<AGROW>
            end
        end
        
        
        function [varID_Elevation, varID_Latitude, varID_Longitude, varID_Time] = defVarPosition(grdID, dimID_Slice, t, dimLength)
            % TODO : Help
            
            NC_Storage = NetcdfUtils.getNetcdfClassname('single');
            varID_Elevation = netcdf.defVar(grdID, 'elevation', NC_Storage, dimID_Slice);
            netcdf.defVarDeflate(grdID, varID_Elevation, false, true, 1);
            netcdf.putAtt(grdID, varID_Elevation, 'long_name',     'elevation');
            netcdf.putAtt(grdID, varID_Elevation, 'standard_name', 'elevation');
            netcdf.putAtt(grdID, varID_Elevation, 'units',          'm');
            
            NC_Storage = NetcdfUtils.getNetcdfClassname('double');
            varID_Latitude  = netcdf.defVar(grdID, 'latitude',  NC_Storage, dimID_Slice);
            netcdf.defVarDeflate(grdID, varID_Latitude, false, true, 1);
            netcdf.putAtt(grdID, varID_Latitude, 'long_name',     'latitude');
            netcdf.putAtt(grdID, varID_Latitude, 'standard_name', 'latitude');
            netcdf.putAtt(grdID, varID_Latitude, 'units',         'degrees_north');
            
            varID_Longitude = netcdf.defVar(grdID, 'longitude', NC_Storage, dimID_Slice);
            netcdf.defVarDeflate(grdID, varID_Longitude, false, true, 1);
            netcdf.putAtt(grdID, varID_Longitude, 'long_name',     'longitude');
            netcdf.putAtt(grdID, varID_Longitude, 'standard_name', 'longitude');
            netcdf.putAtt(grdID, varID_Longitude, 'units',         'degrees_east');
            
            if isempty(t)
                varID_Time = NaN;
            else
                NC_StorageTime = NetcdfUtils.getNetcdfClassname('int64');
                varID_Time = netcdf.defVar(grdID, 'time', NC_StorageTime, dimLength);
                netcdf.defVarDeflate(grdID, varID_Time, false, true, 1);
                netcdf.putAtt(grdID, varID_Time, 'long_name',     'time');
                netcdf.putAtt(grdID, varID_Time, 'standard_name', 'time');
                netcdf.putAtt(grdID, varID_Time, 'units', 'milliseconds since 1970-01-01 00:00:00Z');
            end
        end
        
        
        function varID_Layer = defVarLayers(grdID, sliceNames, layerUnits, dimID_Layer, MaskSignification)
            % TODO : Help
            
            nbSlices = length(sliceNames);
            varID_Layer = NaN(1,nbSlices);
            for kSlice=1:nbSlices
                layerName = lower(sliceNames{kSlice});
                switch layerName
                    case 'mask'
                        NC_Storage = NetcdfUtils.getNetcdfClassname('uint8');
                    otherwise
                        NC_Storage = NetcdfUtils.getNetcdfClassname('single');
                end
                varID_Layer(kSlice) = netcdf.defVar(grdID, layerName, NC_Storage, dimID_Layer);
                netcdf.defVarDeflate(grdID, varID_Layer(kSlice), false, true, 1);
                netcdf.putAtt(grdID, varID_Layer(kSlice), 'long_name',     layerName);
                netcdf.putAtt(grdID, varID_Layer(kSlice), 'standard_name', layerName);
                netcdf.putAtt(grdID, varID_Layer(kSlice), 'units',         layerUnits{kSlice});
                
                switch layerName
                    case 'mask'
                        nbBits = length(MaskSignification);
                        netcdf.putAtt(grdID, varID_Layer(kSlice), 'nbBits', int32(nbBits));
                        for kBit=1:nbBits
                            netcdf.putAtt(grdID, varID_Layer(kSlice), ['bit_signification_' num2str(kBit)], MaskSignification{kBit});
                        end
                end
            end
        end
        
        
        function [flag, Data, groupNames, sliceNames, layerUnits, MaskSignification] = readGlobeFlyTexture(ncFileName, varargin)
            % TODO : Help
            
            Data              = [];
            groupNames        = [];
            sliceNames        = [];
            layerUnits        = [];
            MaskSignification = [];
            
            flag = exist(ncFileName, 'file');
            if ~flag
                return
            end
            
            % Open netcdf file
            try
                ncID = netcdf.open(ncFileName);
            catch ME
                ME.getReport
                my_warndlg(ME.message, 1);
                flag = 0;
                return
            end
            
            % Read slices names
            [flag, sliceNames] = NetcdfGlobe3DUtils.readGlobalAttAndSliceNames(ncID, ncFileName);
            if ~flag
                netcdf.close(ncID);
                return
            end
            
            % Read data
            [flag, Data, groupNames, sliceNames, layerUnits, MaskSignification] = NetcdfGlobe3DUtils.readAllGroups(ncID, varargin{:});
            if ~flag
                netcdf.close(ncID);
                return
            end
            
            % Close the netcdf file
            netcdf.close(ncID);
            
            flag = 1;
        end
        
        
        function [flag, Data, groupNames, sliceNames, layerUnits, MaskSignification] = readAllGroups(ncID, varargin)
            % TODO : Help
            
            [varargin, flagLight] = getPropertyValue(varargin, 'Light', 0); %#ok<ASGLU>
            
            flag              = 0;
            groupNames        = {};
            sliceNames        = {};
            layerUnits        = [];
            MaskSignification = {};
            
            childGroups = netcdf.inqGrps(ncID);
            if isempty(childGroups)
                Data = [];
                return
            end
            
            for kGrp=length(childGroups):-1:1
                [flag, DataGrp, grpName, sliceNames, layerUnits, MaskSignification, flagAtt] = ...
                    NetcdfGlobe3DUtils.readOneGroup(ncID, kGrp, 'Light', flagLight);
                if flag
                    groupNames{kGrp} = grpName;
                    
                    if flagAtt.Datetime
                        Data.Datetime(kGrp) = DataGrp.Datetime;
                    end
                    
                    if flagAtt.across_distance
                        Data.across_distance(kGrp) = DataGrp.across_distance;
                    end
                    
                    if flagAtt.latitude_nadir
                        Data.latitude_nadir(kGrp) = DataGrp.latitude_nadir;
                    end
                    
                    if flagAtt.longitude_nadir
                        Data.longitude_nadir(kGrp) = DataGrp.longitude_nadir;
                    end
                    
                    if flagAtt.across_dist_L
                        Data.across_dist_L(kGrp) = DataGrp.across_dist_L;
                    end
                    
                    if flagAtt.across_dist_R
                        Data.across_dist_R(kGrp) = DataGrp.across_dist_R;
                    end
                    
                    for kVar=length(sliceNames):-1:1
                        varName = sliceNames{kVar};
                        if isfield(DataGrp, varName) % Car option Light possiblementt utilis�e dans readOneGroup
                            %                             [ny, nx] = size(DataGrp.(varName));
                            %                             subx = 1:nx; % Rajput� par JMA le 15/10/2020 : MAYOBS15 pour EI donn�es WC-DeprthAcrossDist
                            %                             suby = 1:ny;
                            %                             Data.(varName)(suby,subx,kGrp) = DataGrp.(varName);
                            X = DataGrp.(varName);
                            [n1, n2] = size(X);
                            if isfield(Data, 'varName')
                                [n1Var, n2Var, n3Var] = size(Data.(varName)); %#ok<ASGLU> Ne pas supprimer n3Var sinon Boom !
                                if n1 > n1Var
                                    Data.(varName)(n1Var+1:n1) = NaN;
                                end
                                if n2 > n2Var
                                    Data.(varName)(n2Var+1:n2) = NaN;
                                end
                            end
                            Data.(varName)(1:n1,1:n2,kGrp) = X;
                        end
                    end
                    
                    if flagAtt.Soundings
                        Data.SoundingsDepth(kGrp,:)      = DataGrp.SoundingsDepth;
                        Data.SoundingsAcrossDist(kGrp,:) = DataGrp.SoundingsAcrossDist;
                        Data.SoundingsRange(kGrp,:)      = DataGrp.SoundingsRange;
                    end
                end
            end
        end
        
        
        function [flag, Data, groupName, sliceNames, layerUnits, MaskSignification, flagAtt] = readOneGroup(ncID, kGrp, varargin)
            % TODO : Help
            
            flag = 1;
            
            [varargin, flagSubGroups] = getPropertyValue(varargin, 'flagSubGroups', 0);
            [varargin, flagLight]     = getPropertyValue(varargin, 'Light',         0); %#ok<ASGLU>
            
            layerUnits        = [];
            MaskSignification = {};
            flagAtt.Datetime        = false;
            flagAtt.across_distance = false;
            flagAtt.latitude_nadir  = false;
            flagAtt.longitude_nadir = false;
            flagAtt.across_dist_L   = false;
            flagAtt.across_dist_R   = false;
            flagAtt.Soundings       = false;
            
            attGlobal = -1; % netcdf.getConstant('GLOBAL');
            
            childGroups = netcdf.inqGrps(ncID);
            groupName = netcdf.inqGrpName(childGroups(kGrp));
            grpID = netcdf.inqNcid(ncID, groupName);
            
            try
                str = netcdf.getAtt(grpID, attGlobal, 'time');
                str = strrep(str, 'ConvertFromdatenum', ''); % Temporaire pour bug S7K Sysmaore
                Data.Datetime = str2datetime(str);
                flagAtt.Datetime = true;
            catch
            end
            
            try
                str = netcdf.getAtt(grpID, attGlobal, 'across_distance');
                Data.across_distance = str2double(str(1:end-1));
                flagAtt.across_distance = true;
            catch
            end
            
            try
                str = netcdf.getAtt(grpID, attGlobal, 'latitude_nadir');
                Data.latitude_nadir = str2double(str);
                flagAtt.latitude_nadir = true;
            catch
            end
            
            try
                str = netcdf.getAtt(grpID, attGlobal, 'longitude_nadir');
                Data.longitude_nadir = str2double(str);
                flagAtt.longitude_nadir = true;
            catch
            end
            
            try
                str = netcdf.getAtt(grpID, attGlobal, 'across_dist_L');
                Data.across_dist_L = str2double(str);
                flagAtt.across_dist_L = true;
            catch
            end
            
            try
                str = netcdf.getAtt(grpID, attGlobal, 'across_dist_R');
                Data.across_dist_R = str2double(str);
                flagAtt.across_dist_R = true;
            catch
            end
            
            varids = netcdf.inqVarIDs(grpID);
            for kVar=length(varids):-1:1
                varID = varids(kVar);
                varName = netcdf.inqVar(grpID, varID);
                
                %                 if flagLight == 1
                %                     switch varName
                %                         case {'elevation'; 'latitude'; 'longitude'}
                %                         otherwise
                %                             continue
                %                     end
                %                 end
                
                switch flagLight
                    case 0
                        X = netcdf.getVar(grpID, varID);
                        Data.(varName) = X';
                    case 1
                        switch varName
                            %                             case {'elevation'; 'latitude'; 'longitude'}
                            case {'raw'; 'comp'; 'mask'} % Modif JMA le 15/10/2020 pour EI WC-DepthAcrossDist
                                
                                % ATTENTION : risque majeur d'effet de bord sur d'autres applis
                                
                            otherwise
                                X = netcdf.getVar(grpID, varID);
                                Data.(varName) = X';
                        end
                    case 2
                end
                
                sliceNames{kVar} = varName;
                layerUnits{kVar} = netcdf.getAtt(grpID, varID, 'units');
                if strcmp(varName, 'mask')
                    nbBits = netcdf.getAtt(grpID, varID, 'nbBits');
                    netcdf.getAtt(grpID, varID, 'bit_signification_1');
                    for kbit=1:nbBits
                        MaskSignification{kbit} = netcdf.getAtt(grpID, varID, ['bit_signification_' num2str(kbit)]);
                    end
                end
            end
            
            %% Test si les sondes sont renseign�es
            
            if flagSubGroups
                flag = NetcdfUtils.existGrp([], 'additional_data', 'ncID', grpID, 'Close', false);
                if flag
                    childSoundingsGroups = netcdf.inqGrps(grpID);
                    groupSoundingsName = netcdf.inqGrpName(childSoundingsGroups);
                    grpSoundingsID = netcdf.inqNcid(grpID, groupSoundingsName);
                    
                    varSoundingsIds = netcdf.inqVarIDs(grpSoundingsID);
                    for kSoundingsVar=length(varSoundingsIds):-1:1
                        varSoundingsID = varSoundingsIds(kSoundingsVar);
                        varSoundingsName = netcdf.inqVar(grpSoundingsID, varSoundingsID);
                        
                        X = netcdf.getVar(grpSoundingsID, varSoundingsID);
                        Data.(['Soundings' varSoundingsName]) = X';
                    end
                    flagAtt.Soundings = true;
                end
            end
        end
        
        
        function WC_writeImageNetcdfPing(ncID, DataPingMat)
            if isempty(DataPingMat)
                return
            end
            
            kGroup          = DataPingMat.kGroup;
            TPing           = DataPingMat.TPing;
            Latitude        = DataPingMat.Latitude;
            Longitude       = DataPingMat.Longitude;
            TransducerDepth = DataPingMat.TransducerDepth;
            Coord           = DataPingMat.Coord;
            Mask            = DataPingMat.Mask;
            sliceNames      = DataPingMat.sliceNames;
            LayerRawImage   = DataPingMat.LayerRawImage;
            LayerCompImage  = DataPingMat.LayerCompImage;
            
            [nbRows, nbColumns] = size(LayerRawImage);
            
            grdID = netcdf.defGrp(ncID, sprintf('%03d', kGroup));
            
            attID = -1; % netcdf.getConstant('GLOBAL');
            netcdf.putAtt(grdID, attID, 'long_name', sprintf('Ping %03d', kGroup));
            
            TPing.Format = 'yyyy-MM-dd''T''HH:mm:ss.SSS''Z''';
            netcdf.putAtt(grdID, attID, 'time',            sprintf('%s', char(TPing)));
            netcdf.putAtt(grdID, attID, 'latitude_nadir',  num2strPrecis(Latitude));
            netcdf.putAtt(grdID, attID, 'longitude_nadir', num2strPrecis(Longitude));
            netcdf.putAtt(grdID, attID, 'across_dist_L',   num2str(Coord.xBab));
            netcdf.putAtt(grdID, attID, 'across_dist_R',   num2str(Coord.xTri));
            
            % Define group variable dimensions
            dimIDHeight = netcdf.defDim(grdID, 'height',   nbRows);
            dimIDLength = netcdf.defDim(grdID, 'length',   nbColumns);
            dimIDVector = netcdf.defDim(grdID, 'vector',   2);
            
            AllPositions = false;
            if AllPositions
                dimIDPosition = netcdf.defDim(grdID, 'position', nbColumns); %#ok<UNRCH>
            else
                dimIDPosition = netcdf.defDim(grdID, 'position', 2);
            end
            
            % Define group variables
            [varID_Elevation, varID_Latitude, varID_Longitude] = NetcdfGlobe3DUtils.defVarPosition(grdID, [dimIDPosition dimIDVector], []);
            
            if AllPositions
                X = [zeros(1, nbColumns); repmat(Coord.Depth, 1, nbColumns)]; %#ok<UNRCH>
            else
                X = [0 0;Coord.Depth Coord.Depth];
            end
            X = X + TransducerDepth;
            netcdf.putVar(grdID, varID_Elevation, X');
            
            if AllPositions
                X = linspace(Coord.latBab, Coord.latTri, nbColumns); %#ok<UNRCH>
            else
                X = [Coord.latBab, Coord.latTri];
            end
            netcdf.putVar(grdID, varID_Latitude,  [X;X]');
            
            if AllPositions
                X = linspace(Coord.lonBab, Coord.lonTri, nbColumns); %#ok<UNRCH>
            else
                X = [Coord.lonBab, Coord.lonTri];
            end
            netcdf.putVar(grdID, varID_Longitude, [X;X]');
            
            % dimID_Slice = dimID([2 1]);
            layerUnits = {'dB'; 'dB'; ''};
            MaskSignification = {'After bottom detection'};
            
            varID_Layer = NetcdfGlobe3DUtils.defVarLayers(grdID, sliceNames, layerUnits, ...
                [dimIDLength dimIDHeight], MaskSignification);
            
            kSlice = 1;
            X = LayerRawImage(:,:);
            netcdf.putVar(grdID, varID_Layer(kSlice), single(X'));
            
            kSlice = 2;
            X = LayerCompImage(:,:);
            netcdf.putVar(grdID, varID_Layer(kSlice), single(X'));
            
            kSlice = 3;
            X = Mask; % TODO image compens�e
            netcdf.putVar(grdID, varID_Layer(kSlice), uint8(X'));
            
            if isa(LayerRawImage, 'cl_memmapfile')
            	LayerRawImage  = set(LayerRawImage,  'DoNotDelete2', 0); %#ok<NASGU>
            	LayerCompImage = set(LayerCompImage, 'DoNotDelete2', 0); %#ok<NASGU>
                clear LayerRawImage LayerCompImage
            end
            
            if isfield(DataPingMat, 'SoundingsRangeInMeters')
                grdSoundingsID = netcdf.defGrp(grdID, 'additional_data');
                
                nbSoundings = length(DataPingMat.SoundingsRangeInMeters);
                dimIDSoundingsLength = netcdf.defDim(grdSoundingsID, 'length', nbSoundings);

                NC_Storage = NetcdfUtils.getNetcdfClassname('single');
                
                varIDSoundings = netcdf.defVar(grdSoundingsID, 'Range', NC_Storage, dimIDSoundingsLength);
                netcdf.putVar(grdSoundingsID, varIDSoundings, DataPingMat.SoundingsRangeInMeters);

                varIDSoundings = netcdf.defVar(grdSoundingsID, 'AcrossDist', NC_Storage, dimIDSoundingsLength);
                netcdf.putVar(grdSoundingsID, varIDSoundings,  DataPingMat.SoundingsAcrossDist);

                varIDSoundings = netcdf.defVar(grdSoundingsID, 'Depth', NC_Storage, dimIDSoundingsLength);
                netcdf.putVar(grdSoundingsID, varIDSoundings,  DataPingMat.SoundingsDepth);
            end
        end
    end
end
