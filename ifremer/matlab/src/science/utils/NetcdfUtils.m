classdef NetcdfUtils
    % Collection of static functions to write data into SonarScope Netcdf files.
    %   Some conventions are defined for SonarScope Netcdf files as :
    %     - Statistics for all variables
    %     - Populated attribute to know if the data is filled with real
    %       data or just initialised empty values
    %     - etc
    % Authors  : JMA Authors
    % See also : ALL_Cache2Netcdf, netcdf
    %
    % Reference pages in Help browser
    %   <a href="matlab:doc NetcdfUtils" >NetcdfUtils</a>
    %   <a href="matlab:doc netcdf"      >netcdf</a>
    %  -------------------------------------------------------------------------------
    
    methods (Static)
        
        function Struct2Netcdf(Data, ncFileName, grpNames, varargin)
            
            [varargin, ncIDIn]       = getPropertyValue(varargin, 'ncID',         []);
            [varargin, deflateLevel] = getPropertyValue(varargin, 'deflateLevel', 1); %#ok<ASGLU>
            
            if ~exist(ncFileName, 'file')
                ncID = netcdf.create(ncFileName, 'NETCDF4');
                netcdf.close(ncID);
            end
            
            if ~iscell(grpNames)
                grpNames = {grpNames};
            end
            
            for k=1:length(grpNames)
                if k == 1
                    grpName = grpNames{k};
                    if isempty(ncIDIn)
                        ncID  = netcdf.open(ncFileName, 'WRITE');
                    else
                        ncID = ncIDIn;
                    end
% % Ancien code
%                     if NetcdfUtils.existGrp(ncFileName, grpName, 'ncID', ncID, 'Close', 0) % TODO : am�liorer en donnant 'ncID', ncID !!!!!!!!!!!!!!!!!!
%                         grpID = netcdf.inqNcid(ncID, grpName);
%                     else
%                         grpID = netcdf.defGrp(ncID, grpName);
%                     end
% Test nouveau code
                    try
                        grpID = netcdf.inqNcid(ncID, grpName);
                    catch
                        grpID = netcdf.defGrp(ncID, grpName);
                    end
                else
                    grpName = grpNames{k};
                    %{
                    childGroups = netcdf.inqGrps(grpID);
                    flag = false;
                    for kGroup=1:length(childGroups)
                        groupName = netcdf.inqGrpName(childGroups(kGroup));
                        if strcmp(groupName, grpName)
                            flag = true;
                            break
                        end
                    end
                    
                    if flag
                        grpID = netcdf.inqNcid(grpID, grpName);
                    else
                        grpID = netcdf.defGrp(grpID, grpName);
                    end
                    %}
                    
                    try
                        grpID = netcdf.inqNcid(grpID, grpName);
                    catch
                        grpID = netcdf.defGrp(grpID, grpName);
                    end
                end
            end
            
            dimVal = [];
            if isfield(Data, 'Dimensions')
                dimName = fieldnames(Data.Dimensions);
                for k=1:length(dimName)
                    dimVal(k) = Data.Dimensions.(dimName{k}); %#ok<AGROW>
                    try % Pas top. Il faudrait savoir si la dimension a d�j� �t� cr��e
                        dimID(k) = netcdf.defDim(grpID, dimName{k}, dimVal(k)); %#ok<AGROW>
                    catch
                        dimID(k) = netcdf.inqDimID(grpID, dimName{k}); %#ok<AGROW>
                    end
                end
            end
            
            if ~isempty(dimVal) && any(dimVal == 0)
                if isempty(ncIDIn)
                    netcdf.close(ncID);
                end
                return
            end
            
            fields = fieldnames(Data);
            for k=1:length(fields)
                varName = fields{k};
                
                if strcmp(varName, 'Dimensions')
                    continue
                end
                
                X = Data.(varName);
                
                if isa(X, 'cl_time')
                    X = X.timeMat;
                elseif isa(X, 'datetime')
                    X = datenum(X);
                end
                
                if isempty(X)
                    continue
                end
                
                if isstruct(X)
                    grpNames{end+1} = varName; %#ok<AGROW>
%                     if isempty(ncIDIn)
%                         netcdf.close(ncID);
%                     end
%                     NetcdfUtils.Struct2Netcdf(X, ncFileName, grpNames, 'ncID', ncID);
                    NetcdfUtils.Struct2Netcdf(X, ncFileName, varName, 'ncID', grpID);
%                     if isempty(ncIDIn)
%                         ncID  = netcdf.open(ncFileName, 'WRITE');
%                     end
                    grpNames(end) = [];
                    continue
                end
                
                if islogical(X) % OK
                    attID = -1; % netcdf.getConstant('GLOBAL');
                    netcdf.putAtt(grpID, attID, varName, uint8(X));
                    
                elseif isscalar(X) % OK
                    attID = -1; % netcdf.getConstant('GLOBAL');
                    netcdf.putAtt(grpID, attID, varName, X);
                    
                elseif ischar(X)
                    attID = -1; % netcdf.getConstant('GLOBAL');
                    netcdf.putAtt(grpID, attID, varName, X);
                    
                elseif isvector(X)
                    n = numel(X);
                    if isempty(dimVal)
                        dimID  = NetcdfUtils.generateDimValDimId(grpID, X);
                        kID = 1;
                    else
                        kID = find(dimVal == n);
                        if length(kID) > 1
                            kID = kID(1);
                        end
                    end
                    NC_Storage = NetcdfUtils.getNetcdfClassname(class(X));
                    try
                        varID = netcdf.defVar(grpID, varName, NC_Storage, dimID(kID));
                        netcdf.defVarDeflate(grpID, varID, false, true, deflateLevel);
                    catch
                        varID = netcdf.inqVarID(grpID, varName);
                    end
                    netcdf.putVar(grpID, varID, X);
                    
                elseif ismatrix(X)
                    if isempty(dimVal) % || ~isequal(dimVal, size(X))
                        dimIDHere = NetcdfUtils.generateDimValDimId(grpID, X);
                    else
                        if isequal(dimVal, size(X))
                            dimIDHere = dimID;
                        else
                            [~, ~, ib] = intersect(size(X), dimVal);
                            dimIDHere = dimID(ib);
                        end
                    end
                    
                    % TODO : trouver l'ordre des dimensions
                    %         n = numel(X);
                    %         kID = find(dimVal == n);
                    %         if length(kID) == 1
                    NC_Storage = NetcdfUtils.getNetcdfClassname(class(X));
                    try
                        varID = netcdf.defVar(grpID, varName, NC_Storage, dimIDHere);
                        netcdf.defVarDeflate(grpID, varID, false, true, deflateLevel);
                    catch
                        varID = netcdf.inqVarID(grpID, varName);
                    end
                    netcdf.putVar(grpID, varID, X);
                    %         else
                    %             my_breakpoint
                    %         end
                    
                else
                    my_breakpoint
                end
            end
            
            if isempty(ncIDIn)
                netcdf.close(ncID);
            end
        end
        
        
        function [dimID, dimVal, dimName] = generateDimValDimId(grpID, X)
            dimVal = size(X);
            dimVal(dimVal == 1) = [];
            for k=1:length(dimVal)
                dimName{k} = sprintf('dim_%05d', dimVal(k)); %#ok<AGROW>
                try
                    dimID(k) = netcdf.defDim(grpID, dimName{k}, dimVal(k)); %#ok<AGROW>
                catch
                    dimID(k) = netcdf.inqDimID(grpID, dimName{k}); %#ok<AGROW>
                end
            end
        end
        
        
        function Data = Netcdf2Struct(ncFileName, grpNames, varargin)
            
            [varargin, ncIDIn] = getPropertyValue(varargin, 'ncID', []); %#ok<ASGLU>
            
            Data = [];
            
            if ~exist(ncFileName, 'file')
                return
            end
            
            if ~iscell(grpNames)
                grpNames = {grpNames};
            end
            
            if isempty(ncIDIn)
%                 ncID = netcdf.open(ncFileName, 'WRITE');
                ncID = netcdf.open(ncFileName);
            else
                ncID = ncIDIn;
            end
            for k=1:length(grpNames)
                if k == 1
                    grpName = grpNames{k};
                    try
                        grpID = netcdf.inqNcid(ncID, grpName);
                    catch
                        netcdf.close(ncID)
                        return
                    end
                else
                    try
                        grpID = netcdf.inqNcid(grpID, grpNames{k});
                        grpName = sprintf('%s/%s', grpName, grpNames{k});
                    catch
                        netcdf.close(ncID)
                        return
                    end
                end
            end
            
            try
%                 ginfo = ncinfo(ncFileName, grpName); % Dommage, on ne peut pas utiliser cela car �a bugue si le groupe n'existe pas
                childGrps = netcdf.inqGrps(grpID);
            catch
                return
            end
            
            % if ~isempty(ginfo.Groups)
            if ~isempty(childGrps)
                % for k=1:length(ginfo.Groups)
                
                for k=1:length(childGrps)
                    %  grpNames{end+1} = ginfo.Groups(k).Name; %#ok<AGROW>
                    grpNames{end+1} = netcdf.inqGrpName(childGrps(k)); %#ok<AGROW>
                    % Data.(ginfo.Groups(k).Name) = NetcdfUtils.Netcdf2Struct(ncFileName, grpNames);
%                     netcdf.close(ncID);
                    switch length(grpNames)
                        case 1
                            Data.(grpNames{1}) = NetcdfUtils.Netcdf2Struct(ncFileName, grpNames, 'ncID', ncID);
                        case 2
%                             Data.(grpNames{1}).(grpNames{2}) = NetcdfUtils.Netcdf2Struct(ncFileName, grpNames, 'ncID', ncID);
                            Data.(grpNames{2}) = NetcdfUtils.Netcdf2Struct(ncFileName, grpNames, 'ncID', ncID);
                        case 3
%                             Data.(grpNames{1}).(grpNames{2}).(grpNames{3}) = NetcdfUtils.Netcdf2Struct(ncFileName, grpNames, 'ncID', ncID);
                            Data.(grpNames{3}) = NetcdfUtils.Netcdf2Struct(ncFileName, grpNames, 'ncID', ncID);
                        otherwise
                            pi;%JMA
                    end
%                     ncID  = netcdf.open(ncFileName, 'WRITE');
                    grpNames(end) = [];
                end
            end
            
%             grpID = netcdf.inqNcid(grpID, grpNames{end});
            
            %% Read global attributs
            
%             for kAtt=1:length(ginfo.Attributes)
%                 Data.(ginfo.Attributes(kAtt).Name) = ginfo.Attributes(kAtt).Value;
%             end
            [~, nvars, ngatts] = netcdf.inq(grpID);
            for kAtt=1:ngatts
%                 attname = netcdf.inqAttName(grpID, -1, kAtt-1); % -1 = getConstant('NC_GLOBAL') 
%                 attrvalue = netcdf.getAtt(  grpID, -1, attname); % -1 = getConstant('NC_GLOBAL') 
                attname = netcdf.inqAttName(grpID, -1, kAtt-1);
                attrvalue = netcdf.getAtt(  grpID, -1, attname);
                Data.(attname) = attrvalue;
            end

            %% Read dimensions
            
%             for kDim=1:length(ginfo.Dimensions)
%                 Data.Dimensions.(ginfo.Dimensions(kDim).Name) = ginfo.Dimensions(kDim).Length;
%             end
            dimIDs = netcdf.inqDimIDs(grpID);
            for kDim=1:length(dimIDs)
                [dimname, dimlen] = netcdf.inqDim(grpID, dimIDs(kDim));
                Data.Dimensions.(dimname) = dimlen;
            end
            
            %% Read data
            
%             for kVar=1:length(ginfo.Variables)
            for kVar=1:nvars
%                 varName = ginfo.Variables(kVar).Name;
                varName = netcdf.inqVar(grpID, kVar-1); % Pour info au cas o�: [varName, xtype, dimids, natts] = netcdf.inqVar
                if strcmp(varName, 'Dimensions')
                    continue
                end
                varID = netcdf.inqVarID(grpID, varName);
                X = netcdf.getVar(grpID, varID);
                
                if strcmp(varName, 'Time')
                    X = cl_time('timeMat', X);
                elseif strcmp(varName, 'Datetime')
                    X = datetime(X, 'ConvertFrom', 'datenum');
                end
                
                Data.(varName) = X;
            end
            
            if isfield(Data, 'Dimensions')
                Data = rmfield(Data, 'Dimensions');
            end
            
            if isempty(ncIDIn)
                netcdf.close(ncID);
            end
        end
        
        
        function flag = XMLBin2Netcdf(XML, Data, ncFileName, groupName, varargin)
            % Write an XML structure and its associated data into a SSc Netcdf file
            %
            % Syntax
            %   flag = NetcdfUtils.XMLBin2Netcdf(XML, Data, ncFileName, groupName, ...)
            %
            % Input Arguments
            %   XML        : SSc cache XML structure
            %   Data       : data associated to the XML file
            %   ncFileName : Netcdf file name
            %   groupName  : Name of the Netcdf group (Ex : Runtime)
            %
            % Name-Value Pair Arguments
            %   deflateLevel : Compression level from 0 (no compression) to 9 (highest compression) (Default : 1)
            %
            % Output Arguments
            %   flag : 0=KO, 1=OK
            %
            % Examples
            %   dataFileName = getNomFicDatabase('0011_20110307_211804_IKATERE_02.all');
            %   flag = ALL_Cache2Netcdf(dataFileName)
            %
            % Authors  : JMA
            % See also : Cache2Netcdf, putVarVal
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc NetcdfUtils.XMLBin2Netcdf"        >NetcdfUtils.XMLBin2Netcdf</a>
            %   <a href="matlab:doc SScCacheNetcdfUtils.Cache2Netcdf" >SScCacheNetcdfUtils.Cache2Netcdf</a>
            %   <a href="matlab:doc NetcdfUtils"                      >NetcdfUtils</a>
            %  -------------------------------------------------------------------------------
            
            [varargin, deflateLevel] = getPropertyValue(varargin, 'deflateLevel', 1); %#ok<ASGLU>
            
            for kBug=1:10 % Added to prevent dysfunction if parallel toolbox is used on a powerful computer (high speed and many workers)
                if kBug > 1
                    fprintf('>-------------- XMLBin2Netcdf kBug=%d --------------<\n', kBug);
                end
                try % The code between try and catch works perfectly if we do not use the //Tbx of if we use it on a small computer (laptop with up to 6 workers)
                    
                    %% Create Runtime group
                    
                    ncID  = netcdf.open(ncFileName, 'WRITE');
                    grpID = netcdf.defGrp(ncID, groupName);
                    
                    %% Cr�ation des variables globales
                    
                    fields = fieldnames(XML);
                    for k=1:length(fields)
                        switch fields{k}
                            case {'Dimensions'; 'Signals'; 'Images'; 'Strings'}
                            otherwise
                                AttName = fields{k};
                                AttVal  = XML.(AttName);
                                if isstruct(AttVal)
                                    fieldsStruct = fieldnames(AttVal);
                                    for k2=1:length(fieldsStruct)
                                        AttNameStruct  = sprintf('Struct_%s_%s', AttName, fieldsStruct{k2});
                                        AttValueStruct = AttVal.(fieldsStruct{k2});
                                        attID = -1; % netcdf.getConstant('GLOBAL');
                                        netcdf.putAtt(grpID, attID, AttNameStruct, AttValueStruct);
                                    end
                                else
                                    attID = -1; % netcdf.getConstant('GLOBAL');
                                    if isa(AttVal, 'cl_time')
                                        AttVal = AttVal.timeMat;
                                    elseif isa(AttVal, 'datetime')
                                        AttVal = datenum(AttVal);
                                    end
                                    netcdf.putAtt(grpID, attID, AttName, AttVal);
                                end
                        end
                    end
                    
                    %% Create dimensions
                    
                    if isfield(XML, 'Dimensions')
                        dimName = fieldnames(XML.Dimensions);
                        for k=1:length(dimName)
                            dimVal = XML.Dimensions.(dimName{k});
                            dimID(k) = netcdf.defDim(grpID, dimName{k}, dimVal); %#ok<AGROW>
                        end
                        % else % Cas o� il n'y a pas de Dimensions (de FileHeader des fichiers RDF)
                    end
                    
                    %% Define Signals
                    
                    if isfield(XML, 'Signals')
                        for k1=1:length(XML.Signals)
                            Storage = XML.Signals(k1).Storage;
                            Name    = XML.Signals(k1).Name;
                            switch class(XML.Signals(k1).Dimensions)
                                case 'char'
                                    Dimensions = strsplit(strrep(XML.Signals(k1).Dimensions, ' ', ''), ',');
                                    kDim = [];
                                    for k2=1:length(Dimensions)
                                        if strcmp(Dimensions{k2}, '1')
                                            continue
                                        end
                                        kDim(end+1) = find(strcmp(dimName, Dimensions{k2})); %#ok<AGROW>
                                    end
                                case 'struct'
                                    kDim = [];
                                    for k2=1:length(XML.Signals(k1).Dimensions)
                                        kDim(end+1) = find(strcmp(dimName, XML.Signals(k1).Dimensions(k2).Name)); %#ok<AGROW>
                                    end
                                otherwise
                                    kDim = 1:length(dimID); % Ajout JMA le 01/06/2021 pour kmall
                            end
                            
                            NC_Storage = NetcdfUtils.getNetcdfClassname(Storage);
                            varIDSignals(k1) = netcdf.defVar(grpID, Name, NC_Storage, dimID(kDim)); %#ok<AGROW>
                            netcdf.defVarDeflate(grpID, varIDSignals(k1), false, true, deflateLevel);
                            
                            Unit = XML.Signals(k1).Unit;
                            netcdf.putAtt(grpID, varIDSignals(k1), 'Unit', Unit);
                            netcdf.putAtt(grpID, varIDSignals(k1), 'Populated', 0);
                        end
                    end
                    
                    %% Define Images
                    
                    if isfield(XML, 'Images')
                        for k1=1:length(XML.Images)
                            Storage = XML.Images(k1).Storage;
                            Name    = XML.Images(k1).Name;
                            switch class(XML.Images(k1).Dimensions)
                                case 'char'
                                    Dimensions = strsplit(strrep(XML.Images(k1).Dimensions, ' ', ''), ',');
                                    kDim = [];
                                    for k2=1:length(Dimensions)
                                        if strcmp(Dimensions{k2}, '1')
                                            continue
                                        end
                                        kDim(end+1) = find(strcmp(dimName, Dimensions{k2})); %#ok<AGROW>
                                    end
                                case 'struct'
                                    kDim = [];
                                    for k2=1:length(XML.Images(k1).Dimensions)
                                        kDim(end+1) = find(strcmp(dimName, XML.Images(k1).Dimensions(k2).Name)); %#ok<AGROW>
                                    end
                                otherwise
                                    kDim = 1:length(dimID); % Ajout JMA le 01/06/2021 pour kmall
                            end
                            
                            NC_Storage = NetcdfUtils.getNetcdfClassname(Storage);
                            varIDImages(k1) = netcdf.defVar(grpID, Name, NC_Storage, dimID(kDim)); %#ok<AGROW>
                            netcdf.defVarDeflate(grpID, varIDImages(k1), false, true, deflateLevel);
                            
                            Unit = XML.Images(k1).Unit;
                            netcdf.putAtt(grpID, varIDImages(k1), 'Unit', Unit);
                            netcdf.putAtt(grpID, varIDImages(k1), 'Populated', 0);
                        end
                    end
                    
                    %% Define Strings
                    
                    if isfield(XML, 'Strings')
                        for k1=1:length(XML.Strings)
                            dimVal = NetcdfUtils.getValDimStrings(Data, XML.Strings(k1));
%                             dimID(end+1) = netcdf.defDim(grpID, ['nbCharString_1_' num2str(k1)], dimVal(1)); %#ok<AGROW>
%                             dimID(end+1) = netcdf.defDim(grpID, ['nbCharString_2_' num2str(k1)], dimVal(2)); %#ok<AGROW>
                            
                            % Modif JMA le 07/06/2021 car bug avec les 2 % instruction ci-dessous. Tenter de corriger ce pb
                            dimID(end+1) = netcdf.defDim(grpID, ['nbCharString_x1_' num2str(k1)], dimVal(1)); %#ok<AGROW>
                            dimID(end+1) = netcdf.defDim(grpID, ['nbCharString_x2_' num2str(k1)], dimVal(2)); %#ok<AGROW>
                            
                            %                     if isfield(XML.Strings(k1), 'Storage')
                            %                         Storage = XML.Strings(k1).Storage;
                            %                     else
                            Storage = 'char';
                            %                     end
                            
                            NC_Storage = NetcdfUtils.getNetcdfClassname(Storage);
                            Name = XML.Strings(k1).Name;
                            varIDStrings(k1) = netcdf.defVar(grpID, Name, NC_Storage, dimID([end-1 end])); %#ok<AGROW>  % Comment� le 26/03/2020 pour 'D:\Temp\ExData\EK80\D20190625-T100713.raw'
                            netcdf.putAtt(grpID, varIDStrings(k1), 'Unit',      '');
                            netcdf.putAtt(grpID, varIDStrings(k1), 'Populated', 0);
                            % TODO JMA : Attribut Unit
                        end
                    end
                    netcdf.endDef(grpID);
                    
                    %% Write Signals
                    
                    if isfield(XML, 'Signals')
                        for k1=1:length(XML.Signals)
                            Val = Data.(XML.Signals(k1).Name);
                            Val = Val(:,:,:,:,:,:,:,:);
                            switch class(Val)
                                case 'cl_time'
                                    Val = Val.timeMat;
                                    
                                    % Verrue pour corriger erreur irratrapable dans XMLBinUtils.readSignal
                                    if strcmp(XML.Signals(k1).Dimensions, 'nbPings, nbSounders') ...
                                            && (XML.Dimensions.nbSounders == 2) && (size(Val,2) == 1)
                                        Val = [Val Val]; %#ok<AGROW>
                                    end
                                case 'datetime'
                                    % ----------------------------------------------------------------
                                    % Impose un beakpoint ici pour v�rification
                                    my_breakpoint('FctName', 'XMLBin2Netcdf');
                                    % ----------------------------------------------------------------
                                    Val = datenum(Val);
%                                     Val = Val';
                            end
                            NetcdfUtils.putVarVal(grpID, varIDSignals(k1), Val, 1)
                        end
                    end
                    
                    %% Write Images
                    
                    if isfield(XML, 'Images')
                        for k1=1:length(XML.Images)
                            Val = Data.(XML.Images(k1).Name);
                            switch class(Val)
                                case 'cl_time'
                                    Val = Val.timeMat;
                                case 'datetime'
                                    % ----------------------------------------------------------------
                                    % Impose un beakpoint ici pour v�rification
                                    my_breakpoint('FctName', 'XMLBin2Netcdf');
                                    % ----------------------------------------------------------------
                                    Val = datenum(Val);
                            end
                            
                            if isempty(Val) % V�rue pour les layers pr�d�finis mais remplis plus tard (SlopeAcross, AbsorptionCoefficientUser par ex)
                                Dim = NetcdfUtils.getValDimImages(XML, XML.Images(k1));
                                switch XML.Images(k1).Storage
                                    case {'single'; 'double'}
                                        Val = NaN(Dim, XML.Images(k1).Storage);
                                    otherwise
                                        Val = zeros(Dim, XML.Images(k1).Storage);
                                end
                                Populated = 0;
                            else
                                Populated = 1;
                            end
                            
                            NetcdfUtils.putVarVal(grpID, varIDImages(k1), Val, Populated)
                        end
                    end
                    
                    %% Write Strings
                    
                    if isfield(XML, 'Strings')
                        for k1=1:length(XML.Strings)
                            %       Val = Data.(XML.Strings(k1).Name);
                            %       Val = char(Val(:));
                            [Dim, Values] = NetcdfUtils.getValDimStrings(Data, XML.Strings(k1)); %#ok<ASGLU>
                            Val = char(Values);
                            netcdf.putVar(grpID, varIDStrings(k1), Val(:,:));
                            netcdf.putAtt(grpID, varIDStrings(k1), 'Populated', 1);
                        end
                    end
                    
                    %% Close file
                    
                    netcdf.close(ncID);
                    flag = 1;
                    break
                    
                catch ME % In case a netcdf function gives hand without having terminated the job, the next functions abort. In this case we close the file and try again after a small pause
                    ErrorReport = getReport(ME);
                    try
                        netcdf.close(ncID);
                    catch
                    end
                    ErrorReport
                    my_breakpoint
                    flag = 0;
                    pause(0.2);
                end
            end
        end
        
        
        function putVarVal(grpID, varID, Val, Populated, varargin)
            % Write the value of a variable into a SSc Netcdf file
            %
            % Syntax
            %   flag = NetcdfUtils.putVarVal(grpID, varID, Val, Populated, ...)
            %
            % Input Arguments
            %   grpID     : Group ID
            %   varID     : Var ID
            %   Val       : Values
            %               - The type (double, single, ...) must be
            %                 the same as declared in the defVar
            %               - The dimensions [dim1, dim2, ...] must be
            %                 the same as declared in the defVar
            %   Populated : Use true if the variable is filled with real values,
            %                 false if the data is filled withinitialisation values such as  NaN
            %                 This is useful when a variable will be filled afterwards
            %
            % Name-Value Pair Arguments
            %   Unit : The unit will be used in the statstic computation in order
            %          to avoid bad results as "mean value" of heading per example
            %
            % Examples
            %   dataFileName = getNomFicDatabase('0011_20110307_211804_IKATERE_02.all');
            %   flag = ALL_Cache2Netcdf(dataFileName)
            %
            % Authors  : JMA
            % See also : XMLBin2Netcdf, addValStats
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc NetcdfUtils.putVarVal"     >NetcdfUtils.putVarVal</a>
            %   <a href="matlab:doc NetcdfUtils.XMLBin2Netcdf" >NetcdfUtils.XMLBin2Netcdf</a>
            %   <a href="matlab:doc NetcdfUtils"               >NetcdfUtils</a>
            %  -------------------------------------------------------------------------------
            
            [varargin, Unit] = getPropertyValue(varargin, 'Unit', 'Empty'); %#ok<ASGLU>
            
            Val = Val(:,:,:,:); % Attention, il est imp�ratif de faire (:,:,:, ...) plut�t que (:) car inversion de dimensions si matrice 3D lors de la lecture
            
            if isa(Val, 'logical')
                Val = uint8(Val);
            end

%             sz = size(Val);
%             if length(sz) == 3
%                 for k3=1:sz(3)
%                     k3
%                     netcdf.putVar(grpID, varID, [0 0 k3-1], [sz(1) sz(2) 1], Val(:,:,k3));
%                 end
%             else
              netcdf.putVar(grpID, varID, Val);
%             end
            netcdf.putAtt(grpID, varID, 'Populated', Populated);
            if strcmp(Unit, 'Empty')
                Unit = netcdf.getAtt(grpID, varID, 'Unit');
            end
            netcdf.putAtt(grpID, varID, 'Unit', Unit);
            NetcdfUtils.addValStats(grpID, varID, Val(:), Unit);
        end
        
        
        function addValStats(grpID, varID, Val, Unit)
            % Compute and add the statistics of the variable values as var attributes
            %
            % Syntax
            %   flag = NetcdfUtils.addValStats(grpID, varID, Val, Unit)
            %
            % Input Arguments
            %   grpID : Group ID
            %   varID : Var ID
            %   Val   : Values
            %   Unit  : The unit will be used in the statstic computation in order
            %           to avoid bad results as "mean value" of heading per example
            %
            % Examples
            %   dataFileName = getNomFicDatabase('0011_20110307_211804_IKATERE_02.all');
            %   flag = ALL_Cache2Netcdf(dataFileName)
            %
            % Authors  : JMA
            % See also : putVarVal, putValStats
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc NetcdfUtils.addValStats" >NetcdfUtils.addValStats</a>
            %   <a href="matlab:doc NetcdfUtils.putVarVal"   >NetcdfUtils.putVarVal</a>
            %   <a href="matlab:doc NetcdfUtils"             >NetcdfUtils</a>
            %  -------------------------------------------------------------------------------
            
            valStats = stats(Val, 'Unit', Unit);
            NetcdfUtils.putValStats(grpID, varID, valStats)
        end
        
        
        function putValStats(grpID, varID, valStats)
            % Compute and add the statistics of the variable values as var attributes
            %
            % Syntax
            %   NetcdfUtils.putValStats(grpID, varID, valStats)
            %
            % Input Arguments
            %   grpID    : Group ID
            %   varID    : Var ID
            %   valStats : Structure containing the statistic descriptors
            %
            % More About : The attribues are describes as
            %              "Struct_Stats_Xxx' names in order to rekognize that it is a
            %              strcuture named "Stats" when we will read the file
            %
            % Examples
            %   dataFileName = getNomFicDatabase('0011_20110307_211804_IKATERE_02.all');
            %   flag = ALL_Cache2Netcdf(dataFileName)
            %
            % Authors  : JMA
            % See also : addValStats
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc NetcdfUtils.putValStats" >NetcdfUtils.putValStats</a>
            %   <a href="matlab:doc NetcdfUtils.addValStats" >NetcdfUtils.addValStats</a>
            %   <a href="matlab:doc NetcdfUtils"             >NetcdfUtils</a>
            %  -------------------------------------------------------------------------------
            
            netcdf.putAtt(grpID, varID, 'Struct_Stats_Max',        valStats.Max);
            netcdf.putAtt(grpID, varID, 'Struct_Stats_Min',        valStats.Min);
            netcdf.putAtt(grpID, varID, 'Struct_Stats_Mean',       valStats.Moyenne);
            netcdf.putAtt(grpID, varID, 'Struct_Stats_Median',     valStats.Mediane);
            netcdf.putAtt(grpID, varID, 'Struct_Stats_Std',        valStats.Sigma);
            netcdf.putAtt(grpID, varID, 'Struct_Stats_Quant_00p5', valStats.Quant_25_75(1));
            netcdf.putAtt(grpID, varID, 'Struct_Stats_Quant_99p5', valStats.Quant_25_75(2));
            netcdf.putAtt(grpID, varID, 'Struct_Stats_Quant_01',   valStats.Quant_01_99(1));
            netcdf.putAtt(grpID, varID, 'Struct_Stats_Quant_99',   valStats.Quant_01_99(2));
            netcdf.putAtt(grpID, varID, 'Struct_Stats_Quant_03',   valStats.Quant_03_97(1));
            netcdf.putAtt(grpID, varID, 'Struct_Stats_Quant_97',   valStats.Quant_03_97(2));
            netcdf.putAtt(grpID, varID, 'Struct_Stats_NbData',     valStats.NbData);
            netcdf.putAtt(grpID, varID, 'Struct_Stats_NbNoData',   valStats.nan);
            netcdf.putAtt(grpID, varID, 'Struct_Stats_Skewness',   valStats.Skewness);
            netcdf.putAtt(grpID, varID, 'Struct_Stats_Kurtosis',   valStats.Kurtosis);
            netcdf.putAtt(grpID, varID, 'Struct_Stats_PercentNaN', valStats.PercentNaN);
            netcdf.putAtt(grpID, varID, 'Struct_Stats_Sampling',   valStats.Sampling);
            netcdf.putAtt(grpID, varID, 'Struct_Stats_Entropy',    valStats.Entropy);
            netcdf.putAtt(grpID, varID, 'Struct_Stats_Var',        valStats.Variance);
        end
        
        
        function [flag, ncID] = existGrp(ncFileName, grpNames, varargin)
            % Check if a group exist in the Netcdf file
            %
            % Syntax
            %   flag = NetcdfUtils.existGrp(ncFileName, grpName)
            %
            % Input Arguments
            %   ncFileName : Netcdf file name
            %   grpName    : Name of the Netcdf group
            %
            % Name-Value Pair Arguments
            %   deflateLevel : Compression level from 0 (no compression) to 9 (highest compression) (Default : 1)
            %
            % Output Arguments
            %   flag : 0=KO, 1=OK
            %
            % Examples
            %   dataFileName = getNomFicDatabase('0011_20110307_211804_IKATERE_02.all');
            %   flag = existGrp(dataFileName, 'depth')
            %
            % Authors  : JMA
            % See also : existVar
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc NetcdfUtils.existGrp" >NetcdfUtils.existGrp</a>
            %   <a href="matlab:doc NetcdfUtils"          >NetcdfUtils</a>
            %  -------------------------------------------------------------------------------
            
            [varargin, ncID]  = getPropertyValue(varargin, 'ncID',  []); %#ok<ASGLU>
%             [varargin, Close] = getPropertyValue(varargin, 'Close', true); %#ok<ASGLU>
            
            if isempty(ncID)
                ncID = netcdf.open(ncFileName); %ncFileName);
                Close = true;
            else
                Close = false;
            end
            
            grpId = ncID;
            if iscell(grpNames)
                for k=1:length(grpNames)
                    if k == 1
                        grpName = grpNames{1};
                        [flag, grpId] = NetcdfUtils.existGrpOneLevel(grpId, grpName);
                        if ~flag
                            if Close
                                netcdf.close(ncID);
                            end
                            return
                        end
                    else
                        [flag, grpId] = NetcdfUtils.existGrpOneLevel(grpId, grpNames{k});
                        if ~flag
                            if Close
                                netcdf.close(ncID);
                            end
                            return
                        end
                        grpName = sprintf('%s/%s', grpName, grpNames{k});
                    end
                end
            else
                grpName = grpNames;
                flag = NetcdfUtils.existGrpOneLevel(grpId, grpName);
                if ~flag
                    if Close
                        netcdf.close(ncID);
                    end
                    return
                end
            end
            if Close
                netcdf.close(ncID);
            end
        end
        
        
        function [flag, grpId] = existGrpOneLevel(ncID, grpNames)
            % Check if a group exist in the Netcdf file
            %
            % Syntax
            %   flag = NetcdfUtils.existGrp(ncFileName, grpName)
            %
            % Input Arguments
            %   ncFileName : Netcdf file name
            %   grpName    : Name of the Netcdf group
            %
            % Name-Value Pair Arguments
            %   deflateLevel : Compression level from 0 (no compression) to 9 (highest compression) (Default : 1)
            %
            % Output Arguments
            %   flag : 0=KO, 1=OK
            %
            % Examples
            %   dataFileName = getNomFicDatabase('0011_20110307_211804_IKATERE_02.all');
            %   flag = existGrp(dataFileName, 'depth')
            %
            % Authors  : JMA
            % See also : existVar
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc NetcdfUtils.existGrp" >NetcdfUtils.existGrp</a>
            %   <a href="matlab:doc NetcdfUtils"          >NetcdfUtils</a>
            %  -------------------------------------------------------------------------------

            grpId = [];
            childGroups = netcdf.inqGrps(ncID);
            
            % Check if the group exists in the file
            flag = false;
            for kGroup=1:length(childGroups)
                groupName = netcdf.inqGrpName(childGroups(kGroup));
                if strcmp(groupName, grpNames)
                    flag = true;
                    grpId = childGroups(kGroup);
                    break
                end
            end
        end
        
        
        function flag = existVar(ncFileName, grpName, varName)
            % Check if a variable exist in a group of a Netcdf file
            %
            % Syntax
            %   flag = existVar(ncFileName, grpName, varName)
            %
            % Input Arguments
            %   ncFileName : Netcdf file name
            %   grpName    : Name of the Netcdf group
            %   varName    : Name of the variable
            %
            % Name-Value Pair Arguments
            %   deflateLevel : Compression level from 0 (no compression) to 9 (highest compression) (Default : 1)
            %
            % Output Arguments
            %   flag : 0=KO, 1=OK
            %
            % Examples
            %   dataFileName = getNomFicDatabase('0011_20110307_211804_IKATERE_02.all');
            %   flag = ALL_Cache2Netcdf(dataFileName)
            %
            % Authors  : JMA
            % See also : RDF_ProcessRTK
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc NetcdfUtils.existGrp" >NetcdfUtils.existGrp</a>
            %   <a href="matlab:doc NetcdfUtils"          >NetcdfUtils</a>
            %  -------------------------------------------------------------------------------
            
            ncID = netcdf.open(ncFileName); %ncFileName);
            childGroups = netcdf.inqGrps(ncID);
            
            % Check if the group exists in the file
            flag = false;
            for kGroup=1:length(childGroups)
                groupName = netcdf.inqGrpName(childGroups(kGroup));
                if strcmp(groupName, grpName)
                    childVars = netcdf.inqVarIDs(childGroups(kGroup));
                    for kVar=1:length(childVars)
                        variableName = netcdf.inqVar(childGroups(kGroup), childVars(kVar));
                        if strcmp(variableName, varName)
                            flag = true;
                            netcdf.close(ncID);
                            return
                        end
                    end
                end
            end
            netcdf.close(ncID);
        end
        
        
        function addValStatsfromHisto(grpID, varID, x, histo, Unit) %#ok<INUSD>
            % Compute and add the statistics of the variable values as var attributes
            %
            % Syntax
            %   flag = NetcdfUtils.addValStatsfromHisto(grpID, varID, x, histo, Unit)
            %
            % Input Arguments
            %   grpID : Group ID
            %   varID : Var ID
            %   x     : Bins of histogram (centers of the intervals)
            %   histo : Number of elements per bin
            %   Unit  : The unit will be used in the statstic computation in order
            %           to avoid bad results as "mean value" of heading per example
            %
            % Examples
            %   dataFileName = getNomFicDatabase('0011_20110307_211804_IKATERE_02.all');
            %   flag = ALL_Cache2Netcdf(dataFileName)
            %
            % Authors  : JMA
            % See also : CacheWaterColumn2Netcdf, putValStats
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc NetcdfUtils.addValStatsfromHisto" >NetcdfUtils.addValStatsfromHisto</a>
            %   <a href="matlab:doc NetcdfUtils.putVarVal"            >NetcdfUtils.putVarVal</a>
            %   <a href="matlab:doc NetcdfUtils"                      >NetcdfUtils</a>
            %  -------------------------------------------------------------------------------
            
            if sum(histo) == 0
                valStats.Max            = NaN;
                valStats.Min            = NaN;
                valStats.Moyenne        = NaN;
                valStats.Mediane        = NaN;
                valStats.Sigma          = NaN;
                valStats.Quant_25_75(1) = NaN;
                valStats.Quant_25_75(2) = NaN;
                valStats.Quant_01_99(1) = NaN;
                valStats.Quant_01_99(2) = NaN;
                valStats.Quant_03_97(1) = NaN;
                valStats.Quant_03_97(2) = NaN;
                valStats.NbData         = NaN;
                valStats.nan            = NaN;
                valStats.Skewness       = NaN;
                valStats.Kurtosis       = NaN;
                valStats.PercentNaN     = NaN;
                valStats.Sampling       = NaN;
                valStats.Entropy        = NaN;
                valStats.Variance       = NaN;
            else
                %     figure; bar(x, histo); grid on;
                
                px = histo / sum(histo);
                pxCum = cumsum(px);
                
                m = sum(x .* px);
                v = sum(x.*x .* px);
                
                valStats.Max            = x(find((px ~= 0), 1, 'last'));
                valStats.Min            = x(find((px ~= 0), 1, 'first'));
                valStats.Moyenne        = m;
                valStats.Mediane        = x(find((pxCum >= 0.5), 1, 'first'));
                valStats.Sigma          = sqrt(v);
                valStats.Variance       = v;
                valStats.Quant_25_75(1) = x(find((pxCum >= 0.005), 1, 'first'));
                valStats.Quant_25_75(2) = x(find((pxCum >= 0.995), 1, 'first'));
                valStats.Quant_01_99(1) = x(find((pxCum >= 0.010), 1, 'first'));
                valStats.Quant_01_99(2) = x(find((pxCum >= 0.990), 1, 'first'));
                valStats.Quant_03_97(1) = x(find((pxCum >= 0.030), 1, 'first'));
                valStats.Quant_03_97(2) = x(find((pxCum >= 0.970), 1, 'first'));
                valStats.NbData         = sum(histo);
                valStats.nan            = 0;
                valStats.Skewness       = sum(x.^3 .* px);
                valStats.Kurtosis       = sum(x.^4 .* px);
                valStats.PercentNaN     = 0; % TODO JMA
                valStats.Sampling       = 1;
                valStats.Entropy        = NaN; % TODO JMA
            end
            
            % valStats = stats(Val, 'Unit', Unit); % keep is commented in
            % order to validate Skewness, Kurtosis, Entropy TODO JMA
            
            NetcdfUtils.putValStats(grpID, varID, valStats)
        end
        
        
        function [flag, Data, ncFileName] = FileXMLBin2Netcdf(xmlFileName, grpName, varargin)
            % Convert SSc XML-Bin data into SSc Netcdf file
            %
            % Syntax
            %   flag = NetcdfUtils.FileXMLBin2Netcdf(xmlFileName, ...)
            %
            % Input Arguments
            %   xmlFileName : XML file name
            %   grpName     : group name
            %
            % Name-Value Pair Arguments
            %   deflateLevel : Compression level from 0 (no compression) to 9 (highest compression) (Default : 1)
            %
            % Output Arguments
            %   flag : 0=KO, 1=OK
            %
            % More About : This function is not related to a "SSc cache directory",
            %              it does the conversion of XML-Bin structures
            %              directly such as Water Column echograms, etc ...
            %
            % Examples
            %   xmlFileName = 'D:\Temp\SPFE\ATraiter\L21_ForTEST\WC-3DMatrix\0021_20180306_210305_SimonStevin_WCCubeRaw_01.xml'
            %   flag = FileXMLBin2Netcdf(xmlFileName)
            %
            % Authors  : JMA
            % See also : XMLBin2Netcdf
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc NetcdfUtils.FileXMLBin2Netcdf" >NetcdfUtils.FileXMLBin2Netcdf</a>
            %   <a href="matlab:doc NetcdfUtils.XMLBin2Netcdf"     >NetcdfUtils.XMLBin2Netcdf</a>
            %   <a href="matlab:doc NetcdfUtils"                   >NetcdfUtils</a>
            %  -------------------------------------------------------------------------------
            
            [varargin, deflateLevel] = getPropertyValue(varargin, 'deflateLevel', 1); %#ok<ASGLU>
            
            [flag, XML] = XMLBinUtils.readGrpData(xmlFileName, 'XMLOnly', 1);
            if ~flag
                Data = [];
                return
            end
            [flag, Data] = XMLBinUtils.readGrpData(xmlFileName, 'Memmapfile', 1);
            if ~flag
                return
            end
            
            %% Cr�ation du fichier Netcdf
            
            [filepath, nomFicSeul] = fileparts(xmlFileName);
            ncFileName = fullfile(filepath, [nomFicSeul '.nc']);
            
            ncID = netcdf.create(ncFileName, 'NETCDF4');
            netcdf.close(ncID);
            
            flag = NetcdfUtils.XMLBin2Netcdf(XML, Data, ncFileName, grpName, 'deflateLevel', deflateLevel); % TODO JMA : pr�voir de ne pas donner de nom de groupe
        end
        
        
        function flag = FileXMLBinImages2Netcdf(xmlFileName, grpName)
            % Conversion d'un XML-Bin-Images en un fichier Netcdf unique
            %             [flag, Data] = NetcdfUtils.FileXMLBin2Netcdf(xmlFileName, grpName);
            
            [flag, XML] = XMLBinUtils.readGrpData(xmlFileName, 'XMLOnly', 1);
            if ~flag
                return
            end
            [flag, Data] = XMLBinUtils.readGrpData(xmlFileName, 'Memmapfile', 1);
            if ~flag
                return
            end
            
            if flag
                N = Data.Dimensions.nbSlices;
                for k=N:-1:1
                    [nomDr1, nomFic1] = fileparts(xmlFileName);
                    iDir = floor(k/100);
                    nomDir2 = num2str(iDir, '%03d');
                    nomFicPng = fullfile(nomDr1, nomFic1, nomDir2, sprintf('%05d.tif', k));
                    if exist(nomFicPng, 'file')
                        I = imread(nomFicPng);
                    else
                        nomFicPng = fullfile(nomDr1, nomFic1, nomDir2, sprintf('%05d.png', k));
                        if ~exist(nomFicPng, 'file')
                            continue
                        end
                        I = imread(nomFicPng);
                        I = flipud(I);
                    end
                    Data.Reflectivity(:,:,k) = I;
                end
                matrixDef.Name       = 'Reflectivity';
                matrixDef.Dimensions = 'nbRows, nbPings, nbSlices';
                matrixDef.Storage    = 'single';
                matrixDef.Unit       = 'dB';
                matrixDef.FileName   = '0021_20180306_210305_SimonStevin_WCSliceRaw_00\Reflectivity.bin';
                XML.Images(end+1)  = matrixDef;
                Data.Images(end+1) = matrixDef;
                
                %% Cr�ation du fichier Netcdf
                
                [filepath, nomFicSeul] = fileparts(xmlFileName);
                ncFileName = fullfile(filepath, [nomFicSeul '.nc']);
                
                ncID = netcdf.create(ncFileName, 'NETCDF4');
                netcdf.close(ncID);
                
                flag = NetcdfUtils.XMLBin2Netcdf(XML, Data, ncFileName, grpName); % TODO JMA : pr�voir de ne pas donner de nom de groupe
            end
        end
        
        
        function [flag, Data, DataBin, VarNameInFailure] = readGrpData(ncFileName, grpName, varargin)
            % Reads a SonarScope Netcdf file
            %
            % Syntax
            %   [flag, Data, Bin, VarNameInFailure] = NetcdfUtils.readGrpData(ncFileName, ...)
            %
            % Input Arguments
            %   ncFileName : Name of the Data file
            %   grpName      : Name of the Netcdf group (Ex : Runtime)
            %
            % Name-Value Pair Arguments
            %   XMLOnly    : true=only the XML file information is returned, not the data (Default : false)
            %   BinInData  : 0 : Data contains only the Data structure and Bin contains the data
            %                1 : Data contains both the Data and the Bin structure
            %   Memmapfile : Parameter used to optimize the memory (Default : 0)
            %                 0 : data is returned in memory (Ram)
            %                 1 : data is returned in virtual memory (cl_memmapfile objetcs)
            %                -1 : Signals are returned in memory, Images (Matices) are returned in virtual memory
            %
            % Output Arguments
            %   Data : Data structure of the file
            %   Bin  : Structure containing the data only
            %   VarNameInFailure : Name of the var that failed in case of error
            %
            % Examples
            %   FileNameAll = getNomFicDatabase('EM1002_BELGICA_005053_raw.all')
            %   cl_simrad_all('nomFic', FileNameAll); % Just to be sure the Data-Bin is created
            %   [DirName, FileName] = fileparts(FileNameAll)
            %   ncFileName = fullfile(DirName, 'SonarScope', [FileName, '.nc'])
            %
            %   [flag, Data, Bin] = NetcdfUtils.readGrpData(ncFileName, 'Attitude');
            %   [flag, Data, Bin] = NetcdfUtils.readGrpData(ncFileName, 'Depth', 'Memmapfile', -1);
            %
            % See also SScCacheNetcdfUtils.readWholeData
            % Authors : JMA
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc NetcdfUtils.readGrpData"                       >NetcdfUtils.readGrpData</a>
            %   <a href="matlab:doc NetcdfUtils.SScCacheNetcdfUtils.readWholeData" >NetcdfUtils.SScCacheNetcdfUtils.readWholeData</a>
            %   <a href="matlab:doc NetcdfUtils"                                   >NetcdfUtils</a>
            %  -------------------------------------------------------------------------------
            
            [varargin, XMLOnly]    = getPropertyValue(varargin, 'XMLOnly',    0);
            [varargin, ReadOnly]   = getPropertyValue(varargin, 'ReadOnly',   0);
            [varargin, Memmapfile] = getPropertyValue(varargin, 'Memmapfile', 0);
            [varargin, BinInData]  = getPropertyValue(varargin, 'BinInData',  1); %#ok<ASGLU>
            % [varargin, LogSilence] = getPropertyValue(varargin, 'LogSilence', 1);
            
            Data             = [];
            VarNameInFailure = [];
            DataBin          = [];
            
            flag = exist(ncFileName, 'file');
            if ~flag
                return
            end
            
            %% Open file
            
            for kBug=1:10
                if kBug > 1
                    fprintf('>-------------- readGrpData kBug=%d --------------<\n', kBug);
                end
                try
                    ncID = netcdf.open(ncFileName);
                    
                    %% Read file
                    
                    childGroups = netcdf.inqGrps(ncID);
                    
                    % Check if the group exists in the file
                    flag = 0;
                    for kGroup=1:length(childGroups)
                        groupName = netcdf.inqGrpName(childGroups(kGroup));
                        if strcmp(groupName, grpName)
                            grpID = childGroups(kGroup);
                            flag = 1;
                            break
                        end
                    end
                    if ~flag
                        netcdf.close(ncID);
                        break
                    end
                    
                    ginfo = ncinfo(ncFileName, grpName); % Dommage, on ne peut pas utiliser cela car �a bugue si le groupe n'existe pas
                    
                    %% Read global attributs
                    
                    for kAtt=1:length(ginfo.Attributes)
                        Data.(ginfo.Attributes(kAtt).Name) = ginfo.Attributes(kAtt).Value;
                        if strcmpi(ginfo.Attributes(kAtt).Name, 'datetime')
                            Data.(ginfo.Attributes(kAtt).Name) = datetime(Data.(ginfo.Attributes(kAtt).Name), 'ConvertFrom', 'datenum');
                        end
                    end
                    
                    %% Read dimensions
                    
                    for kDim=1:length(ginfo.Dimensions)
                        Data.Dimensions.(ginfo.Dimensions(kDim).Name) = ginfo.Dimensions(kDim).Length;
                    end
                    
                    %% Read signals and images
                    
                    kVarSignal = 0;
                    kVarImage  = 0;
                    kVarString = 0;
                    for kVar=1:length(ginfo.Variables)
                        [Type, TypeMemmapfile] = NetcdfUtils.classifySignalImageString(ginfo.Variables(kVar), Memmapfile);
                        
                        % TODO BugMatricesCarr�es
                        if TypeMemmapfile && (length(ginfo.Variables(kVar).Size) >= 2) && (ginfo.Variables(kVar).Size(1) == ginfo.Variables(kVar).Size(2))
                            TypeMemmapfile = 0;
                        end
                        
                        switch Type
                            case 'Signals'
                                kVarSignal = kVarSignal + 1;
                                kVarType   = kVarSignal;
                            case 'Images'
                                kVarImage = kVarImage + 1;
                                kVarType  = kVarImage;
                            case 'Strings'
                                kVarString = kVarString + 1;
                                kVarType   = kVarString;
                            otherwise
                                pi
                        end
                        
                        varName = ginfo.Variables(kVar).Name;
                        Data.(Type)(kVarType).Name       = varName;
                        Data.(Type)(kVarType).Dimensions = ginfo.Variables(kVar).Dimensions;
                        Data.(Type)(kVarType).Storage    = ginfo.Variables(kVar).Datatype;
                        
                        varID = netcdf.inqVarID(grpID, varName);
                        
                        Unit      = netcdf.getAtt(grpID, varID, 'Unit');
                        Populated = netcdf.getAtt(grpID, varID, 'Populated');
                        
                        Data.(Type)(kVarType).Unit      = Unit;
                        Data.(Type)(kVarType).Populated = Populated;
                        
                        for kAtt=3:length(ginfo.Variables(kVar).Attributes)
                            Data.(Type)(kVarType).(ginfo.Variables(kVar).Attributes(kAtt).Name) = ginfo.Variables(kVar).Attributes(kAtt).Value;
                        end
                        
                        if ~XMLOnly
                            %                             data = netcdf.getVar(grpID, varID); %  data      446x1437x871
                            
                            if strcmp(Unit, 'days since JC') || strcmp(Unit, 'datetime')
                                data = netcdf.getVar(grpID, varID); %  data      446x1437x871
                                if TypeMemmapfile ~= 2
                                    Data.(varName)    = cl_time('timeMat', data(:,:));
                                    DataBin.(varName) = Data.(varName);
                                    
                                    varNameDatetime = strrep(varName, 'Time', 'Datetime'); % On assume que les noms des temps sont toujours xxxTimeyyyy
                                    Data.(varNameDatetime)    = datetime(data(:,:), 'ConvertFrom', 'datenum');
                                    DataBin.(varNameDatetime) = Data.(varNameDatetime);
                                end
                            else
                                
                                % TODO JMA : g�rer TypeMemmapfile=3 pour demander une copie par morceaux
                                switch TypeMemmapfile
                                    case 1 % Transfert de la donn�e en Memmapfile
                                        % TODO GLU : faire un m�chanisme plus sophistiqu�
                                        % (m�morisation du nom du groupe, du nom de la
                                        % variable, etc et ne cr�er r�ellement le fichier
                                        % memmapfile que si on demande la lecture des valeurs .
                                        % Attention, on fait souvent class(X(1)) dans SSc, il
                                        % faut d�tecter ce cas pour ne pas cr�er de fichiers
                                        % inutilement
                                        
                                        data = netcdf.getVar(grpID, varID);
                                        Data.(varName) = cl_memmapfile('Value', data);
                                        if ReadOnly
                                            Data.(varName) = set(Data.(varName), 'Writable', 0); % Ne r�gle pas le pb de bug quand la //Tbx est utilis�e dans ALL_WC_EI_SampleBeam_SignalVsPings
                                        end
                                    case 2 % Pas de transfert de la donn�e (appel si par exemple on veut lire les signaux mais pas les images :
                                        % OK, c'est pas top, il faudrait plut�t faire une fonction sp�cifique. Appel dans ALL_writePingFlagsWaterColumn
                                        %                                         my_breakpoint
                                    case 3
                                        varSize = [Data.(Type)(kVarType).Dimensions(:).Length];
                                        varType = Data.(Type)(kVarType).Storage;
                                        Data.(varName) = ClNetcdfMemmapfile(ncFileName, grpName, grpID, ...
                                            varName, varID, varSize, varType, Unit, TypeMemmapfile == 3);
                                        DataBin.(varName) = Data.(varName);
                                    otherwise % Transfert int�gral de la donn�e
                                        data = netcdf.getVar(grpID, varID);
                                        Data.(varName)    = data;
                                        DataBin.(varName) = Data.(varName);
                                end
                            end
                        end
                    end
                    flag = 1;
                    
                    %% Close file
                    
                    netcdf.close(ncID);
                    
                    break
                catch ME
                    my_breakpoint
                    ErrorReport = getReport(ME); %#ok<NASGU>
                    try
                        netcdf.close(ncID);
                    catch
                    end
                    pause(0.2)
                    flag = 0;
                end
            end
            if ~flag
                return
            end
            
            %% In case of dates
            
            if isfield(Data, 'Time')
                %     Data.Time = cl_time('timeMat', Data.Time(:,:));
                %     Data.Datetime = datetime(Data.Time.timeMat, 'ConvertFrom', 'datenum');
                %     DataBin.Time     = Data.Time;
                %     DataBin.Datetime = Data.Datetime;
            elseif isfield(Data, 'Date') && isfield(Data, 'Hour')
                Data.Time     = cl_time('timeIfr', Data.Date(:), Data.Hour(:)); % For retro compatibility
                Data.Datetime = datetime(Data.Time.timeMat, 'ConvertFrom', 'datenum');
                DataBin.Time     = Data.Time;
                DataBin.Datetime = Data.Datetime;
            end
            
            %% Create structures
            
            Data = NetcdfUtils.rebuiltStructure(Data);
        end
        
        
        function flag = RestoreOriginalSignal(ncFileName, InfoSignal, grpName)
            % Restore a backup signal
            %
            % Syntax
            %   flag = NetcdfUtils.RestoreOriginalSignal(ncFileName, InfoSignal)
            %
            % Input Arguments
            %   ncFileName : Netcdf file name
            %   InfoSignal : Structure containing the description of the signal
            %
            % Output Arguments
            %   flag : 0=KO, 1=OK
            %
            % Examples
            %   ncFileName =
            %   flag = NetcdfUtils.RestoreOriginalSignal(ncFileName, XML.Signals(1));
            %
            % See also RestoreOriginalSignal readGrpData
            % Authors : JMA
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc NetcdfUtils.BackupOriginalSignal"  >NetcdfUtils.BackupOriginalSignal</a>
            %   <a href="matlab:doc NetcdfUtils.RestoreOriginalSignal" >NetcdfUtils.RestoreOriginalSignal</a>
            %   <a href="matlab:doc NetcdfUtils"                       >NetcdfUtils</a>
            %  -------------------------------------------------------------------------------
            
            if ~exist(ncFileName, 'file')
                flag = 0;
                return
            end
            
            grpName = strrep(grpName, 'Ssc_', '');
            varName = InfoSignal.Name;
            
            if contains(varName, '_Backup')
                flag = 1;
                return
            end
            varNameBackup = [varName '_Backup'];
            
            for kBug=1:10
                if kBug > 1
                    fprintf('>-------------- RestoreOriginalSignal kBug=%d --------------<\n', kBug);
                end
                try
                    ncID  = netcdf.open(ncFileName, 'WRITE');
                    grpID = netcdf.inqNcid(ncID, grpName);
                    
                    varID = netcdf.inqVarID(grpID, varNameBackup);
                    X = netcdf.getVar(grpID, varID);
                    
                    varID2 = netcdf.inqVarID(grpID, varName);
                    NetcdfUtils.putVarVal(grpID, varID2, X, 1)
                    
                    netcdf.close(ncID);
                    
                    flag = 1;
                    break
                catch
                    my_breakpoint
                    try
                        netcdf.close(ncID);
                    catch
                    end
                    pause(0.2)
                    flag = 0;
                end
            end
        end
        
        
        function flag = BackupOriginalSignal(ncFileName, InfoSignal, grpName)
            % Backup of a signal in case we want to modify it and retreive it later
            %
            % Syntax
            %   flag = NetcdfUtils.BackupOriginalSignal(ncFileName, InfoSignal)
            %
            % Input Arguments
            %   ncFileName : Netcdf file name
            %   InfoSignal : Structure containing the description of the signal
            %
            % Output Arguments
            %   flag : 0=KO, 1=OK
            %
            % Examples
            %   ncFileName =
            %   flag = NetcdfUtils.BackupOriginalSignal(ncFileName, XML.Signals(1));
            %
            % See also RestoreOriginalSignal readGrpData
            % Authors : JMA
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc NetcdfUtils.BackupOriginalSignal"  >NetcdfUtils.BackupOriginalSignal</a>
            %   <a href="matlab:doc NetcdfUtils.RestoreOriginalSignal" >NetcdfUtils.RestoreOriginalSignal</a>
            %   <a href="matlab:doc NetcdfUtils"                       >NetcdfUtils</a>
            %  -------------------------------------------------------------------------------
            
            if ~exist(ncFileName, 'file')
                flag = 0;
                return
            end
            
            grpName = strrep(grpName, 'Ssc_', '');
            varName = InfoSignal.Name;
            
            if contains(varName, '_Backup')
                flag = 1;
                return
            end
            
            for kBug=1:10
                if kBug > 1
                    fprintf('>-------------- BackupOriginalSignal kBug=%d --------------<\n', kBug);
                end
                try
                    ncID  = netcdf.open(ncFileName, 'WRITE');
                    grpID = netcdf.inqNcid(ncID, grpName);
                    
                    varID = netcdf.inqVarID(grpID, varName);
                    X = netcdf.getVar(grpID, varID);
                    Unit = netcdf.getAtt(grpID, varID, 'Unit');
                    
                    [~, ~, dimids] = netcdf.inqVar(grpID, varID);
                    
                    varNameBackup = [varName '_Backup'];
                    NC_Storage = NetcdfUtils.getNetcdfClassname(class(X));
                    
                    try
                        varID2 = netcdf.inqVarID(grpID, varNameBackup);
                    catch
                        varID2 = netcdf.defVar(grpID, varNameBackup, NC_Storage, dimids);
                    end
                    
                    NetcdfUtils.putVarVal(grpID, varID2, X, 1, 'Unit', Unit)
                    
                    netcdf.close(ncID);
                    
                    flag = 1;
                    break
                catch
                    my_breakpoint
                    try
                        netcdf.close(ncID);
                    catch
                    end
                    flag = 0;
                    pause(0.2);
                end
            end
        end
        
        
        function [flag, varVal, Unit] = readVar(ncFileName, grpName, varName)
            % Read the values of one particular variable of one group
            %
            % Syntax
            %   [flag, varVal, Unit] = readVar(ncFileName, grpName, varName)
            %
            % Input Arguments
            %   ncFileName : Netcdf file name
            %   grpName    : Group name
            %   varName    : VarName
            %   Unit       : Unit
            %
            % Output Arguments
            %   flag   : 0=KO, 1=OK
            %   varVal : Values
            %
            % Examples
            %   dataFileName = getNomFicDatabase('0011_20110307_211804_IKATERE_02.all');
            %   [pathname, filename] = fileparts(dataFileName);
            %   ncFileName = fullfile(pathname, 'SonarScope', [filename '.nc']);
            %   [flag, varVal, Unit] = NetcdfUtils.readVar(ncFileName, 'Depth', 'ReflectivityFromSnippets');
            %   if flag
            %       SonarScope(varVal)
            %   end
            %
            % See also RestoreOriginalSignal readGrpData
            % Authors : JMA
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc NetcdfUtils.readVar"  >NetcdfUtils.readVar</a>
            %   <a href="matlab:doc NetcdfUtils"                       >NetcdfUtils</a>
            %  -------------------------------------------------------------------------------
            
            flag   = 0;
            varVal = [];
            Unit   = [];
            
            if ~exist(ncFileName, 'file')
                return
            end
            
            for kBug=1:10
                if kBug > 1
                    fprintf('>-------------- BackupOriginalSignal kBug=%d --------------<\n', kBug);
                end
                try
                    ncID  = netcdf.open(ncFileName);
                    grpID = netcdf.inqNcid(ncID, grpName);
                    
                    varID = netcdf.inqVarID(grpID, varName);
                    varVal = netcdf.getVar(grpID, varID);
                    Unit = netcdf.getAtt(grpID, varID, 'Unit');
                    
                    netcdf.close(ncID);
                    
                    flag = 1;
                    break
                catch ME
                     ErrorReport = getReport(ME);
                    try
                        netcdf.close(ncID);
                    catch
                    end
                    ErrorReport
%                     my_breakpoint
                    flag = 0;
                    pause(0.2);
                end
            end
        end
        
        
        function [flag, attVal] = readGlobalAtt(ncFileName, attName)
            % Read the value of one particular global attribut
            %
            % Syntax
            %   [flag, attVal] = readVar(ncFileName, attName)
            %
            % Input Arguments
            %   ncFileName : Netcdf file name
            %   attName    : Global attribut name
            %
            % Output Arguments
            %   flag   : 0=KO, 1=OK
            %   attVal : Values
            %
            % Examples
            %   dataFileName = getNomFicDatabase('0011_20110307_211804_IKATERE_02.all');
            %   [pathname, filename] = fileparts(dataFileName);
            %   ncFileName = fullfile(pathname, 'SonarScope', [filename '.nc']);
            %   [flag, attVal] = NetcdfUtils.readGlobalAtt(ncFileName, 'DatagramVersion');
            %   if flag
            %       SonarScope(varVal)
            %   end
            %
            % See also RestoreOriginalSignal readGrpData
            % Authors : JMA
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc NetcdfUtils.readVar"  >NetcdfUtils.readVar</a>
            %   <a href="matlab:doc NetcdfUtils"                       >NetcdfUtils</a>
            %  -------------------------------------------------------------------------------
            
            flag   = 0;
            attVal = [];
            
            if ~exist(ncFileName, 'file')
                return
            end
            
            for kBug=1:10
                if kBug > 1
                    fprintf('>-------------- readGlobalAtt kBug=%d --------------<\n', kBug);
                end
                try
                    ncID  = netcdf.open(ncFileName, 'NOWRITE');
                    attID = -1; % netcdf.getConstant('GLOBAL');
                    
                    attVal = netcdf.getAtt(ncID, attID, attName);
                    netcdf.close(ncID);
                    
                    flag = 1;
                    break
                catch ME
                    ErrorReport = getReport(ME);
                    try
                        netcdf.close(ncID);
                    catch
                    end
                    ErrorReport
                    my_breakpoint
                    flag = 0;
                    pause(0.2);
                end
            end
        end
        
        
        function NC_Storage = getNetcdfClassname(Storage)
            % Netcdf code for the corresponding Matlab data type
            %
            % Syntax
            %   NC_Storage = getNetcdfClassname(Storage)
            %
            % Input Arguments
            %   Storage : Matlab data type code ('double, 'single', ...)
            %
            % Output Arguments
            %   NC_Storage : Netcdf data type code ('NC_DOUBLE', 'NC_FLOAT', ...)
            %
            % Examples
            %   NC_Storage = NetcdfUtils.getNetcdfClassname('single')
            %
            % See also netcdf.defVar
            % Authors : JMA
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc NetcdfUtils.getNetcdfClassname"  >NetcdfUtils.getNetcdfClassname</a>
            %   <a href="matlab:doc NetcdfUtils"                     >NetcdfUtils</a>
            %  -------------------------------------------------------------------------------
            
            switch Storage
                case 'logical' % Test : vaut mieux �viter les booleen au d�part
                    NC_Storage = 'NC_BYTE';
                    
                case {'int8'; 1}
                    NC_Storage = 'NC_BYTE';
                case {'char'; 2}
                    NC_Storage = 'NC_CHAR';
                case {'int16'; 3}
                    NC_Storage = 'NC_SHORT';
                case {'int32'; 4}
                    NC_Storage = 'NC_INT';
                case {'single'; 5}
                    NC_Storage = 'NC_FLOAT';
                case {'double'; 6}
                    NC_Storage = 'NC_DOUBLE';
                case {'uint8'; 7}
                    NC_Storage = 'NC_UBYTE';
                case {'uint16'; 8}
                    NC_Storage = 'NC_USHORT';
                case {'uint32'; 9}
                    NC_Storage = 'NC_UINT';
                case {'int64'; 10}
                    NC_Storage = 'NC_INT64';
                case {'uint64'; 11}
                    NC_Storage = 'NC_UINT64';
                    
                case 'string'
                    NC_Storage = 'NC_CHAR'; % 'NC_STRING'; pas dans la liste rendue par netcdf.getConstantNames
                otherwise
                    % ----------------------------------------------------------------
                    % Impose un beakpoint ici pour v�rification
                    my_breakpoint('FctName', 'NetcdfUtils.getNetcdfClassname');
                    % ----------------------------------------------------------------
            end
        end
        
        
        function flag = copyVarDef(ncFileName, grpName, varName1, varName2)
            
            if ~NetcdfUtils.existVar(ncFileName, grpName, varName1)
                flag = false;
                return
            end
            if NetcdfUtils.existVar(ncFileName, grpName, varName2)
                flag = true;
                return
            end
            
            ncID = netcdf.open(ncFileName, 'WRITE'); %ncFileName);
            childGroups = netcdf.inqGrps(ncID);
            
            % Check if the group exists in the file
            flag = false;
            for kGroup=1:length(childGroups)
                groupName = netcdf.inqGrpName(childGroups(kGroup));
                if strcmp(groupName, grpName)
                    childVars = netcdf.inqVarIDs(childGroups(kGroup));
                    for kVar=1:length(childVars)
                        variableName = netcdf.inqVar(childGroups(kGroup), childVars(kVar));
                        if strcmp(variableName, varName1)
                            grpID  = childGroups(kGroup);
                            var1ID = childVars(kVar);
                            [~, xtype, dimID] = netcdf.inqVar(grpID, var1ID);
                            NC_Storage = NetcdfUtils.getNetcdfClassname(xtype);
                            flag = true;
                            break
                        end
                    end
                end
            end
            if flag
                var2ID = netcdf.defVar(grpID, varName2, NC_Storage, dimID);
                Unit = netcdf.getAtt(grpID, var1ID, 'Unit');
                netcdf.putAtt(grpID, var2ID, 'Unit', Unit);
                netcdf.putAtt(grpID, var2ID, 'Populated', 0);
            end
            
            netcdf.close(ncID);
        end
        
        
        function flag = createVarDef(ncFileName, grpName, varName, xtype, Unit, varNameThatAsTheSameDimensions)
            
            if ~exist(ncFileName, 'file')
                flag = false;
                return
            end
            
            if ~NetcdfUtils.existVar(ncFileName, grpName, varNameThatAsTheSameDimensions)
                flag = false;
                return
            end
            
            if NetcdfUtils.existVar(ncFileName, grpName, varName)
                flag = true;
                return
            end
            
            ncID = netcdf.open(ncFileName, 'WRITE'); %ncFileName);
            childGroups = netcdf.inqGrps(ncID);
            
            % Check if the group exists in the file
            flag = false;
            for kGroup=1:length(childGroups)
                groupName = netcdf.inqGrpName(childGroups(kGroup));
                if strcmp(groupName, grpName)
                    childVars = netcdf.inqVarIDs(childGroups(kGroup));
                    for kVar=1:length(childVars)
                        variableName = netcdf.inqVar(childGroups(kGroup), childVars(kVar));
                        if strcmp(variableName, varNameThatAsTheSameDimensions)
                            grpID  = childGroups(kGroup);
                            var1ID = childVars(kVar);
                            [~, ~, dimID] = netcdf.inqVar(grpID, var1ID);
                            flag = true;
                            break
                        end
                    end
                end
            end
            if flag
                NC_Storage = NetcdfUtils.getNetcdfClassname(xtype);
                var2ID = netcdf.defVar(grpID, varName, NC_Storage, dimID);
                netcdf.putAtt(grpID, var2ID, 'Unit', Unit);
                netcdf.putAtt(grpID, var2ID, 'Populated', 0);
            end
            
            netcdf.close(ncID);
        end
    end
    
    
    methods (Static, Access = private)
        
        function Data = rebuiltStructure(Data)
            if isempty(Data)
                return
            end
            
            fNames = fieldnames(Data);
            for k=1:length(fNames)
                name = fNames{k};
                if (length(name) >= 7) && strcmp(name(1:7), 'Struct_')
                    str = name(8:end);
                    k2 = strfind(str, '_');
                    nameStruct = str(1:(k2-1));
                    nameVar    = str((k2+1):end);
                    Data.(nameStruct).(nameVar) = Data.(name);
                    Data = rmfield(Data, name);
                end
                if strcmp(name, 'Signals')
                    for k2=length(Data.(name)):-1:1
                        S(k2) = NetcdfUtils.rebuiltStructure(Data.(name)(k2));
                    end
                    Data.(name) = S;
                elseif strcmp(name, 'Images')
                    clear S;
                    for k2=length(Data.(name)):-1:1
                        S(k2) = NetcdfUtils.rebuiltStructure(Data.(name)(k2));
                    end
                    Data.(name) = S;
                end
            end
        end
        
        
        function [Type, TypeMemmapfile] = classifySignalImageString(Variable, Memmapfile)
            switch Variable.Datatype
                case 'char'
                    Type = 'Strings';
                    TypeMemmapfile = 0;
                otherwise
                    if (length(Variable.Dimensions) >= 2) && (Variable.Dimensions(2).Length > 10)
                        Type = 'Images';
                        TypeMemmapfile = abs(Memmapfile);
                    elseif (length(Variable.Dimensions) == 1) && (Variable.Dimensions(1).Length > 1e6)
                        Type = 'Signals'; % Pour Water Column
                        TypeMemmapfile = abs(Memmapfile);
                        % TODO JMA : cr�er TypeMemmapfile=3 pour demander une copie par morceaux
                    else
                        Type = 'Signals';
                        TypeMemmapfile = (Memmapfile == 1);
                    end
            end
        end
        
        
        function Dim = getValDimImages(Data, Var)
            % Data.Dimensions
            switch class(Var.Dimensions)
                case 'char'
                    DimNames = split(Var.Dimensions, ',');
                case 'struct'
                    for k2=1:length(Var.Dimensions)
                        DimNames{k2} = Var.Dimensions(k2).Name; %#ok<AGROW>
                    end
                otherwise
                    for k2=1:length(Var.Dimensions)
                        DimNames{k2} = Var.Dimensions(k2).Name; %#ok<AGROW>
                    end
            end
            for k=1:length(DimNames)
                name = strtrim(DimNames{k});
                if strcmp(name, '1')
                    Dim(k) = 1; %#ok<AGROW>
                else
                    Dim(k) = Data.Dimensions.(name); %#ok<AGROW>
                end
            end
        end
        
        
        function [Dim, Values] = getValDimStrings(Data, DataString)
            Dim = NetcdfUtils.getValDimImages(Data, DataString);
            
            Values = Data.(DataString.Name);
            str = char(Values);
            if size(str,2) < Dim(2)
                str(:,Dim(2)) = ' ';
            elseif size(str,2) > Dim(2)
                % ----------------------------------------------------------------
                % Impose un beakpoint ici pour v�rification
                %                 my_breakpoint('FctName', 'XMLBin2Netcdf');
                % ----------------------------------------------------------------
                %     str = str(:,1:Dim(2)); % TODO JMA : attention danger. Pour l'instant je n'ai pas rencontr� ce pb
                Dim(2) = size(str,2);
            end
            Values = string(str);
        end
    end
end