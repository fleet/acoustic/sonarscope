classdef SScCacheNetcdfUtils
    % Collection of static functions to write data into SonarScope Netcdf files.
    %   Some conventions are defined for SonarScope Netcdf files as :
    %     - Statistics for all variables
    %     - Populated attribute to know if the data is filled with real
    %       data or just initialised empty values
    %     - etc
    % Authors  : JMA Authors
    % See also : ALL_Cache2Netcdf, netcdf
    %
    % Reference pages in Help browser
    %   <a href="matlab:doc SScCacheNetcdfUtils" >SScCacheNetcdfUtils</a>
    %   <a href="matlab:doc NetcdfUtils"         >NetcdfUtils</a>
    %   <a href="matlab:doc netcdf"              >netcdf</a>
    %  -------------------------------------------------------------------------------
    
    methods (Static)
        
        function [flag, ncFileName] = createSScNetcdfFile(fileName)
            % Create an empty netcdf file in the SSc cache directory of a data file
            %
            % Syntax
            %   [flag, ncFileName] = SScCacheNetcdfUtils.createSScNetcdfFile(ncFileName)
            %
            % Input Arguments
            %   fileName : Data file name
            %
            % Output Arguments
            %   flag       : 0=KO, 1=OK
            %   ncFileName : Netcdf file name
            %
            % Examples
            %   dataFileName = getNomFicDatabase('0011_20110307_211804_IKATERE_02.all');
            %   flag = ALL_Cache2Netcdf(dataFileName)
            %
            % Authors  : JMA
            % See also : ALL_Cache2Netcdf, netcdf
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc SScCacheNetcdfUtils.createSScNetcdfFile" >SScCacheNetcdfUtils.createSScNetcdfFile</a>
            %   <a href="matlab:doc SScCacheNetcdfUtils"                     >SScCacheNetcdfUtils</a>
            %  -------------------------------------------------------------------------------
            
            for kBug=1:10
                if kBug > 1
                    fprintf('>-------------- createSScNetcdfFile kBug=%d --------------<\n', kBug);
                end
                try
                    [filepath, name] = fileparts(fileName);
                    ncFileName = fullfile(filepath, 'SonarScope', [name '.NC']);
                    if exist(ncFileName, 'file')
                        drawnow
                    end
                    ncID = netcdf.create(ncFileName, 'NETCDF4');
                    pause(0.5) % A l'essai pour bug sur machine ayant 24 workers
                    netcdf.close(ncID);
                    pause(0.5) % A l'essai pour bug sur machine ayant 24 workers
                    flag = 1;
                    break
                catch ME
                    ME.getReport
                    msg = sprintf('\n%s\n%s\n\n', ME.message, ncFileName);
                    my_warndlg(msg, 0);
                    my_breakpoint
                    try
                        netcdf.close(ncID);
                    catch
                    end
                    pause(0.2)
                    flag = 0;
                end
            end
        end
        
        
        function flag = Cache2Netcdf(ncFileName, dataFileName, package, varargin)
            % Convert SSc XML-Bin cache into SSc Netcdf file
            %
            % Syntax
            %   flag = SScCacheNetcdfUtils.Cache2Netcdf(ncFileName, dataFileName, package, ...)
            %
            % Input Arguments
            %   ncFileName   : Netcdf file name
            %   dataFileName : Data file (Ex : .all)
            %   package      : Name of the Netcdf group (Ex : Runtime)
            %
            % Name-Value Pair Arguments
            %   deflateLevel : Compression level from 0 (no compression) to 9 (highest compression) (Default : 1)
            %
            % Output Arguments
            %   flag : 0=KO, 1=OK
            %
            % Examples
            %   dataFileName = getNomFicDatabase('0011_20110307_211804_IKATERE_02.all');
            %   flag = ALL_Cache2Netcdf(dataFileName)
            %
            % Authors  : JMA
            % See also : ALL_Cache2Netcdf, XMLBin2Netcdf
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc SScCacheNetcdfUtils.Cache2Netcdf" >SScCacheNetcdfUtils.Cache2Netcdf</a>
            %   <a href="matlab:doc SScCacheNetcdfUtils"              >SScCacheNetcdfUtils</a>
            %  -------------------------------------------------------------------------------
            
            [varargin, deflateLevel] = getPropertyValue(varargin, 'deflateLevel', 1); %#ok<ASGLU>
            
%             msg = sprintf(' "%s" - %s', dataFileName, package);
%             logFileId = getLogFileId;
%             logFileId.info('SScCacheNetcdfUtils.Cache2Netcdf', msg);

            [flag, XML] = SScCacheXMLBinUtils.readWholeData(dataFileName, ['Ssc_' package], 'XMLOnly', 1);
            if flag
                [flag, Data] = SScCacheXMLBinUtils.readWholeData(dataFileName, ['Ssc_' package], 'Memmapfile', -1);
                if flag
                    flag = NetcdfUtils.XMLBin2Netcdf(XML, Data, ncFileName, package, 'deflateLevel', deflateLevel);
                    if ~flag
                        return
                    end
                end
            else
                flag = 1; % On ne consid�re pas que ce soit une erreur car le fichier peut ne pas exister
            end
        end
        
        
        function [flag, Data, VarNameInFailure, DataBin] = readWholeData(dataFileName, nomDatagram, varargin)
            % Read the whole data of one group
            %
            % Syntax
            %   [flag, Data, VarNameInFailure, DataBin] = SScCacheNetcdfUtils.readWholeData(dataFileName, nomDatagram, ...)
            %
            % Input Arguments
            %   dataFileName : Netcdf file name
            %   nomDatagram  : SonarScope group name
            %
            % Name-Value Pair Arguments
            %   XMLOnly    :  (Default : 0)
            %   Memmapfile :  (Default : 0)
            %   ReadOnly   :  (Default : 0)
            %   LogSilence :  (Default : 1)
            %
            % Output Arguments
            %   flag             : 0=KO, 1=OK
            %   Data             : 
            %   VarNameInFailure : 
            %   DataBin          : 
            %
            % Examples
            %   dataFileName = getNomFicDatabase('0011_20110307_211804_IKATERE_02.all');
            %   [flag, Data] = SScCacheNetcdfUtils.readWholeData(dataFileName, 'Ssc_Depth'); 
            %
            % See also getSignalUnit
            % Authors : JMA
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc SScCacheNetcdfUtils.readVar" >SScCacheNetcdfUtils.readWholeData</a>
            %   <a href="matlab:doc SScCacheNetcdfUtils"         >SScCacheNetcdfUtils</a>
            %  -------------------------------------------------------------------------------
            
            [varargin, XMLOnly]    = getPropertyValue(varargin, 'XMLOnly',    0);
            [varargin, Memmapfile] = getPropertyValue(varargin, 'Memmapfile', 0);
            [varargin, ReadOnly]   = getPropertyValue(varargin, 'ReadOnly',   0);
            [varargin, LogSilence] = getPropertyValue(varargin, 'LogSilence', 1); %#ok<ASGLU>
            
            % Lecture du r�pertoire SonarScope
            
            [pathname, filename] = fileparts(dataFileName);
            pathname = fullfile(pathname, 'SonarScope', filename);
            
            % Lecture du fichier XML d�crivant la donn�e
            
            ncFileName = [pathname '.nc'];
            nomDatagram = strrep(nomDatagram, 'Ssc_', '');
            nomDatagram = strrep(nomDatagram, 'SSc_', '');
            [flag, Data, DataBin, VarNameInFailure] = NetcdfUtils.readGrpData(ncFileName, nomDatagram, ...
                'XMLOnly', XMLOnly, 'Memmapfile', Memmapfile, 'ReadOnly', ReadOnly, 'LogSilence', LogSilence);
            if ~flag
                return
            end
            
            % Bidouille pour cr�er des dimensions r�elles dans le cas o� on a mis "Dimensions: 'nbPings,2'"
            
            %             Data = SScCacheXMLBinUtils.repareDimensions(Data); % Comment� le 26/03/2020 car bug
        end
        
        
        function [flag, varVal, Unit] = getSignalUnit(dataFileName, nomDatagram, varName, varargin)
            % Read the values of one particular variable of one group
            %
            % Syntax
            %   [flag, varVal, Unit] = SScCacheNetcdfUtils.getSignalUnit(dataFileName, nomDatagram, varName)
            %
            % Input Arguments
            %   dataFileName : Netcdf file name
            %   nomDatagram  : SonarScope group name
            %   varName      : VarName
            %
            % Output Arguments
            %   flag   : 0=KO, 1=OK
            %   varVal : Values
            %   Unit   : Unit
            %
            % Examples
            %   dataFileName = getNomFicDatabase('0011_20110307_211804_IKATERE_02.all');
            %   [flag, varVal, Unit] = SScCacheNetcdfUtils.getSignalUnit(dataFileName, 'Ssc_Depth', 'ReflectivityFromSnippets'); 
            %
            % See also readWholeData
            % Authors : JMA
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc SScCacheNetcdfUtils.readVar" >SScCacheNetcdfUtils.getSignalUnit</a>
            %   <a href="matlab:doc SScCacheNetcdfUtils"         >SScCacheNetcdfUtils</a>
            %  -------------------------------------------------------------------------------

            [varargin, Memmapfile] = getPropertyValue(varargin, 'Memmapfile', 0); %#ok<ASGLU>
            
            [pathname, filename] = fileparts(dataFileName);
            pathname = fullfile(pathname, 'SonarScope', filename);
            ncFileName = [pathname '.nc'];
            
            grpName = strrep(nomDatagram, 'Ssc_', '');
            [flag, varVal, Unit] = NetcdfUtils.readVar(ncFileName, grpName, varName);
            if ~flag
                return
            end
            
            if ~isempty(Unit)
                switch Unit
                    case 'days since JC'
                        varVal = cl_time('timeMat', varVal);
                    case 'Datenum'
                        % Nothing to do � priori
                    case 'Datetime'
                        varVal = datetime(varVam, 'ConvertFrom', 'datenum');
                    otherwise
                        if abs(Memmapfile) == 1
                            varVal = cl_memmapfile('Value', varVal);
                        end
                end
            end
        end
    end
end
