classdef SScCacheXMLBinUtils
    % Collection of static functions to write data into SonarScope cache XMLBin files.
    %     - etc
    % Authors  : JMA Authors
    % See also : ALL_Cache2Netcdf, XMLBinUtils
    %
    % Reference pages in Help browser
    %   <a href="matlab:doc XMLBinUtils" >XMLBinUtils</a>
    %   <a href="matlab:doc SScCacheXMLBinUtils" >SScCacheXMLBinUtils</a>
    %  -------------------------------------------------------------------------------
    
    methods (Static)
        
        function [flag, Y, Unit, XML] = getSignalUnit(dataFileName, grpName, varName, varargin)
            % Reads one signal in a SSc XML-Bin data structure
            %
            % Syntax
            %   [flag, Y, Unit, XML] = SScCacheXMLBinUtils.getSignalUnit(dataFileName, grpName, varName, ...)
            %
            % Input Arguments
            %   dataFileName : Name of the data file
            %   grpName      : Group name
            %   varName      : Var name
            %
            % Name-Value Pair Arguments
            %   Memmapfile : Parameter used to optimize the memory (Default : 0)
            %                 0 : data is returned in memory (Ram)
            %                 1 : data is returned in virtual memory (cl_memmapfile objetcs)
            %                -1 : Signals are returned in memory, Images (Matices) are returned in virtual memory
            %
            % Output Arguments
            %   flag : 0=KO, 1=OK
            %   Y    : Value of the signal
            %   Unit : Unit of the second
            %   XML  : XML structure
            %
            % Examples
            %   [flag, dataFileName] = FtpUtils.getSScDemoFile('tan1505-D20150428-T013331.raw', 'OutputDir', 'D:\Temp\ExData')
            %   a = cl_ExRaw('nomFic', dataFileName);
            %
            %   [flag, t, Unit] = SScCacheXMLBinUtils.getSignalUnit(dataFileName, 'Ssc_Position', 'Time')
            %   [flag, f, Unit] = SScCacheXMLBinUtils.getSignalUnit(dataFileName, 'Ssc_Sample',   'AcousticFrequency')
            %   [flag, R, Unit] = SScCacheXMLBinUtils.getSignalUnit(dataFileName, 'Ssc_Sample',   'Amplitude_70kHz');
            %
            % See also XMLBinUtils.readGrpData
            % Authors : JMA
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc SScCacheXMLBinUtils.getSignalUnit" >SScCacheXMLBinUtils.getSignalUnit</a>
            %   <a href="matlab:doc SScCacheXMLBinUtils"               >SScCacheXMLBinUtils</a>
            %  -------------------------------------------------------------------------------
            
            [varargin, Memmapfile] = getPropertyValue(varargin, 'Memmapfile', 0);
            [varargin, XML]        = getPropertyValue(varargin, 'XML',        []); %#ok<ASGLU>
            
            Y    = [];
            Unit = [];
            
            %% Noms des r�pertoires et du fichier XML
            
            [dataDirName, fileName] = fileparts(dataFileName);
            xmlDirName  = fullfile(dataDirName, 'SonarScope', fileName);
            xmlFileName = fullfile(xmlDirName, [grpName '.xml']);
            if ~exist(xmlFileName, 'file') % C'est pas tom !
                xmlFileName = fullfile(xmlDirName, ['SSc_' grpName '.xml']);
            end
            
            %% Lecture du fichier XML
            
            if isempty(XML)
                [flag, XML] = XMLBinUtils.readGrpData(xmlFileName, 'XMLOnly', 1);
                if ~flag
                    return
                end
            end
            
            %% Recherche de l'index du signal et lecture
            
            if isfield(XML, 'Signals')
                k = findIndVariable(XML.Signals, varName);
                if isempty(k)
                    if isfield(XML, 'Images')
                        k = findIndVariable(XML.Images, varName);
                        if isempty(k)
                            return
                        else
                            [flag, Y] = XMLBinUtils.readSignal(xmlDirName, XML.Images(k), XML.Dimensions, 'Memmapfile', Memmapfile, 'LogSilence', 0);
                            if ~flag
                                return
                            end
                            Unit = XML.Images(k).Unit;
                        end
                    end
                else
                    [flag, Y] = XMLBinUtils.readSignal(xmlDirName, XML.Signals(k), XML.Dimensions, 'Memmapfile', Memmapfile, 'LogSilence', 0);
                    if ~flag
                        return
                    end
                    Unit = XML.Signals(k).Unit;
                end
            end
        end
        
        
        function [flag, Signal, XML] = getTimeSignalUnit(dataFileName, grpName, varName, varargin)
            % Reads one signal in a SSc XML-Bin data structure
            %
            % Syntax
            %   [flag, Signal, XML] = SScCacheXMLBinUtils.getSignalUnit(dataFileName, grpName, varName, ...)
            %
            % Input Arguments
            %   dataFileName : Name of the data file
            %   grpName      : Group name
            %   varName      : Var name
            %
            % Name-Value Pair Arguments
            %   XML        : XML structure in case it has already been read
            %   Memmapfile : Parameter used to optimize the memory (Default : 0)
            %                 0 : data is returned in memory (Ram)
            %                 1 : data is returned in virtual memory (cl_memmapfile objetcs)
            %                -1 : Signals are returned in memory, Images (Matices) are returned in virtual memory
            %
            % Output Arguments
            %   flag   : 0=KO, 1=OK
            %   Signal : Structure whos fields are Time, Value and Unit
            %   XML    : XML structure
            %
            % Examples
            %   [flag, dataFileName] = FtpUtils.getSScDemoFile('tan1505-D20150428-T013331.raw', 'OutputDir', 'D:\Temp\ExData')
            %   a = cl_ExRaw('nomFic', dataFileName);
            %
            %   [flag, Signal] = SScCacheXMLBinUtils.getTimeSignalUnit(dataFileName, 'Ssc_Position', 'Latitude')
            %
            % See also XMLBinUtils.readGrpData
            % Authors : JMA
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc SScCacheXMLBinUtils.getTimeSignalUnit" >SScCacheXMLBinUtils.getTimeSignalUnit</a>
            %   <a href="matlab:doc SScCacheXMLBinUtils"                   >SScCacheXMLBinUtils</a>
            %  -------------------------------------------------------------------------------
            
            [varargin, XML]        = getPropertyValue(varargin, 'XML',        []);
            [varargin, Memmapfile] = getPropertyValue(varargin, 'Memmapfile', 0); %#ok<ASGLU>
            
            %% Noms des r�pertoires et du fichier XML
            
            [dataDirName, fileName] = fileparts(dataFileName);
            xmlDirName  = fullfile(dataDirName, 'SonarScope', fileName);
            xmlFileName = fullfile(xmlDirName, [grpName '.xml']);
            
            %% Lecture du fichier XML
            
            if isempty(XML)
                [flag, XML] = XMLBinUtils.readGrpData(xmlFileName, 'XMLOnly', 1);
                if ~flag
                    Signal = [];
                    return
                end
            end
            
            %% Recherche de Time
            
            k = findIndVariable(XML.Signals, 'Time');
            if isempty(k)
                flag = 0;
                Signal = [];
                return
            end
            
            [flag, S] = XMLBinUtils.readSignal(xmlDirName, XML.Signals(k), XML.Dimensions, 'Memmapfile', Memmapfile);
            if ~flag
                return
            end
            Signal.Time = S;
            
            %% Recherche de Datetime
            
            k = findIndVariable(XML.Signals, 'Datetime');
            if isempty(k)
                S = datetime(S.timeMat, 'ConvertFrom', 'datenum');
            else
                [flag, S] = XMLBinUtils.readSignal(xmlDirName, XML.Signals(k), XML.Dimensions, 'Memmapfile', Memmapfile);
                if ~flag
                    return
                end
            end
            Signal.Datetime = S;
            
            %% Recherche du signal
            
            k = findIndVariable(XML.Signals, varName);
            if isempty(k)
                flag = 0;
                Signal = [];
                return
            end
            
            [flag, S] = XMLBinUtils.readSignal(xmlDirName, XML.Signals(k), XML.Dimensions, 'Memmapfile', Memmapfile);
            if ~flag
                return
            end
            
            %% Cr�ation de la structure de sortie
            
            Signal.Value = S;
            Signal.Unit = XML.Signals(k).Unit;
        end
        
        
        function [flag, Data, DataBin, VarNameInFailure] = readWholeData(dataFileName, grpName, varargin)
            % Reads a SonarScope XML-Bin data structure
            %
            % Syntax
            %   [flag, Data, DataBin, VarNameInFailure] = SScCacheXMLBinUtils.XMLBinUtils.readGrpData(xmlFileName, ...)
            %
            % Input Arguments
            %   xmlFileName : Name of the XML-Bin Data structure
            %
            % Name-Value Pair Arguments
            %   XMLOnly    : true=only the XML file information is returned, not the data (Default : false)
            %   BinInData  : false : Data contains only the Data structure and Bin contains the data
            %                true  : Data contains both the Data and the Bin structure
            %   Memmapfile : Parameter used to optimize the memory (Default : 0)
            %                 0 : data is returned in memory (Ram)
            %                 1 : data is returned in virtual memory (cl_memmapfile objetcs)
            %                -1 : Signals are returned in memory, Images (Matices) are returned in virtual memory
            %   LogSilence : true=No printed messages (defaul : false)
            %
            % Output Arguments
            %   flag             : 0=KO, 1=OK
            %   Data             : Data structure of the file
            %   DataBin          : Structure containing the data only
            %   VarNameInFailure : Name of the var that failed in case of error
            %
            % Examples
            %   SScCacheXMLBinUtils.checkReadWholeData
            %
            % See also XMLBinUtils.readGrpData
            % Authors : JMA
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc SScCacheXMLBinUtils.checkReadWholeData" >SScCacheXMLBinUtils.checkReadWholeData</a>
            %   <a href="matlab:doc SScCacheXMLBinUtils.readWholeData"      >SScCacheXMLBinUtils.readWholeData</a>
            %   <a href="matlab:doc SScCacheXMLBinUtils"                    >SScCacheXMLBinUtils</a>
            %   <a href="matlab:doc XMLBinUtils.readGrpData"                >XMLBinUtils.readGrpData</a>
            %   <a href="matlab:doc XMLBinUtils"                            >XMLBinUtils</a>
            %  -------------------------------------------------------------------------------
            
            [varargin, XMLOnly]    = getPropertyValue(varargin, 'XMLOnly',    0);
            [varargin, Memmapfile] = getPropertyValue(varargin, 'Memmapfile', 0);
            [varargin, LogSilence] = getPropertyValue(varargin, 'LogSilence', 1); %#ok<ASGLU>
            
            flag             = 0;
            Data             = [];
            VarNameInFailure = [];
            DataBin          = [];
            
            %% Lecture du r�pertoire SonarScope
            
            [dataDirName, nom] = fileparts(dataFileName);
            xmlDirName = fullfile(dataDirName, 'SonarScope', nom);
            if ~exist(xmlDirName, 'dir')
                return
            end
            
            %% Lecture du fichier XML d�crivant la donn�e
            
            xmlFileName = fullfile(xmlDirName, [grpName '.xml']);
%             if ~exist(xmlFileName, 'file') && ~contains(xmlFileName, 'SSc_', 'IgnoreCase', true) % Car 'Ssc existe !!!
            if ~exist(xmlFileName, 'file') && ~contains(nom, 'SSc_', 'IgnoreCase', true) % Modif JMA le 22/03/2021 pour r�pertoire de Carla nomm�s SSc_XML et SSc_NC
                xmlFileName = fullfile(xmlDirName, ['SSc_' grpName '.xml']);
            end
            if ~exist(xmlFileName, 'file') % Ajout� par JMA le 12/03/2021
                flag = 0;
                return
            end
            [flag, Data, DataBin, VarNameInFailure] = XMLBinUtils.readGrpData(xmlFileName, ...
                'Memmapfile', Memmapfile, 'LogSilence', LogSilence, 'XMLOnly', XMLOnly);
            if ~flag
                return
            end
            
            %% Bidouille pour cr�er des dimensions r�elles dans le cas o� on a mis "Dimensions: 'nbPings,2'"
            
            Data = SScCacheXMLBinUtils.repairDimensions(Data);
        end
        
        
        function deleteCachDirectory(listFileNames)
            
            if ischar(listFileNames)
                listFileNames = {listFileNames};
            end
            
            N = length(listFileNames);
            hw = create_waitbar('Cache directory : delete XML-Bin', 'N', N);
            for k=1:N
                deleteCachDirectory_unitaire(listFileNames{k});
                my_waitbar(k, N, hw)
            end
            my_close(hw, 'MsgEnd')
            
            
            function flag = deleteCachDirectory_unitaire(nomFic)
                flag = 1;
%                 fprintf('\nProcessing "deleteCachDirectory" file "%s"\n', nomFic);
                [nomDir, nomFic] = fileparts(nomFic);
                nomDirXMLBin = fullfile(nomDir, 'SonarScope', nomFic);
                if exist(nomDirXMLBin, 'dir')
                    [flag, mes] = rmdir(nomDirXMLBin, 's'); %#ok<ASGLU>
                    if ~flag
                        mes1 = sprintf('Le r�pertoire %s n''a pas pu �tre supprim�.\n%s', nomDirXMLBin); %, mes);
                        mes2 = sprintf('Directory %s could not be removed from your computer.\n%s', nomDirXMLBin); %, mes);
                        my_warndlg(Lang(mes1,mes2), 0, 'Tag', 'rmDirFailed', 'TimeDelay', 30);
                        return
                    end
                end
            end
        end
        
        
        function Data = repairDimensions(Data)
            NewDimName  = {};
            NewDimValue = [];
            if isfield(Data, 'Signals')
                [Data, NewDimName, NewDimValue]  = SScCacheXMLBinUtils.repareDataBlock(Data, 'Signals', NewDimName, NewDimValue);
            end
            if isfield(Data, 'Images')
                [Data, NewDimName, NewDimValue]  = SScCacheXMLBinUtils.repareDataBlock(Data, 'Images', NewDimName, NewDimValue);
            end
            if isfield(Data, 'Strings')
                [Data, NewDimName, NewDimValue]  = SScCacheXMLBinUtils.repareDataBlock(Data, 'Strings', NewDimName, NewDimValue);
            end
            [~, ordre] = unique(NewDimName);
            NewDimName  = NewDimName(ordre);
            NewDimValue = NewDimValue(ordre);
            for k=1:length(NewDimValue)
                Data.Dimensions.(NewDimName{k}) = NewDimValue(k);
            end
        end
    end
    
    
    methods (Static, Access = private)
        
        function [Data, NewDimName, NewDimValue] = repareDataBlock(Data, BlockName, NewDimName, NewDimValue)
            for k1=1:length(Data.(BlockName))
                Dim = Data.(BlockName)(k1).Dimensions;
                if isequal(Dim, [1 1]) % Ratrapage erreur format RAW (EK60)
                    %{
            <Strings>
                <Name>NavigationFile</Name>
                <Dimensions>1, 1</Dimensions>
                <Storage>char</Storage>
                <Unit/>
                <FileName>Ssc_Position\NavigationFile.bin</FileName>
                <Tag>SounderTime</Tag>
            </Strings>
                    %}
                    Data.(BlockName)(k1).Dimensions = 'StringLength1,StringLength2';
                    NewDimName{end+1}  = 'StringLength2'; %#ok<AGROW>
                    NewDimValue(end+1) = 256; %#ok<AGROW>
                    NewDimName{end+1}  = 'StringLength1'; %#ok<AGROW>
                    NewDimValue(end+1) = 1; %#ok<AGROW>
                    
                else
                    if ischar(Dim)
                        DimNames = strtrim(Dim);
                        DimNames = strsplit(DimNames, ',');
                        DimVar = '';
                        flag = 0;
                        for k2=1:length(DimNames)
                            X = str2double(DimNames{k2});
                            if ~isnan(X) && (X ~= 1)
                                DimNames{k2} = sprintf('DimVal_%d', X);
                                NewDimName{end+1}  = DimNames{k2}; %#ok<AGROW>
                                NewDimValue(end+1) = X; %#ok<AGROW>
                                flag = 1;
                            end
                            DimVar = sprintf('%s,%s', DimVar, DimNames{k2});
                        end
                        if flag
                            DimVar(1) = [];
                            Data.(BlockName)(k1).Dimensions = DimVar;
                        end
                    end
                end
            end
        end
    end
end