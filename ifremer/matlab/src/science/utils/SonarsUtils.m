classdef SonarsUtils
    % Description
    %   User Interfaces collection
    
    %     properties
            % TODO JMA : on pourrait avoir une propri�t� qui dit si on interpole ou non les signaux
    %     end
    
    methods (Static)
        
        function DatetimeRx = get_DatetimeRx(TwoWayTravelTime_s, TxDelay_PingBeam_s, DatetimeTx)
            TwoWayTravelTime_s = double(TwoWayTravelTime_s);
            TxDelay_PingBeam_s = double(TxDelay_PingBeam_s);
            if size(DatetimeTx, 1) == 1
                DatetimeTx = DatetimeTx';
            end
            DatetimeRx = bsxfun(@plus, (TwoWayTravelTime_s + TxDelay_PingBeam_s) / 86400, datenum(DatetimeTx));
            DatetimeRx = datetime(DatetimeRx, 'ConvertFrom', 'datenum');
        end
        
        function RollRx = get_RollRx(DataAttitude, DatetimeTx, TwoWayTravelTime_s, TxDelay_PingBeam_s)
            % Roll is filered because data is quantified (0.01 degree)
            [B,A] = butter(2, 0.1);
            Roll = filtfilt(B, A, double(DataAttitude.Roll(:)));
            DatetimeRx = SonarsUtils.get_DatetimeRx(TwoWayTravelTime_s, TxDelay_PingBeam_s, DatetimeTx); % TODO : ClSounderAll.get_DatetimeTx : fct Static
            RollRx = my_interp1(DataAttitude.Datetime, Roll, DatetimeRx);
        end
        
        % Etc et remplacer les m�thodes de cl_simrad_all par celles-ci quand la version R2011b sera fig�e
    end
end
