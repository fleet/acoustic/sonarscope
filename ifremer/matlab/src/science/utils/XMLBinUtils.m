classdef XMLBinUtils
    % Collection of static functions to write data into SonarScope XMLBin files.
    %     - etc
    % Authors  : JMA Authors
    % See also : ALL_Cache2Netcdf, XMLBinUtils
    %
    % Reference pages in Help browser
    %   <a href="matlab:doc XMLBinUtils" >XMLBinUtils</a>
    %  -------------------------------------------------------------------------------
    
    methods (Static)
        
        function [flag, Data, DataBin, VarNameInFailure] = readGrpData(xmlFileName, varargin)
            % Reads a SonarScope XML-Bin data structure
            %
            % Syntax
            %   [flag, Data, DataBin, VarNameInFailure] = XMLBinUtils.readGrpData(xmlFileName, ...)
            %
            % Input Arguments
            %   xmlFileName : Name of the XML-Bin Data structure
            %
            % Name-Value Pair Arguments
            %   XMLOnly    : true=only the XML file information is returned, not the data (Default : false)
            %   BinInData  : false : Data contains only the Data structure and Bin contains the data
            %                true  : Data contains both the Data and the Bin structure
            %   Memmapfile : Parameter used to optimize the memory (Default : 0)
            %                 0 : data is returned in memory (Ram)
            %                 1 : data is returned in virtual memory (cl_memmapfile objetcs)
            %                -1 : Signals are returned in memory, Images (Matices) are returned in virtual memory
            %   LogSilence : true=No printed messages (defaul : false)
            %
            % Output Arguments
            %   flag             : 0=KO, 1=OK
            %   Data             : Data structure of the file
            %   DataBin          : Structure containing the data only
            %   VarNameInFailure : Name of the var that failed in case of error
            %
            % Examples
            %   fileNameAll = getNomFicDatabase('EM1002_BELGICA_005053_raw.all')
            %   cl_simrad_all('nomFic', fileNameAll); % Just to be sure the Data-Bin is created
            %   [dirName, fileName] = fileparts(fileNameAll)
            %
            %   xmlFileName = fullfile(dirName, 'SonarScope', fileName, 'Ssc_Attitude.xml')
            %   [flag, Data, DataBin] = XMLBinUtils.readGrpData(xmlFileName);
            %
            %   xmlFileName = fullfile(dirName, 'SonarScope', fileName, 'Ssc_Depth.xml')
            %   [flag, Data, DataBin] = XMLBinUtils.readGrpData(xmlFileName, 'Memmapfile', -1);
            %
            % See also SScCacheXMLBinUtils.readWholeData
            % Authors : JMA
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc XMLBinUtils.readGrpData" >XMLBinUtils.readGrpData</a>
            %   <a href="matlab:doc XMLBinUtils"             >XMLBinUtils</a>
            %  -------------------------------------------------------------------------------
            
            [varargin, XMLOnly]    = getPropertyValue(varargin, 'XMLOnly',    0);
            [varargin, Memmapfile] = getPropertyValue(varargin, 'Memmapfile', 0);
            [varargin, LogSilence] = getPropertyValue(varargin, 'LogSilence', 1);
            [varargin, BinInData]  = getPropertyValue(varargin, 'BinInData',  1); %#ok<ASGLU>
            
            Data             = [];
            DataBin          = [];
            VarNameInFailure = [];
            
            flag = exist(xmlFileName, 'file');
            if ~flag
                return
            end
            
            try
                % edit(xmlFileName)
                XML = xml_mat_read(xmlFileName);
                % gen_object_display(Data)
            catch %#ok<CTCH>
                messageErreurFichier(xmlFileName, 'ReadFailure');
                return
            end
            % gen_object_display(XML)
            
            %% Cas o� on ne veut lire que le XML
            
            if XMLOnly
                flag = 1;
                Data = XML;
                return
            end
            
            %% Lecture du r�pertoire SonarScope
            
            [xmlPathName, filename] = fileparts(xmlFileName);
            signalDirName = fullfile(xmlPathName, filename);
            if ~exist(signalDirName, 'dir')
                return
            end
            
            %% Lecture des fichiers binaires des signaux
            
            if isfield(XML, 'Signals')
                if Memmapfile == -1
                    MemmapfileSignal = 0;
                else
                    MemmapfileSignal = abs(Memmapfile);
                end
                for k=1:length(XML.Signals)
                    [flag, Signal, VarNameInFailure] = XMLBinUtils.readSignal(xmlPathName, XML.Signals(k), XML.Dimensions, ...
                        'Memmapfile', MemmapfileSignal & ~strcmp(XML.Signals(k).Storage, 'char'), 'LogSilence', LogSilence);
                    if ~flag
                        if XMLBinUtils.isVarPostprocessed(XML.Signals(k))
                            XML.(XML.Signals(k).Name) = {};
                            continue
                        else
                            return
                        end
                    end
                    DataBin.(XML.Signals(k).Name) = Signal; % TODO JMA : possible bug si nom de variable > 63 caract�res
                    if isa(Signal, 'cl_time') % Ajout JMA le 02/02/2020
                        nomVarDatetime = strrep(XML.Signals(k).Name, 'Time', 'Datetime');
                        DataBin.(nomVarDatetime) = datetime(Signal.timeMat, 'ConvertFrom', 'datenum');
                    end
                end
            end
            
            %% Lecture des fichiers binaires des images
            
            if isfield(XML, 'Images')
                MemmapfileImage = abs(Memmapfile);
                for k=1:length(XML.Images)
                    [flag, Image, VarNameInFailure] = XMLBinUtils.readSignal(xmlPathName, XML.Images(k), XML.Dimensions, ...
                        'Memmapfile', MemmapfileImage, 'LogSilence', LogSilence);
                    if ~flag
                        if XMLBinUtils.isVarPostprocessed(XML.Images(k))
                            XML.(XML.Images(k).Name) = {};
                            continue
                        else
                            return
                        end
                    end
                    DataBin.(XML.Images(k).Name) = Image; % TODO JMA : possible bug si nom de variable > 63 caract�res
                end
            end
            
            %% Lecture des fichiers binaires des Strings
            
            if isfield(XML, 'Strings')
                for k=1:length(XML.Strings)
                    [flag, String] = XMLBinUtils.readStrings(xmlPathName, XML.Strings(k), XML.Dimensions);
                    if ~flag
                        return
                    end
                    DataBin.(XML.Strings(k).Name) = String; % TODO JMA : possible bug si nom de variable > 63 caract�res
                end
            end
            
            if BinInData
                names = fieldnames(XML);
                for k=1:numel(names)
                    Data.(names{k}) = XML.(names{k});
                end
                if ~isempty(DataBin)
                    names = fieldnames(DataBin);
                    for k=1:numel(names)
                        Data.(names{k}) = DataBin.(names{k});
                    end
                end
            else
                Data = XML;
            end
            
            if isfield(Data, 'Time') && isa(Data.Time, 'cl_time')
                Data.Datetime = datetime(Data.Time.timeMat, 'ConvertFrom', 'datenum');
            elseif isfield(Data, 'Date') && isfield(Data, 'Hour')
                Data.Time = cl_time('timeIfr', Data.Date(:), Data.Hour(:));
                Data.Datetime = datetime(Data.Time.timeMat, 'ConvertFrom', 'datenum');
            end
            
            flag = 1;
        end
        
        
        function [flag, Signal, VarNameInFailure] = readSignal(xmlPathName, InfoSignal, Dimensions, varargin)
            % Reads a signal in a SSc XML-Bin data structure
            %
            % Syntax
            %   [flag, Signal, VarNameInFailure] = XMLBinUtils.readSignal(xmlPathName, InfoSignal, Dimensions, ...)
            %
            % Input Arguments
            %   xmlPathName : Path name of the XML file
            %   InfoSignal  : Structure containing the description of the signal
            %   Dimensions  : Structure containing the description of the dimensions
            %
            % Name-Value Pair Arguments
            %   ErMapper   : true=The signal is stored up/down (Default : false)
            %   Memmapfile : Parameter used to optimize the memory (Default : 0)
            %                 0 : data is returned in memory (Ram)
            %                 1 : data is returned in virtual memory (cl_memmapfile objetcs)
            %                -1 : Signals are returned in memory, Images (Matices) are returned in virtual memory
            %   LogSilence : true=No printed messages (defaul : false)
            %
            % Output Arguments
            %   flag             : 0=KO, 1=OK
            %   Signal           : Value of the signals
            %   VarNameInFailure : Name of the var that failed in case of error
            %
            % Examples
            %   fileNameAll = getNomFicDatabase('EM1002_BELGICA_005053_raw.all')
            %   cl_simrad_all('nomFic', fileNameAll); % Just to be sure the Data-Bin is created
            %   [dirName, fileName] = fileparts(fileNameAll)
            %
            %   xmlPathName = fullfile(dirName, 'SonarScope', fileName);
            %   xmlFileName = fullfile(xmlPathName, 'Ssc_Depth.xml')
            %   XML = xml_mat_read(xmlFileName);
            %   [flag, Signal, VarNameInFailure] = XMLBinUtils.readSignal(xmlPathName, XML.Signals(1), XML.Dimensions);
            %
            % See also readGrpData
            % Authors : JMA
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc XMLBinUtils.readSignal"  >XMLBinUtils.readSignal</a>
            %   <a href="matlab:doc XMLBinUtils.readGrpData" >XMLBinUtils.readGrpData</a>
            %   <a href="matlab:doc XMLBinUtils"             >XMLBinUtils</a>
            %  -------------------------------------------------------------------------------
            
            % ErMapper sert uniquement dans le cas d'une image RGB o� on doit �crire
            % chaque ligne enti�re en Red, Green et blue et passer � la ligne suivante
            % sinon les matrices 3D sont �crites les canaux les uns apr�s les autres
            [varargin, ErMapper]       = getPropertyValue(varargin, 'ErMapper',   0);
            [varargin, flagMemmapfile] = getPropertyValue(varargin, 'Memmapfile', 0);
            [varargin, LogSilence]     = getPropertyValue(varargin, 'LogSilence', 1); %#ok<ASGLU>
            
            VarNameInFailure = [];
            
            mots = strsplit(InfoSignal.Dimensions, ',');
            nomDim = strtrim(mots);
            valDim = str2double(nomDim);
            for k=1:length(valDim)
                if isnan(valDim(k))
                    Dims(k) = Dimensions.(nomDim{k}); %#ok<AGROW>
                else
                    Dims(k) = valDim(k); %#ok<AGROW>
                end
            end
            
            if any(Dims == 0)
                Signal = [];
                flag = 1;
                return
            end
            
            signalFileName = fullfile(xmlPathName, InfoSignal.FileName);
            if ~exist(signalFileName, 'file')
                % if ~java.io.File(signalFileName).exists % Test JMA le 25/01/2019 mais c'est pas concluant au niveau performances
                Signal = [];
                VarNameInFailure = InfoSignal.Name;
                flag = 0;
                return
            end
            
            [signalDirName, nomFic2] = fileparts(signalFileName);
            nomFic2 = fullfile(signalDirName, [nomFic2 '_new.bin']);
            if exist(nomFic2, 'file')
                signalFileName = nomFic2;
                [~, InfoSignal.FileName] = fileparts(nomFic2);
                % TODO JMA
                % Il faudrait essayer de supprimer signalFileName et renommer nomFic2 en
                % signalFileName
            end
            
            if flagMemmapfile == - 1
                flagMemmapfile = 0;
            end
            
            % TODO BugMatricesCarr�es
            if flagMemmapfile && (length(Dims) >= 2) && (Dims(1) == Dims(2))
                flagMemmapfile = 0;
            end
            
            if flagMemmapfile && ~strcmpi(InfoSignal.Unit, 'days since JC') && ~strcmp(InfoSignal.Storage, 'char')
                Signal = cl_memmapfile('FileName', signalFileName, 'FileCreation', 0, ...
                    'Format', InfoSignal.Storage, 'Size', Dims, 'ErMapper', ErMapper, 'LogSilence', LogSilence);
                %     flag = 1;
                flag = ~isempty(Signal); % Modif JMA le 13/04/2017
                if ~flag
                    VarNameInFailure = InfoSignal.Name;
                end
                return
            end
            
            %% Pour trouver tout ce qui n'est pas appel� en memmapfile
            
            % if (get_LevelQuestion >= 3) && ~isdeployed && LogSilence && ~strcmpi(InfoSignal.Unit, 'days since JC') && isempty(strfind(InfoSignal.FileName, 'Ssc_InstallationParameters'))
            %     fprintf('Developpement uniquement : Signal appel� sans memmapfile : "%s"\n', InfoSignal.FileName);
            % end
            
            fid = fopen(signalFileName, 'r');
            if fid == -1
                messageErreurFichier(signalFileName);
                VarNameInFailure = InfoSignal.Name;
                flag = 0;
                return
            end
            
            [Signal, n] = fread(fid, [InfoSignal.Storage '=>' InfoSignal.Storage]);
            fclose(fid);
            
            if n < prod(Dims)
                messageErreurFichier(signalFileName);
                VarNameInFailure = InfoSignal.Name;
                flag = 0;
                return
            end
            if n ~= prod(Dims)
                if ~strcmp(InfoSignal.Name, 'Entries') % Exception pour SebedImage : � r�soudre plus intelligemment
                    messageErreurFichier(signalFileName, 'ReadFailure');
                    flag = 0;
                    return
                end
            end
            
            if isfield(InfoSignal, 'Unit') && strcmpi(InfoSignal.Unit, 'days since JC')
                try
                    Signal = reshape(Signal, fliplr(Dims))';
                catch %#ok<CTCH>
                    % Pour corriger un bug d'export Anne-Laure : faut chercher pourquoi ces donn�es n'�taient pas coh�rentes (une valeur de trop par rapport � la taille du tableau)
                    Signal = reshape(Signal(1:prod(Dims)), fliplr(Dims))';
                end
                
                % TODO JMA : � modifier quand on fera le changement des signaux
                % Cette manip est d�sastreuse pour le Geoswath qui pingue en alternance
                % entre b�bord et tribord
                if all(size(Signal) > 2)
                    % TODO JMA : pour le traitement des Time du format HAC
                else
                    if all(isnan(Signal(:,1))) && (size(Signal,2) == 2)
                        Signal = Signal(:,2);
                    else
                        Signal = Signal(:,1); % Cata pour conversion SSC -> NetCDF !!!
                    end
                end
                Signal = cl_time('timeMat', Signal);
                % Signal = datetime(Signal, 'ConvertFrom', 'datenum');
            else
                if ErMapper && (length(Dims) == 3)
                    Signal = reshape(Signal, Dims([2 3 1]));
                    Signal = permute(Signal, [3 1 2]);
                elseif length(Dims) == 2
                    Signal = reshape(Signal, Dims([2 1]));
                    Signal = my_transpose(Signal);
                else
                    Signal = reshape(Signal, Dims([2 1 3:end]));
                    Signal = my_transpose(Signal);
                end
            end
            
            flag = 1;
        end
        
        
        function [flag, Signal] = readStrings(xmlPathName, InfoSignal, Dimensions)
            % Reads a signal made of strings in a SSc XML-Bin data structure
            %
            % Syntax
            %   [flag, Signal] = XMLBinUtils.readStrings(xmlPathName, InfoSignal, Dimensions, ...)
            %
            % Input Arguments
            %   xmlPathName : Path name of the XML file
            %   InfoSignal  : Structure containing the description of the signal
            %   Dimensions  :  Structure containing the description of the dimensions
            %
            % Output Arguments
            %   flag   : 0=KO, 1=OK
            %   Signal : Value of the signals
            %
            % Examples
            %   %{
            %   % TODO JMA : Cr�er un vrai exemple
            %   fileNameAll = getNomFicDatabase('EM1002_BELGICA_005053_raw.all')
            %   cl_simrad_all('nomFic', fileNameAll); % Just to be sure the Data-Bin is created
            %   [dirName, fileName] = fileparts(fileNameAll)
            %
            %   xmlPathName = fullfile(dirName, 'SonarScope', fileName);
            %   xmlFileName = fullfile(xmlPathName, 'Ssc_Depth.xml')
            %   %}
            %   XML = xml_mat_read(xmlFileName);
            %   [flag, Signal] = XMLBinUtils.readStrings(xmlPathName, XML.Strings(1), XML.Dimensions);
            %
            % See also readGrpData
            % Authors : JMA
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc XMLBinUtils.readStrings" >XMLBinUtils.readStrings</a>
            %   <a href="matlab:doc XMLBinUtils.readGrpData" >XMLBinUtils.readGrpData</a>
            %   <a href="matlab:doc XMLBinUtils"             >XMLBinUtils</a>
            %  -------------------------------------------------------------------------------
            
            % Line = InfoSignal.Dimensions;
            % Line = strtrim(Line);
            
            if isa(InfoSignal.Dimensions, 'double')
                Dims = InfoSignal.Dimensions;
            else
                mots = strsplit(InfoSignal.Dimensions, ',');
                for k=1:length(mots)
                    if ischar(mots{k})
                        nomDim = strtrim(mots{k});
                        valDim = str2double(nomDim);
                        if isnan(valDim)
                            Dims(k) = Dimensions.(nomDim); %#ok<AGROW>
                        else
                            Dims(k) = valDim; %#ok<AGROW>
                        end
                    else
                        % Cas des Strings reproduite qu'une fois (fichier de navigation annexe Raw et Segy).
                        Dims(k) = mots{1}(k); %#ok<AGROW>
                    end
                end
            end
            if any(Dims == 0)
                Signal = [];
                flag = 1;
                return
            end
            
            signalFileName = fullfile(xmlPathName, InfoSignal.FileName);
            
            [nomDir2, nomFic2] = fileparts(signalFileName);
            nomFic2 = fullfile(nomDir2, [nomFic2 '_new.txt']);
            if exist(nomFic2, 'file')
                signalFileName = nomFic2;
                [~, InfoSignal.FileName] = fileparts(nomFic2);
                % TODO JMA
                % Il faudrait essayer de supprimer signalFileName et renommer nomFic2 en
                % signalFileName
            end
            
            fid = fopen(signalFileName, 'r');
            if fid == -1
                messageErreurFichier(signalFileName);
                flag = 0;
                return
            end
            
            Signal = strings(Dims);
            for k=1:prod(Dims)
                Ligne = fgetl(fid);
                if Ligne == -1
                    Signal(k) = "";
                else
                    Ligne = deblank(Ligne);
                    Signal(k) = string(Ligne);
                end
            end
            fclose(fid);
            
            flag = 1;
        end
        
        
        function [flag, varargout] = getSignals(xmlFileName, varargin)
            % Reads one or several signals in a SSc XML-Bin data structure
            %
            % Syntax
            %   [flag, ...] = XMLBinUtils.getSignals(xmlFileName, ...)
            %
            % Input Arguments
            %   xmlFileName : Name of the XML file
            %
            % Optional Input Arguments
            %   firstSignalName
            %   secondSignalName
            %   etc ...
            %
            % Name-Value Pair Arguments
            %   Memmapfile : Parameter used to optimize the memory (Default : 0)
            %                 0 : data is returned in memory (Ram)
            %                 1 : data is returned in virtual memory (cl_memmapfile objetcs)
            %                -1 : Signals are returned in memory, Images (Matices) are returned in virtual memory
            %
            % Output Arguments
            %   flag         : 0=KO, 1=OK
            %   firstSignal  : Value of the first signal
            %   secondSignal : Value of the second signal
            %   etc ...
            %
            % Examples
            %   fileNameAll = getNomFicDatabase('0011_20110307_211804_IKATERE_02.all');
            %   cl_simrad_all('nomFic', fileNameAll); % Just to be sure the Data-Bin is created
            %   [dirName, fileName] = fileparts(fileNameAll)
            %   xmlFileName = fullfile(dirName, 'SonarScope', fileName, 'Ssc_Depth.xml')
            %   [flag, Heading, SoundSpeed, Depth] = XMLBinUtils.getSignals(xmlFileName, 'Heading', 'SoundSpeed', 'Depth', 'Memmapfile', -1);
            %
            % See also readGrpData
            % Authors : JMA
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc XMLBinUtils.getSignals" >XMLBinUtils.getSignals</a>
            %   <a href="matlab:doc XMLBinUtils"            >XMLBinUtils</a>
            %  -------------------------------------------------------------------------------
            
            [varargin, flagMemmapfile] = getPropertyValue(varargin, 'Memmapfile', 0);
            [varargin, Mute]           = getPropertyValue(varargin, 'Mute',       0);
            
            nbSignals = length(varargin);
            for k=1:nbSignals
                varargout{k} = []; %#ok<AGROW>
            end
            
            [flag, XML] = XMLBinUtils.readGrpData(xmlFileName, 'XMLOnly', 1);
            if ~flag
                return
            end
            
            for k=1:nbSignals
                signalName = varargin{k};
                indexVar = findIndVariable(XML.Signals, signalName);
                isImage = false;
                if isempty(indexVar)
                    indexVar = findIndVariable(XML.Images, signalName);
                    isImage = true;
                end
                if isempty(indexVar)
                    if ~Mute % Pour �viter message NbSwaths not found si sondeur Reson
                        str1 = sprintf('Signal "%s" non trouv� dans "%s', signalName, xmlFileName);
                        str2 = sprintf('Signal "%s" not found in "%s',    signalName, xmlFileName);
                        my_warndlg(Lang(str1,str2), 0);
                    end
                    varargout{k} = []; % Cas de donn�es du Reson 7150
                    return
                end
                
                nomDir = fileparts(xmlFileName);
                if isImage
                    [flag, Signal] = XMLBinUtils.readSignal(nomDir, XML.Images(indexVar), XML.Dimensions, ...
                        'Memmapfile', abs(flagMemmapfile));
                else
                    [flag, Signal] = XMLBinUtils.readSignal(nomDir, XML.Signals(indexVar), XML.Dimensions, ...
                        'Memmapfile', max(0, flagMemmapfile));
                end
                if ~flag
                    return
                end
                varargout{k} = Signal;
            end
        end
        
        
        function flag = isVarPostprocessed(Info)
            % Test if a variable has already been postprocessed
            %
            % Syntax
            %   flag = XMLBinUtils.isVarPostprocessed(Info)
            %
            % Input Arguments
            %   Info :
            %
            % Output Arguments
            %   flag : 0=Var is not postprocessed, 1=Var is already postprocessed
            %
            % Examples
            %   fileNameAll = getNomFicDatabase('EM1002_BELGICA_005053_raw.all')
            %   cl_simrad_all('nomFic', fileNameAll); % Just to be sure the Data-Bin is created
            %   [dirName, fileName] = fileparts(fileNameAll)
            %   xmlPathName = fullfile(dirName, 'SonarScope', fileName);
            %   xmlFileName = fullfile(xmlPathName, 'Ssc_Depth.xml')
            %  [flag, XML] = XMLBinUtils.readGrpData(xmlFileName, 'XMLOnly', 1)
            %
            %   flag = XMLBinUtils.isVarPostprocessed(XML.Images(1))
            %
            % See also readGrpData
            % Authors : JMA
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc XMLBinUtils.isVarPostprocessed" >XMLBinUtils.isVarPostprocessed</a>
            %   <a href="matlab:doc XMLBinUtils"                    >XMLBinUtils</a>
            %  -------------------------------------------------------------------------------
            
            flag = isfield(Info, 'Postprocess') && ~isempty(Info.Postprocess) && (Info.Postprocess == 1);
        end
        
        
        function flag = RestoreOriginalSignal(xmlPathName, InfoSignal)
            % Restore a backup signal
            %
            % Syntax
            %   flag = XMLBinUtils.RestoreOriginalSignal(xmlPathName, InfoSignal)
            %
            % Input Arguments
            %   xmlPathName : Path name of the XML file
            %   InfoSignal  : Structure containing the description of the signal
            %
            % Output Arguments
            %   flag : 0=KO, 1=OK
            %
            % Examples
            %   xmlFileName = '???'; % TODO JMA : Cr�er un vrai exemple
            %   XML = xml_mat_read(xmlFileName);
            %   flag = XMLBinUtils.RestoreOriginalSignal(xmlPathName, XML.Signals(1));
            %
            % See also BackupOriginalSignal readGrpData
            % Authors : JMA
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc XMLBinUtils.BackupOriginalSignal"  >XMLBinUtils.BackupOriginalSignal</a>
            %   <a href="matlab:doc XMLBinUtils.RestoreOriginalSignal" >XMLBinUtils.RestoreOriginalSignal</a>
            %   <a href="matlab:doc XMLBinUtils"                       >XMLBinUtils</a>
            %  -------------------------------------------------------------------------------

            InfoSignalOrigin = InfoSignal;
            InfoSignalOrigin.FileName = strrep(InfoSignal.FileName, '.bin', 'Origin.bin');
            
            if ~exist(fullfile(xmlPathName, InfoSignalOrigin.FileName), 'file')
                flag = 0;
                return
            end
            
            [flag, X] = readSignal(xmlPathName, InfoSignalOrigin, []);
            if ~flag
                return
            end
            flag = writeSignal(xmlPathName, InfoSignal, X);
        end
        
        
        function flag = BackupOriginalSignal(xmlPathName, InfoSignal)
            % Backup of a signal in case we want to modify it and retreive it later
            %
            % Syntax
            %   flag = XMLBinUtils.BackupOriginalSignal(xmlPathName, InfoSignal)
            %
            % Input Arguments
            %   xmlPathName : Path name of the XML file
            %   InfoSignal  : Structure containing the description of the signal
            %
            % Output Arguments
            %   flag : 0=KO, 1=OK
            %
            % Examples
            %   xmlFileName = '???'; % TODO JMA : Cr�er un vrai exemple
            %   XML = xml_mat_read(xmlFileName);
            %   flag = XMLBinUtils.BackupOriginalSignal(xmlPathName, XML.Signals(1));
            %
            % See also RestoreOriginalSignal readGrpData
            % Authors : JMA
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc XMLBinUtils.BackupOriginalSignal"  >XMLBinUtils.BackupOriginalSignal</a>
            %   <a href="matlab:doc XMLBinUtils.RestoreOriginalSignal" >XMLBinUtils.RestoreOriginalSignal</a>
            %   <a href="matlab:doc XMLBinUtils"                       >XMLBinUtils</a>
            %  -------------------------------------------------------------------------------
            
            [flag, X] = readSignal(xmlPathName, InfoSignal, []);
            if ~flag
                return
            end
            
            InfoSignal.FileName = strrep(InfoSignal.FileName, '.bin', 'Origin.bin');
            
            flag = writeSignal(xmlPathName, InfoSignal, X);
        end
    end
end