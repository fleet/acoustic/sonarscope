classdef DropDownButtonComponent < handle & uiextras.HBox
    % Description
    %   Component DropDownButtonComponent who can edit value of a ClParametre
    %
    % Syntax
    %   a = DropDownButtonComponent(...)
    %
    % DropDownButtonComponent properties :
    %   parent - parent component
    %   param  - ClParametre instance
    %   sizes  - width array of chlid components
    %
    % Output Arguments
    %   a : One DropDownButtonComponent instance
    %
    % Examples
    % see real example in DemoDropDownButtonComponent
    %  contextMenu = uicontextmenu(this.parentFigure);
    %  contextMenuItem1 = uimenu(contextMenu, 'Text', 'item1', 'Callback', @contextMenuItem1Callback);
    %  contextMenuItem2 = uimenu(contextMenu, 'Text', 'item2', 'Callback', @contextMenuItem2Callback);
    %  contextMenuItem3 = uimenu(contextMenu, 'Text', 'item3', 'Callback', @contextMenuItem3Callback);
    %  %DropDown bouton
    %  DropDownButtonComponent(parentComponent, parentFigure, 'Test', @buttonCallback, contextMenu);
    %
    % Authors : MHO
    % See also DemoDropDownButtonComponent Authors
    % ----------------------------------------------------------------------------
    
    properties
        parent          % parent component
        sizes = [-1 16] % width array of chlid components
    end
    
    properties (Access = private)
        parentFigure
        actionTitle
        actionButton
        contextMenuButton
        callbackFunc
        contextMenu
    end
    
    methods
        function this = DropDownButtonComponent(parent, parentFigure, actionTitle, callbackFunc, contextMenu, varargin)
            % Constructor of the class
            
            % Call SuperConstructor
            this = this@uiextras.HBox('Parent', parent);
            
            %% Lecture des arguments
            this.parent       = parent;
            this.parentFigure = parentFigure;
            this.actionTitle  = actionTitle;
            this.callbackFunc = callbackFunc;
            this.contextMenu  = contextMenu;
            
            [varargin, this.sizes] = getPropertyValue(varargin, 'sizes', this.sizes); %#ok<ASGLU>
            
            % Action Button
            this.actionButton = uicontrol('Parent', this, ...
                'Style',    'pushButton', ...
                'String',   actionTitle, ...
                'Callback', @this.actionButtonCallback);
            
            % DropDown Button
            dropDownIcon = iconizeFic(getNomFicDatabase('1454012086_ic_arrow_drop_down_48px.png'));
            this.contextMenuButton = uicontrol('Parent', this, ...
                'Style',         'pushButton', ...
                'CData',         dropDownIcon, ...
                'UIContextMenu', this.contextMenu, ...
                'Callback',      @this.showContextMenuCallback);
            
            
            % set sizes of uicontrols
            if ~isempty(this.sizes)
                this.set('Sizes', this.sizes);
            end
            
            % set components visible or not depending size
            this.setComponentsVisibleDependingSize();
            
            % set Padding & Spacing 0px
            this.set('Padding', 0);
            this.set('Spacing', 0);
        end
        
        function actionButtonCallback(this, src, ~)
            this.callbackFunc(src);
        end
        
        function showContextMenuCallback(this, src, ~)
            % Show contextmenu callback
            
            obj = src;
            actionButtonPosition = this.actionButton.get('Position');
            hPosition = -actionButtonPosition(3); % - width
            vPosition = -4;
            while isprop(obj, 'Position') && (obj~= this.parentFigure)
                hPosition = hPosition + obj.Position(1);
                vPosition = vPosition + obj.Position(2);
                
                if isprop(obj,'Parent')
                    obj = obj.Parent;
                end
            end
            
            this.contextMenu.Position = [hPosition vPosition];
            this.contextMenu.Visible  = 'on';
        end
        
        function this = set.sizes(this, sz) %#ok<MCHV2>
            % Setter of "sizes" property
            
            if ~isempty(sz)
                if length(sz) == 2
                    this.sizes = sz;
                else
%                     str1 = 'Le param�tre "sizes" transmis � "DropDownButtonComponent" doit �tre de taille 2.';
                    str2 = 'The parametre "sizes" sent to "DropDownButtonComponent" must be an array of size 2.';
                    my_warndlg(str2, 1);
                end
            end
        end
        
        function setComponentsVisibleDependingSize(this)
            % set components visible or not depending their size
            
            visible = 'on';
            if this.sizes(1) == 0
                visible = 'off';
            end
            this.actionButton.set('Visible', visible);
            
            visible = 'on';
            if this.sizes(2) == 0
                visible = 'off';
            end
            this.contextMenuButton.set('Visible', visible);
        end
    end
end
