classdef DropDownButtonUiComponent
    % Description
    %   Component DropDownButtonComponent who can edit value of a ClParametre
    %
    % Syntax
    %   a = DropDownButtonComponent(...)
    %
    % DropDownButtonComponent properties :
    %   parent  - parent component
    %   param   - ClParametre instance
    %   sizes   - width array of chlid components
    %
    % Output Arguments
    %   a : One DropDownButtonComponent instance
    %
    % Examples
    % see real example in DemoDropDownButtonComponent
    %  contextMenu = uicontextmenu(this.parentFigure);
    %  contextMenuItem1 = uimenu(contextMenu,'Text','item1','Callback',@contextMenuItem1Callback);
    %  contextMenuItem2 = uimenu(contextMenu,'Text','item2','Callback',@contextMenuItem2Callback);
    %  contextMenuItem3 = uimenu(contextMenu,'Text','item3','Callback',@contextMenuItem3Callback);
    %  %DropDown bouton
    %  DropDownButtonUiComponent(parentComponent,parentFigure, 'Test', @buttonCallback, contextMenu);
    %
    % Authors : MHO
    % See also DemoDropDownButtonUiComponent Authors
    % ----------------------------------------------------------------------------
    
    properties
        parent          % parent component
        sizes = {'1x', 16}
        layout
    end
    
    properties (Access = private)
        parentFigure
        actionTitle
        actionButton
        contextMenuButton
        callbackFunc
        contextMenu
    end
    
    methods
        function this = DropDownButtonUiComponent(parent, parentFigure, actionTitle, callbackFunc, contextMenu, varargin)
            % Constructor of the class
            
            % Call SuperConstructor
            this.layout = uigridlayout(parent, 'ColumnSpacing', 0, 'RowSpacing', 0, 'Padding', 0);
            
            %% Lecture des arguments
            this.parent       = parent;
            this.parentFigure = parentFigure;
            this.actionTitle  = actionTitle;
            this.callbackFunc = callbackFunc;
            this.contextMenu  = contextMenu;
            
            [varargin, this.sizes] = getPropertyValue(varargin, 'sizes', this.sizes); %#ok<ASGLU>
            
            % Action Button
            this.actionButton = uibutton(this.layout, 'push', ...
                'Text',    actionTitle, ...
                'ButtonPushedFcn', @this.actionButtonCallback);
            
            % DropDown Button
            dropDownIcon = iconizeFic(getNomFicDatabase('1454012086_ic_arrow_drop_down_48px.png'));
            this.contextMenuButton = uibutton(this.layout, 'push', ...
                'Text',    '', ...
                'Icon',    dropDownIcon, ...
                'ContextMenu', this.contextMenu, ...
                'ButtonPushedFcn', @this.showContextMenuCallback);
            
            
            % set sizes of uicontrols
            if ~isempty(this.sizes)
                %                 this.set('Sizes', this.sizes);
                this.layout.ColumnWidth = this.sizes;
                this.layout.RowHeight = {'1x'};
                
            end
            
            % set components visible or not depending size
            this.setComponentsVisibleDependingSize();
            
        end
        
        function actionButtonCallback(this, src, ~)
            this.callbackFunc(src);
        end
        
        function showContextMenuCallback(this, ~, ~)
            % Show contextmenu callback
            
            position = getpixelposition(this.actionButton, true);
            
            open(this.contextMenu,position(1),position(2));
        end
        
        function this = set.sizes(this, sz)
            % Setter of "sizes" property
            
            if ~isempty(sz)
                if length(sz) == 2
                    this.sizes = sz;
                else
                    str2 = 'The parametre "sizes" sent to "DropDownButtonComponent" must be an array of size 2.';
                    my_warndlg(str2, 1);
                end
            end
        end
        
        function setComponentsVisibleDependingSize(this)
            % set components visible or not depending their size
            
            visible = 'on';
            if this.sizes{1} == 0
                visible = 'off';
            end
            this.actionButton.set('Visible', visible);
            
            visible = 'on';
            if this.sizes{2} == 0
                visible = 'off';
            end
            this.contextMenuButton.set('Visible', visible);
        end
    end
end
