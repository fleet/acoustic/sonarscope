classdef FilterDataModeParametreComponent < handle & uiextras.Panel
% classdef FilterDataModeParametreComponent < handle & uix.Panel
    % Description
    %   Class FilterDataModeParametreComponent which is used to display a set of
    %   ClParametre instances in a component HBox
    %
    %   parent = FigUtils.createSScDialog('Position', centrageFig(600, 18));
    %   a = FilterDataModeParametreComponent(parent);
    %   filterDataParametre = a.getFilterDataParametre();
    %
    %
    % Authors : MHO
    % See also ParametreDialog ClParametre ParametreComponent Authors
    % ----------------------------------------------------------------------------
    
    properties
        parent % parent component
        paramStyle = ColorAndFontStyle; % - Style of uicontrol text of Parametre Components
    end
    
    properties (Access = private)
        mainHBox
        filterBoxList = uiextras.VBox.empty
        
        % Filter Data Parametre
        filterDataParametre = FilterDataParametre.empty
    end
    
    events
        OneParamValueChange % event if one param value change
    end
    
    methods
        function this = FilterDataModeParametreComponent(parent, varargin)
            % Constructor of the class
            
            % Call SuperConstructor
            this = this@uiextras.Panel('Parent', parent,  'Title', 'Filter Parameters');
%             this = this@uix.Panel('Parent', parent,  'Title', 'Filter Parameters');
            
            [varargin, filterDataParametre] = getPropertyValue(varargin, 'filterDataParametre', FilterDataParametre()); %#ok<ASGLU>
            
            %% Lecture des arguments
            this.parent   = parent;
            
            % Filter Data Parametre
            this.filterDataParametre   = filterDataParametre;
            
            % Create body
            this.createBody();
        end
        
        function handleValueChange(this, ~, ~)
            % Send one value change event
            this.notify('OneParamValueChange');
        end
        
        function filterDataParametre = getFilterDataParametre(this)
            filterDataParametre = this.filterDataParametre;
        end
        
    end
    
    methods (Access = protected)
        function createBody(this)
            % Create ParametreComponent List
            
            this.mainHBox = uiextras.HBox('Parent', this);
            
            %% Select Filter Combobox (popupmenu)
            selectfilterBox = uiextras.HBox('Parent', this.mainHBox);
            % Filter Name  Param
            selectedFilterText = uicontrol('Parent', selectfilterBox, ...
                'Style',  'text', ...
                'String', 'Filter');
            % add style to text
            ColorAndFontStyle.addStyle(selectedFilterText, this.paramStyle);
            
            %             filterPopupMenu = uicontrol('parent', selectfilterBox, ...
            uicontrol('parent', selectfilterBox, ...
                'Style',    'popupmenu', ...
                'String',   this.filterDataParametre.FilterNameList, ...
                'Value',    this.filterDataParametre.selectedFilterIndex, ...
                'Callback', @this.selectFilterCallback);
            
            %% ButterWorth
            this.filterBoxList(1) = uiextras.HBox('Parent', this.mainHBox);
            
            % Create Cut Off box for Combobox (popupmenu)
            cutOffFrequencyBox = uiextras.HBox('Parent', this.filterBoxList(1));
            % Cut Off Name  Param
            cutOffText = uicontrol('Parent', cutOffFrequencyBox, ...
                'Style',  'text', ...
                'String', this.filterDataParametre.butterworthWcParam.Name);
            % add style to text
            ColorAndFontStyle.addStyle(cutOffText, this.paramStyle);
            
            uicontrol('parent', cutOffFrequencyBox, ...
                'Style',    'popupmenu', ...
                'String',   this.filterDataParametre.WcNameList, ...
                'Value',    this.filterDataParametre.butterworthWcIndex, ...
                'Callback', @this.selectCutOffFrequencyCallback);
            
            % Create Order SpinnerParametreComponent component
            orderParamComponent = SpinnerParametreComponent(this.filterBoxList(1), 'param', this.filterDataParametre.butterworthOrderParam, 'sizes', [-1 -1 0 0], 'style', this.paramStyle);
            % Add a listener for Value change event
            addlistener(orderParamComponent, 'ValueChange', @this.handleValueChange);
            
            % Create Progressive box for CheckBox
            progressiveBox = uiextras.HBox('Parent', this.filterBoxList(1));
            % Progressive Name  Param
            ProgressiveText = uicontrol('Parent', progressiveBox, ...
                'Style',  'text', ...
                'String', 'Progressive');
            % add style to text
            ColorAndFontStyle.addStyle(ProgressiveText, this.paramStyle);
            
            uicontrol('parent', progressiveBox, ...
                'Style',    'checkbox', ...
                'Value',    this.filterDataParametre.butterworthProgressive, ...
                'Callback', @this.selectProgressiveCallback);
            progressiveBox.set('Sizes', [-3 -1]);
            
            uiextras.Empty('Parent', this.filterBoxList(1));
            
            this.filterBoxList(1).set('Sizes', [350 200 200 -1]);
            
            %% Gaussian
            this.filterBoxList(2) = uiextras.HBox('Parent', this.mainHBox);
            % Create Sigma ParametreComponent component
            sigmaParamComponent = SimpleParametreComponent(this.filterBoxList(2), 'param', this.filterDataParametre.gaussianSigmaParam, 'sizes', [-1 -1 0 0], 'style', this.paramStyle);
            % Add a listener for Value change event
            addlistener(sigmaParamComponent, 'ValueChange', @this.handleValueChange);
            uiextras.Empty('Parent', this.filterBoxList(2));
            this.filterBoxList(2).set('Sizes', [200 -1]);
            
            %% Polynome % Ajout JMA le 20/02/2024
            % TODO : ça fonctionne sans ça !!!
            %{
            this.filterBoxList(3) = uiextras.HBox('Parent', this.mainHBox);
            % Create PolynomeOrder ParametreComponent component
            polynomeOrderParamComponent = SimpleParametreComponent(this.filterBoxList(3), 'param', this.filterDataParametre.polynomeOrderParam, 'sizes', [-1 -1 0 0], 'style', this.paramStyle);
            % Add a listener for Value change event
            addlistener(polynomeOrderParamComponent, 'ValueChange', @this.handleValueChange);
            uiextras.Empty('Parent', this.filterBoxList(3));
            this.filterBoxList(3).set('Sizes', [200 -1]);
            %}
            
            % update size
            this.updateFilterComponent();
        end
        
        function selectFilterCallback(this,src,~)
            this.filterDataParametre.selectedFilterIndex = src.Value;
            this.updateFilterComponent();
            
            % launch event
            this.handleValueChange();
        end
        
        function updateFilterComponent(this)
            switch this.filterDataParametre.selectedFilterIndex
                case 1 % Butterworth
                    this.mainHBox.set('Sizes', [200 -1 0]);
                case 2 % Gaussian
                    this.mainHBox.set('Sizes', [200 0 -1]);
                case 3 % Polynome
                    this.mainHBox.set('Sizes', [200 0 -1]);
            end
        end
        
        function selectCutOffFrequencyCallback(this, src, ~)
            % Select Cut Off Frequency Callback
            this.filterDataParametre.butterworthWcIndex = src.Value;
            [wcValue,ok] = str2num( this.filterDataParametre.WcNameList(src.Value));
            if (ok)
                this.filterDataParametre.butterworthWcParam.Value = wcValue;
            else
                this.filterDataParametre.butterworthWcParam.Value = 0;
            end
            
            % launch event
            this.handleValueChange();
        end
        
        function selectProgressiveCallback(this, src, ~)
            % Select Progressive option Callback
            this.filterDataParametre.butterworthProgressive = src.Value;
            
            % launch event
            this.handleValueChange();
        end
        
    end
end
