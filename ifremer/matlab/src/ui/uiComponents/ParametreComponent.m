classdef ParametreComponent < handle & uiextras.HBox
    % Description
    %   Component ParametreComponent who can edit value, min/max... of a ClParametre instance
    %
    % Syntax
    %   a = ParametreComponent(...)
    %
    % ParametreComponent properties :
    %   parent  - parent component
    %   param   - ClParametre instance
    %   sizes   - width array of chlid components
    %
    % Output Arguments
    %   a : One ParametreComponent instance
    %
    % Examples
    %   param  = ClParametre('Name', 'Temperature', 'Unit', 'abd', 'MinValue', -10, 'MaxValue', 50, 'Value', 20);
    %   parent = FigUtils.createSScDialog('Position', centrageFig(600, 18));
    %   style  = ColorAndFontStyle('BackgroundColor', [0.7 0.7 1], 'ForegroundColor', 'k', 'FontWeight', 'bold', 'FontSize', 13, 'FontAngle', 'italic'); %option
    %   a = ParametreComponent(parent, 'param', param, 'style', style);
    %
    % Authors : MHO
    % See also ClParametre ParametreDialog Authors
    % ----------------------------------------------------------------------------
    properties
        parent % parent component
        param  % ClParametre instance
        sizes = [25 -2 -1 -2 -1 -1 -1 25]; % width array of chlid components
        style = ColorAndFontStyle;         % - Style of uicontrol text
    end
    
    properties (Access = private)
        editable = 'on'; % text controls editable by default
        
        % uicontrols
        enableCheck
        nameText
        minValueEdit
        valueSlider
        maxValueEdit
        valueEdit
        unitText
        helpButton
    end
    
    events
        ValueChange % event if value of param change
    end
    
    methods
        function this = ParametreComponent(parent, varargin)
            % Constructor of the class
            
            % Call SuperConstructor
            this = this@uiextras.HBox('Parent', parent);
            
            %% Lecture des arguments
            this.parent   = parent;
            [varargin, this.param] = getPropertyValue(varargin, 'param', this.param);
            [varargin, this.sizes] = getPropertyValue(varargin, 'sizes', this.sizes);
            [varargin, this.style] = getPropertyValue(varargin, 'style', this.style);
            [varargin, this.editable] = getPropertyValue(varargin, 'editable', this.editable);
            
            if isempty(this.param)
                return
            end
            
            % Check if all the arguments have been interpretated
            if checkIfVararginIsEmpty(varargin)
                return
            end
            
            % Enable Param checkbox
            this.enableCheck = uicontrol('parent', this, ...
                'Style',    'checkbox', ...
                'Value',    this.param.Enable, ...
                'Tooltip', 'Freeze the parameter', ...
                'Callback', @this.enableCallback);
            
            % Name  Param
            this.nameText = uicontrol('Parent', this, ...
                'Style',   'text', ...
                'String',  this.param.Name);
            % add style to text
            ColorAndFontStyle.addStyle(this.nameText, this.style);
            % set min max value in tool tip if not editable min max value
            if (this.sizes(3) == 0) && (this.sizes(5)== 0)
                this.nameText.set('TooltipString', sprintf('MinValue : %s\nMaxValue : %s', num2str(this.param.MinValue),  num2str(this.param.MaxValue)));
            end
            %             ColorAndFontStyle.addStyle(nameText, style);
            
            % MinValue Param
            this.minValueEdit = uicontrol('Parent', this, ...
                'Style',    'edit', ...
                'String',   this.param.MinValue, ...
                'Tooltip',  'Minimum value', ...
                'Callback', @this.setMinValue);
            
            % Slider Param
            this.valueSlider =uicontrol('Parent', this, ...
                'Style',    'slider', ...
                'Min',      this.param.MinValue, ...
                'Max',      this.param.MaxValue, ...
                'Value',    this.param.Value, ...
                'Callback', @this.setValueSlider);
            if strcmp(this.param.Format, '%d') && (this.param.MaxValue > this.param.MinValue)
                this.valueSlider.set('SliderStep', [1 1] / (this.param.MaxValue-this.param.MinValue));
            end
            
            % MaxValue Param
            this.maxValueEdit = uicontrol('Parent', this, ...
                'Style',    'edit', ...
                'String',   this.param.MaxValue, ...
                'Tooltip',  'Maximum value', ...
                'Callback', @this.setMaxValue);
            
            % Value Param
            this.valueEdit = uicontrol('Parent', this, ...
                'Style',   'edit', ...
                'String',   this.param.Value, ...
                'Tooltip',  'Value of the parameter', ...
                'Callback', @this.setValue);
            
            % Unit Param
            this.unitText = uicontrol('Parent', this, ...
                'Style',  'text', ...
                'Tooltip', 'Unit', ...
                'String',  this.param.Unit);
            if isempty(this.param.Unit)
                this.sizes(7) = 0;
            end
            
            % Help Param
            this.helpButton = UiUtils.createHelpButton(this, this.param.Help);
            if isempty(this.param.Help)
                this.sizes(8) = 0;
            end
            
            % set sizes of uicontrols
            if ~isempty(this.sizes)
                this.set('Sizes', this.sizes);
            end
            
            % set component editable or not
            this.setEditable(this.editable);
            
            % set components enabled or diabled depending param.Enable
            if this.param.Enable
                enable = 'on';
            else
                enable = 'off';
            end
            this.setEnable(enable);
            
            
            % set components visible or not depending size
            this.setComponentsVisibleDependingSize();
            
            % set Padding & Spacing 1px
            this.set('Padding', 1);
            this.set('Spacing',1);
            
        end
        
        
        function updateComponent(this)
            % Update the component MinValue
            this.minValueEdit.set('String', this.param.MinValue);
            % Update the component slider
            this.valueSlider.set('Min', this.param.MinValue, 'Max', this.param.MaxValue, 'Value', this.param.Value);
            % Update the component MaxValue
            this.maxValueEdit.set('String', this.param.MaxValue);
            % Update the component value
            this.valueEdit.set('String', this.param.Value);
        end
        
        function enableCallback(this, src, ~)
            % Callback enable qui enable les uicontrols du composant
            
            % enable param
            this.param.Enable = src.Value;
            
            % uicontrols enable status
            if src.Value == 1
                enable = 'on';
            else
                enable = 'off';
            end
            
            this.setEnable(enable);
        end
        
        function setEnable(this, enableStr)
            % set components enabled or diabled depending param.Enable
            
            if (strcmp(enableStr, 'on'))
                enableInt = 1;
            else
                enableInt = 0;
            end
            
            
            if strcmp(this.editable, 'on')
                editableInt = 1;
            else
                editableInt = 0;
            end
            
            if editableInt && enableInt
                enable = 'on';
            else
                enable = 'off';
            end
            
            this.nameText.set(    'Enable', enableStr);
            this.minValueEdit.set('Enable', enable);
            this.valueSlider.set( 'Enable', enable);
            this.maxValueEdit.set('Enable', enable);
            this.valueEdit.set(   'Enable', enable);
            this.unitText.set(    'Enable', enableStr);
        end
        
        function setEditable(this, editable)
            % set component editable or not
            
            if (strcmp(editable, 'on'))
                editableInt = 1;
            else
                editableInt = 0;
            end
            
            if editableInt && this.param.Enable
                enable = 'on';
            else
                enable = 'off';
            end
            
            this.minValueEdit.set('Enable', enable);
            this.valueSlider.set( 'Enable', enable);
            this.maxValueEdit.set('Enable', enable);
            this.valueEdit.set(   'Enable', enable);
        end
        
        function setMinValue(this, src, ~)
            % Callback set MinValue
            
            value = str2num(src.String); %#ok<ST2NM>
            this.param.MinValue = value; % ClParametre controls
            src.String = this.param.MinValue;
            this.valueSlider.Min = this.param.MinValue;
            
            if this.valueSlider.Value ~= this.param.Value % if value change
                this.valueSlider.Value = this.param.Value;
                this.valueEdit.String  = this.param.Value;
                % Send value change event
                notify(this, 'ValueChange');
            end
        end
        
        function setValueSlider(this, src, ~)
            % Callback set Value Slider
            
            this.param.Value = src.Value; % ClParametre controls
            src.Value = this.param.Value;
            this.valueEdit.String = num2str(this.param.Value);
            
            % Send value change event
            notify(this, 'ValueChange');
        end
        
        function setMaxValue(this, src, ~)
            % Callback set Max Value
            
            value = str2num(src.String); %#ok<ST2NM>
            this.param.MaxValue = value; % ClParametre controls
            src.String = this.param.MaxValue;
            this.valueSlider.Max = this.param.MaxValue;
            
            if this.valueSlider.Value ~= this.param.Value % if value change
                this.valueSlider.Value = this.param.Value;
                this.valueEdit.String  = this.param.Value;
                % Send value change event
                notify(this, 'ValueChange');
            end
        end
        
        function setValue(this, src, ~)
            % Callback set Value
            
            % try to parse string to num
            value = str2num(src.String); %#ok<ST2NM>
            this.param.Value       = value; % ClParametre controls
            src.String             = this.param.Value;
            this.valueSlider.Min   = this.param.MinValue;
            this.valueSlider.Value = this.param.Value;
            this.valueSlider.Max   = this.param.MaxValue;
            
            % Send value change event
            notify(this, 'ValueChange');
        end
        
        function setComponentsVisibleDependingSize(this)
            % set components visible or not depending their size
            
            visible = 'on';
            if this.sizes(1) == 0
                visible = 'off';
            end
            this.enableCheck.set('Visible', visible);
            
            visible = 'on';
            if this.sizes(2) == 0
                visible = 'off';
            end
            this.nameText.set('Visible', visible);
            
            visible = 'on';
            if this.sizes(3) == 0
                visible = 'off';
            end
            this.minValueEdit.set('Visible', visible);
            
            visible = 'on';
            if this.sizes(4) == 0
                visible = 'off';
            end
            this.valueSlider.set('Visible', visible);
            
            visible = 'on';
            if this.sizes(5) == 0
                visible = 'off';
            end
            this.maxValueEdit.set('Visible', visible);
            
            visible = 'on';
            if this.sizes(6) == 0
                visible = 'off';
            end
            this.valueEdit.set('Visible', visible);
            
            visible = 'on';
            if this.sizes(7) == 0
                visible = 'off';
            end
            this.unitText.set('Visible', visible);
            
            visible = 'on';
            if this.sizes(8) == 0
                visible = 'off';
            end
            this.helpButton.set('Visible', visible);
        end
        
        function this = set.sizes(this, sz) %#ok<MCHV2>
            % Setter of "sizes" property
            
            if ~isempty(sz)
                if length(sz) == 8
                    if sz(6) == 0
%                         str1 = 'Size(6) ne peut pas �tre �gal � 0 sinon, vous ne verrez pas la valeur du param�tre.';
                        str2 = 'Size(6) cannot be equal to 0, this would say that you do not want to see the value of the parametre.';
                        my_warndlg(str2, 1);
                    else
                        this.sizes = sz;
                    end
                else
%                     str1 = 'Le param�tre "sizes" transmis � "ParametreComponent" doit �tre de taille 8.';
                    str2 = 'The parametre "sizes" sent to "ParametreComponent" must be an array of size 8.';
                    my_warndlg(str2, 1);
                end
            end
        end
        
        function updateFullComponent(this)
            % Update the whole component. Usefull if ClParametre have changed
            
            this.enableCheck.set('Value', this.param.Enable);
            this.nameText.set('String', this.param.Name);
            this.updateComponentValue();
            this.unitText.set('String', this.param.Unit);
            this.helpButton.set('Callback', {@UiUtils.helpAction, this.param.Help});
        end
        
        function updateComponentValue(this)
            % Update the value of components
            this.minValueEdit.set('String', this.param.MinValue);
            this.valueSlider.set('Min',      this.param.MinValue, ...
                'Max',      this.param.MaxValue, ...
                'Value',    this.param.Value)
            this.maxValueEdit.set('String', this.param.MaxValue);
            this.valueEdit.set('String',    this.param.Value);
        end
        
        function updateSizes(this, sizes)
            % Update sizes property, sizes display, and component visiblity
            this.sizes = sizes;
            this.set('Sizes', this.sizes);
            this.setComponentsVisibleDependingSize();
        end
    end
end
