classdef ParametreListComponent < handle & uiextras.HBox
    % Description
    %   Component ParametreListComponent : a component for a list of
    %   ClParametre
    %
    % Syntax
    %   a = ParametreListComponent(...)
    %
    % ParametreComponent properties :
    %   parent  - parent component
    %   paramsList   - ClParametre list
    %   parentFigure   - the parent figure
    %
    % Output Arguments
    %   a : One ParametreListComponent instance
    %
    % Examples
    %   param  = ClParametre('Name', 'Temperature', 'Unit', 'abd', 'MinValue', -10, 'MaxValue', 50, 'Value', 20);
    %   parentFigure = FigUtils.createSScDialog('Position', centrageFig(600, 18));
    %   panel = uipanel('Parent', parentFigure, 'Title', 'Parameter List');
    %   a = ParametreListComponent(panel,  param, parentFigure);
    %
    % Authors : MHO
    % See also ClParametre ParametreDialog Authors
    % ----------------------------------------------------------------------------
 
    properties
        parent     % parent component
        paramsList % ClParametre List
        parametrecomponents = ParametreComponent.empty;
    end
    
    properties (Access = private)
        paramsVBox
        parametreScrollbar
        parametreIniHeight
    end
    
    events
        ValueChange % event if value of param change
    end
    
    methods
        function this = ParametreListComponent(parent, paramsList, parentFigure)
            % Constructor of the class

            % Call SuperConstructor
            this = this@uiextras.HBox('Parent', parent);
            
            %% Lecture des arguments
            this.parent   = parent;
            this.paramsList = paramsList;
            
            % Create Vertical Box for parametre
            this.paramsVBox = uiextras.VBox('Parent', this);
            
            % Create scrollbar
            this.parametreScrollbar = uicontrol('Parent',this, ...
                'Style','slider', 'Enable','off', ...
                'Units','pixels', ...
                'Min',-1, 'Max',0, 'Value',0, ...
                'Callback',@this.parametreScrollbarCallback);
            
            this.set('Sizes', [-1 20]);
            
            % Update parametres
            % For each parametre, create parametre component
            
            sizesArray = [];
            % Crate ParametreComponent
            for k=1:length(this.paramsList)
                this.parametrecomponents(k) = ParametreComponent(this.paramsVBox, 'param', this.paramsList(k));
                sizesArray(k,:) = this.parametrecomponents(k).sizes; %#ok<AGROW>
            end
            
            % Compute commun sizes of ParametreComponent
            for columnIndex=1:length(sizesArray(1,:)) % for each column
                
                maxValue = max(sizesArray(:,columnIndex)); % max of givin column
                minValue = min(sizesArray(:,columnIndex));% min of givin column
                
                if minValue < 0 % if min is under 0, its a relative size
                    communSizes(columnIndex) = minValue; %#ok<AGROW>
                else % else, its a pixel size
                    communSizes(columnIndex) = maxValue; %#ok<AGROW>
                end
                
            end
            
            for k=1:length(this.paramsList)
                this.parametrecomponents(k).updateSizes(communSizes);
            end
             
            this.computeSizeScrollBar();
            % Callcback parametreScrollbar
            this.parametreScrollbarCallback();
            
            parentFigure.set('ResizeFcn',@this.doResizeFcn);
            
            % Resize VBox
            this.paramsVBox.set('Sizes', repmat(25, 1, numel(this.paramsList)));
        end
        
        function doResizeFcn(this,~,~)
            this.computeSizeScrollBar();
            this.parametreScrollbarCallback();
        end
        
        function computeSizeScrollBar(this)
            p = this.paramsVBox.get('Position');
            this.parametreIniHeight = p(4);
            sizeParams = numel(this.paramsList)*25;
            
            scrollValue = this.parametreScrollbar.get('Value');
            difference=sizeParams - (p(4)-scrollValue-p(2));
            
            if difference > 0
                newScrollValue = scrollValue/this.parametreScrollbar.get('Min')*(-difference);
                
                this.parametreScrollbar.set('Min', -difference, 'Max',  0,  'Value', newScrollValue, 'SliderStep', [1/difference 10/difference], 'Enable','on');
            else
                this.parametreScrollbar.set( 'Value', 0, 'Enable','off');
            end
        end
        
        function parametreScrollbarCallback(this,~,~)
            % slider value
            scrollValue = get(this.parametreScrollbar,'Value');
            % update panel position
            p = get(this.paramsVBox, 'Position'); % panel current position
            %             set(this.paramsVBox, 'Position',[p(1) -scrollValue p(3) (p(4)+scrollValue)]);
            set(this.paramsVBox, 'Position',[p(1) -scrollValue p(3) this.parametreIniHeight-scrollValue]);
        end
    end
end
