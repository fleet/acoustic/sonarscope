classdef ParametreListUiComponent < handle
    % Description
    %   Component ParametreListComponent : a component for a list of
    %   ClParametre
    %
    % Syntax
    %   a = ParametreListComponent(...)
    %
    % ParametreComponent properties :
    %   parent  - parent component
    %   paramsList   - ClParametre list
    %   parentFigure   - the parent figure
    %
    % Output Arguments
    %   a : One ParametreListComponent instance
    %
    % Examples
    %
    %   param(1)  = ClParametre('Name', 'Temperature1', 'Unit', 'abd', 'MinValue', -10, 'MaxValue', 50, 'Value', 20);
    %   param(2)  = ClParametre('Name', 'Temperature2', 'Unit', 'abd', 'MinValue', -10, 'MaxValue', 50, 'Value', 20);
    %   param(3)  = ClParametre('Name', 'Temperature3', 'Unit', 'abd', 'MinValue', -10, 'MaxValue', 50, 'Value', 20);
    %   param(4)  = ClParametre('Name', 'Temperature4', 'Unit', 'abd', 'MinValue', -10, 'MaxValue', 50, 'Value', 20);
    %   parentFigure = FigUtils.createSScUiFigure();
    %   a = ParametreListUiComponent(parentFigure,  param);
    %
    % Authors : MHO
    % See also ClParametre ParametreDialog Authors
    % ----------------------------------------------------------------------------
    
    properties
        parent     % parent component
        gridlayout
        paramsList % ClParametre List
        parametrecomponents = ParametreUiComponent.empty;
    end
    
    properties (Access = private)
        parametreScrollbar
        parametreIniHeight
    end
    
    events
        ValueChange % event if value of param change
    end
    
    methods
        function this = ParametreListUiComponent(parent, paramsList)
            % Constructor of the class
            
            %% Lecture des arguments
            this.parent   = parent;
            this.paramsList = paramsList;
            
            % Create Gridlayout
            this.gridlayout =  uigridlayout(parent, 'Scrollable','on', 'ColumnSpacing', 1, 'RowSpacing', 1, 'Padding', 1);
            this.gridlayout.ColumnWidth = {'1x'};
            
            
            % Update parametres
            % For each parametre, create parametre component
            
            sizesArray = [];
            % Crate ParametreComponent
            for k=1:length(this.paramsList)
                this.parametrecomponents(k) = ParametreUiComponent(this.gridlayout, 'param', this.paramsList(k));
                sizesArray{k} = this.parametrecomponents(k).sizes; %#ok<AGROW>
                this.gridlayout.RowHeight{k} = 25; % Height of one row
            end
            
            % Compute commun sizes of ParametreComponent
            for columnIndex=1:length(sizesArray{1}) % for each column
                for paramIndex = 1:length(sizesArray)
                    
                    
                    
                    sizes = sizesArray{paramIndex};
                    size = sizes{columnIndex};
                    
                    
                    maxValue = 0;
                    if (isnumeric(size))
                        if (size > maxValue)
                            maxValue = size;
                        end
                    else % if not num, take the relative size (1x, 2x...)
                        maxValue = size;
                        break;
                    end
                    
                    
                    
                end
                
                communSizes{columnIndex} = maxValue; %#ok<AGROW>
            end
            
            for k=1:length(this.paramsList)
                this.parametrecomponents(k).updateSizes(communSizes);
            end
            
        end
        
    end
end
