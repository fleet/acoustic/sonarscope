classdef ParametreUiComponent < handle
    % Description
    %   Component ParametreComponent who can edit value, min/max... of a ClParametre instance
    %
    % Syntax
    %   a = ParametreComponent(...)
    %
    % ParametreComponent properties :
    %   parent  - parent component
    %   param   - ClParametre instance
    %   sizes   - width array of chlid components
    %
    % Output Arguments
    %   a : One ParametreComponent instance
    %
    % Examples
    %   param  = ClParametre('Name', 'Temperature', 'Unit', 'abd', 'MinValue', -10, 'MaxValue', 50, 'Value', 20);
    %   parent = FigUtils.createSScUiFigure();
    %   a = ParametreUiComponent(parent, 'param', param);
    %
    % Authors : MHO
    % See also ClParametre ParametreDialog Authors
    % ----------------------------------------------------------------------------

    properties
        parent % parent component
        layout
        param  % ClParametre instance
        sizes = {25, '2x', '1x', '2x', '1x', '1x', '1x', 25}; % width array of chlid components
        style = ColorAndFontStyle;         % - Style of uicontrol text
    end
    
    properties (Access = private)
        editable = 'on'; % text controls editable by default
        
        % uicontrols
        enableCheck
        nameLabel
        minValueEdit
        valueSlider
        maxValueEdit
        valueEdit
        unitText
        helpButton
    end
    
    events
        ValueChange % event if value of param change
    end
    
    methods
        function this = ParametreUiComponent(parent, varargin)
            % Constructor of the class
            
            % uigridlayout
            this.layout = uigridlayout(parent, [1 8], 'ColumnSpacing', 0, 'RowSpacing', 0, 'Padding', 0);
            this.layout.RowHeight = {25};
            this.layout.ColumnWidth = {25,'2x', '1x', '2x', '1x', '1x', '1x', 25};
            
            %% Lecture des arguments
            this.parent   = parent;
            [varargin, this.param] = getPropertyValue(varargin, 'param', this.param);
            [varargin, this.sizes] = getPropertyValue(varargin, 'sizes', this.sizes);
            [varargin, this.style] = getPropertyValue(varargin, 'style', this.style);
            [varargin, this.editable] = getPropertyValue(varargin, 'editable', this.editable);
            
            if isempty(this.param)
                return
            end
            
            % Check if all the arguments have been interpretated
            if checkIfVararginIsEmpty(varargin)
                return
            end
            
            % Enable Param checkbox
            this.enableCheck = uicheckbox(this.layout, ...
                'Text','', ...
                'Value',    this.param.Enable, ...
                'Tooltip', 'Freeze the parameter', ...
                'ValueChangedFcn', @this.enableCallback);
            
            % Name  Param
            this.nameLabel = uilabel(this.layout, ...
                'Text',  this.param.Name);
            
            % add style to text
            ColorAndFontStyle.addUiStyle(this.nameLabel, this.style);


            if this.param.MinValue == this.param.MaxValue
                this.param.MinValue = this.param.MinValue - 100*eps(this.param.MinValue);
                this.param.MaxValue = this.param.MaxValue + 100*eps(this.param.MaxValue);
            end


            % MinValue Param
            this.minValueEdit = uieditfield(this.layout, 'numeric',...
                'Value',   this.param.MinValue, ...
                'Tooltip',  'Minimum value', ...
                'ValueChangedFcn', @this.setMinValue);
            
            % Slider Param
            this.valueSlider = uislider(this.layout, ...
                'Limits',           [this.param.MinValue this.param.MaxValue], ...
                'Value',            this.param.Value, ...
                'ValueChangingFcn', @this.setValueSlider, ...
                'ValueChangedFcn',  @this.setValueSlider);
            
            % set min max value in tool tip if not editable min max value
            if ((isnumeric(this.sizes{3})  && this.sizes{3} == 0) && (isnumeric(this.sizes{5})  && this.sizes{5}== 0))
                this.valueSlider.set('Tooltip', sprintf('MinValue : %s\nMaxValue : %s', num2str(this.param.MinValue),  num2str(this.param.MaxValue)));
            end
            
            %             if strcmp(this.param.Format, '%d') && (this.param.MaxValue > this.param.MinValue)
            %                 this.valueSlider.set('SliderStep', [1 1] / (this.param.MaxValue-this.param.MinValue));
            %             end
            
            % MaxValue Param
            this.maxValueEdit = uieditfield(this.layout, 'numeric',...
                'Value',   this.param.MaxValue, ...
                'Tooltip',  'Maximum value', ...
                'ValueChangedFcn', @this.setMaxValue);
            
            % Value Param
            this.valueEdit = uieditfield(this.layout, 'numeric',...
                'Value',   this.param.Value, ...
                'Tooltip',  'Value of the parameter', ...
                'ValueChangedFcn', @this.setValue);
            
            % Unit Param
            this.unitText = uilabel(this.layout, ...
                'Tooltip', 'Unit', ...
                'Text',  this.param.Unit);
            
            if isempty(this.param.Unit)
                this.sizes{7} = 0;
            end
            
            % Help Param
            this.helpButton = UiUtils.createHelpUiButton(this.layout, this.param.Help);
            if isempty(this.param.Help)
                this.sizes{8} = 0;
            end
            
            % set sizes of uicontrols
            if ~isempty(this.sizes)
                this.layout.ColumnWidth= this.sizes;
            end
            
            % set component editable or not
            this.setEditable(this.editable);
            
            % set components enabled or diabled depending param.Enable
            if this.param.Enable
                enable = 'on';
            else
                enable = 'off';
            end
            this.setEnable(enable);
            
            % set components visible or not depending size
            this.setComponentsVisibleDependingSize();
        end
        
        
        function updateComponent(this)
            % Update the component MinValue
            this.minValueEdit.set('Value', this.param.MinValue);
            % Update the component slider
            this.valueSlider.set('Limits', [this.param.MinValue this.param.MaxValue], ...
                'Value',    this.param.Value);
            % Update the component MaxValue
            this.maxValueEdit.set('Value', this.param.MaxValue);
            % Update the component value
            this.valueEdit.set('Value', this.param.Value);
        end
        
        function enableCallback(this, src, ~)
            % Callback enable qui enable les uicontrols du composant
            
            % enable param
            this.param.Enable = src.Value;
            
            % uicontrols enable status
            if src.Value == 1
                enable = 'on';
            else
                enable = 'off';
            end
            
            this.setEnable(enable);
        end
        
        function setEnable(this, enableStr)
            % set components enabled or diabled depending param.Enable
            
            if (strcmp(enableStr, 'on'))
                enableInt = 1;
            else
                enableInt = 0;
            end
            
            if strcmp(this.editable, 'on')
                editableInt = 1;
            else
                editableInt = 0;
            end
            
            if editableInt && enableInt
                enable = 'on';
            else
                enable = 'off';
            end
            
            this.nameLabel.set(    'Enable', enableStr);
            this.minValueEdit.set('Enable', enable);
            this.valueSlider.set( 'Enable', enable);
            this.maxValueEdit.set('Enable', enable);
            this.valueEdit.set(   'Enable', enable);
            this.unitText.set(    'Enable', enableStr);
        end
        
        function setEditable(this, editable)
            % set component editable or not
            
            if strcmp(editable, 'on')
                editableInt = 1;
            else
                editableInt = 0;
            end
            
            if editableInt && this.param.Enable
                enable = 'on';
            else
                enable = 'off';
            end
            
            this.minValueEdit.set('Enable', enable);
            this.valueSlider.set( 'Enable', enable);
            this.maxValueEdit.set('Enable', enable);
            this.valueEdit.set(   'Enable', enable);
        end
        
        function setMinValue(this, src, ~)
            % Callback set MinValue
            
            value = src.Value;
            this.param.MinValue = value; % ClParametre controls
            src.Value = this.param.MinValue;

            % Ajout JMA le 22/09/2022 mais pas suffisant
            if src.Value  >= this.valueSlider.Limits(2) 
                return
            end

            this.valueSlider.Limits(1) = this.param.MinValue;
            
            if this.valueSlider.Value ~= this.param.Value % if value change
                this.valueSlider.Value = this.param.Value;
                this.valueEdit.Value  = this.param.Value;
                % Send value change event
                notify(this, 'ValueChange');
            end
        end
        
        function setValueSlider(this, ~, event)
            % Callback set Value Slider
            
            this.param.Value = event.Value; % ClParametre controls
            this.valueEdit.Value = this.param.Value;
            
            % Send value change event
            notify(this, 'ValueChange');
        end
        
        function setMaxValue(this, src, ~)
            % Callback set Max Value
            
            value = src.Value;
            this.param.MaxValue = value; % ClParametre controls
            src.Value = this.param.MaxValue;

            % Ajout JMA le 22/09/2022 mais pas suffisant
            if src.Value  <= this.valueSlider.Limits(1) 
                return
            end

            this.valueSlider.Limits(2) = this.param.MaxValue;
            if this.valueSlider.Value ~= this.param.Value % if value change
                this.valueSlider.Value = this.param.Value;
                this.valueEdit.Value   = this.param.Value;
                % Send value change event
                notify(this, 'ValueChange');
            end
        end
        
        function setValue(this, src, ~)
            % Callback set Value
            
            value = src.Value;
            this.param.Value = value; % ClParametre controls
            src.Value = this.param.Value;
            this.valueSlider.Limits(1) = this.param.MinValue;
            this.valueSlider.Value = this.param.Value;
            this.valueSlider.Limits(2) = this.param.MaxValue;
            
            % Send value change event
            notify(this, 'ValueChange');
        end
        
        function setComponentsVisibleDependingSize(this)
            % set components visible or not depending their size
            
            visible = 'on';
            if isnumeric(this.sizes{1}) && this.sizes{1} == 0
                visible = 'off';
            end
            this.enableCheck.set('Visible', visible);
            
            visible = 'on';
            if isnumeric(this.sizes{2}) && this.sizes{2} == 0
                visible = 'off';
            end
            this.nameLabel.set('Visible', visible);
            
            visible = 'on';
            if isnumeric(this.sizes{3}) && this.sizes{3} == 0
                visible = 'off';
            end
            this.minValueEdit.set('Visible', visible);
            
            visible = 'on';
            if isnumeric(this.sizes{4}) && this.sizes{4} == 0
                visible = 'off';
            end
            this.valueSlider.set('Visible', visible);
            
            visible = 'on';
            if isnumeric(this.sizes{5}) && this.sizes{5} == 0
                visible = 'off';
            end
            this.maxValueEdit.set('Visible', visible);
            
            visible = 'on';
            if isnumeric(this.sizes{6}) && this.sizes{6} == 0
                visible = 'off';
            end
            this.valueEdit.set('Visible', visible);
            
            visible = 'on';
            if isnumeric(this.sizes{7}) && this.sizes{7} == 0
                visible = 'off';
            end
            this.unitText.set('Visible', visible);
            
            visible = 'on';
            if isnumeric(this.sizes{8}) && this.sizes{8} == 0
                visible = 'off';
            end
            this.helpButton.set('Visible', visible);
        end
        
        function this = set.sizes(this, sz) %#ok<MCHV2>
            % Setter of "sizes" property
            
            if ~isempty(sz)
                if length(sz) == 8
                    if sz{6} == 0
                        %                         str1 = 'Size(6) ne peut pas être égal à 0 sinon, vous ne verrez pas la valeur du paramètre.';
                        str2 = 'Size{6} cannot be equal to 0, this would say that you do not want to see the value of the parametre.';
                        my_warndlg(str2, 1);
                    else
                        this.sizes = sz;
                    end
                else
                    %                     str1 = 'Le paramètre "sizes" transmis à "ParametreComponent" doit être de taille 8.';
                    str2 = 'The parametre "sizes" sent to "ParametreComponent" must be an array of size 8.';
                    my_warndlg(str2, 1);
                end
            end
        end
        
        function updateFullComponent(this)
            % Update the whole component. Usefull if ClParametre have changed
            
            this.enableCheck.set('Value', this.param.Enable);
            this.nameLabel.set('Text', this.param.Name);
            this.updateComponentValue();
            this.unitText.set('Text', this.param.Unit);
            this.helpButton.set('Callback', {@UiUtils.helpAction, this.param.Help});
        end
        
        function updateComponentValue(this)
            % Update the value of components
            this.minValueEdit.set('Value', this.param.MinValue);
            this.valueSlider.set('Limits', [this.param.MinValue this.param.MaxValue], ...
                'Value', this.param.Value);
            
            this.maxValueEdit.set('Value', this.param.MaxValue);
            this.valueEdit.set('Value',    this.param.Value);
        end
        
        function updateSizes(this, sizes)
            % Update sizes property, sizes display, and component visiblity
            this.sizes = sizes;
            %             this.set('sizes', this.sizes);
            this.layout.ColumnWidth = this.sizes;
            this.setComponentsVisibleDependingSize();
        end
    end
end
