classdef RadioButtonsComponent < handle & uiextras.Grid
    % Description
    %   Component RadioButtonsComponent who can edit value of a ClParametre
    %
    % Syntax
    %   a = RadioButtonsComponent(...)
    %
    % RadioButtonsComponent properties :
    %   parent  - parent component
    %   sizes   - width array of chlid components
    %
    % Output Arguments
    %   a : One RadioButtonsComponent instance
    %
    % Examples
    %   helpHtml = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
    %   parent = FigUtils.createSScDialog('Position', centrageFig(600, 18),'WindowStyle', 'normal');
    %   a = RadioButtonsComponent(parent, 'orientation', RadioButtonsComponent.Orientation{1}, 'title', 'Radio Button Component', 'titleButtons', {'button1', 'button2'}, 'helpHtml', helpHtml);
    %
    %   parent = FigUtils.createSScDialog('Position', centrageFig(600, 18));
    %   a = RadioButtonsComponent(parent, 'orientation', RadioButtonsComponent.Orientation{1}, 'title', 'Radio Button Component', 'titleButtons', {'button1'; 'button2'; 'Hello'; 'World'}, 'helpHtml', helpHtml);
    %
    %   parent = FigUtils.createSScDialog('Position', centrageFig(600, 18));
    %   a = RadioButtonsComponent(parent, 'orientation', RadioButtonsComponent.Orientation{1}, 'title', 'Radio Button Component', 'titleButtons', {'button1'; 'button2'; 'Hello'; 'World'}, 'Init', 3, 'helpHtml', helpHtml);
    %
    % % callback :
    % %  a = RadioButtonsComponent(parent, 'orientation', RadioButtonsComponent.Orientation{1}, 'title', 'Radio Button Component', 'titleButtons', {'button1', 'button2'}, 'callbackFunc',@this.radioButtonsComponentCallback);
    % %function radioButtonsComponentCallback(this, src, ~)
    % %        disp(src.String);
    % %    end
    %
    % Authors : MHO
    % See also ParametreComponent ClParametre ParametreDialog Authors
    % ----------------------------------------------------------------------------
    
    properties (Constant)
        Orientation  = {'horizontal'; 'vertical'}; % Orientation
    end
    
    properties
        parent                 % parent component
        selectedRadioButtonStr % String of selected Radio buton String
    end
    
    properties (Access = protected)
        title
        titleButtons
        helpHtml
        callbackFunc
        sizes       % width array of chlid components
        iconPathList % List of path of icon if needed
        style       = ColorAndFontStyle; % Style of uicontrol text
        orientation = RadioButtonsComponent.Orientation{1};
        editable    = 'on'; % text controls editable by default
        
        % uicontrols
        nameText
        radioButtons = matlab.ui.control.UIControl.empty;
        helpButton
    end
    
    events
        RadioChange % event if radiobutton change
    end
    
    
    methods
        function this = RadioButtonsComponent(parent, varargin)
            % Constructor of the class
            
            % Call SuperConstructor
            this = this@uiextras.Grid('Parent', parent);
            
            %% Lecture des arguments
            
            this.parent = parent;
            
            [varargin, this.title]               = getPropertyValue(varargin, 'title',        this.title);
            [varargin, this.orientation]         = getPropertyValue(varargin, 'orientation',  this.orientation);
            [varargin, this.titleButtons]        = getPropertyValue(varargin, 'titleButtons', this.titleButtons);
            [varargin, this.helpHtml]            = getPropertyValue(varargin, 'helpHtml',     this.helpHtml);
            [varargin, this.callbackFunc]        = getPropertyValue(varargin, 'callbackFunc', this.callbackFunc);
            [varargin, this.style]               = getPropertyValue(varargin, 'style',        this.style);
            [varargin, this.editable]            = getPropertyValue(varargin, 'editable',     this.editable);
            [varargin, this.iconPathList]        = getPropertyValue(varargin, 'iconPathList', this.iconPathList);
            [varargin, selectedRadioButtonIndex] = getPropertyValue(varargin, 'Init',         1);
            [varargin, this.sizes]               = getPropertyValue(varargin, 'sizes',        this.sizes); %#ok<ASGLU>
            
            if isempty(selectedRadioButtonIndex)
                selectedRadioButtonIndex = 1;
            end
            
            % Name  Param
            this.nameText = uicontrol('Parent', this, ...
                'Style',               'text', ...
                'HorizontalAlignment', 'left',...
                'String',              this.title);
            % add style to text
            ColorAndFontStyle.addStyle(this.nameText, this.style);
            
            
            if ~isempty(this.iconPathList)
                % create toggle buttons with icons
                for k=1:numel(this.iconPathList)
                    icon = iconizeFic(getNomFicDatabase(this.iconPathList{k}));
                    this.radioButtons(k) = uicontrol('parent', this, ...
                        'Style',    'toggle', ...
                        'Value',    k == selectedRadioButtonIndex, ...
                        'CData',    icon, ...
                        'Tag',      num2str(k), ...
                        'Callback', @this.buttonCallback);
                end
            else
                % create simple radio
                for k=1:numel(this.titleButtons)
                    this.radioButtons(k) = uicontrol('parent', this, ...
                        'Style',    'radio', ...
                        'Value',    (k == selectedRadioButtonIndex), ...
                        'String',   this.titleButtons{k}, ...
                        'Tag',      num2str(k), ...
                        'Callback', @this.buttonCallback);
                end
            end
            
            this.selectedRadioButtonStr = this.radioButtons(selectedRadioButtonIndex).String;
            
            % Help Param
            this.helpButton = UiUtils.createHelpButton(this, this.helpHtml);
            
            % set sizes of uicontrols
            if isempty(this.sizes)
                this.sizes = [-2 repmat(-1, 1, numel(this.radioButtons)) 25];
            end
            
            if isempty(this.helpHtml)
                this.sizes(end) = 0;
            end
            % orientation & Sizes
            if strcmp(this.orientation, this.Orientation{1}) % horizontal
                this.set('ColumnSizes', this.sizes, 'RowSizes', -1 );
            elseif strcmp(this.orientation, this.Orientation{2}) % vertical
                this.set('ColumnSizes', -1, 'RowSizes', [repmat(-1, 1, numel(this.radioButtons)+1) 0] );
            end
            
            % set component editable or not
            this.setEditable(this.editable);
            
            % set components visible or not depending size
            this.setComponentsVisibleDependingSize();
            
            % set Padding & Spacing 1px
            this.set('Padding', 1);
            this.set('Spacing', 1);
        end
        
        
        function updateComponent(this, index)
            
            % Select / Unselect buttons
            for k=1:numel(this.radioButtons)
                if k == index
                    this.radioButtons(k).Value = 1;
                else
                    this.radioButtons(k).Value = 0;
                end
            end
        end
            
        
        function setComponentsVisibleDependingSize(this)
            % set components visible or not depending their size
            
            visible = 'on';
            if this.sizes(1) == 0
                visible = 'off';
            end
            this.nameText.set('Visible', visible);
            
            visible = 'on';
            for k=1:length(this.radioButtons)
                if this.sizes(k+1) == 0
                    visible = 'off';
                end
                this.radioButtons.set('Visible', visible);
            end
            
            visible = 'on';
            if this.sizes(k+2) == 0 || isempty(this.helpHtml)
                visible = 'off';
            end
            this.helpButton.set('Visible', visible);
        end
        
        
        function this = set.sizes(this, sz) %#ok<MCHV2>
            % Setter of "sizes" property
            
            if ~isempty(sz)
                
                if ~isempty(this.radioButtons) %#ok<MCSUP>
                    buttonsNumber = numel(this.radioButtons);%#ok<MCSUP>
                elseif ~isempty(this.iconPathList)%#ok<MCSUP>
                    buttonsNumber = numel(this.iconPathList);%#ok<MCSUP>
                elseif ~isempty(this.titleButtons)%#ok<MCSUP>
                    buttonsNumber = numel(this.titleButtons);%#ok<MCSUP>
                end
                
                if length(sz) == (2+buttonsNumber)
                    this.sizes = sz;
                else
%                     str1 = 'Le param�tre "sizes" transmis � "RadioButtonsComponent"  n''a pas la bonne taille.';
                    str2 = 'The parametre "sizes" sent to "RadioButtonsComponent" have the wrong size.';
                    my_warndlg(str2, 1);
                end
            end
        end
        
        
        function setEnable(this, enable)
            % set components enabled or diabled depending param.Enable
            
            this.nameText.set('Enable', enable);
            n = numel(this.radioButtons);
            for k=1:n
                this.radioButtons(k).set('Enable', enable);
            end
        end
        
        
        function setEditable(this, editable)
            % set component editable or not
            for k=1:numel(this.radioButtons)
                this.radioButtons(k).set('Enable', editable);
            end
        end
        
        
        function buttonCallback(this, src, ~)
            % Radio Buttons CallBack
            
            % Select button
            src.Value = 1;
            this.selectedRadioButtonStr = src.String;
            
            % Unselect other buttons
            n = numel(this.radioButtons);
            for k=1:n
                if this.radioButtons(k) ~= src
                    this.radioButtons(k).Value = 0;
                end
            end
            
            if ~isempty(this.callbackFunc)
                this.callbackFunc(src);
            end
            % Send radio change event
            this.notify('RadioChange');
        end
    end
end
