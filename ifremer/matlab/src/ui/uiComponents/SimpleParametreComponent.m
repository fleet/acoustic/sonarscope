classdef SimpleParametreComponent < handle & uiextras.HBox
    % Description
    %   Component SimpleParametreComponent who can edit value of a ClParametre
    %
    % Syntax
    %   a = SimpleParametreComponent(...)
    %
    % SimpleParametreComponent properties :
    %   parent - parent component
    %   param  - ClParametre instance
    %   sizes  - width array of chlid components
    %
    % Output Arguments
    %   a : One SimpleParametreComponent instance
    %
    % Examples
    %   param = ClParametre('Name', 'Temperature', 'Unit', 'deg', 'MinValue', -10, 'MaxValue', 50, 'Value', 20);
    %   param.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
    %   parent = FigUtils.createSScDialog('Position', centrageFig(600, 18));
    %   a = SimpleParametreComponent(parent, 'param', param);
    %
    % Authors : MHO
    % See also ParametreComponent ClParametre ParametreDialog Authors
    % ----------------------------------------------------------------------------
    
    properties
        parent                     % parent component
        param                      % ClParametre instance
        sizes = [-2 -1 -1 25]      % width array of chlid components
        style = ColorAndFontStyle; % Style of uicontrol text

    end
    
    properties (Access = private)
        editable = 'on'; % text controls editable by default
        
        % uicontrols
        nameText
        valueEdit
        unitText
        helpButton
    end
    
    events
        ValueChange % event if value of param change
    end
    
    methods
        function this = SimpleParametreComponent(parent, varargin)
            % Constructor of the class
            
            % Call SuperConstructor
            this = this@uiextras.HBox('Parent', parent);
            
            %% Lecture des arguments
            this.parent   = parent;
            [varargin, this.param]    = getPropertyValue(varargin, 'param',    this.param);
            [varargin, this.sizes]    = getPropertyValue(varargin, 'sizes',    this.sizes);
            [varargin, this.style]    = getPropertyValue(varargin, 'style',    this.style);
            [varargin, this.editable] = getPropertyValue(varargin, 'editable', this.editable); %#ok<ASGLU>
            
            if isempty(this.param)
                return
            end
            
            % Name  Param
            this.nameText = uicontrol('Parent', this, ...
                'Style',  'text', ...
                'String', this.param.Name);
            % add style to text
            ColorAndFontStyle.addStyle(this.nameText, this.style);
            % set min max value in tool tip
            this.nameText.set('TooltipString', sprintf('MinValue : %s\nMaxValue : %s', num2str(this.param.MinValue),  num2str(this.param.MaxValue)));
            
            
            % Value Param
            this.valueEdit = uicontrol('Parent', this, ...
                'Style',   'edit', ...
                'String',   this.param.Value, ...
                'Tooltip',  'Value of the parameter', ...
                'Callback', @this.setValue);
            
            % Unit Param
            this.unitText = uicontrol('Parent', this, ...
                'Style',  'text', ...
                'Tooltip', 'Unit', ...
                'String',  this.param.Unit);
            if isempty(this.param.Unit)
                this.sizes(3) = 0;
            end
            
            % Help Param
            this.helpButton = UiUtils.createHelpButton(this, this.param.Help);
            if isempty(this.param.Help)
                this.sizes(4) = 0;
            end
            
            % set sizes of uicontrols
            if ~isempty(this.sizes)
                this.set('Sizes', this.sizes);
            end
            
            % set components enabled or diabled depending param.Enable
            if this.param.Enable
                enable = 'on';
            else
                enable = 'off';
            end
            this.setEnable(enable);
            
            % set component editable or not
            this.setEditable(this.editable);
            
            % set components visible or not depending size
            this.setComponentsVisibleDependingSize();
            
            % set Padding & Spacing 1px
            this.set('Padding', 1);
            this.set('Spacing', 1);
        end
        
        function updateComponent(this)
            % Update the component value
              this.valueEdit.set('String', this.param.Value);
        end
        
        function setValue(this, src, ~)
            % Callback set Value
            
            % try to parse string to num
            value = str2num(src.String); %#ok<ST2NM>
            
            this.param.Value = value; %ClParametre controls
            src.String = this.param.Value;
            
            % Send value change event
            notify(this, 'ValueChange');
        end
        
        function this = set.sizes(this, sz) %#ok<MCHV2>
            % Setter of "sizes" property
            
            if ~isempty(sz)
                if length(sz) == 4
                    this.sizes = sz;
                else
%                     str1 = 'Le param�tre "sizes" transmis � "SimpleParametreComponent" doit �tre de taille 4.';
                    str2 = 'The parametre "sizes" sent to "SimpleParametreComponent" must be an array of size 4.';
                    my_warndlg(str2, 1);
                end
            end
        end
        
        
        function setEnable(this, enableStr)
            % set components enabled or diabled depending param.Enable
            
            if strcmp(enableStr, 'on')
                enableInt = 1;
            else
                enableInt = 0;
            end
            
            
            if strcmp(this.editable, 'on')
                editableInt = 1;
            else
                editableInt = 0;
            end
            
            if editableInt && enableInt
                enable = 'on';
            else
                enable = 'off';
            end
            
            this.nameText.set( 'Enable', enableStr);
            this.valueEdit.set('Enable', enable);
            this.unitText.set( 'Enable', enableStr);
        end
        
        function setEditable(this, editable)
            % set component editable or not
            
            if strcmp(editable, 'on')
                editableInt = 1;
            else
                editableInt = 0;
            end
            
            if editableInt && this.param.Enable
                enable = 'on';
            else
                enable = 'off';
            end
            
            this.valueEdit.set(   'Enable', enable);
        end
        
        function setComponentsVisibleDependingSize(this)
            % set components visible or not depending their size
            
            visible = 'on';
            if this.sizes(1) == 0
                visible = 'off';
            end
            this.nameText.set('Visible', visible);
            
            visible = 'on';
            if this.sizes(2) == 0
                visible = 'off';
            end
            this.valueEdit.set('Visible', visible);
            
            visible = 'on';
            if this.sizes(3) == 0 || isempty(this.param.Unit)
                visible = 'off';
            end
            this.unitText.set('Visible', visible);
            
            visible = 'on';
            if this.sizes(4) == 0|| isempty(this.param.Help)
                visible = 'off';
            end
            this.helpButton.set('Visible', visible);
        end
    end
end
