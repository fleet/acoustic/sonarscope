classdef SpinnerParametreComponent < handle & uiextras.HBox
    % Description
    %   Component SpinnerParametreComponent who can edit value of a ClParametre
    %
    % Syntax
    %   a = SpinnerParametreComponent(...)
    %
    % SpinnerParametreComponent properties :
    %   parent  - parent component
    %   param   - ClParametre instance
    %   sizes   - width array of chlid components
    %
    % Output Arguments
    %   a : One SpinnerParametreComponent instance
    %
    % Examples
    %   param = ClParametre('Name', 'Temperature', 'Unit', 'deg', 'MinValue', -10, 'MaxValue', 50, 'Value', 20.5);
    %   parent = FigUtils.createSScDialog('Position', centrageFig(600, 18));
    %   a = SpinnerParametreComponent(parent, 'param', param);
    %
    %   style2 = ColorAndFontStyle('BackgroundColor', [1 0.8 0.6], 'ForegroundColor', 'k', 'FontWeight', 'bold');
    %   parent = FigUtils.createSScDialog('Position', centrageFig(600, 18));
    %   param.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
    %   a = SpinnerParametreComponent(parent, 'param', param, 'style', style2);
    %
    % Authors : MHO
    % See also ClParametre ParametreComponent ParametreDialog Authors
    % ----------------------------------------------------------------------------
    
    properties
        parent                     % parent component
        param                      % ClParametre instance
        sizes = [-2 -1 -2 25]      % width array of child components
        style = ColorAndFontStyle; % Style of uicontrol text
    end
    
    properties (Access = private)
        editable = 'on'; % text controls editable by default
        
        % uicontrols
        nameText
        valueSpinner
        unitText
        helpButton
        
        % swing
        valueJSpinner
    end
    
    events
        ValueChange % event if value of param change
    end
    
    methods
        function this = SpinnerParametreComponent(parent, varargin)
            % Constructor of the class
            
            % Call SuperConstructor
            this = this@uiextras.HBox('Parent', parent);
            
            %% Lecture des arguments
            this.parent = parent;
            [varargin, this.param]    = getPropertyValue(varargin, 'param',    this.param);
            [varargin, this.sizes]    = getPropertyValue(varargin, 'sizes',    this.sizes);
            [varargin, this.style]    = getPropertyValue(varargin, 'style',    this.style);
            [varargin, this.editable] = getPropertyValue(varargin, 'editable', this.editable); %#ok<ASGLU>
            
            if isempty(this.param)
                return
            end
            
            % Name  Param
            this.nameText = uicontrol('Parent', this, ...
                'Style',  'text', ...
                'String', this.param.Name);
            % add style to text
            ColorAndFontStyle.addStyle(this.nameText, this.style);
            % set min max value in tool tip
            this.nameText.set('TooltipString', sprintf('MinValue : %s\nMaxValue : %s', num2str(this.param.MinValue), num2str(this.param.MaxValue)));
            
            % Spinner Value Param
            [this.valueSpinner, this.valueJSpinner] = uicomponent('parent', this, 'style', 'jSpinner');
            
            this.valueJSpinner.setValue(double(this.param.Value));
            this.valueJSpinner.set('StateChangedCallback', @this.stateChangedCallback);
            this.valueJSpinner.getEditor.getTextField.set('KeyPressedCallback', @this.keyPressedCallback);
            
            % Unit Param
            this.unitText = uicontrol('Parent', this, ...
                'Style',   'text', ...
                'Tooltip', 'Unit', ...
                'String',  this.param.Unit);
            if isempty(this.param.Unit)
                this.sizes(3) = 0;
            end
            
            % Help Param
            this.helpButton = UiUtils.createHelpButton(this, this.param.Help);
            if isempty(this.param.Help)
                this.sizes(4) = 0;
            end
            
            % set sizes of uicontrols
            if ~isempty(this.sizes)
                this.set('Sizes', this.sizes);
            end
            
            % set components enabled or diabled depending param.Enable
            if this.param.Enable
                enable = 'on';
            else
                enable = 'off';
            end
            this.setEnable(enable);
            
            % set component editable or not
            this.setEditable(this.editable);
            
            % set components visible or not depending size
            this.setComponentsVisibleDependingSize();
            
            % set Padding & Spacing 1px
            this.set('Padding', 1);
            this.set('Spacing',1);
        end
        
        function stateChangedCallback(this, src, event) %#ok<INUSD>
            this.param.Value = src.Value; % ClParametre controls
            if this.param.Value ~= src.Value % if ClParametre modify value
                src.Value = this.param.Value;
            end
            % Send value change event
            notify(this, 'ValueChange');
        end
        
        function keyPressedCallback(this, src, event)
            if isequal(event.getKeyCode, 10) % keycode10 == enter key
                res = str2num(this.valueJSpinner.getEditor.getTextField.getText); %#ok<ST2NM>
                if ~isempty(res)
                    this.param.Value = res; %ClParametre controls
                    src.text = num2str(this.param.Value);
                    this.valueJSpinner.Value = this.param.Value;
                end
            end
        end
        
        function this = set.sizes(this, sz) %#ok<MCHV2>
            % Setter of "sizes" property
            
            if ~isempty(sz)
                if length(sz) == 4
                    this.sizes = sz;
                else
%                     str1 = 'Le param�tre "sizes" transmis � "SpinnerParametreComponent" doit �tre de taille 4.';
                    str2 = 'The parametre "sizes" sent to "SpinnerParametreComponent" must be an array of size 4.';
                    my_warndlg(str2, 1);
                end
            end
        end
        
        function setEnable(this, enable)
            % set components enabled or diabled depending param.Enable
            
            this.nameText.set('Enable', enable);
            
            if strcmp(enable,'on')
                javaEnable = true;
            else
                javaEnable = false;
            end
            this.valueJSpinner.getEditor.getTextField.setEditable(javaEnable)
            this.unitText.set('Enable', enable);
        end
        
        function setEditable(this, editable)
            % set component editable or not
            
            if strcmp(editable,'on')
                javaEnable = true;
            else
                javaEnable = false;
            end
            this.valueJSpinner.getEditor.getTextField.setEditable(javaEnable)
        end
        
        function setComponentsVisibleDependingSize(this)
            % set components visible or not depending their size
            
            visible = 'on';
            if this.sizes(1) == 0
                visible = 'off';
            end
            this.nameText.set('Visible', visible);
            
            visible = 'on';
            if this.sizes(2) == 0
                visible = 'off';
            end
            this.valueSpinner.set('Visible', visible);
            
            visible = 'on';
            if this.sizes(3) == 0 || isempty(this.param.Unit)
                visible = 'off';
            end
            this.unitText.set('Visible', visible);
            
            visible = 'on';
            if (this.sizes(4) == 0) || isempty(this.param.Help)
                visible = 'off';
            end
            this.helpButton.set('Visible', visible);
        end
    end
end
