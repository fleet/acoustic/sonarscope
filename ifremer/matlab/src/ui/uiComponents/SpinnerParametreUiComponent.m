classdef SpinnerParametreUiComponent < handle
    % Description
    %   Component ParametreComponent who can edit value, min/max... of a ClParametre instance
    %
    % Syntax
    %   a = ParametreComponent(...)
    %
    % ParametreComponent properties :
    %   parent  - parent component
    %   param   - ClParametre instance
    %   sizes   - width array of chlid components
    %
    % Output Arguments
    %   a : One ParametreComponent instance
    %
    % Examples
    %   param  = ClParametre('Name', 'Temperature', 'Unit', 'abd', 'MinValue', -10, 'MaxValue', 50, 'Value', 20);
    %   parent = FigUtils.createSScUiFigure();
    %   a = SpinnerParametreUiComponent(parent, 'param', param);
    %
    % Authors : MHO
    % See also ClParametre ParametreDialog Authors
    % ----------------------------------------------------------------------------
    properties
        parent % parent component
        layout
        param  % ClParametre instance
        sizes = {'2x', '1x', '2x', 25}; % width array of chlid components
        style = ColorAndFontStyle;         % - Style of uicontrol text
    end
    
    properties (Access = private)
        editable = 'on'; % text controls editable by default
        
        
        % uicontrols
        %         enableCheck
        nameLabel
        valueSpinner
        unitText
        helpButton
    end
    
    events
        ValueChange % event if value of param change
    end
    
    methods
        function this = SpinnerParametreUiComponent(parent, varargin)
            % Constructor of the class
            
            % uigridlayout
            this.layout = uigridlayout(parent, [1 8], 'ColumnSpacing', 0, 'RowSpacing', 0, 'Padding', 0);
            this.layout.RowHeight = {25};
            this.layout.ColumnWidth = {25,'2x', '1x', '2x', '1x', '1x', '1x', 25};
            
            
            %% Lecture des arguments
            this.parent   = parent;
            [varargin, this.param] = getPropertyValue(varargin, 'param', this.param);
            [varargin, this.sizes] = getPropertyValue(varargin, 'sizes', this.sizes);
            [varargin, this.style] = getPropertyValue(varargin, 'style', this.style);
            [varargin, this.editable] = getPropertyValue(varargin, 'editable', this.editable);
            
            if isempty(this.param)
                return
            end
            
            
            % Check if all the arguments have been interpretated
            if checkIfVararginIsEmpty(varargin)
                return
            end
            
            % Name  Param
            this.nameLabel = uilabel(this.layout, ...
                'Text',  this.param.Name);
            
            % add style to text
            ColorAndFontStyle.addUiStyle(this.nameLabel, this.style);
            
            
            
            
            % Value Param
            this.valueSpinner = uispinner(this.layout,...
                'Limits', [this.param.MinValue this.param.MaxValue], ...
                'Value',   this.param.Value, ...
                'Tooltip',  'Value of the parameter', ...
                'ValueChangedFcn', @this.setValue);
            
            
            % set min max value in tool tip
            this.valueSpinner.set('Tooltip', sprintf('MinValue : %s\nMaxValue : %s', num2str(this.param.MinValue),  num2str(this.param.MaxValue)));
            
            
            
            % Unit Param
            this.unitText = uilabel(this.layout, ...
                'Tooltip', 'Unit', ...
                'Text',  this.param.Unit);
            
            if isempty(this.param.Unit)
                this.sizes{3} = 0;
            end
            
            % Help Param
            this.helpButton = UiUtils.createHelpUiButton(this.layout, this.param.Help);
            if isempty(this.param.Help)
                this.sizes{4} = 0;
            end
            
            % set sizes of uicontrols
            if ~isempty(this.sizes)
                this.layout.ColumnWidth= this.sizes;
            end
            
            % set component editable or not
            this.setEditable(this.editable);
            
            % set components enabled or diabled depending param.Enable
            if this.param.Enable
                enable = 'on';
            else
                enable = 'off';
            end
            this.setEnable(enable);
            
            
            % set components visible or not depending size
            this.setComponentsVisibleDependingSize();
        end
        
        
        function updateComponent(this)
            % Update the component spinner
            this.valueSpinner.set('Limits', [this.param.MinValue this.param.MaxValue], ...
                'Value',    this.param.Value);
        end
        
        function enableCallback(this, src, ~)
            % Callback enable qui enable les uicontrols du composant
            
            % enable param
            this.param.Enable = src.Value;
            
            % uicontrols enable status
            if src.Value == 1
                enable = 'on';
            else
                enable = 'off';
            end
            
            this.setEnable(enable);
        end
        
        function setEnable(this, enableStr)
            % set components enabled or diabled depending param.Enable
            
            if (strcmp(enableStr, 'on'))
                enableInt = 1;
            else
                enableInt = 0;
            end
            
            
            if strcmp(this.editable, 'on')
                editableInt = 1;
            else
                editableInt = 0;
            end
            
            if editableInt && enableInt
                enable = 'on';
            else
                enable = 'off';
            end
            
            this.nameLabel.set(    'Enable', enableStr);
            this.valueSpinner.set( 'Enable', enable);
            this.unitText.set(    'Enable', enableStr);
        end
        
        function setEditable(this, editable)
            % set component editable or not
            
            if (strcmp(editable, 'on'))
                editableInt = 1;
            else
                editableInt = 0;
            end
            
            if editableInt && this.param.Enable
                enable = 'on';
            else
                enable = 'off';
            end
            
            this.valueSpinner.set( 'Enable', enable);
        end
        
        function setMinValue(this, src, ~)
            % Callback set MinValue
            
            value = src.Value;
            this.param.MinValue = value; % ClParametre controls
            src.Value = this.param.MinValue;
            this.valueSpinner.Limits(1) = this.param.MinValue;
            
            if this.valueSpinner.Value ~= this.param.Value % if value change
                this.valueSpinner.Value = this.param.Value;
                % Send value change event
                notify(this, 'ValueChange');
            end
        end
        
        function setValueSlider(this, ~, event)
            % Callback set Value Slider
            
            this.param.Value = event.Value; % ClParametre controls
            this.valueEdit.Value = this.param.Value;
            
            % Send value change event
            notify(this, 'ValueChange');
        end
        
        function setMaxValue(this, src, ~)
            % Callback set Max Value
            
            value = src.Value;
            this.param.MaxValue = value; % ClParametre controls
            src.Value = this.param.MaxValue;
            this.valueSpinner.Limits(2) = this.param.MaxValue;
            
            if this.valueSpinner.Value ~= this.param.Value % if value change
                this.valueSpinner.Value = this.param.Value;
                % Send value change event
                notify(this, 'ValueChange');
            end
        end
        
        function setValue(this, src, ~)
            % Callback set Value
            
            value = src.Value;
            this.param.Value = value; % ClParametre controls
            src.Value = this.param.Value;
            this.valueSpinner.Limits(1) = this.param.MinValue;
            this.valueSpinner.Value = this.param.Value;
            this.valueSpinner.Limits(2) = this.param.MaxValue;
            
            % Send value change event
            notify(this, 'ValueChange');
        end
        
        function setComponentsVisibleDependingSize(this)
            % set components visible or not depending their size
            
            visible = 'on';
            if isnumeric(this.sizes{1}) && this.sizes{1} == 0
                visible = 'off';
            end
            this.nameLabel.set('Visible', visible);
            
            visible = 'on';
            if isnumeric(this.sizes{2}) && this.sizes{2} == 0
                visible = 'off';
            end
            this.valueSpinner.set('Visible', visible);
            
            visible = 'on';
            if isnumeric(this.sizes{3}) && this.sizes{3} == 0
                visible = 'off';
            end
            this.unitText.set('Visible', visible);
            
            visible = 'on';
            if isnumeric(this.sizes{4}) && this.sizes{4} == 0
                visible = 'off';
            end
            this.helpButton.set('Visible', visible);
        end
        
        function this = set.sizes(this, sz) %#ok<MCHV2>
            % Setter of "sizes" property
            
            if ~isempty(sz)
                if length(sz) == 4
                    this.sizes = sz;
                else
                    str2 = 'The parametre "sizes" sent to "SpinnerParametreComponent" must be an array of size 4.';
                    my_warndlg(str2, 1);
                end
            end
        end
        
        function updateFullComponent(this)
            % Update the whole component. Usefull if ClParametre have changed
            
            %             this.enableCheck.set('Value', this.param.Enable);
            this.nameLabel.set('Text', this.param.Name);
            this.updateComponentValue();
            this.unitText.set('Text', this.param.Unit);
            this.helpButton.set('Callback', {@UiUtils.helpAction, this.param.Help});
        end
        
        function updateComponentValue(this)
            % Update the value of components
            this.valueSpinner.set('Limits', [this.param.MinValue this.param.MaxValue], ...
                'Value',    this.param.Value);
        end
        
        function updateSizes(this, sizes)
            % Update sizes property, sizes display, and component visiblity
            this.sizes = sizes;
            %             this.set('sizes', this.sizes);
            this.layout.ColumnWidth = this.sizes;
            this.setComponentsVisibleDependingSize();
        end
    end
end
