classdef ToolBarModeComponent < RadioButtonsComponent
    % Description
    %   Component ToolBarModeComponent who can create axes toolbar box
    %
    % Examples
    %   parent = FigUtils.createSScDialog('Position', centrageFig(600, 25));
    %   a = ToolBarModeComponent(parent);
    
    
    methods
        function this = ToolBarModeComponent(varargin)
            
            % Superclass Constructor
            this = this@RadioButtonsComponent(varargin{:},...
                'orienation', RadioButtonsComponent.Orientation{1},...
                'title', 'Graphic Mode : ',...
                'iconPathList', {'tool_pointer.png' 'tool_zoom_in.png' 'tool_zoom_out.png' 'tool_hand.png'},...
                'sizes', [100 28 28 28 28 0], 'callbackFunc','');
            
            [varargin, callbackFunc] = getPropertyValue(varargin, 'callbackFunc', @this.switchToolBarModeCallback); %#ok<ASGLU>
            this.set('callbackFunc', callbackFunc);
        end
    end
    
    methods (Access = protected)
        function switchToolBarModeCallback(~, src, ~)
            %switch ToolBar Mod Callback
            
            modeIndex = str2num(src.Tag); %#ok<ST2NM>
            if (modeIndex == 1 ) % No mode
                zoom off;
                pan off;
            elseif (modeIndex == 2 ) % Zoom in
                h = zoom;
                set(h,'Enable','on', 'Motion', 'both', 'Direction', 'in');
                pan off;
            elseif (modeIndex == 3 ) % Zoom out
                h = zoom;
                set(h,'Enable','on', 'Motion', 'both', 'Direction', 'out');
                pan off;
            elseif (modeIndex == 4 ) % Pan
                zoom off;
                pan on;
            end
        end
    end
end
