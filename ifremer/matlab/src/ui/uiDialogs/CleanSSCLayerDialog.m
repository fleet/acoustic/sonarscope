classdef CleanSSCLayerDialog < handle & SimpleTitleDialog
    % Description
    %   Class CleanSSCLayerDialog
    %
    % Syntax
    %   a = CleanSSCLayerDialog(...)
    %
    % Output Arguments
    %   s : One CleanSSCLayerDialog instance
    %
    % Examples
    % % New object language call :
    %   a = CleanSSCLayerDialog;
    %   a.openDialog();
    %  [flag, typeInteraction, Value, CleanMask, ValToBeCleaned, widthToClean, heightToClean] = a.getTypeInteractionCleanData
    %
    % Authors : MHO
    % See also SimpleTitleDialog Authors
    % ----------------------------------------------------------------------------

    properties (Access = private)
        % data
        flag            = 1;
        typeOfSelection = 1;
        filteringValue  = str2double('NaN');
        value           = str2double('NaN');
        widthValue      = 5;
        heightValue     = 5;
        
        %uicontrols
        filterCheck
        filterEdit
        valueCheck
        valueEdit
    end
    
    methods
        function this = CleanSSCLayerDialog(varargin)
            % Superclass Constructor
            this = this@SimpleTitleDialog('WindowStyle', 'normal', 'waitAnswer', false, varargin{:});
            if isempty(this.Title)
                this.Title = 'Clean SonarScope Layer';
            end
        end
        
        function [flag, typeInteraction, Value, CleanMask, ValToBeCleaned, widthToClean, heightToClean] = getTypeInteractionCleanData(this)
            % Getter for data of this GUI
            flag            = this.flag;
            typeInteraction = this.typeOfSelection;
            Value           = this.value;
            CleanMask       = 1;
            ValToBeCleaned  = this.filteringValue;
            widthToClean    = this.widthValue;
            heightToClean   = this.heightValue;
        end
        
        function widthValue = getWidthValue(this)
            widthValue = this.widthValue;
        end
        
        function heightValue = getHeightValue(this)
            heightValue = this.heightValue;
        end
    end
    
    methods (Access = protected)
        function createActionButtons(this, verticalBox)
            % creates action horizontal box for save/quit buttons
            actionBox = uiextras.HBox('Parent', verticalBox);
            
            uiextras.Empty('Parent', actionBox);
            
            % Quit button
            uicontrol('parent', actionBox, ...
                'style',    'pushbutton', ...
                'string',   'Cancel', ...
                'Callback', @this.cancel, ...
                'tag',      'quitButton');
            
            % set Padding & Spacing 1px
            actionBox.set('Sizes',   [-1 100]);
            actionBox.set('Padding', 1);
            actionBox.set('Spacing', 5);
        end
        
        function [widthBody ,heightBody] = createBody(this, parent)
            % Create Body of CleanSSCLayerDialog
            
            verticalBox = uiextras.VBox('Parent', parent, 'Spacing', 10, 'Padding', 10);
            
            % Type of Selection
            typeOfSelectionBox = uiextras.HBox('Parent', verticalBox);
            
            uicontrol('Parent', typeOfSelectionBox, ...
                'Style',  'text', ...
                'String', 'Type of Selection : ');
            
            uicontrol('parent', typeOfSelectionBox, ...
                'Style',    'popupmenu', ...
                'String',   {'Rectangle', 'FreeHand', 'Polygone', 'Rubber', 'Point&Click'}, ...
                'Value',    this.typeOfSelection, ...
                'Callback', @this. typeOfSelectionCallback);
            typeOfSelectionBox.set('Sizes', [-2 -1]);
            
            % Filtering parameters
            strUS = 'Filtering & Data Value to Replace';
%             strFR = 'Filtrage & Valeur de donn�e � remplacer';
            filterPanel = uipanel('Parent', verticalBox,  'Title', strUS);
            
            filterGrid = uiextras.Grid( 'Parent', filterPanel, 'Padding', 5, 'Spacing', 5);
            
            % Filting Value
            strUS = 'Specific value to be cleaned :';
%             strFR = 'Valeur particuli�re :';
            this.filterCheck = uicontrol('Parent', filterGrid, ...
                'Style',    'check', ...
                'Value',    1, ...
                'String',   strUS, ...
                'Callback', @this. filterCheckCallback);
            
            strUS = 'The cleaned pixels will be set to :';
%             strFR = 'Valeur des pixels nettoy�s :';
            this.valueCheck = uicontrol('Parent', filterGrid, ...
                'Style',    'check', ...
                'Value',    1, ...
                'String',   strUS, ...
                'Callback', @this. valueCheckCallback);
            
            this.filterEdit = uicontrol('Parent', filterGrid, ...
                'Style',  'edit',...
                'String', this.filteringValue,...
                'Callback', @this. filterEditCallback);
            
            if strcmp(num2str(this.filteringValue), 'NaN')
                this.filterCheck.set('Value', 0);
                this.filterEdit.set('Enable', 'off', 'String', '');
            end
            
            % Value to be set
            this.valueEdit = uicontrol('Parent', filterGrid, ...
                'Style',  'edit',...
                'String', this.value,...
                'Callback', @this. valueEditCallback);
            
            if strcmp(num2str(this.value), 'NaN')
                this.valueCheck.set('Value', 0);
                this.valueEdit.set('Enable', 'off', 'String', 'NaN');
            end
            
            filterGrid.set('ColumnSizes', [-2 -1], 'RowSizes', [-1 -1] );
            
            % Axes parameters
            strUS = 'Size of axes';
%             strFR = 'Largeur des axis';
            axesPanel = uipanel('Parent', verticalBox,  'Title', strUS);
            
            axesGrid = uiextras.Grid('Parent', axesPanel, 'Padding', 5, 'Spacing', 5);
            
            % Width
            strUS = 'Height of the horizontal profil axis :';
%             strFR = 'Hauteur de l''axe de trac� du profil vertical : ';
            uicontrol('Parent', axesGrid, ...
                'Style',  'text', ...
                'String', strUS);
            
            % Height
            strUS = 'Width of the vertical profil axis : ';
%             strFR = 'Largeur de l''axe de trac� du profil horizontal : ';
            uicontrol('Parent', axesGrid, ...
                'Style',  'text', ...
                'String', strUS);
            
            %Width Spinner TODO MHO: pourquoi on n'utilise pas SpinnerParametreComponent
            [~, widthJSpinner] = uicomponent('parent', axesGrid, 'style', 'jSpinner');
            widthJSpinner.setValue(this.widthValue);
            widthJSpinner.set('StateChangedCallback', @this.widthChangedCallback);
            
            %Height Spinner
            [~, heightJSpinner] = uicomponent('parent', axesGrid, 'style', 'jSpinner');
            heightJSpinner.setValue(this.heightValue);
            heightJSpinner.set('StateChangedCallback', @this.heightChangedCallback);
            
            axesGrid.set('ColumnSizes', [-2 -1], 'RowSizes', [-1 -1] );
            
            verticalBox.set('Sizes', [30 75 75]);
            
            widthBody = 400;
            heightBody = 205;
        end
        
        %% Callbacks
        
        function typeOfSelectionCallback(this, src, ~)
            this.typeOfSelection = src.Value;
        end
        
        function filterCheckCallback(this, src, ~)
            if src.Value
                this.filterEdit.set('Enable', 'on');
            else
                this.filterEdit.set('Enable', 'off');
                this.filterEdit.set('String', '');
                
                this.filteringValue = str2double('NaN');
            end
        end
        
        function filterEditCallback(this, src, ~)
            this.filteringValue = str2double(src.String);
        end
        
        function valueCheckCallback(this, src, ~)
            if src.Value
                this.valueEdit.set('Enable', 'on', 'String', '0');
                this.value = str2double('0');
            else
                this.valueEdit.set('Enable', 'off', 'String', 'NaN');
                this.value = str2double('NaN');
            end
        end
        
        function valueEditCallback(this, src, ~)
            this.value = str2double(src.String);
        end
        
        function widthChangedCallback(this, src, ~)
            this.widthValue = src.Value;
        end
        
        function heightChangedCallback(this, src, ~)
            this.heightValue = src.Value;
        end
    end
end
