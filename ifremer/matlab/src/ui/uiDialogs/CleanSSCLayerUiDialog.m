classdef CleanSSCLayerUiDialog < handle & SimpleTitleUiDialog
    % Description
    %   Class CleanSSCLayerUiDialog
    %
    % Syntax
    %   a = CleanSSCLayerUiDialog(...)
    %
    % Output Arguments
    %   s : One CleanSSCLayerUiDialog instance
    %
    % Examples
    % % New object language call :
    %   a = CleanSSCLayerUiDialog;
    %   a.openDialog();
    %  [flag, typeInteraction, Value, CleanMask, ValToBeCleaned, widthToClean, heightToClean] = a.getTypeInteractionCleanData
    %
    % Authors : MHO
    % See also SimpleTitleUiDialog Authors
    % ----------------------------------------------------------------------------
    
    properties (Access = private)
        % data
        flag            = 1;
        typeOfSelection = 1;
        filteringValue  = 0;
        value           = 0;
        widthValue      = 5;
        heightValue     = 5;
        
        %uicontrols
        filterCheck
        filterEdit
        valueCheck
        valueEdit
    end
    
    methods
        function this = CleanSSCLayerUiDialog(varargin)
            % Superclass Constructor
            this = this@SimpleTitleUiDialog('waitAnswer', false, varargin{:});
            if isempty(this.Title)
                this.Title = 'Clean SonarScope Layer';
            end
        end
        
        function [flag, typeInteraction, Value, CleanMask, ValToBeCleaned, widthToClean, heightToClean] = getTypeInteractionCleanData(this)
            % Getter for data of this GUI
            flag            = this.flag;
            typeInteraction = this.typeOfSelection;
            Value           = this.value;
            CleanMask       = 1;
            ValToBeCleaned  = this.filteringValue;
            widthToClean    = this.widthValue;
            heightToClean   = this.heightValue;
        end
        
        function widthValue = getWidthValue(this)
            widthValue = this.widthValue;
        end
        
        function heightValue = getHeightValue(this)
            heightValue = this.heightValue;
        end
    end
    
    methods (Access = protected)
        function createActionButtons(this, verticalGridlayout)
            % creates action horizontal box for save/quit buttons
            actionGridLayout = uigridlayout(verticalGridlayout, 'ColumnSpacing', 5, 'RowSpacing', 5, 'Padding', 1);
            actionGridLayout.ColumnWidth = {'1x', 100};
            actionGridLayout.RowHeight = {'1x'};
            
            
            uilabel(actionGridLayout, 'Text',  '');
            
            % Quit button
            uibutton(actionGridLayout,'push',...
                'Text','Cancel',...
                'ButtonPushedFcn', @this.cancel, ...
                'tag',      'quitButton');
            
        end
        
        function [widthBody ,heightBody] = createBody(this, parent)
            % Create Body of CleanSSCLayerDialog
            verticalGridLayout = uigridlayout(parent, 'ColumnSpacing', 10, 'RowSpacing', 10, 'Padding', 10);
            verticalGridLayout.ColumnWidth = {'1x'};
            
            % Type of Selection
            typeOfSelectionLayout = uigridlayout(verticalGridLayout, 'ColumnSpacing', 0, 'RowSpacing', 0, 'Padding', 0);
            typeOfSelectionLayout.RowHeight = {'1x'};

            uilabel(typeOfSelectionLayout, 'Text', 'Type of Selection : ');

            typeOfSlectionList = {'Rectangle', 'FreeHand', 'Polygone', 'Rubber', 'Point&Click'};
            uidropdown(typeOfSelectionLayout, ...
                'Items',{'Rectangle', 'FreeHand', 'Polygone', 'Rubber', 'Point&Click'},...
                'Value',typeOfSlectionList{this.typeOfSelection}, ...
                'ValueChangedFcn', @this.typeOfSelectionCallback);

            typeOfSelectionLayout.ColumnWidth = {'2x', '1x'};
            
            % Filtering parameters
            filterPanel = uipanel('Parent', verticalGridLayout,  'Title', 'Filtering & Data Value to Replace');
            
            filterGridLayout = uigridlayout(filterPanel, 'ColumnSpacing', 5, 'RowSpacing', 5, 'Padding', 5);
            
            % Filting Value
            this.filterCheck = uicheckbox(filterGridLayout, ...
                'Text', 'Specific value to be cleaned :', ...
                'Value', 0, ...
                'ValueChangedFcn', @this.filterCheckCallback);

            this.filterEdit = uieditfield(filterGridLayout, 'numeric',...
                'Value',   this.filteringValue, ...
                'Enable', 'off', ...
                'ValueChangedFcn', @this.filterEditCallback);
        
            this.valueCheck = uicheckbox(filterGridLayout, ...
                'Text', 'The cleaned pixels will be set to :', ...
                'Value', 0, ...
                'ValueChangedFcn', @this.valueCheckCallback);
            
            % Value to be set
            this.valueEdit = uieditfield(filterGridLayout, 'numeric',...
                'Value',   this.value, ...
                          'Enable', 'off', ...
                'ValueChangedFcn', @this.valueEditCallback);

            filterGridLayout.ColumnWidth = {'2x', '1x'};
            filterGridLayout.RowHeight = {'1x', '1x'};
            
            % Axes parameters
            axesPanel = uipanel('Parent', verticalGridLayout,  'Title', 'Size of axes');

            axesGridLayout = uigridlayout(axesPanel, 'ColumnSpacing', 5, 'RowSpacing', 5, 'Padding', 5);
            
            % Width
            uilabel(axesGridLayout, 'Text', 'Height of the horizontal profil axis :');
            
            %Width Spinner 
                     uispinner(axesGridLayout,...
                'Value',   this.widthValue, ...
                'ValueChangedFcn', @this.widthChangedCallback);
            
            
            % Height
            uilabel(axesGridLayout, 'Text', 'Width of the vertical profil axis : ');
            
            %Height Spinner
             uispinner(axesGridLayout,...
                'Value',   this.heightValue, ...
                'ValueChangedFcn', @this.heightChangedCallback);

            axesGridLayout.ColumnWidth = {'2x', '1x'};
            axesGridLayout.RowHeight = {'1x', '1x'};
            
            verticalGridLayout.RowHeight = {30, 75, 75};
            
            widthBody = 400;
            heightBody = 205;
        end
        
        %% Callbacks
        
        function typeOfSelectionCallback(this, src, ~)
            this.typeOfSelection = src.Value;
        end
        
        function filterCheckCallback(this, src, ~)
            if src.Value
                this.filterEdit.set('Enable', 'on');
            else
                this.filterEdit.set('Enable', 'off');
                this.filterEdit.set('Value', 0);
                
                this.filteringValue = 0;
            end
        end
        
        function filterEditCallback(this, src, ~)
            this.filteringValue = src.Value;
        end
        
        function valueCheckCallback(this, src, ~)
            if src.Value
                this.valueEdit.set('Enable', 'on', 'Value', 0);
                this.value = 0;
            else
                this.valueEdit.set('Enable', 'off', 'Value', 0);
                this.value = 0;
            end
        end
        
        function valueEditCallback(this, src, ~)
            this.value = src.Value;
        end
        
        function widthChangedCallback(this, src, ~)
            this.widthValue = src.Value;
        end
        
        function heightChangedCallback(this, src, ~)
            this.heightValue = src.Value;
        end
    end
end
