classdef EditDataTableDialog < handle & SimpleTitleDialog
    % Description
    %   Class EditDataTableDialog wich is used to display a set of
    %   ClParametre instances in a figure and waits for Ok or Cancel
    %   buttons
    %
    %   a = EditDataTableDialog('Title', 'Demo of EditDataTableDialog');
    %   a.openDialog;
    %   % output :
    %     a.okPressedOut %(0 if not Ok pressed, 1 if Ok pressed)
    %
    %   a = EditDataTableDialog('hideActionButtons', true, 'windowStyle', 'normal', 'waitAnswer', false, 'Title', 'Demo of EditDataTableDialog');
    %   a.openDialog;
    %
    % Authors : MHO
    % See also ParametreDialog ClParametre ParametreComponent Authors
    % ----------------------------------------------------------------------------
    
    properties (Access = private)
        bodyHBox
        selectionLabel               = 'Selection';
        selectionContextMenu
        selectionContextMenuItemList
        displayedSelectionValue      = 1;
        selectionTitleUiControl
        dataUiTable
        selectedAxes
        selectedLineList
        editedAxes
        editedLine
        editedPointList
        cleanInterpolation           = false;
    end
    
    events
        DataTableValueChange % event if one data value change
    end
    
    methods
        function this = EditDataTableDialog(varargin)
            % Superclass Constructor
            this = this@SimpleTitleDialog(varargin{:});
            [varargin, this.cleanInterpolation] = getPropertyValue(varargin, 'cleanInterpolation', this.cleanInterpolation); %#ok<ASGLU>
        end
        
        function handleDataTableValueChange(this, ~, editDataEventData)
            % Send one value change event
            this.notify('DataTableValueChange', editDataEventData);
        end
        
        
        function refreshSelectedData(this, selectedAxes, selectedLineIndex, selectedLineList)
            
            this.selectedAxes = selectedAxes;
            this.selectedLineList = selectedLineList;
            
            %init the displayedSelectionValue
            this.displayedSelectionValue = selectedLineIndex;
            
            % Config selectionContextMenu
            this.configSelectionContextMenu();
            
            %Refresh Selection Title
            this.refreshSelectionTitle();
            
            % Refresh the dataTable
            this.refreshDataTable();
            
            % Refresh the line-axes
            this.refreshDataEditedAxes();
        end
        
        function refreshSelectionTitle(this)
            this.selectionTitleUiControl.String = this.selectedLineList(this.displayedSelectionValue).DisplayName;
        end
        
        function refreshDataTable(this)
            
            dataTable = table();
            
            % XData
            %check if XData is datetime
            if (isa(this.selectedLineList(this.displayedSelectionValue).XData, 'datetime'))
                dataTable.data =  char(this.selectedLineList(this.displayedSelectionValue).XData') ;
            else
                dataTable.data =  this.selectedLineList(this.displayedSelectionValue).XData' ;
            end
            xDataName = ['XData' num2str(this.displayedSelectionValue)];
            dataTable.Properties.VariableNames{end} = xDataName;
            
            % YData
            dataTable.data =  this.selectedLineList(this.displayedSelectionValue).YData' ;
            yDataName = ['YData' num2str(this.displayedSelectionValue)];
            dataTable.Properties.VariableNames{end} = yDataName;
            
            tc = table2cell(dataTable);
            this.dataUiTable.Data = tc;
            this.dataUiTable.ColumnName     = dataTable.Properties.VariableNames;
            this.dataUiTable.ColumnEditable = [false true];
            
            
            %resize column width if needed
            if (isa(this.selectedLineList(this.displayedSelectionValue).XData, 'datetime'))
                this.dataUiTable.ColumnWidth = {125 'auto'};
            else
                this.dataUiTable.ColumnWidth = {'auto' 'auto'};
            end
            % resize table box
            if this.dataUiTable.Extent(4) > this.dataUiTable.Position(4)
                width = this.dataUiTable.Extent(3)+15;
            else
                width = this.dataUiTable.Extent(3);
            end
            
            this.bodyHBox.set('Sizes', [width -1]);
        end
        
        function refreshDataEditedAxes(this)
            this.editedLine = plot(this.editedAxes, ...
                this.selectedLineList(this.displayedSelectionValue).XData, ...
                this.selectedLineList(this.displayedSelectionValue).YData, ...
                'Color',      this.selectedLineList(this.displayedSelectionValue).Color, ...
                'LineStyle',  this.selectedLineList(this.displayedSelectionValue).LineStyle, ...
                'LineWidth',  this.selectedLineList(this.displayedSelectionValue).LineWidth, ...
                'Marker',     this.selectedLineList(this.displayedSelectionValue).Marker, ...
                'MarkerSize', this.selectedLineList(this.displayedSelectionValue).MarkerSize, ...
                'UserData',   this.selectedLineList(this.displayedSelectionValue).UserData);
            grid(this.editedAxes,'on')
        end
        
        
    end
    
    methods (Access = protected)
        
        function [widthBody, heightBody] = createBody(this, parent)
            
            set(this.parentFigure, 'windowbuttondownfcn', @this.windowButtonDownFcnCallback);
            
            mainVBox = uiextras.VBox('Parent', parent);
            % Create Sample Action Box
            this.createTopBox(mainVBox);
            
            this.bodyHBox = uiextras.HBoxFlex('Parent', mainVBox, 'Spacing', 5);
            
            % DataTable
            dataTableBox = uiextras.VBox('Parent',this.bodyHBox);
            this.dataUiTable = uitable(dataTableBox);
            this.dataUiTable.CellEditCallback = @this.editCellCallback;
            this.dataUiTable.CellSelectionCallback = @this.selectCellCallback;
            %Axes
            axesBox = uiextras.VBox('Parent',this.bodyHBox);
            this.editedAxes = axes(axesBox);
            
            mainVBox.set('Sizes', [32 -1]);
            
            %% Size of dialog
            widthBody  = 1000;
            heightBody = 1000;
        end
        
        function createTopBox(this, parent)
            % Create samples action box
            
            topBox = uiextras.HBox('Parent', parent);
            
            %% Selection
            selectionBox = uiextras.HBox('Parent', topBox, 'Padding', 5, 'Spacing', 5);
            %Create context menu
            this.selectionContextMenu = uicontextmenu(this.parentFigure);
            this.selectionContextMenuItemList = matlab.ui.container.Menu.empty;
            
            %DropDown Bouton Component
            DropDownButtonComponent(selectionBox, this.parentFigure, '', [], this.selectionContextMenu, 'sizes', [0 -1]);
            
            uicontrol('Parent', selectionBox, ...
                'Style',               'text', ...
                'string',              this.selectionLabel, ...
                'HorizontalAlignment', 'left');
            selectionBox.set('Sizes', [25 100]);
            
            %% Title
            this.selectionTitleUiControl = uicontrol('Parent', topBox, ...
                'Style',               'text', ...
                'string',              '', ...
                'HorizontalAlignment', 'center', ...
                'FontSize',        12,...
                'FontWeight', 'bold');
            
            %% Toolbar
            % Create axes toolbar box
            ToolBarModeComponent(topBox);
            
            topBox.set('Sizes', [240 -1 240]);
        end
        
        
        function configSelectionContextMenu(this)
            
            %Vider les enfants du contextMenu
            childrenList = this.selectionContextMenu.Children;
            for k=1: numel(childrenList)
                childrenList(k).Parent = matlab.graphics.Graphics.empty;
            end
            
            % Reset selectionContextMenuItemList
            this.selectionContextMenuItemList = matlab.ui.container.Menu.empty;
            for k=1:numel(this.selectedLineList)
                if k == this.displayedSelectionValue
                    isChecked = 'on';
                else
                    isChecked = 'off';
                end
                
                this.selectionContextMenuItemList(k) = uimenu(this.selectionContextMenu, ...
                    'Text', this.selectedLineList(k).DisplayName, ...
                    'Checked', isChecked, ...
                    'ForegroundColor', this.selectedLineList(k).Color, ...
                    'Callback',@this.selectionContextMenuItemCallback);
            end
        end
        
        function refreshSelectedPoint(this, selectedIndexList)
            % highlight the selected point givin the selected index
            
            %remove previous editedPoint if exist
            delete(this.editedPointList);
            
            % Draw the new edited Point
            selectedX = this.editedLine.XData(selectedIndexList);
            selectedY = this.editedLine.YData(selectedIndexList);
            hold(this.editedAxes,'on');
            this.editedPointList = plot(this.editedAxes, selectedX,selectedY, 'r*');
            hold(this.editedAxes,'off');
        end
        
        function selectionContextMenuItemCallback(this, src, ~)
            % selectionContextMenuItemCallback Callback
            for k=1:numel(this.selectionContextMenuItemList)
                this.selectionContextMenuItemList(k).set('Checked', 'off');
            end
            
            [~,index] = ismember(src, this.selectionContextMenuItemList);
            
            this.selectionContextMenuItemList(index).set('Checked', 'on');
            this.displayedSelectionValue = index;
            
            this.refreshSelectionTitle();
            this.refreshDataTable();
            this.refreshDataEditedAxes();
        end
        
        function editCellCallback(this, src, callbackdata)
            % edit cell Callback
            editedIndex = callbackdata.Indices(1);
            newValue = callbackdata.NewData;
            
            % Send event
            editDataEventData = EditDataEventData(this.selectedLineList(this.displayedSelectionValue), editedIndex, newValue);
            this.handleDataTableValueChange(src, editDataEventData);
            
            %Refresh editedLine
            if isnan(newValue)
                if this.cleanInterpolation == false
                    this.editedLine.YData(editedIndex) = Inf;
                else
                    this.editedLine.XData(editedIndex) = [];
                    this.editedLine.YData(editedIndex) = [];
                    this.editedLine.UserData(editedIndex) = [];
                    editedIndex = 0;
                end
            else
                this.editedLine.YData(editedIndex) = newValue;
            end
            
            % Refresh the selected point
            this.refreshSelectedPoint(editedIndex);
        end
        
        function selectCellCallback(this, ~, callbackdata)
            % select cell callback
            
            % Selected index in the first column
            selectedIndexList = callbackdata.Indices(:,1);
            % Refresh the selected point
            this.refreshSelectedPoint(selectedIndexList);
        end
        
        function windowButtonDownFcnCallback(this, ~, ~)
            
            %% Search nearest Index from mouse click
            
            XLim = get(this.editedAxes, 'XLim');
            YLim = get(this.editedAxes, 'YLim');
            
            % Compute transformation factor axeunit ==> pixel unit
            this.editedAxes.set('Units', 'pixels');
            rangeX = (XLim(2)-XLim(1));
            if isa(rangeX, 'duration') % Ajout JMA le 26/09/2016 pour R2016b
                rangeX = seconds(rangeX) / (24*3600);
            end
            factorX = this.editedAxes.Position(3) / rangeX;
            factorY = this.editedAxes.Position(4) / (YLim(2)-YLim(1));
            
            % Mouse Position
            mouseAxesPosition  = get(this.editedAxes, 'CurrentPoint');
            mouseAxesPositionX = mouseAxesPosition(1,1);
            mouseAxesPositionY = mouseAxesPosition(1,2);
            
            xData = this.editedLine.XData;
            yData = this.editedLine.YData;
            
            % Compute distance in pixels
            if isa(xData, 'datetime') % Modif JMA le 26/09/2016 pour R2016b
                xMax = datetime(0,0,0,0,0,-Inf);
                xMax = max(xMax, max(xData));
                x0 = datetime(xMax.Year, xMax.Month, xMax.Day);
                
                dX = duration + mouseAxesPositionX;
                xClick = x0 + dX;
                
                dX = xData - xClick;
                dX = seconds(dX) / (24*3600);
                
            else % Fonctionne en R2016a (Code de Pierre Mahoudo)
                dX = (xData - mouseAxesPositionX);
            end
            
            deltaX = dX * factorX;
            deltaY = (yData - mouseAxesPositionY) * factorY;
            distanceArray = deltaX.^2 + deltaY.^2;
            
            dataIndex = (distanceArray < 50); % 50 pixels
            
            % Index of found data
            foundDataIndex = find(dataIndex);
            
            %% Select in the uitable
            
            jUIScrollPane = findjobj(this.dataUiTable, 'class', 'UIScrollPane');
            jUITable = jUIScrollPane.getViewport.getView;
            if ~isempty(foundDataIndex)
                for k=1:numel(foundDataIndex)
                    if k ==1
                        keepSelection = false;
                    else
                        keepSelection = true;
                    end
                    jUITable.changeSelection(foundDataIndex(k)-1, 0, keepSelection, false);
                end
            end
        end
    end
end
