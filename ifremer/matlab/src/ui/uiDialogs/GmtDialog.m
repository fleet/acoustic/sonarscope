classdef GmtDialog < handle & SimpleTitleDialog
    % Description
    %   Class GmtDialog
    %
    % Syntax
    %   a = GmtDialog(...)
    %
    % Output Arguments
    %   s : One GmtDialog instance
    %
    % Examples
    %   a = GmtDialog();
    %   a.openDialog;
    %
    % Authors : MHO
    % See also ParametreComponent ClModel Authors
    % ----------------------------------------------------------------------------

    properties (Constant)
        paperSizeList       = {'A0', 'A1', 'A2', 'A3', 'A4', 'A5', 'A6', 'A7', 'A8', 'A9', 'A10'};
        printingModeList    = {'Automatic', 'Portrait', 'LandScape'};
        gridList            = {'None', 'Lines', 'Cross'};
        projectionList      = {'Mercator', 'UTM', 'Lambert', 'StereoGraphique Polaire', 'Oblique Mercator'};
        hemisphereList      = {'N', 'S'};
        tickmarksFormatList = {'Deg-Min-Sec', 'Decimal', 'Deg-Min,00'};
        scaleTypeList       = {'Geometrical', 'Fit to page'};
        borderStyleList     = {'None', 'Plain', 'Fancy'}; 
    end
    
    properties
        utmZoneList
        
        
        %% GMT model
        % Print
        paperSize    = 5;
        printingMode = 1;
        isPdfDisplay    = false;
        
        % Display
        grid            = 2;
        tickmarksFormat = 3;
        borderStyle     = 3;
  
        
        % Projection
        projection     = 1;
        latitudeParam  = ClParametre('Name', 'Latitude LatEch Cons',  'Value', 0, 'unit', 'deg');
        longitudeParam = ClParametre('Name', 'Long Meridien Origine', 'Value', 0, 'unit', 'deg');
        utmZone        = 1;
        hemisphere     = 1;
        firstParallel  = ClParametre('Name', 'First Parallel',  'Value', 5, 'unit', 'deg');
        secondParallel = ClParametre('Name', 'Second Parallel', 'Value', 5, 'unit', 'deg');
        
        % Scale
        scaleType        = 2;
        isGeometricScale = false;
        denomScaleParam  = ClParametre('Name', 'Denom scale', 'Value', 10000000);
        
        % Line
        isContourLines         = false;
        isCoastLines           = false;
        firstIsoBathLevelParam = ClParametre('Name', 'First Iso-Bath Level', 'Value', -1000.00, 'unit', 'm');
        lastIsoBathLevelParam  = ClParametre('Name', 'Last Iso-Bath Level', 'Value', 1000.00, 'unit', 'm');
        contourColor           = [0 0 0];
        isoBathStepParam       = ClParametre('Name', 'Iso-Bath Step', 'Value', 100.00, 'unit', 'm');
        isoBathLabelingParam   = ClParametre('Name', 'Iso-Bath Labeling', 'Value', 200.00, 'unit', 'm');
        ticksColor             = [0 0 0];
        
        % Nav
        isPlotNav    = false
        navPathList  = '';
        repImportNav = '';
        
        % Comment
        comment = '';
    end
    
    
    properties (Access = protected)
        
        %% UI
        verticalBox
        projectionTypeBox
        projMercatorBox
        projUtmBox
        projLambertBox
        denomScaleParamComponent
        linesVBox
        contourBox
        contourColorComponent
        ticksColorComponent
        navigationBox
        navPathBox
        navPathListComponent
    end
    
    methods
        function this = GmtDialog(varargin)
            % Superclass Constructor
            this = this@SimpleTitleDialog('windowStyle', 'normal', 'Title', 'GMT Printing Mode', varargin{:});
            
            %Print
            [varargin, this.paperSize]    = getPropertyValue(varargin, 'paperSize',    this.paperSize);
            [varargin, this.printingMode] = getPropertyValue(varargin, 'printingMode', this.printingMode);
            [varargin, this.isPdfDisplay] = getPropertyValue(varargin, 'isPdfDisplay', this.isPdfDisplay);
            
            % Display
            [varargin, this.grid]            = getPropertyValue(varargin, 'grid',            this.grid);
            [varargin, this.tickmarksFormat] = getPropertyValue(varargin, 'tickmarksFormat', this.tickmarksFormat);
            [varargin, this.borderStyle]     = getPropertyValue(varargin, 'borderStyle',     this.borderStyle);

            
            % Projection
            [varargin, this.projection]     = getPropertyValue(varargin, 'projection',     this.projection);
            [varargin, this.latitudeParam]  = getPropertyValue(varargin, 'latitudeParam',  this.latitudeParam);
            [varargin, this.longitudeParam] = getPropertyValue(varargin, 'longitudeParam', this.longitudeParam);
            [varargin, this.utmZone]        = getPropertyValue(varargin, 'utmZone',        this.utmZone);
            [varargin, this.hemisphere]     = getPropertyValue(varargin, 'hemisphere',     this.hemisphere);
            [varargin, this.firstParallel]  = getPropertyValue(varargin, 'firstParallel',  this.firstParallel);
            [varargin, this.secondParallel] = getPropertyValue(varargin, 'secondParallel', this.secondParallel);
            
            % Scale
            [varargin, this.scaleType]        = getPropertyValue(varargin, 'scaleType',        this.scaleType);
            [varargin, this.denomScaleParam]  = getPropertyValue(varargin, 'denomScaleParam',  this.denomScaleParam);
            [varargin, this.isGeometricScale] = getPropertyValue(varargin, 'isGeometricScale', this.isGeometricScale);
            
            % Line
            [varargin, this.isContourLines]         = getPropertyValue(varargin, 'isContourLines',         this.isContourLines);
            [varargin, this.isCoastLines]           = getPropertyValue(varargin, 'isCoastLines',           this.isCoastLines);
            [varargin, this.firstIsoBathLevelParam] = getPropertyValue(varargin, 'firstIsoBathLevelParam', this.firstIsoBathLevelParam);
            [varargin, this.lastIsoBathLevelParam]  = getPropertyValue(varargin, 'lastIsoBathLevelParam',  this.lastIsoBathLevelParam);
            [varargin, this.contourColor]           = getPropertyValue(varargin, 'contourColor',           this.contourColor);
            [varargin, this.isoBathStepParam]       = getPropertyValue(varargin, 'isoBathStepParam',       this.isoBathStepParam);
            [varargin, this.isoBathLabelingParam]   = getPropertyValue(varargin, 'isoBathLabelingParam',   this.isoBathLabelingParam);
            [varargin, this.ticksColor]             = getPropertyValue(varargin, 'ticksColor',             this.ticksColor);
            
            % Nav
            [varargin, this.isPlotNav]    = getPropertyValue(varargin, 'isPlotNav',    this.isPlotNav);
            [varargin, this.navPathList]  = getPropertyValue(varargin, 'navPathList',  this.navPathList);
            [varargin, this.repImportNav] = getPropertyValue(varargin, 'repImportNav', this.repImportNav);
            
            % Comment
            [varargin, this.comment] = getPropertyValue(varargin, 'comment', this.comment); %#ok<ASGLU>
            
            % init utm combo
            for k=1:60
                this.utmZoneList{k} = ['Zone ' num2str(k) ' : [' num2str((k-1:k)*6 - 180) ']'];
            end
        end
    end
    
    
    methods (Access = protected)
        function [widthBody ,heightBody] = createBody(this, parent)
            % Create ParametreComponent List
            
            this.verticalBox = uiextras.VBox('Parent', parent, 'Padding', 5, 'Spacing', 5);
            
            %% Print Panel
            printPanel = uiextras.Panel('Parent', this.verticalBox, 'Title', 'Print Options');
%             printPanel = uix.Panel('Parent', this.verticalBox, 'Title', 'Print Options');
            printBox = uiextras.HBox('Parent', printPanel, 'Padding', 5, 'Spacing', 5);
            % Paper size
            uicontrol('Parent', printBox, ...
                'Style',   'text', ...
                'HorizontalAlignment', 'right',...
                'String',  'Paper size :');
            uicontrol('parent', printBox, ...
                'Style',    'popupmenu', ...
                'String',   this.paperSizeList , ...
                'Value',    this.paperSize, ...
                'Callback', @this.paperSizeCallback);
            
            % Printing Mode
            uicontrol('Parent', printBox, ...
                'Style',   'text', ...
                'HorizontalAlignment', 'right',...
                'String',  'Printing Mode :');
            uicontrol('parent', printBox, ...
                'Style',    'popupmenu', ...
                'String',   this.printingModeList , ...
                'Value',    this.printingMode, ...
                'Callback', @this.printingModeCallback);
            
            printBox.set('Sizes', [100 100 100 100]);
            
            %% Disaply Panel
            displayPanel = uiextras.Panel('Parent', this.verticalBox, 'Title', 'Display Options');
%             displayPanel = uix.Panel('Parent', this.verticalBox, 'Title', 'Display Options');
            displayBox   = uiextras.HBox('Parent', displayPanel, 'Padding', 5, 'Spacing', 5);
            
            % Grid
            uicontrol('Parent', displayBox, ...
                'Style',   'text', ...
                'HorizontalAlignment', 'right',...
                'String',  'Grid :');
            uicontrol('parent', displayBox, ...
                'Style',    'popupmenu', ...
                'String',   this.gridList , ...
                'Value',    this.grid, ...
                'Callback', @this.gridCallback);
            
            % Tickmarks format
            uicontrol('Parent', displayBox, ...
                'Style',               'text', ...
                'HorizontalAlignment', 'right',...
                'String',              'Tickmarks format :');
            uicontrol('parent', displayBox, ...
                'Style',    'popupmenu', ...
                'String',   this.tickmarksFormatList , ...
                'Value',    this.tickmarksFormat, ...
                'Callback', @this.tickmarksFormatCallback);
            
            % Border style
            uicontrol('Parent', displayBox, ...
                'Style',               'text', ...
                'HorizontalAlignment', 'right',...
                'String',              'Border style :');
            uicontrol('parent', displayBox, ...
                'Style',    'popupmenu', ...
                'String',   this.borderStyleList , ...
                'Value',    this.borderStyle, ...
                'Callback', @this.borderStyleCallback);
            
            uicontrol('parent', displayBox, ...
                'Style',    'checkbox', ...
                'String',   'PDF Display' , ...
                'Value',    this.isPdfDisplay, ...
                'Callback', @this.pdfDisplayCallback);
            
            displayBox.set('Sizes', [100 100 100 100 100 100 100]);
            
            %% Projection Panel
            projectionPanel = uiextras.Panel('Parent', this.verticalBox, 'Title', 'Cartographical Projection');
%             projectionPanel = uix.Panel('Parent', this.verticalBox, 'Title', 'Cartographical Projection');
            projectionBox = uiextras.HBox('Parent', projectionPanel, 'Padding', 5, 'Spacing', 5);
            
            uicontrol('Parent', projectionBox, ...
                'Style',               'text', ...
                'HorizontalAlignment', 'right',...
                'String',              'Projection :');
            projectionCombo = uicontrol('parent', projectionBox, ...
                'Style',    'popupmenu', ...
                'String',   this.projectionList , ...
                'Value',    this.projection, ...
                'Callback', @this.projectionCallback);
            
            this.projectionTypeBox = uiextras.HBox('Parent', projectionBox);
            
            % Mercator
            this.projMercatorBox = uiextras.HBox('Parent', this.projectionTypeBox);
            SimpleParametreComponent(this.projMercatorBox, 'param', this.latitudeParam,  'editable', 'on');
            SimpleParametreComponent(this.projMercatorBox, 'param', this.longitudeParam, 'editable', 'on');
            
            %UTM
            this.projUtmBox = uiextras.HBox('Parent', this.projectionTypeBox);
            uicontrol('Parent', this.projUtmBox, ...
                'Style',               'text', ...
                'HorizontalAlignment', 'right',...
                'String',              'UTM Zone :');
            uicontrol('parent', this.projUtmBox, ...
                'Style',    'popupmenu', ...
                'String',   this.utmZoneList , ...
                'Value',    this.utmZone, ...
                'Callback', @this.projUtmZoneCallback);
            uicontrol('Parent', this.projUtmBox, ...
                'Style',               'text', ...
                'HorizontalAlignment', 'right',...
                'String',              'Hemisphere :');
            uicontrol('parent', this.projUtmBox, ...
                'Style',    'popupmenu', ...
                'String',   this.hemisphereList , ...
                'Value',    this.hemisphere, ...
                'Callback', @this.projHemisphereCallback);
            
            % Lambert
            this.projLambertBox = uiextras.HBox('Parent', this.projectionTypeBox);
            SimpleParametreComponent(this.projLambertBox, 'param', this.firstParallel,  'editable', 'on');
            SimpleParametreComponent(this.projLambertBox, 'param', this.secondParallel, 'editable', 'on');
            
            projectionBox.set('Sizes', [100 100 -1]);
            
            % init visibility of components
            this.projectionCallback(projectionCombo);
            
            %% Scale Panel
            scalePanel = uiextras.Panel('Parent', this.verticalBox, 'Title', 'Scale');
%             scalePanel = uix.Panel('Parent', this.verticalBox, 'Title', 'Scale');
            scaleBox   = uiextras.HBox('Parent', scalePanel, 'Padding', 5, 'Spacing', 5);
            % Type of Scale
            uicontrol('Parent', scaleBox, ...
                'Style',               'text', ...
                'HorizontalAlignment', 'right',...
                'String',              'Type of Scale :');
            scaleTypeCombo = uicontrol('parent', scaleBox, ...
                'Style',    'popupmenu', ...
                'String',   this.scaleTypeList , ...
                'Value',    this.scaleType, ...
                'Callback', @this.scaleTypeCallback);
            
            % Denom Scale
            this.denomScaleParamComponent = SimpleParametreComponent(scaleBox, 'param', this.denomScaleParam, 'editable', 'on');
            % init denomScaleParamComponent
            this.scaleTypeCallback(scaleTypeCombo);
            
            uicontrol('parent', scaleBox, ...
                'Style',    'checkbox', ...
                'String',   'Geometric Scale' , ...
                'Value',    this.isGeometricScale, ...
                'Callback', @this.geometricScaleCallback);
            
            this.denomScaleParamComponent.set('Sizes', [100 100 0 0]);
            scaleBox.set('Sizes', [100 100  250  100]);
            
            %% Lines Panel
            linesPanel = uiextras.Panel('Parent', this.verticalBox, 'Title', 'Lines Options');
%             linesPanel = uix.Panel('Parent', this.verticalBox, 'Title', 'Lines Options');
            this.linesVBox = uiextras.VBox('Parent', linesPanel, 'Padding', 5, 'Spacing', 5);
            checkHBox = uiextras.HBox('Parent', this.linesVBox);
            
            
            isContourLinesCheckbox = uicontrol('parent', checkHBox, ...
                'Style',    'checkbox', ...
                'String',   'Contour Lines' , ...
                'Value',    this.isContourLines, ...
                'Callback', @this.contourLinesCallback);
            uicontrol('parent', checkHBox, ...
                'Style',    'checkbox', ...
                'String',   'Coast Lines' , ...
                'Value',    this.isCoastLines, ...
                'Callback', @this.coastLinesCallback);
            
            
            this.contourBox = uiextras.VBox('Parent', this.linesVBox);
            contourBox1 = uiextras.HBox('Parent', this.contourBox);
            contourBox2 = uiextras.HBox('Parent', this.contourBox);
            
            SimpleParametreComponent(contourBox1, 'param', this.firstIsoBathLevelParam, 'editable', 'on');
            SimpleParametreComponent(contourBox1, 'param', this.lastIsoBathLevelParam, 'editable', 'on');
            
            uicontrol('parent', contourBox1, ...
                'Style',    'push', ...
                'String',   'Contour Color' , ...
                'Callback', @this.contourColorCallback);
            
            this.contourColorComponent = uicontrol('parent', contourBox1, ...
                'Style',    'text', ...
                'String',   '   ' );
            this.contourColorComponent.BackgroundColor = this.contourColor;
            
            
            SimpleParametreComponent(contourBox2, 'param', this.isoBathStepParam,     'editable', 'on');
            SimpleParametreComponent(contourBox2, 'param', this.isoBathLabelingParam, 'editable', 'on');
            uicontrol('parent', contourBox2, ...
                'Style',    'push', ...
                'String',   'Ticks Color' , ...
                'Callback', @this.ticksColorCallback);
            
            this.ticksColorComponent = uicontrol('parent', contourBox2, ...
                'Style',    'text', ...
                'String',   '   ' );
            this.ticksColorComponent.BackgroundColor = this.ticksColor;
            
            
            this.linesVBox.set('Sizes', [30 -1]);
            contourBox1.set('Sizes', [-1 -1 100 40]);
            contourBox2.set('Sizes', [-1 -1 100 40]);
            
            % init visibility of components
            this.contourLinesCallback(isContourLinesCheckbox);
            
            
            %% Navigation Panel
            navigationPanel = uiextras.Panel('Parent', this.verticalBox, 'Title', 'Navigation');
%             navigationPanel = uix.Panel('Parent', this.verticalBox, 'Title', 'Navigation');
            this.navigationBox = uiextras.HBox('Parent', navigationPanel, 'Padding', 5, 'Spacing', 5);
            
            isPlotNavCheckbox =  uicontrol('parent', this.navigationBox, ...
                'Style',    'checkbox', ...
                'String',   'Plot Navigation' , ...
                'Value',    this.isPlotNav, ...
                'Callback', @this.isPlotNavCallback);
            
            this.navPathBox = uiextras.HBox('Parent', this.navigationBox);
            
            uicontrol('parent', this.navPathBox, ...
                'Style',    'push', ...
                'String',   'Browser' , ...
                'Callback', @this.navPathBrowserCallback);
            
            % navList field
            this.navPathListComponent = uicontrol('Parent', this.navPathBox, ...
                'Style',   'listbox', ...
                'String', this.navPathList);
            
            
            this.navigationBox.set('Sizes', [100 -1]);
            this.navPathBox.set('Sizes', [100 -1]);
            
            % init visibility of components
            this.isPlotNavCallback(isPlotNavCheckbox);
            
            %% Comment panel
            commentPanel = uiextras.Panel('Parent', this.verticalBox, 'Title', 'Comments');
%             commentPanel = uix.Panel('Parent', this.verticalBox, 'Title', 'Comments');
            commentBox = uiextras.HBox('Parent', commentPanel, 'Padding', 5, 'Spacing', 5);
            % Comment label
            uicontrol('Parent', commentBox, ...
                'Style',               'text', ...
                'HorizontalAlignment', 'right',...
                'String',              'Comment :');
            % Comment field
            uicontrol('Parent', commentBox, ...
                'Style',    'edit', ...
                'String',   this.comment,...
                'Callback', @this.commentCallback);
            
            
            commentBox.set('Sizes', [100 -1]);
            
            %%
            this.verticalBox.set('Sizes', [50 50 50 50 120 50 50]);
            
            widthBody  = 800;
            heightBody = 450;
        end 
        
        
        
        %% Callback
        function paperSizeCallback(this, src, ~)
            this.paperSize = src.Value;
        end
        
        
        function printingModeCallback(this, src, ~)
            this.printingMode = src.Value;
        end
        
        
        function gridCallback(this, src, ~)
            this.grid = src.Value;
        end
        
        
        function projectionCallback(this, src, ~)
            this.projection = src.Value;
            
            % Show / Hide projection box
            if (this.projection == 1) ||  (this.projection == 5) % Mercator // Oblique mecator
                this.projMercatorBox.Visible = 'on';
                this.projUtmBox.Visible      = 'off';
                this.projLambertBox.Visible  = 'off';
                this.projectionTypeBox.set('Sizes', [-1 0 0]);
            elseif (this.projection == 2) % UTM
                this.projMercatorBox.Visible = 'off';
                this.projUtmBox.Visible      = 'on';
                this.projLambertBox.Visible  = 'off';
                this.projectionTypeBox.set('Sizes', [0 -1 0]);
            elseif (this.projection == 3) % Lambert
                this.projMercatorBox.Visible = 'off';
                this.projUtmBox.Visible      = 'off';
                this.projLambertBox.Visible  = 'on';
                this.projectionTypeBox.set('Sizes', [0 0 -1]);
            else
                this.projMercatorBox.Visible = 'off';
                this.projUtmBox.Visible      = 'off';
                this.projLambertBox.Visible  = 'off';
                this.projectionTypeBox.set('Sizes', [0 0 0]);
            end
        end
        
        
        function projUtmZoneCallback(this, src, ~)
            this.utmZone = src.Value;
        end
        
        
        function projHemisphereCallback(this, src, ~)
            this.hemisphere = src.Value;
        end
        
        
        function tickmarksFormatCallback(this, src, ~)
            this.tickmarksFormat = src.Value;
        end
        
        
        function scaleTypeCallback(this, src, ~)
            this.scaleType = src.Value;
            
            % Enabled / disabled scale component
            if this.scaleType == 1
                this.denomScaleParamComponent.setEditable('on');
            else
                this.denomScaleParamComponent.setEditable('off');
            end
        end
        
        
        function borderStyleCallback(this, src, ~)
            this.borderStyle = src.Value;
        end
        
        
        function contourLinesCallback(this, src, ~)
            this.isContourLines = src.Value;
            
            % Show / Hide Contour components
            if  this.isContourLines
                this.contourBox.Visible = 'on';
                size = -1;
            else
                this.contourBox.Visible = 'off';
                size = 0;
            end
            sizes = this.linesVBox.get('Sizes');
            sizes(2) = size;
            this.linesVBox.set('Sizes', sizes);
        end
        
        
        function coastLinesCallback(this, src, ~)
            this.isCoastLines = logical(src.Value);
        end
        
        
        function geometricScaleCallback(this, src, ~)
            this.isGeometricScale = logical(src.Value);
        end
        
        
        function pdfDisplayCallback(this, src, ~)
            this.isPdfDisplay = logical(src.Value);
        end
        
        
        function contourColorCallback(this, ~, ~)
            this.contourColor = uisetcolor(this.contourColor, 'Define color of contour');
            % Update color ui
            this.contourColorComponent.BackgroundColor = this.contourColor;
        end
        
        
        function ticksColorCallback(this, ~, ~)
            this.ticksColor = uisetcolor(this.ticksColor, 'Define color of contour');
            % Update color ui
            this.ticksColorComponent.BackgroundColor = this.ticksColor;
        end
        
        
        function isPlotNavCallback(this, src, ~)
            this.isPlotNav = logical(src.Value);
            
            % Show / Hide Contour components
            if  this.isPlotNav
                this.navPathBox.Visible = 'on';
                size = -1;
            else
                this.navPathBox.Visible = 'off';
                size = 0;
            end
            sizes = this.navigationBox.get('Sizes');
            sizes(2) = size;
            this.navigationBox.set('Sizes', sizes);
        end
        
        
        function navPathBrowserCallback(this, ~, ~)
            % Select files
            [flag, ListeFicNav, lastDir] = uiSelectFiles('ExtensionFiles', '.txt','RepDefaut', this.repImportNav);
            if ~flag
                return
            end
            this.repImportNav = lastDir;
            this.navPathList  = ListeFicNav;
            % update compoent
            this.navPathListComponent.String = this.navPathList;
        end
        
        
        function commentCallback(this, src, ~)
            this.comment = src.String;
        end
    end
end
