classdef HistoMinMaxStepDialog < SimpleTitleDialog
    % HistoMinMaxStepDialog Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        params = ClParametre.empty                           % - ClParametre array (min max step)
        histo
        paramComponentList = SimpleParametreComponent.empty; % - ParametreComponent array
        histValues
    end
    
    events
        % OneParamValueChange % Event if one param value change
    end
    
    methods
        function this = HistoMinMaxStepDialog(varargin)
            % Superclass Constructor
            this = this@SimpleTitleDialog('windowStyle', 'normal', varargin{:});
            if isempty(this.Title)
                this.Title = 'Histogram';
            end
            
            % Read input args
            [varargin, this.histo]      = getPropertyValue(varargin, 'histo',        []);
            [varargin, StatValues]      = getPropertyValue(varargin, 'StatValues',   []);
            [varargin, unit]            = getPropertyValue(varargin, 'unit',         []);
            [varargin, isIntegerVal]    = getPropertyValue(varargin, 'isIntegerVal', []);
            [varargin, this.histValues] = getPropertyValue(varargin, 'histValues',   []);
            
            
            this.histo = this.histo(:);
            if isempty(StatValues)
                ValMin = double(min(this.histo));
                ValMax = double(max(this.histo));
            else
                ValMin = double(StatValues.Min);
                ValMax = double(StatValues.Max);
            end
            
            % if numel(bins)
            if numel(this.histo) == 0 % Modif JMA le 19/05/2017
                Pas = 1;
            else
                if isempty(this.histo)
                    str1 = sprintf('L''histogramme de l''image "%s" est vide.', Titre);
                    str2 = sprintf('The histogram of image "%s" is empty.', Titre);
                    my_warndlg(Lang(str1,str2), 0);
                    flag = 0;
                    return
                end
                Pas = double(this.histo(2)) - double(this.histo(1));
                %     Pas = mean(diff(double(bins)));
                Pas = round(Pas, 2, 'significant');
            end
            
            if isIntegerVal
                Pas = max(1, floor((this.histo(end) - bins(1)) / 100));
                ValMin = floor(ValMin);
                ValMax = ceil(ValMax);
            else
                if strcmp(unit, 'num') || strcmp(unit, '#')
                    Pas = 1;
                end
            end
            
            %             if nargin == 6
            %                 HistValues = varargin{1};
            % %                 Fig = figure;
            %                 try
            %                     bar(bins, HistValues); grid
            %                 catch %#ok<CTCH>
            %                     my_close(Fig)
            %                 end
            %             end
            
            if nargin == 6
                this.histValues = varargin{6};
            end
            
            
            
            this.params(1) = ClParametre('Name', 'Min', 'Unit', unit, 'MinValue', ValMin, 'MaxValue', ValMax, 'Value', ValMin);
            this.params(2) = ClParametre('Name', 'Max', 'Unit', unit, 'MinValue', ValMin, 'MaxValue', ValMax, 'Value', ValMax);
            this.params(3) = ClParametre('Name', Lang('Pas', 'Step'), 'Unit', unit, 'MinValue', 0, 'MaxValue', ValMax-ValMin, 'Value', Pas);
            
        end
        
        
        function handleValueChange(this, ~, ~)
            % Send one value change event
            %             this.notify('OneParamValueChange');
            
            % TODO refresh histogram
        end
        
        
        function paramsValue = getParamsValue(this)
            % Get the "Value" properties of the parametres in an array
            paramsValue = [];
            for k=length(this.params):-1:1
                paramsValue(k) = this.params(k).Value;
            end
        end
    end
    
    
    methods (Access = protected)
        function [widthBody ,heightBody] = createBody(this, parent)
            
            bodyBox = uiextras.VBox('Parent', parent);
            
            % TODO create Histo
            heightHisto = 400;
            
            ax = axes(bodyBox, 'Units', 'normalized');
            bar(ax, this.histo, this.histValues); grid
            
            % Create ParametreComponent List
            paramBox = uiextras.VBox('Parent', bodyBox);
            
            
            lengthComponents = length(this.params);
            for paramIdx=1:lengthComponents
                % Create ParametreComponent component
                %                 this.paramComponentList(paramIdx) = ParametreComponent(paramBox, 'param', this.params(paramIdx), 'sizes', this.sizes, 'style', this.paramStyle);
                this.paramComponentList(paramIdx) = SimpleParametreComponent(paramBox, 'param', this.params(paramIdx));
                % Add a listener for Value change event
                addlistener(this.paramComponentList(paramIdx), 'ValueChange', @this.handleValueChange);
            end
            
            
            bodyBox.set('Sizes', [-1 25 * lengthComponents]);
            
            widthBody  = 600;
            heightBody = (25 * lengthComponents) + heightHisto;
        end
    end
end
