classdef ModelDialog < handle & AbstractDisplay
    % Desciption
    %   GUI that displays the parameters of a model and display the model
    %
    % Syntax
    %   a = ModelDialog(...)
    %
    % ModelDialog properties :
    %   Title       - Title of paramDialog
    %   model       - ClModel
    %   paramDialog - parameters dialog
    %
    % Examples
    %   model = BSLurtonModel('XData', -80:80);
    %   model = noise(model, 'Coeff', 5);
    %   plot(model); %option
    %   b = ModelDialog('This is a demonstration of ModelDialog', model);
    %   b.openDialog
    %   flag = b.paramDialog.okPressedOut
    %    
    %   style1 = ColorAndFontStyle('BackgroundColor', [0.7 0.7 1], 'ForegroundColor', 'k', 'FontWeight', 'bold', 'FontSize', 13, 'FontAngle', 'italic'); %option
    %   style2 = ColorAndFontStyle('BackgroundColor', [1 0.8 0.6], 'ForegroundColor', 'k', 'FontWeight', 'bold');
    %   b.paramDialog.Help =  'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
    %   b.paramDialog.titleStyle = style1;
    %   b.paramDialog.paramStyle = style2;
    %   b.openDialog
    %   flag = b.paramDialog.okPressedOut
    %
    %   XData  = -80:0.5:-30;
    %   xNodes = -80:10:-30;
    %   yNodes = [-9.8 -4.0 -0.7  1.0 -1.1 -8.0];
    %   model = AntenneSplineModel('XData', XData, 'xNodes', xNodes, 'yNodes', yNodes)
    %   model = noise(model, 'Coeff', 2);
    %   plot(model);
    %   b = ModelDialog('This is a demonstration of ModelDialog', model);
    %   b.paramDialog.titleStyle = style1;
    %   b.paramDialog.paramStyle = style2;
    %   b.openDialog
    %   flag = b.paramDialog.okPressedOut
    % 
    % Authors : MHO
    % See also ClModel ParametreDialog ParametreComponent Authors
    % ----------------------------------------------------------------------------
    
    properties
        model
        paramDialog
        hPlot
        plotFigure
    end
    
    methods
        function this = ModelDialog(Title, model, varargin)
            this.model = model;
            
            % Create the instance of ParametreDialog
            this.paramDialog = ParametreDialog('params', this.model.Params, 'Title', Title, varargin{:});
            
            % Attach a listener to the property 'OneParamValueChange' of paramDialog
            addlistener(this.paramDialog, 'OneParamValueChange', @this.handleValueChange);
            % Attach a listener to the property 'OkPressedEvent' of paramDialog, triggers 'this.closePlotCallback'
            addlistener(this.paramDialog, 'OkPressedEvent', @this.closePlotCallback);
            % Attach a listener to the property 'CancelPressedEvent' of paramDialog, triggers 'this.closePlotCallback'
            addlistener(this.paramDialog, 'CancelPressedEvent', @this.closePlotCallback);
        end
        
        function openDialog(this)
            % Plot the model
            this.plot;
            
            % Open the parametre dialog
            this.paramDialog.openDialog;
        end
        
        function this = plot(this)
            [~, plotFigureOut, hCurves] = plot(this.model);
            this.hPlot = hCurves;
            this.plotFigure = plotFigureOut;
        end
        
        function handleValueChange(this, src, eventData) %#ok<INUSD>
            [y, components] = this.model.compute;
            if isempty(this.hPlot.Model) || ~ishandle(this.hPlot.Model(1))
                plot(this);
            else
                k1 = 1;
                set(this.hPlot.Model(k1), 'YData', y);
                for k2=1:size(components, k1)
                    set(this.hPlot.Components(1,k2), 'YData', components(k2,:));
                end
            end
        end
        
        function closePlotCallback(this, src, eventData) %#ok<INUSD>
            delete(this.plotFigure);
        end
    end
    
    methods (Access = protected)
        function str = displayScalarObjectCustom(this)
            % Summary of one ModelDialog instance in a character array
            % Abstract methods from AbstractDisplay
            
            str = {};
            str{end+1} = sprintf('Title     <-> %s', this.paramDialog.Title);
            
            str{end+1} = sprintf('--- aggregation model (ClModel) ---');
            str{end+1} = displayScalarObjectCustom(this.model);
            
            str = cell2str(str);
        end
    end
end
