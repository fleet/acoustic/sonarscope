classdef NavigationDialog < handle & SignalDialog
    % Description
    %   Class NavigationDialog
    %
    % ParametreDialog properties :
    %   optim         - Optim Data Model
    %   selectedModel - the selected Model Data Model
    %
    % Examples
    %   latSignal = ClSignal('ySample', YSample('data', sin(0:0.01*pi:10*pi-0.01*pi)));
    %   lonSignal = ClSignal('ySample', YSample('name', 'Longitude', 'unit', 'Deg', 'data', sin(0:0.02*pi:20*pi-0.01*pi)));
    %   immersionSignal = ClSignal('ySample', YSample('name', 'Immersion', 'unit', 'm', 'data', sin(0:0.03*pi:30*pi-0.01*pi)));
    %   nav  = ClNavigation(latSignal, lonSignal, 'immersionSignal', immersionSignal);
    %
    %   a = NavigationDialog(nav, 'Title', 'Navigation', 'CleanInterpolation', true);
    %   a.openDialog();
    %
    % Create n navigation and 1 sample in each
    %   fileName{1} = 'DataForDemo/NavAUV/usblRepeater09Modif.txt';
    %   fileName{2} = 'DataForDemo/NavAUV/usblRepeater10Modif.txt';
    %   fileName{3} = 'DataForDemo/NavAUV/usblRepeater11Modif.txt';
    %   fileName{4} = 'DataForDemo/NavAUV/usblRepeater12Modif.txt';
    %   FileNameOut = FtpUtils.importDataForDemoFromSScFTP(fileName);
    %   for k=1:length(FileNameOut)
    %        [flag, Latitude, Longitude, SonarTime, Heading, SonarSpeed, Immersion] = lecFicNav(FileNameOut{k});
    %        T = datetime(SonarTime.timeMat, 'ConvertFrom', 'datenum');
    %        timeSample = XSample('name', 'time', 'data', T);
    %        latSample = YSample('data', Latitude);
    %        latSignal =  ClSignal('ySample', latSample, 'xSample', timeSample);
    %        lonSample = YSample('data', Longitude);
    %        lonSignal =  ClSignal('ySample', lonSample, 'xSample', timeSample);
    %        immersionSample = YSample('name', 'Immersion', 'unit', 'm', 'data', Immersion);
    %        immersionSignal =  ClSignal('ySample', immersionSample, 'xSample', timeSample);
    %        nav(k)  = ClNavigation(latSignal, lonSignal, 'immersionSignal', immersionSignal, 'name', ['navigation ' num2str(k)]);
    %   end
    %   a = NavigationDialog(nav, 'Title', 'Navigation', 'CleanInterpolation', false, 'waitAnswer', false);
    %   a.openDialog();
    %
    %   fileName = 'Z:\private\ifremer\DataForDemo\Delph\20131010-navSMF-std.txt';
    %   [flag, Latitude, Longitude, SonarTime, Heading, SonarSpeed, Immersion] = lecFicNav(fileName);
    %   T = datetime(SonarTime.timeMat, 'ConvertFrom', 'datenum');
    %   timeSample = XSample('name', 'time', 'data', T);
    %   latSample = YSample('data', Latitude);
    %   latSignal = ClSignal('ySample', latSample, 'xSample', timeSample);
    %   lonSample = YSample('data', Longitude);
    %   lonSignal = ClSignal('ySample', lonSample, 'xSample', timeSample);
    %   immersionSample = YSample('name', 'Immersion', 'unit', 'm', 'data', -Immersion);
    %   immersionSignal =  ClSignal('ySample', immersionSample, 'xSample', timeSample);
    %   headingSample = YSample('name', 'Heading',   'unit', 'deg', 'data', Heading);
    %   headingSignal = ClSignal('ySample', headingSample, 'xSample', timeSample);
    %   nav = ClNavigation(latSignal, lonSignal, 'headingVesselSignal', headingSignal, 'immersionSignal', immersionSignal, ...
    %                      'name', '20131010-navSMF-std.txt');
    %   a = NavigationDialog(nav, 'Title', 'Navigation');
    %   a.openDialog();
    %   %   waitfor(a.parentFigure)
    %
    %   subNaN = ~isnan(a.navigationList.ySample(1).data)
    %   timeSample      = XSample('name', 'time', 'data', T(subNaN));
    %   latSample       = YSample('data', Latitude(subNaN));
    %   latSignal       = ClSignal('ySample', latSample, 'xSample', timeSample);
    %   lonSample       = YSample('data', Longitude(subNaN));
    %   lonSignal       = ClSignal('ySample', lonSample, 'xSample', timeSample);
    %   immersionSample = YSample('name', 'Immersion', 'unit', 'm', 'data', -Immersion(subNaN));
    %   immersionSignal = ClSignal('ySample', immersionSample, 'xSample', timeSample);
    %   headingSample   = YSample('name', 'Heading',   'unit', 'deg', 'data', Heading(subNaN));
    %   headingSignal   = ClSignal('ySample', headingSample, 'xSample', timeSample);
    %   nav = ClNavigation(latSignal, lonSignal, 'headingVesselSignal', headingSignal, 'immersionSignal', immersionSignal, ...
    %                      'xSample', timeSample, 'name', '20131010-navSMF-std.txt');
    %   b = NavigationDialog(nav, 'Title', 'Navigation');
    %   b.openDialog();
    %
    % Authors : MHO
    % See also SignalDialog ClNavigation Authors
    % ----------------------------------------------------------------------------
    
    properties (Access = private)
        
        %ui
        navBox
        navContextMenuItemList
        navListBox
        
        % ui - axes/line array
        nav2DBox
        nav2DAxes
        navLineArray
        nav3DPanel
        nav3DAxes
        nav3DLineArray
        latLonSelection % lat lon rectangle selection
        
        %data
        nav3D                 = true;
        displayedNavPlotValue = 3;
    end
    
    methods
        function this = NavigationDialog(navigationList, varargin)
            % Superclass Constructor
            this = this@SignalDialog(navigationList, varargin{:});
            if isempty(this.Title)
                this.Title = 'Navigation';
            end
            
            if navigationList(1).immersionSignalIndex == 0 % no immersion, no 3d
                this.nav3D = false;
                this.displayedNavPlotValue = 1;
            end
            
        end
        
        function saved = saveChangePlotInSignal(this)
            saved = saveChangePlotInSignal@SignalDialog(this);
            switch saved
                case 'Yes' % SAVE
                    % Dont do anything
                case 'No' % DONT SAVE
                    % Reset NavLines
                    this.updateAllNav();
            end
            
            %Clean tmpLine list on nav
            TmpLineUtils.cleanTmpLineList(this.nav2DAxes);
            if this.nav3D
                TmpLineUtils.cleanTmpLineList(this.nav3DAxes);
            end
            
            %Clean tmpLine list on lat/lon
            % Lat
            latitudeSignalIndex  = this.signalContainerList(1).latitudeSignalIndex;
            latAxes = this.signalAxesArray(latitudeSignalIndex);
            TmpLineUtils.cleanTmpLineList(latAxes);
            %Lon
            longitudeSignalIndex = this.signalContainerList(1).longitudeSignalIndex;
            lonAxes = this.signalAxesArray(longitudeSignalIndex);
            TmpLineUtils.cleanTmpLineList(lonAxes);
            
        end
    end
    
    methods (Access = protected)
        
        function configureFigure(this)
            configureFigure@SignalDialog(this);
            % add listener to send event when tmp lines are deleted.
            % Update the lat/lon rectangle seletction
            addlistener(this.manualCleanDataMode, 'CleanTmpEvent', @this.updateLatLonSelection);
            addlistener(this.algoCleanDataMode,   'CleanTmpEvent', @this.updateLatLonSelection);
        end
        
        function [widthBody ,heightBody] = createBody(this, parent)
            % Override the function in mother class SimpleTitleDialog
            % Create Body of NavigationDialog
            
            verticalBox = uiextras.VBox('Parent', parent, 'Spacing', 0);
            
            % FilterParametresComponent
            if isempty(this.filterDataParametre)
                this.filterParametreComponent  =  FilterDataModeParametreComponent(verticalBox);
            else
                % if not empty, init the filter parametres
                this.filterParametreComponent  =  FilterDataModeParametreComponent(verticalBox, 'filterDataParametre', this.filterDataParametre);
            end
            
            [signalSelectionPanel, signalSelectionBoxComponentNb] =  this.createSignalSelectionBox(verticalBox);
            
            horizontalBox = uiextras.HBoxFlex('Parent', verticalBox, 'Spacing', 5);
            
            this.createLeftBox(horizontalBox);
            this.createRightBox(horizontalBox);
            
            % Set visible signalSelectionBox if not empty
            if signalSelectionBoxComponentNb == 0
                signalSelectionPanel.Visible = 'off';
                signalSelectionPanelSize     = 0;
            else
                signalSelectionPanel.Visible = 'on';
                signalSelectionPanelSize     = 32;
            end
            
            verticalBox.set('Sizes', [0 signalSelectionPanelSize -1]);
            
            widthBody  = 1000;
            heightBody =  700;
        end
        
        function createRightBox(this, parent)
            rightPanel  = uiextras.Panel('Parent', parent, 'Title', 'Navigation');
%             rightPanel  = uix.Panel('Parent', parent, 'Title', 'Navigation');
            this.navBox = uiextras.VBox('Parent', rightPanel);
            
            % Comment to avoid freez whith compiled version
            % Create Sample Action Box
            this.createNavActionBox(this.navBox);
            
            % create/update samples Box
            this.createNavListAxes(this.navBox);
            
            this.navBox.set('Sizes', [32 -1]);
            %             this.navBox.set('Sizes', -1);
        end
        
        %         Comment to avoid freez whith compiled version
        function createNavActionBox(this, parent)
            % Create samples action box
            
            navActionBox = uiextras.HBox('Parent', parent, 'Padding', 5, 'Spacing', 5);
            
            contextMenu = uicontextmenu(this.parentFigure);
            menuItemChecked = {'off', 'off', 'off'};
            menuItemChecked{this.displayedNavPlotValue} = 'on';
            this.navContextMenuItemList = matlab.ui.container.Menu.empty;
            this.navContextMenuItemList(1) = uimenu(contextMenu, 'Text', 'Navigation 2D' ,'Checked', menuItemChecked{1}, ...
                'Callback',@this.navContextMenuItemCallback);
            
            enabled = 'off';
            %             if ~isempty(this.signalContainerList(1).signalList(this.signalContainerList(1).immersionSignalIndex))
            if (this.signalContainerList(1).immersionSignalIndex ~= 0) ...
                    && ~isempty(this.signalContainerList(1).signalList(this.signalContainerList(1).immersionSignalIndex)) % Modif JMA le 10/01/2018 : TODO MHO : v�rifier
                enabled = 'on';
            end
            this.navContextMenuItemList(2) = uimenu(contextMenu,'Text', 'Navigation 3D' , 'Checked', menuItemChecked{2}, 'Enable', enabled, ...
                'Callback',@this.navContextMenuItemCallback);
            this.navContextMenuItemList(3) = uimenu(contextMenu,'Text','Navigation 2D & Navigation 3D ', 'Checked', menuItemChecked{3}, 'Enable', enabled, ...
                'Callback',@this.navContextMenuItemCallback);
            
            %DropDown Bouton Component
            DropDownButtonComponent(navActionBox, this.parentFigure, '', [], contextMenu, 'sizes', [0 -1]);
            
            uicontrol('Parent', navActionBox, ...
                'Style',               'text', ...
                'string',              'Select Navigation to Show / Hide', ...
                'HorizontalAlignment', 'left');
            
            navActionBox.set('Sizes', [25 -1]);
        end
        
        function createNavListAxes(this, parent)
            % Create the Nav List Axes
            
            this.navListBox = uiextras.VBoxFlex('Parent', parent);
            
            this.nav2DBox = uiextras.VBox('Parent', this.navListBox);
            this.nav2DAxes = axes('Parent',this.nav2DBox, 'Units', 'normalized');
            [~, this.navLineArray] = this.signalContainerList.plotNav('navAxes', this.nav2DAxes);
            % Create the lat/lon rectangle selection
            this.createLatLonSelection();
            this.linkPropertiesOfLines(this.navLineArray);
            
            % avoid rotate3d
            rotate3dObj = rotate3d(this.parentFigure);
            rotate3dObj.setAllowAxesRotate(this.nav2DAxes,false);
            % panel here to avoid problem with size when rotate
            this.nav3DPanel = uipanel('Parent', this.navListBox);
            %             this.nav3DPanel = uiextras.VBox('Parent', this.navListBox);
            
            this.nav3DAxes = axes('Parent',this.nav3DPanel, 'Units', 'normalized');
            if (this.nav3D) %nav3d if immersion
                [~, this.nav3DLineArray] = this.signalContainerList.plotNav('navAxes', this.nav3DAxes, '3D', true);
                this.linkPropertiesOfLines(this.nav3DLineArray);
            end
            
            % draw nav
            this.showHideNavs();
        end
        
        function createLatLonSelection(this)
            % Create the lat/lon rectangle selection
            this.latLonSelection = rectangle(this.nav2DAxes, 'LineStyle', '--');
            
            
            % add listeners on YLim on all axes to update latlon selection (event only
            % on lat/lon dosen't work)
            for k=1:this.signalMaxNb
                addlistener(this.signalAxesArray(k), 'XLim',     'PostSet', @(src,eventdata)updateLatLonSelection(this, src, eventdata));
                addlistener(this.signalAxesArray(k), 'YLim',     'PostSet', @(src,eventdata)updateLatLonSelection(this, src, eventdata));
                addlistener(this.signalAxesArray(k), 'XLimMode', 'PostSet', @(src,eventdata)updateLatLonSelection(this, src, eventdata));
                addlistener(this.signalAxesArray(k), 'YLimMode', 'PostSet', @(src,eventdata)updateLatLonSelection(this, src, eventdata));
            end
            
            % update postion of rectangle
            this.updateLatLonSelection();
        end
        
        function updateLatLonSelection(this, ~, ~)
            % Update the postion of lat/lon rectangle selection
            latIndex =  this.signalContainerList(1).latitudeSignalIndex;
            lonIndex =  this.signalContainerList(1).longitudeSignalIndex;
            
            latAxes = this.signalAxesArray(latIndex);
            lonAxes = this.signalAxesArray(lonIndex);
            
            latYLim = latAxes.get('YLim');
            lonYLim = lonAxes.get('YLim');
            if isvalid(this.latLonSelection)
                this.latLonSelection.set('Position', [lonYLim(1) latYLim(1) lonYLim(2)-lonYLim(1)  latYLim(2)-latYLim(1)]);
            end
        end
        
        function linkPropertiesOfLines(this, navLineArray)
            % Add listener to sync color and lineStyle between nav line and and latitude line
            
            for containerIndex=1:numel(this.signalContainerList)
                for sampleIndex=1:this.ySampleMaxNb
                    navLine = navLineArray(containerIndex, sampleIndex);
                    
                    % Find latitude line
                    latIndex = this.signalContainerList(containerIndex).latitudeSignalIndex;
                    latLine  = this.signalLineArray(containerIndex, latIndex, sampleIndex);
                    % add listner to navLine
                    addlistener(navLine, 'Color',           'PostSet', @(~,~)set(latLine, 'Color',           get(navLine, 'Color')));
                    addlistener(navLine, 'LineStyle',       'PostSet', @(~,~)set(latLine, 'LineStyle',       get(navLine, 'LineStyle')));
                    addlistener(navLine, 'LineWidth',       'PostSet', @(~,~)set(latLine, 'LineWidth',       get(navLine, 'LineWidth')));
                    addlistener(navLine, 'Marker',          'PostSet', @(~,~)set(latLine, 'Marker',          get(navLine, 'Marker')));
                    addlistener(navLine, 'MarkerSize',      'PostSet', @(~,~)set(latLine, 'MarkerSize',      get(navLine, 'MarkerSize')));
                    addlistener(navLine, 'MarkerEdgeColor', 'PostSet', @(~,~)set(latLine, 'MarkerEdgeColor', get(navLine, 'MarkerEdgeColor')));
                    % add listner to latLine
                    addlistener(latLine, 'Color',           'PostSet', @(~,~)set(navLine, 'Color',           get(latLine, 'Color')));
                    addlistener(latLine, 'LineStyle',       'PostSet', @(~,~)set(navLine, 'LineStyle',       get(latLine, 'LineStyle')));
                    addlistener(latLine, 'LineWidth',       'PostSet', @(~,~)set(navLine, 'LineWidth',       get(latLine, 'LineWidth')));
                    addlistener(latLine, 'Marker',          'PostSet', @(~,~)set(navLine, 'Marker',          get(latLine, 'Marker')));
                    addlistener(latLine, 'MarkerSize',      'PostSet', @(~,~)set(navLine, 'MarkerSize',      get(latLine, 'MarkerSize')));
                    addlistener(latLine, 'MarkerEdgeColor', 'PostSet', @(~,~)set(navLine, 'MarkerEdgeColor', get(latLine, 'MarkerEdgeColor')));
                end
            end
        end
        
        
        function showHideNavs(this)
            % set Visible on/off and size -1/0 for nav Axes & nav3D axes
            vBoxSizes = this.navListBox.get('Sizes');
            
            %NAV2D
            vBoxSizes = this.showHideNav(this.nav2DBox, this.nav2DAxes, vBoxSizes, 1);
            
            if this.nav3D
                %NAV3D
                vBoxSizes = this.showHideNav(this.nav3DPanel, this.nav3DAxes, vBoxSizes, 2);
            else
                vBoxSizes(2) = 0;
                this.nav3DPanel.Visible = 'off';
            end
            
            this.navListBox.set('Sizes', vBoxSizes);
            
            this.updateAllNav();
        end
        
        function vBoxSizes = showHideNav(this, navPanel, navAxes, vBoxSizes, navAxesIndex)
            
            % show/hide panel
            if (((this.displayedNavPlotValue == 1) || (this.displayedNavPlotValue == 3)) && (navAxesIndex == 1)) || ...
                    (((this.displayedNavPlotValue == 2) || (this.displayedNavPlotValue == 3)) && (navAxesIndex == 2))
                navPanel.Visible = 'on';
                if  vBoxSizes(navAxesIndex) == 0
                    vBoxSizes(navAxesIndex) = -1;
                end
            else
                navPanel.Visible = 'off';
                vBoxSizes(navAxesIndex) = 0;
            end
            
            % show hide line
            [lineList, tmpLineList] = TmpLineUtils.getLines(navAxes);
            for navIndex = 1:numel(this.signalContainerList)
                for sampleIndex=1:numel(this.signalContainerList(navIndex).signalList(1).ySample)
                    lineIndex = this.signalYSampleLineIndex(navIndex, 1, sampleIndex);
                    if lineIndex ~= 0
                        if this.displayedSignalContainerList(navIndex)  && this.displayedSampleList(sampleIndex)
                            lineList(lineIndex).Visible = 'on';
                            lineList(lineIndex).Tag = this.manualCleanDataMode.toNotCleanUnitaryActionLineTagLabel;
                            if ~isempty(tmpLineList)
                                tmpLineList(lineIndex).Visible = 'on';
                            end
                        else
                            lineList(lineIndex).Visible = 'off';
                            lineList(lineIndex).Tag = TmpLineUtils.toNotCleanLineTagLabel;
                            if ~isempty(tmpLineList)
                                tmpLineList(lineIndex).Visible = 'off';
                            end
                        end
                    end
                end
            end
        end
        
        
        %% Callback
        
        function handleCleanEvent(this, ~, cleanDataEventData)
            
            % Read event
            modifiedLine = cleanDataEventData.line;
            modifiedIndex = cleanDataEventData.modifiedIndex;
            
            if this.editDataMode.dirty || this.drawDataMode.dirty
                resetClean = [];
            else
                resetClean = cleanDataEventData.reset;
            end
            
            % Get cleanDataMode
            cleanDataMode = this.getCleanDataMode();
            
            % Create tmpLine on Nav
            if strcmp(this.nav2DBox.Visible, 'on')
                cleanDataMode.createTmpLineList(this.nav2DAxes);
            end
            if strcmp(this.nav3DPanel.Visible, 'on')
                cleanDataMode.createTmpLineList(this.nav3DAxes);
            end
            
            % Search the indexes of modified line
            [modifiedNavIndex, modifiedSignalIndex, modifiedSampleIndex] = this.searchModifiedLine(modifiedLine);
            
            % Check if the navLine is modified
            modifiedNav = false;
            if (modifiedNavIndex == 0) && (modifiedSignalIndex == 0) && (modifiedSampleIndex == 0)
                [modifiedNavIndex, modifiedSampleIndex, modifiedNav] = this.searchModifiedNavLine(modifiedLine);
            end
            
            % If modification on navLine
            if modifiedNav == true
                
                %% Set indexToClean
                if this.algoCleanDataMode.dirty
                    % if algoCleanDataMode, use selectedIndex instead of modifiedIndex to despike all signals
                    selectedIndex = cleanDataEventData.selectedIndex;
                    % despike each signal sync, then return the union of all modified index
                    unionModifiexIndex = this.updateSignalsWithAlgoCleanFromNav(modifiedNavIndex, modifiedSampleIndex,  selectedIndex, resetClean);
                    % clean each signal sync with the union of all modified index
                    indexToClean = unionModifiexIndex;
                elseif this.manualCleanDataMode.dirty
                    indexToClean = cleanDataEventData.selectedIndex;
                else
                    indexToClean = modifiedIndex;
                end
                
                %% update other signals
                if this.filterDataMode.dirty
                    % filter lat & lon  signal
                    this.updateLatLonWithFilterFromNav(modifiedNavIndex, modifiedSampleIndex,  indexToClean, resetClean);
                elseif this.editDataMode.dirty || this.drawDataMode.dirty
                    % update lat & lon
                    values = cleanDataEventData.value;
                    this.updateLatLonWithEditFromNav(modifiedNavIndex, modifiedSampleIndex,  indexToClean, values);
                elseif this.manualCleanDataMode.dirty || this.algoCleanDataMode.dirty
                    % clean each signal sync
                    this.updateSignals(modifiedNavIndex, 0, modifiedSampleIndex,  indexToClean, resetClean);
                end
                
                %% update nav
                this.updateNav(modifiedNavIndex, 0, modifiedSampleIndex,  indexToClean);
                %                 this.updateAllNav()
                
            else % Modification on signals
                if (modifiedSignalIndex == 0) || (this.syncSignalList(modifiedSignalIndex) == 0)
                    % YSample Source not synchronized, nothing to do
                elseif (modifiedSignalIndex ~= 0) && (this.syncSignalList(modifiedSignalIndex) == 1)
                    if this.manualCleanDataMode.dirty || this.algoCleanDataMode.dirty
                        % Update other signals sync
                        this.updateSignals(modifiedNavIndex, modifiedSignalIndex, modifiedSampleIndex,  modifiedIndex, resetClean);
                    end
                    % Update other Nav if needed
                    this.updateNav(modifiedNavIndex, modifiedSignalIndex, modifiedSampleIndex,  modifiedIndex);
                    %                     this.updateAllNav()
                end
            end
            
        end
        
        function [modifiedNavIndex, modifiedSampleIndex, modifiedNav] = searchModifiedNavLine(this, modifiedLine)
            
            modifiedNavIndex = 0;
            modifiedSampleIndex = 0;
            modifiedNav         = false;
            
            % Search modifiedSignalIndex and if Nav modified or Nav3D modified
            for navIndex=1:numel(this.signalContainerList) % for each signalContainer
                for sampleIndex=1:this.ySampleMaxNb % for each sample
                    navLine = this.navLineArray(navIndex, sampleIndex);
                    if isa(navLine, SignalDialog.lineClassName) && isvalid(navLine)
                        if eq(navLine, modifiedLine)
                            modifiedNavIndex = navIndex;
                            modifiedSampleIndex = sampleIndex;
                            modifiedNav = true;
                            break
                        end
                    end
                    
                    if this.nav3D
                        nav3DLine =  this.nav3DLineArray(navIndex);
                        if isa(nav3DLine, SignalDialog.lineClassName) && isvalid(nav3DLine)
                            if eq(nav3DLine, modifiedLine)
                                modifiedNavIndex = navIndex;
                                modifiedSampleIndex = sampleIndex;
                                modifiedNav = true;
                                break
                            end
                        end
                    end
                end
            end
        end
                
        
        function unionModifiexIndex = updateSignalsWithAlgoCleanFromNav(this, modifiedNavIndex, modifiedSampleIndex,  selectedIndex, resetClean)
            cleanDataMode = this.algoCleanDataMode;
            
            % init union of modifiedIndex arrays
            unionModifiexIndex = false(1, numel(selectedIndex));
            
            if ~isempty(cleanDataMode)
                % update other ysample
                for signalIndex=1:this.signalMaxNb
                    if this.syncSignalList(signalIndex) ~= 0 % update signal if not src signal and sync signal
                        
                        axes = this.signalAxesArray(signalIndex);
                        
                        % create tmpLines if needed
                        cleanDataMode.createTmpLineList(axes);
                        
                        [lineList, tmpLineList] = TmpLineUtils.getLines(axes);
                        % Find tmpLine
                        lineIndex = this.signalYSampleLineIndex(modifiedNavIndex, signalIndex, modifiedSampleIndex);
                        if lineIndex ~=0
                            line = lineList(lineIndex);
                            tmpLine = tmpLineList(lineIndex);
                            
                            if this.algoCleanDataMode.dirty && ~resetClean % non reset mode && algorithmic mode
                                % use despike on selectedIndex with
                                modifiedIndex = cleanDataMode.computeCleanedIndexFromSelectedIndex(line, selectedIndex);
                                % Compute union of  modifiedIndex arrays
                                unionModifiexIndex = unionModifiexIndex | modifiedIndex;
                            else
                                modifiedIndex = selectedIndex;
                            end
                            
                            cleanDataMode.cleanDataUnitaryAction(line, tmpLine, modifiedIndex, resetClean);
                            
                        end
                    end
                end
            end
        end
        
        
        function updateLatLonWithFilterFromNav(this, modifiedNavIndex, modifiedSampleIndex,  selectedIndex, resetClean)
            cleanDataMode = this.filterDataMode;
            
            latitudeSignalIndex  = this.signalContainerList(modifiedNavIndex).latitudeSignalIndex;
            longitudeSignalIndex = this.signalContainerList(modifiedNavIndex).longitudeSignalIndex;
            
            if ~isempty(cleanDataMode)
                % update lat & lon ysample
                
                %% Latitude
                axes = this.signalAxesArray(latitudeSignalIndex);
                % create tmpLines if needed
                cleanDataMode.createTmpLineList(axes);
                
                [lineList, tmpLineList] = TmpLineUtils.getLines(axes);
                % Find tmpLine
                lineIndex = this.signalYSampleLineIndex(modifiedNavIndex, latitudeSignalIndex, modifiedSampleIndex);
                if lineIndex ~=0
                    line = lineList(lineIndex);
                    tmpLine = tmpLineList(lineIndex);
                    
                    cleanDataMode.filterDataUnitaryAction(line, tmpLine, selectedIndex, resetClean);
                end
                
                %% Longitude
                axes = this.signalAxesArray(longitudeSignalIndex);
                % create tmpLines if needed
                cleanDataMode.createTmpLineList(axes);
                
                [lineList, tmpLineList] = TmpLineUtils.getLines(axes);
                % Find tmpLine
                lineIndex = this.signalYSampleLineIndex(modifiedNavIndex, longitudeSignalIndex, modifiedSampleIndex);
                if lineIndex ~=0
                    line = lineList(lineIndex);
                    tmpLine = tmpLineList(lineIndex);
                    
                    cleanDataMode.filterDataUnitaryAction(line, tmpLine, selectedIndex, resetClean);
                end
            end
        end
        
        function updateLatLonWithEditFromNav(this, modifiedNavIndex, modifiedSampleIndex,  modifiedIndex, values)
            
            cleanDataMode = this.getCleanDataMode();  % EditDataMode or DrawDataMode
            
            %latitude line
            latitudeSignalIndex  = this.signalContainerList(modifiedNavIndex).latitudeSignalIndex;
            latAxes = this.signalAxesArray(latitudeSignalIndex);
            cleanDataMode.createTmpLineList(latAxes);
            [latLineList, ~] = TmpLineUtils.getLines(latAxes);
            latLineIndex = this.signalYSampleLineIndex(modifiedNavIndex, latitudeSignalIndex, modifiedSampleIndex);
            latLine = latLineList(latLineIndex);
            
            latLine.YData(modifiedIndex) = values;
        end
        
        %         function updateNav(this, modifiedNavIndex, modifiedSignalIndex, modifiedSampleIndex, modifiedIndex)
        %             cleanDataMode = [];
        %             if this.manualCleanDataMode.dirty % Manual CleanMode
        %                 cleanDataMode = this.manualCleanDataMode;
        %             elseif this.algoCleanDataMode.dirty % Algo CleanMode
        %                 cleanDataMode = this.algoCleanDataMode;
        %             elseif this.editDataMode.dirty % EditDataMode
        %                 cleanDataMode = this.editDataMode;
        %             elseif this.filterDataMode.dirty % FilterDataMode
        %                 cleanDataMode = this.filterDataMode;
        %             elseif this.drawDataMode.dirty % DrawDataMode
        %                 cleanDataMode = this.drawDataMode;
        %             end
        %
        %             if (~isempty(cleanDataMode))
        %                 latitudeSignalIndex  = this.signalContainerList(modifiedNavIndex).latitudeSignalIndex;
        %                 longitudeSignalIndex = this.signalContainerList(modifiedNavIndex).longitudeSignalIndex;
        %                 immersionSignalIndex = this.signalContainerList(modifiedNavIndex).immersionSignalIndex;
        %
        %                 if (modifiedSignalIndex ~= 0)
        %                     isSrcAndLatLonSync = false;
        %                     if  (this.syncSignalList(modifiedSignalIndex) == 1 ...
        %                             && (this.syncSignalList(latitudeSignalIndex) == 1 ...
        %                             ||  this.syncSignalList(longitudeSignalIndex) == 1 ...
        %                             ||  this.syncSignalList(immersionSignalIndex) == 1))
        %                         isSrcAndLatLonSync = true;
        %                     end
        %                 end
        %                 % Check if latitude, longitude modified
        %                 if (modifiedSignalIndex == latitudeSignalIndex ...
        %                         || modifiedSignalIndex == longitudeSignalIndex ...
        %                         || modifiedSignalIndex == immersionSignalIndex ...
        %                         || modifiedSignalIndex == 0 ...
        %                         || isSrcAndLatLonSync)
        %
        %
        %
        %                     %% Retrieve latitude & longitude line
        %                     %latitude line
        %                     latAxes = this.signalAxesArray(latitudeSignalIndex);
        %                     [latLineList, latTmpLineList] = TmpLineUtils.getLines(latAxes);
        %                     latLineIndex = this.signalYSampleLineIndex(modifiedNavIndex, latitudeSignalIndex, modifiedSampleIndex);
        %                     latLine = latLineList(latLineIndex);
        %                     %longitude line
        %                     lonAxes = this.signalAxesArray(longitudeSignalIndex);
        %                     [lonLineList, lonTmpLineList] = TmpLineUtils.getLines(lonAxes);
        %                     lonLineIndex = this.signalYSampleLineIndex(modifiedNavIndex, longitudeSignalIndex, modifiedSampleIndex);
        %                     lonLine = lonLineList(lonLineIndex);
        %                     %immersion line (optional)
        %                     if (immersionSignalIndex ~= 0)
        %                         immAxes = this.signalAxesArray(immersionSignalIndex);
        %                         [immLineList, immTmpLineList] = TmpLineUtils.getLines(immAxes);
        %                         immLineIndex = this.signalYSampleLineIndex(modifiedNavIndex, immersionSignalIndex, modifiedSampleIndex);
        %                         immLine = immLineList(immLineIndex);
        %                     end
        %
        %                     % Update Nav Plot
        %                     axes = this.nav2DAxes;
        %                     % create tmpLines if needed
        %                     cleanDataMode.createTmpLineList(axes);
        %                     [lineList, tmpLineList] = TmpLineUtils.getLines(axes);
        %                     lineIndex = this.signalYSampleLineIndex(modifiedNavIndex, 1, modifiedSampleIndex);
        %                     if lineIndex ~= 0
        %                         % Find tmpLine
        %                         navLine = lineList(lineIndex);
        %                         navLine.YData(modifiedIndex) = latLine.YData(modifiedIndex);
        %                         navLine.XData(modifiedIndex) = lonLine.YData(modifiedIndex);
        %
        %                         if this.nav3D
        %                             %FIXME : no update if nav3d hidden!!!!
        %
        %
        %                             % Update Nav3D Plot
        %                             axes = this.nav3DAxes;
        %                             % create tmpLines if needed
        %                             cleanDataMode.createTmpLineList(axes);
        %                             [lineList, tmpLineList] = TmpLineUtils.getLines(axes);
        %                             % Find tmpLine
        %                             nav3DLine = lineList(lineIndex);
        %                             nav3DLine.XData(modifiedIndex) = latLine.YData(modifiedIndex);
        %                             nav3DLine.YData(modifiedIndex) = lonLine.YData(modifiedIndex);
        %                             nav3DLine.ZData(modifiedIndex) = immLine.YData(modifiedIndex);
        %                         end
        %                     end
        %                 end
        %             end
        %         end
        
        
        function updateNav(this, modifiedNavIndex, modifiedSignalIndex, modifiedSampleIndex, modifiedIndex)
            
            % Lat/Lon/Imm Index
            latitudeSignalIndex  = this.signalContainerList(1).latitudeSignalIndex;
            longitudeSignalIndex = this.signalContainerList(1).longitudeSignalIndex;
            immersionSignalIndex = this.signalContainerList(1).immersionSignalIndex;
            
            if (modifiedSignalIndex ~= 0)
                isSrcAndLatLonSync = false;
                if  (this.syncSignalList(modifiedSignalIndex) == 1 ...
                        && (this.syncSignalList(latitudeSignalIndex) == 1 ...
                        ||  this.syncSignalList(longitudeSignalIndex) == 1 ...
                        ||  this.syncSignalList(immersionSignalIndex) == 1))
                    isSrcAndLatLonSync = true;
                end
            end
            % Check if latitude, longitude modified
            if (modifiedSignalIndex == latitudeSignalIndex ...
                    || modifiedSignalIndex == longitudeSignalIndex ...
                    || modifiedSignalIndex == immersionSignalIndex ...
                    || modifiedSignalIndex == 0 ...
                    || isSrcAndLatLonSync)
                
                
                
                
                %% Retrieve latitude & longitude & Immersion lineLists
                %latitude line
                latAxes = this.signalAxesArray(latitudeSignalIndex);
                [latLineList, ~] = TmpLineUtils.getLines(latAxes);
                %longitude line
                lonAxes = this.signalAxesArray(longitudeSignalIndex);
                [lonLineList, ~] = TmpLineUtils.getLines(lonAxes);
                %immersion line (optional)
                if (immersionSignalIndex ~= 0)
                    immAxes = this.signalAxesArray(immersionSignalIndex);
                    [immLineList, ~] = TmpLineUtils.getLines(immAxes);
                end
                
                
                % Re draw nav
                if strcmp(this.nav2DBox.Visible, 'on')
                    axes = this.nav2DAxes;
                    [lineList, ~] = TmpLineUtils.getLines(axes);
                    if ~isempty(lineList)
                        % Find tmpLine
                        lineIndex = this.signalYSampleLineIndex(modifiedNavIndex, 1, modifiedSampleIndex);
                        if lineIndex ~= 0
                            navLine = lineList(lineIndex);
                            % Latitude line
                            latLineIndex = this.signalYSampleLineIndex(modifiedNavIndex, latitudeSignalIndex, modifiedSampleIndex);
                            latLine = latLineList(latLineIndex);
                            % Longitude Line
                            lonLineIndex = this.signalYSampleLineIndex(modifiedNavIndex, longitudeSignalIndex, modifiedSampleIndex);
                            lonLine = lonLineList(lonLineIndex);
                            % Modify NavLine
                            navLine.YData(modifiedIndex) = latLine.YData(modifiedIndex);
                            navLine.XData(modifiedIndex) = lonLine.YData(modifiedIndex);
                        end
                    end
                end
                
                if strcmp(this.nav3DPanel.Visible, 'on')
                    if this.nav3D
                        axes = this.nav3DAxes;
                        [lineList, ~] = TmpLineUtils.getLines(axes);
                        if ~isempty(lineList)
                            % Find tmpLine
                            lineIndex = this.signalYSampleLineIndex(modifiedNavIndex, 1, modifiedSampleIndex);
                            if lineIndex ~= 0
                                nav3DLine = lineList(lineIndex);
                                % Latitude line
                                latLineIndex = this.signalYSampleLineIndex(modifiedNavIndex, latitudeSignalIndex, modifiedSampleIndex);
                                latLine = latLineList(latLineIndex);
                                % Longitude Line
                                lonLineIndex = this.signalYSampleLineIndex(modifiedNavIndex, longitudeSignalIndex, modifiedSampleIndex);
                                lonLine = lonLineList(lonLineIndex);
                                % Immersion Line
                                immLineIndex = this.signalYSampleLineIndex(modifiedNavIndex, immersionSignalIndex, modifiedSampleIndex);
                                immLine = immLineList(immLineIndex);
                                % Modify NavLine
                                nav3DLine.XData(modifiedIndex) = latLine.YData(modifiedIndex);
                                nav3DLine.YData(modifiedIndex) = lonLine.YData(modifiedIndex);
                                nav3DLine.ZData(modifiedIndex) = immLine.YData(modifiedIndex);
                            end
                        end
                    end
                end
            end
        end
        
        
        
        function updateAllNav(this)
            
            % Lat/Lon/Imm Index
            latitudeSignalIndex  = this.signalContainerList(1).latitudeSignalIndex;
            longitudeSignalIndex = this.signalContainerList(1).longitudeSignalIndex;
            immersionSignalIndex = this.signalContainerList(1).immersionSignalIndex;
            
            %% Retrieve latitude & longitude & Immersion lineLists
            %latitude line
            latAxes = this.signalAxesArray(latitudeSignalIndex);
            [latLineList, ~] = TmpLineUtils.getLines(latAxes);
            %longitude line
            lonAxes = this.signalAxesArray(longitudeSignalIndex);
            [lonLineList, ~] = TmpLineUtils.getLines(lonAxes);
            %immersion line (optional)
            if (immersionSignalIndex ~= 0)
                immAxes = this.signalAxesArray(immersionSignalIndex);
                [immLineList, ~] = TmpLineUtils.getLines(immAxes);
            end
            
            % Re draw nav
            if strcmp(this.nav2DBox.Visible, 'on')
                axes = this.nav2DAxes;
                [lineList, ~] = TmpLineUtils.getLines(axes);
                if ~isempty(lineList)
                    for navIndex=1:numel(this.signalContainerList)
                        for sampleIndex=1:numel(this.signalContainerList(navIndex).signalList(1).ySample)
                            % Find tmpLine
                            lineIndex = this.signalYSampleLineIndex(navIndex, 1, sampleIndex);
                            if lineIndex ~= 0
                                navLine = lineList(lineIndex);
                                % Latitude line
                                latLineIndex = this.signalYSampleLineIndex(navIndex, latitudeSignalIndex, sampleIndex);
                                latLine = latLineList(latLineIndex);
                                % Longitude Line
                                lonLineIndex = this.signalYSampleLineIndex(navIndex, longitudeSignalIndex, sampleIndex);
                                lonLine = lonLineList(lonLineIndex);
                                % Modify NavLine
                                navLine.YData = latLine.YData;
                                navLine.XData = lonLine.YData;
                            end
                        end
                    end
                end
            end
            
            if strcmp(this.nav3DPanel.Visible, 'on')
                if this.nav3D
                    axes = this.nav3DAxes;
                    [lineList, ~] = TmpLineUtils.getLines(axes);
                    if ~isempty(lineList)
                        for navIndex=1:numel(this.signalContainerList)
                            for sampleIndex=1:numel(this.signalContainerList(navIndex).signalList(1).ySample)
                                % Find tmpLine
                                lineIndex = this.signalYSampleLineIndex(navIndex, 1, sampleIndex);
                                if lineIndex ~= 0
                                    nav3DLine = lineList(lineIndex);
                                    % Latitude line
                                    latLineIndex = this.signalYSampleLineIndex(navIndex, latitudeSignalIndex, sampleIndex);
                                    latLine = latLineList(latLineIndex);
                                    % Longitude Line
                                    lonLineIndex = this.signalYSampleLineIndex(navIndex, longitudeSignalIndex, sampleIndex);
                                    lonLine = lonLineList(lonLineIndex);
                                    % Immersion Line
                                    immLineIndex = this.signalYSampleLineIndex(navIndex, immersionSignalIndex, sampleIndex);
                                    immLine = immLineList(immLineIndex);
                                    % Modify NavLine
                                    nav3DLine.XData = latLine.YData;
                                    nav3DLine.YData = lonLine.YData;
                                    nav3DLine.ZData = immLine.YData;
                                end
                            end
                        end
                    end
                end
            end
        end
        
        function showSignalContainerSelectionDialog(this, ~, ~)
            showSignalContainerSelectionDialog@SignalDialog(this);
            % re-draw nav
            this.showHideNavs();
            % update postion of rectangle
            this.updateLatLonSelection();
        end
        
        
        function showSignalSelectionDialog(this, ~, ~)
            showSignalSelectionDialog@SignalDialog(this);
            
            % re-draw nav
            this.showHideNavs();
            
            % update postion of rectangle
            this.updateLatLonSelection();
            
        end
        
        function xLimModeAutoSignalsCallback(this, src, ~)
            % XLimMode auto
            xLimModeAutoSignalsCallback@SignalDialog(this, src);
            % update postion of rectangle
            this.updateLatLonSelection();
        end
        
        function yLimModeAutoSignalsCallback(this, src, ~)
            % YLimMode auto
            yLimModeAutoSignalsCallback@SignalDialog(this, src);
            % update postion of rectangle
            this.updateLatLonSelection();
        end
        
        
        function shiftSignalsCallback(this, src, evnt, isShiftToRight)
            % Shif Signal Callback
            
            shiftSignalsCallback@SignalDialog(this, src, evnt, isShiftToRight);
            % update postion of rectangle
            this.updateLatLonSelection();
        end
        
        function navContextMenuItemCallback(this, src, ~)
            % navContextMenuItem Callback
            
            this.navContextMenuItemList(1).set('Checked', 'off');
            this.navContextMenuItemList(2).set('Checked', 'off');
            this.navContextMenuItemList(3).set('Checked', 'off');
            
            [~,index] = ismember(src, this.navContextMenuItemList);
            
            this.navContextMenuItemList(index).set('Checked', 'on');
            this.displayedNavPlotValue = index;
            
            % re-draw nav
            this.showHideNavs();
        end
    end
end
