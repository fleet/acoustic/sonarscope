classdef OptimDialog < handle & SimpleTitleDialog
    % Description
    %   Class OptimDialog
    %
    % Syntax
    %   a = OptimDialog(...)
    %
    % ParametreDialog properties :
    %   optim         - Optim Data Model
    %   selectedModel - the selected Model Data Model
    %
    % Output Arguments
    %   s : One OptimDialog instance
    %
    % Examples
    %   optim = ExClOptimBS;
    %   a = OptimDialog(optim, 'Title', 'Optim ExClOptimBS', 'windowStyle', 'normal');
    %   a.openDialog();
    %
    % Authors  : MHO
    % See also : ClOptim ClParametre ParametreComponent Authors
    % ----------------------------------------------------------------------------
    
    properties
        optim;
        selectedModel
    end
    
    properties (Access=private)
        %% ui
        xMinAlgoEdit
        xMaxAlgoEdit
        selectModelPopupMenu
        rmsEdit
        parametrePanel
        parametreListComponent
        loadModelContextMenuButton
        loadModelContextMenu
        loadModelContextMenuItemList = matlab.ui.container.Menu.empty;
        deleteModelContextMenuButton
        deleteModelContextMenu
        deleteModelContextMenuItemList = matlab.ui.container.Menu.empty;
        
        % model box/axes/curves
        modelAxesBox
        modelAxes
        modelCurves
        minModelPatch
        maxModelPatch
        % weights box/axes/curves
        weightsAxesBox
        weightsAxes
        weightsCurves
        minWeightsPatch
        maxWeightsPatch
        
        %data
        xMinValue
        xMaxValue
        xMinAlgoValue
        xMaxAlgoValue
        loadModelContextMenuItemValue   = 1 % (1 = manually, 2 = one of best, 3 = the best)
        deleteModelContextMenuItemValue = 1 % (1 = manually, 2 = all, 3 = one of best, 3 = the best)
        savedModelList = ClModel.empty
        showModelComponentsValue = 1;
        showModelLegendValue     = 0;
    end
    
    methods
        function this = OptimDialog(optim, varargin)
            % Superclass Constructor
            this = this@SimpleTitleDialog('resizable', 'on', varargin{:});
            if isempty(this.Title)
                this.Title = 'Optim Title';
            end
            this.optim = optim;
        end
        
    end
    
    methods (Access = protected)
        function [widthBody ,heightBody] = createBody(this, parent)
            % Create Body of optimDialog
            
            horizontalBox = uiextras.HBoxFlex('Parent', parent, 'Spacing', 5);
            this.createLeftBox(horizontalBox);
            this.createRightBox(horizontalBox);
            horizontalBox.set('Sizes', [500 -1], 'MinimumSizes', [200 200]);
            widthBody  = 1000;
            heightBody = 700;
        end
        
        function createLeftBox(this, parent)
            leftBox = uiextras.VBox('Parent', parent, 'Spacing', 5);
            this.createOptimBox(leftBox);
            this.createModelBox(leftBox);
            leftBox.set('Sizes', [175 -1]);
        end
        
        function createOptimBox(this, parent)
            optimPanel = uipanel('Parent', parent, ...
                'Title', 'Optimization parameters');
            
            %OptimGrid
            optimGrid = uiextras.Grid( 'Parent', optimPanel, 'Padding', 5, 'Spacing', 15);
            
            % Select Algo
            RadioButtonsComponent(optimGrid, 'orientation', RadioButtonsComponent.Orientation{2}, ...
                'title', 'Algorithme Type : ', 'titleButtons', {this.optim.AlgoTypeStr{1}, this.optim.AlgoTypeStr{2}}, ...
                'Init', this.optim.AlgoType, 'callbackFunc',@this.algoTypeCallback);
            
            % MinMaxGrid
            optimMinMaxGrid = uiextras.Grid('Parent', optimGrid);
            uicontrol('Parent', optimMinMaxGrid, ...
                'Style',               'text', ...
                'string',              'Xmin Algorithm :', ...
                'HorizontalAlignment', 'left');
            uicontrol('Parent', optimMinMaxGrid, ...
                'Style',               'text', ...
                'string',              'Xmax Algorithm :', ...
                'HorizontalAlignment', 'left');
            this.xMinAlgoEdit = uicontrol('Parent', optimMinMaxGrid, ...
                'Style',   'edit', ...
                'string',  '###', ...
                'Callback', @this.algoXMinCallback);
            
            this.xMaxAlgoEdit = uicontrol('Parent', optimMinMaxGrid, ...
                'Style',   'edit', ...
                'string',  '###', ...
                'Callback', @this.algoXMaxCallback);
            optimMinMaxGrid.set('ColumnSizes', [-1 -1], 'RowSizes', [-1 -1] );
            
            %             uiextras.Empty('Parent', optimGrid);
            uicontrol('parent', optimGrid, ...
                'Style',               'checkbox', ...
                'String',              'Visualize intermediate results', ...
                'Value',               this.optim.UseVisu, ...
                'HorizontalAlignment', 'right', ...
                'Callback',            @this.useVisuResultsCallback);
            
            % CheckBoxesGrid
            optimCheckVBox = uiextras.VBox('Parent', optimGrid);
            uicontrol('parent', optimCheckVBox, ...
                'Style',    'checkbox', ...
                'String',   'Use Simulation Annealing', ...
                'Value',    this.optim.UseSimAnneal , ...
                'Callback', @this.useSimAnnealCallback);
            
            uicontrol('parent', optimCheckVBox, ...
                'Style',    'checkbox', ...
                'String',   'Use Dependences', ...
                'Value',    this.optim.UseDependences, ...
                'Callback', @this.useDependencesCallback);
            
            uicontrol('parent', optimCheckVBox, ...
                'Style',               'checkbox', ...
                'String',              'Use Weights', ...
                'Value',               this.optim.UseWeights, ...
                'HorizontalAlignment', 'center', ...
                'Callback',            @this.useWeightsCallback);
            
            
            optimIterVisuVBox = uiextras.VBox('Parent', optimGrid, 'Spacing', 1);
            
            uiextras.Empty('Parent', optimIterVisuVBox);
            % Select Iteration
            optimIterHBox = uiextras.HBox('Parent', optimIterVisuVBox);
            uicontrol('Parent', optimIterHBox, ...
                'Style',  'text', ...
                'string', 'Number of Iterations :');
            
            uicontrol('Parent', optimIterHBox, ...
                'Style',    'edit', ...
                'string',   num2str(this.optim.MaxIter), ...
                'Callback', @this.algoIterCallback);
            
            optimLaunchHBox = uiextras.HBox('Parent', optimGrid, 'Spacing', 1);
            uiextras.Empty('Parent', optimLaunchHBox);
            uicontrol('Parent', optimLaunchHBox, ...
                'Style',   'pushButton', ...
                'string',  'Launch', ...
                'Callback', @this.launchAlgoCallback);
            optimLaunchHBox.set('Sizes', [-1 100]);
            
            optimGrid.set('ColumnSizes', [-1 -1], 'RowSizes', [-1.5 -1 30] );
        end
        
        function createModelBox(this, parent)
            modelPanel = uiextras.Panel('Parent', parent,  'Title', 'Model');
%             modelPanel = uix.Panel('Parent', parent,  'Title', 'Model');
            modelBox = uiextras.VBox('Parent', modelPanel, 'Padding', 5, 'Spacing', 5);
            
            % Select Model
            selectModelBox = uiextras.HBox('Parent', modelBox, 'Spacing', 2);
            uicontrol('Parent', selectModelBox, ...
                'Style',                'text', ...
                'string',               'Selected Model : ', ...
                'HorizontalAlignment', 'left');
            
            for k=length(this.optim.models):-1:1
                modelNameList{k} = this.optim.models(k).Name;
            end
            this.selectModelPopupMenu = uicontrol('parent', selectModelBox, ...
                'Style',    'popupmenu', ...
                'String',   modelNameList, ...
                'Value',    this.optim.currentModel, ...
                'Callback', @this.selectModelCallback);
            this.selectedModel = this.optim.models(this.optim.currentModel);
            
            uicontrol('Parent', selectModelBox, ...
                'Style',               'text', ...
                'HorizontalAlignment', 'right', ...
                'string',              'RMS : '); %  'VerticalAlignment', 'middle', ...
            
            this.rmsEdit = uicontrol('Parent', selectModelBox, ...
                'Style',  'edit', ...
                'Enable', 'off', ...
                'string', '###');
            
            selectModelBox.set('Sizes', [-1 -1 -1 -1]);
            
            % Model Action Buttons BOx (Save/load/Delete/random)
            modelActionsBox = uiextras.HBox('Parent', modelBox, 'Spacing', 15);
            
            % Save Model
            uicontrol('Parent', modelActionsBox, ...
                'Style',    'pushButton', ...
                'String',   'Save', ...
                'Callback', @this.saveModelCallback);
            
            % Load Model DropDown Button
            %ContextMneu
            this.loadModelContextMenu = uicontextmenu(this.parentFigure);
            menuItemChecked = {'off', 'off', 'off'};
            menuItemChecked{this.loadModelContextMenuItemValue} = 'on';
            this.loadModelContextMenuItemList = matlab.ui.container.Menu.empty;
            this.loadModelContextMenuItemList(1) = uimenu(this.loadModelContextMenu, ...
                'Label', 'Load manually saved model', 'Checked', menuItemChecked{1}, ...
                'Callback', @this.loadModelContextMenuItemCallback);
            this.loadModelContextMenuItemList(2) = uimenu(this.loadModelContextMenu, ...
                'Label', 'Load one best model',       'Checked', menuItemChecked{2}, ...
                'Callback', @this.loadModelContextMenuItemCallback);
            this.loadModelContextMenuItemList(3) = uimenu(this.loadModelContextMenu, ...
                'Label', 'Load the best model',       'Checked', menuItemChecked{3}, ...
                'Callback', @this.loadModelContextMenuItemCallback);
            %DropDown bouton
            DropDownButtonComponent(modelActionsBox,this.parentFigure, 'Load', @this.loadModelCallback, this.loadModelContextMenu);
            
            
            % Delete Model DropDown Button
            %ContextMneu
            this.deleteModelContextMenu = uicontextmenu(this.parentFigure);
            menuItemChecked = {'off', 'off', 'off', 'off'};
            menuItemChecked{this.deleteModelContextMenuItemValue} = 'on';
            this.deleteModelContextMenuItemList(1) = uimenu(this.deleteModelContextMenu, ...
                'Label', 'Delete one manually saved model',                'Checked', menuItemChecked{1}, ...
                'Callback', @this.deleteModelContextMenuItemCallback);
            this.deleteModelContextMenuItemList(2) = uimenu(this.deleteModelContextMenu, ...
                'Label', 'Delete all except the best models of each type', 'Checked', menuItemChecked{2}, ...
                'Callback', @this.deleteModelContextMenuItemCallback);
            this.deleteModelContextMenuItemList(3) = uimenu(this.deleteModelContextMenu, ...
                'Label', 'Delete all except the best model',               'Checked', menuItemChecked{3}, ...
                'Callback', @this.deleteModelContextMenuItemCallback);
            this.deleteModelContextMenuItemList(4) = uimenu(this.deleteModelContextMenu, ...
                'Label', 'Delete all saved models',                        'Checked', menuItemChecked{4}, ...
                'Callback', @this.deleteModelContextMenuItemCallback);
            %DropDown bouton
            DropDownButtonComponent(modelActionsBox,this.parentFigure, 'Delete', @this.deleteModelCallback, this.deleteModelContextMenu);
            
            % Push Button random
            uicontrol('Parent', modelActionsBox, ...
                'Style',    'pushButton', ...
                'string',   'Random', ...
                'Callback', @this.randomModelCallback);
            
            modelActionsBox.set('Sizes', [-1 -1 -1 -1]);
            
            this.parametrePanel = uipanel('Parent', modelBox, ...
                'Title', 'Model Parameters');
            
            % Create  parametre components
            this.createModelParametreComponents();
            
            modelBox.set('Sizes', [25 25 -1]);
        end
        
        function createRightBox(this, parent)
            rightPanel = uiextras.Panel('Parent', parent, 'Title', 'Visualization');
%             rightPanel = uix.Panel('Parent', parent, 'Title', 'Visualization');
            rightBox   = uiextras.VBox( 'Parent', rightPanel);
            
            % Create axes toolbar box
            ToolBarModeComponent(rightBox);
            
            % Model Axes Box
            this.modelAxesBox = uiextras.VBox('Parent', rightBox);
            % Draw Model Axes
            this.createModelAxes();
            
            % Init xMin/xMax/xMinAlgo/xMaxAlgo Value/component of Data Axes
            minMaxDataAxes = axis(this.modelAxes);
            this.xMinValue     = minMaxDataAxes(1);
            this.xMaxValue     = minMaxDataAxes(2);
            this.xMinAlgoValue = minMaxDataAxes(1);
            this.xMaxAlgoValue = minMaxDataAxes(2);
            this.xMinAlgoEdit.set('String', minMaxDataAxes(1));
            this.xMaxAlgoEdit.set('String', minMaxDataAxes(2));
            
            % Weights Axes Box
            this.weightsAxesBox = uiextras.VBox('Parent', rightBox);
            % Draw Weights Axes
            this.createWeightsAxes();
            
            % Create patch for model/weight axes
            this.minModelPatch = patch([this.xMinValue this.xMinValue this.xMinAlgoValue this.xMinAlgoValue], ...
                [-1e3 1e3 1e3 -1e3], [0.7 0.7 0.7], ...
                'Parent',    this.modelAxes, ...
                'FaceAlpha', 0.5);
            
            this.maxModelPatch = patch([ this.xMaxValue  this.xMaxValue this.xMaxAlgoValue this.xMaxAlgoValue], ...
                [-1e3 1e3 1e3 -1e3], [0.7 0.7 0.7], ...
                'Parent',    this.modelAxes, ...
                'FaceAlpha', 0.5);
            % Recadrage
            axis(this.modelAxes,[this.xMinValue this.xMaxValue minMaxDataAxes(3) minMaxDataAxes(4)]);
            
            
            this.minWeightsPatch = patch([this.xMinValue this.xMinValue this.xMinAlgoValue this.xMinAlgoValue], ...
                [-1e3 1e3 1e3 -1e3], [0.7 0.7 0.7], ...
                'Parent',    this.weightsAxes, ...
                'FaceAlpha', 0.5);
            
            this.maxWeightsPatch = patch([this.xMaxValue  this.xMaxValue this.xMaxAlgoValue this.xMaxAlgoValue], ...
                [-1e3 1e3 1e3 -1e3], [0.7 0.7 0.7], ...
                'Parent',    this.weightsAxes, ...
                'FaceAlpha', 0.5);
            
            axesActionsBox = uiextras.HBox('Parent', rightBox, 'Spacing', 5);
            
            uicontrol('parent', axesActionsBox, ...
                'Style',               'checkbox', ...
                'String',              'Show Model Components', ...
                'Value',               this.showModelComponentsValue, ...
                'HorizontalAlignment', 'center', ...
                'Callback',            @this.showModelComponentsCallback);
            
            %             uiextras.Empty('parent', axesActionsBox);
            %             uicontrol('parent', axesActionsBox, ...
            %                 'Style',    'checkbox', ...
            %                 'String',    'Show Model Legend', ...
            %                 'Value',    this.showModelLegendValue, ...
            %                 'HorizontalAlignment', 'center', ...
            %                 'Callback', @this.showModelLegendCallback);
            
            uicontrol('parent', axesActionsBox, ...
                'Style',               'pushbutton', ...
                'String',              'Clean Data', ...
                'HorizontalAlignment', 'center', ...
                'Callback',            @this.cleanDataCallback);
            
            axesActionsBox.set('Sizes', [-1 100]);
            
            rightBox.set('Sizes', [30 -4 -1 30]);
        end
        
        function selectModelInOptim(this, modelIndex)
            % Update selected Model
            this.selectedModel = this.optim.models(modelIndex);
            % Update rms
            this.selectedModel.rms(this.optim.UseWeights);
            % Update current model index
            this.optim.currentModel = modelIndex;
            % Delete old parametres component panel
            delete(this.parametrePanel.Children(1));
%             this.parametreListComponent.parametrecomponents = ParametreComponent.empty;
            % Create  parametre components
            this.createModelParametreComponents();
            % Plot the model axe
            this.createModelAxes();
            this.createWeightsAxes();
        end
        
        function createModelParametreComponents(this)
            % update rms Text
            this.rmsEdit.set('String', num2str(this.selectedModel.rms(this.optim.UseWeights)));
            
            this.parametreListComponent = ParametreListComponent(this.parametrePanel,  this.selectedModel.Params, this.parentFigure);
%             this.parentFigure.set('ResizeFcn',@this.parametreListComponent.doResizeFcn);
               
            % For parametre component, add listener
            
%            for k=1:length(this.parametreListComponent.parametrecomponents())

            % Modif JMA le 01/03/2017 pour �viter message suivant
%             Warning: A value of class "ParametreComponent" was indexed with no subscripts specified. Currently the result of this
%             operation is the indexed value itself, but in a future release, it will be an error. 
%             > In OptimDialog/createModelParametreComponents (line 386)

            for k=1:length(this.parametreListComponent.parametrecomponents)
                % Add a listener for Value change event
                addlistener(this.parametreListComponent.parametrecomponents(k), 'ValueChange', @this.handleParametreValueChange);
            end
        end
 
        function updateModelParametreComponents(this)
            
            % update rms Text
            this.rmsEdit.set('String', num2str(this.selectedModel.rms(this.optim.UseWeights)));
            
            for k=1:length(this.parametreListComponent.parametrecomponents)
                this.parametreListComponent.parametrecomponents(k).updateComponentValue();
            end
        end
        
        function createModelAxes(this)
            % Create the model Axes
            
            % create new Axes
            newModelAxes = axes('Parent', this.modelAxesBox);
            this.selectedModel.set('XData', this.optim.XData, 'YData', this.optim.YData); %FIXEME XData/YData dans Signal%%%%%%%%%%%%%%%%%%
            % Create curves
            [~,~,this.modelCurves] = this.selectedModel.plot('Axe', newModelAxes, 'Erase', false);
            % Show/hide model components
            this.showModelComponents();
            
            % if old axes exists and not deleted
            if ~isempty(this.modelAxes) && this.modelAxes.isvalid()
                
                % Set new parents to patchs
                this.minModelPatch.set('Parent', newModelAxes);
                this.maxModelPatch.set('Parent', newModelAxes);
                
                % delete old Axes
                delete(this.modelAxes);
            end
            % Set the new axes
            this.modelAxes =  newModelAxes;
        end
        
        function updateModelAxes(this)
            % Update the model axe
            
            % Recompute model
            [y, components] = this.selectedModel.compute;
            set(this.modelCurves.Model(1), 'YData', y);
            for k=1:size(components, 1)
                set(this.modelCurves.Components(1,k), 'YData', components(k,:));
            end
        end
        
        function showModelComponents(this)
            % Show / Hide model components
            
            if this.showModelComponentsValue
                visible = 'on';
            else
                visible = 'off';
            end
            
            for k=1:length(this.modelCurves.Components)
                set(this.modelCurves.Components(1,k), 'Visible', visible);
                %                 this.modelCurves.Components(k).set('Visible', visible);
            end
        end
        
        function createWeightsAxes(this)
            % Create the weights Axes
            
            % create new Axes
            if ~ishandle(this.weightsAxesBox)
                return
            end
            newWeightsAxes = axes('Parent', this.weightsAxesBox);
            this.weightsCurves = plot(newWeightsAxes, this.optim.XData, this.selectedModel.WeightsData);
            
            % Set line style of weights curves
            this.setLineStyleWeights(this.optim.UseWeights);
            
            % Reaffichage infos et grille
            title(newWeightsAxes, ['Weights (' this.selectedModel.WeightsTypeStr{this.selectedModel.WeightsType} ')']);
            xlabel(newWeightsAxes, [this.selectedModel.xLabel ' (' this.selectedModel.xUnit ')']);
            grid(newWeightsAxes,'on')
            
            % Save min max of weights axes
            minMaxWeightsAxes = axis(newWeightsAxes);
            
            % Recadrage
            axis(newWeightsAxes, [this.xMinValue this.xMaxValue minMaxWeightsAxes(3) minMaxWeightsAxes(4)]);
            
            % Link axes
            linkaxes([newWeightsAxes, this.modelAxes], 'x');
            
            % if old axes exists and not deleted
            if ~isempty(this.weightsAxes) && this.weightsAxes.isvalid()
                
                % Set new parents to patchs
                this.minWeightsPatch.set('Parent', newWeightsAxes);
                this.maxWeightsPatch.set('Parent', newWeightsAxes);
                
                % delete old Axes
                delete(this.weightsAxes);
            end
            % Set the new axes
            this.weightsAxes =  newWeightsAxes;
        end
        
        function setLineStyleWeights(this, useWeights)
            % Set line style of weights curves
            if useWeights
                weightsLineStyle = '-';
            else
                weightsLineStyle = ':';
            end
            this.weightsCurves.set('LineStyle', weightsLineStyle);
        end
        
        %% Callbacks
        
        function algoTypeCallback(this, src, ~)
            % Algo Type Callback
            
            %   Algo - Type of the optimization algorithm {1='Simplex'}; 2='Newton'
            if strcmp(src.String ,this.optim.AlgoTypeStr{1})
                this.optim.AlgoType = 1;
            elseif strcmp(src.String, this.optim.AlgoTypeStr{2})
                this.optim.AlgoType = 2;
            end
        end
        
        function algoIterCallback(this, src, ~)
            % Algo Iteration Callback
            value = str2num(src.String); %#ok<ST2NM>
            if isempty(value)
                my_warndlg(['OptimDialog.algoIterCallback : value must be numerical ', src.String], 0);
                src.String = this.optim.MaxIter;
            else
                this.optim.MaxIter = value;
            end
        end
        
        function useSimAnnealCallback(this, src, ~)
            % Use Simulation Annealing Callback
            this.optim.UseSimAnneal = src.Value;
        end
        
        function useDependencesCallback(this, src, ~)
            % Use DependencesCallback
            this.optim.UseDependences = src.Value;
        end
        
        function useWeightsCallback(this, src, ~)
            % Use Weights Callback
            this.optim.UseWeights = src.Value;
            % Set line style of weights curves
            this.setLineStyleWeights(this.optim.UseWeights);
        end
        
        function algoXMinCallback(this, src, ~)
            % Algo X Min Callback
            
            old_Xmin_Algo = this.xMinAlgoValue;
            new_Xmin_Algo = str2num(src.String); %#ok<ST2NM>
            Xmax_Algo = this.xMaxAlgoValue;
            
            if new_Xmin_Algo < Xmax_Algo
                Xmin_Algo = new_Xmin_Algo;
                this.xMinAlgoValue = new_Xmin_Algo;
            else
                Xmin_Algo = old_Xmin_Algo;
                my_warndlg(['OptimDialog.algoXMinCallback : value greater than or equal ', num2str(Xmax_Algo)], 0);
            end
            
            if Xmin_Algo ~= old_Xmin_Algo
                % Modification du XDataCompute
                tmp_diff_min = abs(this.optim.XData - Xmin_Algo);
                tmp_diff_max = abs(this.optim.XData - Xmax_Algo);
                tmp_XDataCompute_min = find(min(tmp_diff_min) == tmp_diff_min);
                tmp_XDataCompute_max = find(min(tmp_diff_max) == tmp_diff_max);
                tmp_XDataCompute_min = tmp_XDataCompute_min(length(tmp_XDataCompute_min)); % cas o� plusieurs valeurs : on prend la max
                tmp_XDataCompute_max = tmp_XDataCompute_max(length(tmp_XDataCompute_max)); % cas o� plusieurs valeurs : on prend la max
                tmp_XDataCompute(1:tmp_XDataCompute_min-1)                        = 0; % zeros(1,tmp_XDataCompute_min-1);
                tmp_XDataCompute(tmp_XDataCompute_min:tmp_XDataCompute_max)       = 1; % ones(1,tmp_XDataCompute_max-tmp_XDataCompute_min+1);
                tmp_XDataCompute(tmp_XDataCompute_max+1:length(this.optim.XData)) = 0; % zeros(1,length(this.optim.XData)-tmp_XDataCompute_max);
                this.selectedModel.set('XDataFlags', tmp_XDataCompute);
                
                % update patch
                this.minModelPatch.set(  'XData', [this.xMinValue this.xMinValue Xmin_Algo Xmin_Algo]);
                this.minWeightsPatch.set('XData', [this.xMinValue this.xMinValue Xmin_Algo Xmin_Algo]);
            else
                % set old value to component
                src.String = Xmin_Algo;
            end
        end
        
        function algoXMaxCallback(this, src, ~)
            % Algo X Max Callback
            
            old_Xmax_Algo = this.xMaxAlgoValue;
            new_Xmax_Algo = str2num(src.String); %#ok<ST2NM>
            Xmin_Algo     = this.xMinAlgoValue;
            
            if new_Xmax_Algo > Xmin_Algo
                Xmax_Algo = new_Xmax_Algo;
                this.xMaxAlgoValue = new_Xmax_Algo;
            else
                Xmax_Algo = old_Xmax_Algo;
                my_warndlg(['OptimDialog.algoXMaxCallback : value less than or equal to ', num2str(Xmin_Algo)], 0);
            end
            
            if Xmax_Algo ~= old_Xmax_Algo
                % Modification du XDataCompute
                tmp_diff_min = abs(this.optim.XData - Xmin_Algo);
                tmp_diff_max = abs(this.optim.XData - Xmax_Algo);
                tmp_XDataCompute_min = find(min(tmp_diff_min) == tmp_diff_min);
                tmp_XDataCompute_max = find(min(tmp_diff_max) == tmp_diff_max);
                tmp_XDataCompute_min = tmp_XDataCompute_min(1); % cas o� plusieurs valeurs : on prend la min
                tmp_XDataCompute_max = tmp_XDataCompute_max(1); % cas o� plusieurs valeurs : on prend la min
                tmp_XDataCompute(1:tmp_XDataCompute_min-1)                        = 0; % zeros(1, tmp_XDataCompute_min - 1);
                tmp_XDataCompute(tmp_XDataCompute_min:tmp_XDataCompute_max)       = 1; % ones( 1, tmp_XDataCompute_max   - tmp_XDataCompute_min + 1);
                tmp_XDataCompute(tmp_XDataCompute_max+1:length(this.optim.XData)) = 0; % zeros(1, length(ClOptim_XData) - tmp_XDataCompute_max);
%                 this.optim = set(this.optim, 'XDataFlags', tmp_XDataCompute);
                this.selectedModel.set('XDataFlags', tmp_XDataCompute);

                % update patch
                this.maxModelPatch.set(  'XData', [this.xMaxValue this.xMaxValue Xmax_Algo Xmax_Algo]);
                this.maxWeightsPatch.set('XData', [this.xMaxValue this.xMaxValue Xmax_Algo Xmax_Algo]);
            else
                % set old value to component
                src.String = Xmax_Algo;
            end
        end
        
        function useVisuResultsCallback(this, src, ~)
            % Visualize intermediate results Callback
            this.optim.UseVisu = src.Value;
        end
        
        function launchAlgoCallback(this, src, ~)
            % Launch algo callback
            
            % Wait cursor
            this.parentFigure.set('Pointer','watch');
            % Disable button
            src.set('String', 'Computing...', 'Enable', 'off');
            drawnow;
            %launch algo
            this.optim.optimize();
            
            % Update ui parameters
            this.updateModelParametreComponents();
            % Update model axes
            this.updateModelAxes();
            
            % Enable button
            src.set('String', 'Launch', 'Enable', 'on');
            
            % Normal cursor
            this.parentFigure.set('Pointer', 'arrow');
        end
        
        
        function selectModelCallback(this, src, ~)
            % Select Model Callback
            
            this.selectModelInOptim(src.Value);
        end
        
        function randomModelCallback(this, ~, ~)
            % Randomize model Callback
            
            % Randomize model
            this.selectedModel.random();
            % Update ui parameters
            this.updateModelParametreComponents();
            % Update model axes
            this.updateModelAxes();
        end
        
        function saveModelCallback(this, ~, ~)
            this.savedModelList(end+1) = this.selectedModel.clone;
        end
        
        function loadModelCallback(this, ~, ~)
            % Load model callback
            if isempty(this.savedModelList)
                my_warndlg('No model to load.', 1);
                return
            else
                switch this.loadModelContextMenuItemValue
                    case 1 % load one of all models
                        for  k=length(this.savedModelList):-1:1
                            savedModelListStr{k} = [this.savedModelList(k).Name ' rms = ' num2str(this.savedModelList(k).Err)];
                        end
                        [modelNumber, validation] = my_listdlg('Please select one of all models to load', ...
                            savedModelListStr, 'SelectionMode', 'Single');
                        if ~validation
                            return
                        end
                        modelToLoad = this.savedModelList(modelNumber);
                        
                    case 2 % load one of best models
                        % Find the best models of each type
                        bestModelList = this.findBestSavedModels();
                        bestModelListStr = [];
                        for k=1:length(bestModelList)
                            bestModelListStr{end+1} = [bestModelList(k).Name ' rms = ' num2str(bestModelList(k).Err)]; %#ok<AGROW>
                        end
                        
                        [modelNumber, validation] = my_listdlg('Please select one of best models to load', ...
                            bestModelListStr, 'SelectionMode', 'Single');
                        if ~validation
                            return
                        end
                        modelToLoad = bestModelList(modelNumber);
                        
                    case 3 % load the best
                        % Find the best model
                        bestModel = this.findTheBestSavedModel();
                        bestModelStr = [bestModel.Name ' rms = ' num2str(bestModel.Err)];
%                         str1 = 'S�lectionnez le "meilleur" model (pas clair comme formulation)';
                        str2 = 'Please select the best model to load';
                        [modelNumber, validation] = my_listdlg(str2, bestModelStr, 'SelectionMode', 'Single'); %#ok<ASGLU>
                        if ~validation
                            return
                        end
                        modelToLoad = bestModel;
                end
                
                if ~isempty(modelToLoad)
                    for k=1:length(this.optim.models)
                        if strcmp(this.optim.models(k).Name, modelToLoad.Name)
                            % updatem odel in optim
                            this.optim.models(k) = modelToLoad;
                            % update selectModelPopupMenu
                            this.selectModelPopupMenu.Value = k;
                            % update ihm
                            this.selectModelInOptim(k);
                            break;
                        end
                    end
                end
            end
        end
        
        function deleteModelCallback(this, ~, ~)
            % Delete model callback
            if isempty(this.savedModelList)
                my_warndlg('No model to delete.', 1);
                return
            else
                switch this.deleteModelContextMenuItemValue
                    case 1 % delete one of all models
                        for  k=length(this.savedModelList):-1:1
                            savedModelListStr{k} = [this.savedModelList(k).Name ' rms = ' num2str(this.savedModelList(k).Err)];
                        end
                        [modelNumber, validation] = my_listdlg('Please select models to delete', ...
                            savedModelListStr, 'SelectionMode', 'Multiple');
                        if ~validation
                            return
                        end
                        modelsToDelete = this.savedModelList(modelNumber);
                        % Delete models
                        if ~isempty(modelsToDelete)
                            % Create the new saved model List
                            newSavedModelList = ClModel.empty;
                            
                            for  k=1:length(this.savedModelList)
                                keepThisModel = true;
                                for l =1:length(modelsToDelete)
                                    if (this.savedModelList(k) == modelsToDelete(l))
                                        keepThisModel = false;
                                    end
                                end
                                if keepThisModel
                                    newSavedModelList(end+1) = this.savedModelList(k); %#ok<AGROW>
                                end
                            end
                            % set the new saved Model List
                            this.savedModelList = newSavedModelList;
                        end
                        
                    case 2 % delete all models except the best models of each type
                        % Find the best models of each type
                        bestModelList = this.findBestSavedModels();
                        this.savedModelList = bestModelList;
                    case 3 % delete all models except the best model
                        % Find the best model
                        bestModel = this.findTheBestSavedModel();
                        this.savedModelList = bestModel;
                    case 4 % delete all models
                        this.savedModelList = ClModel.empty;
                end
            end
        end
        
        function bestModelList = findBestSavedModels(this)
            % Find the best models of each type
            bestModelList = ClModel.empty;
            for  k=1:length(this.savedModelList)
                
                % if best model list empty, add the model
                if isempty(bestModelList)
                    bestModelList(end+1) = this.savedModelList(k); %#ok<AGROW>
                else
                    % if same name and rms <, add model
                    isBestModelListContainsThisType = false;
                    
                    for l=1:length(bestModelList)
                        if strcmp(bestModelList(l).Name, this.savedModelList(k).Name)
                            isBestModelListContainsThisType = true;
                            
                            if this.savedModelList(k).Err <bestModelList(l).Err
                                bestModelList(l) = this.savedModelList(k);
                            end
                        end
                    end
                    
                    % if best model list dont contains this type, add model
                    if ~isBestModelListContainsThisType
                        bestModelList(end+1) = this.savedModelList(k); %#ok<AGROW>
                    end
                end
            end
        end
        
        function bestModel =  findTheBestSavedModel(this)
            % Find the best model
            bestModel = [];
            for  k=1:length(this.savedModelList)
                
                % if best model  empty, add the model
                if isempty(bestModel)
                    bestModel = this.savedModelList(k);
                else
                    if this.savedModelList(k).Err <bestModel.Err
                        bestModel = this.savedModelList(k);
                    end
                end
            end
        end
        
        function loadModelContextMenuItemCallback(this, src, ~)
            % loadModelContextMenuItem Callback
            this.loadModelContextMenuItemList(1).set('Checked', 'off');
            this.loadModelContextMenuItemList(2).set('Checked', 'off');
            this.loadModelContextMenuItemList(3).set('Checked', 'off');
            
            [~,index] = ismember(src, this.loadModelContextMenuItemList);
            this.loadModelContextMenuItemValue = index;
            this.loadModelContextMenuItemList(index).set('Checked', 'on');
            
            this.loadModelCallback();
        end
        
        function deleteModelContextMenuItemCallback(this, src, ~)
            % deleteModelContextMenuItem Callback
            this.deleteModelContextMenuItemList(1).set('Checked', 'off');
            this.deleteModelContextMenuItemList(2).set('Checked', 'off');
            this.deleteModelContextMenuItemList(3).set('Checked', 'off');
            this.deleteModelContextMenuItemList(4).set('Checked', 'off');
            
            [~,index] = ismember(src, this.deleteModelContextMenuItemList);
            this.deleteModelContextMenuItemValue = index;
            this.deleteModelContextMenuItemList(index).set('Checked', 'on');
            
            this.deleteModelCallback();
        end
        
        function showModelComponentsCallback(this, src, ~)
            % Show Hide components callback
            this.showModelComponentsValue = src.Value;
            
            this.showModelComponents();
        end
        
        function showModelLegendCallback(this, src, ~)
            % Show Hide model legend callback
            
            this.showModelLegendValue = src.Value;
            if src.Value
                legend(this.modelAxes, 'show');
            else
                legend(this.modelAxes, 'hide');
            end
        end
        
        function cleanDataCallback(this, ~, ~)
            % create ClSignal
            xSample = YSample('data', this.optim.XData);
            ySample1 = YSample('name', 'YData', 'data', this.optim.YData, 'marker', '.');
            signal(1)  = ClSignal('name', this.selectedModel.Name, 'xSample', xSample, 'ySample', ySample1);
            ySample2 = YSample('name', 'WeightsData', 'data', this.selectedModel.WeightsData, 'marker', '.');
            signal(2)  = ClSignal('name', this.selectedModel.Name, 'xSample', xSample, 'ySample', ySample2);
            % crate signail dialog
            signalDialog = SignalDialog(signal, 'Title', this.selectedModel.Name, 'CleanInterpolation', false);
            signalDialog.openDialog();
            
            % wait Ok button
            if signalDialog.okPressedOut == 1
                
                noData = isinf(signal(1).ySample.data);
                
                signal(1).ySample.data(noData)= NaN;
                signal(2).ySample.data(noData)= NaN;
                this.selectedModel.WeightsData	= signal(2).ySample.data;
                this.optim.YData = signal(1).ySample.data;
                
                % Modification des XData et YData de tous les modeles
                for k=1:length(this.optim.models)
                    model = this.optim.models(k);
                    
                    model.YData = this.optim.YData;
                    model.WeightsData(noData)	= NaN;
                end
                
                this.createModelAxes();
                this.createWeightsAxes();
            end
        end
        
        function handleParametreValueChange(this, ~, ~)
            % update rms Text
            this.rmsEdit.set('String', num2str(this.selectedModel.rms(this.optim.UseWeights)));
            % Update the model axe
            this.updateModelAxes();
        end
    end
end
