classdef ParametrePlotDialog < handle & AbstractDisplay
    % Desciption
    %   GUI that displays the parameters of a model and display the model
    %
    % Syntax
    %   a = ParametrePlotDialog(...)
    %
    % ParametrePlotDialog properties :
    %   Title       - Title of paramDialog
    %   model       - ClModel
    %   paramDialog - parameters dialog
    %
    % Examples
    %   p(1) = ClParametre('Name', 'Height of the sonar over seabed', 'Unit', 'm',   'MinValue', 1, 'MaxValue', 50, 'Value', 20);
    %   p(2) = ClParametre('Name', 'Height of the sand ripples',      'Unit', 'm',   'MinValue', 0, 'MaxValue', 1,  'Value', 0.04);
    %   p(3) = ClParametre('Name', 'Period of the sand ripples',      'Unit', 'm',   'MinValue', 0.01, 'MaxValue', 10, 'Value', 0.15);
    %   p(4) = ClParametre('Name', 'Asymetry', 'MinValue', 0, 'MaxValue', 1, 'Value', 0.5);
    %   p(5) = ClParametre('Name', 'Noise',    'MinValue', 0, 'MaxValue', 1, 'Value', 0.5);
    %   p(6) = ClParametre('Name', 'Max angles',                      'Unit', 'deg', 'MinValue', 0, 'MaxValue', 80, 'Value', 65);
    %   a = ParametrePlotDialog('Dimitrios quest for BS azimuth', p, @plotSandRipples);
    %   a.openDialog;
    %
    % Authors : MHO
    % See also ClModel ParametreDialog ParametreComponent Authors
    % ----------------------------------------------------------------------------
    
    properties
        %         Title
        params
        paramDialog
        hPlot
        plotFigure
        hPlotFunc
    end
    
    methods
        function this = ParametrePlotDialog(Title, params, hPlotFunc)
            this.params    = params;
            this.hPlotFunc = hPlotFunc;
            
            % Create the instance of ParametreDialog
            this.paramDialog = ParametreDialog('params', this.params, 'Title', Title);
            this.paramDialog.sizes = [0 -4 -1 -3 -1 -1 -1 0];
            
            % Attach a listener to the property 'OneParamValueChange' of paramDialog
            addlistener(this.paramDialog, 'OneParamValueChange', @this.handleValueChange);
            % Attach a listener to the property 'OkPressedEvent' of paramDialog, triggers 'this.closePlotCallback'
            addlistener(this.paramDialog, 'OkPressedEvent',      @this.closePlotCallback);
            % Attach a listener to the property 'CancelPressedEvent' of paramDialog, triggers 'this.closePlotCallback'
            addlistener(this.paramDialog, 'CancelPressedEvent',  @this.closePlotCallback);
        end
        
        function openDialog(this)
            % Plot the model
            this.plot;
            
            % Open the parametre dialog
            this.paramDialog.openDialog;
        end
        
        function this = plot(this)
            [this.plotFigure, this.hPlot] = this.hPlotFunc(this.plotFigure, this.hPlot, [this.paramDialog.params(:).Value]);
        end
        
        function handleValueChange(this, src, eventData) %#ok<INUSD>
            [this.plotFigure, this.hPlot] = this.hPlotFunc(this.plotFigure, this.hPlot, [this.paramDialog.params(:).Value]);
        end
        
        function closePlotCallback(this, src, eventData) %#ok<INUSD>
            if ishandle(this.plotFigure)
%                 delete(this.plotFigure); % Comment� car je veux garder les fen�tres
                this.plotFigure = []; % Ajout JMA pour que la prochaine fois les fen�tre soient recr��es
            end
        end
    end
    
    methods (Access = protected)
        function str = displayScalarObjectCustom(this)
            % Summary of one ParametrePlotDialog instance in a character array
            % Abstract methods from AbstractDisplay
            
            str = {};
            str{end+1} = sprintf('Title     <-> %s', this.paramDialog.Title);
            
            str = cell2str(str);
        end
    end
end
