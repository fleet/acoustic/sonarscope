classdef ParametreUiDialog < handle & SimpleTitleUiDialog
    % Description
    %   Class ParametreDialog wich is used to display a set of
    %   ClParametre instances in a figure and waits for Ok or Cancel
    %   buttons
    %
    % Syntax
    %   a = ParametreDialog(...)
    %
    % ParametreDialog properties :
    %   params             - ClParametre array
    %   sizes              - array of 8 size subComponents
    %   paramComponentList - ParametreComponent array
    %
    % Output Arguments
    %   s : One ParametreDialog instance
    %
    % Examples
    %   p    = ClParametre('Name', 'Temperature', 'Unit', 'deg',  'MinValue', -10, 'MaxValue', 50, 'Value', 20);
    %   p(2) = ClParametre('Name', 'Salinity',    'Unit', '0/00', 'MinValue', 25,  'MaxValue', 35, 'Value', 30);
    %   a = ParametreUiDialog('params', p, 'Title', 'Demo of ParametreDialog');
    %   a.openDialog;
    %   % output :
    %     a.okPressedOut %(0 if not Ok pressed, 1 if Ok pressed)
    %     paramsValue = a.getParamsValue()
    %
    %   style1 = ColorAndFontStyle('BackgroundColor', [0.7 0.7 1], 'ForegroundColor', 'k', 'FontWeight', 'bold', 'FontSize', 13, 'FontAngle', 'italic');
    %   style2 = ColorAndFontStyle('BackgroundColor', [1 0.8 0.6], 'ForegroundColor', 'k', 'FontWeight', 'bold');
    %   a.titleStyle = style1;
    %   a.paramStyle = style2;
    %   a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
    %     %   a.sizes = [0 -2 -1 -2 -1 -1 -1 0];
    %   a.sizes = {0, '2x', '1x', '2x', '1x', '1x', '1x', 0};
    %   a.openDialog;
    %   flag = a.okPressedOut;
    %   if ~flag
    %       return
    %   end
    %   paramsValue = a.getParamsValue;
    %
    %     %   a.sizes = [0 -2 0 0 0 -1 -1 0];
    %   a.sizes = {0, '2x', 0, 0, 0, '1x', '1x', 0};
    %   a.openDialog;
    %
    % Authors : MHO
    % See also StyledParametreDialog ClParametre ParametreComponent Authors
    % ----------------------------------------------------------------------------
    
    properties
        params             = ClParametre.empty         % - ClParametre array
        sizes = {25, '2x', '1x', '2x', '1x', '1x', '1x', 25}; % - Array of 8 subComponents widths
        paramStyle         = ColorAndFontStyle;        % - Style of uicontrol text of Parametre Components
        paramComponentList = ParametreUiComponent.empty; % - ParametreComponent array
    end
    
    properties (Access = private)
        %         paramComponentList = ParametreComponent.empty; % - ParametreComponent array
    end
    
    events
        OneParamValueChange % Event if one param value change
    end
    
    methods
        function this = ParametreUiDialog(varargin)
            % Superclass Constructor
            this = this@SimpleTitleUiDialog(varargin{:});
            % Read input args
            [varargin, this.params]     = getPropertyValue(varargin, 'params',     []);
            [varargin, this.sizes]      = getPropertyValue(varargin, 'sizes',      this.sizes);
            [varargin, this.paramStyle] = getPropertyValue(varargin, 'paramStyle', this.paramStyle); %#ok<ASGLU>
        end
        
        function handleValueChange(this, ~, ~)
            % Send one value change event
            this.notify('OneParamValueChange');
        end
        
        function paramsValue = getParamsValue(this)
            % Get the "Value" properties of the parametres in an array
            paramsValue = [];
            for k=length(this.params):-1:1
                paramsValue(k) = this.params(k).Value;
            end
        end
        
        function this = set.sizes(this, sz) %#ok<MCHV2>
            % Setter of "sizes" property
            
            if ~isempty(sz)
                if length(sz) == 8
                    if sz{6} == 0
                        %                         str1 = 'Size(6) ne peut pas �tre �gal � 0 sinon, vous ne verrez pas la valeur du param�tre.';
                        str2 = 'Size(6) cannot be equal to 0, this would say that you do not want to see the value of the parametre.';
                        my_warndlg(str2, 1);
                    else
                        this.sizes = sz;
                        if ~isempty(this.paramComponentList) %#ok<MCSUP>
                            
                            
                            % TODO : Comment� par JMA le 19/11/2019
                            
                            
                            %                             for paramIdx=1:length(this.paramComponentList) %#ok<MCSUP>
                            %                                 this.paramComponentList(paramIdx).sizes = this.sizes; %#ok<MCSUP>
                            %                             end
                        end
                    end
                else
                    %                     str1 = 'Le param�tre "sizes" transmis � "ParametreComponent" doit �tre de taille 8.';
                    str2 = 'The parametre "sizes" sent to "ParametreComponent" must be an array of size 8.';
                    my_warndlg(str2, 1);
                end
            end
        end
    end
    
    methods (Access = protected)
        function [widthBody ,heightBody] = createBody(this, parent)
            % Create ParametreComponent List
            
            %             verticalBox = uiextras.VBox('Parent', parent);
            gridlayout = uigridlayout(parent, 'ColumnSpacing', 1, 'RowSpacing', 1, 'Padding', 1);
            gridlayout.ColumnWidth = {'1x'};
            
            lengthComponents = length(this.params);
            for paramIdx=1:lengthComponents
                % Create ParametreComponent component
                this.paramComponentList(paramIdx) = ParametreUiComponent(gridlayout, 'param', this.params(paramIdx), 'sizes', this.sizes, 'style', this.paramStyle);
                % Add a listener for Value change event
                addlistener(this.paramComponentList(paramIdx), 'ValueChange', @this.handleValueChange);
                gridlayout.RowHeight{paramIdx} = 25; % Height of one row
            end
            
            widthBody  = 600;
            heightBody = 50 * lengthComponents;
        end
    end
end
