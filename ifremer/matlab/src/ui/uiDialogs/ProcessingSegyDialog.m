classdef ProcessingSegyDialog < handle & SimpleTitleDialog
    % Description
    %   Class ProcessingSegyDialog
    %
    % Syntax
    %   a = ProcessingSegyDialog(...)
    %
    % Output Arguments
    %   s : One ProcessingSegyDialog instance
    %
    % Examples
    % % New object language call :
    %   a = ProcessingSegyDialog.getInstance(); % get the singleton instance
    %   a.openDialog();
    %   % Output :
    %     a.okPressedOut %(0 if not pressed, 1 if pressed)
    %     a.isFilterProcessing
    %     a.isCorrelationProcessing
    %     a.isSphericalDivergence
    %     a.isEnvelop
    %     a.isdB
    %     a.isHeaveCorrection
    %     a.isImmersionCorrection
    %     a.isSaveProcessingData
    %     a.selectedNavNbr
    %
    % % Old object language call :
    % [flag, outputStruct] = ProcessingSegyDialog.paramsProcessingSegy()
    %  % Output :
    %     flag
    %     outputStruct
    %
    % Authors : MHO
    % See also SimpleTitleDialog ClModel Authors
    % ----------------------------------------------------------------------------
    
    properties
        % Data
        isFilterProcessing      = 1;
        isCorrelationProcessing = 1;
        isSphericalDivergence   = 1;
        isEnvelop               = 1;
        isdB                    = 0;
        isHeaveCorrection       = 1;
        isImmersionCorrection   = 0;
        isSaveProcessingData    = 1;
        selectedNavNbr          = 1;
    end
    
    properties  (Access = private)
        % uicontrols
        filterProcessingCheckbox
        correlationProcessingCheckbox
        sphericalDivergenceCheckbox
        envelopCheckbox
        dBCheckbox
        heaveCorrectionCheckbox
        immersionCorrectionCheckbox
        saveCheckbox
        navList
    end
    
    methods (Static)
        function instance = getInstance()
            % Get the singleton instance of ProcessingSegyDialog
            persistent currentInstance;
            if isempty(currentInstance)
                currentInstance = ProcessingSegyDialog();
            end
            
            instance = currentInstance;
        end
        
        function [flag, outputStruct] = paramsProcessingSegy()
            % Old language call.
            
            instance = ProcessingSegyDialog.getInstance();
            instance.openDialog();
            
            flag = instance.okPressedOut;
            outputStruct.FilterProcessing      = instance.isFilterProcessing;
            outputStruct.CorrelationProcessing = instance.isCorrelationProcessing;
            outputStruct.SphericalDivergence   = instance.isSphericalDivergence;
            outputStruct.Envelop               = instance.isEnvelop;
            outputStruct.dB                    = instance.isdB;
            outputStruct.HeaveCorrection       = instance.isHeaveCorrection;
            outputStruct.ImmersionCorrection   = instance.isImmersionCorrection;
            outputStruct.SaveProcessingData    = instance.isSaveProcessingData;
            outputStruct.selectNav             = instance.selectedNavNbr;
        end
        
        
    end
    
    methods
        function this = ProcessingSegyDialog(varargin)
            % Superclass Constructor
            this = this@SimpleTitleDialog('resizable', 'on', varargin{:});
            if isempty(this.Title)
                this.Title = 'Processing Segy Dialog';
                
            end
        end
        
        function okPressed(this, ~, ~)
            % Save function : set the output then close dialog
            
            this.isFilterProcessing      = this.filterProcessingCheckbox.Value;
            this.isCorrelationProcessing = this.correlationProcessingCheckbox.Value;
            this.isSphericalDivergence   = this.sphericalDivergenceCheckbox.Value;
            this.isEnvelop               = this.envelopCheckbox.Value;
            this.isdB                    = this.dBCheckbox.Value;
            this.isHeaveCorrection       = this.heaveCorrectionCheckbox.Value;
            this.isImmersionCorrection   = this.immersionCorrectionCheckbox.Value;
            this.isSaveProcessingData    = this.saveCheckbox.Value;
            this.selectedNavNbr          = this.navList.Value;
            
            % Close the dialog
            okPressed@SimpleTitleDialog(this);
        end
    end
    
    methods (Access = protected)
        function [widthBody ,heightBody] = createBody(this, parent)
            % Create Body of ProcessingSegyDialog
            
            verticalBox = uiextras.VBox('Parent', parent, 'Spacing', 10, 'Padding', 10);
            
            % Filter processing checkbox
            strUS = '<html><FONT SIZE=4 FACE="Geneva"><b>Filter</b> processing</FONT><br><i><font color="b" style="font-weight:bold;">Data filtering in bandwith fmin-200 fmax+200 Hz. For sparker : filter 100-2000Hz</i></html>';
%             strFR = '<html><FONT SIZE=4 FACE="Geneva"><b>Filtrage</b> process</FONT><br><i><font color="b" style="font-weight:bold;">Filtrage des donn�es dans la bande fmin-200 fmax+200 Hz. Pour sparker : filtre 100-2000Hz</i></html>';
            this.filterProcessingCheckbox = uicontrol('parent', verticalBox, ...
                'Style',  'checkbox', ...
                'Value',  this.isFilterProcessing, ...
                'String', strUS);
            
            % Correlation Processing checkbox
            strUS = '<html><FONT SIZE=4 FACE="Geneva"><b>Correlation</b> processing</FONT><br><i><font color="b" style="font-weight:bold;">Correlation with the transmitted signal  - 0 for sparker</i></html>';
%             strFR = '<html><FONT SIZE=4 FACE="Geneva">Correlation<b> process</b>.</FONT></b><br><i><font color="b" style="font-weight:bold;">Correlation avec le signal �mis  - 0 pour sparker</i></html>';
            this.correlationProcessingCheckbox = uicontrol('parent', verticalBox, ...
                'Style',  'checkbox', ...
                'Value',  this.isCorrelationProcessing, ...
                'String', strUS);
            
            % Spherical Divergence checkbox
            strUS = '<html><FONT SIZE=4 FACE="Geneva"><b>Spherical Divergence</b></FONT><br><i><font color="b" style="font-weight:bold;">Spherical divergence correction </i></html>';
%             strFR = '<html><FONT SIZE=4 FACE="Geneva"><b>Divergence sph�rique</b></FONT></b><br><i><font color="b" style="font-weight:bold;">Correction de divergence sph�rique </i></html>';
            this.sphericalDivergenceCheckbox = uicontrol('parent', verticalBox, ...
                'Style',  'checkbox', ...
                'Value',  this.isSphericalDivergence, ...
                'String', strUS);
            
            % Envelop checkbox
            strUS = '<html><FONT SIZE=4 FACE="Geneva"><b>Envelop</b></FONT><br><i><font color="b" style="font-weight:bold;">Signal envelope - 0 for sparker</i></html>';
%             strFR = '<html><FONT SIZE=4 FACE="Geneva"><b>Enveloppe</b></FONT></b><br><i><font color="b" style="font-weight:bold;">Enveloppe du signal - 0 pour sparker</i></html>';
            this.envelopCheckbox = uicontrol('parent', verticalBox, ...
                'Style',  'checkbox', ...
                'Value',  this.isEnvelop, ...
                'String', strUS);
            
            % dB checkbox
            strUS = '<html><FONT SIZE=4 FACE="Geneva"><b>dB</b></FONT><br><i><font color="b" style="font-weight:bold;">dB conversion</i></html>';
%             strFR = '<html><FONT SIZE=4 FACE="Geneva"><b>dB</b></FONT></b><br><i><font color="b"style="font-weight:bold;">Conversion en dB </i></html>';
            this.dBCheckbox = uicontrol('parent', verticalBox, ...
                'Style',  'checkbox', ...
                'Value',  this.isdB, ...
                'String', strUS);
            
            % Heave Correction checkbox
            strUS = '<html><FONT SIZE=4 FACE="Geneva"><b>Heave correction</b></FONT><br><i><font color="b" style="font-weight:bold;">Heave correction (valeur : 0 ou 1)</i></html>';
%             strFR = '<html><FONT SIZE=4 FACE="Geneva"><b>Correction de pilonnement</b></FONT></b><br><i><font color="b" style="font-weight:bold;">Application des corrections de pilonnement (valeur : 0 ou 1)</i></html>';
            this.heaveCorrectionCheckbox = uicontrol('parent', verticalBox, ...
                'Style',  'checkbox', ...
                'Value',  this.isHeaveCorrection, ...
                'String', strUS);
            
            % Immersion Correction checkbox
            strUS = '<html><FONT SIZE=4 FACE="Geneva"><b>Immersion correction</b></FONT><br><i><font color="b" style="font-weight:bold;">Immersion correction for the AUV</i></html>';
%             strFR = '<html><FONT SIZE=4 FACE="Geneva"><b>Correction d''immersion</b></FONT></b><br><i><font color="b" style="font-weight:bold;">Correction de l''immersion pour l''AUV</i></html>';
            this.immersionCorrectionCheckbox = uicontrol('parent', verticalBox, ...
                'Style',  'checkbox', ...
                'Value',  this.isImmersionCorrection, ...
                'String', strUS);
            
            % Save checkbox
            strUS = '<html><FONT SIZE=4 FACE="Geneva"><b>Save processing Data</b></FONT><br><i><font color="b" style="font-weight:bold;">Do you want to save processed data ?</i></html>';
%             strFR = '<html><FONT SIZE=4 FACE="Geneva"><b>Sauver les filtrages</b></FONT></b><br><i><font color="b" style="font-weight:bold;">Voulez-vous sauver les donn�es filtr�es ?</i></html>';
            this.saveCheckbox =  uicontrol('parent', verticalBox, ...
                'Style',  'checkbox', ...
                'Value',  this.isSaveProcessingData, ...
                'String', strUS);
            
            navListPanel = uipanel('Parent', verticalBox,  'Title', 'Choice the vessel of survey');
            navListBox = uiextras.VBox('Parent', navListPanel, 'Padding',5);
            this.navList = uicontrol('parent', navListBox,...
                'Style',  'listbox',...
                'string', {'Pourquoi-Pas?', 'Atalante', 'Suroit', 'Haliotis', 'AUV', 'Sparker', 'Other',},...
                'Value',  this.selectedNavNbr);
            
            heightLine = 32;
            verticalBox.set('Sizes', [heightLine heightLine heightLine heightLine heightLine heightLine heightLine heightLine -1]);
            
            widthBody  = 500;
            heightBody = 550;
        end  
    end
end
