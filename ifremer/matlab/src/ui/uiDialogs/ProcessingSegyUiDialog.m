classdef ProcessingSegyUiDialog < handle & SimpleTitleUiDialog
    % Description
    %   Class ProcessingSegyUiDialog
    %
    % Syntax
    %   a = ProcessingSegyUiDialog(...)
    %
    % Output Arguments
    %   s : One ProcessingSegyUiDialog instance
    %
    % Examples
    % % New object language call :
    %   a = ProcessingSegyUiDialog.getInstance(); % get the singleton instance
    %   a.openDialog();
    %   % Output :
    %     a.okPressedOut %(0 if not pressed, 1 if pressed)
    %     a.isFilterProcessing
    %     a.isCorrelationProcessing
    %     a.isSphericalDivergence
    %     a.isEnvelop
    %     a.isdB
    %     a.isHeaveCorrection
    %     a.isImmersionCorrection
    %     a.isSaveProcessingData
    %     a.selectedNavNbr
    %
    % % Old object language call :
    % [flag, outputStruct] = ProcessingSegyUiDialog.paramsProcessingSegy()
    %  % Output :
    %     flag
    %     outputStruct
    %
    % Authors : MHO
    % See also SimpleTitleDialog ClModel Authors
    % ----------------------------------------------------------------------------
    
    properties
        % Data
        isFilterProcessing      = 1;
        isCorrelationProcessing = 1;
        isSphericalDivergence   = 1;
        isEnvelop               = 1;
        isdB                    = 0;
        isHeaveCorrection       = 1;
        isImmersionCorrection   = 0;
        isSaveProcessingData    = 1;
        selectedNavNbr          = 1;
    end
    
    properties  (Access = private)
        % uicontrols
        filterProcessingCheckbox
        correlationProcessingCheckbox
        sphericalDivergenceCheckbox
        envelopCheckbox
        dBCheckbox
        heaveCorrectionCheckbox
        immersionCorrectionCheckbox
        saveCheckbox
        navList
    end
    
    methods (Static)
        function instance = getInstance()
            % Get the singleton instance of ProcessingSegyDialog
            persistent currentInstance;
            if isempty(currentInstance)
                currentInstance = ProcessingSegyUiDialog();
            end
            
            instance = currentInstance;
        end
        
        function [flag, outputStruct] = paramsProcessingSegy()
            % Old language call.
            
            instance = ProcessingSegyUiDialog.getInstance();
            instance.openDialog();
            
            flag = instance.okPressedOut;
            outputStruct.FilterProcessing      = instance.isFilterProcessing;
            outputStruct.CorrelationProcessing = instance.isCorrelationProcessing;
            outputStruct.SphericalDivergence   = instance.isSphericalDivergence;
            outputStruct.Envelop               = instance.isEnvelop;
            outputStruct.dB                    = instance.isdB;
            outputStruct.HeaveCorrection       = instance.isHeaveCorrection;
            outputStruct.ImmersionCorrection   = instance.isImmersionCorrection;
            outputStruct.SaveProcessingData    = instance.isSaveProcessingData;
            outputStruct.selectNav             = instance.selectedNavNbr;
        end
        
        
    end
    
    methods
        function this = ProcessingSegyUiDialog(varargin)
            % Superclass Constructor
            this = this@SimpleTitleUiDialog('resizable', 'on', varargin{:});
            if isempty(this.Title)
                this.Title = 'Processing Segy Dialog';
                
            end
        end
        
        function okPressed(this, ~, ~)
            % Save function : set the output then close dialog
            
            this.isFilterProcessing      = this.filterProcessingCheckbox.Value;
            this.isCorrelationProcessing = this.correlationProcessingCheckbox.Value;
            this.isSphericalDivergence   = this.sphericalDivergenceCheckbox.Value;
            this.isEnvelop               = this.envelopCheckbox.Value;
            this.isdB                    = this.dBCheckbox.Value;
            this.isHeaveCorrection       = this.heaveCorrectionCheckbox.Value;
            this.isImmersionCorrection   = this.immersionCorrectionCheckbox.Value;
            this.isSaveProcessingData    = this.saveCheckbox.Value;
            [~, index] = ismember(this.navList.Value, this.navList.Items);
            this.selectedNavNbr          = index;
            
            % Close the dialog
            okPressed@SimpleTitleUiDialog(this);
        end
    end
    
    methods (Access = protected)
        function [widthBody ,heightBody] = createBody(this, parent)
            % Create Body of ProcessingSegyDialog
            
            %             verticalBox = uiextras.VBox('Parent', parent, 'Spacing', 10, 'Padding', 10);
            
            verticalGridlayout = uigridlayout(parent, 'ColumnSpacing', 10, 'RowSpacing', 10, 'Padding', 10);
            verticalGridlayout.ColumnWidth = {'1x'};
            
            % Filter processing checkbox
            strUS = 'Filter processing (Data filtering in bandwith fmin-200 fmax+200 Hz. For sparker : filter 100-2000Hz)';
            this.filterProcessingCheckbox = uicheckbox(verticalGridlayout, ...
                'Value',  this.isFilterProcessing, ...
                'Text', strUS);
            
            % Correlation Processing checkbox
            strUS = 'Correlation processing (Correlation with the transmitted signal - 0 for sparker)';
            this.correlationProcessingCheckbox = uicheckbox(verticalGridlayout, ...
                'Value',  this.isCorrelationProcessing, ...
                'Text', strUS);
            
            % Spherical Divergence checkbox
            strUS = 'Spherical Divergence (Spherical divergence correction)';
            this.sphericalDivergenceCheckbox = uicheckbox(verticalGridlayout, ...
                'Value',  this.isSphericalDivergence, ...
                'Text', strUS);
            
            % Envelop checkbox
            strUS = 'Envelop (Signal envelope - 0 for sparker)';
            this.envelopCheckbox = uicheckbox(verticalGridlayout, ...
                'Value',  this.isEnvelop, ...
                'Text', strUS);
            
            % dB checkbox
            strUS = 'dB (dB conversion)';
            this.dBCheckbox = uicheckbox(verticalGridlayout, ...
                'Value',  this.isdB, ...
                'Text', strUS);
            
            % Heave Correction checkbox
            strUS = 'Heave correction (Heave correction (valeur : 0 ou 1))';
            this.heaveCorrectionCheckbox = uicheckbox(verticalGridlayout, ...
                'Value',  this.isHeaveCorrection, ...
                'Text', strUS);
            
            % Immersion Correction checkbox
            strUS = 'Immersion correction (Immersion correction for the AUV)';
            this.immersionCorrectionCheckbox = uicheckbox(verticalGridlayout, ...
                'Value',  this.isImmersionCorrection, ...
                'Text', strUS);
            
            % Save checkbox
            strUS = 'Save processing Data (Do you want to save processed data ?)';
            this.saveCheckbox = uicheckbox(verticalGridlayout, ...
                'Value',  this.isSaveProcessingData, ...
                'Text', strUS);
            
            navListPanel = uipanel(verticalGridlayout,  'Title', 'Choice the vessel of survey');
            navListGridlayout = uigridlayout(navListPanel, 'ColumnSpacing', 0, 'RowSpacing', 0, 'Padding', 5);
            navListGridlayout.ColumnWidth = {'1x'};
            navListGridlayout.RowHeight = {'1x'};
            %

            itemList = {'Pourquoi-Pas?', 'Atalante', 'Suroit', 'Haliotis', 'AUV', 'Sparker', 'Other'};
            this.navList = uilistbox(navListGridlayout,...
                'Items', itemList,...
                'Value',  itemList{this.selectedNavNbr});
            
            
            
            heightLine = 32;
            
            verticalGridlayout.RowHeight ={heightLine, heightLine, heightLine, heightLine, heightLine, heightLine, heightLine, heightLine, '1x'};
            
            widthBody  = 500;
            heightBody = 550;
        end
    end
end
