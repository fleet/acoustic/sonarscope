classdef SelectDatagramUiDialog < handle & SimpleTitleUiDialog
    % Description
    %   Class SelectDatagramUiDialog
    %
    % Syntax
    %   a = SelectDatagramUiDialog(...)
    %
    % Output Arguments
    %   selectedDatagramList : Datagrams sélectionnés.
    %   selectIndex  : Layer indiqués comme Index.
    %
    % Examples
    % clear mandatoryDatagramList
    % mandatoryDatagramList.code = [];
    % mandatoryDatagramList.name = {};
    % mandatoryDatagramList.nb   = [];
    % for(k=1:5)
    % mandatoryDatagramList.code(k) = randi([0 100]);
    % mandatoryDatagramList.name{k} = ['testMandatory' num2str(k)];
    % mandatoryDatagramList.nb(k)   = randi([0 100]);
    % end
    %
    %
    % clear optDatagramList
    % optDatagramList.code = [];
    % optDatagramList.name = {};
    % optDatagramList.nb   = [];
    %
    % for(k=1:3)
    % optDatagramList.code(k) = randi([0 100]);
    % optDatagramList.name{k} = ['testOpt' num2str(k)];
    % optDatagramList.nb(k)   = randi([0 100]);
    % end
    %   a = SelectDatagramUiDialog(mandatoryDatagramList, optDatagramList);
    %   a.openDialog();
    %   % Output :
    %   a.selectedDatagramList
    %
    % Authors : MHO
    % See also SimpleTitleDialog ClModel Authors
    % ----------------------------------------------------------------------------
    
    properties
        selectedDatagramList = [];
    end
    
    properties  (Access = private)
        mandatoryUitableData
        optUitableData
        %         listLayer
        
        % uicontrols
        mandatoryUitableComponent
        optUitableComponent
        selectAllButton
    end
    
    
    methods
        function this = SelectDatagramUiDialog(mandatoryDatagramList, optDatagramList, varargin)
            % Superclass Constructor
            this = this@SimpleTitleUiDialog('resizable', 'off', varargin{:});
            if isempty(this.Title)
                this.Title = 'Select datagrams you want to save';
            end
            
            
            % Create uitable data
            this.mandatoryUitableData = [];
            for k=1:length(mandatoryDatagramList.name)
                this.mandatoryUitableData = [ this.mandatoryUitableData; {mandatoryDatagramList.nb(k), mandatoryDatagramList.code(k),mandatoryDatagramList.name{k}, true}];
            end
            
            
            this.optUitableData = [];
            for k=1:length(optDatagramList.name)
                this.optUitableData = [ this.optUitableData; {optDatagramList.nb(k), optDatagramList.code(k),optDatagramList.name{k}, true}];
            end
            
            
        end
        
        function okPressed(this, ~, ~)
            % Save function : set the output then close dialog
            
            this.mandatoryUitableData = this.mandatoryUitableComponent.Data;
            this.optUitableData = this.optUitableComponent.Data;
            
            this.selectedDatagramList   = [this.mandatoryUitableComponent.Data ; this.optUitableComponent.Data];
            
            
            % Close the dialog
            okPressed@SimpleTitleUiDialog(this);
        end
        
        function cancel(this, ~, ~)
            % Close the dialog
            
            this.mandatoryUitableData = this.mandatoryUitableComponent.Data;
            this.optUitableData = this.optUitableComponent.Data;
            
            this.selectedDatagramList   = [];
            
            % Close the dialog
            cancel@SimpleTitleUiDialog(this);
        end
        
        
    end
    
    methods (Access = protected)
        function [widthBody ,heightBody] = createBody(this, parent)
            % Create Body of SelectLayerUiDialog
            verticalGridlayout = uigridlayout(parent, 'ColumnSpacing', 0, 'RowSpacing', 5, 'Padding', 1);
            verticalGridlayout.ColumnWidth = {'1x'};
            
            
            % Création du container de la liste de Layers.
            colName         = {'Numbers', 'Code', 'Name', 'Available'};
            colFormat       = {'numeric', 'numeric', 'char', 'logical'};
            colEditOpt      = [false false false true];
            colEditMand     = [false false false false];
            colSize   = {70, 70, 300, 100};
            
            
            mandatoryPanel = uipanel(verticalGridlayout,  'Title', 'Mandatory datagrams');
            mandatoryGridlayout = uigridlayout(mandatoryPanel, 'ColumnSpacing', 0, 'RowSpacing', 5, 'Padding', 1);
            mandatoryGridlayout.ColumnWidth = {'1x'};
            mandatoryGridlayout.RowHeight = {'1x'};
            this.mandatoryUitableComponent = uitable(mandatoryGridlayout, ...
                'Data',           this.mandatoryUitableData,...
                'ColumnName',     colName,...
                'ColumnFormat',   colFormat,...
                'ColumnEditable', colEditMand,...
                'RowName',        [], ...
                'ColumnWidth',    colSize);
            %             mandatoryPanel.Layout.Column = [1 2];
            
            
            
            optionalPanel = uipanel(verticalGridlayout,  'Title', 'Optionnal datagrams');
            optionalGridlayout = uigridlayout(optionalPanel, 'ColumnSpacing', 0, 'RowSpacing', 5, 'Padding', 1);
            optionalGridlayout.ColumnWidth = {'1x', 100};
            optionalGridlayout.RowHeight = {'1x', 30};
            this.optUitableComponent = uitable(optionalGridlayout, ...
                'Data',           this.optUitableData,...
                'ColumnName',     colName,...
                'ColumnFormat',   colFormat,...
                'ColumnEditable', colEditOpt,...
                'RowName',        [], ...
                'ColumnWidth',    colSize);
            this.optUitableComponent.Layout.Column = [1 2];
            
            
            % Select/ Unselect all
            this.selectAllButton = uibutton(optionalGridlayout, 'push', ...
                'Text',    'Unselect All', ...
                'ButtonPushedFcn', @this.selectallCallback);
            this.selectAllButton.Layout.Column = 2;
            
            verticalGridlayout.RowHeight ={'1x', '1x'};
            
            widthBody  = 560;
            heightBody = 800;
        end
        
        function selectallCallback(this, src, ~)
            
            %             this.mandatoryUitableData = this.mandatoryUitableComponent.Data;
            this.optUitableData = this.optUitableComponent.Data;
            
            if strcmp(src.Text, 'Select All')
                src.Text = 'Unselect All';
                
                %                 for k2=1:length(this.mandatoryUitableData)
                %                     this.mandatoryUitableData{k2,end} = true;
                %                 end
                for k2=1:size(this.optUitableData,1)
                    this.optUitableData{k2,end} = true;
                end
            else
                src.Text = 'Select All';
                %                 for k2=1:length(this.mandatoryUitableData)
                %                     this.mandatoryUitableData{k2,end} = false;
                %                 end
                for k2=1:size(this.optUitableData,1)
                    this.optUitableData{k2,end} = false;
                end
            end
            
            %             this.mandatoryUitableComponent.Data =  this.mandatoryUitableData;
            this.optUitableComponent.Data =  this.optUitableData;
            
        end
    end
end
