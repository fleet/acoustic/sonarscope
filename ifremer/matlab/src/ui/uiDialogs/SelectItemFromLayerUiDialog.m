classdef SelectItemFromLayerUiDialog < handle & SimpleTitleUiDialog
    % Description
    %   Class SelectItemFromLayerUiDialog
    %
    % Syntax
    %   a = SelectItemFromLayerUiDialog(...)
    %
    % Output Arguments
    %   selectedDatagramList : Datagrams sélectionnés.
    %   selectIndex  : Layer indiqués comme Index.
    %
    % Examples
    %  layerListOrig = Simrad_getDefaultLayersV2;
    %
    %     titleDataSet{1,1} = 'From Depth datagram - XYZ 88';
    %     titleDataSet{2,1} = 'From Raw Range Beam Angles datagram';
    %     titleDataSet{3,1} = 'From Quality Factor datagram'; % OK
    %     titleDataSet{3,2} = 'From Seabed Image datagram';
    %     titleDataSet{1,2} = 'Deduced from Depth datagram - XYZ 88';
    %     titleDataSet{2,2} = 'Deduced from Raw Range Beam Angles datagram';
    %
    %     layerList.Order = layerListOrig.Order;
    %     layerList.SelectionLayers      = layerListOrig.SelectionLayers(1:2,:);
    %     layerList.SelectionLayers(3,1) = layerListOrig.SelectionLayers(3,1);
    %     layerList.SelectionLayers(3,2) = layerListOrig.SelectionLayers(4,1);
    %
    %     layerList.nomLayers      = layerListOrig.nomLayers(1:2,:);
    %     layerList.nomLayers(3,1) = layerListOrig.nomLayers(3,1);
    %     layerList.nomLayers(3,2) = layerListOrig.nomLayers(4,1);
    %
    %     layerList.Units      = layerListOrig.Units(1:2,:);
    %     layerList.Units(3,1) = layerListOrig.Units(3,1);
    %     layerList.Units(3,2) = layerListOrig.Units(4,1);
    %
    %     layerList.Comments      = layerListOrig.Comments(1:2,:);
    %     layerList.Comments(3,1) = layerListOrig.Comments(3,1);
    %     layerList.Comments(3,2) = layerListOrig.Comments(4,1);
    %
    %
    %   a = SelectItemFromLayerUiDialog(layerList, 'titleDataSet', titleDataSet);
    %   a.openDialog();
    %   % Output :
    %   a.selectedDatagramList
    %
    % Authors : MHO
    % See also SimpleTitleDialog ClModel Authors
    % ----------------------------------------------------------------------------
    
    properties
        selectedDatagramList = [];
    end
    
    properties (Access = private)
        titleDataSet
        
        uitableComponent
        
        selectionMode
        orientation
        checkBoxExist
        
        nbLine
        nbColumn
        dataSet
        
        idxSelect
        colWidth
        
        selectAllButton
    end
    
    
    methods
        function this = SelectItemFromLayerUiDialog(layerList, varargin)
            % Superclass Constructor
            this = this@SimpleTitleUiDialog('resizable', 'off', varargin{:});
            
            [varargin, this.titleDataSet]  = getPropertyValue(varargin, 'titleDataSet',  []);
            [varargin, this.selectionMode] = getPropertyValue(varargin, 'SelectionMode', 'Multiple');
            [varargin, this.orientation]   = getPropertyValue(varargin, 'Orientation',   'portrait');
            [varargin, this.checkBoxExist] = getPropertyValue(varargin, 'CheckBoxExist',  true); %#ok<ASGLU>
            
            if isempty(this.Title)
                this.Title = 'Select datagrams you want to save';
            end
            
            %             % Create uitable data
            %             this.mandatoryUitableData = [];
            %             for k=1:length(mandatoryDatagramList.name)
            %                 this.mandatoryUitableData = [ this.mandatoryUitableData; {mandatoryDatagramList.nb(k), mandatoryDatagramList.code(k),mandatoryDatagramList.name{k}, true}];
            %             end
            %
            %
            %             this.optUitableData = [];
            %             for k=1:length(optDatagramList.name)
            %                 this.optUitableData = [ this.optUitableData; {optDatagramList.nb(k), optDatagramList.code(k),optDatagramList.name{k}, true}];
            %             end
                        
            if ~isfield(layerList, 'SelectionLayers')
                for k1=1:size(layerList.nomLayers,1)
                    for k2=1:size(layerList.nomLayers,2)
                        layerList.SelectionLayers{k1,k2} = true(1,length(layerList.nomLayers{k1,k2}));
                    end
                end
            end
            if ~isfield(layerList, 'Order')
                for k1=1:size(layerList.nomLayers,1)
                    for k2=1:size(layerList.nomLayers,2)
                        layerList.Order{k1,k2} = false(1,length(layerList.nomLayers{k1,k2}));
                    end
                end
            end
            if ~isfield(layerList, 'Units')
                for k1=1:size(layerList.nomLayers,1)
                    for k2=1:size(layerList.nomLayers,2)
                        layerList.Units{k1,k2} = cell(1,length(layerList.nomLayers{k1,k2}));
                    end
                end
            end
            if ~isfield(layerList, 'Comments')
                for k1=1:size(layerList.nomLayers,1)
                    for k2=1:size(layerList.nomLayers,2)
                        layerList.Comments{k1,k2} = cell(1,length(layerList.nomLayers{k1,k2}));
                    end
                end
            end
            
            this.nbLine = size(layerList.SelectionLayers,1);
            this.nbColumn = size(layerList.SelectionLayers,2);
            
            this.dataSet = cell(this.nbLine, this.nbColumn);
            for iL=1:this.nbLine
                for iC=1:this.nbColumn
                    %         for k=1:size(layerList.SelectionLayers{iL,iC},2)
                    for k=1:length(layerList.SelectionLayers{iL,iC}(:))
                        % Composition des données (concaténation de Nom et Unité si ce
                        % dernier existe).
                        if isfield(layerList, 'Units')
                            if ~isempty(layerList.Units{iL,iC}{k})
                                strName = [layerList.nomLayers{iL,iC}{k} ' (' layerList.Units{iL,iC}{k} ')'];
                            else
                                strName = layerList.nomLayers{iL,iC}{k};
                            end
                            this.dataSet{iL,iC}{k} = {logical(layerList.SelectionLayers{iL,iC}(k)), strName};
                        else
                            this.dataSet{iL,iC}{k} = {logical(layerList.SelectionLayers{iL,iC}(k)), layerList.nomLayers{iL,iC}{k}};
                        end
                    end
                end
            end
            
            this.dataSet(cellfun(@isempty,  this.dataSet)) = [];
            
            this.nbLine = size( this.dataSet,1);
            this.nbColumn = size( this.dataSet,2);
            
            
            %% Mise en forme des données selon que le sélecteur soit CheckBox ou un Select d'une ligne
            
            if this.checkBoxExist == false
                %                 colFormat     = {'char'};
                %                 colEditStatus = false;
                idxName = 1;
            else
                %                 colFormat     = {'logical', 'char'};
                %                 colEditStatus = [true false];
                idxName = 2;
            end
            
            this.idxSelect = cell( this.nbLine,this.nbColumn);
            cellMaxLen     = zeros( this.nbLine,this.nbColumn);
            this.colWidth  = cell( this.nbLine, this.nbColumn);
            
            sizeWidthCheckBox = 30;
            
            for iL=1: this.nbLine
                for iC=1: this.nbColumn
                    if ~isempty( this.dataSet{iL,iC})
                        pppp   = [ this.dataSet{iL,iC}{:,:}];
                        if this.checkBoxExist == false
                            dummy           = reshape(pppp, [2 size( this.dataSet{iL,iC},2)])';
                            this.dataSet{iL,iC}  = dummy(:,2);
                            % Détection des index de pre-sélection dans le cas où il n'y a pas de
                            % CheckBox.
                            idx     = [dummy{:,1}];
                            [u, v]  = find(idx == 1); %#ok<ASGLU>
                            this.idxSelect{iL,iC} = (v-1);
                            
                            % Calcul de la longueur max des chaînes pour ajustement des
                            % colonnes
                            maxWidth = 0;
                            for k=1:size( this.dataSet{iL,iC},1)
                                maxWidth = max(maxWidth, length( this.dataSet{iL,iC}{k,idxName}));
                            end
                            % Some calibration needed as ColumnWidth is in pixels
                            % http://www.mathworks.fr/support/solutions/en/data/1-EIJGZ9/index.html?solution=1-EIJGZ9
                            cellMaxLen(iL, iC) = maxWidth*7;
                            this.colWidth{iL, iC}   = {cellMaxLen(iL, iC)};
                        else
                            this.dataSet{iL,iC}  = reshape(pppp, [2 size( this.dataSet{iL,iC},2)])';
                            % Calcul de la longueur max des chaînes pour ajustement des
                            % colonnes
                            maxWidth        = 0;
                            for k=1:size( this.dataSet{iL,iC},1)
                                maxWidth = max(maxWidth, length( this.dataSet{iL,iC}{k,idxName}));
                            end
                            % Some calibration needed as ColumnWidth is in pixels
                            % http://www.mathworks.fr/support/solutions/en/data/1-EIJGZ9/index.html?solution=1-EIJGZ9
                            cellMaxLen(iL, iC) = maxWidth*7;
                            this.colWidth{iL, iC}   = {sizeWidthCheckBox, cellMaxLen(iL, iC)};
                        end
                    else
                        this.dataSet{iL,iC}= {};
                    end
                end
            end
        end
        
        
        function okPressed(this, ~, ~)
            % Save function : set the output then close dialog
            %
            %             this.mandatoryUitableData = this.mandatoryUitableComponent.Data;
            %             this.optUitableData = this.optUitableComponent.Data;
            %
            %             this.selectedDatagramList   = [this.mandatoryUitableComponent.Data ; this.optUitableComponent.Data];
            
            % Close the dialog
            okPressed@SimpleTitleUiDialog(this);
        end
        
        
        function cancel(this, ~, ~)
            % Close the dialog
            
            %             this.mandatoryUitableData = this.mandatoryUitableComponent.Data;
            %             this.optUitableData = this.optUitableComponent.Data;
            %
            %             this.selectedDatagramList   = [];
            
            % Close the dialog
            cancel@SimpleTitleUiDialog(this);
        end
    end
    
    
    methods (Access = protected)
        function [widthBody , heightBody] = createBody(this, parent)
            % Create Body of SelectLayerUiDialog
            mainGridlayout             = uigridlayout(parent, 'ColumnSpacing', 0, 'RowSpacing', 5, 'Padding', 1);
            mainGridlayout.ColumnWidth = {'1x', 100};
            mainGridlayout.RowHeight   = {'1x', 30};
            
            talbeGridlayout = uigridlayout(mainGridlayout, [this.nbLine this.nbColumn], 'ColumnSpacing', 0, 'RowSpacing', 0, 'Padding', 0);
            talbeGridlayout.Layout.Column = [1 2];
            %              talbeGridlayout.RowHeight = {'1x'};
            %              talbeGridlayout.ColumnWidth = {'1x'};
            
            %             % Création du container de la liste de Layers.
            %             colName         = {'Numbers', 'Code', 'Name', 'Available'};
            %             colFormat       = {'numeric', 'numeric', 'char', 'logical'};
            %             colEditOpt      = [false false false true];
            %             colEditMand     = [false false false false];
            %             colSize   = {70, 70, 300, 100};
            %
            %
            %             mandatoryPanel = uipanel(verticalGridlayout,  'Title', 'Mandatory datagrams');
            %             mandatoryGridlayout = uigridlayout(mandatoryPanel, 'ColumnSpacing', 0, 'RowSpacing', 5, 'Padding', 1);
            %             mandatoryGridlayout.ColumnWidth = {'1x'};
            %             mandatoryGridlayout.RowHeight = {'1x'};
            %             this.mandatoryUitableComponent = uitable(mandatoryGridlayout, ...
            %                 'Data',           this.mandatoryUitableData,...
            %                 'ColumnName',     colName,...
            %                 'ColumnFormat',   colFormat,...
            %                 'ColumnEditable', colEditMand,...
            %                 'RowName',        [], ...
            %                 'ColumnWidth',    colSize);
            %             %             mandatoryPanel.Layout.Column = [1 2];
            %
            %
            %
            %             optionalPanel = uipanel(verticalGridlayout,  'Title', 'Optionnal datagrams');
            %             optionalGridlayout = uigridlayout(optionalPanel, 'ColumnSpacing', 0, 'RowSpacing', 5, 'Padding', 1);
            %             optionalGridlayout.ColumnWidth = {'1x', 100};
            %             optionalGridlayout.RowHeight = {'1x', 30};
            %             this.optUitableComponent = uitable(optionalGridlayout, ...
            %                 'Data',           this.optUitableData,...
            %                 'ColumnName',     colName,...
            %                 'ColumnFormat',   colFormat,...
            %                 'ColumnEditable', colEditOpt,...
            %                 'RowName',        [], ...
            %                 'ColumnWidth',    colSize);
            %             this.optUitableComponent.Layout.Column = [1 2];
            
            if this.checkBoxExist == false
                colFormat     = {'char'};
                colEditStatus = false;
            else
                colFormat     = {'logical', 'char'};
                colEditStatus = [true false];
            end
            
            %% Création des tables
            
%             jTableHeight = zeros(this.nbLine,this.nbColumn);
%             jTableWidth  = zeros(this.nbLine,this.nbColumn);
            for iL=1:this.nbLine
                for iC=1:this.nbColumn
                    if ~isempty([this.dataSet{iL,iC}])
                        
                        tablePanel = uipanel(talbeGridlayout, 'Title',  this.titleDataSet(iL,iC));
                        
                        this.uitableComponent(iL, iC) = uitable(tablePanel, ...
                            'Data',                 this.dataSet{iL,iC}, ...
                            'RearrangeableColumns', 'off', ...
                            'ColumnName',           [],...
                            'ColumnFormat',         colFormat,...
                            'ColumnEditable',       colEditStatus,...
                            'RowName',              [], ...
                            'ColumnWidth',          this.colWidth{iL,iC});
                        
                        tablePanel.Layout.Row = iL;
                        tablePanel.Layout.Column = iC;
                        
                        %             posTable            = get(hTabDataSet(iL, iC), 'Extent');
                        %             jTableHeight(iL,iC) = posTable(4) - 4; % 4, valeur empirique
                        %             jTableWidth(iL,iC)  = posTable(3) - 4; % 4, valeur empirique
                        %             % Remise en unité d'origine.
                        %             set(hTabDataSet(iL, iC), 'units', unitsHdlTable);
                        
                        %             % Sans affichage du CheckBox, on présélectionne les Items.
                        %             if this.checkBoxExist == false
                        %                 idxFieldName = 0;
                        %                 % Surlignage des lignes sélectionnées.
                        %                 flag = uiTable_fcnSetFocusOnIndices(hTabDataSet(iL, iC), idxSelect{iL, iC}, idxFieldName);
                        %                 if ~flag
                        %                     return
                        %                 end
                        %
                        %                 % Callback de gestion du mode de Selection Single.
                        %                 % Pb à faire fonctionner le mode Single pour un UiTable.
                        % %                 if strcmpi(this.selectionMode, 'Single')
                        % %                     [flag, jTable, jScroll] = uiTable_fcnGetJTableHandle(hTabDataSet(iL, iC)); %#ok<ASGLU>
                        % %                     h = handle(jTable,'callbackproperties');
                        % %                     % get(h) %list callbacks
                        % %                     set(h,'MouseClickedCallback',{@cbCellSelectionInJTable, hdlFig, iL, iC});
                        % %                     % uiTable_fcnSetSelectionMode(hTabDataSet(iL, iC), 0);
                        % %                     % Non-fonctionnel ! set(hTabDataSet(iL, iC), 'ButtonDownFcn', @cbCellSelectionInTable);
                        % %                 end
                        %
                        %             else
                        %                 jTableWidth(iL,iC) = jTableWidth(iL,iC) + sizeWidthCheckBox;
                        %                 % Callback de gestion du mode de Selection Single.
                        %                 if strcmpi(SelectionMode, 'Single')
                        %                     set(hTabDataSet(iL, iC), 'CellEditCallback', @cbCellSelectionInTable);
                        %                 end
                        %             end
                        
                    else
                        %             % Si on est en mode Selection Multiple.
                        %             if ~strcmpi(this.selectionMode, 'Single')
                        %                 set(hBtnCheckAll(iL, iC), 'enable', 'off');
                        %                 set(hBtnUncheckAll(iL, iC), 'enable', 'off');
                        %             end
                    end
                end
            end
            
            % Select/ Unselect all
            this.selectAllButton = uibutton(mainGridlayout, 'push', ...
                'Text',    'Unselect All', ...
                'ButtonPushedFcn', @this.selectallCallback);
            this.selectAllButton.Layout.Column = 2;
            
            %             verticalGridlayout.RowHeight ={'1x', '1x'};
            
            widthBody  = 700;
            heightBody = 800;
        end
        
        
        function selectallCallback(this, src, ~)
            
            %             this.mandatoryUitableData = this.mandatoryUitableComponent.Data;
            %             this.optUitableData = this.optUitableComponent.Data;
            %
            %             if strcmp(src.Text, 'Select All')
            %                 src.Text = 'Unselect All';
            %
            %                 %                 for k2=1:length(this.mandatoryUitableData)
            %                 %                     this.mandatoryUitableData{k2,end} = true;
            %                 %                 end
            %                 for k2=1:size(this.optUitableData,1)
            %                     this.optUitableData{k2,end} = true;
            %                 end
            %             else
            %                 src.Text = 'Select All';
            %                 %                 for k2=1:length(this.mandatoryUitableData)
            %                 %                     this.mandatoryUitableData{k2,end} = false;
            %                 %                 end
            %                 for k2=1:size(this.optUitableData,1)
            %                     this.optUitableData{k2,end} = false;
            %                 end
            %             end
            %
            %             %             this.mandatoryUitableComponent.Data =  this.mandatoryUitableData;
            %             this.optUitableComponent.Data =  this.optUitableData;
            
        end
    end
end
