classdef SelectLayerUiDialog < handle & SimpleTitleUiDialog
    % Description
    %   Class SelectLayerUiDialog
    %
    % Syntax
    %   a = SelectLayerUiDialog(...)
    %
    % Output Arguments
    %   selectLayers : Layer sélectionnés.
    %   selectIndex  : Layer indiqués comme Index.
    %
    % Examples
    %     clear list1
    %     list1(1) = struct('type', 'Reflectivity', 'name', 'Reflectivity_0001_12032013', 'Index', 0);
    %     list1(2) = struct('type', 'ReflectivityFromSnippets', 'name', 'ReflectivityFromSnippets_0001_12032013', 'Index', 0);
    %     list1(3) = struct('type', 'Bathymetry', 'name', 'Bathymetry_0001_12032013', 'Index', 0);
    %     list1(4) = struct('type', 'TxAngle', 'name', 'TxAngle_0001_12032013', 'Index', 1);
    %   a = SelectLayerUiDialog(list1);
    %   a.openDialog();
    %   % Output :
    %   a.selectedLayerList
    %
    % Authors : MHO
    % See also SimpleTitleDialog ClModel Authors
    % ----------------------------------------------------------------------------
    
    properties
        selectedLayerList = [];
    end
    
    properties  (Access = private)
        uitableData
        listLayer
        
        % uicontrols
        uitableComponent
        selectAllButton
    end
    
    
    methods
        function this = SelectLayerUiDialog(listLayer, varargin)
            % Superclass Constructor
            this = this@SimpleTitleUiDialog('resizable', 'off', varargin{:});
            if isempty(this.Title)
                this.Title = 'Select Layers you want to consider as index';
            end
            
            this.listLayer = listLayer;
            
            % Create uitable data
            this.uitableData = [];
            for k=1:length(this.listLayer)
                this.uitableData = [ this.uitableData; {this.listLayer(k).type, this.listLayer(k).name, logical(this.listLayer(k).Index)}, true];
            end
            
            
        end
        
        function okPressed(this, ~, ~)
            % Save function : set the output then close dialog
            
            this.uitableData = this.uitableComponent.Data;
            this.selectedLayerList   = this.uitableComponent.Data;
            
            % Close the dialog
            okPressed@SimpleTitleUiDialog(this);
        end
        
        function cancel(this, ~, ~)
            % Close the dialog
            
            this.uitableData = this.uitableComponent.Data;
            this.selectedLayerList   = this.uitableComponent.Data;
            
            this.selectedLayerList   = [];
            
            % Close the dialog
            cancel@SimpleTitleUiDialog(this);
        end
        
        
    end
    
    methods (Access = protected)
        function [widthBody ,heightBody] = createBody(this, parent)
            % Create Body of SelectLayerUiDialog
            verticalGridlayout = uigridlayout(parent, 'ColumnSpacing', 0, 'RowSpacing', 5, 'Padding', 1);
            verticalGridlayout.ColumnWidth = {'1x', 100};
            
            
            % Création du container de la liste de Layers.
            colName   = {'Type', 'Name', 'Index Layer', 'Selected'};
            colFormat = {'char', 'char', 'logical', 'logical'};
            colEdit   = [false false true true];
            
            colSize   = {200, 400, 100, 100};
            this.uitableComponent = uitable(verticalGridlayout, ...
                'Data',           this.uitableData,...
                'ColumnName',     colName,...
                'ColumnFormat',   colFormat,...
                'ColumnEditable', colEdit,...
                'RowName',        [], ...
                'ColumnWidth',    colSize);
            this.uitableComponent.Layout.Column = [1 2];
            
            % Select/ Unselect all
            this.selectAllButton = uibutton(verticalGridlayout, 'push', ...
                'Text',    'Unselect All', ...
                'ButtonPushedFcn', @this.selectallCallback);
            this.selectAllButton.Layout.Column = 2;
            
            verticalGridlayout.RowHeight ={'1x', 30};
            
            widthBody  = 805;
            heightBody = 400;
        end
        
        function selectallCallback(this, src, ~)
            
            this.uitableData = this.uitableComponent.Data;
            
            if strcmp(src.Text, 'Select All')
                src.Text = 'Unselect All';
                
                for k2=1:size(this.uitableData,1)
                    this.uitableData{k2,end} = true;
                end
            else
                src.Text = 'Select All';
                for k2=1:size(this.uitableData,1)
                    this.uitableData{k2,end} = false;
                end
            end
            
            this.uitableComponent.Data =  this.uitableData;
            
        end
    end
end
