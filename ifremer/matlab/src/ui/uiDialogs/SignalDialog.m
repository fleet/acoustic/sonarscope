classdef SignalDialog < handle & SimpleTitleDialog
    % Description
    %   Class SignalDialog
    %
    % SignalDialog properties :
    %
    % Examples
    %
    %   % Exemple 1 :
    %   ySample = YSample('name', 'ysample1', 'data', sin(0:0.1*pi:10*pi-0.1*pi));
    %   signal  = ClSignal('name', 'Simple sinus function','ySample',ySample);
    %   a = SignalDialog(signal, 'Title', 'Signal', 'CleanInterpolation', true);
    %   a.openDialog();
    %
    %   % Exemple 2 :
    %   ySampleSignal1    = YSample('name', 'ysample1', 'data', cos(0:0.1*pi:10*pi-0.1*pi));
    %   ySampleSignal1(2) = YSample('name', 'ysample2', 'data', sin(0:0.1*pi:10*pi-0.1*pi));
    %   signal = ClSignal('name', 'signal1','ySample',ySampleSignal1);
    %   ySampleSignal2    = YSample('name', 'ysample1', 'data', cos(0:0.1*pi:10*pi-0.1*pi)+0.2);
    %   ySampleSignal2(2) = YSample('name', 'ysample2', 'data', sin(0:0.1*pi:10*pi-0.1*pi)+0.2);
    %   ySampleSignal2(3) = YSample('name', 'ysample3', 'data', sin(0:0.1*pi:10*pi-0.1*pi)+0.5);
    %   signal(2) = ClSignal('name', 'signal2','ySample',ySampleSignal2);
    %   a = SignalDialog(signal, 'Title', 'Signal', 'waitAnswer', false);
    %   a.openDialog();
    %
    %   % Exemple 3 :
    %     nomFicCurve = getNomFicDatabase('0554_20140726_012633_Amundsen_Reflectivity_BeamPointingAngle_TxBeamIndexSwath.mat');
    %     [CourbeConditionnelle, flag] = load_courbesStats(nomFicCurve);
    %     Colors = 'brkgmc';
    %     sectors = CourbeConditionnelle.bilan{1};
    %     nbSectors = length(sectors);
    %     clear ySample1 ySample2;
    %
    %     for k=1:nbSectors
    %         kmod = 1 + mod(k, length(Colors));
    %         xSample(k)  = XSample('name', 'time', 'data', sectors(k).x);
    %         ySample1(k) = YSample('Name', sectors(k).titreV, 'data', sectors(k).y,  'unit',  sectors(k).Unit, 'Color', Colors(kmod));
    %         ySample2(k) = YSample('Name', sectors(k).titreV, 'data', sectors(k).nx, 'unit', 'nb',             'Color', Colors(kmod));
    %     end
    %
    %     sigCurves    = ClSignal('Name', strcat('Curves ', num2str(1)), 'xSample', xSample, 'ySample', ySample1);
    %     sigCurves(2) = ClSignal('Name', strcat('Curves ', num2str(2)), 'xSample', xSample, 'ySample', ySample2);
    %
    %     a = SignalDialog(sigCurves, 'Title', 'sigCurves');
    %     a.openDialog();
    %
    %
    %   % Exemple 4 (avec ClImageData)
    %   nomFic = 'Z:\private\ifremer\JMA\DemoImageEnBackground\DF1000_ExStr-grRawImage_PingRange_Reflectivity.xml';
    %   [flag, a] = cl_image.import_xml(nomFic);
    %   H = get(a, 'SonarHeight');
    %   I = get(a, 'Image');
    %   x = get(a, 'x');
    %   y = get(a, 'y');
    %
    %   imageData = ClImageData('cData', I, 'xData', x, 'yData', y, 'colormapData', gray);
    %
    %   xSample = XSample('name', 'Depth', 'data', H);
    %   ySample = YSample('Name', 'Ping',  'data', 1:length(H));
    %
    %   a = ClSignal('Name', 'test image', 'xSample', xSample, 'ySample', ySample, 'isVertical', false);
    %   b = SignalDialog(a, 'Title', 'Demo SignalDialog with background image', 'waitAnswer', false, 'imageDataList', imageData);
    %   b.openDialog();
    %
    %   a = ClSignal('Name', 'test image', 'xSample', xSample, 'ySample', ySample, 'isVertical', true);
    %   b = SignalDialog(a, 'Title', 'Demo SignalDialog with background image', 'waitAnswer', false, 'imageDataList', imageData);
    %   b.openDialog();
    %
    %
    %   % Exemple 5 (avec 2 images en background):
    %   nomFic = 'Z:\private\ifremer\JMA\DemoImageEnBackground\DF1000_ExStr-grRawImage_PingRange_Reflectivity.xml';
    %   [flag, a] = cl_image.import_xml(nomFic);
    %   H = get(a, 'SonarHeight'); I = get(a, 'Image'); x = get(a, 'x'); y = get(a, 'y');
    %
    %   xSample   = XSample('data', 1:length(H));
    %   ySample1  = YSample('name', 'test1', 'data',  H, 'color', 'r');
    %   ySample2  = YSample('name', 'test2', 'data', -H, 'color', 'r');
    %   signal    = ClSignal('name', 'Test1', 'xSample', xSample, 'ySample', ySample1);
    %   signal(2) = ClSignal('name', 'Test2', 'xSample', xSample, 'ySample', ySample2);
    %
    %   %im(1) = imagesc(y, x, I'); colormap(gray(256))
    %   imageDataList = ClImageData('cData', I', 'xData', y, 'yData', x, 'colormapData', gray);
    %   %im(2) = imagesc(y, x, I'); colormap(gray(256))
    %   imageDataList(2) = ClImageData('cData', I', 'xData', y, 'yData', x, 'colormapData', gray);
    %   % parentAxesArray = [im(1).Parent, im(2).Parent];
    %   signalDlg = SignalDialog(signal, 'Title', 'Signal', 'imageDataList' , imageDataList);
    %   signalDlg.openDialog();
    %
    %
    %   % Exemple 6 (avec image en background & imagesc) :
    %   % [Attention, il est préférable d'utiliser l'objet ClImageData (exemple 4) ]
    %   nomFic = 'Z:\private\ifremer\JMA\DemoImageEnBackground\DF1000_ExStr-grRawImage_PingRange_Reflectivity.xml';
    %   [flag, a] = cl_image.import_xml(nomFic);
    %   H = get(a, 'SonarHeight');
    %   I = get(a, 'Image');
    %   x = get(a, 'x');
    %   y = get(a, 'y');
    %
    %   xSample(1) = XSample('data', H);
    %   ySample(1) = YSample('name', 'test1', 'data', 1:length(H));
    %   xSample(2) = XSample('data', -H);
    %   ySample(2) = YSample('name', 'test2', 'data', 1:length(H));
    %   signal  = ClSignal('name', 'Test', 'xSample', xSample, 'ySample', ySample);
    %
    %   figure
    %   im = imagesc(x, y, I); colormap(im.Parent, gray);
    %   signalDlg = SignalDialog(signal, 'Title', 'Signal', 'signalAxesArray', im.Parent);
    %   signalDlg.openDialog();
    %
    %   % Exemple 7 Récupèration des paramètres de filtrage :
    %   % Paramètres de filtrage
    %   FilterDataParametre.FilterNameList
    %   FilterDataParametre.WcNameList
    %   filterDataParam = FilterDataParametre('FilterType', FilterDataParametre.FilterNameList(1), 'ButterworthWc', FilterDataParametre.WcNameList(3), 'ButterworthOrder', 2)
    %
    %   ySample = YSample('name', 'ysample1', 'data', sin(0:0.1*pi:10*pi-0.1*pi));
    %   signal  = ClSignal('name', 'Simple sinus function','ySample',ySample);
    %   a = SignalDialog(signal, 'Title', 'Signal Sin', 'CleanInterpolation', true, 'filterDataParametre', filterDataParam);
    %   a.openDialog();
    %   % Recupération du paramètres de filtrage
    %   filterDataParametre= a.filterDataParametre
    %
    % Authors : MHO
    % See also ClParametre ParametreComponent Authors
    % ----------------------------------------------------------------------------
    
    properties (Constant)
        lineClassName = 'matlab.graphics.chart.primitive.Line'
    end
    
    properties
        signalContainerList
        
        displayedSignalContainerList % 1 if displayed, 0 if not
        displayedSignalList          % 1 if displayed, 0 if not
        displayedSampleList          % 1 if displayed, 0 if not
        
        signalAxesArray              % axes array
        cleanInterpolation = false;
        
        imageDataList                % list of image data to display on axes
        
        filterDataParametre          % Filter Data Parametre
    end
    
    properties (Access = protected)
        %ui
        filterParametreComponent % box of parameters for filer mode
        signalContainerSelectionLabel = 'Signal Container Selection';
        signalSelectionLabel          = 'Signal Selection';
        signalBox
        ySampleSelectionLabel         = 'Sample Selection'
        syncContextMenuItemList       = matlab.ui.container.Menu.empty;
        signalListBox
        xLimModeToggleButton
        yLimModeToggleButton
        
        manualCleanDataMode
        algoCleanDataMode
        filterDataMode
        editDataMode
        drawDataMode
        
        % ui - axes/line array
        signalBoxArray
        signalLineArray
        
        %data
        signalMaxNb            % number max of ClSignal for one signalContainer
        ySampleMaxNb           % number max of ySample for one signal
        signalYSampleLineIndex % matrix of index of line for signalContainer, signal and ySample
        syncSignalList         % 1 if sync, 0 if not
    end
    
    methods
        function this = SignalDialog(signalContainerList, varargin)
            % Superclass Constructor
            % (windowsStyle = normal because of bug of get(jcleanerTool, 'MenuComponent');
            this = this@SimpleTitleDialog('resizable', 'on', 'windowStyle', 'normal', varargin{:});
            if isempty(this.Title)
                this.Title = 'Signal';
            end
            
            if isa(signalContainerList, 'ClSignal')
                signalContainer = SignalContainer('signalList', signalContainerList);
                this.signalContainerList = signalContainer;
            elseif isa(signalContainerList, 'SignalContainer')
                this.signalContainerList = signalContainerList;
            end
            
            [varargin, this.cleanInterpolation]  = getPropertyValue(varargin, 'CleanInterpolation',  false);
            [varargin, this.signalAxesArray]     = getPropertyValue(varargin, 'signalAxesArray',     []);
            [varargin, this.imageDataList]       = getPropertyValue(varargin, 'imageDataList',       []);
            [varargin, this.filterDataParametre] = getPropertyValue(varargin, 'filterDataParametre', []); %#ok<ASGLU>
            
            % init the list of displayed signal Containers
            this.displayedSignalContainerList = true(1, numel(this.signalContainerList));
            
            % compute the max number of signal
            for signalContainerIndex=1:numel(this.signalContainerList)
                signalNb(signalContainerIndex) = numel(this.signalContainerList(signalContainerIndex).signalList); %#ok<AGROW>
            end
            this.signalMaxNb = max(signalNb);
            % init the list of displayed signals
            this.displayedSignalList = true(1, this.signalMaxNb);
            
            %compute the max number of ySample
            for signalContainerIndex=1:numel(this.signalContainerList)
                for signalIndex=1:this.signalMaxNb
                    if signalIndex <= numel(this.signalContainerList(signalContainerIndex).signalList) % Check if signal exists
                        ySampleNb(signalContainerIndex, signalIndex) = numel(this.signalContainerList(signalContainerIndex).signalList(signalIndex).ySample); %#ok<AGROW>
                    end
                end
            end
            this.ySampleMaxNb = max(max(ySampleNb));
            % init the list of displayed sample
            this.displayedSampleList = true(1, this.ySampleMaxNb);
            
            % Compute the matrix of index for signalContainer, signal and ySample
            this.signalYSampleLineIndex = zeros(numel(this.signalContainerList), this.signalMaxNb, this.ySampleMaxNb);
            
            for signalIndex=1:this.signalMaxNb
                increment = 0;
                for signalContainerIndex=1:numel(this.signalContainerList)
                    if signalIndex <= numel(this.signalContainerList(signalContainerIndex).signalList) % Check if signal exists
                        for ySampleIndex=1:this.ySampleMaxNb
                            if  ySampleIndex <= numel(this.signalContainerList(signalContainerIndex).signalList(signalIndex).ySample)
                                increment = increment+1;
                                this.signalYSampleLineIndex(signalContainerIndex, signalIndex, ySampleIndex) = increment;
                            end
                        end
                    end
                end
            end
            
            % init the list of sync signals
            this.syncSignalList = true(1, this.signalMaxNb);
        end
        
        
        function okPressed(this, ~, ~)
            % Override the function in mother class SimpleTitleDialog
            
            % perform clean if needed
            if this.manualCleanDataMode.dirty
                this.manualCleanDataMode.performClean();
            end
            if this.algoCleanDataMode.dirty
                this.algoCleanDataMode.performClean();
            end
            % perform filter if needed
            if this.filterDataMode.dirty
                this.filterDataMode.performClean();
            end
            % perform editDataMode if needed
            if this.editDataMode.dirty
                this.editDataMode.performEdit();
            end
            % perform drawDataMode if needed
            if this.drawDataMode.dirty
                this.drawDataMode.performDraw();
            end
            
            % save filter parametres in variable
            this.filterDataParametre = this.filterDataMode.filterParametreComponent.getFilterDataParametre();
            
            okPressed@SimpleTitleDialog(this);
        end
        
        
        function cancel(this, ~, ~)
            % Override the function in mother class SimpleTitleDialog
            
            % save filter parametre in variable
            this.filterDataParametre = this.filterDataMode.filterParametreComponent.getFilterDataParametre();
            
            cancel@SimpleTitleDialog(this);
        end
        
        
        function saved = saveChangePlotInSignal(this)
            % Save modifiedPlot in Navigation Model
            
            saved = '';
            % Get cleanDataMode and check if dirty (not empty)
            cleanDataMode = this.getCleanDataMode();
            if ~isempty(cleanDataMode)
                
                % Construct a questdlg
                saved = questdlg('Do you want to save the modifications ?', ...
                    'Clean Data Save', 'Yes', 'No', 'Yes');
                
                % Handle response
                switch saved
                    case 'Yes' % SAVE
                        % set lines modification in ySample signal instance
                        for signalContainerIndex=1:numel(this.signalContainerList)
                            for signalIndex=1:this.signalMaxNb
                                if this.displayedSignalList(signalIndex) % if this signal is displayed
                                    
                                    % Find tmpLine
                                    axes = this.signalAxesArray(signalIndex);
                                    if (this.manualCleanDataMode.dirty || this.algoCleanDataMode.dirty || this.editDataMode.dirty ) % CleanMode
                                        
                                        drawnow; %pause(0.1)
                                        
                                        [lineList, ~] = TmpLineUtils.getLines(axes);
                                        
                                        for ySampleIndex=1:this.ySampleMaxNb
                                            lineIndex = this.signalYSampleLineIndex(signalContainerIndex, signalIndex, ySampleIndex);
                                            if lineIndex ~= 0
                                                line = lineList(lineIndex);
                                                
                                                this.signalContainerList(signalContainerIndex).signalList(signalIndex).xSample(ySampleIndex).data =   line.XData;
                                                this.signalContainerList(signalContainerIndex).signalList(signalIndex).ySample(ySampleIndex).data =   line.YData;
                                            end
                                        end
                                        
                                    elseif this.filterDataMode.dirty || this.drawDataMode.dirty % FilterMode & EditMode & DrawMode
                                        [lineList, ~] = TmpLineUtils.getLines(axes);
                                        
                                        for ySampleIndex=1:this.ySampleMaxNb
                                            lineIndex = this.signalYSampleLineIndex(signalContainerIndex, signalIndex, ySampleIndex);
                                            if lineIndex~=0
                                                line = lineList(lineIndex);
                                                this.signalContainerList(signalContainerIndex).signalList(signalIndex).ySample(ySampleIndex).data = line.YData;
                                            end
                                        end
                                    end
                                end
                            end
                        end
                        
                    case 'No' % DONT SAVE
                        % Set tmpLines in Line
                        for signalIndex=1:this.signalMaxNb
                            axes = this.signalAxesArray(signalIndex);
                            [lineList, tmpLineList] = TmpLineUtils.getLines(axes);
                            
                            if (~isempty(lineList) && ~isempty(tmpLineList))
                                
                                for lineIndex = 1:numel(lineList)
                                    
                                    line = lineList(lineIndex);
                                    tmpLine = tmpLineList(lineIndex);
                                    
                                    line.XData = tmpLine.XData;
                                    line.YData = tmpLine.YData;
                                end
                            end
                        end
                end
            end
        end
    end
    
    
    methods (Access = protected)
        
        function configureFigure(this)
            % Override the function in mother class SimpleTitleDialog
            
            fig = this.parentFigure;
            
            fig.set('ToolBar', 'auto', 'menubar', 'figure');
            % add an Add-On Menu in menubar of input figure
            FigUtils.addAdOnMenu(fig, 'isLightAddOnMenu', true, 'editPropertiesObject', this.signalContainerList);
            % Add Undo Redo tool bar items
            FigUtils.addUndoRedoToolBarItems(fig)
            
            % Create the manual clean mode
            this.manualCleanDataMode = CleanDataMode(this.parentFigure, true, this.cleanInterpolation);
            % Add a listener for clean event & perform clean event
            addlistener(this.manualCleanDataMode, 'CleanEvent',        @this.handleCleanEvent);
            addlistener(this.manualCleanDataMode, 'PerformCleanEvent', @this.handlePerformModifyEvent);
            
            % Create the algo clean mode
            this.algoCleanDataMode = CleanDataMode(this.parentFigure, false, this.cleanInterpolation);
            % Add a listener for clean event & perform clean event
            addlistener(this.algoCleanDataMode, 'CleanEvent',        @this.handleCleanEvent);
            addlistener(this.algoCleanDataMode, 'PerformCleanEvent', @this.handlePerformModifyEvent);
            
            % Create the filter mode
            this.filterDataMode = FilterDataMode(this.parentFigure);
            % Add a listener for filter event & perform filter event
            addlistener(this.filterDataMode, 'FilterEvent',        @this.handleCleanEvent);
            addlistener(this.filterDataMode, 'PerformFilterEvent', @this.handlePerformModifyEvent);
            
            % Create the edit mode
            this.editDataMode = EditDataMode(this.parentFigure, this.cleanInterpolation);
            % Add a listener for edit event & perform edit event
            addlistener(this.editDataMode, 'EditEvent',        @this.handleCleanEvent);
            addlistener(this.editDataMode, 'PerformEditEvent', @this.handlePerformModifyEvent);
            
            % Create the draw mode
            this.drawDataMode = DrawDataMode(this.parentFigure);
            % Add a listener for edit event & perform edit event
            addlistener(this.drawDataMode, 'DrawEvent',        @this.handleCleanEvent);
            addlistener(this.drawDataMode, 'PerformDrawEvent', @this.handlePerformModifyEvent);
        end
        
        
        function configureFigureAfterCreation(this)
            % Override the function in mother class SimpleTitleDialog
            
            %% Clean Data Mode
            % create the manual clean tool menu & menu items
            this.manualCleanDataMode.createCleanToolMenu();
            % create the algo clean tool menu & menu items
            this.algoCleanDataMode.createCleanToolMenu();
            
            %% Filter Data Mode
            % create the filter tool menu & menu items
            this.filterDataMode.createFilterToolMenu();
            % Set the filter parametre component
            this.filterDataMode.setFilterParametreComponent(this.filterParametreComponent);
            % Enabled filter mode if filterDataParametre not empty
            if ~isempty(this.filterDataParametre)
                this.filterDataMode.showHideFilterParametreComponent(true);   % Show FilterParametreComponent
                this.filterDataMode.filterTool.set('State', 'on');   % Button on
                this.filterDataMode.filterToolToggleActionCallback(this.filterDataMode.filterTool); % Start Mode
            end
            
            %% Draw Data Mode
            % create the draw tool menu & menu items
            this.drawDataMode.createDrawToolMenu();
        end
        
        
        function [widthBody, heightBody] = createBody(this, parent)
            % Override the function in mother class SimpleTitleDialog
            % Create Body of NavigationDialog
            
            verticalBox = uiextras.VBox('Parent', parent, 'Spacing', 0);
            
            % FilterParametresComponent
            if isempty(this.filterDataParametre)
                this.filterParametreComponent = FilterDataModeParametreComponent(verticalBox);
            else
                % if not empty, init the filter parametres
                this.filterParametreComponent = FilterDataModeParametreComponent(verticalBox, 'filterDataParametre', this.filterDataParametre);
            end
            
            [signalSelectionPanel, signalSelectionBoxComponentNb] = this.createSignalSelectionBox(verticalBox);
            
            horizontalBox = uiextras.HBoxFlex('Parent', verticalBox, 'Spacing', 5);
            
            this.createLeftBox(horizontalBox);
            
            % Set visible signalSelectionBox if not empty
            if signalSelectionBoxComponentNb == 0
                signalSelectionPanel.Visible = 'off';
                signalSelectionPanelSize     = 0;
            else
                signalSelectionPanel.Visible = 'on';
                signalSelectionPanelSize     = 34;
            end
            
            verticalBox.set('Sizes', [0 signalSelectionPanelSize -1]);
            
            widthBody  = 1000;
            heightBody =  700;
        end
        
        
        function [signalSelectionPanel, signalSelectionBoxComponentNb] = createSignalSelectionBox(this, parent)
            signalSelectionPanel = uiextras.Panel('Parent', parent);
%             signalSelectionPanel = uix.Panel('Parent', parent);
            signalSelectionBox   = uiextras.HBoxFlex('Parent', signalSelectionPanel);
            signalSelectionBoxComponentNb = 0;
            if numel(this.signalContainerList) > 1
                % Signal Container Selection
                signalContainerSelectionBox = uiextras.HBox('Parent', signalSelectionBox, 'Padding', 5, 'Spacing', 5);
                
                % DropDown Button
                dropDownIcon = iconizeFic(getNomFicDatabase('1454012086_ic_arrow_drop_down_48px.png'));
                uicontrol('Parent', signalContainerSelectionBox, ...
                    'Style',    'pushButton', ...
                    'CData',    dropDownIcon, ...
                    'Callback', @this.showSignalContainerSelectionDialog);
                
                uicontrol('Parent', signalContainerSelectionBox, ...
                    'Style',               'text', ...
                    'string',               this.signalContainerSelectionLabel, ...
                    'HorizontalAlignment', 'left');
                
                signalContainerSelectionBox.set('Sizes', [25 -1]);
                signalSelectionBoxComponentNb = signalSelectionBoxComponentNb+1;
            end
            
            if this.signalMaxNb > 1
                % Signal Selection
                signalSelectionIndexBox = uiextras.HBox('Parent', signalSelectionBox, 'Padding', 5, 'Spacing', 5);
                
                % DropDown Button
                dropDownIcon = iconizeFic(getNomFicDatabase('1454012086_ic_arrow_drop_down_48px.png'));
                uicontrol('Parent', signalSelectionIndexBox, ...
                    'Style',    'pushButton', ...
                    'CData',    dropDownIcon, ...
                    'Callback', @this.showSignalSelectionDialog);
                
                uicontrol('Parent', signalSelectionIndexBox, ...
                    'Style',               'text', ...
                    'string',               this.signalSelectionLabel, ...
                    'HorizontalAlignment', 'left');
                
                signalSelectionIndexBox.set('Sizes', [25 -1]);
                signalSelectionBoxComponentNb = signalSelectionBoxComponentNb+1;
            end
            
            if this.ySampleMaxNb > 1
                %Sample selection
                sampleSelectionBox = uiextras.HBox('Parent', signalSelectionBox, 'Padding', 5, 'Spacing', 5);
                % DropDown Button
                dropDownIcon = iconizeFic(getNomFicDatabase('1454012086_ic_arrow_drop_down_48px.png'));
                uicontrol('Parent', sampleSelectionBox, ...
                    'Style',    'pushButton', ...
                    'CData',    dropDownIcon, ...
                    'Callback', @this.showYSampleSelectionDialog);
                
                uicontrol('Parent', sampleSelectionBox, ...
                    'Style',               'text', ...
                    'string',               this.ySampleSelectionLabel, ...
                    'HorizontalAlignment', 'left');
                
                sampleSelectionBox.set('Sizes', [25 -1]);
                signalSelectionBoxComponentNb = signalSelectionBoxComponentNb+1;
            end
            
            if signalSelectionBoxComponentNb == 3
                signalSelectionBox.set('Sizes', [300 300 300]);
            elseif signalSelectionBoxComponentNb == 2
                signalSelectionBox.set('Sizes', [300 300]);
            else
                % nothing to do
            end
        end
        
        
        function createLeftBox(this, parent)
            leftPanel = uiextras.Panel('Parent', parent, 'Title', 'Signals');
%             leftPanel = uix.Panel('Parent', parent, 'Title', 'Signals');
            this.signalBox = uiextras.VBox('Parent', leftPanel);
            
            % Create Axes Action Box
            this.createAxesActionBox(this.signalBox);
            
            % create/update signal list Axes
            this.createSignalListAxes(this.signalBox);
            
            this.signalBox.set('Sizes', [32 -1]);
        end
        
        
        function createAxesActionBox(this, parent)
            % Create signal action box
            axesActionBox = uiextras.HBox('Parent', parent, 'Padding', 5, 'Spacing', 5);
            
            %% Shift Signals buttons
            axesControlButtonsBox = uiextras.HBox('Parent', axesActionBox);
            
            % Icon
            shiftLeftIconName      = 'Actions-media-seek-backward-icon.png';
            shiftRightIconName     = 'Actions-media-seek-forward-icon.png';
            shiftXLimLeftIconName  = 'Actions-media-skip-backward-icon.png';
            shiftXLimRightIconName = 'Actions-media-skip-forward-icon.png';
            zoomInIconName         = 'tool_zoom_in.png';
            zoomOutIconName        = 'tool_zoom_out.png';
            autoCenterZoomIconName = 'binoculars-icon.png';
            
            shiftLeftIcon      = iconRead(  getNomFicDatabase(shiftLeftIconName));
            shiftRightIcon     = iconRead(  getNomFicDatabase(shiftRightIconName));
            shiftXLimLeftIcon  = iconRead(  getNomFicDatabase(shiftXLimLeftIconName));
            shiftXLimRightIcon = iconRead(  getNomFicDatabase(shiftXLimRightIconName));
            zoomInIcon         = iconizeFic(getNomFicDatabase(zoomInIconName));
            zoomOutIcon        = iconizeFic(getNomFicDatabase(zoomOutIconName));
            autoCenterZoomIcon = iconRead(  getNomFicDatabase(autoCenterZoomIconName));
            
            uicontrol('Parent', axesControlButtonsBox, 'Style', 'pushbutton', 'CData', shiftXLimLeftIcon, ...
                'TooltipString', 'Shift to the start', 'Callback', {@this.shiftoXLimCallback, 'left'});
            uicontrol('Parent', axesControlButtonsBox, 'Style', 'pushbutton', 'CData', shiftLeftIcon, ...
                'TooltipString', 'Shift to the left', 'Callback', {@this.shiftSignalsCallback, 'left'});
            uicontrol('Parent', axesControlButtonsBox, 'Style', 'pushbutton', 'CData', zoomInIcon, ...
                'TooltipString', 'Zoom in', 'Callback', {@this.zoomSignalsCallback, 'in'});
            uicontrol('Parent', axesControlButtonsBox, 'Style', 'pushbutton', 'CData', autoCenterZoomIcon, ...
                'TooltipString', 'Auto Center Zoom', 'Callback', {@this.autoCenterZoomCallback});
            uicontrol('Parent', axesControlButtonsBox, 'Style', 'pushbutton', 'CData', zoomOutIcon, ...
                'TooltipString', 'Zoom out', 'Callback', {@this.zoomSignalsCallback, 'out'});
            uicontrol('Parent', axesControlButtonsBox, 'Style', 'pushbutton', 'CData', shiftRightIcon, ...
                'TooltipString', 'Shift to the right', 'Callback', {@this.shiftSignalsCallback, 'right'});
            uicontrol('Parent', axesControlButtonsBox, 'Style', 'pushbutton', 'CData', shiftXLimRightIcon, ...
                'TooltipString', 'Shift to the end', 'Callback', {@this.shiftoXLimCallback, 'right'});
            axesControlButtonsBox.set('Sizes', [25 25 25 25 25 25 25]);
            
            
            %% XLim / YLimMode Auto
            xyLimAutoSignalsButtonsBox = uiextras.HBox('Parent', axesActionBox);
            %XLim Auto Mode
            this.xLimModeToggleButton = uicontrol('Parent', xyLimAutoSignalsButtonsBox, 'Style', 'togglebutton', 'String', 'X Auto', 'TooltipString',  'XLimMode Auto / Manual', 'Callback', {@this.xLimModeAutoSignalsCallback});
            % YLim Auto Mode
            this.yLimModeToggleButton = uicontrol('Parent', xyLimAutoSignalsButtonsBox, 'Style', 'togglebutton', 'String', 'Y Auto', 'TooltipString',  'YLimMode Auto / Manual', 'Callback', {@this.yLimModeAutoSignalsCallback});
            
            xyLimAutoSignalsButtonsBox.set('Sizes', [100 100]);
            
            %                       uiextras.Empty('Parent', axesActionBox);
            
            %% Sync signal
            if this.signalMaxNb > 1
                syncContextMenu = uicontextmenu(this.parentFigure);
                
                %Construct the signalNameList
                signalNameList = {};
                for signalIndex=1:this.signalMaxNb
                    signalNameList{signalIndex} = this.signalContainerList(1).signalList(signalIndex).name; %#ok<AGROW>
                end
                
                for signalIndex=1:this.signalMaxNb
                    checked = 'off';
                    if (this.syncSignalList(signalIndex))
                        checked = 'on';
                    end
                    this.syncContextMenuItemList(signalIndex) = uimenu(syncContextMenu, 'Text', signalNameList{signalIndex}, ...
                        'Checked', checked, 'Callback', @this.syncContextMenuItemCallback);
                end
                % Select all elements
                this.syncContextMenuItemList(end+1) = uimenu(syncContextMenu, 'Separator', 'on', 'Text', 'Select All', ...
                    'Callback', @this.syncContextMenuItemCallback);
                % Unselect all elements
                this.syncContextMenuItemList(end+1) = uimenu(syncContextMenu,'Text', 'Unselect All', ...
                    'Callback', @this.syncContextMenuItemCallback);
                %DropDown Bouton Component
                DropDownButtonComponent(axesActionBox, this.parentFigure, '', [], syncContextMenu, 'sizes', [0 -1]);
                uicontrol('Parent', axesActionBox, ...
                    'Style',               'text', ...
                    'string',              'Select Signals To Synchronize', ...
                    'HorizontalAlignment', 'left');
                
                axesActionBox.set('Sizes', [200 -1 25 -1]);
            else
                axesActionBox.set('Sizes', [200 -1]);
            end
        end
        
        
        function createSignalListAxes(this, parent)
            % Create the Signal List Axes
            
            % create signal list box
            if this.signalContainerList(1).signalList(1).isVertical % signal vertical
                this.signalListBox = uiextras.HBoxFlex('Parent', parent);
            else % signal horizontal
                this.signalListBox = uiextras.VBoxFlex('Parent', parent);
            end
            
            % create axes array
            
            % check if empty or valid axes
            initAxes = false;
            if isempty(this.signalAxesArray)
                initAxes = true;
            else
                for signalIndex=1:this.signalMaxNb
                    if ~isvalid(this.signalAxesArray(signalIndex))
                        initAxes = true;
                    end
                end
            end
            
            % initialize and create axes if needed
            if initAxes
                this.signalAxesArray = matlab.graphics.axis.Axes.empty(this.signalMaxNb,0);
                for signalIndex=1:this.signalMaxNb
                    this.signalAxesArray(signalIndex) = axes('Units', 'normalized');
                    
                    if this.signalContainerList(1).signalList(signalIndex).isVertical
                        % if vertical, rotate each axes
                        camroll(this.signalAxesArray(signalIndex), -90);
                    end
                end
            end
            
            if ~isempty(this.signalAxesArray)
                % plot each SignalContainer & ClSignal
                [~, this.signalAxesArray, this.signalLineArray] = this.signalContainerList.plot('fig', this.parentFigure, 'axesArray', this.signalAxesArray, 'imageDataList', this.imageDataList);
            end
            
            % create box and set parent of axes (after plot to avoid problem with update XLim and panel parent)
            this.signalBoxArray = uiextras.VBox.empty(this.signalMaxNb,0);
            rotate3dObj = rotate3d(this.parentFigure);
            
            % Avoid "Error occurred while moving node between canvases"
            % when set different parent #1149
                drawnow;
            for signalIndex=1:this.signalMaxNb
                this.signalBoxArray(signalIndex) = uiextras.VBox('Parent',this.signalListBox);
%                 this.signalAxesArray(signalIndex).set('Parent', this.signalBoxArray(signalIndex));
                this.signalAxesArray(signalIndex).Parent = this.signalBoxArray(signalIndex); % Modif JMA le 23/01/2020
           
                % avoid rotate3d
                rotate3dObj.setAllowAxesRotate(this.signalAxesArray(signalIndex),false);
            end
            
            % Synchronize XLimAuto YLimAuto
            for k=1:numel(this.signalAxesArray)
                addlistener(this.signalAxesArray(k), 'XLim', 'PostSet', @(src,eventdata)syncXYLimAutoCallback(this,src,eventdata,k));
                addlistener(this.signalAxesArray(k), 'YLim', 'PostSet', @(src,eventdata)syncXYLimAutoCallback(this,src,eventdata,k));
            end
            
            % Show / Hide Signals
            this.showHideSignals();
        end
        
        
        function showHideSignals(this)
            
            % set Visible on/off and size -1/0 for signal Axes
            vBoxSizes = this.signalListBox.get('Sizes');
            
            for signalIndex=1:numel(this.displayedSignalList)
                axes = this.signalAxesArray(signalIndex);
                [lineList, tmpLineList] = TmpLineUtils.getLines(axes);
                
                % show/hide panel
                if this.displayedSignalList(signalIndex)
                    this.signalBoxArray(signalIndex).Visible = 'on';
                    if  vBoxSizes(signalIndex) == 0
                        vBoxSizes(signalIndex) = -1;
                    end
                else
                    this.signalBoxArray(signalIndex).Visible = 'off';
                    vBoxSizes(signalIndex) = 0;
                end
                
                % set line visible
                for signalContainerIndex = 1:numel(this.signalContainerList)
                    if signalIndex <= numel(this.signalContainerList(signalContainerIndex).signalList) % Check if signal exists
                        for sampleIndex = 1:numel(this.signalContainerList(signalContainerIndex).signalList(signalIndex).ySample)
                            lineIndex = this.signalYSampleLineIndex(signalContainerIndex, signalIndex, sampleIndex);
                            if lineIndex ~= 0
                                if this.displayedSignalContainerList(signalContainerIndex) && this.displayedSampleList(sampleIndex)
                                    lineList(lineIndex).Visible = 'on';
                                    lineList(lineIndex).Tag = '';
                                    if ~isempty(tmpLineList)
                                        tmpLineList(lineIndex).Visible = 'on';
                                    end
                                else
                                    lineList(lineIndex).Visible = 'off';
                                    lineList(lineIndex).Tag = TmpLineUtils.toNotCleanLineTagLabel;
                                    if ~isempty(tmpLineList)
                                        tmpLineList(lineIndex).Visible = 'off';
                                    end
                                end
                            end
                        end
                    end
                end
            end
            this.signalListBox.set('Sizes', vBoxSizes);
            
        end
        
        
        function resetXlimAxes(this)
            % Re-Set Xlim of axes (line visible dosnet work with
            % XLimMode = auto)
            for signalIndex=1:numel(this.signalAxesArray)
                % get line list
                axes = this.signalAxesArray(signalIndex);
                [lineList, ~] = TmpLineUtils.getLines(axes);
                
                lineVisibleIndex = 0;
                % compute xmin xmax for each line
                % loop inverted because  xMin/xmax cant be initialised
                % (xMin/xMan numeric or datetime)
                for lineIndex=numel(lineList):-1:1
                    if strcmp(lineList(lineIndex).Visible, 'on')
                        lineVisibleIndex = lineVisibleIndex+1;
                        
                        validIndex = ~isinf(lineList(lineIndex).XData);
                        
                        xMin(lineVisibleIndex) = min(lineList(lineIndex).XData(validIndex));
                        xMax(lineVisibleIndex) = max(lineList(lineIndex).XData(validIndex));
                    end
                end
                
                % set new XLim if at less 1 visible line
                if lineVisibleIndex ~= 0
                    this.signalAxesArray(signalIndex).set('XLim', [min(xMin) max(xMax)]);
                end
            end
        end
        
        
        function cleanDataMode = getCleanDataMode(this)
            cleanDataMode = [];
            
            if this.manualCleanDataMode.dirty % Manual CleanMode
                cleanDataMode = this.manualCleanDataMode;
            elseif this.algoCleanDataMode.dirty % Algo CleanMode
                cleanDataMode = this.algoCleanDataMode;
            elseif this.filterDataMode.dirty % FilterDataMode
                cleanDataMode = this.filterDataMode;
            elseif this.editDataMode.dirty % EditDataMode
                cleanDataMode = this.editDataMode;
            elseif this.drawDataMode.dirty % DrawDataMode
                cleanDataMode = this.drawDataMode;
            end
        end
        
        
        %% Callback
        
        function handleCleanEvent(this, ~, cleanDataEventData)
            
            modifiedLine = cleanDataEventData.line;
            modifiedIndex = cleanDataEventData.modifiedIndex;
            if this.editDataMode.dirty || this.drawDataMode.dirty %EditMode and DrawMode have no resetClean option
                resetClean = [];
            else
                resetClean = cleanDataEventData.reset;
            end
            
            if this.manualCleanDataMode.dirty || this.algoCleanDataMode.dirty
                % Search the indexes of modified line
                [modifiedSignalContainerIndex, modifiedSignalIndex, modifiedSampleIndex] = this.searchModifiedLine(modifiedLine);
                
                if (modifiedSignalContainerIndex ~= 0 ...
                        && modifiedSignalIndex ~= 0 && this.syncSignalList(modifiedSignalIndex) ~= 0 ...
                        && ~this.editDataMode.dirty && ~this.drawDataMode.dirty) % no update with editMode nor drawMode
                    % Update other signals sync
                    this.updateSignals(modifiedSignalContainerIndex, modifiedSignalIndex, modifiedSampleIndex,  modifiedIndex, resetClean);
                end
            end
        end
        
        
        function [modifiedSignalContainerIndex, modifiedSignalIndex, modifiedSampleIndex] = searchModifiedLine(this, modifiedLine)
            % Search modifiedSignalContainerIndex & modifiedSignalIndex & modifiedSampleIndex of modified line
            modifiedSignalContainerIndex = 0;
            modifiedSignalIndex          = 0;
            modifiedSampleIndex          = 0;
            
            for signalContainerIndex=1:numel(this.signalContainerList)
                for signalIndex = 1:numel(this.signalContainerList(signalContainerIndex).signalList)
                    for ySampleIndex=1:numel(this.signalContainerList(signalContainerIndex).signalList(signalIndex).ySample)
                        ySampleLine = this.signalLineArray(signalContainerIndex, signalIndex, ySampleIndex);
                        if isa(ySampleLine, SignalDialog.lineClassName) && isvalid(ySampleLine)
                            if eq(ySampleLine, modifiedLine)
                                modifiedSignalContainerIndex = signalContainerIndex;
                                modifiedSignalIndex = signalIndex;
                                modifiedSampleIndex = ySampleIndex;
                                break;
                            end
                        end
                    end
                end
            end
        end
        
        
        function updateSignals(this, modifiedSignalContainerIndex, modifiedSignalIndex, modifiedSampleIndex,  modifiedIndex, resetClean)
            
            cleanDataMode = this.getCleanDataMode();  % Manual or Algo CleanMode
            
            if ~isempty(cleanDataMode)
                % update other ysample if modifiedSignal sync or modifiedSignal = 0 (update all sync signals)
                if  modifiedSignalIndex == 0 || this.syncSignalList(modifiedSignalIndex) ~= 0
                    for signalIndex=1:this.signalMaxNb
                        if signalIndex ~= modifiedSignalIndex && this.syncSignalList(signalIndex) ~= 0 % update signal if not src signal and sync signal
                            
                            axes = this.signalAxesArray(signalIndex);
                            
                            % create tmpLines if needed
                            cleanDataMode.createTmpLineList(axes);
                            
                            [lineList, tmpLineList] = TmpLineUtils.getLines(axes);
                            % Find tmpLine
                            lineIndex = this.signalYSampleLineIndex(modifiedSignalContainerIndex, signalIndex, modifiedSampleIndex);
                            if lineIndex ~=0
                                line = lineList(lineIndex);
                                tmpLine = tmpLineList(lineIndex);
                                
                                cleanDataMode.cleanDataUnitaryAction(line, tmpLine, modifiedIndex, resetClean);
                            end
                        end
                    end
                end
            end
        end
        
        
        function showSignalContainerSelectionDialog(this, ~, ~)
            
            signalContainerNameList  = {};
            signalContainerValueList = [];
            
            % Construl list of name and list of displayed Signal Container
            for signalContainerIndex=1:numel(this.signalContainerList)
                if ~isempty(this.signalContainerList(signalContainerIndex).name)
                    signalContainerNameList{signalContainerIndex} = this.signalContainerList(signalContainerIndex).name; %#ok<AGROW>
                else
                    signalContainerNameList{signalContainerIndex} = 'Empty Name'; %#ok<AGROW>
                end
                
                if this.displayedSignalContainerList(signalContainerIndex)
                    signalContainerValueList(end+1) = signalContainerIndex; %#ok<AGROW>
                end
            end
            
            % Show list dialog
            [rep, validation] = my_listdlgMultiple(this.signalContainerSelectionLabel, signalContainerNameList, 'InitialValue', signalContainerValueList);
            
            % update displayedSignalContainerList
            if validation
                for signalContainerIndex=1:numel(this.signalContainerList)
                    [flag,~] = ismember(signalContainerIndex, rep);
                    if flag
                        this.displayedSignalContainerList(signalContainerIndex) = 1;
                    else
                        this.displayedSignalContainerList(signalContainerIndex) = 0;
                    end
                end
                
                % Show / Hide Signals
                this.showHideSignals();
                %Reset Xlim of axes
                this.resetXlimAxes();
            end
        end
        
        
        function showSignalSelectionDialog(this, ~, ~)
            
            signalNameList  = {};
            signalValueList = [];
            
            % Construl list of name and list of displayed Signal
            for signalIndex=1:this.signalMaxNb
                signalNameList{signalIndex} = this.signalContainerList(1).signalList(signalIndex).name; %#ok<AGROW>
                
                if this.displayedSignalList(signalIndex)
                    signalValueList(end+1) = signalIndex; %#ok<AGROW>
                end
            end
            
            % Show list dialog
            [rep, validation] = my_listdlgMultiple(this.signalSelectionLabel, signalNameList, 'InitialValue', signalValueList);
            
            % update displayedSignalList
            if validation
                for signalIndex=1:this.signalMaxNb
                    [flag,~] = ismember(signalIndex, rep);
                    if flag
                        this.displayedSignalList(signalIndex) = 1;
                    else
                        this.displayedSignalList(signalIndex) = 0;
                    end
                end
                
                % Show / Hide Signals
                this.showHideSignals();
                
                %Reset Xlim of axes
                this.resetXlimAxes();
            end
        end
        
        
        function showYSampleSelectionDialog(this, ~, ~)
            
            %Construct the ySampleNameList and list of displayed YSample
            ySampleNameList  = cell(1,this.ySampleMaxNb);
            ySampleValueList = [];
            for signalIndex=1:this.signalMaxNb
                for ySampleIndex = 1:numel(this.signalContainerList(1).signalList(signalIndex).ySample)
                    ySampleName = this.signalContainerList(1).signalList(signalIndex).ySample(ySampleIndex).name;
                    if isempty(ySampleNameList{ySampleIndex}) && ~isempty(ySampleName)
                        ySampleNameList{ySampleIndex} = ySampleName;
                    else
                        if ~(strcmp( ySampleNameList{ySampleIndex}, ySampleName)) % default name
                            ySampleNameList{ySampleIndex} = ['ySample' num2str(ySampleIndex)];
                        end
                    end
                end
            end
            
            % Construct list of displayed YSample
            for ySampleIndex=1:this.ySampleMaxNb
                if this.displayedSampleList(ySampleIndex)
                    ySampleValueList(end+1) = ySampleIndex; %#ok<AGROW>
                end
            end
            
            % Show list dialog
            [rep, validation] = my_listdlgMultiple(this.ySampleSelectionLabel, cellstr(ySampleNameList), 'InitialValue', ySampleValueList);
            
            % update displayedSampleList
            if validation
                for ySampleIndex=1:this.ySampleMaxNb
                    [flag,~] = ismember(ySampleIndex, rep);
                    if flag
                        this.displayedSampleList(ySampleIndex) = 1;
                    else
                        this.displayedSampleList(ySampleIndex) = 0;
                    end
                end
                
                % Show / Hide Signals
                this.showHideSignals();
            end
        end
        
        
        function shiftoXLimCallback(this, ~, ~, direction)
            % Shift Signals Callback
            switch direction
                case 'left'
                    isShiftToRight = false;
                case 'right'
                    isShiftToRight = true;
            end
            
            xLim = this.signalAxesArray(1).get('XLim');
            
            difference = xLim(2)-xLim(1);
            
            %compute min max  xSample of displayed Signals/ySamples
            xSampleMin = [];
            xSampleMax = [];
            for containerIndex = 1:numel(this.signalContainerList)
                if this.displayedSignalContainerList(containerIndex) == 1
                    for signalIndex=1:numel(this.signalContainerList(containerIndex).signalList)
                        if this.displayedSignalList(signalIndex) == 1
                            for sampleIndex=1:numel(this.signalContainerList(containerIndex).signalList(signalIndex).xSample)
                                if this.displayedSampleList(sampleIndex) == 1
                                    data = this.signalContainerList(containerIndex).signalList(signalIndex).xSample(sampleIndex).data;
                                    if  isempty(xSampleMin) ||  min(data) < xSampleMin
                                        xSampleMin = min(data);
                                    end
                                    if isempty(xSampleMax) || max(data) > xSampleMax
                                        xSampleMax = max(data);
                                    end
                                end
                            end
                        end
                    end
                end
            end
            
            if isShiftToRight % shift to right
                this.signalAxesArray(1).set('XLim', [xSampleMax-difference xSampleMax]);
            else % shift to left
                
                this.signalAxesArray(1).set('XLim', [xSampleMin xSampleMin+difference]);
            end
        end
        
        
        function shiftSignalsCallback(this, ~, ~, direction)
            % Shift Signals Callback
            switch direction
                case 'left'
                    isShiftToRight = false;
                case 'right'
                    isShiftToRight = true;
            end
            
            xLim = this.signalAxesArray(1).get('XLim');
            
            difference = xLim(2)-xLim(1);
            
            %compute min max  xSample of displayed Signals/ySamples
            xSampleMin = [];
            xSampleMax = [];
            for containerIndex = 1:numel(this.signalContainerList)
                if this.displayedSignalContainerList(containerIndex) == 1
                    for signalIndex=1:numel(this.signalContainerList(containerIndex).signalList)
                        if this.displayedSignalList(signalIndex) == 1
                            for sampleIndex=1:numel(this.signalContainerList(containerIndex).signalList(signalIndex).xSample)
                                if this.displayedSampleList(sampleIndex) == 1
                                    data = this.signalContainerList(containerIndex).signalList(signalIndex).xSample(sampleIndex).data;
                                    if  isempty(xSampleMin) ||  min(data) < xSampleMin
                                        xSampleMin = min(data);
                                    end
                                    if isempty(xSampleMax) || max(data) > xSampleMax
                                        xSampleMax = max(data);
                                    end
                                end
                            end
                        end
                    end
                end
            end
            
            %compute shift value
            shiffValue = (9 / 10) * difference;
            
            % Shift action
            if (xSampleMin <= xLim(1)) && (xSampleMax >= xLim(2)) % view between min/max
                if isShiftToRight % shift to right
                    if xSampleMax > (xLim(2) + shiffValue) % normal shift
                        this.signalAxesArray(1).set('XLim', [xLim(1)+shiffValue xLim(2)+shiffValue]);
                    else % shift to limit
                        this.signalAxesArray(1).set('XLim', [xSampleMax-difference xSampleMax]);
                    end
                else % shift to left
                    if xSampleMin < (xLim(1) - shiffValue)  % normal shift
                        this.signalAxesArray(1).set('XLim', [xLim(1)-shiffValue xLim(2)-shiffValue]);
                    else % shift to limit
                        this.signalAxesArray(1).set('XLim', [xSampleMin xSampleMin+difference]);
                    end
                end
                
            elseif (xSampleMin >= xLim(1)) && (xSampleMax <= xLim(2))  % view outside min/max
                if isShiftToRight % shift to right
                    if xSampleMin > (xLim(1)+shiffValue) % normal shift
                        this.signalAxesArray(1).set('XLim', [xLim(1)+shiffValue xLim(2)+shiffValue]);
                    else % shift to limit
                        this.signalAxesArray(1).set('XLim', [xSampleMin xSampleMin+difference]);
                    end
                else % shift to left
                    if xSampleMax < (xLim(2) - shiffValue) % normal shift
                        this.signalAxesArray(1).set('XLim', [xLim(1)-shiffValue xLim(2)-shiffValue]);
                    else % shift to limit
                        this.signalAxesArray(1).set('XLim', [xSampleMax-difference xSampleMax]);
                    end
                end
            elseif (xSampleMin >= xLim(1)) && (xSampleMax >= xLim(2))% view on left
                if isShiftToRight % shift to right
                    if (xSampleMax > xLim(2)+shiffValue) % normal shift
                        this.signalAxesArray(1).set('XLim', [xLim(1)+shiffValue xLim(2)+shiffValue]);
                    else % shift to limit
                        this.signalAxesArray(1).set('XLim', [xSampleMax-difference xSampleMax]);
                    end
                end
            elseif (xSampleMin <= xLim(1)) && (xSampleMax <= xLim(2))% view on right
                if ~isShiftToRight % shift to left
                    if xSampleMin < (xLim(1)-shiffValue)  % normal shift
                        this.signalAxesArray(1).set('XLim', [xLim(1)-shiffValue xLim(2)-shiffValue]);
                    else % shift to limit
                        this.signalAxesArray(1).set('XLim', [xSampleMin xSampleMin+difference]);
                    end
                end
            end
        end
        
        
        function zoomSignalsCallback(this, ~, ~, direction)
            % Shift Signals Callback
            switch direction
                case 'in'
                    isZoomin = true;
                case 'out'
                    isZoomin = false;
            end
            
            xLim = this.signalAxesArray(1).get('XLim');
            difference = xLim(2)-xLim(1);
            % compute offset value
            offsetValue = 1/4*difference;
            
            if isZoomin
                this.signalAxesArray(1).set('XLim', [xLim(1)+offsetValue  xLim(2)-offsetValue]);
            else
                this.signalAxesArray(1).set('XLim', [xLim(1)-offsetValue  xLim(2)+offsetValue]);
            end
        end
        
        
        function autoCenterZoomCallback(this, ~, ~)
            % Number of visible points on axes after auto zoom
            visiblePointNb = 100;
            
            xLim = this.signalAxesArray(1).get('XLim');
            % compute center X
            difference = xLim(2)-xLim(1);
            centerX = xLim(1) + difference/2;
            
            % Find the sample nearest of centerX
            isCenterXInXData = false;
            minAbsGap = Inf;
            xDataFound = [];
            for signalContainerIndex=1:numel(this.signalContainerList)
                signalIndex = 1;
                for ySampleIndex=1:numel(this.signalContainerList(signalContainerIndex).signalList(signalIndex).ySample)
                    xData =  this.signalContainerList(signalContainerIndex).signalList(signalIndex).xSample(ySampleIndex).data;
                    if (centerX > min(xData) && centerX < max(xData))
                        isCenterXInXData = true;
                        xDataFound = xData;
                        break;
                    else
                        % compute the absolute gap between centerX and xData
                        [absGap, minIndex] = min(abs(centerX - xData));
                        
                        if (absGap < minAbsGap)
                            xDataFound = xData;
                            minAbsGap = absGap;
                            relGap =  centerX - xDataFound(minIndex);
                        end
                    end
                end
                if isCenterXInXData
                    break;
                end
            end
            
            if ~isempty(xDataFound)
                
                % If centerX is not on xData, re-set center X
                if ~isCenterXInXData
                    centerX = centerX - relGap;
                end
                
                % Init the new XLim
                newXLim = [centerX centerX];
                
                % Execute the loop 'visiblePointNb' times
                for pointCount=1:visiblePointNb
                    
                    % Compute the nearest Dist and nearest Index of a point
                    % in the min side
                    minIndex = xDataFound < newXLim(1);
                    
                    if all(~minIndex)
                        continue
                    end
                    
                    xDataFoundMin = xDataFound(minIndex);
                    [nearestDistMin, nearestIndexMin] = min(abs(centerX - xDataFoundMin));
                    
                    % Compute the nearest Dist and nearest Index of a point
                    % in the max side
                    maxIndex = xDataFound > newXLim(2);
                    xDataFoundMax = xDataFound(maxIndex);
                    [nearestDistMax, nearestIndexMax] = min(abs(centerX - xDataFoundMax));
                    
                    % Compare and reset the new XLim
                    if isempty(nearestDistMax) || (nearestDistMin <= nearestDistMax)
                        newXLim(1) = xDataFoundMin(nearestIndexMin);
                    elseif isempty(nearestIndexMin) || nearestDistMin >= nearestDistMax
                        newXLim(2) = xDataFoundMax(nearestIndexMax);
                    end
                end
                
                % TODO MHO : ajout JMA le 25/11/2020
                if diff(newXLim) == 0
                    newXLim(2) = newXLim(2) + duration(0,1,0);
                end
                
                % Re-set new XLim in axes
                this.signalAxesArray(1).set('XLim', newXLim);
            end
        end
        
        
        function xLimModeAutoSignalsCallback(this, src, ~)
            if src.Value == 1
                mode = 'auto';
            else
                mode = 'manual';
            end
            
            % XLimMode auto
            for k=1:numel(this.signalAxesArray)
                % set axes to current and redraw
                axes(this.signalAxesArray(k)); %#ok<LAXES>
                this.signalAxesArray(k).set('XLimMode', mode);
            end
        end
        
        
        function yLimModeAutoSignalsCallback(this, src, ~)
            if src.Value == 1
                mode = 'auto';
            else
                mode = 'manual';
            end
            
            % YLimMode auto
            for k=1:numel(this.signalAxesArray)
                % set axes to current and redraw
                axes(this.signalAxesArray(k)); %#ok<LAXES>
                this.signalAxesArray(k).set('YLimMode', mode);
            end
        end
        
        
        function syncXYLimAutoCallback(this, ~, ~, signalIndexSrc)
            if this.xLimModeToggleButton.Value == 1
                this.signalAxesArray(signalIndexSrc).set('XLimMode', 'auto');
            end
            if this.yLimModeToggleButton.Value == 1
                this.signalAxesArray(signalIndexSrc).set('YLimMode', 'auto');
            end
        end
        
        
        function syncContextMenuItemCallback(this, src, ~)
            % syncContextMenuItem Callback
            
            [~,index] = ismember(src, this.syncContextMenuItemList);
            switch index
                case numel(this.syncContextMenuItemList)-1 % if select all menu selection
                    for signalIndex=1:this.signalMaxNb
                        this.syncSignalList(signalIndex) = 1;
                        this.syncContextMenuItemList(signalIndex).set('Checked', 'on');
                    end
                    
                case numel(this.syncContextMenuItemList) % if unselect all menu selection
                    for signalIndex=1:this.signalMaxNb
                        this.syncSignalList(signalIndex) = 0;
                        this.syncContextMenuItemList(signalIndex).set('Checked', 'off');
                    end
                    
                otherwise % signal selection
                    if this.syncSignalList(index)
                        this.syncSignalList(index) = 0;
                        src.set('Checked', 'off');
                    else
                        this.syncSignalList(index) = 1;
                        src.set('Checked', 'on');
                    end
            end
        end
        
        
        function handlePerformModifyEvent(this, ~, ~)
            % Perform Clean data event
            % Savethe  modification in navigation model
            this.saveChangePlotInSignal();
        end
    end
end
