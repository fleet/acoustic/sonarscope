classdef SimpleParametreDialog < handle & SimpleTitleDialog
    % Description
    %   Class SimpleParametreDialog wich is used to display a set of
    %   ClParametre instances in a figure and waits for Ok or Cancel
    %   buttons
    %
    % Syntax
    %   a = SimpleParametreDialog(...)
    %
    % ParametreDialog properties :
    %   params             - ClParametre array
    %   sizes              - array of 8 size subComponents
    %   paramComponentList - SimpleParametreComponent array
    %
    % Output Arguments
    %   s : One SimpleParametreDialog instance
    %
    % Examples
    %   p(1) = ClParametre('Name', 'Temperature', 'Unit', 'deg',  'MinValue', -10, 'MaxValue', 50, 'Value', 20);
    %   p(2) = ClParametre('Name', 'Salinity',    'Unit', '0/00', 'MinValue', 25,  'MaxValue', 35, 'Value', 30);
    %   a = SimpleParametreDialog('params', p, 'Title', 'Demo of ParametreDialog');
    %   a.openDialog;
    %   flag = a.okPressedOut;
    %   if ~flag
    %       return
    %   end
    %   paramsValue = a.getParamsValue;
    %
    %   style1 = ColorAndFontStyle('BackgroundColor', [0.7 0.7 1], 'ForegroundColor', 'k', 'FontWeight', 'bold', 'FontSize', 13, 'FontAngle', 'italic');
    %   style2 = ColorAndFontStyle('BackgroundColor', [1 0.8 0.6], 'ForegroundColor', 'k', 'FontWeight', 'bold');
    %   a.titleStyle = style1;
    %   a.paramStyle = style2;
    %   a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
    %   a.sizes = [-1 -1 -1 0];
    %   a.openDialog;
    %   paramsValue = a.getParamsValue;
    %
    % Authors : MHO
    % See also StyledSimpleParametreDialog ParametreDialog ClParametre ParametreComponent Authors
    % ----------------------------------------------------------------------------
    
    properties
        params = ClParametre.empty;     % - ClParametre array
        sizes = [-3 -1 -1 25]           % - array of 4 size subComponents
        paramStyle = ColorAndFontStyle; % - Style of uicontrol text of Parametre Components
    end
    
    properties (Access = private)
        paramComponentList = SimpleParametreComponent.empty; % - ParametreComponent array
    end
    
    events
        OneParamValueChange % event if one param value change
    end
    
    methods
        function this = SimpleParametreDialog(varargin)
            % Superclass Constructor
            this = this@SimpleTitleDialog(varargin{:});
            
            % Read input args
            [varargin, this.params]     = getPropertyValue(varargin, 'params',     []);
            [varargin, this.sizes]      = getPropertyValue(varargin, 'sizes',      this.sizes);
            [varargin, this.paramStyle] = getPropertyValue(varargin, 'paramStyle', this.paramStyle); %#ok<ASGLU>
        end
        
        function handleValueChange(this, ~, ~)
            % Send one value change event
            this.notify('OneParamValueChange');
        end
        
        function paramsValue = getParamsValue(this)
            % Get the "Value" properties of the parametres in an array
            paramsValue = [];
            for k=length(this.params):-1:1
                paramsValue(k) = this.params(k).Value;
            end
        end
        
    end
    
    methods (Access = protected)
        function [widthBody, heightBody] = createBody(this, parent)
            % Create ParametreComponent List
            
            verticalBox = uiextras.VBox('Parent', parent);
            lengthComponents = length(this.params);
            for paramIdx=1:lengthComponents
                % Create ParametreComponent component
                this.paramComponentList(paramIdx) = SimpleParametreComponent(verticalBox, 'param', this.params(paramIdx), 'sizes', this.sizes, 'style', this.paramStyle);
                % Add a listener for Value change event
                addlistener(this.paramComponentList(paramIdx), 'ValueChange', @this.handleValueChange);
            end
            
            widthBody  = 400;
            heightBody = 25 * lengthComponents;
        end
    end
end
