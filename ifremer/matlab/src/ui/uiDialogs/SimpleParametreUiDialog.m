classdef SimpleParametreUiDialog < handle & SimpleTitleUiDialog
    % Description
    %   Class SimpleParametreUiDialog wich is used to display a set of
    %   ClParametre instances in a figure and waits for Ok or Cancel
    %   buttons
    %
    % Syntax
    %   a = SimpleParametreUiDialog(...)
    %
    % ParametreDialog properties :
    %   params             - ClParametre array
    %   sizes              - array of 8 size subComponents
    %   paramComponentList - ParametreComponent array
    %
    % Output Arguments
    %   s : One ParametreDialog instance
    %
    % Examples
    %   p(1) = ClParametre('Name', 'Temperature', 'Unit', 'deg',  'MinValue', -10, 'MaxValue', 50, 'Value', 20);
    %   p(2) = ClParametre('Name', 'Salinity',    'Unit', '0/00', 'MinValue', 25,  'MaxValue', 35, 'Value', 30);
    %   a = SimpleParametreUiDialog('params', p, 'Title', 'Demo of ParametreDialog');
    %   a.openDialog;
    %   % output :
    %     a.okPressedOut %(0 if not Ok pressed, 1 if Ok pressed)
    %     paramsValue = a.getParamsValue()
    %
    %   style1 = ColorAndFontStyle('BackgroundColor', [0.7 0.7 1], 'ForegroundColor', 'k', 'FontWeight', 'bold', 'FontSize', 13, 'FontAngle', 'italic');
    %   style2 = ColorAndFontStyle('BackgroundColor', [1 0.8 0.6], 'ForegroundColor', 'k', 'FontWeight', 'bold');
    %   a.titleStyle = style1;
    %   a.paramStyle = style2;
    %   a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
    %   a.sizes = {'1x', '1x', '1x', 0};
    %   a.openDialog;
    %   paramsValue = a.getParamsValue;
    %
    % Authors : MHO
    % See also StyledParametreDialog ClParametre ParametreComponent Authors
    % ----------------------------------------------------------------------------
    
    properties
        params             = ClParametre.empty         % - ClParametre array
        sizes = {'3x', '1x', '1x', 25}; % - Array of 4 subComponents widths
        paramStyle         = ColorAndFontStyle;        % - Style of uicontrol text of Parametre Components
        paramComponentList = SimpleParametreUiComponent.empty; % - ParametreComponent array
    end
    
    properties (Access = private)
        %         paramComponentList = SimpleParametreUiComponent.empty; % - ParametreComponent array
    end
    
    events
        OneParamValueChange % Event if one param value change
    end
    
    methods
        function this = SimpleParametreUiDialog(varargin)
            % Superclass Constructor
            this = this@SimpleTitleUiDialog(varargin{:});
            % Read input args
            [varargin, this.params]     = getPropertyValue(varargin, 'params',     []);
            [varargin, this.sizes]      = getPropertyValue(varargin, 'sizes',      this.sizes);
            [varargin, this.paramStyle] = getPropertyValue(varargin, 'paramStyle', this.paramStyle); %#ok<ASGLU>
        end
        
        function handleValueChange(this, ~, ~)
            % Send one value change event
            this.notify('OneParamValueChange');
        end
        
        function paramsValue = getParamsValue(this)
            % Get the "Value" properties of the parametres in an array
            paramsValue = [];
            for k=length(this.params):-1:1
                paramsValue(k) = this.params(k).Value;
            end
        end
        
        function this = set.sizes(this, sz) %#ok<MCHV2>
            % Setter of "sizes" property
            
            if ~isempty(sz)
                if length(sz) == 4
                    if sz{2} == 0
                        str2 = 'Size(2) cannot be equal to 0, this would say that you do not want to see the value of the parametre.';
                        my_warndlg(str2, 1);
                    else
                        this.sizes = sz;
                    end
                else
                    str2 = 'The parametre "sizes" sent to "ParametreComponent" must be an array of size 4.';
                    my_warndlg(str2, 1);
                end
            end
        end
    end
    
    methods (Access = protected)
        function [widthBody ,heightBody] = createBody(this, parent)
            % Create ParametreComponent List
            
            gridlayout = uigridlayout(parent, 'ColumnSpacing', 1, 'RowSpacing', 1, 'Padding', 1);
            gridlayout.ColumnWidth = {'1x'};
            
            lengthComponents = length(this.params);
            for paramIdx=1:lengthComponents
                % Create ParametreComponent component
                this.paramComponentList(paramIdx) = SimpleParametreUiComponent(gridlayout, 'param', this.params(paramIdx), 'sizes', this.sizes, 'style', this.paramStyle);
                % Add a listener for Value change event
                addlistener(this.paramComponentList(paramIdx), 'ValueChange', @this.handleValueChange);
                gridlayout.RowHeight{paramIdx} = 25; % Height of one row
            end
            
            widthBody  = 600;
            heightBody = 50 * lengthComponents;
        end
    end
end
