classdef (Abstract) SimpleTitleDialog < handle & AbstractDisplay
    % Description
    %   Class SimpleTitleDialog wich is used to display the title of
    %   windows displaying components such as ParametreDialog
    %
    % Syntax
    %   a = SimpleTitleDialog(...)
    %
    % SimpleTitleDialog properties :
    %   title        - Title of dialog
    %   Help         - Path or URL giving explanation for the dialog
    %   titleStyle   - Style of title of dialog
    %   okPressedOut - 0 if cancel pressed, 1 if Ok pressed
    %   windowStyle  - model/normal
    %   waitAnswer   - (default = true) if true,  wait for dialog to close before running to completion
    %   hideActionButtons - if true, hide OK / Cancel Buttons
    %
    % Examples
    %   This class is an absctract one, one cannot illustrate it
    %   separetelly from other xxxDialog classes which inheritate it.
    %
    % Authors : MHO
    % See also ParametreDialog GUIParametre ClModel Authors
    % ----------------------------------------------------------------------------
    
    properties
        titleStyle   = ColorAndFontStyle.getTitleColorAndFontStyle; % - Style of title of dialog box
        Title                      % - Title of dialog box
        Help         = ''          % - Path or URL giving explanation for the dialog box
        okPressedOut = []          % - 0 if Cancel pressed, 1 if Ok pressed
        windowStyle  = 'modal'     % - {modal} / normal
        waitAnswer   = true        % - Wait for dialog box to close before running to completion
        resizable    = 'off'       % - on / {off} if the dialog is resizable or not
        figureVarargin
        hideActionButtons = false; % - If true, hide OK / Cancel Buttons
    end
    
    properties (SetAccess = protected)
        parentFigure % -The parent figure of this dialog box
    end
    
    events
        OkPressedEvent % event if Ok pressed
        CancelPressedEvent % event if Cancel pressed
    end
    
    methods
        function this = SimpleTitleDialog(varargin)
            % Constructor of the class
            
            % Lecture des arguments
            [varargin, this.Title]              = getPropertyValue(varargin, 'Title',             '');
            [varargin, this.Help]               = getPropertyValue(varargin, 'Help', 	          this.Help);
            [varargin, this.titleStyle]         = getPropertyValue(varargin, 'titleStyle',        this.titleStyle);
            [varargin, this.windowStyle]        = getPropertyValue(varargin, 'WindowStyle',       this.windowStyle);
            [varargin, this.waitAnswer]         = getPropertyValue(varargin, 'waitAnswer',        this.waitAnswer);
            [varargin, this.resizable]          = getPropertyValue(varargin, 'resizable',         this.resizable);
            [varargin, this.hideActionButtons]  = getPropertyValue(varargin, 'hideActionButtons', this.hideActionButtons); %#ok<ASGLU>
            this.okPressedOut      = [];
        end
        
        function openDialog(this)
            % Open the dialog figure
            
            % init output
            this.okPressedOut = [];
            
            % Create a title array for each line of title string
            titles = strsplit(this.Title, newline);
            
            % Creates parent figure
            this.parentFigure = FigUtils.createSScDialog('SuffixName', class(this), 'Visible', 'off', 'WindowStyle', this.windowStyle, 'CloseRequestFcn', @this.cancel);
            
            % Configure figure if needed
            this.configureFigure();
            
            % Creates main vertical box
            verticalMainBox = uiextras.VBox('Parent', this.parentFigure, 'Spacing', 0);
            
            % Creates titles text
            this.createTitles(verticalMainBox, titles);
            
            % Creates  vertical sub-box
            % parametreComponentPanel = uipanel('Parent', verticalMainBox');
            verticalSubBox = uiextras.VBox('Parent', verticalMainBox, 'Spacing', 0);
            
            % Create the body of dialog
            [widthBody, heightBody] = this.createBody(verticalSubBox);
            
            % creates action horizontal box for save/quit buttons
            this.createActionButtons(verticalSubBox)
            
            % set size title & body & actionButtons
            this.setSizesDialog(verticalMainBox, verticalSubBox, widthBody, heightBody, length(titles));
            
            % set resize on / off
            this.parentFigure.set('Resize', this.resizable);
            
            % set figure visible a the end of creation
            this.parentFigure.set('Visible', 'on');
            
            % Configure figure after creation if needed
            this.configureFigureAfterCreation();
            
            % if waitAnswer,  wait for dialog to close before running to completion
            % (avoid uiwait because imrect, impoly etc.. breaks uiwait)
            if this.waitAnswer
                waitfor(this.parentFigure);
            end
        end
        
        function okPressed(this, ~, ~)
            % Close the dialog with Ok button
            this.okPressedOut = 1;
            delete(this.parentFigure);
            
            % Send Ok pressed event
            this.notify('OkPressedEvent');
        end
        
        function cancel(this, ~, ~)
            % Close the dialog
            this.okPressedOut = 0;
            delete(this.parentFigure);
            
            % Send Cancel pressed event
            this.notify('CancelPressedEvent');
        end
    end
    
    methods (Access = protected)
        
        function configureFigure(this) %#ok<MANU>
            % Nothing to do here but can be overriden by children classes
        end
        
        function configureFigureAfterCreation(this) %#ok<MANU>
            % Nothing to do here but can be overriden by children classes
        end
        
        function createTitles(this, verticalMainBox, titles)
            titlePanel = uipanel('Parent', verticalMainBox');
            titleBox = uiextras.VBox('Parent', titlePanel);
            % Uicontrols for each line of title
            for titleIdx=1:length(titles)
                % creates help button if exist
                if (titleIdx == 1) &&  ~isempty(this.Help)
                    % create horizontalBox for title + help button
                    hBoxTitleHelp = uiextras.HBox( 'Parent', titleBox);
                    this.createTitle(hBoxTitleHelp, titles{titleIdx});
                    UiUtils.createHelpButton(hBoxTitleHelp, this.Help);
                    hBoxTitleHelp.set('Sizes', [-1 25]);
                else
                    this.createTitle(titleBox, titles{titleIdx});
                end
            end
        end
        
        function  createTitle(this, parent, title)
            % Create TitleText
            uicontrol('Style', 'text', 'Parent', parent, ...
                'string',          title, ...
                'BackgroundColor', this.titleStyle.BackgroundColor, ...
                'ForegroundColor', this.titleStyle.ForegroundColor, ...
                'FontName',        this.titleStyle.FontName, ...
                'FontSize',        this.titleStyle.FontSize, ...
                'FontUnits',       this.titleStyle.FontUnits, ...
                'FontWeight',      this.titleStyle.FontWeight, ...
                'FontAngle',       this.titleStyle.FontAngle);
        end
        
        function  createHelpButton(this, parent)
            % Create Help Button
            icon = iconizeFic(getNomFicDatabase('Apps-Help-Info-icon.png'));
            uicontrol('Parent', parent, ...
                'Style',   'pushbutton', ...
                'CData',    icon, ...
                'Callback', @this.helpAction);
        end
        
        function setSizesDialog(this, verticalMainBox, verticalSubBox, widthBody, heightBody, lengthTitles)
            % Compute and set sizes of dialog
            
            lineHeight = 30;
            padding = 5;
            spacing = 5;
            
            verticalMainBox.set('Sizes', [lineHeight*lengthTitles -1]);
            
            % set Padding & Spacing 1px
            verticalSubBox.set('Padding', padding);
            verticalSubBox.set('Spacing',spacing);
            
            % Compute height of window
            H = lineHeight*lengthTitles + heightBody + padding*2 + spacing*2;
            
            % hide OK/ Cancel buttons if needed
            if this.hideActionButtons
                verticalSubBox.set('Sizes', [-1 0]);
            else
                verticalSubBox.set('Sizes', [-1 lineHeight]);
                % add 1 lineHeight to size of window
                H = H + lineHeight;
            end
            
            % set size of figure window
            centrageFig(widthBody, H, 'Fig', this.parentFigure);
        end
        
        function createActionButtons(this, verticalBox)
            % Create action horizontal box for save/quit buttons
            actionBox = uiextras.HBox('Parent', verticalBox);
            
            uiextras.Empty('Parent', actionBox);
            
            % Save button
            uicontrol('parent', actionBox, ...
                'style',    'pushbutton', ...
                'string',   'OK', ...
                'callback', @this.okPressed, ...
                'tag',      'saveButton');
            
            % Quit button
            uicontrol('parent', actionBox, ...
                'style',    'pushbutton', ...
                'string',   'Cancel', ...
                'Callback', @this.cancel, ...
                'tag',      'quitButton');
            
            % set Padding & Spacing 1px
            actionBox.set('Sizes', [-1 100 100]);
            actionBox.set('Padding', 1);
            actionBox.set('Spacing',5);
        end
    end
    
    methods (Access = protected, Abstract)
        createBody(this);
        % Create the body of dialog
    end
end
