classdef (Abstract) SimpleTitleUiDialog < handle & AbstractDisplay
    % Description
    %   Class SimpleTitleDialog wich is used to display the title of
    %   windows displaying components such as ParametreDialog
    %
    % Syntax
    %   a = SimpleTitleDialog(...)
    %
    % SimpleTitleDialog properties :
    %   title        - Title of dialog
    %   Help         - Path or URL giving explanation for the dialog
    %   titleStyle   - Style of title of dialog
    %   okPressedOut - 0 if cancel pressed, 1 if Ok pressed
    %   windowStyle  - model/normal
    %   waitAnswer   - (default = true) if true,  wait for dialog to close before running to completion
    %   hideActionButtons - if true, hide OK / Cancel Buttons
    %
    % Examples
    %   This class is an absctract one, one cannot illustrate it
    %   separetelly from other xxxDialog classes which inheritate it.
    %
    % Authors : MHO
    % See also ParametreDialog GUIParametre ClModel Authors
    % ----------------------------------------------------------------------------
    
    properties
        titleStyle   = ColorAndFontStyle.getUiTitleColorAndFontStyle; % - Style of title of dialog box
        Title                      % - Title of dialog box
        Help         = ''          % - Path or URL giving explanation for the dialog box
        okPressedOut = []          % - 0 if Cancel pressed, 1 if Ok pressed
        %         windowStyle  = 'modal'     % - {modal} / normal
        waitAnswer   = true        % - Wait for dialog box to close before running to completion
        resizable    = 'off'       % - on / {off} if the dialog is resizable or not
        figureVarargin
        hideActionButtons = false; % - If true, hide OK / Cancel Buttons
    end
    
    properties (SetAccess = protected)
        parentFigure % -The parent figure of this dialog box
    end
    
    events
        OkPressedEvent % event if Ok pressed
        CancelPressedEvent % event if Cancel pressed
    end
    
    methods
        function this = SimpleTitleUiDialog(varargin)
            % Constructor of the class
            
            % Lecture des arguments
            [varargin, this.Title]              = getPropertyValue(varargin, 'Title',             '');
            [varargin, this.Help]               = getPropertyValue(varargin, 'Help', 	          this.Help);
            [varargin, this.titleStyle]         = getPropertyValue(varargin, 'titleStyle',        this.titleStyle);
            % not implemented in uifigure
            %             [varargin, this.windowStyle]        = getPropertyValue(varargin, 'WindowStyle',       this.windowStyle);
            [varargin, this.waitAnswer]         = getPropertyValue(varargin, 'waitAnswer',        this.waitAnswer);
            [varargin, this.resizable]          = getPropertyValue(varargin, 'resizable',         this.resizable);
            [varargin, this.hideActionButtons]  = getPropertyValue(varargin, 'hideActionButtons', this.hideActionButtons); %#ok<ASGLU>
            this.okPressedOut      = [];
        end
        
        function openDialog(this)
            % Open the dialog figure
            
            % init output
            this.okPressedOut = [];
            
            % Create a title array for each line of title string
            titles = strsplit(this.Title, newline);
            
            % Creates parent figure
            this.parentFigure = FigUtils.createSScUiDialog('SuffixName', class(this), 'Visible', 'off', 'CloseRequestFcn', @this.cancel);
            %               this.parentFigure = FigUtils.createSScDialog('SuffixName', class(this), 'Visible', 'off', 'WindowStyle', this.windowStyle, 'CloseRequestFcn', @this.cancel);
            
            % Configure figure if needed
            this.configureFigure();
            
            % Creates main vertical box
            %             verticalMainBox = uiextras.VBox('Parent', this.parentFigure, 'Spacing', 0);
            mainGridlayout = uigridlayout(this.parentFigure, 'ColumnSpacing', 1, 'RowSpacing', 1, 'Padding', 1);
            mainGridlayout.ColumnWidth = {'1x'};
            mainGridlayout.RowSpacing = 0;
            
            % Creates titles text
            this.createTitles(mainGridlayout, titles);
            
            % Creates  vertical sub-box
            % parametreComponentPanel = uipanel('Parent', verticalMainBox');
            %              subGridlayout = uiextras.VBox('Parent', mainGridlayout, 'Spacing', 0);
            subGridlayout = uigridlayout(mainGridlayout, 'ColumnSpacing', 1, 'RowSpacing', 1, 'Padding', 1);
            subGridlayout.ColumnWidth = {'1x'};
            subGridlayout.RowSpacing = 0;
            
            % Create the body of dialog
            
            [widthBody, heightBody] = this.createBody(subGridlayout);
            
            % creates action horizontal box for save/quit buttons
            this.createActionButtons(subGridlayout)
            
            % set size title & body & actionButtons
            this.setSizesDialog(mainGridlayout, subGridlayout, widthBody, heightBody, length(titles));
            
            % set resize on / off
            this.parentFigure.set('Resize', this.resizable);
            
            % set figure visible a the end of creation
            this.parentFigure.set('Visible', 'on');
            
            % Configure figure after creation if needed
            this.configureFigureAfterCreation();
            
            % if waitAnswer,  wait for dialog to close before running to completion
            % (avoid uiwait because imrect, impoly etc.. breaks uiwait)
            if this.waitAnswer
                waitfor(this.parentFigure);
            end
        end
        
        function okPressed(this, ~, ~)
            % Close the dialog with Ok button
            this.okPressedOut = 1;
            delete(this.parentFigure);
            
            % Send Ok pressed event
            this.notify('OkPressedEvent');
        end
        
        function cancel(this, ~, ~)
            % Close the dialog
            this.okPressedOut = 0;
            delete(this.parentFigure);
            
            % Send Cancel pressed event
            this.notify('CancelPressedEvent');
        end
    end
    
    methods (Access = protected)
        
        function configureFigure(this) %#ok<MANU>
            % Nothing to do here but can be overriden by children classes
        end
        
        function configureFigureAfterCreation(this) %#ok<MANU>
            % Nothing to do here but can be overriden by children classes
        end
        
        function createTitles(this, mainGridlayout, titles)
            titlePanel = uipanel('Parent', mainGridlayout');
            %             titleBox = uiextras.VBox('Parent', titlePanel);
            titlelayout = uigridlayout(titlePanel, 'ColumnSpacing', 1, 'RowSpacing', 1, 'Padding', 1);
            titlelayout.ColumnWidth = {'1x'};
            titlelayout.RowHeight = {'fit'};
            
            
            % Uicontrols for each line of title
            for titleIdx=1:length(titles)
                % creates help button if exist
                if (titleIdx == 1) &&  ~isempty(this.Help)
                    % create horizontalBox for title + help button
                    titleHelpLayout = uigridlayout(titlelayout, 'ColumnSpacing', 0, 'RowSpacing', 0, 'Padding', 0);
                    titleHelpLayout.RowHeight = {'fit'};
                    this.createTitle(titleHelpLayout, titles{titleIdx});
                    
                    UiUtils.createHelpUiButton(titleHelpLayout, this.Help);
                    titleHelpLayout.ColumnWidth = {'1x', 25};
                else
                    this.createTitle(titlelayout, titles{titleIdx});
                end
            end
        end
        
        function  createTitle(this, parent, title)
            % Create TitleText
            
            % Name  Param
            titleLabel = uilabel(parent, ...
                'Text',  title,...
                'HorizontalAlignment', 'center');
            
            % add style to text
            ColorAndFontStyle.addUiStyle(titleLabel, this.titleStyle);
        end
        
        function setSizesDialog(this, mainGridlayout, subGridlayout, widthBody, heightBody, lengthTitles)
            % Compute and set sizes of dialog
            
            lineHeight = 30;
            padding = 5;
            spacing = 5;
            
            %             verticalMainBox.set('Sizes', [lineHeight*lengthTitles -1]);
            mainGridlayout.RowHeight = {lineHeight*lengthTitles, '1x'};
            mainGridlayout.ColumnWidth = {'1x'};
            
            % set Padding & Spacing 1px
            subGridlayout.Padding = padding;
            subGridlayout.RowSpacing = spacing;
            
            % Compute height of window
            H = lineHeight*lengthTitles + heightBody + padding*2 + spacing*2;
            
            % hide OK/ Cancel buttons if needed
            if this.hideActionButtons
                %                 verticalSubBox.set('Sizes', [-1 0]);
                subGridlayout.RowHeight = {'1x', 0};
            else
                %                 verticalSubBox.set('Sizes', [-1 lineHeight]);
                subGridlayout.RowHeight = {'1x', lineHeight};
                % add 1 lineHeight to size of window
                H = H + lineHeight;
            end
            subGridlayout.ColumnWidth = {'1x'};
            
            % set size of figure window
            %             centrageFig(widthBody, H, 'Fig', this.parentFigure);
            FigUtils.centrageUiFig(widthBody, H, 'Fig', this.parentFigure);
        end
        
        function createActionButtons(this, verticalGridlayout)
            % Create action horizontal box for save/quit buttons
            actionGridLayout = uigridlayout(verticalGridlayout, 'ColumnSpacing', 5, 'RowSpacing', 5, 'Padding', 1);
            actionGridLayout.ColumnWidth = {'1x', 100, 100};
            actionGridLayout.RowHeight = {'1x'};
            
            uilabel(actionGridLayout, 'Text',  '');
            
            % Save button
            uibutton(actionGridLayout,'push',...
                'Text','OK',...
                'ButtonPushedFcn', @this.okPressed, ...
                'tag',      'saveButton');
            
            % Quit button
            uibutton(actionGridLayout,'push',...
                'Text','Cancel',...
                'ButtonPushedFcn', @this.cancel, ...
                'tag',      'quitButton');
        end
    end
    
    methods (Access = protected, Abstract)
        createBody(this);
        % Create the body of dialog
    end
end
