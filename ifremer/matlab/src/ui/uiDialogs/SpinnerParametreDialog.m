classdef SpinnerParametreDialog < handle & SimpleTitleDialog
    % Description
    %   Class SpinnerParametreDialog wich is used to display a set of
    %   ClParametre instances with SpinerParametreComponent in a figure and waits for Ok or Cancel
    %   buttons
    %
    % Syntax
    %   a = SpinnerParametreDialog(...)
    %
    % ParametreDialog properties :
    %   params             - ClParametre array
    %   sizes              - array of 8 size subComponents
    %   spinnerParamComponentList - SpinnerParametreComponent array
    %
    % Output Arguments
    %   s : One ParametreDialog instance
    %
    % Examples
    %   p    = ClParametre('Name', 'Temperature', 'Unit', 'deg',  'MinValue', -10, 'MaxValue', 50, 'Value', 20);
    %   p(2) = ClParametre('Name', 'Salinity',    'Unit', '0/00', 'MinValue', 25,  'MaxValue', 35, 'Value', 30);
    %   a = SpinnerParametreDialog('params', p, 'Title', 'Demo of SpinnerParametreDialog');
    %   a.openDialog;
    %   flag = a.okPressedOut;
    %   if ~flag
    %       return
    %   end
    %
    %   style1 = ColorAndFontStyle('BackgroundColor', [0.7 0.7 1], 'ForegroundColor', 'k', 'FontWeight', 'bold', 'FontSize', 13, 'FontAngle', 'italic');
    %   style2 = ColorAndFontStyle('BackgroundColor', [1 0.8 0.6], 'ForegroundColor', 'k', 'FontWeight', 'bold');
    %   a.titleStyle = style1;
    %   a.paramStyle = style2;
    %   a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
    %   a.openDialog;
    %
    % Authors : MHO
    % See also StyledParametreDialog ClParametre ParametreComponent Authors
    % ----------------------------------------------------------------------------
    
    properties
        params = ClParametre.empty      % - ClParametre array
        sizes = [-2 -1 -1 0]            % - array of 4 size subComponents
        paramStyle = ColorAndFontStyle; % - Style of uicontrol text of Parametre Components
    end
    
    properties (Access = private)
        spinnerParamComponentList = SpinnerParametreComponent.empty; % - SpinnerParametreComponent array
    end
    
    events
        OneParamValueChange % event if one param value change
    end
    
    methods
        function this = SpinnerParametreDialog(varargin)
            % Superclass Constructor
            this = this@SimpleTitleDialog('windowStyle', 'normal', varargin{:});
            
            % Read input args
            [varargin, this.params]     = getPropertyValue(varargin, 'params', []);
            [varargin, this.sizes]      = getPropertyValue(varargin, 'sizes', this.sizes);
            [varargin, this.paramStyle] = getPropertyValue(varargin, 'paramStyle', this.paramStyle); %#ok<ASGLU>
        end
        
        function handleValueChange(this, ~, ~)
            % Send one value change event
            this.notify('OneParamValueChange');
        end
        
        function paramsValue = getParamsValue(this)
            % Get the "Value" properties of the parametres in an array
            paramsValue = [];
            for k=length(this.params):-1:1
                paramsValue(k) = this.params(k).Value;
            end
        end
        
        function this = set.sizes(this, sz) %#ok<MCHV2>
            % Setter of "sizes" property
            
            if ~isempty(sz)
                if length(sz) == 4
                    if sz(2) == 0
%                         str1 = 'Size(2) ne peut pas �tre �gal � 0 sinon, vous ne verrez pas la valeur du param�tre.';
                        str2 = 'Size(2) cannot be equal to 0, this would say that you do not want to see the value of the parametre.';
                        my_warndlg(str2, 1);
                    else
                        this.sizes = sz;
                        if ~isempty(this.spinnerParamComponentList) %#ok<MCSUP>
                            for paramIdx=1:length(this.spinnerParamComponentList) %#ok<MCSUP>
                                this.spinnerParamComponentList(paramIdx).sizes = this.sizes; %#ok<MCSUP>
                            end
                        end
                    end
                else
%                     str1 = 'Le param�tre "sizes" transmis � "ParametreComponent" doit �tre de taille 4.';
                    str2 = 'The parametre "sizes" sent to "ParametreComponent" must be an array of size 4.';
                    my_warndlg(str2, 1);
                end
            end
        end
    end
    
    methods (Access = protected)
        function [widthBody ,heightBody] = createBody(this, parent)
            % Create ParametreComponent List
            
            verticalBox = uiextras.VBox('Parent', parent);
            lengthComponents = length(this.params);
            for paramIdx=1:lengthComponents
                % Create SpinnerParametreComponent component
                this.spinnerParamComponentList(paramIdx) = SpinnerParametreComponent(verticalBox, 'param', this.params(paramIdx), 'sizes', this.sizes, 'style', this.paramStyle);
                % Add a listener for Value change event
                addlistener(this.spinnerParamComponentList(paramIdx), 'ValueChange', @this.handleValueChange);
            end
            
            widthBody  = 600;
            heightBody = 25 * lengthComponents;
        end
    end
end
