classdef StyledParametreUiDialog < ParametreUiDialog
    % Description
    %   Class StyledParametreUiDialog wich is used to display, with style, a set of
    %   ClParametre instances in a figure and waits for Ok or Cancel
    %   buttons
    %
    % Examples
    %   p    = ClParametre('Name', 'Temperature', 'Unit', 'deg',  'MinValue', -10, 'MaxValue', 50, 'Value', 20);
    %   p(2) = ClParametre('Name', 'Salinity',    'Unit', '0/00', 'MinValue', 25,  'MaxValue', 35, 'Value', 30);
    %   a = StyledParametreUiDialog('params', p, 'Title', 'Demo of ParametreDialog');
    %   a.openDialog;
    %   flag = a.okPressedOut;
    %   if ~flag
    %       return
    %   end
    %   paramsValue = a.getParamsValue;
    %
    %   a.sizes = {0, '2x', 0, 0, 0, '1x', '1x', 0};
    %   a.openDialog;
    
    methods
        function this = StyledParametreUiDialog(varargin)
            
            % Superclass Constructor
            this = this@ParametreUiDialog(varargin{:});
            
            % Preset styles & size
            style1 = ColorAndFontStyle('BackgroundColor', [0.7 0.7 1], 'ForegroundColor', 'k', 'FontWeight', 'bold', 'UiFontSize', 17, 'FontAngle', 'italic');
            style2 = ColorAndFontStyle('BackgroundColor', [1 0.8 0.6], 'ForegroundColor', 'k', 'FontWeight', 'bold');
            
            [varargin, this.titleStyle] = getPropertyValue(varargin, 'titleStyle', style1);
            [varargin, this.paramStyle] = getPropertyValue(varargin, 'paramStyle', style2); %#ok<ASGLU>
        end
    end
end
