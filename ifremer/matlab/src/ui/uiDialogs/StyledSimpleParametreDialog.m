classdef StyledSimpleParametreDialog < SimpleParametreDialog
    % Description
    %   Class StyledSimpleParametreDialog which is used to display, simply with style, a set of
    %   ClParametre instances in a figure and waits for Ok or Cancel
    %   buttons
    %
    % Examples
    %   p    = ClParametre('Name', 'Temperature', 'Unit', 'deg',  'MinValue', -10, 'MaxValue', 50, 'Value', 20);
    %   p(2) = ClParametre('Name', 'Salinity',    'Unit', '0/00', 'MinValue', 25,  'MaxValue', 35, 'Value', 30);
    %   a = StyledSimpleParametreDialog('params', p, 'Title', 'Demo of StyledSimpleParametreDialog');
    %   a.openDialog;
    %   flag = a.okPressedOut;
    %   if ~flag
    %       return
    %   end
    %   paramsValue = a.getParamsValue
    
    methods
        function this = StyledSimpleParametreDialog(varargin)
            
            % Superclass Constructor
            this = this@SimpleParametreDialog(varargin{:});
            
            % Preset styles & size
            style1 = ColorAndFontStyle('BackgroundColor', [0.7 0.7 1], 'ForegroundColor', 'k', 'FontWeight', 'bold', 'FontSize', 13, 'FontAngle', 'italic');
            style2 = ColorAndFontStyle('BackgroundColor', [1 0.8 0.6], 'ForegroundColor', 'k', 'FontWeight', 'bold');
            
            [varargin, this.titleStyle] = getPropertyValue(varargin, 'titleStyle', style1);
            [varargin, this.paramStyle] = getPropertyValue(varargin, 'paramStyle', style2); %#ok<ASGLU>
        end
    end
end
