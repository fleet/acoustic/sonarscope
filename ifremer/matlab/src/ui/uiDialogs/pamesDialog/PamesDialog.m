classdef PamesDialog < handle
    % Description
    %   Class PamesDialog
    %
    % Syntax
    %   a = PamesDialog(...)
    %
    % Output Arguments
    %   s : One PamesDialog instance
    %
    % Examples
    %   a = PamesDialog();
    %   a.openDialog;
    %
    % Authors : MHO
    % See also ParametreComponent ClModel Authors
    % ----------------------------------------------------------------------------
    
    properties
        parentFigure
        
        parametersComponent
        swathComponent
        resolutionComponent
        accuracyComponent
        
        pamesModel = PamesModel.empty;
    end
    
    properties (Access = private)
    end
    
    methods
        function this = PamesDialog(varargin)
            % Constructor of the class
            
            % this.openDialog();
        end
        
        function openDialog(this)
            
            % creates parent figure
            this.parentFigure = FigUtils.createSScDialog('Position' , centrageFig(1000,  600),  'windowStyle', 'normal');
            
            % Mouse watch
            this.parentFigure.set('pointer', 'watch');
            
            
            this.pamesModel = PamesModel();
            
            % Init models
            this.pamesModel.initModel();
            
            this.pamesModel.updateModel();
            
            % create tab group
            tabGroup = uitabgroup(this.parentFigure);
            
            % create Parameters Tab
            this.createParametersTab(tabGroup);
            
            % create Swath tab
            this.createSwathTab(tabGroup);
            
            % create Resolution Tab
            this.createResolutionTab(tabGroup);
            
            % create Accuracy Tab
            this.createAccuracyTab(tabGroup);
            
            % Mouse arrow
            this.parentFigure.set('pointer', 'arrow');
        end
        
        function update(this)
            % Mouse watch
            this.parentFigure.set('pointer', 'watch');
            drawnow;
            % Update models
            this.pamesModel.updateModel();
            % Update components
            this.updateComponent();
            % Mouse arrow
            this.parentFigure.set('pointer', 'arrow');
        end
        
        function updateComponent(this)
            this.parametersComponent.updateComponent();
            this.swathComponent.updateComponent();
            this.resolutionComponent.updateComponent();
            this.accuracyComponent.updateComponent();
        end
    end
    
    methods (Access = private)
        
        function createParametersTab(this, parentTabGroup)
            % create Parameters Tab
            parametersTab = uitab(parentTabGroup, 'title', 'Parameters');
            
            % create SwathComponents
            this.parametersComponent = ParametersComponent(parametersTab, this);
        end
        
        function createSwathTab(this, parentTabGroup)
            % create Swath Tab
            swathTab = uitab(parentTabGroup, 'title', 'Swath');
            % create SwathComponents
            this.swathComponent = SwathComponent(swathTab, this.pamesModel.swathModel, this);
        end
        
        function createResolutionTab(this, parentTabGroup)
            resolutionTab = uitab(parentTabGroup, 'title', 'Resolution');
            
            % create ResolutionComponents
            this.resolutionComponent = ResolutionComponent(resolutionTab, this.pamesModel.resolutionModel, this);
        end
        
        function createAccuracyTab(this, parentTabGroup)
            accuracyTab = uitab(parentTabGroup, 'title', 'Accuracy');
            % create AccuracyComponents
            this.accuracyComponent = AccuracyComponent(accuracyTab, this.pamesModel.accuracyModel, this);
        end
    end
end
