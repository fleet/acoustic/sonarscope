classdef PamesModel < handle
    %PAMESMODEL Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        
        % Sounder
        sounder = [];
        
        configurationModel = ConfigurationModel.empty;
        signalArraysModel  = SignalArraysModel.empty;
        environmentModel   = EnvironmentModel.empty;
        extSensorModel     = ExtSensorModel.empty
        
        swathModel         = SwathModel.empty;
        resolutionModel    = ResolutionModel.empty;
        accuracyModel      = AccuracyModel.empty;
    end
    
    methods
        function this = PamesModel()
            
            % Parameters
            this.configurationModel = ConfigurationModel(this);
            this.signalArraysModel  = SignalArraysModel(this);
            this.environmentModel   = EnvironmentModel(this);
            this.extSensorModel     = ExtSensorModel(this);
            
            % Swath
            this.swathModel      = SwathModel(this);
            % Resolution
            this.resolutionModel = ResolutionModel(this);
            % Accuracy
            this.accuracyModel   = AccuracyModel(this);  
        end
        
        function initModel(this)
            this.sounder = [];
            
            this.configurationModel.initModel();
            this.signalArraysModel.initModel();
            this.environmentModel.initModel();
            this.extSensorModel.initModel();
            this.swathModel.initModel();
            this.resolutionModel.initModel();
            this.accuracyModel.initModel();
        end
        
        function updateModel(this)
            this.configurationModel.updateModel();
            this.signalArraysModel.updateModel();
            this.environmentModel.updateModel();
            this.extSensorModel.updateModel();
            this.swathModel.updateModel();
            this.resolutionModel.updateModel();
            this.accuracyModel.updateModel();
        end
        
        
        % Calcul de la largeur couverte
        %
        % Width = calculFauchee(b, Z, listeTypesFond)
        %
        % Input Arguments
        %   b              : Instance de la classe cl_pames
        %   Z              : Profondeur (m)
        %   listeTypesFond : Liste des types de fond
        %
        % Output Arguments
        %   Width : Portee laterale (m)
        %
        % Examples
        %   a = cl_pames;
        %   Width = calculFauchee(a, 4000, 1:3)
        %   Z = 1:10:6000;
        %   Width = calculFauchee(a, Z, 1:3);
        %   plot(Width', -Z); grid on;
        %
        % See also CalculSBSortie Authors
        % Authors : JMA + XL
        %----------------------------------------------------------------------------
        function width = calculFauchee(this, Z, listeTypesFond)
            
            Z = abs(Z);
            
            %% Calcul de l'attenuation
            
            attenuationUtilisee = this.get_attenuation(-Z );
            
            %% Calcul du rapport signal/bruit pour une serie d'angles
            
            if isempty(this.signalArraysModel.receptionMaxAngleParam.Value)
                this.signalArraysModel.receptionMaxAngleParam.Value = 75; % Ajout� par JMA le 21/09/2015
            end
            
            teta = 1:0.1:this.signalArraysModel.receptionMaxAngleParam.Value;
            sbSortie = this.calculSBSortie(Z, teta, attenuationUtilisee, listeTypesFond);
            
            for iBS=1:length(listeTypesFond)
                vallee = abs(sbSortie(:,:,iBS) - this.swathModel.sBminSortie);
                [minVallee, subMinVallee] = min(vallee'); %#ok<ASGLU,UDIM>
                AngleMilieu = teta(subMinVallee);
                
                for k=1:length(Z)
                    % Bidouille pour supprimer un petit ziguigui au passage de la limitation angulaire a la limitation par le SNR
                    if sbSortie(k,subMinVallee(k),iBS) < this.swathModel.sBminSortie
                        subMinVallee(k) = subMinVallee(k) - 1;
                    end
                    
                    if subMinVallee(k) < 1
                        AngleMilieu(k) = 0;
                    else
                        if subMinVallee(k) < length(teta)
                            %                 subCentre = max(1,(subMinVallee(k)-1)):min(length(teta), (subMinVallee(k)+1));
                            subCentre = (subMinVallee(k)-1):(subMinVallee(k)+1);
                            %                 AM = zeros(size(teta));
                            sub = (subCentre > 0);
                            subCentre = subCentre(sub);
                            pppp = sbSortie(k, subCentre, iBS)- this.swathModel.sBminSortie;
                            sub = ~isnan(pppp);
                            AM = interp1(pppp(sub), teta(subCentre(sub)), 0);
                            AngleMilieu(k) = AM;
                        end
                    end
                end
                width(iBS, :) = Z' .* tand(AngleMilieu); %#ok<AGROW>
            end
        end
        
        
        % Calcul de l'attenuation pour des profondeurs donnees
        %
        % alpha = get_attenuation(a, z)
        %
        % Input Arguments
        %   a : Instance de la classe
        %   z : profondeurs (m)
        %
        % Output Arguments
        %   alpha : Attenuation
        %
        % Examples
        %   a = cl_pames;
        %   Z = 0:100:6000;
        %   alpha = get_attenuation( a, Z )
        %
        % See also Authors
        % Authors : JMA + XL
        %----------------------------------------------------------------------------
        function alpha = get_attenuation(this, Z)
            if this.environmentModel.absorptionCoeffTypePopupValue == 1  % Valeur constante
                alpha = ones(size(Z)) * this.environmentModel.absorptionCoefficientParam.Value;
                
            else % Valeur dependante de la profondeur
                alpha = interp1(this.environmentModel.levitusDepth', ...
                    this.environmentModel.levitusAlphaSum, ...
                    Z, 'linear', 'extrap');
            end
        end
        
        
        % Calcul du rapport signal/bruit
        %
        % sbSortie = CalculSBSortie(b, Z, teta, AttenuationUtilisee, listeTypesFond)
        %
        % Input Arguments
        %   b                    : Instance de la classe cl_pames
        %   Z                    : Profondeur (m)
        %   teta                 : Angles des rayons (deg)
        %   AttenuationUtilisee  : Attenuation (dB/km)
        %   listeTypesFond       : Liste des types de fond
        %
        % Output Arguments
        %   Width : Portee laterale (m)
        %
        % Examples
        %   a = cl_pames;
        %   sbSortie = CalculSBSortie(a, 4000, 50, 11, 1:3)
        %   teta = 0:70;
        %   sbSortie = CalculSBSortie(a, 4000, teta, 11, 1);
        %   plot(teta, sbSortie); grid on;
        %   Z = 1:10:6000;
        %   sbSortie = CalculSBSortie(a, Z, 50, 11, 1);
        %   plot(sbSortie', -Z); grid on;
        %   sbSortie = CalculSBSortie(a, Z, teta, 11, 1);
        %   imagesc(teta, -Z, sbSortie); axis xy
        %
        % See also PerteTransmission Authors
        % Authors : JMA + XL
        %----------------------------------------------------------------------------
        
        function sbSortie = calculSBSortie(this, Z, teta, attenuationUtilisee, listeTypesFond)
            
            Z = abs(Z);
            
            % R�solution fond longit (m)
            resolFondLongit = this.resolutionModel.resolLong(Z, teta);
            
            % R�solution fond transv (m)
            resolFondTransvImagery = this.resolutionModel.resolTransIma(Z, teta);
            
            % Surface insonifiee (m2)
            surfaceInsonifiee   = resolFondLongit .* resolFondTransvImagery;
            
            % Perte de transmission (dB)
            pertePropaAR = PerteTransmission(Z, teta, attenuationUtilisee);
            
            % Niveau d'emission en fonction de l'angle
            xxxx = 0;   % A PARAMETRER CORRECTEMENT A PARAMETRER CORRECTEMENT A PARAMETRER CORRECTEMENT A PARAMETRER CORRECTEMENT
            if xxxx
                nivEmissionTeta = AntenneSincDep( teta, [b.Signal.EmissionLevel, 0, b.Arrays.TxBeamWidthAcrossTrack, b.Arrays.TxArrayTilt] );
            else
                nivEmissionTeta = repmat(this.signalArraysModel.emissionLevelParam.Value, 1, length(teta));
            end
            
            % Directivite en reception
            directiviteAntenne = fctIndexDirectRect(this, teta);
            
            % Bruit de la bande (dB/mpa/sqrt(Hz))
            BruitBande = this.environmentModel.totalNoiseLevelParam.Value + reflec_Enr2dB(this.signalArraysModel.signalBandWidthParam.Value * 1.e3);
            
            % Coeff unitaire de retrodiffusion (dB/m�)
            switch this.environmentModel.modelPopupValue
                case 1  % Speculaire + Lambert
                    Coefs = this.environmentModel.lambertParams;
                case 2  % Jackson
                    Coefs = this.Environment.Jackson.Parametres;
            end
            Coefs = Coefs(listeTypesFond, :);
            
            % Bubble transmission loss
            % if(b.Environment.bubbleTransmissionLossMax == 0)
            if this.environmentModel.bubbleTransmissionLoss == 0
                bubbleTransmissionLoss = 0;
            else
                %     bubbleTransmissionLoss = interp1(b.Environment.bubbleTransmissionAngle, b.Environment.bubbleTransmissionLoss, teta, 'spline');
                bubbleTransmissionLoss = this.environmentModel.bubbleTransmissionLoss ./  cosd(teta);
                %         figure; plot(b.Environment.bubbleTransmissionAngle, b.Environment.bubbleTransmissionLoss, '*'); hold on; plot(teta, bubbleTransmissionLoss); grid on; hold off;
                bubbleTransmissionLoss = repmat(bubbleTransmissionLoss, length(Z), 1);
            end
            
            constante = reflec_Enr2dB(surfaceInsonifiee) ...
                + repmat(nivEmissionTeta, length(Z), 1) ...
                + pertePropaAR ...
                - bubbleTransmissionLoss ...
                - max(BruitBande) ...
                + repmat(directiviteAntenne, length(Z), 1) ...
                + min(this.signalArraysModel.processingGainParam.Value);
            
            for i=1:size(Coefs, 1)
                switch this.environmentModel.modelPopupValue
                    case 1  % Speculaire + Lambert
                        rfOblique = BSLurton(teta, Coefs(i,:)); %TODO BSLurton ==> BSLurtonModel
                    case 2  % Jackson
                        rfOblique = BSJackson(teta, Coefs(i,:)); %TODO BSJackson ==> BSJacksonModel
                end
                
                % S/B sortie
                sb =  constante + repmat(rfOblique, length(Z), 1);
                sbSortie(:,:,i) = sb; %#ok<AGROW>
            end
        end
        
        
        % Calcul de l'index de directivite d'une antenne en reception
        %
        % IndexDirectRec = fctIndexDirectRect(b, teta)
        %
        % Input Arguments
        %   b    : Instance de la classe cl_pames
        %   teta : Angles des rayons (deg)
        %
        % Output Arguments
        %   IndexDirectRec : Index de directivite (dB)
        %
        % Examples
        %
        % See also Authors
        % Authors : JMA + XL
        %----------------------------------------------------------------------------
        
        function IndexDirectRec = fctIndexDirectRect(this, teta)
            
            if this.signalArraysModel.rxArrayShapeValue == 1  % Antenne cylindrique
                IndexDirectRec = repmat(this.signalArraysModel.rxDirectivityIndexParam.Value, 1, length(teta));
            else    % Antenne Plane
                % TODO JMA bizarrerie � corriger
                %                beamFormRxAngles = ??? = this.signalArraysModel.rxArrayTiltHorizontal??? pour l'instant beamFormRxAngles = -75:75
                if length(teta) == length(this.signalArraysModel.beamFormRxAngles) % beamFormRxAngles = rxArrayTiltHorizontal?????? -75:75
                    IndexDirectRec = this.signalArraysModel.rxDirectivityIndexParam.Value + 20 * log10(cosd(teta - beamFormRxAngles));
                else
                    IndexDirectRec = repmat(this.signalArraysModel.rxDirectivityIndexParam.Value, 1, length(teta));
                end
            end
        end
        
        % Ouverture en emission/reception d'une antenne
        %
        % Syntax
        %   theta = AntenneOuvEmiRec(ouvertureEmission, ouvertureReception)
        %
        % Input Arguments
        %   ouvertureEmission  : Ouverture en emission  (deg)
        %   ouvertureReception : ouverture en reception (deg)
        %
        % Output Arguments
        %   theta : Angle d'ouverture en emission/reception (deg)
        %
        % Examples
        %   theta = AntenneOuvEmiRec(150, 1.5)
        %
        % See also AntenneTeta2Long AntenneLong2Teta Authors
        % Authors : JMA + XL
        %-------------------------------------------------------------------------------
        function theta = antenneOuvEmiRec(~, ouvertureEmission, ouvertureReception)
            theta = (ouvertureEmission * ouvertureReception) / sqrt(ouvertureEmission ^ 2 + ouvertureReception^2);
        end
    end
end
