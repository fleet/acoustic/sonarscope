classdef AccuracyComponent < handle & uiextras.VBox
    %CONFIGURATIONPARAMETERSCOMPONENT Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Constant)
    end
    
    properties
        accuracyModel = AccuracyModel.empty;
        pamesDialog % PamesDialog
        
        signalAxes
        acrossDistLine
        
        generalComponents = SimpleParametreComponent.empty;
        
        bathyPosParamBox
        
        bathyParamBox
        bathyComponents = SimpleParametreComponent.empty;
        
        posParamBox
        posComponents = SimpleParametreComponent.empty;
    end
    
    properties (Access = private)
        %% Components
    end
    
    methods
        
        function this = AccuracyComponent(parentTab, accuracyModel, pamesDialog)
            % Constructor of the class
            
            % Call SuperConstructor
            this = this@uiextras.VBox('Parent', parentTab, 'Spacing', 5);
            
            this.accuracyModel = accuracyModel;
            this.pamesDialog = pamesDialog;
            
            % head part
            this.createHeadPart();
            
            % axe part
            this.createAxePart();
            
            % paramameters part
            this.createParametersPart();
            
            this.set('Sizes', [-10 -60 180]);
        end
        
        
        function updateComponent(this)
            % delete old line
            delete(this.signalAxes.Children);
            %             this.signalAxes.set ('XLimMode', 'auto', 'YLimMode', 'auto');
            % Re-Plot Signal
            if this.accuracyModel.typeValue == 1 % Bathymetry
                % Plot Bathy Signal
                this.accuracyModel.bathySignal.plot( 'axesArray', this.signalAxes);
                
                xlabel (this.signalAxes, 'Accross Distance (m)');
                ylabel(this.signalAxes, 'Bathymetry uncertainty (m)');
            elseif this.accuracyModel.typeValue == 2 % Position
                % Plot Position Signal
                this.accuracyModel.posSignal.plot( 'axesArray', this.signalAxes);
                
                xlabel (this.signalAxes, 'Accross Track Range (m)');
                ylabel(this.signalAxes, 'Position Precision (m)');
            end
            
            % Across Distance Line
            X = [this.accuracyModel.distanceParam.Value this.accuracyModel.distanceParam.Value];
            this.acrossDistLine = line (this.signalAxes, X, this.signalAxes.get('YLim'), 'Color', [0 0 0]);
            
            % Update Component
            for k=1:numel(this.generalComponents)
                this.generalComponents(k).updateComponent();
            end
            
            if this.accuracyModel.typeValue == 1 % Bathymetry
                this.bathyParamBox.Visible = 'on';
                this.posParamBox.Visible = 'off';
                this.bathyPosParamBox.set('Sizes', [-1 0]);
            elseif this.accuracyModel.typeValue == 2 % Position
                this.bathyParamBox.Visible = 'off';
                this.posParamBox.Visible = 'on';
                this.bathyPosParamBox.set('Sizes', [0 -1]);
            end
            
            for k=1:numel(this.bathyComponents)
                this.bathyComponents(k).updateComponent();
            end
            for k=1:numel(this.posComponents)
                this.posComponents(k).updateComponent();
            end
        end
    end
    
    
    methods (Access = private)
        
        function createHeadPart(this)
            headVBox = uiextras.VBox('Parent', this, 'Spacing', 0);
            
            titleStyle   = ColorAndFontStyle.getTitleColorAndFontStyle; % - Style of title of dialog box
            
            title = 'Bathymetry and Position Accuracy';
            % Create TitleText
            uicontrol('Style', 'text', 'Parent', headVBox, ...
                'string',          title, ...
                'BackgroundColor', titleStyle.BackgroundColor, ...
                'ForegroundColor', titleStyle.ForegroundColor, ...
                'FontName',        titleStyle.FontName, ...
                'FontSize',        titleStyle.FontSize, ...
                'FontUnits',       titleStyle.FontUnits, ...
                'FontWeight',      titleStyle.FontWeight, ...
                'FontAngle',       titleStyle.FontAngle,...
                'HorizontalAlignment', 'center');
            
            displayHeadBox =  uiextras.HBox('Parent', headVBox, 'Spacing', 0);
            
%             bathyPosRadioButtonsComponent = RadioButtonsComponent(displayHeadBox, 'orientation', RadioButtonsComponent.Orientation{1},...
            RadioButtonsComponent(displayHeadBox, 'orientation', RadioButtonsComponent.Orientation{1},...
                'title', 'Display', 'titleButtons', this.accuracyModel.typeList, 'init', this.accuracyModel.typeValue, 'callbackFunc',@this.bathyPosRadioCallback);
            displayHeadBox.set('Sizes', 500);
            
            headVBox.set('Sizes', [-1 25]);
        end
        
        function createAxePart(this)
            axesVBox = uiextras.VBox('Parent', this, 'Spacing', 5);
            
            % Axes
            this.signalAxes = axes('Parent',axesVBox, 'Units', 'normalized');
            [tb,~] = axtoolbar(this.signalAxes, {'datacursor', 'pan', 'zoomin','zoomout','restoreview'});
            tb.Visible = 'on';
            
            if this.accuracyModel.typeValue == 1 % Bathymetry
                % Plot Bathy Signal
                this.accuracyModel.bathySignal.plot('axesArray', this.signalAxes);
                
                xlabel (this.signalAxes, 'Accross Distance (m)');
                ylabel(this.signalAxes, 'Bathymetry uncertainty (m)');
            elseif this.accuracyModel.typeValue == 2 % Position
                % Plot Position Signal
                this.accuracyModel.posSignal.plot('axesArray', this.signalAxes);
                
                xlabel (this.signalAxes, 'Accross Track Range (m)');
                ylabel(this.signalAxes, 'Position Precision (m)');
            end
            
            % Across Distance Line
            X = [this.accuracyModel.distanceParam.Value this.accuracyModel.distanceParam.Value];
            this.acrossDistLine = line (this.signalAxes, X, this.signalAxes.get('YLim'), 'Color', [0 0 0]);
        end
        
        
        function createParametersPart(this)
            parametersPanel = uipanel('Parent', this, 'Title', 'Parameters', 'FontSize', 13, 'FontWeight', 'bold');
            parametersHBox  = uiextras.HBox('Parent', parametersPanel, 'Spacing', 5);
            
            % VBox1
                generalParamPanel = uipanel('Parent', parametersHBox);
            generalParamBox = uiextras.VBox('Parent', generalParamPanel, 'Spacing', 0);
            
            sz = [-2 -1 -1 0];
            for k=1:numel(this.accuracyModel.generalParams)
                this.generalComponents(k) = SimpleParametreComponent(generalParamBox, 'param', this.accuracyModel.generalParams(k), 'Sizes', sz);
                       % Add a listener for Value change event
                addlistener(this.generalComponents(k), 'ValueChange', @this.paramValueChange);
            end
            
            generalParamBox.set('Sizes', [25 25 25]);
            
            % VBox2
            bathyPosParamPanel = uipanel('Parent', parametersHBox);
            this.bathyPosParamBox = uiextras.VBox('Parent', bathyPosParamPanel, 'Spacing', 0);
            
            % Color styles
            style(1)  = ColorAndFontStyle('BackgroundColor', 'k', 'ForegroundColor', 'w');
            style(2)  = ColorAndFontStyle('BackgroundColor', 'r', 'ForegroundColor', 'k');
            style(3)  = ColorAndFontStyle('BackgroundColor', 'g', 'ForegroundColor', 'k');
            style(4)  = ColorAndFontStyle('BackgroundColor', 'b', 'ForegroundColor', 'w');
            style(5)  = ColorAndFontStyle('BackgroundColor', 'm', 'ForegroundColor', 'k');
            style(6)  = ColorAndFontStyle('BackgroundColor', 'c', 'ForegroundColor', 'k');
            
            % Bathy Box
            this.bathyParamBox = uiextras.VBox('Parent', this.bathyPosParamBox, 'Spacing', 0);
            sz = [-2 -1 -1 0];

            for k=1:numel(this.accuracyModel.bathyParams)
                this.bathyComponents(k) = SimpleParametreComponent(this.bathyParamBox, 'param', this.accuracyModel.bathyParams(k), 'editable', false, 'style', style(k), 'Sizes', sz);
            end
            this.bathyParamBox.set('Sizes', [25 25 25 25 25 25]);
            
            %Pos Box
            this.posParamBox = uiextras.VBox('Parent', this.bathyPosParamBox, 'Spacing', 0);
            
            sz = [-2 -1 -1 0];
            for k=1:numel(this.accuracyModel.posParams)
                this.posComponents(k) = SimpleParametreComponent(this.posParamBox, 'param', this.accuracyModel.posParams(k), 'editable', false, 'style', style(k), 'Sizes', sz);
            end
            this.posParamBox.set('Sizes', [25 25 25 25 25 25]);
            
            if this.accuracyModel.typeValue == 1 % Bathymetry
                this.bathyParamBox.Visible = 'on';
                this.posParamBox.Visible   = 'off';
                this.bathyPosParamBox.set('Sizes', [-1 0]);
            elseif this.accuracyModel.typeValue == 2 % Position
                this.bathyParamBox.Visible = 'off';
                this.posParamBox.Visible   = 'on';
                this.bathyPosParamBox.set('Sizes', [0 -1]);
            end
            
            parametersHBox.set('Sizes', [-1 -1]);
        end
        
        %% Callback
        function bathyPosRadioCallback(this, src, ~)
            
            if strcmp(src.String, this.accuracyModel.typeList{1})
                this.accuracyModel.typeValue = 1;
            elseif  strcmp(src.String, this.accuracyModel.typeList{2})
                this.accuracyModel.typeValue = 2;
            end
            
            this.updateComponent();
        end
        
        function paramValueChange(this, ~, ~)
            % Update models & components
            this.pamesDialog.update();
        end
        
    end
end
