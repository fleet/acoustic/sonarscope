classdef AccuracyModel < handle
    %ACCURACYMODEL Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        pamesModel
        
        %% Params
        
        typeList = {'Bathymetry Accuracy', 'Position Accuracy'};
        typeValue = 1; % typeValue 1 = Bathymetry / 2 = Position
        
        % TODO JMA same params than resolutionModel???
        % General Params
        generalParams = ClParametre.empty;
        depthParam    = ClParametre('Name', 'Depth',           'Unit', 'm');
        distanceParam = ClParametre('Name', 'Across Distance', 'Unit', 'm');
        angleParam    = ClParametre('Name', 'Beam Angle',      'Unit', 'deg');
        
        % Bathymetry params
        bathyParams                 = ClParametre.empty;
        bathyTotalParam             = ClParametre('Name', 'Total',               'Unit', 'm');
        bathyAcousticParam          = ClParametre('Name', 'Acoustic',            'Unit', 'm');
        bathyRollParam              = ClParametre('Name', 'Roll',                'Unit', 'm');
        bathyHeaveParam             = ClParametre('Name', 'Heave',               'Unit', 'm');
        bathyArraySoundSpeedParam   = ClParametre('Name', 'Array Sound Speed',   'Unit', 'm');
        bathyAverageSoundSpeedParam = ClParametre('Name', 'Average Sound Speed', 'Unit', 'm');
        
        % Position params
        posParams                 = ClParametre.empty;
        posTotalParam             = ClParametre('Name', 'Total',               'Unit', 'm');
        posAcousticParam          = ClParametre('Name', 'Acoustic',            'Unit', 'm');
        % posRollParam            = ClParametre('Name', 'Roll',                'Unit', 'm');
        posPitchParam             = ClParametre('Name', 'Pitch',               'Unit', 'm');
        posHeadingParam           = ClParametre('Name', 'Heading',             'Unit', 'm');
        posArraySoundSpeedParam   = ClParametre('Name', 'Array Sound Speed',   'Unit', 'm');
        posAverageSoundSpeedParam = ClParametre('Name', 'Average Sound Speed', 'Unit', 'm');
        
        %% Signals
        xSample    =  XSample('name', 'X',     'unit', 'm'); % Same for bathy and pos
        ihoXSample =  XSample('name', 'IHO X', 'unit', 'm'); % Same for iho bathy and iho pos
        
        % Bathymetry Signal
        bathyYSample                 = YSample.empty;
        bathyTotalSample             = YSample('name', 'Total',               'color', 'k');
        bathyAcousticSample          = YSample('name', 'Acoustic',            'color', 'r');
        bathyRollSample              = YSample('name', 'Roll',                'color', 'g');
        bathyHeaveSample             = YSample('name', 'Heave',               'color', 'b');
        bathyArraySoundSpeedSample   = YSample('name', 'Array Sound Speed',   'color', 'm');
        bathyAverageSoundSpeedSample = YSample('name', 'Average Sound Speed', 'color', 'c');
        bathySignal                  = ClSignal('name', 'Bathymetry Accuracy');
        
        % Bathy IHO
        bathyIhoYSample       = YSample.empty;
        bathyIhoSpecialSample = YSample('name', 'Special', 'color', 'k');
        bathyIho1Sample       = YSample('name', 'IHO 1',   'color', 'k');
        bathyIho2Sample       = YSample('name', 'IHO 2',   'color', 'g');
        bathyIho3Sample       = YSample('name', 'IHO 3',   'color', 'b');
        bathyIhoSignal        = ClSignal('name', 'Bathy IHO');
        
        % Position Signal
        posYSample                 = YSample.empty;
        posTotalSample             = YSample('name', 'Total',               'color', 'k');
        posAcousticSample          = YSample('name', 'Acoustic',            'color', 'r');
        %  posRollSample           = YSample('name', 'Roll',                'color', 'g');
        posPitchSample             = YSample('name', 'Pitch',               'color', 'g');
        posHeadingSample           = YSample('name', 'Heading',             'color', 'b');
        posArraySoundSpeedSample   = YSample('name', 'Array Sound Speed',   'color', 'm');
        posAverageSoundSpeedSample = YSample('name', 'Average Sound Speed', 'color', 'c');
        posSignal                  = ClSignal('name', 'Position Accuracy');
        
        % Position IHO
        posIhoYSample       = YSample.empty;
        posIhoSpecialSample = YSample('name', 'Special', 'color', 'k');
        posIho1Sample       = YSample('name', 'IHO 1',   'color', 'k');
        posIho2Sample       = YSample('name', 'IHO 2',   'color', 'g');
        posIho3Sample       = YSample('name', 'IHO 3',   'color', 'b');
        posIhoSignal        = ClSignal('name', 'Position IHO');
    end
    
    methods
        function this = AccuracyModel(pamesModel)
            this.pamesModel = pamesModel;
            
            %% Params
            
            % Genral Params
            this.generalParams = ClParametre.empty;
            this.generalParams(end+1) = this.depthParam;
            this.generalParams(end+1) = this.distanceParam;
            this.generalParams(end+1) = this.angleParam;
            
            % Bathymetry params
            this.bathyParams = ClParametre.empty;
            this.bathyParams(end+1) = this.bathyTotalParam;
            this.bathyParams(end+1) = this.bathyAcousticParam;
            this.bathyParams(end+1) = this.bathyRollParam;
            this.bathyParams(end+1) = this.bathyHeaveParam;
            this.bathyParams(end+1) = this.bathyArraySoundSpeedParam;
            this.bathyParams(end+1) = this.bathyAverageSoundSpeedParam;
            
            % Position params
            this.posParams = ClParametre.empty;
            this.posParams(end+1) = this.posTotalParam;
            this.posParams(end+1) = this.posAcousticParam;
            %  this.posParams(end+1) = this.posRollParam;
            this.posParams(end+1) = this.posPitchParam;
            this.posParams(end+1) = this.posHeadingParam;
            this.posParams(end+1) = this.posArraySoundSpeedParam;
            this.posParams(end+1) = this.posAverageSoundSpeedParam;
            
            %% Signals
            
            % Init Bathy signal
            this.bathyYSample(1) = this.bathyTotalSample;
            this.bathyYSample(2) = this.bathyAcousticSample;
            this.bathyYSample(3) = this.bathyRollSample;
            this.bathyYSample(4) = this.bathyHeaveSample;
            this.bathyYSample(5) = this.bathyArraySoundSpeedSample;
            this.bathyYSample(6) = this.bathyAverageSoundSpeedSample;
            this.bathySignal.set('xSample', this.xSample, 'ySample',this.bathyYSample);
            this.bathySignal.setDefaultXSampleIfNeeded();
            
            % Init Bathy IHO
            this.bathyIhoYSample(1) = this.bathyIhoSpecialSample;
            this.bathyIhoYSample(2) = this.bathyIho1Sample;
            this.bathyIhoYSample(3) = this.bathyIho2Sample;
            this.bathyIhoYSample(4) = this.bathyIho3Sample;
            this.bathyIhoSignal.set('xSample', this.ihoXSample, 'ySample',this.bathyIhoYSample);
            this.bathyIhoSignal.setDefaultXSampleIfNeeded();
            
            % Init Pos signal
            this.posYSample(1) = this.posTotalSample;
            this.posYSample(2) = this.posAcousticSample;
            this.posYSample(3) = this.posPitchSample;
            this.posYSample(4) = this.posHeadingSample;
            this.posYSample(5) = this.posArraySoundSpeedSample;
            this.posYSample(6) = this.posAverageSoundSpeedSample;
            this.posSignal.set('xSample', this.xSample, 'ySample',this.posYSample);
            this.posSignal.setDefaultXSampleIfNeeded();
            
            % Init Bathy signal
            this.posIhoYSample(1) = this.posIhoSpecialSample;
            this.posIhoYSample(2) = this.posIho1Sample;
            this.posIhoYSample(3) = this.posIho2Sample;
            this.posIhoYSample(4) = this.posIho3Sample;
            this.bathyIhoSignal.set('xSample', this.ihoXSample, 'ySample',this.posIhoYSample);
            this.bathyIhoSignal.setDefaultXSampleIfNeeded();
        end
        
        
        function initModel(this)
            %% Params
            % Genral Params
            for k=1:numel(this.generalParams)
                this.generalParams(k).Value = [];
            end
            % Bathymetry params
            for k=1:numel(this.bathyParams)
                this.bathyParams(k).Value = [];
            end
            % Position params
            for k=1:numel(this.posParams)
                this.posParams(k).Value = [];
            end
            
            %% Signals
            % Init Bathy signal
            this.xSample.data = [];
            for k=1:numel(this.bathyYSample)
                this.bathyYSample(k).data = [];
            end
            % Init Bathy IHO
            this.ihoXSample.data = [];
            for k=1:numel(this.bathyIhoYSample)
                this.bathyIhoYSample(k).data = [];
            end
            % Init Pos signal
            for k=1:numel(this.posYSample)
                this.posYSample(k).data = [];
            end
            % Init Bathy signal
            for k=1:numel(this.posIhoYSample)
                this.posIhoYSample(k).data = [];
            end
        end
        
        function updateModel(this)
            calculer_preci_param(this);
            calculer_preci_courbes(this);
            calculer_preci_limites(this); 
        end
        
        % Précision du positionnement
        %
        % deltaX = CalculDx(a, Z, T)
        %
        % Input Arguments
        %   a : Instance de la classe cl_pames
        %   Z : Profondeur (m)
        %   T : Angle du rayon (deg)
        %
        % Output Arguments
        %   deltaX                  : Précision de mesure de positionnement (m)
        %   deltaXAcou              : Précision de mesure due a l'acoustique
        %   deltaXRoulis            : Précision de mesure due au roulis
        %   deltaXHeave             : Précision de mesure due au Heave
        %   deltaXCeleriteAntenne   : Précision de mesure due a la celerite au niveau de l'antenne
        %   deltaXCeleriteMoyenne   : Précision de mesure due a la celerite
        function [deltaX, deltaXAcou, deltaXRoulis, deltaXTangage, deltaXCap, deltaXCeleriteAntenne, deltaXCeleriteMoyenne] = calculDx(this, Z, T)
            
            size_Z = size(Z);
            size_T = size(T);
            
            deltaXAcou = this.calculDracoustique(Z, T);
            
            deltaXRoulis = this.calculDrroulis(Z);
            if ~isequal(size_Z, size_T)
                if (length(Z) ~= 1) && isvector(Z)
                    deltaXRoulis = repmat(deltaXRoulis, 1, size_T(2));
                end
            end
            
            deltaXTangage = this.calculDrtangage(Z);
            if ~isequal(size_Z, size_T)
                if isvector(Z)
                    deltaXTangage = repmat(deltaXTangage, 1, size_T(2));
                end
            end
            
            deltaXCap             = this.calculDrcap(Z, T);
            deltaXCeleriteAntenne = this.calculDrceleriteAntenne(Z, T);
            deltaXCeleriteMoyenne = this.calculDrceleriteMoyenne(Z, T);
            
            deltaX = sqrt (...
                deltaXAcou            .^ 2 + ...
                deltaXRoulis          .^ 2 + ...
                deltaXTangage         .^ 2 + ...
                deltaXCap             .^ 2 + ...
                deltaXCeleriteAntenne .^ 2 + ...
                deltaXCeleriteMoyenne .^ 2  );
        end
        
        % Précision de la bathymetrie
        %
        % deltaZ = CalculDz(a, Z, T)
        %
        % Input Arguments
        %   a     : Instance de la classe cl_pames
        %   Z : Profondeur (m)
        %   T : Angle du rayon (deg)
        %
        % Output Arguments
        %   deltaZ                  : Précision de mesure de positionnement (m)
        %   deltaZAcou              : Précision de mesure due a l'acoustique
        %   deltaZRoulis            : Précision de mesure due au roulis
        %   deltaZHeave             : Précision de mesure due au Heave
        %   deltaZCeleriteAntenne   : Précision de mesure due a la celerite au niveau de l'antenne
        %   deltaZCeleriteMoyenne   : Précision de mesure due a la celerite
        function [deltaZ, deltaZAcou, deltaZRoulis, deltaZHeave, deltaZCeleriteAntenne, deltaZCeleriteMoyenne] = calculDz(this, Z, T)
            
            deltaZAcou            = this.calculDzacoustique(Z, T);
            deltaZRoulis          = this.calculDzroulis(Z, T);
            deltaZHeave           = this.pamesModel.extSensorModel.heaveMeasurementParam.Value * ones(size(Z, 1), size(T,2));
            deltaZCeleriteAntenne = this.calculDzceleriteAntenne(Z, T);
            deltaZCeleriteMoyenne = this.calculDzceleriteMoyenne(Z);
            
            size_Z = size(Z);
            size_T = size(T);
            if ~isequal(size_Z, size_T)
                if isvector(Z)
                    deltaZCeleriteMoyenne = repmat(deltaZCeleriteMoyenne, 1, size_T(2));
                end
            end
            deltaZ = sqrt (...
                deltaZAcou            .^ 2 + ...
                deltaZRoulis          .^ 2 + ...
                deltaZHeave           .^ 2 + ...
                deltaZCeleriteAntenne .^ 2 + ...
                deltaZCeleriteMoyenne .^2  );
        end
    end
    
    methods (Access = private)
        
        % Calcul des valeurs, parametres et courbes de Précision
        function calculer_preci_param (this)
            
            %             if ~isempty(this.depthParam.Value)
            %                 a.Resolution.Depth    = this.depthParam.Value;
            %                 a.Resolution.Distance = this.distanceParam.Value;
            %                 a.Resolution.Angle    = this.angleParam.Value;
            %             end
            %
            %             calculer_resol_param (this);
            
            this.depthParam.Value    = this.pamesModel.resolutionModel.depthParam.Value;
            this.distanceParam.Value = this.pamesModel.resolutionModel.distanceParam.Value;
            this.angleParam.Value    = this.pamesModel.resolutionModel.angleParam.Value;
            
            teta = atand(this.distanceParam.Value / this.depthParam.Value);
            
            % Bathy
            [deltaZ, deltaZAcou, deltaZRoulis, deltaZHeave, deltaZCeleriteAntenne, deltaZCeleriteMoyenne] = ...
                this.calculDz(this.depthParam.Value, teta);
            this.bathyAcousticParam.Value          = deltaZAcou;
            this.bathyRollParam.Value              = deltaZRoulis;
            this.bathyHeaveParam.Value             = deltaZHeave;
            this.bathyArraySoundSpeedParam.Value   = deltaZCeleriteAntenne;
            this.bathyAverageSoundSpeedParam.Value = deltaZCeleriteMoyenne;
            this.bathyTotalParam.Value             = deltaZ;
            
            % Position
            [deltaX, deltaXAcou, ~, deltaXTangage, deltaXCap, deltaXCeleriteAntenne, deltaXCeleriteMoyenne] = ...
                this.calculDx(this.depthParam.Value, teta);
            this.posAcousticParam.Value             = deltaXAcou;
            %         this.posRollParam.Value                 = deltaXRoulis;
            this.posPitchParam.Value             = deltaXTangage;
            this.posHeadingParam.Value           = deltaXCap;
            this.posArraySoundSpeedParam.Value   = deltaXCeleriteAntenne;
            this.posAverageSoundSpeedParam.Value = deltaXCeleriteMoyenne;
            this.posTotalParam.Value             = deltaX;
        end
        
        % Calcul des courbes de Précision
        function calculer_preci_courbes(this)
            
            width    = this.pamesModel.calculFauchee (this.depthParam.Value, 1:4);
            maxWidth = max(width);
            
            X = linspace(0, maxWidth, 500);
            X(1) = [];
            this.xSample.data = X ;
            teta = atand(X / this.depthParam.Value);
            
            % Bathy
            [deltaZ, deltaZAcou, deltaZRoulis, deltaZHeave, deltaZCeleriteAntenne, deltaZCeleriteMoyenne] = ...
                this.calculDz(this.depthParam.Value, teta);
            this.bathyAcousticSample.data          = deltaZAcou;
            this.bathyRollSample.data              = deltaZRoulis;
            this.bathyHeaveSample.data             = deltaZHeave;
            this.bathyArraySoundSpeedSample.data   = deltaZCeleriteAntenne;
            this.bathyAverageSoundSpeedSample.data = deltaZCeleriteMoyenne;
            this.bathyTotalSample.data             = deltaZ;
            
            % Position
            [deltaX, deltaXAcou, ~, deltaXTangage, deltaXCap, deltaXCeleriteAntenne, deltaXCeleriteMoyenne] = ...
                this.calculDx(this.depthParam.Value, teta);
            this.posAcousticSample.data             = deltaXAcou;
            %     this.posRollSample.data                 = deltaXRoulis;
            this.posPitchSample.data             = deltaXTangage;
            this.posHeadingSample.data           = deltaXCap;
            this.posArraySoundSpeedSample.data   = deltaXCeleriteAntenne;
            this.posAverageSoundSpeedSample.data = deltaXCeleriteMoyenne;
            this.posTotalSample.data             = deltaX;
        end  
        
        % Calcul des limites de Précision de la norme IHO pour une hauteur donnee
        function calculer_preci_limites (this)
            
            this.ihoXSample.data(1) = min (this.xSample.data);
            this.ihoXSample.data(2) = max (this.xSample.data);
            
            if ~isempty(this.depthParam.Value)
                % Bathy
                this.bathyIhoSpecialSample.data(1:2) = .25 + this.depthParam.Value * .0075;
                if this.depthParam.Value > 2
                    this.bathyIho1Sample.data(1:2) = .5  + this.depthParam.Value * .013;
                end
                if this.depthParam.Value > 100
                    this.bathyIho2Sample.data(1:2) = 1   + this.depthParam.Value * 0.023;
                end
                if this.depthParam.Value > 200
                    this.bathyIho3Sample.data(1:2) = 1   + this.depthParam.Value * 0.023;
                end
                
                % Postion
                this.posIhoSpecialSample.data(1:2) = 2;
                if this.depthParam.Value > 2
                    this.posIho1Sample.data(1:2) = 5   + this.depthParam.Value * .05;
                end
                if this.depthParam.Value > 100
                    this.posIho2Sample.data(1:2) = 20  + this.depthParam.Value * .05;
                end
                if this.depthParam.Value > 200
                    this.posIho3Sample.data(1:2) = 150 + this.depthParam.Value * .05;
                end
            end
        end
        
        
        % Précision de la bathymetrie / l'acoustique
        %
        % deltaZ = CalculDzacoustique(a, Z, T)
        %
        % Input Arguments
        %   a : Instance de la classe cl_pames
        %   Z : Profondeur (m)
        %   T : Angle du rayon (deg)
        %
        % Output Arguments
        %   deltaZ : Précision de mesure de bathymetrie (m)
        function deltaZ = calculDzacoustique(this, Z, T)
            
            Z = abs(Z);
            
            size_Z = size(Z);
            size_T = size(T);
            if ~isequal(size_Z, size_T)
                if (length(Z) ~= 1) && isvector(Z)
                    Z = repmat(Z, 1, size_T(2));
                end
                if (length(T) ~= 1) && isvector(T)
                    T = repmat(T, size_Z(1), 1);
                end
            end
            
            DeltaRsSur2 = this.pamesModel.signalArraysModel.rangeSamplingParam.Value / 2; %FIXME
            CThoSur4 = this.pamesModel.signalArraysModel.rangeResolutionParam.Value / 2;
            Partie1 = (DeltaRsSur2 ^ 2 + CThoSur4 ^ 2) * (cosd(T) .^ 2);
            
            OuvertureTransvEmiRec = AntenneOuvEmiRec(this.pamesModel.signalArraysModel.txBeamWidthAcrossTrackParam.Value, this.pamesModel.signalArraysModel.rxBeamWidthAcrossTrackParam.Value);
            
            P1 = OuvertureTransvEmiRec * (pi / 180) * Z;
            P2 = tand(T) / 12;
            Partie2 = P1 .* P2;
            DzacAmplitude = Partie1 + Partie2 .* Partie2;
            DzacAmplitude = sqrt(DzacAmplitude);
            
            DzacPhase = Partie1;
            Partie2 = 0.04 * this.pamesModel.signalArraysModel.rangeSamplingParam.Value * OuvertureTransvEmiRec * (pi / 180) * ...
                Z .* sind(T);
            DzacPhase = sqrt(DzacPhase + Partie2);
            deltaZ = min(DzacAmplitude, DzacPhase);
        end
        
        % Précision de la bathymetrie / mesure du roulis
        %
        % deltaZ = CalculDzroulis(a, Z, T)
        %
        % Input Arguments
        %   a     : Instance de la classe cl_pames
        %   Z : Profondeur (m)
        %   T : Angle du rayon (deg)
        %
        % Output Arguments
        %   deltaZ : Précision de mesure de bathymetrie (m)
        function deltaZ = calculDzroulis(this, Z, T)
            
            X = this.calculDrroulis(Z);
            
            size_X = size(X);
            size_T = size(T);
            if ~isequal(size_X, size_T)
                if (length(X) ~= 1) && isvector(X)
                    X = repmat(X, 1, size_T(2));
                end
                if (length(T) ~= 1) && isvector(T)
                    T = repmat(T, size_X(1), 1);
                end
            end
            
            deltaZ = X .* tand(T);
        end
        
        
        % Précision du positionnement / mesure du roulis
        %
        % deltaX = CalculDrroulis(a, Z)
        %
        % Input Arguments
        %   a     : Instance de la classe cl_pames
        %   Z : Profondeur (m)
        %
        % Output Arguments
        %   deltaX : Précision de mesure de positionnement (m)
        function deltaX = calculDrroulis(this, Z)
            
            deltaX = Z * this.pamesModel.extSensorModel.rollMeasurementParam.Value * (pi/180);
        end
        
        % Précision de la bathymetrie / mesure de la celerite au niveau de l'antenne
        %
        % deltaZ = CalculDzceleriteAntenne(a, Z, T)
        %
        % Input Arguments
        %   a     : Instance de la classe cl_pames
        %   Z : Profondeur (m)
        %   T : Angle du rayon (deg)
        %
        % Output Arguments
        %   deltaZ : Précision de mesure de positionnement (m)
        function deltaZ = calculDzceleriteAntenne(this, Z, T)
            
            X = this.calculDrceleriteAntenne(Z, T);
            tg = tand(T);
            
            size_Z = size(Z);
            size_T = size(T);
            if ~isequal(size_Z, size_T)
                if (length(T) ~= 1) && isvector(T)
                    tg = repmat(tg, size_Z(1), 1);
                end
            end
            
            deltaZ = X .* tg;
        end
        
        % Précision du positionnement / mesure de la celerite au niveau de l'antenne
        %
        % deltaX = CalculDrceleriteAntenne(a, Z, T)
        %
        % Input Arguments
        %   a     : Instance de la classe cl_pames
        %   Z : Profondeur (m)
        %   T : Angle du rayon (deg)
        %
        % Output Arguments
        %   deltaX : Précision de mesure de positionnement (m)
        function deltaX = calculDrceleriteAntenne(this, Z, T)
            
            Z = abs(Z);
            
            size_Z = size(Z);
            size_T = size(T);
            if ~isequal(size_Z, size_T)
                if (length(Z) ~= 1) && isvector(Z)
                    Z = repmat(Z, 1, size_T(2));
                end
                if (length(T) ~= 1) && isvector(T)
                    T = repmat(T, size_Z(1), 1);
                end
            end
            
            if this.pamesModel.signalArraysModel.rxArrayShapeValue == 2  % Antenne circulaire
                deltaX = zeros(size(Z,1), size(T,2));
            else                            % antenne plane
                if isempty(this.pamesModel.signalArraysModel.rxArrayTiltHorizontalParam.Value)
                    deltaX = Z * (this.pamesModel.extSensorModel.arraySoundSpeedMeasurementParam.Value / 1500) .* abs(tand(T));
                else
                    deltaX = Z * (this.pamesModel.extSensorModel.arraySoundSpeedMeasurementParam.Value / 1500) .* abs(tand(T - this.pamesModel.signalArraysModel.rxArrayTiltHorizontalParam.Value));
                end
            end
        end
        
        % Précision de la bathymetrie / mesure de la celerite
        %
        % deltaZ = CalculDzceleriteMoyenne(a, Z, T)
        %
        % Input Arguments
        %   a     : Instance de la classe cl_pames
        %   Z : Profondeur (m)
        %
        % Output Arguments
        %   deltaZ : Précision de mesure de bathymetrie (m)
        function deltaZ = calculDzceleriteMoyenne(this, Z)
            deltaZ = Z * this.pamesModel.extSensorModel.averageSoundSpeedParam.Value / 1500;
        end
        
        % Précision du positionnement / l'acoustique
        %
        % deltaX = CalculDracoustique(a, Z, T)
        %
        % Input Arguments
        %   a     : Instance de la classe cl_pames
        %   Z : Profondeur (m)
        %   T : Angle du rayon (deg)
        %
        % Output Arguments
        %   deltaX : Précision de mesure de positionnement (m)
        function deltaX = calculDracoustique(this, Z, T)
            
            Dz = this.calculDzacoustique(Z, T);
            tg = tand(T);
            
            size_Z = size(Z);
            size_T = size(T);
            if ~isequal(size_Z, size_T)
                if (length(T) ~= 1) && isvector(T)
                    tg = repmat(tg, size_Z(1), 1);
                end
            end
            
            deltaX  = Dz .* tg;
        end
        
        % Précision du positionnement / mesure du tangage
        %
        % deltaX = CalculDrtangage(a, Z)
        %
        % Input Arguments
        %   a     : Instance de la classe cl_pames
        %   Z : Profondeur (m)
        %
        % Output Arguments
        %   deltaX : Précision de mesure de positionnement (m)
        function deltaX = calculDrtangage(this, Z)
            deltaX = Z * (this.pamesModel.extSensorModel.pitchMeasurementParam.Value * (pi / 180));
        end
        
        % Précision du positionnement / mesure du cap
        %
        % deltaX = CalculDrcap(a, Z, T)
        %
        % Input Arguments
        %   a     : Instance de la classe cl_pames
        %   Z : Profondeur (m)
        %   T : Angle du rayon (deg)
        %
        % Output Arguments
        %   deltaX : Précision de mesure de positionnement (m)
        function deltaX = calculDrcap(this, Z, T)
            Z = abs(Z);
            size_Z = size(Z);
            size_T = size(T);
            if ~isequal(size_Z, size_T)
                if (length(Z) ~= 1) && isvector(Z)
                    Z = repmat(Z, 1, size_T(2));
                end
                if (length(T) ~= 1) && isvector(T)
                    T = repmat(T, size_Z(1), 1);
                end
            end
            
            tg = tand(T);
            deltaX = (this.pamesModel.extSensorModel.headingMeasurementParam.Value * (pi / 180)) * Z .* tg;
        end
        
        % Précision de la bathymetrie / mesure de la celerite
        %
        % deltaZ = CalculDrceleriteMoyenne(a, Z, T)
        %
        % Input Arguments
        %   a     : Instance de la classe cl_pames
        %   Z : Profondeur (m)
        %   T : Angle du rayon (deg)
        %
        % Output Arguments
        %   deltaZ : Précision de mesure de bathymetrie (m)
        function deltaZ = calculDrceleriteMoyenne(this, Z, T)
            Z = abs(Z);
            tg = tand(T);
            deltaZ = this.calculDzceleriteMoyenne(Z);
            
            size_Z = size(Z);
            size_T = size(T);
            if ~isequal(size_Z, size_T)
                if (length(Z) ~= 1) && isvector(Z)
                    deltaZ = repmat(deltaZ, 1, size_T(2));
                end
                if (length(T) ~= 1) && isvector(T)
                    tg = repmat(tg, size_Z(1), 1);
                end
            end
            deltaZ = deltaZ .* tg;
        end
    end
end
