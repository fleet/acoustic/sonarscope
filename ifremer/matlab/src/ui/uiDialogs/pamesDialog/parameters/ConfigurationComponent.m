classdef ConfigurationComponent < handle & uiextras.HBoxFlex
    %CONFIGURATIONPARAMETERSCOMPONENT Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Constant)
    end
    
    properties
        pamesDialog % PamesDialog
        configurationModel
    end
    
    properties (Access = private)
        
        %% Components
        
        % Sounder
        modeRadioComponent
        sounderNameText
        sounderTypePopupmenu
        mainModePopupmenu
        optModePopupmenu
        opt2ModePopupmenu
        
%         % Ship / Fish / Vehicle
%         shipNamePopupmenu
%         shipNoiseComponent  = SimpleParametreComponent.empty;
%         attitudeSensorPopupmenu
%         sonarDepthComponent = SimpleParametreComponent.empty;
        
    end
    
    methods
        
        function this = ConfigurationComponent(parentTab, configurationModel, pamesDialog)
            % Constructor of the class
            
            % Call SuperConstructor
            this = this@uiextras.HBoxFlex('Parent', parentTab, 'Spacing', 5);
            
            this.configurationModel = configurationModel;
            this.pamesDialog = pamesDialog;
            
            % icon left component (left component)
            % icon axes
            iconAxes = axes('Parent', this);
            %image incon
            icon = iconizeFic(getNomFicDatabase('CourtesyATLAS.png'));
            image(icon);
            iconAxes.set('Position', [0 0 1 1]);
            axis(iconAxes, 'equal');
            axis(iconAxes, 'off');
            
            % right component
            rightBox = uiextras.VBoxFlex('Parent', this, 'Spacing', 5);
            
            %Top box
            topBox = uiextras.VBox('Parent', rightBox, 'Spacing', 5);
            % sounder load box
            sounderLoadBox = uiextras.HBox('Parent', topBox, 'Spacing', 5);
            uicontrol('Parent', sounderLoadBox, ...
                'Style',   'pushbutton', ...
                'String',  'Load Sounder from an XML file', ...
                'Callback', @this.loadSounderCallback);
            
            uicontrol('Parent', sounderLoadBox, ...
                'Style',   'pushbutton', ...
                'String',  'Reset', ...
                'Callback', @this.resetCallback);
            
            this.modeRadioComponent = RadioButtonsComponent(topBox, 'orientation', RadioButtonsComponent.Orientation{1},...
                'title', 'Mode', 'titleButtons', this.configurationModel.modeList,...
                'Init', this.configurationModel.modeValue, 'callbackFunc',@this.modeRadioCallback);
            
            
            %% EchosounderPanel
            echosounderPanel = uipanel('Parent', rightBox, 'Title', 'Echosounder');
            echosounderPanelGrid = uiextras.Grid('Parent', echosounderPanel, 'Spacing', 5); % 2 column grid
            
            %column1
            
            uicontrol('Parent', echosounderPanelGrid, ...
                'Style',  'text', ...
                'String', 'Name : ');
            
            uicontrol('Parent', echosounderPanelGrid, ...
                'Style',  'text', ...
                'String', 'Type : ');
            
            uicontrol('Parent', echosounderPanelGrid, ...
                'Style',  'text', ...
                'String', 'Main Mode : ');
            
            uicontrol('Parent', echosounderPanelGrid, ...
                'Style',  'text', ...
                'String', 'Optional Mode : ');
            
            %column2
            this.sounderNameText = uicontrol('Parent', echosounderPanelGrid, 'Style',  'text', ...
                'String', this.configurationModel.sounderName);
            
            this.sounderTypePopupmenu = uicontrol('parent', echosounderPanelGrid, ...
                'Style',    'popupmenu', ...
                'String',   this.configurationModel.sounderTypeList, ...
                'Value',   this.configurationModel.sounderTypeValue, ...
                'Callback', @this.sounderTypeCallback);
            
            this.mainModePopupmenu = uicontrol('parent', echosounderPanelGrid, ...
                'Style',    'popupmenu', ...
                'String',   this.configurationModel.mode1List, ...
                'Value',   this.configurationModel.mode1Value, ...
                'Callback', @this.mainModeCallback);
            
            this.optModePopupmenu = uicontrol('parent', echosounderPanelGrid, ...
                'Style',    'popupmenu', ...
                'String',   this.configurationModel.mode2List, ...
                'Value',   this.configurationModel.mode2Value, ...
                'Callback', @this.optModeCallback);
            
            echosounderPanelGrid.set('ColumnSizes', [-1 -1], 'RowSizes', [25 25 25 25] );
            
%             %% shipPanel
%             shipPanel = uipanel('Parent', rightBox, 'Title', 'Ship / Fish / Vehicle');
%             shipVBox = uiextras.VBox('Parent', shipPanel, 'Spacing', 5);
%             
%             % Ship Name
%             shipNameBox = uiextras.HBox('Parent', shipVBox);
%             uicontrol('Parent', shipNameBox, ...
%                 'Style',  'text', ...
%                 'String', 'Ship''s Name : ');
%             this.shipNamePopupmenu = uicontrol('parent', shipNameBox, ...
%                 'Style',    'popupmenu', ...
%                 'String',   this.configurationModel.shipNameList, ...
%                 'Value',   this.configurationModel.shipNameValue, ...
%                 'Callback', @this.shipNameCallback);
%             
%             % Ship noise level
%             this.shipNoiseComponent = SimpleParametreComponent(shipVBox, 'param', this.configurationModel.selfNoiseParam, 'Sizes', [-2 -1 -1 0]);
%             % Add a listener for Value change event
%             addlistener(this.shipNoiseComponent, 'ValueChange', @this.paramValueChange);
%             
%             % Attitude Sensor
%             attitudeSensorBox = uiextras.HBox('Parent', shipVBox);
%             uicontrol('Parent', attitudeSensorBox, ...
%                 'Style',  'text', ...
%                 'String', 'Attitude Sensor : ');
%             this.attitudeSensorPopupmenu = uicontrol('parent', attitudeSensorBox, ...
%                 'Style',    'popupmenu', ...
%                 'String',   this.configurationModel.attitudeSensorList, ...
%                 'Value',   this.configurationModel.attitudeSensorValue, ...
%                 'Callback', @this.attitudeSensorCallback);
%             
%             % Sonard Depth
%             this.sonarDepthComponent = SimpleParametreComponent(shipVBox, 'param', this.configurationModel.immersionParam, 'Sizes', [-2 -1 -1 0]);
%             % Add a listener for Value change event
%             addlistener(this.sonarDepthComponent, 'ValueChange', @this.paramValueChange);
%             
%             shipVBox.set('Sizes', [25 25 25 25]);
%             rightBox.set('Sizes', [60 -1 -1]);

            rightBox.set('Sizes', [60 -1]);
        end
        
        function updateComponent(this)
            % Sounder
            this.sounderNameText.set('String', this.configurationModel.sounderName);
            this.sounderTypePopupmenu.set('String', this.configurationModel.sounderTypeList, 'Value', this.configurationModel.sounderTypeValue);
            this.mainModePopupmenu.set('String', this.configurationModel.mode1List, 'Value', this.configurationModel.mode1Value);
            this.optModePopupmenu.set('String', this.configurationModel.mode2List, 'Value', this.configurationModel.mode2Value);
            % this.opt2ModePopupmenu.set('String', this.configurationModel.mode3List, 'Value', this.configurationModel.mode3Value);
            
%             % Ship / Fish / Vehicle
%             this.shipNamePopupmenu.set('String', this.configurationModel.shipNameList, 'Value', this.configurationModel.shipNameValue);
%             this.shipNoiseComponent.updateComponent();
%             this.attitudeSensorPopupmenu.set('String', this.configurationModel.attitudeSensorList, 'Value', this.configurationModel.attitudeSensorValue);
%             this.sonarDepthComponent.updateComponent();
        end
    end
    
    methods (Access = private)
        
        %% Callbacks
        
        function loadSounderCallback(this, ~, ~)
            % load sounder xml file callback
            
            nomFic = getNomFicDatabase('EM300.xml');
            if ~exist(nomFic, 'file')
                my_warndlg('Directory not found !', 1);
                return
            end
            directoryPath = fileparts(nomFic);
            [flag, fileName] = my_uigetfile('*.xml', 'Load Sounder from an XML file', directoryPath);
            if ~flag
                return
            end
            
            if exist(fileName, 'file') % Si le fichier existe
                if isempty(fileName)
                    
                else % Cr�ation de l'instance cl_sounder
                    % Mouse watch
                    this.pamesDialog.parentFigure.set('pointer', 'watch');
                    drawnow;
                    
                    
                    sounder = cl_sounder('paramsFileName', fileName);
                    if sounder.IsGood == 1
                        
                        % Init models
                        this.pamesDialog.pamesModel.initModel();
                        
                        % Set sounder
                        this.pamesDialog.pamesModel.sounder = sounder;
                        
                        % Update models
                        this.pamesDialog.pamesModel.updateModel();
                        % Update components
                        this.pamesDialog.updateComponent();
                        
                        % Mouse arrow
                        this.pamesDialog.parentFigure.set('pointer', 'arrow');
                    else
                        my_warndlg('Wrong file format !', 1);
                    end
                end
            else
                my_warndlg('File doesn''t exist', 1);
            end
        end
        
        function resetCallback(this, ~, ~)
            % Mouse watch
            this.pamesDialog.parentFigure.set('pointer', 'watch');
            drawnow;
            % Init models
            this.pamesDialog.pamesModel.initModel();
            % Update models
            this.pamesDialog.pamesModel.updateModel();
            % Update components
            this.pamesDialog.updateComponent();
            
            % Mouse arrow
            this.pamesDialog.parentFigure.set('pointer', 'arrow');
        end
        
        function modeRadioCallback(this, src, ~)
            
            signalArraysComponent = this.pamesDialog.parametersComponent.signalArraysComponent;
            % set standard/expert mode (editable/non-editable) to signal/arrays components
            if strcmp(src.String, this.configurationModel.modeList{1})
                this.configurationModel.modeValue = 1;
                signalArraysComponent.setEditableSignalArraysComponents('off');
            elseif  strcmp(src.String, this.configurationModel.modeList{2})
                this.configurationModel.modeValue = 2;
                signalArraysComponent.setEditableSignalArraysComponents('on');
            end
        end
        
        
        function sounderTypeCallback(this, src, ~)
            this.configurationModel.sounderTypeValue = src.Value;
            % Update models & components
            this.pamesDialog.update();
        end
        
        function mainModeCallback(this, src, ~)
            this.configurationModel.mode1Value = src.Value;
            % Update models & components
            this.pamesDialog.update();
        end
        
        function optModeCallback(this, src, ~)
            this.configurationModel.mode2Value = src.Value;
            % Update models & components
            this.pamesDialog.update();
        end
       
        
        function paramValueChange(this, ~, ~)
            % Update models & components
            this.pamesDialog.update();
        end
    end
end
