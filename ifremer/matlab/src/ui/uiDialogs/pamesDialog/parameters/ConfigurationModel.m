classdef ConfigurationModel < handle
    %CONFIGURATIONPARAMTERSMODEL Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Constant)
        modeList = {'Standard', 'Expert'};
    end
    
    properties
        pamesModel
        
        %% Sounder
        modeValue        = 1;  % Mode value 1 = Standard /  2 = Expert)
        sounderName      = [];
        sounderTypeList  = {};
        sounderTypeValue = 23; % EM122
        mode1List        = {};
        mode1Value       = 1;
        mode2List        = {};
        mode2Value       = 4;
        
        previousSounderTypeValue = [] % previous sounder
        
        % TODO MHO v�rifier
        mode3List                = {};
        mode3Value               = 1;
        sounderAutoChoixSousMode = 0;
        sounderMaximumDepth      = [];
        sounderImmersion         = 0;
        
%         %TODO MHO FIXME in ConfigurationModel OR SignalArraysModel
%         signalFrequencyParam = ClParametre('Name', 'Frequency', 'Unit', 'kHz');
        
        %% Ship / Fish / Vehicle  used Environment & Accuracy
%         shipNameList        = num2navire; % From Environment
%         shipNameValue       = 1; % From Environment
%         selfNoiseParam      = ClParametre('Name', 'Ship''s noise level', 'Unit', 'dB/�Pa/sqrt(Hz)'); % From Environment
%         attitudeSensorList  = {}; % From Accuracy
%         attitudeSensorValue = []; % From Accuracy
%         immersionParam      = ClParametre('Name', 'Sonar depth', 'Unit', 'm'); % From Environment
    end
    
    methods
        function this = ConfigurationModel(pamesModel)
            this.pamesModel = pamesModel;
        end
        
        function initModel(this)
            %% Sounder
            this.modeValue        = 1;  % Mode value 1 = Standard /  2 = Expert)
            this.sounderName      = [];
            this.sounderTypeList  = {};
            this.sounderTypeValue = 23; % EM122
            this.mode1List        = {};
            this.mode1Value       = 1;
            this.mode2List        = {};
            this.mode2Value       = 4;
            
            this.previousSounderTypeValue = []; % previous sounder
            
            % TODO MHO v�rifier
            this.mode3List                = {};
            this.mode3Value               = 1;
            this.sounderAutoChoixSousMode = 0;
            this.sounderMaximumDepth      = [];
            this.sounderImmersion         = 0;
            
            %% Ship / Fish / Vehicle used Environment & Accuracy
%             this.shipNameList         = num2navire; %From Environment
%             this.shipNameValue        = 1; % From Environment
%             this.selfNoiseParam.Value = 0; % From Environment
%             this.attitudeSensorList   = {}; % From Accuracy
%             this.attitudeSensorValue  = []; % From Accuracy
%             this.immersionParam.Value = []; % From Environment
        end
        
        % cl_sounder is not handle a = a.set() mandatory
        function updateModel(this)
            
            % Get sounder
            sounder = this.pamesModel.sounder;
            
            if isempty(sounder)
                
                % Empty sounder object
                emptySounder =  cl_sounder();
                
                % Sounder Type
                this.sounderTypeList = emptySounder.get('SonarNames');
                if this.sounderTypeValue > length(this.sounderTypeList)
                    this.sounderTypeValue = 1;
                end
                emptySounder = emptySounder.set('Sonar.Ident', this.sounderTypeValue);
                
                % Sounder Main Mode
                this.mode1List =  emptySounder.get('strModes_1');
                if this.mode1Value > length(this.mode1List)
                    this.mode1Value = 1;
                end
                emptySounder = emptySounder.set('Sonar.Mode_1', this.mode1Value);
                
                % Sounder Optional Mode
                this.mode2List =  emptySounder.get('strModes_2');
                if this.mode2Value > length(this.mode2List)
                    this.mode2Value = 1;
                end
                emptySounder = emptySounder.set('Sonar.Mode_2', this.mode2Value);
                
                % Sounder Name
                if this.modeValue == 1 % Standard Mode
                    this.sounderName = [this.sounderTypeList{this.sounderTypeValue} ' ' this.mode1List{this.mode1Value}];
                    if ~isempty(this.mode2List) && ~isempty(this.mode2Value)
                        this.sounderName = [this.sounderName ' ' this.mode2List{this.mode2Value}];
                    end
                elseif this.modeValue == 2 % Expert Mode
                    % TODO ???
                end
                
                % Mode Automatique (Mode prefere) %TODO metre ce bloc
                % juste au dessus de sounder Name???
                if this.sounderAutoChoixSousMode
                    emptySounder = emptySounder.set('Sonar.Mode_2', 0);
                    this.mode2Value = emptySounder.get('strModes_2');
                end
                
                % Sounder Freq & Maximum Depth
                if this.modeValue % CurrentSystem %TODO signalArrayModel???
%                     this.signalFrequencyParam.Value = emptySounder.get('Sonar.Freq');
                    this.sounderMaximumDepth = emptySounder.get('MaxDepth');
                end
                
                %                     if b.Sounder.IdentSounder ~= b.Sounder.IdentSounderPrecedent
                %         b.Fauchee.MaxDepth = params.MaximumDepth;
                %     end
                % FIXME TODO
                this.previousSounderTypeValue = this.sounderTypeValue;      
            else
                

                
                % Sounder Type
                this.sounderTypeList = sounder.get('SonarNames');
                this.sounderTypeValue = sounder.get('SonarIdent');
                if this.sounderTypeValue > length(this.sounderTypeList)
                    this.sounderTypeValue = 1;
                end
                
                % TODO maj d=qui ne sert a rien?????
                %                 %                             %% Mode 1, 2 et 3 --> mise � jour de l'objet cl_sounder
                %                                  [mode1, mode2, mode3] = sounder.get('Sonar.Mode_1', 'Sonar.Mode_2', 'Sonar.Mode_3');
                %
                %                                        %TODO mode1 2 & 3 n'existe pas???
                %                                     mode1 = [mode1, 1];
                %                                     mode1 = mode1(1);     % si vide, mise � 1;
                %
                %                                     mode2 = [mode2, 1];
                %                                     mode2 = mode2(1);     % si vide, mise � 1;
                %
                %                                     mode3 = [mode3, 1];
                %                                     mode3 = mode3(1);     % si vide, mise � 1;
                %
                %                                  if mode1 ~= this.mode1Value || mode2 ~= this.mode2Value || mode3 ~= this.mode3Value
                %                                         if mode1 ~= this.mode1Value && mode2 ~= this.mode2Value && mode3 ~= this.mode3Value
                %                                             sounder = sounder.set('Sonar.Mode_1', this.mode1Value,...
                %                                                 'Sonar.Mode_2', this.mode2Value ,...
                %                                                 'Sonar.Mode_3', this.mode3Value);
                %                                         elseif mode1 ~= this.mode1Value && mode2 ~= this.mode2Value
                %                                             sounder = sounder.set('Sonar.Mode_1', this.mode1Value,...
                %                                                 'Sonar.Mode_2', this.mode2Value  );
                %                                         elseif mode1 ~= this.mode1Value && mode3 ~= this.mode3Value
                %                                             sounder = sounder.set('Sonar.Mode_1', this.mode1Value,...
                %                                                 'Sonar.Mode_3', this.mode3Value  );
                %                                         elseif mode3 ~= this.mode3Value && mode2 ~= this.mode2Value
                %                                             sounder = sounder.set('Sonar.Mode_3', this.mode3Value,...
                %                                                 'Sonar.Mode_2', this.mode2Value  );
                %                                         elseif mode1 ~= this.mode1Value
                %                                             sounder = sounder.set('Sonar.Mode_1', this.mode1Value);
                %                                         elseif mode2 ~= this.mode2Value
                %                                             sounder = sounder.set('Sonar.Mode_2', this.mode2Value);
                %                                         elseif mode3 ~= this.mode3Value
                %                                             sounder = sounder.set('Sonar.Mode_3', this.mode3Value);
                %                                         end
                %                                     end
                
                
                % Sounder Main Mode List
                this.mode1List =  sounder.get('strModes_1');
                this.mode1List = strcat(this.mode1List, ' '); % Pour �viter une liste vide
                
                % Mode Automatique (Mode prefere)
                if this.sounderAutoChoixSousMode
                    this.mode2Value = sounder.get('Sonar.Mode_2');
                end
                
                if this.mode1Value > length(this.mode1List)
                    this.mode1Value = 1;
                end
                
                % Sounder Optional Mode
                this.mode2List =  sounder.get('strModes_2');
                this.mode2List = strcat(this.mode2List, ' '); % Pour �viter une liste vide
                if this.mode2Value > length(this.mode2List)
                    this.mode2Value = 1;
                end
                
                % Sounder Optional 2 Mode
                this.mode3List =  sounder.get('strModes_3');
                this.mode3List = strcat(this.mode3List, ' '); % Pour �viter une liste vide
                if this.mode3Value > length(this.mode3List)
                    this.mode3Value = 1;
                end
                
                % Sounder Name
                if this.modeValue == 1 % Standard Mode
                    this.sounderName = [sounder.get('SonarName') ' ' this.mode1List{this.mode1Value}];
                    if ~isempty(this.mode2List) && ~isempty(this.mode2Value)
                        this.sounderName = [this.sounderName ' ' this.mode2List{this.mode2Value}];
                    end
                elseif this.modeValue == 2 % Expert Mode
                    % TODO ???
                end
                
                if this.sounderTypeValue ~= this.previousSounderTypeValue
                    %TODO
                    %                       b.Fauchee.MaxDepth = get(b.cl_sounder, 'MaxDepth');
                end
                
                this.previousSounderTypeValue = this.sounderTypeValue;
                
                if this.modeValue == 1
                    
%                     % Sounder Freq
%                     this.signalFrequencyParam.Value = sounder.get('Sonar.Freq');
                    % Sounder Maximum Depth
                    this.sounderMaximumDepth = sounder.get('MaxDepth');
                    if isempty(this.sounderMaximumDepth)
                        this.sounderMaximumDepth = 1000; %m
                    end
                end
            end
        end
    end
end
