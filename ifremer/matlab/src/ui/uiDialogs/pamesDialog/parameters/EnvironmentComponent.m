classdef EnvironmentComponent < handle & uiextras.VBoxFlex
    %EnvironmentComponent Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        environmentModel
        pamesDialog % PamesDialog
    end
    
    properties (Access = private)
        %% Components
        freqComponent
        
        %% Absorption Components
        absorptionCoeffBox
        absorptionCoeffTypePopup
        bubbleRadioButtonsComponent
        
        % Constant depth
        constantDepthBox
        
        absorptionTypePopup
        absorptionCoefficientComponent
        averageTemperatureComponent
        averageSalinityComponent
        averageDepthComponent
        
        %varying
        varyingDepthBox
        oceanPopup
        seasonPopup
        absorptionCoeffSurfaceComponent
        absorptionCoeffBottomComponent
        absorptionCoeffMeanComponent
                
        %% Noiuse Components
        noiseTypePopup
        totalNoiseComponent
        noiseBox
        seaStateComponent
        thermalNoiseComponent
        surfaceNoiseComponent
        % Ship / Fish / Vehicle
        shipNamePopupmenu
        shipNoiseComponent  = SimpleParametreComponent.empty;
        sonarDepthComponent = SimpleParametreComponent.empty;
        
        %% SeaFloor
        seaFloorCoeff
    end
    
    methods
        
        function this = EnvironmentComponent(parentTab, environmentModel, pamesDialog)
            % Constructor of the class
            
            % Call SuperConstructor
            this = this@uiextras.VBoxFlex('Parent', parentTab);
            
            this.environmentModel = environmentModel;
            this.pamesDialog = pamesDialog;
            
            % Frequence
            freqBox = uiextras.HBox('Parent', this);
            this.freqComponent = SimpleParametreComponent(freqBox, 'param', this.pamesDialog.pamesModel.signalArraysModel.signalFrequencyParam, 'editable', 'off');
            uiextras.Empty('Parent', freqBox);
            freqBox.set('Sizes', [400 -1]);
            
            hBox = uiextras.HBox('Parent', this, 'Spacing', 5);
            % create AbsorptionCoefParameters Panel
            this.createAbsorptionCoefParametersPanel(hBox);
            % create NoiseLevelParameters Panel
            this.createNoiseLevelParametersPanel(hBox);
            % create SeafloorParameters Panel
            this.createSeafloorParametersPanel(this);
            
            this.set('Sizes', [25 280 -1]);
        end
        
        function updateComponent(this)
            
            this.freqComponent.updateComponent();
            
            %% Absorption Components
            this.absorptionCoeffTypePopup.set('Value', this.environmentModel.absorptionCoeffTypePopupValue);
            
            
            
            
            if (this.environmentModel.absorptionCoeffTypePopupValue ==1)
                
                this.constantDepthBox.set('Visible', 'on');
                this.varyingDepthBox.set('Visible', 'off');
                
                this.absorptionTypePopup.set('Value', this.environmentModel.absorptionTypePopupValue);
                
                this.absorptionCoefficientComponent.updateComponent();
                
                % Show/hide components depending absorptionTypePopup
                if this.environmentModel.absorptionTypePopupValue ==1
                    this.absorptionCoefficientComponent.setEditable('off');
                    
                    this.averageTemperatureComponent.set('Visible', 'on');
                    this.averageTemperatureComponent.updateComponent();
                    this.averageSalinityComponent.set('Visible', 'on');
                    this.averageSalinityComponent.updateComponent();
                    this.averageDepthComponent.set('Visible', 'on');
                    this.averageDepthComponent.updateComponent();
                else
                    this.absorptionCoefficientComponent.setEditable('on');
                    
                    this.averageTemperatureComponent.set('Visible', 'off');
                    this.averageSalinityComponent.set('Visible', 'off');
                    this.averageDepthComponent.set('Visible', 'off');
                end
                
                this.absorptionCoeffBox.set('Sizes', [25 -1 0 25]);
            else
                this.constantDepthBox.set('Visible', 'off');
                this.varyingDepthBox.set('Visible', 'on');
                this.oceanPopup.set('Value', this.environmentModel.oceanValue);
                this.seasonPopup.set('Value', this.environmentModel.seasonValue);
                this.absorptionCoeffSurfaceComponent.updateComponent();
                this.absorptionCoeffBottomComponent.updateComponent();
                this.absorptionCoeffMeanComponent.updateComponent();
                
                this.absorptionCoeffBox.set('Sizes', [25 0 -1 25]);
            end
            
            this.bubbleRadioButtonsComponent.updateComponent(this.environmentModel.bubbleValue);
            
            %% Noise Components
            this.noiseTypePopup.set('Value', this.environmentModel.noiseTypePopupValue);
            
            this.totalNoiseComponent.updateComponent();
            if this.environmentModel.noiseTypePopupValue ==1
                this.totalNoiseComponent.setEditable('off');
                this.noiseBox.set('Visible', 'on');
                
                this.seaStateComponent.updateComponent();
                this.thermalNoiseComponent.updateComponent();
                this.surfaceNoiseComponent.updateComponent();
                % Ship / Fish / Vehicle
                this.shipNamePopupmenu.set('String', this.environmentModel.shipNameList, 'Value', this.environmentModel.shipNameValue);
                this.shipNoiseComponent.updateComponent();
                this.sonarDepthComponent.updateComponent();
                
            else
                this.totalNoiseComponent.setEditable('on');
                this.noiseBox.set('Visible', 'off');
            end
            
            this.totalNoiseComponent.updateComponent();
            this.seaStateComponent.updateComponent();
            this.thermalNoiseComponent.updateComponent();
            this.surfaceNoiseComponent.updateComponent();
            % Ship / Fish / Vehicle
            this.shipNamePopupmenu.set('String', this.environmentModel.shipNameList, 'Value', this.environmentModel.shipNameValue);
            this.shipNoiseComponent.updateComponent();
            this.sonarDepthComponent.updateComponent();
            
            %% SeaFloor coeffs
            
            for columnIndex = 1:4
                seaFloorIndex =  this.environmentModel.selectedSeaFloors(columnIndex);
                if seaFloorIndex==4
                    enable = 'on';
                else
                    enable = 'off';
                end
                for lineIndex = 1:4
                    this.seaFloorCoeff(columnIndex, lineIndex).set( 'String', this.environmentModel.lambertParams(seaFloorIndex,lineIndex), 'Enable', enable);
                end
            end
        end
    end
    
    methods (Access = private)
        
        function createAbsorptionCoefParametersPanel(this, parentBox)
            % create AbsorptionCoefParameters Panel
            
            absorptionCoefPanel = uipanel('Parent', parentBox, 'Title', 'Absorption coefficient');
            
            this.absorptionCoeffBox = uiextras.VBox('Parent', absorptionCoefPanel, 'Spacing', 5);
            
            % absorption Type Popup
            absorptionCoeffTypePopupNameList = {'Constant' , 'Profile'};
            this.absorptionCoeffTypePopup = uicontrol('parent', this.absorptionCoeffBox, ...
                'Style',    'popupmenu', ...
                'String',   absorptionCoeffTypePopupNameList , ...
                'Value',    this.environmentModel.absorptionCoeffTypePopupValue, ...
                'Callback', @this.absorptionCoeffTypeCallback);
            
            %% ConstantDepth
            
            this.constantDepthBox = uiextras.VBox('Parent', this.absorptionCoeffBox, 'Spacing', 5);
            
            % Compute Popup
            absorptionTypePopupNameList = {'Computed' , 'Imposed'};
            this.absorptionTypePopup = uicontrol('parent', this.constantDepthBox, ...
                'Style',    'popupmenu', ...
                'String',   absorptionTypePopupNameList , ...
                'Value',    this.environmentModel.absorptionTypePopupValue, ...
                'Callback', @this.absorptionTypeCallback);
            
            % Parametre
            % Absorption Coeeficient
            this.absorptionCoefficientComponent = SimpleParametreComponent(this.constantDepthBox, 'param', this.environmentModel.absorptionCoefficientParam, 'editable', 'off');
            addlistener(this.absorptionCoefficientComponent, 'ValueChange', @this.paramValueChange);
            % Average Temperature
            this.averageTemperatureComponent = SimpleParametreComponent(this.constantDepthBox, 'param', this.environmentModel.averageTemperatureParam, 'editable', 'on');
            addlistener(this.averageTemperatureComponent, 'ValueChange', @this.paramValueChange);
            % Average Salinity
            this.averageSalinityComponent = SimpleParametreComponent(this.constantDepthBox, 'param', this.environmentModel.averageSalinityParam, 'editable', 'on');
            addlistener(this.averageSalinityComponent, 'ValueChange', @this.paramValueChange);
            % Average Water Depth
            this.averageDepthComponent = SimpleParametreComponent(this.constantDepthBox, 'param', this.environmentModel.averageDepthParam, 'editable', 'on');
            addlistener(this.averageDepthComponent, 'ValueChange', @this.paramValueChange);
            
            this.constantDepthBox.set('Sizes', 25*ones(1,5));
            
            %% Varying Depth
            
            this.varyingDepthBox = uiextras.VBox('Parent', this.absorptionCoeffBox, 'Spacing', 5);
            
            oceanBox =  uiextras.HBox('Parent',  this.varyingDepthBox, 'Spacing', 5);
            uicontrol('Parent', oceanBox, 'Style',  'text', 'String', 'Ocean ');
            % Ocean Popup
            this.oceanPopup = uicontrol('parent', oceanBox, ...
                'Style',    'popupmenu', ...
                'String',   this.environmentModel.oceanList , ...
                'Value',    this.environmentModel.oceanValue, ...
                'Callback', @this.oceanCallback);
            
            seasonBox =  uiextras.HBox('Parent',  this.varyingDepthBox, 'Spacing', 5);
            uicontrol('Parent', seasonBox, 'Style',  'text', 'String', 'Ocean ');
            % Season Popup
            this.seasonPopup = uicontrol('parent', seasonBox, ...
                'Style',    'popupmenu', ...
                'String',   this.environmentModel.seasonList , ...
                'Value',    this.environmentModel.seasonValue, ...
                'Callback', @this.seasonCallback);
            
            % Parametre
            % Absorption Coeeficient Surface
            this.absorptionCoeffSurfaceComponent = SimpleParametreComponent(this.varyingDepthBox, 'param', this.environmentModel.absorptionCoeffSurfaceParam, 'editable', 'off');
            addlistener(this.absorptionCoeffSurfaceComponent, 'ValueChange', @this.paramValueChange);
            % Absorption Coeeficient Bottom
            this.absorptionCoeffBottomComponent = SimpleParametreComponent(this.varyingDepthBox, 'param', this.environmentModel.absorptionCoeffBottomParam, 'editable', 'off');
            addlistener(this.absorptionCoeffBottomComponent, 'ValueChange', @this.paramValueChange);
            % Absorption Coeeficient Mean
            this.absorptionCoeffMeanComponent = SimpleParametreComponent(this.varyingDepthBox, 'param', this.environmentModel.absorptionCoeffMeanParam, 'editable', 'off');
            addlistener(this.absorptionCoeffMeanComponent, 'ValueChange', @this.paramValueChange);
            
            this.varyingDepthBox.set('Sizes', 25*ones(1,5));
            %%
            
            this.bubbleRadioButtonsComponent = RadioButtonsComponent(this.absorptionCoeffBox, 'orientation', RadioButtonsComponent.Orientation{1}, ...
                'title', 'Bubble Layer Effect', 'titleButtons', this.environmentModel.bubbleList, 'Init', this.environmentModel.bubbleValue,...
                'callbackFunc',@this.bubbleCallback);
            
            if this.environmentModel.absorptionCoeffTypePopupValue == 1
                this.absorptionCoeffBox.set('Sizes', [25 -1 0 25]);
            else
                this.absorptionCoeffBox.set('Sizes', [25 0 -1 25]);
            end
            
        end
        
        function createNoiseLevelParametersPanel(this,parentBox)
            % create NoiseLevelParameters Panel
            noiseLevelPanel = uipanel('Parent', parentBox, 'Title', 'Noise level');
            
            vBox = uiextras.VBox('Parent', noiseLevelPanel, 'Spacing', 5);
            
            % Compute Popup
            noiseTypePopupNameList = {'Computed' , 'Imposed'};
            this.noiseTypePopup = uicontrol('parent', vBox, ...
                'Style',    'popupmenu', ...
                'String',   noiseTypePopupNameList , ...
                'Value',    this.environmentModel.noiseTypePopupValue, ...
                'Callback', @this.noiseTypeCallback);
            
            %% Parametre
            % Total Noise Level
            this.totalNoiseComponent = SimpleParametreComponent(vBox, 'param', this.environmentModel.totalNoiseLevelParam, 'editable', 'off');
            addlistener(this.totalNoiseComponent, 'ValueChange', @this.paramValueChange);
            
            this.noiseBox = uiextras.VBox('Parent', vBox, 'Spacing', 5);
            % Sea State Level
            this.seaStateComponent = SimpleParametreComponent(this.noiseBox, 'param', this.environmentModel.seaStateParam);
            addlistener(this.seaStateComponent, 'ValueChange', @this.paramValueChange);
            % Thermal Noise Level
            this.thermalNoiseComponent = SimpleParametreComponent(this.noiseBox, 'param', this.environmentModel.thermalNoiseParam, 'editable', 'off');
            addlistener(this.thermalNoiseComponent, 'ValueChange', @this.paramValueChange);
            % Surface Noise Level
            this.surfaceNoiseComponent = SimpleParametreComponent(this.noiseBox, 'param', this.environmentModel.surfaceNoiseParam, 'editable', 'off');
            addlistener(this.surfaceNoiseComponent, 'ValueChange', @this.paramValueChange);
            
            this.noiseBox.set('Sizes', [25 25 25]);
            
            %% shipPanel
            shipPanel = uipanel('Parent', this.noiseBox, 'Title', 'Ship / Fish / Vehicle');
            shipVBox = uiextras.VBox('Parent', shipPanel, 'Spacing', 5);
            
            % Ship Name
            shipNameBox = uiextras.HBox('Parent', shipVBox);
            uicontrol('Parent', shipNameBox, ...
                'Style',  'text', ...
                'String', 'Ship''s Name : ');
            this.shipNamePopupmenu = uicontrol('parent', shipNameBox, ...
                'Style',    'popupmenu', ...
                'String',   this.environmentModel.shipNameList, ...
                'Value',   this.environmentModel.shipNameValue, ...
                'Callback', @this.shipNameCallback);
            
            % Ship noise level
            this.shipNoiseComponent = SimpleParametreComponent(shipVBox, 'param', this.environmentModel.selfNoiseParam, 'Sizes', [-2 -1 -1 0]);
            % Add a listener for Value change event
            addlistener(this.shipNoiseComponent, 'ValueChange', @this.paramValueChange);
            
            % Sonard Depth
            this.sonarDepthComponent = SimpleParametreComponent(shipVBox, 'param', this.environmentModel.immersionParam, 'Sizes', [-2 -1 -1 0]);
            % Add a listener for Value change event
            addlistener(this.sonarDepthComponent, 'ValueChange', @this.paramValueChange);
            
            shipVBox.set('Sizes', [25 25 25]);
            vBox.set(    'Sizes', [25 25 -1]);
        end
        
        function createSeafloorParametersPanel(this, parentBox)
            % create SeafloorParameters Panel
            
            seafloorPanel = uipanel('Parent', parentBox, 'Title', 'Seafloor');
            
            vBox = uiextras.VBox('Parent', seafloorPanel, 'Spacing', 5);
            
            % Model
            modelBox =  uiextras.HBox('Parent', vBox, 'Spacing', 5);
            uicontrol('Parent', modelBox, 'Style',  'text', 'String', 'Model : ');
            uicontrol('Parent', modelBox, 'Style',  'text', 'String', 'Specular + Lambert');
            uiextras.Empty('Parent', modelBox);
            modelBox.set('Sizes', [100 100 -1]);
            
            coeffGrid = uiextras.Grid('Parent', vBox, 'Spacing', 1); % 5 column grid
            
            %column1 label coeff
            uiextras.Empty('Parent', coeffGrid);
            uicontrol('Parent', coeffGrid, 'Style',  'text', 'String', 'Specular Level (dB)');
            uicontrol('Parent', coeffGrid, 'Style',  'text', 'String', 'Specular width (deg)');
            uicontrol('Parent', coeffGrid, 'Style',  'text', 'String', 'Lambert ref. (dB)');
            uicontrol('Parent', coeffGrid, 'Style',  'text', 'String', 'Lambert decr.');
            
            %column 1:4 (Default = Rock, Sand, Mud, Other)
            this.seaFloorCoeff = gobjects(4, 4);
            for columnIndex = 1:4
                seaFloorIndex =  this.environmentModel.selectedSeaFloors(columnIndex);
                uicontrol('parent', coeffGrid, ...
                    'Style',    'popupmenu', ...
                    'String',   this.environmentModel.seaFloorList, ...
                    'Value',  seaFloorIndex , ...
                    'Callback', @(src,eventdata)floorCallback(this,src,eventdata,seaFloorIndex));
                
                if seaFloorIndex==4
                    enable = 'on';
                else
                    enable = 'off';
                end
                for lineIndex = 1:4
                    this.seaFloorCoeff(columnIndex, lineIndex) = uicontrol('Parent', coeffGrid, 'Style', 'edit', 'String', this.environmentModel.lambertParams(columnIndex, lineIndex), 'Enable', enable, 'Callback', @(src,eventdata)coeffCallback(this,src,eventdata,seaFloorIndex, lineIndex));
                end
            end
            
            coeffGrid.set('ColumnSizes', [-1 -1 -1 -1 -1], 'RowSizes', [25 25 25 25 25] );
            
            buttonBox =  uiextras.HBox('Parent', vBox, 'Spacing', 5);
            uiextras.Empty('Parent', buttonBox);
            uicontrol('Parent', buttonBox, ...
                'Style',   'pushbutton', ...
                'String',  'Show selected', ...
                'Callback', @this.plotModelCallback);
            
            buttonBox.set('Sizes', [-1 200]);
            vBox.set(     'Sizes', [25 -1 25]);
        end
        
        %% Callbacks
        
        function absorptionCoeffTypeCallback(this, src, ~)
            this.environmentModel.absorptionCoeffTypePopupValue = src.Value;
            % Update models & components
            this.pamesDialog.update();
        end
        
        function absorptionTypeCallback(this, src, ~)
            this.environmentModel.absorptionTypePopupValue = src.Value;
            
            % Update models & components
            this.pamesDialog.update();
        end
        
        function oceanCallback(this, src, ~)
            this.environmentModel.oceanValue = src.Value;
            % Update models & components
            this.pamesDialog.update();
        end
        
        function seasonCallback(this, src, ~)
            this.environmentModel.seasonValue = src.Value;
            % Update models & components
            this.pamesDialog.update();
        end
        
        function bubbleCallback(this, src, ~)
            if strcmp(src.String, this.environmentModel.bubbleList{1})
                this.environmentModel.bubbleValue = 1;
            elseif  strcmp(src.String, this.environmentModel.bubbleList{2})
                this.environmentModel.bubbleValue = 2;
            end
            
            % Update models & components
            this.pamesDialog.update();
        end
        
        function noiseTypeCallback(this, src, ~)
            this.environmentModel.noiseTypePopupValue = src.Value;
            % Update models & components
            this.pamesDialog.update();
            
            % TODO switch ihm computed / imposed
        end
        
        function shipNameCallback(this, src, ~)
            this.environmentModel.shipNameValue = src.Value;
            % Update models & components
            this.pamesDialog.update();
        end
        
        function paramValueChange(this, ~, ~)
            % Update models & components
            this.pamesDialog.update();
        end
        
        function floorCallback(this, src, ~, k)
            this.environmentModel.selectedSeaFloors(k) = src.Value;
            % Update models & components
            this.updateComponent();
        end
        
        function coeffCallback(this, src, ~, columnIndex, lineIndex)
            seaFloorIndex =  this.environmentModel.selectedSeaFloors(columnIndex);
            if (seaFloorIndex == 4) % Other
                this.environmentModel.lambertParams(seaFloorIndex, lineIndex) = str2num(src.String);
                % Update models & components
                this.pamesDialog.update();
            end
        end
        
        function plotModelCallback(this, ~, ~)
            % Plot the BSLurton Models
            
            angles = -80:0.25:80;
            
            % lambertParams
            for seaFloorIndex=4:-1:1
                lambertParam = this.environmentModel.lambertParams(seaFloorIndex, :);
                %                 lambertParamsFixed(2) = lambertParamsFixed(2)/2;
                bsLurtonModel(seaFloorIndex) = BSLurtonModel('XData', angles, 'ValParams', [lambertParam -100 10 0]);
            end
            [axe, ~, ~] = bsLurtonModel.plot('DrawComponents', 0, 'DefaultColor', true, ...
                'Title', 'Specular + Lambert''s Model', 'NoData', 1);
            % Add legend
            legend(axe, this.environmentModel.seaFloorList(this.environmentModel.selectedSeaFloors), 'Interpreter', 'none');
        end
    end
end
