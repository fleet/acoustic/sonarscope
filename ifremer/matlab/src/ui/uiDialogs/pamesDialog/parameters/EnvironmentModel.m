classdef EnvironmentModel < handle
    %EnvironmentModel Summary of this class goes here
    %   Detailed explanation goes here
    
    
    properties (Constant)
        bubbleList   = {'No', 'Yes'};
        oceanList    = {'BATHYCEL_North-East_Atlantic', 'BATHYCEL_Equatorial_Atlantic', 'BATHYCEL_Norwegian_Sea', 'BATHYCEL_Mediterranean'};
        seasonList   = {'Winter', 'Spring', 'Summer', 'Autumn'};
        seaFloorList = {'Rock', 'Sand', 'Mud', 'Other'};
    end
    
    properties
        
        pamesModel
        
        %% Abosrption
        absorptionCoeffTypePopupValue = 1; % Constant  Depth / Varying Depth
        
        % Constant  Depth
        absorptionTypePopupValue   = 1; % Computed / Imposed
        absorptionCoefficientParam = ClParametre('Name', 'Absorption Coefficient', 'Unit', 'dB/Km');
        averageTemperatureParam    = ClParametre('Name', 'Temperature', 'Unit', '�C', 'Value', 4);
        averageSalinityParam       = ClParametre('Name', 'Salinity', 'Unit', 'ppt', 'Value', 35);
        averageDepthParam          = ClParametre('Name', 'Water Depth', 'Unit', 'm');
        
        % Varying Depth
        oceanValue = 1;
        seasonValue = 1;
        absorptionCoeffSurfaceParam = ClParametre('Name', 'Absorption Coefficient (surface)', 'Unit', 'dB/Km', 'Value', 0);
        absorptionCoeffBottomParam  = ClParametre('Name', 'Absorption Coefficient (bottom)', 'Unit', 'dB/Km', 'Value', 0);
        absorptionCoeffMeanParam    = ClParametre('Name', 'Absorption Coefficient (mean)', 'Unit', 'dB/Km', 'Value', 0);
        
        bubbleValue             = 1; % No / Yes
        modeCalculBubbleBool    = true;
        bubbleTransmissionLoss  = 0;
        
        levitusDepth    = [];
        levitusAlpha    = [];
        levitusAlphaSum = [];
        
        %% Noise Level
        
        noiseTypePopupValue = 1; % Computed / Imposed
        totalNoiseLevelParam  = ClParametre('Name', 'Total Noise Level', 'Unit', 'dB/�Pa/sqrt(Hz)',   'Value', 0);
        % Computed
        seaStateParam         = ClParametre('Name', 'Sea State',          'Unit', 'Beaufort',         'Value', 4);
        thermalNoiseParam     = ClParametre('Name', 'Thermal Noise',      'Unit', 'dB/�Pa/sqrt(Hz)');
        surfaceNoiseParam     = ClParametre('Name', 'Surface Noise',      'Unit', 'dB/�Pa/sqrt(Hz)');
        % From configuration to Environment
        shipNameList        = num2navire;
        shipNameValue       = 1;
        selfNoiseParam      = ClParametre('Name', 'Ship''s noise level', 'Unit', 'dB/�Pa/sqrt(Hz)');
        immersionParam      = ClParametre('Name', 'Sonar depth', 'Unit', 'm');
        
        
        %% Sea Floor
        
        modelPopupValue = 1; % Specular + Lambert / Jackson (Jackson not implemented)
        
        bSLurtonModel = BSLurtonModel();
        selectedSeaFloors = [1 2 3 4];
        
        
        lambertCoeffRock  = [-12 10 -20 2];
        lambertCoeffSand  = [-15 5 -30 2];
        lambertCoeffMud   = [-10 2 -40 2];
        lambertCoeffOther = [-25 10 -50 2];
        lambertParams % = [lambertCoeffRock; lambertCoeffSand; lambertCoeffMud; lambertCoeffOther]
    end
    
    properties (Access = private)
        nameHtmlTransmissionLoss = ['file://' fullfile(getNomFicDatabase('transmission_loss.html'))];
        nameHtmlNoiseLevel = ['file://' fullfile(getNomFicDatabase('noise_level.html'))];
    end
    
    methods
        function this = EnvironmentModel(pamesModel)
            this.pamesModel = pamesModel;
            
            % Set help link
            this.absorptionCoefficientParam.Help  = this.nameHtmlTransmissionLoss;
            this.absorptionCoeffSurfaceParam.Help  = this.nameHtmlTransmissionLoss;
            this.absorptionCoeffBottomParam.Help  = this.nameHtmlTransmissionLoss;
            this.absorptionCoeffMeanParam.Help  = this.nameHtmlTransmissionLoss;
            
            this.seaStateParam.Help  = this.nameHtmlNoiseLevel;
            this.thermalNoiseParam.Help  = this.nameHtmlNoiseLevel;
            this.surfaceNoiseParam.Help  = this.nameHtmlNoiseLevel;
            this.totalNoiseLevelParam.Help  = this.nameHtmlNoiseLevel;
            this.selfNoiseParam.Help  = this.nameHtmlNoiseLevel;
        end
        
        function initModel(this)
            
            %% Abosrption
            
            this.absorptionCoeffTypePopupValue = 1; % Constant  Depth / Varying Depth
            
            % Constant  Depth
            this.absorptionTypePopupValue         = 1; % Computed / Imposed
            this.absorptionCoefficientParam.Value = [];
            this.averageTemperatureParam.Value    = 4;
            this.averageSalinityParam.Value       = 35;
            this.averageDepthParam.Value          = [];
            
            % Varying Depth
            %         oceanList;
            this.oceanValue                        = 1;
            %         seasonList
            this.seasonValue                       = 1;
            this.absorptionCoeffSurfaceParam.Value = 0;
            this.absorptionCoeffBottomParam.Value  = 0;
            this.absorptionCoeffMeanParam.Value    = 0;
            
            
            this.bubbleValue                       = 1; % No / Yes
            this.modeCalculBubbleBool              = true;
            this.bubbleTransmissionLoss            = 0;
            
            this.levitusDepth                      = [];
            this.levitusAlpha                      = [];
            this.levitusAlphaSum                   = [];
            
            %% Noise Level
            
            this.noiseTypePopupValue               = 1; % Computed / Imposed
            this.totalNoiseLevelParam.Value        = 0;
            % Computed
            this.seaStateParam.Value               = 4;
            this.thermalNoiseParam.Value           = [];
            this.surfaceNoiseParam.Value           = [];
            % From configuration to Environment
            this.shipNameList         = num2navire;
            this.shipNameValue        = 1;
            this.selfNoiseParam.Value = 0;
            this.immersionParam.Value = [];
            
            
            %% Sea Floor
            
            this.modelPopupValue = 1; % Specular + Lambert / Jackson
            
            this.bSLurtonModel     = BSLurtonModel();
            this.selectedSeaFloors = [1 2 3 4];
            
            this.lambertCoeffRock  = [-12 10 -20 2];
            this.lambertCoeffSand  = [-15 5 -30 2];
            this.lambertCoeffMud   = [-10 2 -40 2];
            this.lambertCoeffOther = [-25 10 -50 2];
            this.lambertParams     = [this.lambertCoeffRock; this.lambertCoeffSand; this.lambertCoeffMud; this.lambertCoeffOther];
        end
        
        function updateModel(this)
            
            % Get params of others models
            sounder = this.pamesModel.sounder;
            freq = this.pamesModel.signalArraysModel.signalFrequencyParam.Value;
            modeValue = this.pamesModel.configurationModel.modeValue;
            sounderTypeValue = this.pamesModel.configurationModel.sounderTypeValue;
            maxDepth = this.pamesModel.configurationModel.sounderMaximumDepth;
            immersion = this.pamesModel.configurationModel.sounderImmersion;
            
            %% Abosrption
            
            if isempty(this.averageDepthParam.Value)
                this.averageDepthParam.Value = maxDepth / 2; % Depend du sondeur
            end
            
            if this.absorptionCoeffTypePopupValue == 1
                if this.absorptionTypePopupValue == 1
                    [~, AlphaMoyen] = this.attenuationGarrison(freq, ...
                        this.averageDepthParam.Value, ...
                        this.averageTemperatureParam.Value, ...
                        this.averageSalinityParam.Value, ...
                        'Immersion', immersion);
                    this.absorptionCoefficientParam.Value = AlphaMoyen;
                end
            else
                
                objCoundSpeed = cl_sound_speed('Frequency', freq);
                set(objCoundSpeed, 'Ocean', this.oceanValue, 'Season', this.seasonValue, ...
                    'SonarDepth', immersion);
                
                this.levitusDepth    = get(objCoundSpeed, 'Depth');
                this.levitusAlpha    = get(objCoundSpeed, 'LocalAbsorption');
                this.levitusAlphaSum = get(objCoundSpeed, 'AveragedAbsorption');
                this.absorptionCoeffSurfaceParam.Value = this.levitusAlphaSum(1);
                this.absorptionCoeffBottomParam.Value  = this.levitusAlphaSum(end) ;
                this.absorptionCoeffMeanParam.Value    = trapz(this.levitusDepth, this.levitusAlphaSum) ...
                    / (this.levitusDepth(end) - this.levitusDepth(1));
            end
            
            if this.bubbleValue
                if ~this.modeCalculBubbleBool % Valeur imposee
                    profondeur = maxDepth; % profondeur de la cible depuis la surface (m)
                    vent       = windSpeed( this.seaStateParam.Value ) * 1852 / 3600; % Vitesse du vent en m/s
                    
                    this.bubbleTransmissionLoss = this.bubbleAttenuation(0, freq, immersion, profondeur, vent);
                end
            else
                this.bubbleTransmissionLoss = 0;
            end
            
            %% Noise Level
            
            if isempty(sounder)
                if modeValue == 1
                    
                    % Immersion & selfNoise
                    switch sounderTypeValue % TODO : refaire �a diff�remment
                        case 1 % EM12D
                            switch this.shipNameValue % 1 = Atalante, 2 = Suroit, 3= Thalia, 4 = Europe
                                case 1  % Atalante
                                    this.immersionParam.Value = 4;
                                    this.selfNoiseParam.Value = 65 - 20 * log10(freq);
                                case {2, 3, 4}
                                    % my_warndlg(['Le' this.shipNameValue{this.shipNameValue} ' ne possede pas d''EM12D'], 1)
                                otherwise
                                    % my_warndlg('N''oubliez pas de definir l''immersion des antennes ainsi que le bruit propre du navire', 1)
                            end
                        case 2 % EM12S
                            switch this.shipNameValue
                                case {1, 2, 3, 4}
                                    % my_warndlg(['Le' this.shipNameValue{this.shipNameValue} ' ne possede pas d''EM12S'], 1)
                                otherwise
                                    my_warndlg('N''oubliez pas de definir l''immersion des antennes ainsi que le bruit propre du navire', 1);
                            end
                        case 3 % EM300
                            switch this.shipNameValue
                                case 2  % Suroit
                                    this.immersionParam.Value = 4;
                                    this.selfNoiseParam.Value = 65 - 20 * log10(freq);
                                case {1, 3, 4}
                                    % my_warndlg(['Le' this.shipNameValue{this.shipNameValue} ' ne possede pas d''EM300'], 1)
                                    
                                otherwise
                                    % my_warndlg('N''oubliez pas de definir l''immersion des antennes ainsi que le bruit propre du navire', 1)
                            end
                        case 4 % EM1000
                            switch this.shipNameValue
                                case 1  % Atalante
                                    this.immersionParam.Value = 4.5;
                                    this.selfNoiseParam.Value = 65 - 20 * log10(freq);
                                case 2  % Suroit
                                    this.immersionParam.Value = 4;
                                    this.selfNoiseParam.Value = 65 - 20 * log10(freq);
                                case 3  % Thalia
                                    this.immersionParam.Value = 3;
                                    this.selfNoiseParam.Value = 75 - 20 * log10(freq);
                                case 4  % Europe
                                    this.immersionParam.Value = 2.5;
                                    this.selfNoiseParam.Value = 75 - 20 * log10(freq);
                                otherwise
                                    my_warndlg('N''oubliez pas de definir l''immersion des antennes ainsi que le bruit propre du navire', 1);
                            end
                        case 5 % SAR
                            this.immersionParam.Value = 1000; % Gerer les choses differemment en parlant d'altitude / au fond
                            this.selfNoiseParam.Value = 0;    % VOIR AVEC XL
                        case 6 % EM3000D
                            switch this.shipNameValue
                                case {1, 2, 3, 4}
                                    % my_warndlg(['Le' this.shipNameValue{this.shipNameValue} ' ne possede pas d''EM3000D'], 1)
                                otherwise
                                    % my_warndlg('N''oubliez pas de definir l''immersion des antennes ainsi que le bruit propre du navire', 1)
                            end
                        case 7 % EM3000S
                            switch this.shipNameValue
                                case {1, 2, 3, 4}
                                    % my_warndlg(['Le' this.shipNameValue{this.shipNameValue} ' ne possede pas d''EM3000S'], 1)
                                otherwise
                                    % my_warndlg('N''oubliez pas de definir l''immersion des antennes ainsi que le bruit propre du navire', 1)
                            end
                            
                        case 10 % EM120
                            switch this.shipNameValue
                                case 1  % Beatemps-Beaupr�
                                    this.immersionParam.Value = 4; % A DEFINIR %%%%%%%%%%%%%%%%%%%%%%%%%%%
                                    this.selfNoiseParam.Value = 65 - 20 * log10(freq);% A DEFINIR %%%%%%%%%%%%%%%%%%%%%%%%%%%
                                case 2  % Bateau allemand
                                    this.immersionParam.Value = 4;% A DEFINIR %%%%%%%%%%%%%%%%%%%%%%%%%%%
                                    this.selfNoiseParam.Value = 65 - 20 * log10(freq);% A DEFINIR %%%%%%%%%%%%%%%%%%%%%%%%%%%
                                case {3, 4}
                                    % my_warndlg(['Le' this.shipNameValue{this.shipNameValue} ' ne possede pas d''EM300'], 1)
                                otherwise
                                    % my_warndlg('N''oubliez pas de definir l''immersion des antennes ainsi que le bruit propre du navire', 1)
                            end
                            
                        case {8, 9, 26, 11, 22, 16, 23, 12, 13, 14, 15, 25, 27, 28, 29, 17}
                            % 8=DF1000, 9=DTS1, 26=?, 11=EM120, 22=?, 16=Geoswath, 23=?, 12=Reson7150_12kHz, 13=Reson7150_24kHz,
                            % 14=Reson7111, 15=Reson7125_200kHz, 25=Subbottom, 27=EK60,28=HAC_Generic, 29=ADCP, 17=ME70
                            this.immersionParam.Value = 5; % A DEFINIR %%%%%%%%%%%%%%%%%%%%%%%%%%%
                            this.selfNoiseParam.Value = 65 - 20 * log10(freq);% A DEFINIR %%%%%%%%%%%%%%%%%%%%%%%%%%%
                        case 32 % EM304
                            this.immersionParam.Value = 6;
                            this.selfNoiseParam.Value = 65 - 20 * log10(freq);
                        case {18; 33} % 18=EM710, 33=EM712
                            this.immersionParam.Value = 6;
                            this.selfNoiseParam.Value = 65 - 20 * log10(freq);
                        otherwise
                            str1 = sprintf('"%d" : non d�fini dans cl_pames/init', sounderTypeValue);
                            str2 = sprintf('"%d" : not defined in cl_pames/init', sounderTypeValue);
                            my_warndlg(Lang(str1,str2), 1);
                            %                 my_warndlg('N''oubliez pas de definir l''immersion des antennes ainsi que le bruit propre du navire', 1);
                    end
                    this.shipNameList       = num2navire;
                end
            else
                if modeValue == 1
                    % Sounder Immersion
                    this.immersionParam.Value = sounder.get('SonarHeadDepth');
                    
                    if isempty(this.immersionParam.Value) || this.immersionParam.Value == 0 &&...
                            isempty(this.selfNoiseParam.Value) || this.selfNoiseParam.Value == 0
                        this.immersionParam.Value     = 0;
                        this.selfNoiseParam.Value = 0;
                    end
                    
                    this.shipNameList  = {sounder.get('ShipName')};
                    this.shipNameValue = 1;
                end
            end
            
            % Compute Noise
            if this.noiseTypePopupValue == 1
                this.calculBruit();
            end
            
            %% Sea Floor
            
            for i=1:4
                k = this.selectedSeaFloors(i);
                switch k
                    case 1
                        this.lambertParams(i,:) = this.lambertCoeffRock;
                    case 2
                        this.lambertParams(i,:) = this.lambertCoeffSand;
                    case 3
                        this.lambertParams(i,:) = this.lambertCoeffMud;
                end
            end
        end
        
        % Calcul du niveau de bruit
        %
        % a = calculBruit(a)
        %
        % Input Arguments
        %   a : Instance de la classe
        %
        % Output Arguments
        %   a : Instance modifiee de la classe
        %
        % Examples
        %   a = cl_pames;
        %   a = calculBruit( a )
        %
        % See also windSpeed surfaceNoise thermalNoise Authors
        % Authors : JMA + XL
        %----------------------------------------------------------------------------
        function this = calculBruit(this)
            
            % Get params of others models
            freq = this.pamesModel.signalArraysModel.signalFrequencyParam.Value;
            immersion = this.pamesModel.configurationModel.sounderImmersion;
            
            vitesseVent  = this.windSpeed(this.seaStateParam.Value);
            H = abs(immersion);
            alphaSurface = this.pamesModel.get_attenuation(0);
            this.surfaceNoiseParam.Value = this.surfaceNoise(freq, vitesseVent, H, alphaSurface);
            this.thermalNoiseParam.Value = this.thermalNoise(freq);
            
            this.totalNoiseLevelParam.Value = this.reflec_Enr2dB(this.reflec_dB2Enr(this.surfaceNoiseParam.Value) ...
                + this.reflec_dB2Enr(this.thermalNoiseParam.Value) ...
                + this.reflec_dB2Enr(this.selfNoiseParam.Value));
        end
        
        % Bruit de surface
        %
        % Syntax
        %   sN = surfaceNoise( Frequency, vitesseVent, ... )
        %
        % Input Arguments
        %   Frequency   : Frequence du signal (kHz)
        %   vitesseVent : Vitesse du vent (nds)
        %
        % Optional Input Arguments
        %   H      : Profondeur du sonar (m)
        %   alphaH : Attenuation inegree a la surface (dB/km)
        %
        % Output Arguments
        %   sN          : Bruit de surface (dB)
        %
        % Examples
        %   sN = surfaceNoise( 13, 20 )
        %
        %   f = 1:100; sN = surfaceNoise( f, 20 );
        %   plot(f, sN); grid on; xlabel('Frequence (kHz)'); ylabel('Surface noise (dB)');
        %
        %   seaState = 0:12; sN = surfaceNoise( 13, windSpeed(seaState) );
        %   plot(seaState, sN); grid on; xlabel('Etat de mer (Beaubort)'); ylabel('Surface noise (dB)');
        %
        %   H = 0:0.1:6; sN = surfaceNoise( 13, 20, H, 1.2 );
        %   plot(H, sN); grid on; xlabel('Profondeur des antennes (m)'); ylabel('Surface noise (dB)');
        %
        % See also windSpeed thermalNoise Authors
        % Authors : JMA + XL
        %----------------------------------------------------------------------------
        
        function sN = surfaceNoise(~, Frequency, vitesseVent, varargin )  %/acoustics/noise
            
            sN = 95 + sqrt(21 * vitesseVent) - 17 * log10(Frequency * 1000);
            
            if nargin == 5
                H = varargin{1};
                alpha = varargin{2};
                Hkm = H/ 1000;
                termeCorrectif = alpha * Hkm + 10 * log10(1 + (alpha / 8.686) * Hkm);
                sN = sN - termeCorrectif;
            end
        end
        
        % Bruit thermique
        %
        % Syntax
        %   tN = thermalNoise( Frequency )
        %
        % Input Arguments
        %   Frequency : Frequence du signal (kHz)
        %
        % Output Arguments
        %  tN         : Bruit de surface (dB)
        %
        % Examples
        %   tN = thermalNoise( 13 )
        %
        %   f = 1:100; tN = thermalNoise( f );
        %   plot(f, tN); grid on; xlabel('Frequence (kHz)'); ylabel('Thermal noise (dB)');
        %
        % See also surfaceNoise Authors
        % Authors : JMA + XL
        %----------------------------------------------------------------------------
        
        function noise = thermalNoise(~, Frequency ) %/acoustics/noise
            noise = -15 + 20 *log10(Frequency);
        end
        
        % Coefficient d'attenuation en fonction de f, Z, T, S
        % Z,T et S constituent le profil de bathy-celerimetrie
        %
        % Cette fonction calcule l'attenuation locale ainsi que l'attenuation
        % moyenne integree a partir de la source acoustique
        %
        % Cette fonction calcule egalement la droite de regression lineaire de
        % l'attenuation integree adaptee a la bathymetrie locale
        %
        % Syntax
        %   [Alpha, AlphaSum] = AttenuationGarrison(f, Z, T, S, ...)
        %
        % Input Arguments
        %   f : frequence(s) du sondeur en kHz
        %   Z : Profondeur (m)
        %   T : Temperature (degres celsius)
        %   S : Salinite (0/00)
        %
        % Name-Value Pair Arguments
        %   Immersion      : Immersion de la source (m) (Z(1) par defaut)
        %   LimBathyLocale : Limites de la bathymetie locale afin de calculer la droire de
        %                    regression lineaire de l'attenuation integr�e
        %
        % Output Arguments
        %   []            : Auto-plot activation
        %   Alpha         : Coefficient d'attenuation pour chaque valeur de Z en db/km
        %   AlphaSum      : Valeur moyenne du coefficient d'attenuation sur la tranche d'eau [Immersion,Z] en db/km
        %   DroiteOrigine : Valeur a l'origine de la droite de regression lineaire
        %   DroitePente   : Pente de la droite de regression lineaire
        %
        % Examples
        %   [Alpha, AlphaSum] = AttenuationGarrison(13, 0:500:6000, 4, 35)
        %
        %     nomFic = getNomFicDatabase('BATHYCEL_North-East_Atlantic_Winter.csv');
        %     [Z, S, T] = lecCsv(nomFic);
        %
        %   [Alpha, AlphaSum] = AttenuationGarrison(95, Z, T, S);
        %   AttenuationGarrison(95, Z, T, S );
        %   AttenuationGarrison(95, Z, T, S, 'LimBathyLocale', [-4000 -3000])
        %   AttenuationGarrison(95, Z, T, S ,'Immersion', -3000)
        %   AttenuationGarrison(95, Z, T, S, 'LimBathyLocale', [-4000 -3000], 'Immersion', -3000)
        %
        %   AttenuationGarrison([10:10:100], Z, T, S );
        %
        %   [Alpha, AlphaSum] = AttenuationGarrison([10:10:100], Z, T, S);
        %   plot(Alpha,Z); grid on; zoom on;
        %   hold on; plot(AlphaSum,Z, 'r');
        %
        % See also lecCsv FMgSO4 FBOH3 moyx0 Authors
        % Authors : JMA + XL
        %---------------------------------------------------------------------------------------
        function [Alpha, AlphaSum, DroiteOrigine, DroitePente] = attenuationGarrison(~, f, Z, T, S, varargin)
            
            [varargin, Immersion]      = getPropertyValue(varargin, 'Immersion',      []);
            [varargin, LimBathyLocale] = getPropertyValue(varargin, 'LimBathyLocale', [min(Z) 0]); %#ok<ASGLU>
            
            if isempty(Immersion)
                Immersion = 0;
            end
            
            Z = -abs(Z(:));
            T = T(:);
            S = S(:);
            sz = size(f);
            
            f = double(f);
            Z = double(Z);
            T = double(T);
            S = double(S);
            Immersion = double(Immersion);
            LimBathyLocale = double(LimBathyLocale);
            
            %% Frequence de relaxation des molecules de sulfate de magnesium MgSO4 (kHz)
            
            F2 = FMgSO4(T, S);
            
            %% Frequence de relaxation des molecules d'acide borique B(OH)3 (kHz)
            
            F1 = FBOH3(T, S);
            
            %% C�l�rit� du son dans l'eau (m/s)
            
            Celerite = 1412 + (3.21 * T) + (1.19 * S) - (1.67e-2 * Z);
            
            %%
            
            T_carre = T .* T;
            
            Index = T <= 20;
            A31 = (4.937e-4 - 2.59e-5 * T + 9.11e-7 * T_carre - 1.5e-8  * T_carre .* T) .* Index;
            Index = T > 20;
            A32 = (3.964e-4 - 1.146e-5 * T + 1.45e-7  * T_carre - 6.5e-10  * T_carre .* T) .* Index;
            A3 = A31 + A32;
            
            Z2 = Z .* Z;
            A1 = (154 ./ Celerite);
            P1 = 1;
            A2 = 21.44 * S ./ Celerite .* (1. + 0.025 * T);
            P2 = (1. - 1.37e-4 * (-Z) + 6.2e-9 * Z2);
            P3 = 1. - 3.83e-5 .* (-Z) + 4.9e-10 .* Z2;
            
            % Avant
            
            Alpha = [];
            AlphaSum = [];
            nbp = length(Z);
            for k=1:sz(2)
                fCarre = f(k) .* f(k);
                a = (A1 .* P1 .* (F1 .* fCarre) ./ (F1 .* F1 + fCarre)) + ...
                    A2 .* P2 .* (F2 .* fCarre) ./ (fCarre + F2 .* F2) + ...
                    A3 .* P3 .* fCarre;
                Alpha = [Alpha, a]; %#ok<AGROW>
                
                if nbp == 1
                    b = a;
                else
                    b = moyx0(Z, a, Immersion);
                end
                AlphaSum = [AlphaSum, b]; %#ok<AGROW>
            end
            
            
            % Modif 30/01/2011
            % nbp = length(Z);
            % for k=1:sz(2)
            %     fCarre = f(k) .* f(k);
            %     a = (A1 .* P1 .* (F1 .* fCarre) ./ (F1 .* F1 + fCarre)) + ...
            %         A2 .* P2 .* (F2 .* fCarre) ./ (fCarre + F2 .* F2) + ...
            %         A3 .* P3 .* fCarre;
            %     Alpha = a;
            %
            %     if nbp == 1
            %         b = a;
            %     else
            %         b = moyx0(Z, a, Immersion);
            %     end
            %     AlphaSum = b;
            % end
            
            N = size(AlphaSum,2);
            DroiteOrigine = NaN(1, N, 'single');
            DroitePente   = NaN(1, N, 'single');
            % TODO : actuellement c'est faux il faut calculer polyfit � partir de l'immersion : sub = find(ZBathy > Immersion)
            if (nargout > 2) || (nargout == 0)
                for k=1:N
                    if size(AlphaSum,1) == 1
                        DroiteOrigine(k) = AlphaSum(1,k);
                        DroitePente(k) = 0;
                    else
                        ZBathy = linspace(LimBathyLocale(1), LimBathyLocale(2), 100);
                        AlphaBathy = interp1(Z, AlphaSum(:,k), ZBathy, 'linear', 'extrap');
                        p = polyfit(ZBathy, AlphaBathy, 1);
                        DroiteOrigine(k) = p(2);
                        DroitePente(k) = p(1);
                    end
                end
            end
            
            %% Sortie des param�tres ou trac�s graphiques
            
            if nargout == 0
                FigUtils.createSScFigure;
                if size(Alpha, 2) == 1
                    PlotUtils.createSScPlot(Alpha, Z); grid on; zoom on; hold on;
                    PlotUtils.createSScPlot(AlphaSum, Z, 'r'); grid on;
                    xlabel('Attenuation (db/km)'); ylabel('Z (m)'); title('Alpha et AlphaSum');
                    
                    h = PlotUtils.createSScPlot(polyval(p, Z), Z, 'k--');
                    str = sprintf('disp(''Origine : %f (dB/km) - Pente : %f (dB/km/m)'')', DroiteOrigine, DroitePente);
                    set(h, 'ButtonDownFcn', str)
                    legend({'Alpha'; 'AlphaSum'; 'Approximation'});
                else
                    subplot(1, 2, 1);
                    PlotUtils.createSScPlot(Alpha, Z); grid on; zoom on;
                    xlabel('Attenuation (db/km)'); ylabel('Z (m)'); title('Attenuation');
                    subplot(1, 2, 2);
                    PlotUtils.createSScPlot(AlphaSum, Z); grid on;
                    xlabel('Alpha (db/km)'); ylabel('Z (m)'); title('Attenuation integree de 0 a z');
                end
                %         disp(sprintf('Valeur a l''origine (Z=0) de la droite de regression lineaire : %s', mat2str(DroiteOrigine)))
                %         disp(sprintf('Pente de la droite : %s', mat2str(DroitePente)))
            end
        end
        
        
        % Calcul des pertes de propagation aller_retour dues a une population de bulles
        %
        % Syntax
        %   attenuation = bubbleAttenuation(Angle, Frequence, Immersion, Profondeur, Vent )
        %
        % Input Arguments
        %   Angle       :  Angle d'emission du sondeur (deg/vericale)
        %   Frequence   :  Frequence d'emission du sondeur (kHz)
        %   Immersion   :  Immersion du sondeur (m)
        %   Profondeur  :  Profondeur de la cible depuis la surface (m)
        %   Vent        :  Vitesse du Vent de surface (m/s)
        %
        % Output Arguments
        %   []          :  Auto-plot activation
        %   attenuation :  pertes totales (dB).
        %
        % Examples
        %   IMM  = 3:0.2:5;
        %   A    = 50:10:80;
        %   Abis = 50:5:80;
        %   attenuation = bubbleAttenuation( 30, 10, 4, 20, 15 )
        %   attenuation = bubbleAttenuation( 30, 10, IMM, 12, 15 )
        %   attenuation = bubbleAttenuation( Abis, 10, 4, 12, 15 )
        %   attenuation = bubbleAttenuation( A, 10, IMM, 12, 15 )
        %
        % Remarks    : resolution de l'equation n^ 30
        % See also bubbleRateAttenuation bubbleResonantRadius bubbleComplexCelerity windSpeed plotBubbleAttenuation
        % References : Marshall V. Hall ,Wind-generated bubbles ,J.Acoust.Soc.Am.,Vol.86,No.3,September 1989
        % Authors : PYT
        %------------------------------------------------------------------------------
        
        function  attenuation = bubbleAttenuation(~, Angle, Frequence, Immersion, Profondeur, Vent )
            
            if nargout == 0
                plotBubbleAttenuation(Angle, Frequence, Immersion, Profondeur, Vent )
                return
            end
            
            nA = length(Angle);
            nF = length(Frequence);
            nI = length(Immersion);
            nP = length(Profondeur);
            nV = length(Vent);
            
            if isequal([nA nF nI nP nV], [1 1 1 1 1])
                angleH      = 90 - abs(Angle);
                Immersion  = abs(Immersion);
                Profondeur = abs(Profondeur);
                if Profondeur > 12
                    Profondeur = 12;
                end
                depth       = linspace(Immersion, Profondeur, 32); %domaine d'integration
                attenuation = 2 * trapz(depth, integrand(depth, angleH, Frequence, Immersion, Vent)); %eq30
            else
                attenuation = NaN(nA, nF, nI, nP, nV);
                for iA=1:nA
                    for iF=1:nF
                        for iI=1:nI
                            for iP=1:nP
                                for iV = nV
                                    attenuation(iA, iF, iI, iP, iV) = bubbleAttenuation(Angle(iA), Frequence(iF), Immersion(iI), Profondeur(iP), Vent(iV) );
                                end
                            end
                        end
                    end
                end
                attenuation = squeeze(attenuation);
            end
            
            
            %% Calcul de l'attenuation locale a chaque profondeur
            
            function Integrand = integrand(p1, a1, f1, imm1, v1)
                Integrand = zeros(size(p1));
                for q=1:length(p1)
                    p1q          = p1(q);
                    Integrand(q) = bubbleRateAttenuation(f1, p1q, v1) / real( sqrt(1-(anglelocal(a1, f1, imm1, p1q, v1)^2)) );
                end
            end
            
            %% Calcul de l'angle local eq29
            
            function psi = anglelocal(angl, freq, immer, prof, ven)
                
                Ar   = bubbleResonantRadius(freq, prof);
                ray  = eps: ((Ar-eps) / 100) :Ar;
                
                ArS  = bubbleResonantRadius(freq, immer);
                rayS = eps: ((ArS-eps) / 100) :ArS;
                V    = bubbleComplexCelerity(rayS, freq, immer, ven, ArS) / cos(angl * pi / 180); %determination de la constante 'velocity' V eq29
                
                psi  = bubbleComplexCelerity(ray, freq, prof, ven, Ar) / V; %sortie de la loi de cos( phi(z) ), eq29
            end
        end
        
        
        % Transforms Energy values in dB : xdB = 10 * log10(xNat)
        %
        % Syntax
        %   xdB = reflec_Enr2dB(xNat)
        %
        % Input Arguments
        %   xNat : Values in Energy
        %
        % Output Arguments
        %   xdB : Values in dB
        %
        % Examples
        %   reflec_Enr2dB(0.0001)
        %   reflec_Enr2dB([0 0.1 0.01 0.001 0.0001])
        %
        % See also reflec_dB2Enr cl_image/reflec_Enr2dB sum_dB reflec_Amp2dB Authors
        % Authors : JMA
        %--------------------------------------------------------------------------------
        function xdB = reflec_Enr2dB(~, xNat)
            
            if isempty(xNat)
                xdB = [];
            else
                xNat(xNat <= 0) = NaN;
                xdB = 10 * log10(xNat);
            end
        end
        
        % Transforms Energy values in dB : xNat = 10 .^ (xdB / 10)
        %
        % Syntax
        %   xNat = reflec_dB2Enr(xdB)
        %
        % Input Arguments
        %	xdB : Values in dB
        %
        % Output Arguments
        %   xNat : Values in Amplitude
        
        % Examples
        % 	reflec_dB2Enr(-30)
        % 	reflec_dB2Enr(-40:5:-20)
        %
        % See also reflec_Enr2dB cl_image/reflec_dB2Enr sum_dB reflec_Amp2dB Authors
        % Authors : JMA
        %-------------------------------------------------------------------------------
        function xNat = reflec_dB2Enr(~, xdB)
            xNat = 10 .^ (xdB / 10);
        end
        
        
        % Vitesse du vent en fonction de l'etat de mer
        %
        % Syntax
        %   v = windSpeed( SeaState )
        %
        % Input Arguments
        %   SeaState  : Etat de mer (Beaufort)
        %
        % Output Arguments
        %   v : Vitesse du vent (nds)
        %
        % Examples
        %   v = windSpeed( 0:12 )
        %   plot(0:12, v); grid on;
        %
        % See also SurfaceNoise Authors
        % Authors : JMA + XL
        % References : http://www.univ-lemans.fr/~hainry/articles/beaufort.html
        %----------------------------------------------------------------------------
        
        function v = windSpeed(~, SeaState )
            
            if nargin == 0
                help windSpeed
                return
            end
            
            v = 3 * SeaState .^ 1.5 / 1.852;
        end
    end
end
