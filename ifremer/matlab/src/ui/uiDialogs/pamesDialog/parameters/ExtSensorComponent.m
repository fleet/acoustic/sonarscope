classdef ExtSensorComponent < handle & uiextras.VBox
    %EXTSENSORCOMPONENT Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        extSensorModel
        pamesDialog % PamesDialog
        
        attitudeSensorPopupmenu
        
        attitudeComponents       = SimpleParametreComponent.empty;
        soundSpeedComponents     = SimpleParametreComponent.empty;
        existenceComponents      = SimpleParametreComponent.empty;
        ensonificationComponents = SimpleParametreComponent.empty;
    end
    
    methods
        function this = ExtSensorComponent(parentTab, extSensorModel, pamesDialog)
            % Constructor of the class
            
            % Call SuperConstructor
            this = this@uiextras.VBox('Parent', parentTab);
            
            this.extSensorModel = extSensorModel;
            this.pamesDialog = pamesDialog;
            
            %             % Frequence
            %             AttitudeBox = uiextras.HBox('Parent', this);
            %             this.freqComponent = SimpleParametreComponent(freqBox, 'param', this.pamesDialog.pamesModel.signalArraysModel.signalFrequencyParam, 'editable', 'off');
            %             uiextras.Empty('Parent', freqBox);
            %             freqBox.set('Sizes', [400 -1]);
            
            % Attitude Sensor
            attitudeSensorBox = uiextras.HBox('Parent', this);
            uicontrol('Parent', attitudeSensorBox, ...
                'Style',  'text', ...
                'String', 'AMotion sensor : ');
            this.attitudeSensorPopupmenu = uicontrol('parent', attitudeSensorBox, ...
                'Style',    'popupmenu', ...
                'String',   this.extSensorModel.attitudeSensorList, ...
                'Value',   this.extSensorModel.attitudeSensorValue, ...
                'Callback', @this.attitudeSensorCallback);
            attitudeSensorBox.set('Sizes', [200 200]);
            
            sensorBox = uiextras.HBox('Parent', this);
            
            % size of ParametreComponents
            %             sz = [0 -4 0 0 0 -1 -1 25];
            % sz = [ -2 -1 -1 25];
            
            %% Left Panel
            
            pLeft  = uipanel('Parent', sensorBox, 'Title', 'Auxiliary Sensor Accuracy', 'FontSize', 13, 'FontWeight', 'bold');
            hBoxL = uiextras.VBox('Parent', pLeft, 'Spacing', 5);
            
            %% Top Left Panel : Attitude Sensor
            pTLeft = uipanel('Parent', hBoxL, 'Title', 'Motion sensor', 'FontSize', 10, 'FontWeight', 'bold');
            hBoxTL = uiextras.VBox( 'Parent', pTLeft, 'Spacing', 5);
            %TODO add attitude sensor value (from configurationComponent)
            
            for k=1:numel(this.extSensorModel.attitudeMeasurementParams)
                this.attitudeComponents(k) = SimpleParametreComponent(hBoxTL, 'param', this.extSensorModel.attitudeMeasurementParams(k), 'editable', 'off');%, 'Sizes', sz);
            end
            hBoxTL.set('Sizes', 25*ones(1,numel(this.attitudeComponents)));
            
            %% Bottom Left Panel : Sound Speed Measurement
            
            pBLeft = uipanel('Parent', hBoxL, 'Title', 'Sound Speed', 'FontSize', 10, 'FontWeight', 'bold');
            hBoxBL = uiextras.VBox( 'Parent', pBLeft, 'Spacing', 5);
            for k=1:numel(this.extSensorModel.speedMeasurementParams)
                this.soundSpeedComponents(k) = SimpleParametreComponent(hBoxBL, 'param', this.extSensorModel.speedMeasurementParams(k), 'editable', 'off');%, 'Sizes', sz);
            end
            hBoxBL.set('Sizes', 25*ones(1,numel(this.soundSpeedComponents)));
            
            
            %% Right Panel
            
            pRight = uipanel('Parent', sensorBox, 'Title', 'Ship''s Maximum Allowed Motions', 'FontSize', 13, 'FontWeight', 'bold');
            hBoxR = uiextras.VBox('Parent', pRight, 'Spacing', 5);
            
            %% Top Right Panel : Echo Existence
            
            pTRight = uipanel('Parent', hBoxR, 'Title', 'Echo Existence', 'FontSize', 10, 'FontWeight', 'bold');
            hBoxTR = uiextras.VBox( 'Parent', pTRight, 'Spacing', 5);
            for k=1:numel(this.extSensorModel.existenceParams)
                this.existenceComponents(k) = SimpleParametreComponent(hBoxTR, 'param', this.extSensorModel.existenceParams(k), 'editable', 'off');%, 'Sizes', sz);
            end
            hBoxTR.set('Sizes', 25*ones(1,numel(this.existenceComponents)));
            
            
            %% Bottom Right Panel : 100% Ensonification
            
            pBRight = uipanel('Parent', hBoxR, 'Title', '100% Ensonification', 'FontSize', 10, 'FontWeight', 'bold');
            hBoxBR = uiextras.VBox( 'Parent', pBRight, 'Spacing', 5);
            for k=1:numel(this.extSensorModel.ensonificationParams)
                this.ensonificationComponents(k) = SimpleParametreComponent(hBoxBR, 'param', this.extSensorModel.ensonificationParams(k), 'editable', 'off');%, 'Sizes', sz);
            end
            hBoxBR.set('Sizes', 25*ones(1,numel(this.ensonificationComponents)));
            
            this.set('Sizes', [25 -1]);
            
        end
        
        function updateComponent(this)
            this.attitudeSensorPopupmenu.set('String', this.extSensorModel.attitudeSensorList, 'Value', this.extSensorModel.attitudeSensorValue);
            
            %% Attitude Components
            for k=1:numel(this.attitudeComponents)
                this.attitudeComponents(k).updateComponent();
            end
            
            %% Sound Speed Components
            for k=1:numel(this.soundSpeedComponents)
                this.soundSpeedComponents(k).updateComponent();
            end
            
            %% Echo Existence Components
            for k=1:numel(this.existenceComponents)
                this.existenceComponents(k).updateComponent();
            end
            
            %% Ensonification Components
            for k=1:numel(this.ensonificationComponents)
                this.ensonificationComponents(k).updateComponent();
            end
        end
        
        function attitudeSensorCallback(this, src, ~)
            this.extSensorModel.attitudeSensorValue = src.Value;
            % Update models & components
            this.pamesDialog.update();
        end
    end
end
