classdef ExtSensorModel < handle
    %EXTSENSORMODEL Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Constant)
        Nds2MpS = 1852 / 3600;
        Mps2Nds = 3600 / 1852;
        D2R     = pi / 180;
        R2D     = 180 / pi;
        C       = 1500;
    end
    
    properties
        pamesModel
        
        % Attitude sensor
        attitudeSensorList  = {};
        attitudeSensorValue = [];
        
        % External Sensor Precision Parametres
        attitudeMeasurementParams = ClParametre.empty;
        rollMeasurementParam      = ClParametre('Name', 'Roll (std)',    'Unit', 'deg');
        pitchMeasurementParam     = ClParametre('Name', 'Pitch (std)',   'Unit', 'deg');
        headingMeasurementParam   = ClParametre('Name', 'Heading (std)', 'Unit', 'deg');
        heaveMeasurementParam     = ClParametre('Name', 'Heave (std)',   'Unit', 'm');
        
        % Sound Speed Measurement Parametres
        speedMeasurementParams          = ClParametre.empty;
        averageSoundSpeedParam          = ClParametre('Name', 'Profile (std)',              'Unit', 'm/s');
        arraySoundSpeedMeasurementParam = ClParametre('Name', 'Array Sound velocity (std)', 'Unit', 'm/s');
        
        % Ship's Maximum Allowed Motions Parametres
        existenceParams         = ClParametre.empty;
        existenceShipSpeedParam = ClParametre('Name', 'Ship''s Speed', 'Unit', 'Kts');
        existencePitchParam     = ClParametre('Name', 'Pitch',         'Unit', 'deg/s');
        existenceYawParam       = ClParametre('Name', 'Yaw',           'Unit', 'deg/s');
        
        % 100% Ensonification Parametres
        ensonificationParams         = ClParametre.empty;
        ensonificationShipSpeedParam = ClParametre('Name', 'Ship''s speed', 'Unit', 'Kts');
        ensonificationPitchParam     = ClParametre('Name', 'Pitch rate',    'Unit', 'deg/s');
        ensonificationYawParam       = ClParametre('Name', 'Yaw',           'Unit', 'deg/s');
    end
    
    properties (Access = private)
        %% Help Html
        nameHtmlArrayShipAttitude = ['file://' fullfile(getNomFicDatabase('ships_attitude.html'))];
        nameHtmlArrayShipSpeed    = ['file://' fullfile(getNomFicDatabase('ships_speed.html'))];
        nameHtmlArraySoundSpeed   = ['file://' fullfile(getNomFicDatabase('sound_speed.html'))];
    end
    
    methods
        function this = ExtSensorModel(pamesModel)
            this.pamesModel = pamesModel;
            
            % Attitude Params
            this.attitudeMeasurementParams        = ClParametre.empty;
            this.rollMeasurementParam.Help        = this.nameHtmlArrayShipAttitude;
            this.attitudeMeasurementParams(end+1) = this.rollMeasurementParam;
            this.pitchMeasurementParam.Help       = this.nameHtmlArrayShipAttitude;
            this.attitudeMeasurementParams(end+1) = this.pitchMeasurementParam;
            this.headingMeasurementParam.Help     = this.nameHtmlArrayShipAttitude;
            this.attitudeMeasurementParams(end+1) = this.headingMeasurementParam;
            this.heaveMeasurementParam.Help       = this.nameHtmlArrayShipAttitude;
            this.attitudeMeasurementParams(end+1) = this.heaveMeasurementParam;
            
            % SoundSpeed Params
            this.speedMeasurementParams               = ClParametre.empty;
            this.averageSoundSpeedParam.Help          = this.nameHtmlArraySoundSpeed;
            this.speedMeasurementParams(end+1)        = this.averageSoundSpeedParam;
            this.arraySoundSpeedMeasurementParam.Help = this.nameHtmlArraySoundSpeed;
            this.speedMeasurementParams(end+1)        = this.arraySoundSpeedMeasurementParam;
            
            % Existence Parmas
            this.existenceParams              = ClParametre.empty;
            this.existenceShipSpeedParam.Help = this.nameHtmlArrayShipAttitude;
            this.existenceParams(end+1)       = this.existenceShipSpeedParam;
            this.existencePitchParam.Help     = this.nameHtmlArrayShipAttitude;
            this.existenceParams(end+1)       = this.existencePitchParam;
            this.existenceYawParam.Help       = this.nameHtmlArrayShipAttitude;
            this.existenceParams(end+1)       = this.existenceYawParam;
            
            % Ensonification Params
            this.ensonificationParams              = ClParametre.empty;
            this.ensonificationShipSpeedParam.Help = this.nameHtmlArrayShipSpeed;
            this.ensonificationParams(end+1)       = this.ensonificationShipSpeedParam;
            this.ensonificationPitchParam.Help     = this.nameHtmlArrayShipAttitude;
            this.ensonificationParams(end+1)       = this.ensonificationPitchParam;
            this.ensonificationYawParam.Help       = this.nameHtmlArrayShipAttitude;
            this.ensonificationParams(end+1)       = this.ensonificationYawParam;
        end
        
        
        function initModel(this)
            
            % Attitude sensor
                        this.attitudeSensorList   = {};
            this.attitudeSensorValue  = [];
            
            % Init Attitude Params
            for k=1:numel(this.attitudeMeasurementParams)
                this.attitudeMeasurementParams(k).Value = [];
            end
            % Init Attitude Params
            for k=1:numel(this.speedMeasurementParams)
                this.speedMeasurementParams(k).Value = [];
            end
            % Init Existence Params
            for k=1:numel(this.existenceParams)
                this.existenceParams(k).Value = [];
            end
            % Init Ensonification Params
            for k=1:numel(this.ensonificationParams)
                this.ensonificationParams(k).Value = [];
            end
        end
        
        function updateModel(this)
            
%             %% Paramètres de la frame Accuracy
% 
% if isempty(b.Accuracy.ArraySoundSpeedMeasurement)
%     b.Accuracy.ArraySoundSpeedMeasurement = 1.1;
% end
% if isempty(b.Accuracy.AverageSoundSpeed)
%     b.Accuracy.AverageSoundSpeed = 1.1;
% end
% if isempty(b.Accuracy.AttitudeSensor)
%     b.Accuracy.AttitudeSensor = 1;
% end
% switch b.Accuracy.ListeAttitudeSensor{b.Accuracy.AttitudeSensor} %b.Accuracy.AttitudeSensor
%     case 'Hippy' %1  % Hippy
%         b.Accuracy.RollMeasurement     = 0.05;
%         b.Accuracy.PitchMeasurement    = 0.05;
%         b.Accuracy.HeadingMeasurement  = 0.1;
%         b.Accuracy.HeaveMeasurement    = 0.1;
%     case 'HDMS' %2  % HDMS
%         b.Accuracy.RollMeasurement     = 0.05;
%         b.Accuracy.PitchMeasurement    = 0.05;
%         b.Accuracy.HeadingMeasurement  = 0.11;
%         b.Accuracy.HeaveMeasurement    = 0.11;
%     otherwise %case 3 : Other
%         b.Accuracy.HeadingMeasurement  = get(b.cl_sounder, 'HeadAccuracy');
%         b.Accuracy.PitchMeasurement    = get(b.cl_sounder, 'PitchAccuracy');
%         b.Accuracy.RollMeasurement     = get(b.cl_sounder, 'RollAccuracy');
%         b.Accuracy.HeaveMeasurement    = get(b.cl_sounder, 'HeaveAccuracy');
%         % Si pas d'information concernant la MRU
%         if isempty(b.Accuracy.RollMeasurement)
%             b.Accuracy.ListeAttitudeSensor = {'Default MRU'};
%             if isempty(b.Accuracy.RollMeasurement)
%                 b.Accuracy.RollMeasurement    = 0.05;
%             end
%             if isempty(b.Accuracy.PitchMeasurement),
%                 b.Accuracy.PitchMeasurement   = 0.05;
%             end
%             if isempty(b.Accuracy.HeadingMeasurement),
%                 b.Accuracy.HeadingMeasurement = 0.1;
%             end
%             if isempty(b.Accuracy.HeaveMeasurement),
%                 b.Accuracy.HeaveMeasurement   = 0.1;
%             end
%         end
% end

% % Parametres de la frame Accuracy devant etre calcules
% b.Accuracy.ExistenceShipSpeed   = VitesseEcho;
% b.Accuracy.ExistencePitch       = R2D * (VitesseEcho * Nds2MpS - VitesseBateauImposeeEnMpS) / H;
% b.Accuracy.ExistenceYaw         = R2D * ((C * OuvertureMaxLongitudinale * D2R / 4) - VitesseBateauImposeeEnMpS) / (H * tan(DemiOuvertureMaxUtileEnRad));
% 
% Vitesse100 = C * OuvertureLongitEmissionReception * D2R * cos(DemiOuvertureMaxUtileEnRad) / 2;	% en m/s
% b.Accuracy.EnsonificationShipSpeed = Vitesse100 * Mps2Nds;	    % en noeuds
% b.Accuracy.EnsonificationPitch = R2D * (Vitesse100 - VitesseBateauImposeeEnMpS) / H;
% b.Accuracy.EnsonificationYaw = R2D * ((C * OuvertureLongitEmissionReception * D2R / 2) - VitesseBateauImposeeEnMpS) / (H *  tan(DemiOuvertureMaxUtileEnRad));


            % Get params of others models
            sounder = this.pamesModel.sounder;
            modeValue = this.pamesModel.configurationModel.modeValue;
            
            % Attitude sensor
            if isempty(sounder)
                if modeValue == 1
                    this.attitudeSensorList = {'Hippy';'HDMS';'Other'};
                end
            else
                if modeValue == 1
                    this.attitudeSensorList  = {sounder.get('MRU')};
                    this.attitudeSensorValue = 1;
                end
            end
            if isempty(this.attitudeSensorValue)
                this.attitudeSensorValue = 1;
            end
            
            
            % arraySoundSpeedMeasurement
            if isempty(this.arraySoundSpeedMeasurementParam.Value)
                this.arraySoundSpeedMeasurementParam.Value = 1.1;
            end
            % averageSoundSpeedParam
            if isempty(this.averageSoundSpeedParam.Value)
                this.averageSoundSpeedParam.Value = 1.1;
            end
      
            switch this.attitudeSensorList{this.attitudeSensorValue}
                case 'Hippy' %1  % Hippy
                    this.rollMeasurementParam.Value     = 0.05;
                    this.pitchMeasurementParam.Value    = 0.05;
                    this.headingMeasurementParam.Value  = 0.1;
                    this.heaveMeasurementParam.Value    = 0.1;
                case 'HDMS' %2  % HDMS
                    this.rollMeasurementParam.Value     = 0.05;
                    this.pitchMeasurementParam.Value    = 0.05;
                    this.headingMeasurementParam.Value  = 0.11;
                    this.heaveMeasurementParam.Value    = 0.11;
                otherwise %case 3 : Other
                    this.headingMeasurementParam.Value  = sounder.get('HeadAccuracy');
                    this.pitchMeasurementParam.Value    = sounder.get('PitchAccuracy');
                    this.rollMeasurementParam.Value     = sounder.get('RollAccuracy');
                    this.heaveMeasurementParam.Value    = sounder.get('HeaveAccuracy');
                    % Si pas d'information concernant la MRU
                    if isempty(this.rollMeasurementParam.Value)
                        this.pamesModel.configurationModel.attitudeSensorList = {'Default MRU'};
                        if isempty(this.rollMeasurementParam.Value)
                            this.rollMeasurementParam.Value    = 0.05;
                        end
                        if isempty(this.pitchMeasurementParam.Value)
                            this.pitchMeasurementParam.Value   = 0.05;
                        end
                        if isempty(this.headingMeasurementParam.Value)
                            this.headingMeasurementParam.Value = 0.1;
                        end
                        if isempty(this.heaveMeasurementParam.Value)
                            this.heaveMeasurementParam.Value   = 0.1;
                        end
                    end
            end 
            
            
            %% Computed parmaeters
            
            % Get params from other models (from configuration Model)
            maximumDepth          = this.pamesModel.configurationModel.sounderMaximumDepth;
            sounderTypeValue      = this.pamesModel.configurationModel.sounderTypeValue;
            txBeamWidthAlongTrack = this.pamesModel.signalArraysModel.txBeamWidthAlongTrackParam.Value;
            rxBeamWidthAlongTrack = this.pamesModel.signalArraysModel.rxBeamWidthAlongTrackParam.Value;
            
            H = maximumDepth;
            selectedBottomType = 1;
            X = this.pamesModel.calculFauchee(H, selectedBottomType);
            
            demiOuvertureMaxUtileEnRad = atan(X / H);
            D = H ./ cos(demiOuvertureMaxUtileEnRad);
            T = 2 * D / this.C;
            
            optimumPingRate = T;
            
            if sounderTypeValue == 5  % Cas du SAR %TODO
                imposedPingRate = 1.5;
            else
                imposedPingRate = optimumPingRate * 1.1;
            end % Depend de la profondeur
            
            
            ouvertureMaxLongitudinale        = max(txBeamWidthAlongTrack, rxBeamWidthAlongTrack);
            ouvertureLongitEmissionReception = this.pamesModel.antenneOuvEmiRec(txBeamWidthAlongTrack, rxBeamWidthAlongTrack);
            vitesseBateauOptimaleEnNds       = this.Mps2Nds * H * ouvertureLongitEmissionReception * this.D2R / imposedPingRate;
            
            vitesseEcho               = this.C * ouvertureMaxLongitudinale * this.D2R / 4;	% en m/s
            vitesseBateauImposeeEnMpS = vitesseBateauOptimaleEnNds * this.Nds2MpS;
            vitesseEcho               = vitesseEcho * this.Mps2Nds;	% en noeuds

            
            this.existenceShipSpeedParam.Value = vitesseEcho;
            this.existencePitchParam.Value     = this.R2D * (vitesseEcho * this.Nds2MpS - vitesseBateauImposeeEnMpS) / H;
            this.existenceYawParam.Value       = this.R2D * ((this.C * ouvertureMaxLongitudinale * this.D2R / 4) - vitesseBateauImposeeEnMpS) / (H * tan(demiOuvertureMaxUtileEnRad));

            vitesse100 = this.C * ouvertureLongitEmissionReception * this.D2R * cos(demiOuvertureMaxUtileEnRad) / 2;	% en m/s
            this.ensonificationShipSpeedParam.Value = vitesse100 * this.Mps2Nds;	    % en noeuds
            this.ensonificationPitchParam.Value     = this.R2D * (vitesse100 - vitesseBateauImposeeEnMpS) / H;
            this.ensonificationYawParam.Value       = this.R2D * ((this.C * ouvertureLongitEmissionReception * this.D2R / 2) - vitesseBateauImposeeEnMpS) / (H *  tan(demiOuvertureMaxUtileEnRad));
        end
    end
end
