classdef ParametersComponent < handle
    %PARAM Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        pamesDialog
        
        % Components
        configurationComponent % Configuration Parameters Component
        signalArraysComponent  % Signal & Arrays Parameters Component
        environmentComponent   % Environment Parameters Component
        extSensorComponent     % Ext. Sensor Component
    end
    
    properties (Access = private)
        
    end
    
    methods
        function this = ParametersComponent(parentTab, pamesDialog)
            this.pamesDialog = pamesDialog;
            
            % create tab group Parameters Tab
            parametersTabGroup = uitabgroup(parentTab);
            % create ConfigurationParameters SubTab
            this.createConfigurationParametersTab(parametersTabGroup);
            % create Signal&ArraysParameters SubTab
            this.createSignalArraysTab(parametersTabGroup);
            % create EnvironmentParameters SubTab
            this.createEnvironmentParametersTab(parametersTabGroup);
            % create ExtSnsorsParameters SubTab
            this.createExtSensorsParametersTab(parametersTabGroup);
            
            % set standard/expert mode (editable/non-editable) to signal/arrays components
            configurationModel = this.pamesDialog.pamesModel.configurationModel;
            if configurationModel.modeValue == 1 % Standard mode
                this.signalArraysComponent.setEditableSignalArraysComponents('off');
            elseif  configurationModel.modeValue == 2 % Expert mode
                this.signalArraysComponent.setEditableSignalArraysComponents('on');
            end
        end
        
        function updateComponent(this)
            this.configurationComponent.updateComponent();
            this.signalArraysComponent.updateComponent();
            this.environmentComponent.updateComponent();
            this.extSensorComponent.updateComponent();
        end
    end
    
    methods (Access = private)
        
        function createConfigurationParametersTab(this, parentTabGroup)
            % create ConfigurationParameters SubTab
            
            configurationParametersTab = uitab(parentTabGroup, 'title', 'General Conifguration');
            
            % Get model
            configurationModel = this.pamesDialog.pamesModel.configurationModel;
            % create ConfigurationParameters Components
            this.configurationComponent = ConfigurationComponent(configurationParametersTab, configurationModel, this.pamesDialog);
        end
        
        
        function createSignalArraysTab(this, parentTabGroup)
            % create Signal&ArraysParameters SubTab
            
            signalArraysTab  = uitab(parentTabGroup, 'title', 'Signal & Arrays');
            
            % Get model
            signalArraysModel = this.pamesDialog.pamesModel.signalArraysModel;
            % create SignalArrays Components
            this.signalArraysComponent = SignalArraysComponent(signalArraysTab, signalArraysModel, this.pamesDialog);
        end
        
        function createEnvironmentParametersTab(this, parentTabGroup)
            % create Environment SubTab
            
            environmentParametersTab = uitab(parentTabGroup, 'title', 'Environment');
            
            % Get model
            environmentModel = this.pamesDialog.pamesModel.environmentModel;
            % create Environment Components
            this.environmentComponent = EnvironmentComponent(environmentParametersTab, environmentModel, this.pamesDialog);
        end
        
        function createExtSensorsParametersTab(this, parentTabGroup)
            % create ExtSensorsParameters SubTab
            
            extSensorParametersTab = uitab(parentTabGroup, 'title', 'Auxiliary sensors');
            
            % Get model
            extSensorModel = this.pamesDialog.pamesModel.extSensorModel;
            % create SignalArrays Components
            this.extSensorComponent = ExtSensorComponent(extSensorParametersTab, extSensorModel, this.pamesDialog);
        end
    end
end
