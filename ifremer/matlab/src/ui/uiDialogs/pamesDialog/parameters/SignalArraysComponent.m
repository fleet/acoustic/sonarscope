classdef SignalArraysComponent < handle & uiextras.VBox
    %SignalArraysComponent Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Constant)
    end
    
    properties
        pamesDialog % PamesDialog
        signalArraysModel
    end
    
    properties (Access = private)
        %% Signal & Arrays Tab Components
        signalComponents       = SimpleParametreComponent.empty; % Signal SimpleParametreComponent List
        coherentProcessingCheck                                  % Signal Coherent Processing Checkbox
        signalOptionalComponents       = SimpleParametreComponent.empty; % Signal Optional SimpleParametreComponent List
        txArraysComponents     = SimpleParametreComponent.empty; % Tx SimpleParametreComponent List
        txRadioButtonsComponent                                  % Tx Array Radio Button Component
        rxArraysComponents     = SimpleParametreComponent.empty; % Rx SimpleParametreComponent List
        rxRadioButtonsComponent                                  % Rx Array Radio Button Component
        otherArraysComponents  = SimpleParametreComponent.empty; % Other SimpleParametreComponent List
        
        %% Help Html
        nameHtmlArrayDirectivity = ['file://' fullfile(getNomFicDatabase('array_directivity.html'))];
        nameHtmlArrayPLBandWidth = ['file://' fullfile(getNomFicDatabase('duration_&_bandwith.html'))];
        nameHtmlArrayMaxRange    = ['file://' fullfile(getNomFicDatabase('maximum_range.html'))];
    end
    
    methods
        
        function this = SignalArraysComponent(parentTab, signalArraysModel, pamesDialog)
            % Constructor of the class
            
            % Call SuperConstructor
            this = this@uiextras.VBox('Parent', parentTab);
            
            this.signalArraysModel = signalArraysModel;
            this.pamesDialog = pamesDialog;
            
            % Help Button Box
            UiUtils.createHelpButtonBox(this, this.nameHtmlArrayDirectivity);
            
%             sz = [-2 -1 -1 25];
            
            %% Tx, Rx panels
            siTxRxVBox = uiextras.VBoxFlex('Parent', this, 'Spacing', 5);
            siPanel    = uipanel('Parent', siTxRxVBox, 'Title', 'Signal', 'FontSize', 13, 'FontWeight', 'bold');
            txPanel    = uipanel('Parent', siTxRxVBox, 'Title', 'Tx',     'FontSize', 13, 'FontWeight', 'bold');
            rxPanel    = uipanel('Parent', siTxRxVBox, 'Title', 'Rx',     'FontSize', 13, 'FontWeight', 'bold');
            otherPanel = uipanel('Parent', siTxRxVBox, 'Title', 'Other',  'FontSize', 13, 'FontWeight', 'bold');
            
            %% Signal grid
            siGrid = uiextras.Grid('Parent', siPanel, 'Spacing', 5); % 3 column grid
            
            % Signal Components
            for k=1:numel(this.signalArraysModel.signalParams)
                this.signalComponents(k) = SimpleParametreComponent(siGrid, 'param', this.signalArraysModel.signalParams(k));%, 'Sizes', sz);
                % Add a listener for Value change event
                addlistener(this.signalComponents(k), 'ValueChange', @this.paramValueChange);
            end
            % coherentProcessingBool CheckBox
            coherentProcessingBox = uiextras.HBox('Parent', siGrid, 'Spacing', 5);
            uicontrol('parent', coherentProcessingBox, 'Style',  'text', 'String', 'Coherent Processing');
            this.coherentProcessingCheck = uicontrol('parent', coherentProcessingBox, 'Style', 'checkbox', ...
                'Value', this.signalArraysModel.coherentProcessingBool, 'Callback', @this.coherentProcessingCallback);
            
            UiUtils.createHelpButton(coherentProcessingBox, this.nameHtmlArrayPLBandWidth);
            coherentProcessingBox.set('Sizes', [-2 -2 25]);
            
            
            % Signal Optional Components
            for k=1:numel(this.signalArraysModel.signalOptionalParams)
                this.signalOptionalComponents(k) = SimpleParametreComponent(siGrid, 'param', this.signalArraysModel.signalOptionalParams(k));%, 'Sizes', sz);
                % Show Hide Signal Optional Components
                if (this.signalArraysModel.coherentProcessingBool)
                    this.signalOptionalComponents(k).Visible = "on";
                else
                    this.signalOptionalComponents(k).Visible = "off";
                end
                % Add a listener for Value change event
                addlistener(this.signalOptionalComponents(k), 'ValueChange', @this.paramValueChange);
            end
            
            % Signal grid size
            siGrid.set('ColumnSizes', [-1 -1 -1], 'RowSizes', [25 25 25 25]);
            
            %% TX GRID
            txGrid = uiextras.Grid('Parent', txPanel, 'Spacing', 5); % 3 column grid
            for k=1:numel(this.signalArraysModel.txParams)
                this.txArraysComponents(k) = SimpleParametreComponent(txGrid, 'param', this.signalArraysModel.txParams(k));%, 'Sizes', sz);
                % Add a listener for Value change event
                addlistener(this.txArraysComponents(k), 'ValueChange', @this.paramValueChange);
            end
            % Plane / Ciruclar Tx Array Shape
            this.txRadioButtonsComponent = RadioButtonsComponent(txGrid, 'orientation', RadioButtonsComponent.Orientation{1}, ...
                'title', 'Tx Array Shape', 'titleButtons', this.signalArraysModel.txArrayShapeList, 'Init', this.signalArraysModel.txArrayShapeValue,...
                'helpHtml', this.nameHtmlArrayMaxRange, 'callbackFunc',@this.txArrayShapeCallback);
            % Tx grid size
            txGrid.set('ColumnSizes', [-1 -1 -1], 'RowSizes', [25 25 25] );
            
            %% RX GRID
            rxGrid = uiextras.Grid('Parent', rxPanel, 'Spacing', 5); % 3 column grid
            for k=1:numel(this.signalArraysModel.rxParams)
                this.rxArraysComponents(k) = SimpleParametreComponent(rxGrid, 'param', this.signalArraysModel.rxParams(k));%, 'Sizes', sz);
                % Add a listener for Value change event
                addlistener(this.rxArraysComponents(k), 'ValueChange', @this.paramValueChange);
            end
            
            % Plane / Ciruclar Tx Array Shape
            this.rxRadioButtonsComponent = RadioButtonsComponent(rxGrid, 'orientation', RadioButtonsComponent.Orientation{1},...
                'title', 'Rx Array Shape', 'titleButtons', this.signalArraysModel.rxArrayShapeList, 'Init', this.signalArraysModel.rxArrayShapeValue,...
                'helpHtml', this.nameHtmlArrayMaxRange, 'callbackFunc',@this.rxArrayShapeCallback);
            % Rx grid size
            rxGrid.set('ColumnSizes', [-1 -1 -1], 'RowSizes', [25 25 25]);
            
            %% Other GRID
            otherGrid = uiextras.Grid('Parent', otherPanel, 'Spacing', 5); % 3 column grid
            for k=1:numel(this.signalArraysModel.otherParams)
                this.otherArraysComponents(k) = SimpleParametreComponent(otherGrid, 'param', this.signalArraysModel.otherParams(k));%, 'Sizes', sz);
                % Add a listener for Value change event
                addlistener(this.otherArraysComponents(k), 'ValueChange', @this.paramValueChange);
            end
            % Rx grid size
            otherGrid.set('ColumnSizes', [-1 -1 -1], 'RowSizes', 25);
            
            %% Box  size
            
            siTxRxVBox.set('Sizes', [-4 -3 -3 -1.5]);
            
            this.set('Sizes', [25 -1]);
        end
        
        function updateComponent(this)
            
            %% Signal Components
            for k=1:numel(this.signalComponents)
                this.signalComponents(k).updateComponent();
            end
            this.coherentProcessingCheck.set('Value', this.signalArraysModel.coherentProcessingBool);
            
            % Show Hide Signal Optional Components
            for k=1:numel(this.signalArraysModel.signalOptionalParams)
                if (this.signalArraysModel.coherentProcessingBool)
                    this.signalOptionalComponents(k).Visible = "on";
                else
                    this.signalOptionalComponents(k).Visible = "off";
                end
            end
                
            %% Tx component
            for k=1:numel(this.txArraysComponents)
                this.txArraysComponents(k).updateComponent();
            end
            this.txRadioButtonsComponent.updateComponent(this.signalArraysModel.txArrayShapeValue);
            
            
            %% Rx component
            for k=1:numel(this.rxArraysComponents)
                this.rxArraysComponents(k).updateComponent();
            end
            this.rxRadioButtonsComponent.updateComponent(this.signalArraysModel.rxArrayShapeValue)
            
            %% Other Component
            for k=1:numel(this.otherArraysComponents)
                this.otherArraysComponents(k).updateComponent();
            end
        end
        
        
        function setEditableSignalArraysComponents(this, editable)
            % Set Editable / Non-Editable Signal/Tx/Rx Components
            
            % Signal Components
            for k = 1:numel(this.signalComponents)
                this.signalComponents(k).setEditable(editable);
            end
            this.coherentProcessingCheck.set('Enable', editable);
            
            % Tx Components
            for k = 1:numel(this.txArraysComponents)
                this.txArraysComponents(k).setEditable(editable);
            end
            
            this.txRadioButtonsComponent.setEditable(editable);
            
            % Rx Components
            for k = 1:numel(this.rxArraysComponents)
                this.rxArraysComponents(k).setEditable(editable);
            end
            
            this.rxRadioButtonsComponent.setEditable(editable);
            
            % Other Components
            for k = 1:numel(this.otherArraysComponents)
                this.otherArraysComponents(k).setEditable(editable);
            end
        end
    end
    
    
    methods (Access = private)
        
        %% Callbacks
        function coherentProcessingCallback(this, src, ~)
            % Callback coherentProcessingBool
            
            % coherentProcessingBool param
            this.signalArraysModel.coherentProcessingBool = src.Value;
            
            % Update models & components
            this.pamesDialog.update();
        end
        
        function txArrayShapeCallback(this, src, ~)
            % Callback txArrayShape
            
            if strcmp(src.String, this.signalArraysModel.txArrayShapeList{1})
                this.signalArraysModel.txArrayShapeValue = 1;
            elseif  strcmp(src.String, this.signalArraysModel.txArrayShapeList{2})
                this.signalArraysModel.txArrayShapeValue = 2;
            end
            
            % Update models & components
            this.pamesDialog.update();
        end
        
        function rxArrayShapeCallback(this, src, ~)
            % Callback rxArrayShape
            
            if strcmp(src.String, this.signalArraysModel.rxArrayShapeList{1})
                this.signalArraysModel.rxArrayShapeValue = 1;
            elseif  strcmp(src.String, this.signalArraysModel.rxArrayShapeList{2})
                this.signalArraysModel.rxArrayShapeValue = 2;
            end
            
            % Update models & components
            this.pamesDialog.update();
        end
        
        function paramValueChange(this, ~, ~)
            % Update models & components
            this.pamesDialog.update();
        end
    end
end
