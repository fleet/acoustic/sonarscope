classdef SignalArraysModel < handle
    %SIGNALARRAYSPARAMETERSMODEL Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Constant)
        C = 1500;
        txArrayShapeList = {'Plane', 'Circular'};
        rxArrayShapeList = {'Plane', 'Circular'};
    end
    
    properties
        
        pamesModel
        
        %% Signal Parameters
        signalParams           = ClParametre.empty;
        signalFrequencyParam   = ClParametre('Name', 'Frequency',          'Unit', 'kHz');
        waveLengthParam        = ClParametre('Name', 'Wavelength',         'Unit', 'm');
        emissionLevelParam     = ClParametre('Name', 'Source level',       'Unit', 'dB/mpa/1m');
        signalDurationParam    = ClParametre('Name', 'Signal Duration',    'Unit', 'ms');
        signalBandWidthParam   = ClParametre('Name', 'Signal Bandwidth',   'Unit', 'kHz');
        blindZoneParam         = ClParametre('Name', 'Near field',         'Unit', 'm'); % MHO : renommer en nearfieldParam
        rangeResolutionParam   = ClParametre('Name', 'Range Resolution',   'Unit', 'm');
        rangeSamplingParam     = ClParametre('Name', 'Range Sampling',     'Unit', 'm');
        samplingFrequencyParam = ClParametre('Name', 'Sampling Frequency', 'Unit', 'kHz');
        coherentProcessingBool = false;
        
        % Optional Parameters (visible only if coherentProcessingBool == true)
        signalOptionalParams        = ClParametre.empty;
        modulationBandWidthParam    = ClParametre('Name', 'Modulation Bandwidth', 'Unit', 'kHz');
        processingGainParam         = ClParametre('Name', 'Processing Gain', 'Unit', 'dB');
        
        %% TX Parameters
        txParams = ClParametre.empty;
        txBeamWidthAlongTrackParam   = ClParametre('Name', 'Tx Beam width Along Track',  'Unit', 'deg');
        txBeamWidthAcrossTrackParam  = ClParametre('Name', 'Tx Beam width Across Track', 'Unit', 'deg');
        txDirectivityIndexParam      = ClParametre('Name', 'Tx directivity index',       'Unit', 'dB');
        txArrayLengthParam           = ClParametre('Name', 'Tx Array length',            'Unit', 'm');
        txArrayWidthParam            = ClParametre('Name', 'Tx Array width',             'Unit', 'm');
        txShadingFactorAlongParam    = ClParametre('Name', 'Tx Shading Factor Along',    'Value', 1.3);
        txShadingFactorAcrossParam   = ClParametre('Name', 'Tx Shading Factor Across',   'Value', 1.3);
        txArrayTiltHorizontalParam   = ClParametre('Name', 'Tx Array Tilt / Horizontal', 'Unit', 'deg');
        
        txArrayShapeValue = 1; % Tx Array shape 1 = Plane / 2 = Circular
        
        %% RX Parameters
        rxParams = ClParametre.empty;
        rxBeamWidthAlongTrackParam  = ClParametre('Name', 'Rx Beam width Along Track',  'Unit', 'deg');
        rxBeamWidthAcrossTrackParam = ClParametre('Name', 'Rx Beam width Across Track', 'Unit', 'deg');
        rxDirectivityIndexParam     = ClParametre('Name', 'Rx directivity index',       'Unit', 'dB');
        rxArrayLengthParam          = ClParametre('Name', 'Rx Array length',            'Unit', 'm');
        rxArrayWidthParam           = ClParametre('Name', 'Rx Array width',             'Unit', 'm');
        rxShadingFactorAlongParam   = ClParametre('Name', 'Rx Shading Factor Along',    'Value', 1.3);
        rxShadingFactorAcrossParam  = ClParametre('Name', 'Rx Shading Factor Across',   'Value', 1.3);
        rxArrayTiltHorizontalParam  = ClParametre('Name', 'Rx Array Tilt / Horizontal', 'Unit', 'deg');
        rxArrayShapeValue           = 1; % Rx Array shape 1 = Plane / 2 = Circular
        
        %% Other Parameters
        otherParams = ClParametre.empty;
        receptionMaxAngleParam    = ClParametre('Name', 'Reception Max angle / Vertical', 'Unit', 'm');
        transducerEfficiencyParam = ClParametre('Name', 'Transducer Efficiency',          'Unit', '%', 'Value', 50);
        electricalPowerParam      = ClParametre('Name', 'Electrical power',               'Unit', 'kW');
        
        beamFormRxAngles = -75:75; % FIXME added with cl_sounder
    end
    
    properties (Access = private)
        
        %% Help Html
        nameHtmlArrayFreqWaleLeng = ['file://' fullfile(getNomFicDatabase('frequency_&_wave_length.html'))];
        nameHtmlArrayEmitLevel    = ['file://' fullfile(getNomFicDatabase('emitted_level.html'))];
        nameHtmlArrayPLBandWidth  = ['file://' fullfile(getNomFicDatabase('duration_&_bandwith.html'))];
        nameHtmlArraySampResol    = ['file://' fullfile(getNomFicDatabase('sampling_&_resolution.html'))];
        nameHtmlArrayArrayGain    = ['file://' fullfile(getNomFicDatabase('array_gain.html'))];
        nameHtmlArrayMaxRange     = ['file://' fullfile(getNomFicDatabase('maximum_range.html'))];
    end
    
    methods
        function this = SignalArraysModel(pamesModel)
            this.pamesModel = pamesModel;
            
%             %             %TODO  in ConfigurationModel OR SignalArraysModel
%             this.signalFrequencyParam = this.pamesModel.configurationModel.signalFrequencyParam;
            
            %% Set signalParams & Helps
            
            this.signalParams                = ClParametre.empty;
            this.signalFrequencyParam.Help  = this.nameHtmlArrayFreqWaleLeng;
            this.signalParams(end+1)         = this.signalFrequencyParam;
            this.waveLengthParam.Help        = this.nameHtmlArrayFreqWaleLeng;
            this.signalParams(end+1)         = this.waveLengthParam;
            this.emissionLevelParam.Help     = this.nameHtmlArrayEmitLevel;
            this.signalParams(end+1)         = this.emissionLevelParam;
            this.signalDurationParam.Help    = this.nameHtmlArrayPLBandWidth;
            this.signalParams(end+1)         = this.signalDurationParam;
            this.signalBandWidthParam.Help   = this.nameHtmlArrayPLBandWidth;
            this.signalParams(end+1)         = this.signalBandWidthParam;
            this.blindZoneParam.Help         = this.nameHtmlArrayPLBandWidth;
            this.signalParams(end+1)         = this.blindZoneParam;
            this.rangeResolutionParam.Help   = this.nameHtmlArrayPLBandWidth;
            this.signalParams(end+1)         = this.rangeResolutionParam;
            this.rangeSamplingParam.Help     = this.nameHtmlArraySampResol;
            this.signalParams(end+1)         = this.rangeSamplingParam;
            this.samplingFrequencyParam.Help = this.nameHtmlArraySampResol;
            this.signalParams(end+1)         = this.samplingFrequencyParam;
            
            %% Optional Signal Params
            
            this.signalOptionalParams = ClParametre.empty;
            this.modulationBandWidthParam.Help  = this.nameHtmlArrayPLBandWidth;
            this.signalOptionalParams(end+1)    = this.modulationBandWidthParam;
            this.processingGainParam.Help       = this.nameHtmlArrayPLBandWidth;
            this.signalOptionalParams(end+1)    = this.processingGainParam;
            
            
            %% TX Array Parameters
            
            this.txParams = ClParametre.empty;
            this.txBeamWidthAlongTrackParam.Help  = this.nameHtmlArraySampResol;
            this.txParams(end+1)                  = this.txBeamWidthAlongTrackParam;
            this.txBeamWidthAcrossTrackParam.Help = this.nameHtmlArraySampResol;
            this.txParams(end+1)                  = this.txBeamWidthAcrossTrackParam;
            this.txDirectivityIndexParam.Help     = this.nameHtmlArrayArrayGain;
            this.txParams(end+1)                  = this.txDirectivityIndexParam;
            this.txArrayLengthParam.Help          = this.nameHtmlArraySampResol;
            this.txParams(end+1)                  = this.txArrayLengthParam;
            this.txArrayWidthParam.Help           = this.nameHtmlArraySampResol;
            this.txParams(end+1)                  = this.txArrayWidthParam;
            this.txShadingFactorAlongParam.Help   = this.nameHtmlArraySampResol;
            this.txParams(end+1)                  = this.txShadingFactorAlongParam;
            this.txShadingFactorAcrossParam.Help  = this.nameHtmlArraySampResol;
            this.txParams(end+1)                  = this.txShadingFactorAcrossParam;
            this.txArrayTiltHorizontalParam.Help  = this.nameHtmlArrayMaxRange;
            this.txParams(end+1)                  = this.txArrayTiltHorizontalParam;
            
            %% RX Array Parameters
            
            this.rxParams = ClParametre.empty;
            this.rxBeamWidthAlongTrackParam.Help  = this.nameHtmlArraySampResol;
            this.rxParams(end+1)                  = this.rxBeamWidthAlongTrackParam;
            this.rxBeamWidthAcrossTrackParam.Help = this.nameHtmlArraySampResol;
            this.rxParams(end+1)                  = this.rxBeamWidthAcrossTrackParam;
            this.rxDirectivityIndexParam.Help     = this.nameHtmlArrayArrayGain;
            this.rxParams(end+1)                  = this.rxDirectivityIndexParam;
            this.rxArrayLengthParam.Help          = this.nameHtmlArraySampResol;
            this.rxParams(end+1)                  = this.rxArrayLengthParam;
            this.rxArrayWidthParam.Help           = this.nameHtmlArraySampResol;
            this.rxParams(end+1)                  = this.rxArrayWidthParam;
            this.rxShadingFactorAlongParam.Help   = this.nameHtmlArraySampResol;
            this.rxParams(end+1)                  = this.rxShadingFactorAlongParam;
            this.rxShadingFactorAcrossParam.Help  = this.nameHtmlArraySampResol;
            this.rxParams(end+1)                  = this.rxShadingFactorAcrossParam;
            this.rxArrayTiltHorizontalParam.Help  = this.nameHtmlArrayMaxRange;
            this.rxParams(end+1)                  = this.rxArrayTiltHorizontalParam;
            
            %% Other Parameters
            
            this.otherParams = ClParametre.empty;
            this.receptionMaxAngleParam.Help    = this.nameHtmlArrayMaxRange;
            this.otherParams(end+1)             = this.receptionMaxAngleParam;
            this.transducerEfficiencyParam.Help = this.nameHtmlArrayEmitLevel;
            this.otherParams(end+1)             = this.transducerEfficiencyParam;
            this.electricalPowerParam.Help      = this.nameHtmlArrayEmitLevel;
            this.otherParams(end+1)             = this.electricalPowerParam;
        end
        
        
        function initModel(this)
            
            %% Init signalParams
            for k=1:numel(this.signalParams)
                this.signalParams(k).Value = [];
            end
            
            this.coherentProcessingBool = false;
            this.modulationBandWidthParam.Value = [];
            this.processingGainParam.Value = [];  
            
            %% TX Array Parameters
            for k=1:numel(this.txParams)
                this.txParams(k).Value = [];
            end
            this.txShadingFactorAlongParam.Value = 1.3;
            this.txShadingFactorAcrossParam.Value = 1.3;
            this.txArrayShapeValue = 1; % Tx Array shape 1 = Plane / 2 = Circular
            
            %% RX Array Parameters
            for k=1:numel(this.rxParams)
                this.rxParams(k).Value = [];
            end
            this.rxShadingFactorAlongParam.Value = 1.3;
            this.rxShadingFactorAcrossParam.Value = 1.3;
            this.rxArrayShapeValue = 1; % Rx Array shape 1 = Plane / 2 = Circular
            
            %% Other Parameters
            this.receptionMaxAngleParam.Value = [];
            this.transducerEfficiencyParam.Value = 50;
            this.electricalPowerParam.Value = [];
            this.beamFormRxAngles = -75:75; % FIXME added with cl_sounder
        end
        
        function updateModel(this)
            sounder = this.pamesModel.sounder;
            if isempty(sounder)
                
                % Empty sounder object
                emptySounder = cl_sounder();
                emptySounder = emptySounder.set('Sonar.Ident', this.pamesModel.configurationModel.sounderTypeValue,...
                    'Sonar.Mode_1', this.pamesModel.configurationModel.mode1Value,...
                    'Sonar.Mode_2', this.pamesModel.configurationModel.mode2Value);
                
                if this.pamesModel.configurationModel.modeValue == 1 % CurrentSystem
                    %% Signal
                    this.signalFrequencyParam.Value = emptySounder.get('Sonar.Freq');
                    this.emissionLevelParam.Value   = emptySounder.get_NE();
                    emitedDuration = emptySounder.get_Duration();
                    % Verrue pour corriger sp�cificit� EM122 Deep o� la longueur d'impulsion
                    % peut �tre diff�rente pour chaque secteur
                    if length(emitedDuration) > 1
                        this.signalDurationParam.Value = min(emitedDuration); % Modif JMA le 13/04/2020
                    else
                        this.signalDurationParam.Value = emitedDuration;
                    end
                    coherentProcessingValue = emptySounder.get('Signal.Type'); % 1 false, 2 true
                    if  coherentProcessingValue == 1
                        this.coherentProcessingBool = false;
                    elseif coherentProcessingValue == 2
                        this.coherentProcessingBool = true;
                        BandWth = emptySounder.get('Signal.BandWth');       % Modif JMA le 13/04/2020
                        this.modulationBandWidthParam.Value = min(BandWth); % Modif JMA le 13/04/2020
                    end
                    this.rangeSamplingParam.Value = 1500 / (2 * min(emptySounder.get('SampFreq')));
                    % MaximumDepth =    get(a, 'MaxDepth'); TODO
                    
                    %% Arrays TX
                    this.txBeamWidthAlongTrackParam.Value  = emptySounder.get('BeamForm.Tx.LongWth');
                    this.txBeamWidthAcrossTrackParam.Value = emptySounder.get('TxBeamWidthAcrossTrack');
                    txArrayShape = emptySounder.get('Array.Tx.Type'); %{'1 = Rectangular'; '2 = Linear'; '3 = Cylinder'}
                    if txArrayShape == 1 || txArrayShape == 2
                        this.txArrayShapeValue = 1;
                    elseif txArrayShape == 3
                        this.txArrayShapeValue = 2;
                    end
                    this.txArrayTiltHorizontalParam.Value = emptySounder.get('Array.Tx.TransTilt');
                    
                    %% Arrays RX
                    this.rxBeamWidthAlongTrackParam.Value  = emptySounder.get('BeamForm.Rx.LongWth');
                    this.rxBeamWidthAcrossTrackParam.Value = emptySounder.get('BeamForm.Rx.TransWth');
                    %      RxArrayShape = emptySounder.get('Array.Rx.Type');
                    rxArrayShape = emptySounder.get('Array.Rx.Type'); %{'1 = Rectangular'; '2 = Linear'; '3 = Cylinder'}
                    if rxArrayShape == 1 || rxArrayShape == 2
                        this.rxArrayShapeValue = 1;
                    elseif rxArrayShape == 3
                        this.rxArrayShapeValue = 2;
                    end
                    this.beamFormRxAngles =  emptySounder.get('BeamForm.Rx.Angles');
                    this.rxArrayTiltHorizontalParam.Value = emptySounder.get('Array.Rx.TransTilt');
                    
                    %% Other
                    this.receptionMaxAngleParam.Value = emptySounder.get('ApertWth');
                    %       b.Coverage.NbSoundingsPerPing	= params.NbSoundingsPerPing; % TODO
                end
                
            else
                if this.pamesModel.configurationModel.modeValue == 1 % CurrentSystem
                    %% Signal
                    this.signalFrequencyParam.Value = sounder.get('Sonar.Freq');
                    this.emissionLevelParam.Value = sounder.get('level');
                    emitedDuration = sounder.get('duration'); % Can be a matrix
                    % Verrue pour corriger sp�cificit� EM122 Deep o� la longueur d'impulsion
                    % peut �tre diff�rente pour chaque secteur
                    if length(emitedDuration) > 1
                        this.signalDurationParam.Value = mean(emitedDuration);
                    else
                        this.signalDurationParam.Value = emitedDuration;
                    end
                    coherentProcessingValue = sounder.get('Signal.Type'); % 1 false, 2 true
                    if  coherentProcessingValue == 1
                        this.coherentProcessingBool = false;
                    elseif coherentProcessingValue == 2
                        this.coherentProcessingBool = true;
                        this.modulationBandWidthParam.Value = sounder.get('BandWth');
                    end
                    this.rangeSamplingParam.Value = sounder.get('RangeResol');
                    
                    
                    %% TX Arrays
                    this.txBeamWidthAlongTrackParam.Value= sounder.get('TxBeamWidthAlongTrack');
                    this.txBeamWidthAcrossTrackParam.Value = sounder.get('TxBeamWidthAcrossTrack');
                    
                    txArrayTilt	= sounder.get('TxArrayTilt');
                    if isempty(txArrayTilt)
                        this.txArrayTiltHorizontalParam.Value = 0;
                    else
                        this.txArrayTiltHorizontalParam.Value = txArrayTilt;
                    end
                    
                    txArrayShape = sounder.get('TxArrayType'); %{'1 = Rectangular'; '2 = Linear'; '3 = Cylinder'}
                    if txArrayShape == 1 || txArrayShape == 2
                        this.txArrayShapeValue = 1;
                    elseif txArrayShape == 3
                        this.txArrayShapeValue = 2;
                    end
                    
                    %% RX Arrays
                    this.rxBeamWidthAlongTrackParam.Value  = sounder.get('RxBeamWidthAlongTrack');
                    this.rxBeamWidthAcrossTrackParam.Value = sounder.get('RxBeamWidthAcrossTrack');
                    
                    rxArrayTilt	= sounder.get('RxArrayTilt');
                    if isempty(rxArrayTilt)
                        this.rxArrayTiltHorizontalParam.Value = 0;
                    else
                        this.rxArrayTiltHorizontalParam.Value = rxArrayTilt;
                    end
                    
                    rxArrayShape = sounder.get('RxArrayType'); %{'1 = Rectangular'; '2 = Linear'; '3 = Cylinder'}
                    if rxArrayShape == 1 || rxArrayShape == 2
                        this.rxArrayShapeValue = 1;
                    elseif rxArrayShape == 3
                        this.rxArrayShapeValue = 2;
                    end
                    
                    %% Other
                    this.receptionMaxAngleParam.Value = sounder.get('ReceptionMaximumAngle');
                end
            end
            
            
            % FIXME TODO ca na aucun sens
            % % TODO : faire en sorte qu'il y ait �gaalit� entre ces deux d�finitions
            % switch b.Arrays.RxArrayShape
            %     case 1 % rectangular
            %         b.Arrays.RxArrayShape = 2;
            %     case 2 % linear
            %         b.Arrays.RxArrayShape = 2;
            %     case 3 % cylindrical
            %         b.Arrays.RxArrayShape = 1;
            % end
            % switch b.Arrays.TxArrayShape
            %     case 1 % rectangular
            %         b.Arrays.TxArrayShape = 2;
            %     case 2 % linear
            %         b.Arrays.TxArrayShape = 2;
            %     case 3 % cylindrical
            %         b.Arrays.TxArrayShape = 1;
            % end
            
            
            
            %             % Verrue pour corriger sp�cificit� EM122 Deep o� la longueur d'impulsion
            %             % peut �tre diff�rente pour chaque secteur
            %             if length(emitedDuration) > 1
            %                 this.signalDurationParam.Value = mean(emitedDuration);
            %             end
            
            this.waveLengthParam.Value = this.C / (this.signalFrequencyParam.Value * 1000);
            
            if  this.coherentProcessingBool
                this.signalBandWidthParam.Value = this.modulationBandWidthParam.Value; % Sac de noeuds � v�rifier
                this.processingGainParam.Value = reflec_Enr2dB(this.signalDurationParam.Value * this.modulationBandWidthParam.Value);
                this.rangeResolutionParam.Value    = this.C / max(this.modulationBandWidthParam.Value * 1000) / 2; % TODO : max rajout� le 20/02/2013 pour mode ExtraDeep de l'EM710 en Single Swath
            else
                this.modulationBandWidthParam.Value	= 1 / this.signalDurationParam.Value;
                this.signalBandWidthParam.Value = this.modulationBandWidthParam.Value; % Sac de noeuds � v�rifier
                this.processingGainParam.Value = 0;
                this.rangeResolutionParam.Value = this.C * (this.signalDurationParam.Value / 1000) / 2;
            end
            
            this.blindZoneParam.Value = this.C * (this.signalDurationParam.Value / 1000) / 2;
            if isempty(this.samplingFrequencyParam.Value)
                this.samplingFrequencyParam.Value      = this.C / (2 * 1000 * this.rangeSamplingParam.Value);
            elseif isempty(this.rangeSamplingParam.Value)
                this.rangeSamplingParam.Value = this.C / (2 * 1000 * this.samplingFrequencyParam.Value);
            else
                this.samplingFrequencyParam.Value = this.C / (2 * 1000 * this.rangeSamplingParam.Value);
            end
            
            %% Param�tres de la frame Arrays AntenneLong2Teta
            
            % Tx& Rx array length & width
            if isempty(this.txArrayLengthParam.Value)
                this.txArrayLengthParam.Value = this.antenneTeta2Long(...
                    this.waveLengthParam.Value, ...
                    this.txBeamWidthAlongTrackParam.Value, ...
                    this.txShadingFactorAlongParam.Value) ;
            end
            if isempty(this.txArrayWidthParam.Value)
                this.txArrayWidthParam.Value  = this.antenneTeta2Long(...
                    this.waveLengthParam.Value, ...
                    this.txBeamWidthAcrossTrackParam.Value, ...
                    this.txShadingFactorAcrossParam.Value) ;
            end
            if isempty(this.rxArrayLengthParam.Value)
                this.rxArrayLengthParam.Value = this.antenneTeta2Long(...
                    this.waveLengthParam.Value, ...
                    this.rxBeamWidthAlongTrackParam.Value, ...
                    this.rxShadingFactorAlongParam.Value) ;
            end
            if isempty(this.rxArrayWidthParam.Value)
                this.rxArrayWidthParam.Value  = this.antenneTeta2Long(...
                    this.waveLengthParam.Value, ...
                    this.rxBeamWidthAcrossTrackParam.Value, ...
                    this.rxShadingFactorAcrossParam.Value) ;
            end
            
            % Tx & Rx width along & across
            if isempty( this.txBeamWidthAlongTrackParam.Value)
                this.txBeamWidthAlongTrackParam.Value = this.antenneTeta2Long(...
                    this.waveLengthParam.Value, ...
                    this.txArrayLengthParam.Value, ...
                    this.txShadingFactorAlongParam.Value) ;
            end
            if isempty( this.txBeamWidthAcrossTrackParam.Value)
                this.txBeamWidthAcrossTrackParam.Value = this.antenneTeta2Long( ...
                    this.waveLengthParam.Value, ...
                    this.txArrayWidthParam.Value, ...
                    this.txShadingFactorAcrossParam.Value) ;
            end
            if isempty( this.rxBeamWidthAlongTrackParam.Value)
                this.rxBeamWidthAlongTrackParam.Value = this.antenneTeta2Long( ...
                    this.waveLengthParam.Value, ...
                    this.rxArrayLengthParam.Value, ...
                    this.rxShadingFactorAlongParam.Value) ;
            end
            if isempty( this.rxBeamWidthAcrossTrackParam.Value)
                this.rxBeamWidthAcrossTrackParam.Value = this.antenneTeta2Long( ...
                    this.waveLengthParam.Value, ...
                    this.rxArrayWidthParam.Value, ...
                    this.rxShadingFactorAcrossParam.Value) ;
            end
            
            this.txDirectivityIndexParam.Value = IndexDirectivite(this.txBeamWidthAlongTrackParam.Value, this.txBeamWidthAcrossTrackParam.Value);
            this.rxDirectivityIndexParam.Value	= IndexDirectivite(this.rxBeamWidthAlongTrackParam.Value, this.rxBeamWidthAcrossTrackParam.Value);
            this.electricalPowerParam.Value	= 1e-3 * reflec_dB2Enr( this.emissionLevelParam.Value - 171 - reflec_Enr2dB(this.transducerEfficiencyParam.Value / 100.) - this.txDirectivityIndexParam.Value);
        end
    end
    
    methods (Access = private)
        % Longueur d'une antenne lineaire non ponderee en fonction de sa longueur
        %
        % Syntax
        %   L = AntenneLong2Teta(WaveLength, teta)
        %   L = AntenneLong2Teta(WaveLength, teta, shadingFactor)
        %
        % Input Arguments
        %   WaveLength : Longueur d'onde du signal (m)
        %   teta      : Angle d'ouverture a -3dB (deg)
        %
        % Name-Value Pair Arguments
        %   shadingFactor : Coefficient ponderateur
        %
        % Output Arguments
        %   L          : Longueur de l'antenne (m)
        %
        % Examples
        %   WaveLength = 1500 / 13e3
        %   teta = AntenneTeta2Long(WaveLength, 1.5)
        %
        % See also AntenneLong2Teta Authors
        % Authors : JMA + XL
        %-------------------------------------------------------------------------------
        
        function L = antenneTeta2Long(~, WaveLength, teta, varargin)
            L = WaveLength ./ teta * 65;	% 65. = 50. * 1.3
            if nargin == 4
                shadingFactor = varargin{1};
                L = L * shadingFactor;
            end
        end
    end
end
