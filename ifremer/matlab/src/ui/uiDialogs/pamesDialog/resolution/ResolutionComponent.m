classdef ResolutionComponent < handle & uiextras.VBox
    %CONFIGURATIONPARAMETERSCOMPONENT Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Constant)
    end
    
    properties
        resolutionModel = ResolutionModel.empty;
        pamesDialog % PamesDialog
        
        signalAxes
        acrossDistLine
        
        parametersComponents1 = SimpleParametreComponent.empty;
        parametersComponents2 = SimpleParametreComponent.empty;
    end
    
    properties (Access = private)
        %% Components
    end
    
    methods
        
        function this = ResolutionComponent(parentTab, resolutionModel, pamesDialog)
            % Constructor of the class
            
            % Call SuperConstructor
            this = this@uiextras.VBox('Parent', parentTab, 'Spacing', 5);
            
            this.resolutionModel = resolutionModel;
            this.pamesDialog = pamesDialog;
            
            % head part
            this.createHeadPart();
            
            % axe part
            this.createAxePart();
            
            % paramameters part
            this.createParametersPart();
            
            this.set('Sizes', [-5 -65 105]);
        end
        
        
        function updateComponent(this)
            % delete old line
            delete(this.signalAxes.Children);
            
            % Re-Plot Signal
            this.resolutionModel.resoSignal.plot( 'axesArray', this.signalAxes);
            this.signalAxes.set('XLimMode', 'auto', 'YLimMode', 'auto');
            
                        % Across Distance Line
            X = [this.resolutionModel.distanceParam.Value this.resolutionModel.distanceParam.Value];
            this.acrossDistLine = line (this.signalAxes, X, this.signalAxes.get('YLim'), 'Color', [0 0 0]);
            
            % Update Component
            for k=1:numel(this.parametersComponents1)
                this.parametersComponents1(k).updateComponent();
            end
            for k=1:numel(this.parametersComponents2)
                this.parametersComponents2(k).updateComponent();
            end
        end
    end
    
    
    methods (Access = private)
        
        function createHeadPart(this)
            headHBox = uiextras.HBox('Parent', this, 'Spacing', 0);
            
            titleStyle = ColorAndFontStyle.getTitleColorAndFontStyle; % - Style of title of dialog box
            
            title = 'Bathymetry and Imagery Resolutions';
            % Create TitleText
            uicontrol('Style', 'text', 'Parent', headHBox, ...
                'string',          title, ...
                'BackgroundColor', titleStyle.BackgroundColor, ...
                'ForegroundColor', titleStyle.ForegroundColor, ...
                'FontName',        titleStyle.FontName, ...
                'FontSize',        titleStyle.FontSize, ...
                'FontUnits',       titleStyle.FontUnits, ...
                'FontWeight',      titleStyle.FontWeight, ...
                'FontAngle',       titleStyle.FontAngle,...
                'HorizontalAlignment', 'center');
        end
        
        function createAxePart(this)
            axesVBox = uiextras.VBox('Parent', this, 'Spacing', 5);
            
            % Axes
            this.signalAxes = axes('Parent',axesVBox, 'Units', 'normalized');
            [tb,~] = axtoolbar(this.signalAxes, {'datacursor', 'pan', 'zoomin','zoomout','restoreview'});
            tb.Visible = 'on';
            
            % Plot Signal
            this.resolutionModel.resoSignal.plot( 'axesArray', this.signalAxes);
            xlabel(this.signalAxes, 'Across Track Range (m)');
            ylabel(this.signalAxes, 'Resolution (m)');
            
            
            % Accors Distance Line
            X = [this.resolutionModel.distanceParam.Value this.resolutionModel.distanceParam.Value];
            this.acrossDistLine = line (this.signalAxes, X, this.signalAxes.get('YLim'), 'Color', [0 0 0]);
        end
        
        function createParametersPart(this)
            parametersPanel = uipanel('Parent', this, 'Title', 'Parameters', 'FontSize', 13, 'FontWeight', 'bold');
            parametersHBox = uiextras.HBox('Parent', parametersPanel, 'Spacing', 5);
            
%             sz = [-2 -2 -1 0]; % Size of SimpleParametreComponent
            
            %% parameters1 Panel
            parameters2Panel = uipanel('Parent', parametersHBox);
            vBox1 = uiextras.VBox('Parent', parameters2Panel, 'Spacing', 0);
            
            this.parametersComponents1(1) = SimpleParametreComponent(vBox1, 'param', this.resolutionModel.depthParam);%,    'Sizes', sz);
            this.parametersComponents1(2) = SimpleParametreComponent(vBox1, 'param', this.resolutionModel.distanceParam);%, 'Sizes', sz);
            this.parametersComponents1(3) = SimpleParametreComponent(vBox1, 'param', this.resolutionModel.angleParam);%,    'Sizes', sz);
            % Add a listener for Value change event
            for k=1:3
                addlistener(this.parametersComponents1(k), 'ValueChange', @this.paramValueChange);
            end
            
            vBox1.set('Sizes', [25 25 25]);
            
            %% parameters2 Panel
            parameters2Panel = uipanel('Parent', parametersHBox);
            vBox2 = uiextras.VBox('Parent', parameters2Panel, 'Spacing', 0);
            
            style(1)  = ColorAndFontStyle('BackgroundColor', [0 0 1], 'ForegroundColor', 'w');
            style(2)  = ColorAndFontStyle('BackgroundColor', [1 0 0], 'ForegroundColor', 'k');
            style(3)  = ColorAndFontStyle('BackgroundColor', [0 1 0], 'ForegroundColor', 'k');
            
            this.parametersComponents2(1) = SimpleParametreComponent(vBox2, 'param', this.resolutionModel.alongTrackParam,       'editable', false, 'style', style(1));%, 'Sizes', sz);
            this.parametersComponents2(2) = SimpleParametreComponent(vBox2, 'param', this.resolutionModel.bathyAcrossTrackParam, 'editable', false, 'style', style(2));%, 'Sizes', sz);
            this.parametersComponents2(3) = SimpleParametreComponent(vBox2, 'param', this.resolutionModel.imageAcrossTrackParam, 'editable', false, 'style', style(3));%, 'Sizes', sz);
            
            vBox2.set('Sizes', [25 25 25]);
            
            %%
            parametersHBox.set('Sizes', [-1 -1]);
        end
        
        %%Callback
        function selectIntercationCallback(~, src, ~)
            disp(src.String);
        end
        
        function paramValueChange(this, ~, ~)
            % Update models & components
           this.pamesDialog.update();
        end
    end
end
