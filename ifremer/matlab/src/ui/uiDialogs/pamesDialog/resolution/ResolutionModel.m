classdef ResolutionModel < handle
    %RESOLUTIONMODEL Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        pamesModel
        signalArraysModel
        
        %% params
        depthParam    = ClParametre('Name', 'Depth',           'Unit', 'm');
        distanceParam = ClParametre('Name', 'Across Distance', 'Unit', 'm');
        %  maxWidth   = ClParametre('Name', 'Reception Max angle / Vertical', 'Unit', 'm');
        angleParam    = ClParametre('Name', 'Beam Angle',      'Unit', 'deg');
        
        alongTrackParam       = ClParametre('Name', 'Bathy / Image Along Track', 'Unit', 'm');
        bathyAcrossTrackParam = ClParametre('Name', 'Bathymetry Acorss Track',   'Unit', 'm');
        imageAcrossTrackParam = ClParametre('Name', 'Image Acorss Track',        'Unit', 'm');
        
        %% Resolution Signal
        xSample                 = XSample('name', 'Accross Track Range', 'unit', 'm');
        ySample                 = YSample.empty;
        fondLongitSample        = YSample('name', 'FondLongit',        'color', 'b');
        fondTransvImagerySample = YSample('name', 'FondTransvImagery', 'color', 'g');
        fondTransvBathySample   = YSample('name', 'FondTransvBathy',   'color', 'r');
        resoSignal              = ClSignal('name', 'Bathymetry and Image Resolutions');
    end
    
        properties (Access = private)
            
             %% Help Html
             nameHtmlMaximumRange = ['file://' fullfile(getNomFicDatabase('maximum_range.html'))];
             nameHtmlBathyResol   = ['file://' fullfile(getNomFicDatabase('bathymetry_resolution.html'))];
        end
    
    methods
        
        function this = ResolutionModel(pamesModel)
            this.pamesModel = pamesModel;
            this.signalArraysModel = pamesModel.signalArraysModel;
            
            % Set help link
            this.depthParam.Help  = this.nameHtmlMaximumRange;
            this.distanceParam.Help  = this.nameHtmlMaximumRange;
            this.angleParam.Help  = this.nameHtmlMaximumRange;
            
            this.alongTrackParam.Help  = this.nameHtmlBathyResol;
            this.bathyAcrossTrackParam.Help  = this.nameHtmlBathyResol;
            this.imageAcrossTrackParam.Help  = this.nameHtmlBathyResol;
            
            % Define Resolution signal
            this.ySample(1) = this.fondLongitSample;
            this.ySample(2) = this.fondTransvImagerySample;
            this.ySample(3) = this.fondTransvBathySample;
            this.resoSignal.set('xSample', this.xSample, 'ySample',this.ySample);
            this.resoSignal.setDefaultXSampleIfNeeded();
        end
        
        function initModel(this)
            this.depthParam.Value    = [];
            this.distanceParam.Value = [];
            % maxWidth.Value         = [];
            this.angleParam.Value   = [];
            
            this.alongTrackParam.Value       = [];
            this.bathyAcrossTrackParam.Value = [];
            this.imageAcrossTrackParam.Value = [];
 
            %% Resolution Signal
            this.xSample.data                 = [];
            this.fondLongitSample.data        = [];
            this.fondTransvImagerySample.data = [];
            this.fondTransvBathySample.data   = [];
        end
        
        function updateModel(this)

            % Compute params
            this.calculer_resol_param();
            
            % Compute curves (fondLongit, fondTransvImagery, fondTransvBathy)
            this.calculer_resol_courbes();
        end
        
        % Calcul de la resolution longitudinale
        function resolFondLongit = resolLong(this, Z, T)
            
            size_Z = size(Z);
            size_T = size(T);
            if ~isequal(size_Z, size_T)
                if (length(Z) ~= 1) && isvector(Z)
                    Z = repmat(Z, 1, size_T(2));
                end
                if (length(T) ~= 1) && isvector(T)
                    T = repmat(T, size_Z(1), 1);
                end
            end
            
            distanceOblique     = Z ./cosd(T);
            ouvertureLongitEmissionReception = this.pamesModel.antenneOuvEmiRec(this.signalArraysModel.txBeamWidthAlongTrackParam.Value, this.signalArraysModel.rxBeamWidthAlongTrackParam.Value);
            resolFondLongit = (ouvertureLongitEmissionReception * pi / 180) * distanceOblique;
        end
        
        % Calcul de la resolution transversale de l'imagerie
        function resolFondTransvImagery = resolTransIma(this, Z, T)
            
            size_Z = size(Z);
            size_T = size(T);
            if ~isequal(size_Z, size_T)
                if (length(Z) ~= 1) && isvector(Z)
                    Z = repmat(Z, 1, size_T(2));
                end
                if (length(T) ~= 1) && isvector(T)
                    T = repmat(T, size_Z(1), 1);
                end
            end
            
            distanceOblique = Z ./ cosd(T);
            % resolFondTransvImagery = min(b.Signal.RangeResolution ./ sind(T), ...
            %                                distanceOblique ./ cosd(T) * (b.Arrays.RxBeamWidthAcrossTrack * (pi/180)));
            % Modif GLA 14/12/2009
            
            resolFondTransvImagery = NaN(size(T));
            sub = isnan(T);
            resolFondTransvImagery(~sub) = min(this.signalArraysModel.rangeResolutionParam.Value ./ sind(T(~sub)), ...
                distanceOblique(~sub) ./ cosd(T(~sub)) * (this.signalArraysModel.rxBeamWidthAcrossTrackParam.Value * (pi/180)));
            resolFondTransvImagery(sub) = distanceOblique(sub) ./ cosd(T(sub)) * (this.signalArraysModel.rxBeamWidthAcrossTrackParam.Value * (pi/180));
        end
    end
    
    
    methods (Access = private)
        
        % Calcul des valeurs, parametres et courbes de resolution
        function calculer_resol_param(this)
            
            if isempty(this.depthParam.Value)
                this.depthParam.Value = this.pamesModel.configurationModel.sounderMaximumDepth / 2;
            end
            
            if isempty(this.distanceParam.Value)
                width    = this.pamesModel.calculFauchee(this.depthParam.Value, 1:4);
                width    = max(width);
                this.distanceParam.Value = width / 2;
            end
            
            this.angleParam.Value              = atand(this.distanceParam.Value / this.depthParam.Value);
            this.alongTrackParam.Value         = this.resolLong(this.depthParam.Value, this.angleParam.Value);
            this.bathyAcrossTrackParam.Value   = this.resolTransBathy(this.depthParam.Value, this.angleParam.Value);
            this.imageAcrossTrackParam.Value    = this.resolTransIma(this.depthParam.Value, this.angleParam.Value);
        end
        
        % Calcul des courbes de resolution
        function calculer_resol_courbes(this)
            
            width = this.pamesModel.calculFauchee(this.depthParam.Value, 1:4);
            %             this.maxWidth = max(width);
            maxWidth = max(width);
            
            this.xSample.data = linspace (0, maxWidth, 500);
            this.xSample.data(1) = [];
            theta = atand(this.xSample.data / this.depthParam.Value);
            
            this.fondLongitSample.data         = this.resolLong(this.depthParam.Value, theta);
            this.fondTransvImagerySample.data  = this.resolTransIma(this.depthParam.Value, theta);
            this.fondTransvBathySample.data    = this.resolTransBathy(this.depthParam.Value, theta);
        end
        
        % Calcul de la resolution transversale de la bathymetrie
        function resolTransBathy = resolTransBathy(this, Z, T)
            
            size_Z = size(Z);
            size_T = size(T);
            if ~isequal(size_Z, size_T)
                if (length(Z) ~= 1) && isvector(Z)
                    Z = repmat(Z, 1, size_T(2));
                end
                if (length(T) ~= 1) && isvector(T)
                    T = repmat(T, size_Z(1), 1);
                end
            end
            
            ouvertureTransvEmissionReception = this.pamesModel.antenneOuvEmiRec(this.signalArraysModel.txBeamWidthAcrossTrackParam.Value, this.signalArraysModel.rxBeamWidthAcrossTrackParam.Value);
            resolTransBathy = Z * (ouvertureTransvEmissionReception * (pi / 180)) ./ (cosd(T) .^ 2);
        end
    end
end
