classdef SwathComponent < handle & uiextras.VBox
    %CONFIGURATIONPARAMETERSCOMPONENT Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Constant)
        
    end
    
    properties
        swathModel            = SwathModel.empty;
        pamesDialog % PamesDialog
        
        signalAxes
        depthLine
        
        generalComponents     = SimpleParametreComponent.empty;
        widthComponents       = SimpleParametreComponent.empty;
        angleComponents       = SimpleParametreComponent.empty;
        floorCheckboxs  = gobjects
        limComponents       = SimpleParametreComponent.empty;
        limCheckboxs = gobjects
        
        bathyAccuIHOText
        posAccuIHOText
        
        bathyPosBox
    end
    
    properties (Access = private)
        %% Components
    end
    
    methods
        
        function this = SwathComponent(parentTab, swathModel, pamesDialog)
            % Constructor of the class
            
            % Call SuperConstructor
            this = this@uiextras.VBox('Parent', parentTab, 'Spacing', 5);
            
            this.swathModel = swathModel;
            this.pamesDialog = pamesDialog;
            
            % head part
            this.createHeadPart();
            
            % axe part
            this.createAxePart();
            
            % paramameters part
            this.createParametersPart();
            
            this.set('Sizes', [-5 -65 155]);
        end
        
        
        function updateComponent(this)
            % delete old line
            delete(this.signalAxes.Children);
            
            
            % Compute/Display lim patch
            for k=1:numel(this.limComponents)
                if this.limCheckboxs(k).Value == 1
                    [px, py] = this.swathModel.get_patch(this.limComponents(k).param);
                    patch (this.signalAxes, px, py, [.9 .9 .9]);
                    patch (this.signalAxes, px, -py, [.9 .9 .9]);
                end
            end
            
            % Re-Plot Signal
            this.swathModel.swathSignal.plot( 'axesArray', this.signalAxes);
            
            % Show / hide seafloor lines
            lineListReverted   = findobj(this.signalAxes, 'Type', 'Line');
            % revert ordre because wrong ordre by default
            lineList = flipud(lineListReverted);
            for lineIndex=1:4
                if this.floorCheckboxs(lineIndex).Value == 1
                    lineList(2*lineIndex-1).Visible = 'on';
                    lineList(2*lineIndex).Visible = 'on';
                else
                    lineList(2*lineIndex-1).Visible = 'off';
                    lineList(2*lineIndex).Visible = 'off';
                end
            end
            
            % XLim YLim
            % Compute minMax
            minMax = this.swathModel.getMinMax();
            this.signalAxes.set('XLim', [ minMax(3) this.swathModel.swathMaxDepthParam.Value], 'YLim', minMax(1:2));
            
            % Depth Line
            X = [this.swathModel.swathDepthParam.Value this.swathModel.swathDepthParam.Value];
            this.depthLine = line (this.signalAxes, X, minMax(1:2), 'Color', [0 0 0]);
            
            % Update Component
            for k=1:numel(this.generalComponents)
                this.generalComponents(k).updateComponent();
            end
            for k=1:numel(this.widthComponents)
                this.widthComponents(k).updateComponent();
            end
            for k=1:numel(this.angleComponents)
                this.angleComponents(k).updateComponent();
            end
            
            
            for k=1:numel(this.limComponents)
                this.limComponents(k).updateComponent();
            end
            
            % Optional text
            this.bathyAccuIHOText.set('String' ,this.swathModel.bathyAccuIHOValue);
            this.posAccuIHOText.set('String' ,this.swathModel.posAccuIHOValue);
            
            % Show/hide component depending IHO value
            if (this.swathModel.ihoValue == 0)
                this.limComponents(4:5).set('Sizes', [-2 -1 -1 0])
                this.bathyPosBox.set('Sizes', [-1 0]);
            else
                this.limComponents(4:5).set('Sizes', [-2 0 0 0])
                this.bathyPosBox.set('Sizes', [-1 -1]);
            end   
        end
    end
    
    methods (Access = protected)
        
        function createHeadPart(this)
            headHBox = uiextras.HBox('Parent', this, 'Spacing', 0);
            
            
            titleStyle   = ColorAndFontStyle.getTitleColorAndFontStyle; % - Style of title of dialog box
            
            title = 'Swath Width';
            % Create TitleText
            uicontrol('Style', 'text', 'Parent', headHBox, ...
                'string',          title, ...
                'BackgroundColor', titleStyle.BackgroundColor, ...
                'ForegroundColor', titleStyle.ForegroundColor, ...
                'FontName',        titleStyle.FontName, ...
                'FontSize',        titleStyle.FontSize, ...
                'FontUnits',       titleStyle.FontUnits, ...
                'FontWeight',      titleStyle.FontWeight, ...
                'FontAngle',       titleStyle.FontAngle,...
                'HorizontalAlignment', 'center');
        end
        
        function createAxePart(this)
            axesVBox = uiextras.VBox('Parent', this, 'Spacing', 5);
            
            % Axes
            this.signalAxes = axes('Parent',axesVBox, 'Units', 'normalized');
            [tb,~] = axtoolbar(this.signalAxes, {'datacursor', 'pan', 'zoomin','zoomout','restoreview'});
            tb.Visible = 'on';
            % Plot Signal
            this.swathModel.swathSignal.plot( 'axesArray', this.signalAxes);
            
            % Depth Line
            Y = get(this.signalAxes, 'YLim');
            X = [this.swathModel.swathDepthParam.Value this.swathModel.swathDepthParam.Value];
            this.depthLine = line (this.signalAxes, X, Y, 'Color', [0 0 0]);
            
            this.signalAxes.set('yaxisLocation','right')
            ylabel(this.signalAxes, 'Across Track Range (m)');
            xlabel(this.signalAxes, 'Depth (m)');
            
            % Rotate the axes to 90deg (because signal vertical)
            camroll(this.signalAxes, -90);
            
            % IHO BOx
            ihoHBox = uiextras.HBox('Parent', axesVBox, 'Spacing', 5);
            
            %             ihoRadioButtonsComponent = RadioButtonsComponent(ihoHBox, 'orientation', RadioButtonsComponent.Orientation{1},...
            RadioButtonsComponent(ihoHBox, 'orientation', RadioButtonsComponent.Orientation{1},...
                'title', 'IHO', 'titleButtons', this.swathModel.ihoList, 'Init', this.swathModel.ihoValue ,'callbackFunc',@this.ihoRadioButtonsComponentCallback);
            uiextras.Empty('Parent', ihoHBox);
            
            axesVBox.set('Sizes', [-1 25]);
        end
        
        function createParametersPart(this)
            parametersPanel = uipanel('Parent', this, 'Title', 'Parameters', 'FontSize', 13, 'FontWeight', 'bold');
            parametersHBox = uiextras.HBox('Parent', parametersPanel, 'Spacing', 10);
            
            %% General Params Panel
            generalPanel = uipanel('Parent', parametersHBox);
            vBox1 = uiextras.VBox('Parent', generalPanel, 'Spacing', 0);
            
%             sz = [-2 -2 -1 0];
            for k=1:3
                this.generalComponents(k) = SimpleParametreComponent(vBox1, 'param', this.swathModel.swathGeneralParams(k)); %, 'Sizes', sz);
                % Add a listener for Value change event
                addlistener(this.generalComponents(k), 'ValueChange', @this.paramValueChange);
            end
            
            vBox1.set('Sizes', [25 25 25]);
            
            %% Width / Angle Params Panel
            widthAnglePanel = uipanel('Parent', parametersHBox);
            vBox2 = uiextras.VBox('Parent', widthAnglePanel, 'Spacing', 0);
            
            vBox2TitleBox = uiextras.HBox('Parent', vBox2, 'Spacing', 0);
            uicontrol('Style', 'text', 'Parent', vBox2TitleBox, ...
                'string', 'Bottom Type','HorizontalAlignment', 'center');
            uicontrol('Style', 'text', 'Parent', vBox2TitleBox, ...
                'string', 'Total swath width (m)','HorizontalAlignment', 'center');
            uicontrol('Style', 'text', 'Parent', vBox2TitleBox, ...
                'string', 'Outer beam angle (deg)','HorizontalAlignment', 'center');
            vBox2TitleBox.set('Sizes', [-40 -30 -30]);
            
            sz_1 = [-1 -1 0 0];
            sz_2 = [0 -1 0 0];
            
            style(1) = ColorAndFontStyle('BackgroundColor', [1 0 0], 'ForegroundColor', 'k');
            style(2) = ColorAndFontStyle('BackgroundColor', [0 1 0], 'ForegroundColor', 'k');
            style(3) = ColorAndFontStyle('BackgroundColor', [0 0 1], 'ForegroundColor', 'w');
            style(4) = ColorAndFontStyle('BackgroundColor', [0 0 0], 'ForegroundColor', 'w');
            for k=1:4
                parameters2HBox = uiextras.HBox('Parent', vBox2, 'Spacing', 0);
                
                
                this.floorCheckboxs(k) = uicontrol('parent',parameters2HBox, 'Style', 'checkbox', 'Value', 1, 'HorizontalAlignment', 'center', 'Callback', @(src,eventdata)showTypeCallback(this,src,eventdata));
                this.widthComponents(k) = SimpleParametreComponent(parameters2HBox, 'param', this.swathModel.swathWidthParams(k), 'editable', false, 'Sizes', sz_1, 'style', style(k));
                this.angleComponents(k) = SimpleParametreComponent(parameters2HBox, 'param', this.swathModel.swathAngleParams(k), 'editable', false, 'Sizes', sz_2);
                parameters2HBox.set('Sizes', [25 -65 -25]);
            end
            vBox2.set('Sizes', [25 25 25 25 25]);
            
            %% Seuil Panel
            seuilPanel = uipanel('Parent', parametersHBox);
            vBox3 = uiextras.VBox('Parent', seuilPanel, 'Spacing', 0);
            
            for k=1:3
                hbox = uiextras.HBox('Parent',vBox3, 'Spacing', 0);
                this.limCheckboxs(k) = uicontrol('parent',hbox, 'Style', 'checkbox', 'Value', 0, 'HorizontalAlignment', 'center', 'Callback', @(src,eventdata)showLimCallback(this,src,eventdata));
%                 sz = [-2 -1 -1 0];
                this.limComponents(k) = SimpleParametreComponent(hbox, 'param', this.swathModel.limParams(k));%, 'Sizes', sz);
                % Add a listener for Value change event
                addlistener(this.limComponents(k), 'ValueChange', @this.paramValueChange);
                hbox.set('Sizes', [25 -1]);
            end
            
            % Bathy / Pos Parameters
            this.bathyPosBox = uiextras.HBox('Parent', vBox3, 'Spacing', 0);
            % Mandatory
            bathyPosSeuilBox = uiextras.VBox('Parent', this.bathyPosBox, 'Spacing', 0);
            for k=4:5
                hbox = uiextras.HBox('Parent',bathyPosSeuilBox, 'Spacing', 0);
                this.limCheckboxs(k) = uicontrol('parent',hbox, 'Style', 'checkbox', 'Value', 0, 'HorizontalAlignment', 'center', 'Callback', @(src,eventdata)showLimCallback(this,src,eventdata));
                this.limComponents(k) = SimpleParametreComponent(hbox, 'param', this.swathModel.limParams(k));%, 'Sizes', sz);
                % Add a listener for Value change event
                addlistener(this.limComponents(k), 'ValueChange', @this.paramValueChange);
                hbox.set('Sizes', [25 -1]);
            end
            bathyPosSeuilBox.set('Sizes', [25 25]);
            
            % Optional
            bathyPosSeuilIHOBox = uiextras.VBox('Parent', this.bathyPosBox, 'Spacing', 0);
            
            this.bathyAccuIHOText = uicontrol('Parent', bathyPosSeuilIHOBox, 'Style', 'edit', 'String',   this.swathModel.bathyAccuIHOValue, 'Enable', 'off');
            this.posAccuIHOText = uicontrol('Parent', bathyPosSeuilIHOBox, 'Style', 'edit', 'String',   this.swathModel.posAccuIHOValue, 'Enable', 'off');
            
            bathyPosSeuilIHOBox.set('Sizes', [25 25]);
            
            % Show/Hide depending IHO value
            if (this.swathModel.ihoValue == 0)
                this.limComponents(4:5).set('Sizes', [-2 -1 -1 25])
                this.bathyPosBox.set('Sizes', [-1 0]);
            else
                this.limComponents(4:5).set('Sizes', [-2 0 0 25])
                this.bathyPosBox.set('Sizes', [-1 -1]);
            end
            
            vBox3.set('Sizes', [25 25 25 50]);
            
            parametersHBox.set('Sizes', [-1 -1 -1]);
        end
        
        %%Callback
        
        function ihoRadioButtonsComponentCallback(this, src, ~)
            for ihoIndex=1:length(this.swathModel.ihoList)
                if strcmp(src.String, this.swathModel.ihoList{ihoIndex})
                    this.swathModel.ihoValue = ihoIndex;
                end
            end
            
            % Update models & components
            this.pamesDialog.update();
        end
        
        function showTypeCallback(this, ~, ~)
            this.updateComponent();
        end
        
        function showLimCallback(this, ~, ~)
            this.updateComponent();
        end
        
        function paramValueChange(this, ~, ~)
            % Update models & components
            this.pamesDialog.update();
        end
    end
end
