classdef SwathModel < handle
    %SwathModel Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        pamesModel
        
        % params
        ihoList  = {'Special', '1', '2', '3'};
        ihoValue = 4; % typeValue 1 = Special / 2 = IHO1 / 3 = IHO2 / 4 = IHO3
        
        sBminSortie
        %         flagTypeFond
        %         swathData %??
        
        %% Swath General Params
        swathGeneralParams    = ClParametre.empty;
        swathDepthParam       = ClParametre('Name', 'Depth',                'Unit', 'm');
        swathSignalNoiseParam = ClParametre('Name', 'Signal / Noise Ratio', 'Unit', 'dB');
        swathMaxDepthParam    = ClParametre('Name', 'Maximum Depth',        'Unit', 'm');
        
        %% Swath Width Params
        swathWidthParams     = ClParametre.empty;
        swathRockWidthParam  = ClParametre('Name', 'Rock');
        swathSandWidthParam  = ClParametre('Name', 'Sand');
        swathMudWidthParam   = ClParametre('Name', 'Mud');
        swathOtherWidthParam = ClParametre('Name', 'Other');
        
        
        
        %% Swath Angle Params
        swathAngleParams     = ClParametre.empty;
        swathRockAngleParam  = ClParametre('Name', 'Rock');
        swathSandAngleParam  = ClParametre('Name', 'Sand');
        swathMudAngleParam   = ClParametre('Name', 'Mud');
        swathOtherAngleParam = ClParametre('Name', 'Other');
        
        %% Lim Params
        limParams = ClParametre.empty;
        limResoAlongSeuilParam      = ClParametre('Name', 'Along Track Resolution',   'Value', 0, 'Unit', 'm');
        limResoTransBathySeuilParam = ClParametre('Name', 'Across Track Res. Bathy.', 'Value', 0, 'Unit', 'm');
        limResoTransImageSeuilParam = ClParametre('Name', 'Across Track Res. Image',  'Value', 0, 'Unit', 'm');
        limAccuBathySeuilParam      = ClParametre('Name', 'Bathymetry Accuracy',      'Value', 0);
        limAccuPositionSeuilParam   = ClParametre('Name', 'Position Accuracy',        'Value', 0);
        
        % Used to compute patch
        limDepth
        limResoAlong
        limResoTransBathy
        limResoTransImage
        limAccuBathy
        limAccuPosition
        
        bathyAccuIHOValue
        posAccuIHOValue
        
        depthSample         = XSample('name', 'Depth', 'unit', 'm');
        ySample             = YSample.empty;
        rockSample          = YSample('name', 'Rock',           'color', 'r');
        negativeRockSample  = YSample('name', 'Negative Rock',  'color', 'r');
        sandSample          = YSample('name', 'Sand',           'color', 'g');
        negativesandSample  = YSample('name', 'Negative Sand',  'color', 'g');
        mudSample           = YSample('name', 'Mud',            'color', 'b');
        negativemudSample   = YSample('name', 'Nagative Mud',   'color', 'b');
        otherSample         = YSample('name', 'Other',          'color', 'k');
        negativeotherSample = YSample('name', 'Negative Other', 'color', 'k');
        swathSignal         = ClSignal('name', 'Swath Width', 'isVertical', true);
    end
    
    properties (Access = private)
        
        %% Help Html
        nameHtmlMaximumRange = ['file://' fullfile(getNomFicDatabase('maximum_range.html'))];
        nameHtmlDetextionIndex = ['file://' fullfile(getNomFicDatabase('detection_index.html'))];
        nameHtmlShipsAttitude = ['file://' fullfile(getNomFicDatabase('ships_attitude.html'))];
        nameHtmlBathyResol = ['file://' fullfile(getNomFicDatabase('bathymetry_resolution.html'))];
        nameHtmlImageResol = ['file://' fullfile(getNomFicDatabase('imagery_resolution.html'))];
        
        
    end
    
    methods
        function this = SwathModel(pamesModel)
            this.pamesModel = pamesModel;
            
            this.sBminSortie = 10;
            %             this.flagTypeFond = [1 1 1 0];
            %             this.swathData = [];
            
            % Swath General
            this.swathGeneralParams = ClParametre.empty;
            this.swathDepthParam.Help  = this.nameHtmlMaximumRange;
            this.swathGeneralParams(end+1) = this.swathDepthParam;
            this.swathSignalNoiseParam.Help  = this.nameHtmlDetextionIndex;
            this.swathGeneralParams(end+1) = this.swathSignalNoiseParam;
            this.swathMaxDepthParam.Help  = this.nameHtmlMaximumRange;
            this.swathGeneralParams(end+1) = this.swathMaxDepthParam;
            % Swath Width Params
            this.swathWidthParams = ClParametre.empty;
            this.swathWidthParams(end+1) = this.swathRockWidthParam;
            this.swathWidthParams(end+1) = this.swathSandWidthParam;
            this.swathWidthParams(end+1) = this.swathMudWidthParam;
            this.swathWidthParams(end+1) = this.swathOtherWidthParam;
            % Swath Angle Params
            this.swathAngleParams = ClParametre.empty;
            this.swathAngleParams(end+1) = this.swathRockAngleParam;
            this.swathAngleParams(end+1) = this.swathSandAngleParam;
            this.swathAngleParams(end+1) = this.swathMudAngleParam;
            this.swathAngleParams(end+1) = this.swathOtherAngleParam;
            
            % Lim Params
            this.limParams = ClParametre.empty;
            this.limResoAlongSeuilParam.Help  = this.nameHtmlShipsAttitude;
            this.limParams(end+1) = this.limResoAlongSeuilParam;
            this.limResoTransBathySeuilParam.Help  = this.nameHtmlBathyResol;
            this.limParams(end+1) = this.limResoTransBathySeuilParam;
            this.limResoTransImageSeuilParam.Help  = this.nameHtmlImageResol;
            this.limParams(end+1) = this.limResoTransImageSeuilParam;
            this.limAccuBathySeuilParam.Help  = this.nameHtmlBathyResol;
            this.limParams(end+1) = this.limAccuBathySeuilParam;
            this.limAccuPositionSeuilParam.Help  = this.nameHtmlImageResol;
            this.limParams(end+1) = this.limAccuPositionSeuilParam;
            
            % Define Swath signal
            this.ySample(1) = this.rockSample;
            this.ySample(2) = this.negativeRockSample;
            this.ySample(3) = this.sandSample;
            this.ySample(4) = this.negativesandSample;
            this.ySample(5) = this.mudSample;
            this.ySample(6) = this.negativemudSample;
            this.ySample(7) = this.otherSample;
            this.ySample(8) = this.negativeotherSample;
            this.swathSignal.set('xSample', this.depthSample, 'ySample',this.ySample);
            this.swathSignal.setDefaultXSampleIfNeeded();
        end
        
        
        function this = initModel(this)
            
            this.sBminSortie = 10;
            %             this.flagTypeFond = [1 1 1 0];
            %             this.swathData = [];
            
            % Swath General
            for k=1:numel(this.swathGeneralParams)
                this.swathGeneralParams(k).Value = [];
            end
            % Swath Width Params
            for k=1:numel(this.swathWidthParams)
                this.swathWidthParams(k).Value = [];
            end
            % Swath Angle Params
            for k=1:numel(this.swathAngleParams)
                this.swathAngleParams(k).Value = [];
            end
            
            % Lim Params
            for k=1:numel(this.limParams)
                this.limParams(k).Value = 0;
            end
            
            % Define Swath signal
            this.depthSample.data = [];
            for k=1:numel(this.ySample)
                this.ySample(k).data = [];
            end
        end
        
        function updateModel(this)
            
            %% Paramètres de la frame Swath
            
            %             this.sBminSortie = 10;
            
            %% Lecture des structures pour echo sounder
            
            calculer_swath_param(this);
            calculer_swath_courbes(this);
            calculer_swath_limites(this);
        end
        
        % Compute min max
        function minMax = getMinMax(this)
            
            minMax(1) = Inf;
            minMax(2) = -Inf;
            
            for ySampleIndex=1:numel(this.ySample)
                minMax(1) = min( minMax(1), min(this.ySample(ySampleIndex).data(:)));
                minMax(2) = max(minMax(2), max(this.ySample(ySampleIndex).data(:)));
            end
            
            minMax(3) = min(this.depthSample.data(:));
            minMax(4) = max(this.depthSample.data(:));
        end
        
        % Fermeture des limites pour la representation en patch
        % Input Arguments
        %    this         : instance de clc_swath
        %    datax, datay : courbe de la limite
        %    minMax       : vecteur des min et max selon x et y
        %    limite       : type de la limite
        %
        % Output Arguments
        %    datax, datay : valeurs de la limite apres fermeture
        function [datax, datay] = get_patch (this, limParam)
            
            % Compute min max
            minMax = this.getMinMax();
            
            % Choix du type de fermeture
            
            switch (limParam)
                case this.limResoAlongSeuilParam
                    %%% fermeture basse droite (la profondeur etant inversee)
                    datay = [this.limResoAlong, minMax(2), minMax(2)] ;
                    datax = [this.limDepth, minMax(4), 0] ;
                    
                case this.limResoTransBathySeuilParam
                    
                    %%% fermeture haute droite (la profondeur etant inversee)
                    datay = [0, this.limResoTransBathy, minMax(2), minMax(2)] ;
                    datax = [0, this.limDepth, minMax(4), 0] ;
                    
                case this.limResoTransImageSeuilParam
                    %%% fermeture basse gauche (la profondeur etant inversee)
                    datay = [this.limResoTransImage, 0]         ;
                    datax = [this.limDepth, minMax(4)] ;
                    
                case this.limAccuBathySeuilParam
                    %%% fermeture haute droite (la profondeur etant inversee)
                    datay = [0, this.limAccuBathy, minMax(2), minMax(2)] ;
                    datax = [0, this.limDepth, minMax(4), 0] ;
                    
                case this.limAccuPositionSeuilParam
                    %%% fermeture haute (la profondeur etant inversee)
                    datay = [this.limAccuPosition, minMax(2), minMax(2)] ;
                    datax = [this.limDepth, minMax(4), 0] ;
            end
        end
    end
    
    
    methods (Access = private)
        
        % Calcul des valeurs de portees pour les 4 types de fond
        function calculer_swath_param(this)
            
            if isempty(this.swathDepthParam.Value)
                this.swathDepthParam.Value       = this.pamesModel.configurationModel.sounderMaximumDepth / 2;
            end
            if isempty(this.swathSignalNoiseParam.Value)
                this.swathSignalNoiseParam.Value =  this.sBminSortie;
                this.swathMaxDepthParam.Value    =  this.pamesModel.configurationModel.sounderMaximumDepth;
            else
                this.sBminSortie    = this.swathSignalNoiseParam.Value;
                this.pamesModel.configurationModel.sounderMaximumDepth = this.swathMaxDepthParam.Value;
            end
            
            %             a.Fauchee.SwathData = a.Swath.SwathData;
            
            X = this.pamesModel.calculFauchee(this.swathDepthParam.Value, 1:4);
            
            this.swathRockWidthParam.Value  = 2 * X (1);
            this.swathSandWidthParam.Value  = 2 * X (2);
            this.swathMudWidthParam.Value   = 2 * X (3);
            this.swathOtherWidthParam.Value = 2 * X (4);
            
            this.swathRockAngleParam.Value  = atand(X(1) / this.swathDepthParam.Value);
            this.swathSandAngleParam.Value  = atand(X(2) / this.swathDepthParam.Value);
            this.swathMudAngleParam.Value   = atand(X(3) / this.swathDepthParam.Value);
            this.swathOtherAngleParam.Value = atand(X(1) / this.swathDepthParam.Value);
            
            %             a.Fauchee.NomFondOther = a.Environment.OtherBottom; %TODO
        end
        
        % Calcul des limitations de portee
        function calculer_swath_courbes(this)
            
            this.depthSample.data = linspace (0, this.pamesModel.configurationModel.sounderMaximumDepth, 500);
            this.depthSample.data(1) = [] ;
            
            for k = 1:4
                this.ySample(2*k -1).data = this.pamesModel.calculFauchee(this.depthSample.data', k);
                this.ySample(2*k).data = -this.pamesModel.calculFauchee(this.depthSample.data', k);
            end
        end
        
        % Calcul des limites de portee pour respecter des criteres de qualite en resolution et en precision.
        function calculer_swath_limites(this)
            
            this.limDepth           = linspace (0, this.pamesModel.configurationModel.sounderMaximumDepth, 500) ;
            this.limResoAlong      = this.calculLimSwathResolAlong(this.limDepth, this.limResoAlongSeuilParam.Value);
            this.limResoTransBathy = this.calculLimSwathResolTransBathy(this.limDepth, this.limResoTransBathySeuilParam.Value);
            this.limResoTransImage = this.calculLimSwathResolTransImage(this.limDepth, this.limResoTransImageSeuilParam.Value);
            
            switch this.ihoValue
                case 0 % Valeurs fournies par l'utilisateur
                    A1 = 0;
                    B1 = this.limAccuBathySeuilParam.Value;
                    A2 = this.limAccuPositionSeuilParam.Value;
                    B2 = 0;
                    this.bathyAccuIHOValue = '';
                    this.posAccuIHOValue = '';
                case 1 % IHO "Special"
                    A1 = 0.25;
                    B1 = 0.75;
                    A2 = 2;
                    B2 = 0;
                    this.bathyAccuIHOValue = '0.25 + 0.75% * Depth';
                    this.posAccuIHOValue = '2 + 0.0% * Depth';
                case 2 % IHO "1"
                    A1 = 0.5;
                    B1 = 1.3;
                    A2 = 5;
                    B2 = 5;
                    this.bathyAccuIHOValue = '0.5 + 1.3% * Depth';
                    this.posAccuIHOValue = '5 + 5% * Depth';
                case 3 % IHO "2"
                    A1 = 1;
                    B1 = 2.3;
                    A2 = 20;
                    B2 = 5;
                    this.bathyAccuIHOValue = '1 + 2.3% * Depth';
                    this.posAccuIHOValue = '20 + 5% * Depth';
                case 4 % IHO "3"
                    A1 = 1;
                    B1 = 2.3;
                    A2 = 150;
                    B2 = 5;
                    this.bathyAccuIHOValue = '1 + 2.3% * Depth';
                    this.posAccuIHOValue = '150 + 5% * Depth';
            end
            
            this.limAccuBathy    = this.calculLimSwathPreciBathy(this.limDepth, A1, B1);
            this.limAccuPosition = this.calculLimSwathPreciPosition(this.limDepth, A2, B2);
        end
        
        % Calcul de la limiation de portee en fonction de la resolution longitudinale
        %
        % X = CalculLimSwathResolAlong(a, Z, seuilResolLongi)
        %
        % Input Arguments
        %   a               : Instance de la classe cl_pames
        %   Z               : Profondeur (m)
        %   seuilResolLongi : Limite de resolution longitudinale (m)
        %
        % Output Arguments
        %   X : Limitation de portee (m)
        function X = calculLimSwathResolAlong(this, Z, seuilResolLongi)
            
            Z = Z(:);
            
            v = seuilResolLongi / (this.pamesModel.signalArraysModel.txBeamWidthAlongTrackParam.Value * (pi / 180));
            diff = (v .^ 2) - (Z .^ 2);
            
            X = zeros(size(Z));
            sub = find(diff > 0);
            X(sub) = sqrt(diff(sub));
            X = X';
        end
        
        % Calcul de la limiation de portee en fonction de la resolution transversale de la bathymetrie.
        %
        % X = CalculLimSwathResolTransBathy(a, Z, LimResolTransBathySeuil)
        %
        % Input Arguments
        %   a                       : Instance de la classe cl_pames
        %   Z                       : Profondeur (m)
        %   LimResolTransBathySeuil : Limite de resolution transversale de la bathymetrie (m)
        %
        % Output Arguments
        %   X : Limitation de portee (m)
        function X = calculLimSwathResolTransBathy(this, Z, limResolTransBathySeuil)
            Z = abs(Z(:));
            diff = (limResolTransBathySeuil * Z / (this.pamesModel.signalArraysModel.rxBeamWidthAcrossTrackParam.Value *( pi / 180))) ...
                - (Z .* Z);
            
            X = zeros(size(Z));
            
            sub = find(diff > 0);
            X(sub) = sqrt(diff(sub));
            X = X';
        end
        
        % Calcul de la limiation de portee en fonction de la resolution transversale de l'imagerie.
        %
        % X = CalculLimSwathResolTransImage(a, Z, LimResolTransImageSeuil)
        %
        % Input Arguments
        %   a                       : Instance de la classe cl_pames
        %   Z                       : Profondeur (m)
        %   LimResolTransImageSeuil : Limite de resolution transversale de l'imagerie (m)
        %
        % Output Arguments
        %   X : Limitation de portee (m)
        function X = calculLimSwathResolTransImage(this, Z, LimResolTransImageSeuil)
            Z = abs(Z(:));
            if LimResolTransImageSeuil == 0
                X = zeros(size(Z));
            else
                C = 1500;
                CTOs2 = C * (this.pamesModel.signalArraysModel.signalDurationParam.Value * 1.e-3) / 2;
                arcSin = CTOs2 / LimResolTransImageSeuil;
                
                if arcSin <= 1
                    X = Z * tan(asin(arcSin));
                else
                    X = zeros(size(Z));
                end
            end
            X = X';
        end
        
        % Calcul de la limiation de portee en fonction de la precision de bathymetrie.
        %
        % X = CalculLimSwathPreciBathy(a, Z, A, B100)
        %
        % Input Arguments
        %   a       : Instance de la classe cl_pames
        %   Z       : Profondeur (m)
        %   A, B100 : Equation de la droite de limitation Lim = A + B100/100 * Z
        %
        % Output Arguments
        %   X : Limitation de portee (m)
        function X = calculLimSwathPreciBathy(this, Z, A, B100)
            Z = abs(Z(:));
            B = B100 / 100;
            
            % tetaIn      = linspace(0, a.Arrays.ReceptionMaximumAngle, 100);
            tetaIn      = linspace(0, 90, 100);
            
            angLim = calcul(this, Z, B, tetaIn);
            % X = Z .* tand(angLim);
            % X = X';
            tetaIn = zeros(length(Z), 100);
            for i=1:length(Z)
                tetaIn(i, :) = linspace(angLim(i)-1, angLim(i)+1, 100);
            end
            angLim = calcul(this, Z, B, tetaIn);
            X = Z .* tand(angLim);
            X = X';
            
            function angLim = calcul(this, Z, B, tetaIn)
                
                totalPreci  = this.pamesModel.accuracyModel.calculDz(Z, tetaIn);
                LimZ        = B * Z ;
                totalPreci  = totalPreci > repmat(LimZ, 1, size(tetaIn, 2));
                [I, J]      = find(diff(totalPreci') == 1);
                angLim      = zeros(size(Z));
                if size(tetaIn, 1) == 1
                    angLim(J)   = tetaIn(I);
                else
                    angLim(J)   = tetaIn(J+(I-1)*size(Z,1));
                end
            end
        end
        
        % Calcul de la limiation de portee en fonction de la precision de positionnement.
        %
        % X = CalculLimSwathPreciPosition(a, Z, A, B100)
        %
        % Input Arguments
        %   a       : Instance de la classe cl_pames
        %   Z       : Profondeur (m)
        %   A, B100 : Equation de la droite de limitation Lim = A + B100/100 * Z
        %
        % Output Arguments
        %   X : Limitation de portee (m)
        function X = calculLimSwathPreciPosition(this, Z, A, B100)
            Z = abs(Z(:));
            B = B100 / 100;
            
            % tetaIn      = linspace(0, a.Arrays.ReceptionMaximumAngle, 100);
            tetaIn      = linspace(0, 90, 100);
            
            angLim = calcul(this, Z, A, B, tetaIn);
            % X = Z .* tand(angLim);
            % X = X';
            
            tetaIn = zeros(length(Z), 100);
            for i=1:length(Z)
                tetaIn(i, :) = linspace(angLim(i)-1, angLim(i)+1, 100);
            end
            angLim = calcul(this, Z, A, B, tetaIn);
            
            X = Z .* tand(angLim);
            X = X';
            
            function angLim = calcul(this, Z, A, B, tetaIn)
                TotalPreci  = this.pamesModel.accuracyModel.calculDx(Z, tetaIn);
                % TotalPreci  = TotalPreci > A;
                Lim = A + B * Z;
                TotalPreci  = TotalPreci > repmat(Lim, 1, size(tetaIn, 2));
                [I, J]      = find(diff(TotalPreci') == 1);
                angLim      = zeros(size(Z));
                if(size(tetaIn, 1) == 1)
                    angLim(J)   = tetaIn(I);
                else
                    angLim(J)   = tetaIn(J+(I-1)*size(Z,1));
                end
            end
        end
    end
end
