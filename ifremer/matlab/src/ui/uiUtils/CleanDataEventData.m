classdef CleanDataEventData < event.EventData
    %CLEANDATAEVENTDATA Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        line % line to modify
        selectedIndex % selected index
        modifiedIndex % index to clean
        reset % boolean reset value or not
        
    end
    
    methods
        function this = CleanDataEventData(line, selectedIndex, modifiedIndex, reset)
            this.line = line;
            this.selectedIndex = selectedIndex;
            this.modifiedIndex = modifiedIndex;
            this.reset = reset;
        end
    end
    
end

