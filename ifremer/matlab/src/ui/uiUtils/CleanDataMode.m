classdef CleanDataMode < TmpLineUtils & matlab.uitools.internal.uimode
    % classdef CleanDataMode
    %CLEANMODE Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        % ui
        parentFigure
        cleanerTool
        jcleanerToolManualItemList
        jcleanerToolMenuSaveItem
        despikeFigure
        
        % clean too list
        manualToolNameList = {'Rubber', 'Current zoom on selected axes', 'Rectangle', 'Polygon', 'FreeHand', 'Assisted (only with background image)'};
        
        % data
        cleanInterpolation = false;
        dirty = false;
        isStillInCleanMode      = false;
        isManualCleanMode       = true; % true : manual, false : algo
        selectedCleanerToolName = 'Rubber'; % rubber by default
        mouseButtondown         = false;
        controlKeyPressed       = false;
    end
    properties (Access = protected)
        %% Saved parameters for Rubber Undo/Redo perf
        rubberAxes = [];
        rubberMousePositionCell = {};
        rubberResetClean = [];
    end
    
    events
        CleanEvent
        PerformCleanEvent % send event when clean finish
        CleanTmpEvent % send event after delete tmp line
    end
    
    methods
        function this = CleanDataMode(fig, isManualCleanMode, cleanInterpolation)
            % Superclass Constructor
            this = this@matlab.uitools.internal.uimode();
            createuimode(this,fig,'Exploration.Clean');
            
            this.parentFigure = fig;
            this.cleanInterpolation = cleanInterpolation;
            this.isManualCleanMode = isManualCleanMode;
            if ~this.isManualCleanMode % remplace rubber by axes if algo
                this.manualToolNameList{1} = 'Whole selected axes';
                this.selectedCleanerToolName = 'Whole selected axes';
            end
            
            % create clean tool in figure tool bar
            this.createCleanTool();
            
            this.WindowButtonDownFcn   = @this.windowButtonDownFcnCallback;
            this.WindowButtonUpFcn     = @this.windowButtonUpFcnCallback;
            this.WindowButtonMotionFcn = @this.windowButtonMotionFcnCallback;
            this.WindowKeyPressFcn     = @this.windowKeyPressFcnCallback;
            this.WindowKeyReleaseFcn   = @this.windowKeyReleaseFcnCallback;
            this.WindowScrollWheelFcn  = @this.windowScrollWheelFcnCallback;
            % Start Mode Callback
            this.ModeStartFcn = @this.modeStartFcnCallback;
            % End Mode Callback
            this.ModeStopFcn  = @this.modeStopFcnCallback;
        end
        
        function createCleanTool(this)
            %% Cleaner Tool
            % find / toolbar
            hToolbar = findobj(allchild(this.parentFigure), 'Type', 'uitoolbar');
            if isempty(hToolbar)
                hToolbar = uitoolbar(fig);
            end
            
            % Load the clean icon
            if this.isManualCleanMode
                iconName = 'broom.png';
            else
                iconName = 'Magic-wand-icon.png';
            end
            icon = iconRead(getNomFicDatabase(iconName));
            
            % create the cleaner tool
            this.cleanerTool = uitogglesplittool('parent', hToolbar, 'cdata', icon, ...
                'tooltip', 'Clean data', ...
                'ClickedCallback', @this.cleanerToolToggleActionCallback);
            if this.isManualCleanMode
                this.cleanerTool.set('tooltip', 'Manual Clean Data', 'Separator', 'on');
            else
                this.cleanerTool.set('tooltip', 'Algorithmic Clean Data');
            end
        end
        
        function createCleanToolMenu(this)
            % Add JCheckBoxMenuItems to cleanerTool
            jcleanerTool = get(this.cleanerTool, 'JavaContainer');
            % Try catch beacause  get(jcleanerTool,     'MenuComponent')
            % return null sometimes if no pause...
            try
                jcleanerToolMenu = get(jcleanerTool, 'MenuComponent');
            catch
                pause(0.5)
                jcleanerToolMenu = get(jcleanerTool, 'MenuComponent');
            end
            
            jradioGroup = javax.swing.ButtonGroup();
            
            %% Manual Cleaning
            this.jcleanerToolManualItemList = javaArray('javax.swing.JRadioButtonMenuItem',numel(this.manualToolNameList));
            for k=1:numel(this.manualToolNameList)
                this.jcleanerToolManualItemList(k) = javax.swing.JRadioButtonMenuItem(this.manualToolNameList(k));
                this.jcleanerToolManualItemList(k).set('ActionPerformedCallback', @this.cleanerToolMenuItemCallback);
                
                jradioGroup.add(this.jcleanerToolManualItemList(k));
                jcleanerToolMenu.add(this.jcleanerToolManualItemList(k));
            end
            
            %% Save button
            jcleanerToolMenuSeparatorItem = javax.swing.JSeparator();
            jcleanerToolMenu.add(jcleanerToolMenuSeparatorItem);
            this.jcleanerToolMenuSaveItem = javax.swing.JMenuItem('Save (Shift + S)');
            this.jcleanerToolMenuSaveItem.setEnabled(false);
            this.jcleanerToolMenuSaveItem.set('ActionPerformedCallback', @this.cleanerToolMenuItemCallback);
            jcleanerToolMenu.add(this.jcleanerToolMenuSaveItem);
            
            % select the selected tool
            this.jcleanerToolManualItemList(1).setSelected(true);
        end
        
        
        %% Clean Data Actions
        function selectCleanData(this)
            
            currentAxes = this.parentFigure.get('CurrentAxes');
            
            % Check if clic on CurrentAxes
            if TmpLineUtils.checkIfMousePositionInAxes(currentAxes)
                
                resetClean = this.controlKeyPressed;
                
                %create tmpLineList if needed
                this.createTmpLineList(currentAxes);
                
                if strcmp(this.selectedCleanerToolName, this.manualToolNameList{1})
                    if this.isManualCleanMode
                        % Rubber
                        this.rubberCleanData(currentAxes, resetClean);
                        
                        % save params for rubber undo redo
                        this.rubberAxes = currentAxes;
                        this.rubberResetClean = resetClean;
                    else
                        % Axes
                        this.axesCleanData(currentAxes, resetClean);
                    end
                else
                    this.isStillInCleanMode = true;
                    % Remove dragndrop from normal mode because
                    % imrect/impoly/imfreehand set back to normal mode
                    % callbacks
                    modemanager= uigetmodemanager(this.parentFigure);
                    modemanager.set('CurrentMode', []);
                    FigUtils.removeDragDrop(this.parentFigure);
                    
                    % init selection variable
                    selection = [];
                    
                    switch this.selectedCleanerToolName
                        case this.manualToolNameList{2} % 'Current zoom on axes'
                            
                            % Check is numeric x axis or datetime
                            if isa(currentAxes.XLim(1), 'datetime') % convert in numeric
                                x1 = ruler2num( currentAxes.XLim(1), currentAxes.XAxis);
                                x2 = ruler2num( currentAxes.XLim(2), currentAxes.XAxis);
                            else
                                x1 = currentAxes.XLim(1);
                                x2 = currentAxes.XLim(2);
                            end
                            selectionPositionList(1,:) = [x1 currentAxes.YLim(1)];
                            selectionPositionList(2,:) = [x1 currentAxes.YLim(2)];
                            selectionPositionList(3,:) = [x2 currentAxes.YLim(2)];
                            selectionPositionList(4,:) = [x2 currentAxes.YLim(1)];
                        case this.manualToolNameList{3} % 'Rectangle'
                            %                             selection = drawrectangle(currentAxes, 'InteractionsAllowed', 'none');
                            %                             position = selection.Position;
                            selection = imrect(currentAxes);
                            position = selection.getPosition();
                            % build the list of selected postions position = [posX, posY, widthX,widthY]
                            selectionPositionList(1,:) = [position(1) position(2)];
                            selectionPositionList(2,:) = [position(1) position(2)+position(4)];
                            selectionPositionList(3,:) = [position(1)+position(3) position(2)+position(4)];
                            selectionPositionList(4,:) = [position(1)+position(3) position(2)];
                        case this.manualToolNameList{4} % 'Polygon'
                            %                             selection = drawpolygon(currentAxes, 'InteractionsAllowed', 'none');
                            %                             selectionPositionList = selection.Position;
                            selection = impoly(currentAxes);
                            selectionPositionList = selection.getPosition();
                        case this.manualToolNameList{5} % 'FreeHand'
                            %                             selection =  drawfreehand(currentAxes, 'InteractionsAllowed', 'none');
                            %                             selectionPositionList = selection.Position;
                            selection = imfreehand(currentAxes);
                            selectionPositionList = selection.getPosition();
                            %                         case this.manualToolNameList{6} % 'Assisted'
                            %                             imageObject =  findobj(currentAxes, 'Type', 'Image');
                            %                             selection =  drawassisted(imageObject);
                            %                             selectionPositionList = selection.Position;
                    end
                    % re-set the mode because imrect/impoly/imfreehand set an other mode
                    modemanager.set('CurrentMode', this);
                    this.parentFigure.set('Pointer', 'arrow');
                    
                    %                 pause(0.1); % let time to close the selection before delete % Comment� par JMA le 07/12/2018 pour �viter bug lors d'un double-clik en mode rectangle
                    % delete polygon
                    if ~isempty(selection)
                        selection.delete();
                    end
                    % Clean Data
                    this.manuallyCleanDataAction(currentAxes, selectionPositionList, resetClean);
                    
                    %% End of mode
                    this.mouseButtondown    = false;
                    this.isStillInCleanMode = false;
                    this.controlKeyPressed  = false; % disable alternative mode
                end
            end
        end
        
        
        
        function rubberCleanData(this, axes, resetClean)
            % Rubber Clean Data
            
            % Mouse Position (cell array to allow multiple mousePoistion
            % when undo redo
            mousePositionCell{1} = get(axes, 'CurrentPoint');
            
            hasFoundDataToClean = this.rubberCleanDataAction(axes, mousePositionCell,  resetClean);
            
            if hasFoundDataToClean
                % save params for next rubber undo redo registering
                this.rubberMousePositionCell{end+1} = mousePositionCell{1};
            end
        end
        
        
        function hasFoundDataToClean = rubberCleanDataAction(this, axes, mousePositionCell, resetClean)
            hasFoundDataToClean = false;
            
            %init elements to clean
            lineListToClean = gobjects(0);
            tmpLineListToClean= gobjects(0);
            selectedIndexListToClean = {0};
            
            % Find line objects
            [originalLineList, tmpOriginalLineList] = TmpLineUtils.getLines(axes);
            
            if isvalid(tmpOriginalLineList)
                % Find line to clean
                cleanableLineList = this.getLinesToClean(axes);
                [~, cleanableLineIndex] = ismember(cleanableLineList, originalLineList);
                
                lineList = originalLineList(cleanableLineIndex);
                tmpLineList = tmpOriginalLineList(cleanableLineIndex);
                if isvalid(tmpLineList)
                    
                    if ~resetClean
                        lineListToIterate = lineList;
                    else
                        lineListToIterate = tmpLineList;
                    end
                    
                    % Compute transformation factor axeunit ==> pixel unit
                    XLim = get(axes, 'XLim');
                    YLim = get(axes, 'YLim');
                    axes.set('Units', 'pixels');
                    rangeX = (XLim(2)-XLim(1));
                    if isa(rangeX, 'duration') % Ajout JMA le 26/09/2016 pour R2016b
                        rangeX = seconds(rangeX) / (24*3600);
                    end
                    factorX = axes.Position(3) / rangeX;
                    factorY = axes.Position(4) / (YLim(2)-YLim(1));
                    
                    % Clean Action For each mouse position (numel = 1 when manually
                    % cleaning, multiple mouse position when undo redo
                    for mouseIndex=1:numel(mousePositionCell)
                        
                        % Mouse Position
                        mouseAxesPosition = mousePositionCell{mouseIndex};
                        mouseAxesPositionX = mouseAxesPosition(1,1);
                        mouseAxesPositionY = mouseAxesPosition(1,2);
                        
                        % init index for list of lineListToClean
                        listIndex = 0;
                        
                        for k=1:numel(lineListToIterate)
                            xData = lineListToIterate(k).XData;
                            yData = lineListToIterate(k).YData;
                            
                            % Compute distance in pixels
                            if isa(xData, 'datetime') % compute dX for dateTime dataX
                                % mouse poistion X in datetime
                                mouseAxesPositionX_DT = num2ruler(mouseAxesPositionX,axes.XAxis); % convert to datetime
                                dX = xData - mouseAxesPositionX_DT; % convert to duration
                                dX = days(dX);  % convert to double
                            else % compute dX for numeric dataX
                                dX = (xData - mouseAxesPositionX);
                            end
                            
                            deltaX = dX * factorX;
                            deltaY = (yData - mouseAxesPositionY) * factorY;
                            distanceArray = deltaX.^2 + deltaY.^2;
                            
                            selectedIndex = (distanceArray < 50); % 50 pixels
                            
                            if ~isempty(selectedIndex) && ismember(true, selectedIndex)
                                listIndex = listIndex + 1 ;
                                % add elements to clean in arrays
                                lineListToClean(listIndex) = lineList(k);
                                tmpLineListToClean(listIndex) =  tmpLineList(k);
                                selectedIndexListToClean{listIndex} = selectedIndex;
                                
                                hasFoundDataToClean = true;
                            end
                        end
                    end
                end
            end
            
            % Call clean data action
            this.cleanDataAction(lineListToClean, tmpLineListToClean, selectedIndexListToClean, resetClean);
        end
        
        function axesCleanData(this, axes, resetClean)
            % Clean Data Action on the whole axes
            
            %init elements to clean
            lineListToClean = gobjects(0);
            tmpLineListToClean = gobjects(0);
            selectedIndexListToClean = {0};
            
            % Find line objects
            [originalLineList, tmpOriginalLineList] = TmpLineUtils.getLines(axes);
            
            if isvalid(tmpOriginalLineList)
                % Find line to clean
                cleanableLineList = this.getLinesToClean(axes);
                [~,cleanableLineIndex] = ismember(cleanableLineList, originalLineList);
                
                lineList = originalLineList(cleanableLineIndex);
                tmpLineList = tmpOriginalLineList(cleanableLineIndex);
                if ~resetClean
                    lineListToIterate = lineList;
                else
                    lineListToIterate = tmpLineList;
                end
                
                % init index for list of lineListToClean
                listIndex = 0;
                
                for k=1:numel(lineListToIterate)
                    yData = lineListToIterate(k).YData;
                    
                    % select all index of line
                    selectedIndex = true(1,numel(yData));
                    
                    if ~isempty(selectedIndex) && ismember(true, selectedIndex)
                        listIndex = listIndex + 1;
                        % add elements to clean in arrays
                        lineListToClean(listIndex) = lineList(k);
                        tmpLineListToClean(listIndex) =  tmpLineList(k);
                        selectedIndexListToClean{listIndex} = selectedIndex;
                    end
                end
            end
            
            % Call clean data action
            this.cleanDataAction(lineListToClean, tmpLineListToClean, selectedIndexListToClean, resetClean);
        end
        
        function manuallyCleanDataAction(this, axes, positionList, resetClean)
            % Clean Data Action
            
            %init elements to clean
            lineListToClean = gobjects(0);
            tmpLineListToClean = gobjects(0);
            selectedIndexListToClean = {0};
            
            % Find line objects
            [originalLineList, tmpOriginalLineList] = TmpLineUtils.getLines(axes);
            
            if isvalid(tmpOriginalLineList)
                % Find line to clean
                cleanableLineList = this.getLinesToClean(axes);
                [~,cleanedLineIndex] = ismember(cleanableLineList, originalLineList);
                
                lineList = originalLineList(cleanedLineIndex);
                tmpLineList = tmpOriginalLineList(cleanedLineIndex);
                if  (~resetClean)
                    lineListToIterate = lineList;
                else
                    lineListToIterate = tmpLineList;
                end
                
                % init index for list of lineListToClean
                listIndex = 0;
                
                for k=1:numel(lineListToIterate)
                    xData = lineListToIterate(k).XData;
                    yData = lineListToIterate(k).YData;
                    
                    % compute the point in polygon
                    if isa(xData, 'datetime') % compute dX for dateTime dataX
                        x_limits = get(axes, 'XLim');
                        x0 = x_limits(1); %find the x0 of axes
                        
                        dX = xData - x0; % convert to duration
                        xDataNew = days(dX); % convert to double
                        
                        positionListX_DT = num2ruler(positionList(:,1),axes.XAxis); % convert to datetime
                        positionListX_Duration = positionListX_DT - x0; % convert to duration
                        positionListX_Double =  days(positionListX_Duration);  % convert to double
                        
                        [selectedIndex, ~] = inpolygon(xDataNew, yData, positionListX_Double, positionList(:,2));
                    else % compute dX for numeric dataX
                        [selectedIndex, ~] = inpolygon(xData, yData, positionList(:,1), positionList(:,2));
                    end
                    
                    if ~isempty(selectedIndex) && ismember(true, selectedIndex)
                        listIndex = listIndex + 1;
                        % add elements to clean in arrays
                        lineListToClean(listIndex) = lineList(k);
                        tmpLineListToClean(listIndex) =  tmpLineList(k);
                        selectedIndexListToClean{listIndex} = selectedIndex;
                    end
                end
            end
            
            % Call clean data action
            this.cleanDataAction(lineListToClean, tmpLineListToClean, selectedIndexListToClean, resetClean);
        end
        
        
        function cleanDataAction(this, lineListToClean, tmpLineListToClean, selectedIndexListToClean, resetClean)
            
            %init elements to clean
            cleanedIndexListToClean = {0};
            listIndex = 0;
            
            for lineIndex = 1:numel(lineListToClean)
                line = lineListToClean(lineIndex);
                selectedIndex = selectedIndexListToClean{lineIndex};
                %check if line is Ok for compute cleanedIndex
                if (this.checkIfLineOkForCleaningUnitaryAction(line))
                    if ~resetClean &&  ~this.isManualCleanMode % non reset mode && algorithmic mode
                        % compute index to clean givin selectedIndex
                        cleanedIndex = this.computeCleanedIndexFromSelectedIndex(line, selectedIndex);
                    else
                        cleanedIndex = selectedIndex;
                    end
                else
                    % no clean action
                    cleanedIndex = false(1, numel(selectedIndex));
                end
                listIndex = listIndex +1;
                cleanedIndexListToClean{listIndex} = cleanedIndex;
            end
            
            % Clean unitary action with event
            this.cleanDataActionWithEvent(lineListToClean, tmpLineListToClean, selectedIndexListToClean, cleanedIndexListToClean, resetClean);
            
            % Check if not rubber ==> register undo redo
            if  ~this.isManualCleanMode || ~strcmp(this.selectedCleanerToolName, this.manualToolNameList{1}) % Not Rubber
                %% Prepare an undo/redo action
                cmd.Name = sprintf('cleanDataAction');
                cmd.Function        = @this.cleanDataActionWithEvent; % Redo action
                cmd.Varargin        = {lineListToClean, tmpLineListToClean, selectedIndexListToClean, cleanedIndexListToClean, resetClean};
                cmd.InverseFunction = @this.cleanDataActionWithEvent; % Undo action
                cmd.InverseVarargin = {lineListToClean, tmpLineListToClean, selectedIndexListToClean, cleanedIndexListToClean, ~resetClean};
                % Register the undo/redo action with the figure
                uiundo(gcbf, 'function', cmd);
            end
        end
        
        function cleanedIndex = computeCleanedIndexFromSelectedIndex(this, line, selectedIndex)
            % compute cleanedIndex with algorithm
            yData = line.YData;
            % figure of despiker
            if isempty(this.despikeFigure) || ~isvalid(this.despikeFigure)
                this.despikeFigure = figure;
            end
            % cleaning algorithm
            
            [~, ~, computedIndexToClean] = func_despike_phasespace(yData(selectedIndex), this.despikeFigure, line.DisplayName);
            % compute cleanedIndex
            cleanedIndex = false(1, numel(selectedIndex));
            selectedIndexVal = find(selectedIndex);
            cleanedIndex(selectedIndexVal(computedIndexToClean)) = true;
        end
        
        function  cleanDataActionWithEvent(this, lineListToClean, tmpLineListToClean, selectedIndexListToClean, cleanedIndexListToClean, resetClean)
            for lineIndex = 1:numel(lineListToClean)
                line = lineListToClean(lineIndex);
                tmpLine = tmpLineListToClean(lineIndex);
                selectedIndex = selectedIndexListToClean{lineIndex};
                cleanedIndex = cleanedIndexListToClean{lineIndex};
                
                if isvalid(line) && isvalid(tmpLine) % check if tmpLine (and line) exist
                    
                    %check if line is Ok for Clean Data Unitary Action
                    if (this.checkIfLineOkForCleaningUnitaryAction(line))
                        
                        % Clean Data Unitary Action
                        this.cleanDataUnitaryAction(line, tmpLine, cleanedIndex, resetClean);
                    end
                    
                    % set clean mode dirty
                    this.dirty = true;
                    this.jcleanerToolMenuSaveItem.setEnabled(true);
                    % clean event
                    cleanDataEventData = CleanDataEventData(line, selectedIndex, cleanedIndex, resetClean);
                    notify(this, 'CleanEvent', cleanDataEventData);
                end
            end
        end
        
        function  cleanDataUnitaryAction(this, line, tmpLine, cleanedIndex, resetClean)
            % Clean Data Unitary Action
            if ~resetClean % non reset  mode
                
                %% Clean
                if ~this.cleanInterpolation
                    % clean
                    line.YData(cleanedIndex) = Inf;
                else % interpolation mode
                    % save the initial data
                    xData = line.XData;
                    yData = line.YData;
                    tmpXData = tmpLine.XData;
                    tmpYData = tmpLine.YData;
                    
                    % compute difference between data and tmp data
                    diffX = ~eq(xData, tmpXData);
                    diffY = ~eq(yData, tmpYData);
                    difference = diffX | diffY;
                    if ~isempty(line.ZData)
                        zData = line.ZData;
                        tmpZData = tmpLine.ZData;
                        diffZ = ~eq(zData, tmpZData);
                        difference = difference | diffZ;
                    end
                    
                    % add the index to clean to the difference array
                    difference(cleanedIndex) = 1;
                    
                    % delete the difference data for the next interpolation
                    xData(difference) = [];
                    yData(difference) = [];
                    if ~isempty(line.ZData)
                        zData(difference) = [];
                    end
                    
                    if isempty(line.UserData) % graph f(x) = y
                        % interp Y on X (and Z on X)
                        interpoledYData = my_interp1(xData, yData, line.XData);
                        if ~isempty(line.ZData)
                            interpoledZData = my_interp1(xData, zData, line.XData);
                        end
                        
                        line.YData = interpoledYData;
                        if ~isempty(line.ZData)
                            line.ZData = interpoledZData;
                        end
                    else % graph f(x,y)
                        % if userData, interp1 X AND Y on UserData
                        userData = line.UserData;
                        userData(difference) = [];
                        
                        
                        interpoledXData = my_interp1(userData, xData, line.UserData);
                        interpoledYData = my_interp1(userData, yData, line.UserData);
                        if ~isempty(line.ZData)
                            interpoledZData = my_interp1(userData, zData, line.UserData);
                        end
                        
                        line.XData = interpoledXData;
                        line.YData = interpoledYData;
                        if ~isempty(line.ZData)
                            line.ZData = interpoledZData;
                        end
                    end
                end
            else % resetClean mode
                line.XData(cleanedIndex) =  tmpLine.XData(cleanedIndex);
                line.YData(cleanedIndex) =  tmpLine.YData(cleanedIndex);
                if ~isempty(line.ZData)
                    line.ZData(cleanedIndex) =  tmpLine.ZData(cleanedIndex);
                end
            end
        end
        
        
        %% Callback
        
        function cleanerToolMenuItemCallback(this, src, ~)
            % Cleaner Tool Menu Item Callback
            if src.Component == this.jcleanerToolMenuSaveItem  % Save button
                % perform clean if needed
                if this.dirty
                    this.performClean();
                end
            else
                % find manual tool
                for k=1:numel(this.jcleanerToolManualItemList)
                    if eq(src.Component, this.jcleanerToolManualItemList(k))
                        this.selectedCleanerToolName = this.manualToolNameList{k};
                    end
                end
            end
            
            if strcmp(this.cleanerTool.get('State'), 'off')
                % clean tool ON
                this.cleanerTool.set('State', 'on');
                this.cleanerToolToggleActionCallback(this.cleanerTool);
            else
                % change arrow depends on mode
                if this.isManualCleanMode && strcmp(this.selectedCleanerToolName, this.manualToolNameList{1}) % Rubber
                    this.parentFigure.set('Pointer', 'circle');
                else
                    this.parentFigure.set('Pointer', 'arrow');
                end
            end
        end
        
        function cleanerToolToggleActionCallback(this, src, ~)
            % Cleaner Tool Toggle Button  Action Callback
            modemanager= uigetmodemanager(this.parentFigure);
            if strcmp(src.get('State'), 'on') % clean tool ON
                modemanager.set('CurrentMode', this);
            else
                modemanager.set('CurrentMode', []);
            end
        end
        
        function windowButtonDownFcnCallback(this, fig, ~) %#ok<INUSD>
            % Detect if axes 2d or 3d, clean allowed on 2d, not 3d
            [~, el] = view;
            if el == 90 % axes 2D X-Y view
                this.mouseButtondown = true;
                this.selectCleanData();
            end
        end
        
        function windowButtonUpFcnCallback(this, ~, ~)
            this.mouseButtondown = false;
            
            if this.isManualCleanMode && strcmp(this.selectedCleanerToolName, this.manualToolNameList{1}) % Rubber
                % End of rubber clean
                
                %% Prepare an undo/redo action
                cmd.Name = sprintf('rubberCleanDataAction');
                cmd.Function        = @this.rubberCleanDataAction; % Redo action
                cmd.Varargin        = {this.rubberAxes, this.rubberMousePositionCell, this.rubberResetClean};
                cmd.InverseFunction = @this.rubberCleanDataAction; % Undo action
                cmd.InverseVarargin = {this.rubberAxes, this.rubberMousePositionCell, ~this.rubberResetClean};
                % Register the undo/redo action with the figure
                uiundo(gcbf,'function',cmd);
                
                %% Reset saved params for rubber undo redo
                this.rubberAxes = [];
                this.rubberMousePositionCell = {};
                this.rubberResetClean = []; 
            end
        end
        
        function windowButtonMotionFcnCallback(this, ~, ~)
            %  h = overobj('axes')
            %  h = overobj2('type','axes')
            
            if this.mouseButtondown == true
                if this.isManualCleanMode && strcmp(this.selectedCleanerToolName, this.manualToolNameList{1}) % Rubber
                    currentAxes = this.parentFigure.get('CurrentAxes');
                    this.rubberCleanData(currentAxes, this.controlKeyPressed);
                end
            end
        end
        
        function windowKeyPressFcnCallback(this, obj, event_obj)
            if ~isempty(event_obj.Modifier)
                if strcmp(event_obj.Key, 'control') % alternative mode
                    this.controlKeyPressed = true;
                elseif strcmp(event_obj.Modifier, 'shift') && strcmp(event_obj.Key, 's')
                    % perform clean if needed
                    if this.dirty
                        this.performClean();
                    end
                end   
            elseif strcmp(event_obj.Key, 'uparrow')... % pan mode
                    || strcmp(event_obj.Key, 'downarrow')...
                    || strcmp(event_obj.Key, 'leftarrow')...
                    || strcmp(event_obj.Key, 'rightarrow')
                panHandle = pan(this.parentFigure);
                panMode = panHandle.ModeHandle;
                panMode.KeyPressFcn(obj, event_obj);
            end
        end
        
        function windowKeyReleaseFcnCallback(this, ~, event_obj)
            if strcmp(event_obj.Key, 'control') % alternative mode
                   disp('control release'); % alternative mode)
                this.controlKeyPressed = false;
            end
        end
        
        function windowScrollWheelFcnCallback(this, obj, event_obj)
            % Zoom in / Zomm out callback
            zoomHandle = zoom(this.parentFigure);
            zoomMode = zoomHandle.ModeHandle;
            zoomMode.WindowScrollWheelFcn(obj, event_obj);
        end
        
        function modeStartFcnCallback(this,~,~)
            this.cleanerTool.set('State', 'on');
            
            if ~this.isStillInCleanMode % Check if we are still in Clean Mode
                if this.isManualCleanMode && strcmp(this.selectedCleanerToolName, this.manualToolNameList{1}) % Rubber
                    this.parentFigure.set('Pointer', 'circle');
                else % Rectangle, Polygon, FreeHand
                    
                end
            end
            
            % Ugly hack, set a WindowButtonMotionFcn to fig to avoid
            % problem with position of mouse with currentPoint. Set []
            % dont work
            this.parentFigure.set('WindowButtonMotionFcn', @emptyCallback);
            
            function emptyCallback(~,~)
            end
        end
        
        function modeStopFcnCallback(this, obj, evnt) %#ok<INUSD>
            if ~this.isStillInCleanMode % Check if we are still in Clean Mode
                this.cleanerTool.set('State', 'off');
                this.parentFigure.set('Pointer', 'arrow');
                
                % perform clean if needed
                if this.dirty
                    this.performClean();
                end
            end
        end
        
        function performClean(this)
            % send Perform clean event
            
            notify(this, 'PerformCleanEvent');
            
            % delete parentAxesList reference and delete TmpLines
            this.cleanTmp();
            
            % send Perform clean event
            notify(this, 'CleanTmpEvent');
            
            this.dirty = false;
            this.jcleanerToolMenuSaveItem.setEnabled(false);
        end
    end
end
