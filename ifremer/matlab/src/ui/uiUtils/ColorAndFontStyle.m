classdef ColorAndFontStyle
    % Description
    %   Class that deals with color and font style properties for uiControls
    %
    % Syntax
    %   s = ColorAndFontStyle(...)
    %
    % ColorAndFontStyle properties :
    %   BackgroundColor  - Background color (RGB triplet| short name | long name, default[.94 .94 .94])
    %   ForegroundColor  - Foreground color (RGB triplet| short name | long name, default [0 0 0])
    %   FontName         - Font name (default 'Helvetica')
    %   FontSize         - Font size (default 10)
    %   FontUnits        - Font unit : 'points' |'normalized' | 'inches' | 'centimeters' | 'pixels' (default 'points')
    %   FontWeight       - Font weight : 'normal' | 'bold' (default 'normal')
    %   FontAngle        - Font angle : 'normal' | 'italic' (default 'normal')
    %
    % Output Arguments
    %   s : One ColorAndFontStyle instance
    %
    % Examples
    %   p = ClParametre('Name', 'Temperature', 'Unit', 'deg', 'MinValue', -10, 'MaxValue', 50, 'Value', 20);
    %   style = ColorAndFontStyle('BackgroundColor', 'y','ForegroundColor', 'm');
    %   a = GUIParametreDialog('params', p, 'title', 'test', 'titleStyle', style);
    %   paramOut = a.openDialog
    % ----------------------------------------------------------------------------
    
    properties
        BackgroundColor = [.94 .94 .94] % Background color (RGB triplet| short name | long name, default[.94 .94 .94])
        ForegroundColor = [0 0 0]       % Foreground color (RGB triplet| short name | long name, default [0 0 0])
        FontName        = 'Helvetica'   % Font name (default 'Helvetica')
        FontSize        = 8             % Font size (default 10)
        UiFontName      = 'Helvetica'      % Font name for Ui components(default 'Aerial')
        UiFontSize      = 12            % Font size for Ui components(default 14)
        FontUnits       = 'points'      % Font unit : 'points' |'normalized' | 'inches' | 'centimeters' | 'pixels' (default 'points')
        FontWeight      = 'normal'      % Font weight : 'normal' | 'bold' (default 'normal')
        FontAngle       = 'normal'      % Font angle : 'normal' | 'italic' (default 'normal')
    end
    
    methods
        function this = ColorAndFontStyle(varargin)
            % Constructor of the class
            this = set(this, varargin{:});
        end
        
        function this = set(this, varargin)
            % Setter of the form a = set(a, ...)
            [varargin, this.BackgroundColor] = getPropertyValue(varargin, 'BackgroundColor', this.BackgroundColor);
            [varargin, this.ForegroundColor] = getPropertyValue(varargin, 'ForegroundColor', this.ForegroundColor);
            [varargin, this.FontName]        = getPropertyValue(varargin, 'FontName', this.FontName);
            [varargin, this.FontSize]        = getPropertyValue(varargin, 'FontSize', this.FontSize);
            [varargin, this.UiFontName]        = getPropertyValue(varargin, 'UiFontName', this.UiFontName);
            [varargin, this.UiFontSize]        = getPropertyValue(varargin, 'UiFontSize', this.UiFontSize);
            [varargin, this.FontUnits]       = getPropertyValue(varargin, 'FontUnits', this.FontUnits);
            [varargin, this.FontWeight]       = getPropertyValue(varargin, 'FontWeight', this.FontWeight);
            [varargin, this.FontAngle]       = getPropertyValue(varargin, 'FontAngle', this.FontAngle); %#ok<ASGLU>
        end
    end
    
    methods (Static)
        
        function normalColorAndFontStyle = getNormalColorAndFontStyle
            normalColorAndFontStyle =  ColorAndFontStyle();
        end
        
        function titleColorAndFontStyle = getTitleColorAndFontStyle
            titleColorAndFontStyle =  ColorAndFontStyle('ForegroundColor', 'k', 'BackgroundColor', 'w', 'FontWeight', 'bold', 'FontSize', 15);
        end
        
        function titleColorAndFontStyle = getUiTitleColorAndFontStyle
            titleColorAndFontStyle =  ColorAndFontStyle('ForegroundColor', 'k', 'BackgroundColor', 'w', 'FontWeight', 'bold', 'UiFontSize', 19);
        end
        
        function uicontrol= addStyle(uicontrol, colorAndFontStyle)
            uicontrol.BackgroundColor = colorAndFontStyle.BackgroundColor;
            uicontrol.ForegroundColor = colorAndFontStyle.ForegroundColor;
            uicontrol.FontName        = colorAndFontStyle.FontName;
            uicontrol.FontSize        = colorAndFontStyle.FontSize;
            uicontrol.FontUnits       = colorAndFontStyle.FontUnits;
            uicontrol.FontWeight      = colorAndFontStyle.FontWeight;
            uicontrol.FontAngle       = colorAndFontStyle.FontAngle;
        end
        
        function uicomponent = addUiStyle(uicomponent, colorAndFontStyle)
            uicomponent.BackgroundColor = colorAndFontStyle.BackgroundColor;
            uicomponent.FontColor = colorAndFontStyle.ForegroundColor;
%             uicomponent.FontName        = colorAndFontStyle.FontName;
%             uicomponent.FontSize        = colorAndFontStyle.FontSize;
            uicomponent.FontName        = colorAndFontStyle.UiFontName;
            uicomponent.FontSize        = colorAndFontStyle.UiFontSize;
            % uicomponent.FontUnits       = colorAndFontStyle.FontUnits;
            uicomponent.FontWeight      = colorAndFontStyle.FontWeight;
            uicomponent.FontAngle       = colorAndFontStyle.FontAngle;
        end
    end
end
