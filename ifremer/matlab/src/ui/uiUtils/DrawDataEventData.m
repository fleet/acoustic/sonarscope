classdef DrawDataEventData < event.EventData
    %DrawDataEventData Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        line % line to modify
        modifiedIndex % index to modifiy
        value % new value
    end
    
    methods
        function this = DrawDataEventData(line, modifiedIndex, value)
            this.line = line;
            this.modifiedIndex = modifiedIndex;
            this.value = value;
        end
    end
    
end

