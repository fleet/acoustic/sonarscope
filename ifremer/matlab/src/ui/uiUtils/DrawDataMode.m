classdef DrawDataMode < TmpLineUtils & matlab.uitools.internal.uimode
    % classdef DrawDataMode
    %CLEANMODE Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        % ui
        parentFigure
        drawTool
        jdrawToolManualItemList
        jdrawToolMenuSaveItem
        
        % draw tool list
        manualToolNameList = {'FreeHand', 'Lines'};
        
        % data
        drawState = 0; % 0 = mode ON, 1 = drawingt, 2 = end drawing
        dirty = false;
        isStillInDrawMode      = false
        selectedDrawToolName = 'FreeHand'; %FreeHand by default
        mouseButtondown         = false;
    end
    
    events
        DrawEvent
        PerformDrawEvent % send event when clean finish
        CleanTmpEvent % send event after delete tmp line
    end
    
    methods
        function this = DrawDataMode(fig)
            % Superclass Constructor
            this = this@matlab.uitools.internal.uimode();
            createuimode(this, fig, 'Exploration.Draw');
            
            this.parentFigure = fig;
            
            % create draw tool in figure tool bar
            this.createDrawTool();
            
            this.WindowButtonDownFcn  = @this.windowButtonDownFcnCallback;
            this.WindowKeyPressFcn    = @this.windowKeyPressFcnCallback;
            this.WindowScrollWheelFcn = @this.windowScrollWheelFcnCallback;
            % Start Mode Callback
            this.ModeStartFcn = @this.modeStartFcnCallback;
            % End Mode Callback
            this.ModeStopFcn  = @this.modeStopFcnCallback;
        end
        
        function createDrawTool(this)
            %% Draw Tool
            % find / toolbar
            hToolbar = findobj(allchild(this.parentFigure), 'Type', 'uitoolbar');
            if isempty(hToolbar)
                hToolbar = uitoolbar(fig);
            end
            
            % Load the draw icon
            iconName = 'paintbrush.gif';
            icon = iconizeFic(getNomFicDatabase(iconName));
            
            % create the draw tool
            this.drawTool = uitogglesplittool('parent', hToolbar, 'cdata', icon, ...
                'tooltip', 'Draw data', ...
                'ClickedCallback', @this.drawToolToggleActionCallback);
            
            this.drawTool.set('tooltip', 'Draw Data', 'Separator', 'on');
        end
        
        function createDrawToolMenu(this)
            % Add JCheckBoxMenuItems to drawTool
            jdrawTool     = get(this.drawTool, 'JavaContainer');
            % Try catch beacause  get(jcleanerTool, 'MenuComponent')
            % return null sometimes if no pause...
            try
                jdrawToolMenu = get(jdrawTool, 'MenuComponent');
            catch
                pause(0.5)
                jdrawToolMenu = get(jdrawTool, 'MenuComponent');
            end
            
            jradioGroup = javax.swing.ButtonGroup();
            
            %% Draw Tool
            this.jdrawToolManualItemList = javaArray('javax.swing.JRadioButtonMenuItem', numel(this.manualToolNameList));
            for k=1:numel(this.manualToolNameList)
                this.jdrawToolManualItemList(k) = javax.swing.JRadioButtonMenuItem(this.manualToolNameList(k));
                this.jdrawToolManualItemList(k).set('ActionPerformedCallback', @this.drawToolMenuItemCallback);
                
                jradioGroup.add(this.jdrawToolManualItemList(k));
                jdrawToolMenu.add(this.jdrawToolManualItemList(k));
            end
            
            %% Save button
            jdrawToolMenuSeparatorItem = javax.swing.JSeparator();
            jdrawToolMenu.add(jdrawToolMenuSeparatorItem);
            this.jdrawToolMenuSaveItem = javax.swing.JMenuItem('Save (Shift + S)');
            this.jdrawToolMenuSaveItem.setEnabled(false);
            this.jdrawToolMenuSaveItem.set('ActionPerformedCallback', @this.drawToolMenuItemCallback);
            jdrawToolMenu.add(this.jdrawToolMenuSaveItem);
            
            % select the selected tool
            this.jdrawToolManualItemList(1).setSelected(true);
        end
        
        
        
        
        %% Draw Data Actions
        function selectDrawData(this)
            currentAxes = this.parentFigure.get('CurrentAxes');
            
            %create tmpLineList if needed
            this.createTmpLineList(currentAxes);
            lineList= this.getLinesToClean(currentAxes);
            line = lineList(1); % TODO MHO compute the nearest line to drawedX(1)
            
            if this.checkIfLineOkForCleaningUnitaryAction(line) % avoid draw on navigation
                if ~isa(line.XData, 'datetime') %check if datatime cause getlines and comparaison disabled for datetime
                    
                    this.isStillInDrawMode = true;
                    % Remove dragndrop from normal mode because
                    % imrect/impoly/imfreehand set back to normal mode
                    % callbacks
                    modemanager= uigetmodemanager(this.parentFigure);
                    modemanager.set('CurrentMode', []);
                    FigUtils.removeDragDrop(this.parentFigure);
                    
                    if strcmp(this.selectedDrawToolName, this.manualToolNameList{1}) % FreeHand
                        %                         selection =  drawfreehand(currentAxes, 'InteractionsAllowed', 'none');
                        %                         selectionPositionList = selection.Position;
                        selection =  imfreehand(currentAxes);
                        selectionPositionList = selection.getPosition();
                        drawedX = selectionPositionList(:,1);
                        drawedY = selectionPositionList(:,2);
                        
                        % delete polygon
                        if (~isempty(selection))
                            selection.delete();
                        end
                    elseif strcmp(this.selectedDrawToolName, this.manualToolNameList{2}) % Lines
                        % draw line mode
                        [drawedX, drawedY] = getline(currentAxes); % TODO dosent work with X = datetime
                    end
                    
                    % Sort X & Y
                    [drawedX, sortedIndex] = sort(drawedX);
                    drawedY = drawedY(sortedIndex);
                    
                    % Creta X list for interp
                    indexList = find(line.XData >= min(drawedX) & line.XData <= max(drawedX)); % TODO dosent work with X = datetime
                    if isempty(indexList)
                        return;
                    end
                    
                    interpX = line.XData(indexList)';
                    
                    % Interpolation des points
                    try
                        interpY = interp1(drawedX, drawedY, interpX); % FIXME TODO random error
                    catch
                        interpY = my_interp1(drawedX, drawedY, interpX); % FIXME TODO random error
                    end
                    % Modify line
                    line.YData(indexList) = interpY;
                    
                    % Set dirty
                    this.dirty = true;
                    this.jdrawToolMenuSaveItem.setEnabled(true);
                    % Draw event
                    drawDataEventData = DrawDataEventData(line, indexList, interpY);
                    notify(this, 'DrawEvent', drawDataEventData);
                    
                    
                    % re-set the mode because imrect/impoly/imfreehand set an other mode
                    modemanager.set('CurrentMode', this);
                    this.parentFigure.set('Pointer', 'arrow');
                    
                    
                    this.mouseButtondown    = false;
                    this.isStillInDrawMode = false;
                else
                    % if data is datetime
                    my_warndlg('Draw disabled for datetime XData',1);
                end
            end
        end
        
        %         function hasFoundDataToDraw = drawDataAction(this, line, tmpLine, selectedIndex, resetDraw)
        %             this.drawDataUnitaryAction(line, tmpLine, selectedIndex, resetDraw);
        %
        %             % set draw mode dirty
        %             this.dirty = true;
        %             this.jdrawToolMenuSaveItem.setEnabled(true);
        %             % draw event
        %             drawDataEventData = DrawDataEventData(line, selectedIndex, resetDraw);
        %             notify(this, 'DrawEvent', drawDataEventData);
        %
        %             hasFoundDataToDraw = true;
        %         end
        
        
        
        
        %% Callback
        
        function drawToolMenuItemCallback(this, src, ~)
            % Draw Tool Menu Item Callback
            if src.Component == this.jdrawToolMenuSaveItem  % Save button
                % perform draw if needed
                if this.dirty
                    this.performDraw();
                end
            else
                % find manual tool
                for k=1:numel(this.jdrawToolManualItemList)
                    if eq(src.Component, this.jdrawToolManualItemList(k))
                        this.selectedDrawToolName = this.manualToolNameList{k};
                    end
                end
            end
            
            if strcmp(this.drawTool.get('State'), 'off')
                % draw tool ON
                this.drawTool.set('State', 'on');
                this.drawToolToggleActionCallback(this.drawTool);
            end
        end
        
        function drawToolToggleActionCallback(this, src, ~)
            % Draw Tool Toggle Button  Action Callback
            modemanager= uigetmodemanager(this.parentFigure);
            if strcmp(src.get('State'), 'on') % draw tool ON
                modemanager.set('CurrentMode', this);
            else
                modemanager.set('CurrentMode', []);
            end
        end
        
        function windowButtonDownFcnCallback(this, fig, ~) %#ok<INUSD>
            % Detect if axes 2d or 3d, draw allowed on 2d, not 3d
            [~, el] = view;
            if el == 90 % axes 2D X-Y view
                this.mouseButtondown = true;
                this.selectDrawData();
            end
        end
        
        function windowKeyPressFcnCallback(this, obj, event_obj)
            if ~isempty(event_obj.Modifier)
                if strcmp(event_obj.Modifier, 'shift') && strcmp(event_obj.Key, 's')
                    % perform clean if needed
                    if this.dirty
                        this.performClean();
                    end
                end
            elseif strcmp(event_obj.Key, 'uparrow')... % pan mode
                    || strcmp(event_obj.Key, 'downarrow')...
                    || strcmp(event_obj.Key, 'leftarrow')...
                    || strcmp(event_obj.Key, 'rightarrow')
                panHandle = pan(this.parentFigure);
                panMode = panHandle.ModeHandle;
                panMode.KeyPressFcn(obj, event_obj);
            end
        end
        
        function windowScrollWheelFcnCallback(this, obj, event_obj)
            % Zoom in / Zomm out callback
            zoomHandle = zoom(this.parentFigure);
            zoomMode = zoomHandle.ModeHandle;
            zoomMode.WindowScrollWheelFcn(obj, event_obj);
        end
        
        
        function modeStartFcnCallback(this,~,~)
            this.drawTool.set('State', 'on');
            
            % Ugly hack, set a WindowButtonMotionFcn to fig to avoid
            % problem with position of mouse with currentPoint. Set []
            % dont work
            this.parentFigure.set('WindowButtonMotionFcn', @emptyCallback);
            
            function emptyCallback(~,~)
            end
        end
        
        function modeStopFcnCallback(this, obj, evnt) %#ok<INUSD>
            if ~this.isStillInDrawMode % Check if we are still in Clean Mode
                this.drawTool.set('State', 'off');
                this.parentFigure.set('Pointer', 'arrow');
                
                % perform draw if needed
                if this.dirty
                    this.performDraw();
                end
            end
        end
        
        function performDraw(this)
            % send Perform draw event
            notify(this, 'PerformDrawEvent');
            
            % delete parentAxesList reference and delete TmpLines
            this.cleanTmp();
            % send Perform draw event
            notify(this, 'CleanTmpEvent');
            
            this.dirty = false;
            this.jdrawToolMenuSaveItem.setEnabled(false);
        end
        
    end
end
