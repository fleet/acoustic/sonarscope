classdef EditDataEventData < event.EventData
    %EditDataEventData Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        line % line to modify
        modifiedIndex % index to modifiy
        value % new value
    end
    
    methods
        function this = EditDataEventData(line, modifiedIndex, value)
            this.line = line;
            this.modifiedIndex = modifiedIndex;
            this.value = value;
        end
    end
    
end

