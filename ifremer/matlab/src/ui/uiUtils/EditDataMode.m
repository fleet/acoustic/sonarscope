classdef EditDataMode < TmpLineUtils & matlab.uitools.internal.uimode
    % classdef EditDataMode
    % EditDataMode Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        % ui
        parentFigure
        editTool
        editDataTableDialog % parametre dialog for Butterworth algorithm cleaning
        
        % data
        cleanInterpolation = false;
        dirty = false;
    end
    
    events
        EditEvent
        PerformEditEvent % send event when clean finish
        CleanTmpEvent     % send event after delete tmp line
    end
    
    methods
        function this = EditDataMode(fig, cleanInterpolation)
            % Superclass Constructor
            this = this@matlab.uitools.internal.uimode();
            createuimode(this,fig,'Exploration.Edit');
            
            this.parentFigure = fig;
            this.cleanInterpolation = cleanInterpolation;
            % create filter tool in figure tool bar
            this.createEditTool();
            
            this.WindowButtonDownFcn   = @this.windowButtonDownFcnCallback;
            this.WindowKeyPressFcn     = @this.windowKeyPressFcnCallback;
            this.WindowScrollWheelFcn  = @this.windowScrollWheelFcnCallback;
            % Start Mode Callback
            this.ModeStartFcn = @this.modeStartFcnCallback;
            % End Mode Callback
            this.ModeStopFcn  = @this.modeStopFcnCallback;
        end
        
        function createEditTool(this)
            %% Edit Tool
            % find / toolbar
            hToolbar = findobj(allchild(this.parentFigure), 'Type', 'uitoolbar');
            if isempty(hToolbar)
                hToolbar = uitoolbar(fig);
            end
            
            % Load  icon
            icon = iconizeFic(getNomFicDatabase('editDataTool.png'));
            
            % create the edit tool
            this.editTool = uitoggletool('parent', hToolbar, 'cdata', icon, ...
                'tooltip', 'Edit data', ...
                'ClickedCallback', @this.editToolToggleActionCallback);
            
        end
        
        %% Edit Data Actions
        function selectEditData(this)
            currentAxes = this.parentFigure.get('CurrentAxes');
            currentObject = this.parentFigure.get('currentObject');
            
            %create tmpLineList if needed
            this.createTmpLineList(currentAxes);
            
            this.axesEditData(currentAxes, currentObject);
        end
        
        function axesEditData(this, selectedAxes, selectedObject)
            % Edit Data Action on the whole axes
            
            % Find line objects
            [lineListOriginal, tmpLineListOriginal] = TmpLineUtils.getLines(selectedAxes);
            
            if isvalid(tmpLineListOriginal)
                % Find line to clean
                lineListToClean = this.getLinesToClean(selectedAxes);
                [~, indexToClean] = ismember(lineListToClean, lineListOriginal);
                
                lineList = lineListOriginal(indexToClean);
                
                [~,selectedLineIndex] = ismember(selectedObject, lineList);
                if (isempty(selectedLineIndex) || selectedLineIndex == 0)
                    selectedLineIndex = 1;
                end
                
                this.editDataTableDialog.refreshSelectedData(selectedAxes, selectedLineIndex(1), lineList);
                
            end
        end
        
        function editToolToggleActionCallback(this, src, ~)
            % Filter Tool Toggle Button  Action Callback
            modemanager= uigetmodemanager(this.parentFigure);
            if strcmp(src.get('State'), 'on') % filter tool ON
                modemanager.set('CurrentMode', this);
            else
                modemanager.set('CurrentMode', []);
            end
        end
        
        function windowButtonDownFcnCallback(this, fig, ~) %#ok<INUSD>
            % Detect if axes 2d or 3d, clean allowed on 2d, not 3d
            [~, el] = view;
            if el == 90 % axes 2D X-Y view
                this.selectEditData();
            end
        end
        
        function windowKeyPressFcnCallback(this, obj, event_obj)
            if strcmp(event_obj.Key, 'uparrow')... % pan mode
                    || strcmp(event_obj.Key, 'downarrow')...
                    || strcmp(event_obj.Key, 'leftarrow')...
                    || strcmp(event_obj.Key, 'rightarrow')
                panHandle = pan(this.parentFigure);
                panMode = panHandle.ModeHandle;
                panMode.KeyPressFcn(obj, event_obj);
            elseif strcmp(event_obj.Key, 's')
                % perform clean if needed
                if this.dirty
                    this.performClean();
                end
            end
        end
        
        function windowScrollWheelFcnCallback(this, obj, event_obj)
            % Zoom in / Zomm out callback
            zoomHandle = zoom(this.parentFigure);
            zoomMode = zoomHandle.ModeHandle;
            zoomMode.WindowScrollWheelFcn(obj, event_obj);
        end
        
        
        function modeStartFcnCallback(this,~,~)
            this.editTool.set('State', 'on');
            
            % open editParametreDialog
            if isempty(this.editDataTableDialog) || ~isempty(this.editDataTableDialog.okPressedOut)
                this.openEditDataTableDialog();
                
                %display table for gca
                this.selectEditData();
            end
        end
        
        function modeStopFcnCallback(this, obj, evnt) %#ok<INUSD>
            this.editTool.set('State', 'off');
            
            % perform clean if needed
            if this.dirty
                this.performClean();
            end
            
            % close editDataTableDialog if needed
            if ~isempty(this.editDataTableDialog) && isempty(this.editDataTableDialog.okPressedOut)
                this.editDataTableDialog.okPressed();
            end
        end
        
        function performEdit(this)
            % send Perform clean event
            notify(this, 'PerformEditEvent');
            
            % delete parentAxesList reference and delete TmpLines
            this.cleanTmp();
            
            this.dirty = false;
        end
        
        function cancelEdit(this)
            
            % Set tmpLines in Line
            for axesIndex = 1:numel(this.parentAxesList)
                [lineList, tmpLineList] = TmpLineUtils.getLines(this.parentAxesList(axesIndex));
                if (~isempty(lineList) && ~isempty(tmpLineList))
                    for lineIndex=1:numel(lineList)
                        % Find tmpLine
                        line = lineList(lineIndex);
                        tmpLine = tmpLineList(lineIndex);
                        
                        line.XData = tmpLine.XData;
                        line.YData = tmpLine.YData;
                        if (~isempty(line.ZData))
                            line.ZData = tmpLine.ZData;
                        end
                    end
                end
            end
            
            % delete parentAxesList reference and delete TmpLines
            this.cleanTmp();
            % send Perform clean event
            notify(this, 'CleanTmpEvent');
            
            this.dirty = false;
        end
        
        function openEditDataTableDialog(this)
            % open editDataTableDialog
            
            % create the dialog if needed
            if isempty(this.editDataTableDialog)
                this.editDataTableDialog = EditDataTableDialog('resizable', 'on', 'hideActionButtons', false,...
                    'windowStyle', 'normal', 'waitAnswer', false,...
                    'Title', 'Edit Data Dialog', 'cleanInterpolation', this.cleanInterpolation);
                addlistener(this.editDataTableDialog, 'DataTableValueChange', @this.handleDataTableValueChangeCallback);
                addlistener(this.editDataTableDialog, 'OkPressedEvent', @this.handleOkPressedEventCallback);
                addlistener(this.editDataTableDialog, 'CancelPressedEvent', @this.handleCancelPressedEventCallback);
                this.editDataTableDialog.openDialog;
            elseif ~isempty(this.editDataTableDialog.okPressedOut)
                this.editDataTableDialog.openDialog;
            end
        end
        
        function handleDataTableValueChangeCallback(this, ~, editDataEventData)
            this.dirty = true;
            
            editedLine = editDataEventData.line;
            editedIndex = editDataEventData.modifiedIndex;
            newValue = editDataEventData.value;
            
            if isnan(newValue)
                if this.cleanInterpolation == false
                    editedLine.YData(editedIndex) = Inf;
                else
                    editedLine.XData(editedIndex) = [];
                    editedLine.YData(editedIndex) = [];
                    editedLine.UserData(editedIndex) = [];
                end
            else
                editedLine.YData(editedIndex) = newValue;
            end
            
            notify(this, 'EditEvent', editDataEventData);
        end
        
        function handleOkPressedEventCallback(this, ~, ~)
            % event on OkPressedEvent from editDataTableDialog
            if this.dirty
                this.performEdit();
            end
            this.handleExitDialogEventCallback();
        end
        
        function handleCancelPressedEventCallback(this, ~, ~)
            % event on CancelPressedEvent from editDataTableDialog
            if isvalid(this)
                if this.dirty
                    this.cancelEdit();
                end
                this.handleExitDialogEventCallback();
            end
        end
        
        function handleExitDialogEventCallback(this, ~, ~)
            % event on ExitDialog from editDataTableDialog
            % edit tool OFF
            this.editTool.set('State', 'off');
            this.editToolToggleActionCallback(this.editTool);
        end
    end
end
