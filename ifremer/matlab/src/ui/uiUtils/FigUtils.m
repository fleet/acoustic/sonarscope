classdef FigUtils
    % Class FigUtils provides static methods creating SSc Dialog & Figure
    %
    % Examples
    %   % Creates a figure and adds the Add-On menu and additional icones
    %   fig2 = FigUtils.createSScFigure;
    %
    %   % Creates a basic figure then add drag n drop support
    %   fig3 = figure;
    %   FigUtils.addDragDrop(fig3);
    %
    %   % Creates a native Matlab figure and adds the Add-On menu and additional icones in separate instructions
    %   fig4 = figure;
    %   FigUtils.addAdOnMenu(fig4);
    %   FigUtils.addToolBarItems(fig4);
    %
    %   % Useful if we do not want the Matlab menus and the icones
    %   dialog3 = FigUtils.createSScDialog('Position', centrageFig(500, 200), 'WindowStyle', 'normal');
    %   FigUtils.addAdOnMenu(dialog3);
    %   FigUtils.addToolBarItems(dialog3);
    %
    %   % Creates a dialog window and waits for closure
    %   dialog1 = FigUtils.createSScDialog;
    %
    %   % Creates a sized dialog window but does not wait for closure
    %   dialog2 = FigUtils.createSScDialog('Position', centrageFig(500, 200), 'WindowStyle', 'normal');
    %
    % see also : PlotUtils FigUtils.createSScDialog
    % ----------------------------------------------------------------------------
    
    properties
    end
    
    methods (Static, Access = public)
        
        function fig = createSScFigure(varargin)
            %% SSc Figure
            % Creates a figure for Sonarscope with added menus & tool items
            
            [varargin, Name]     = getPropertyValue(varargin, 'Name',     'IFREMER-SonarScope');
            [varargin, Position] = getPropertyValue(varargin, 'Position', []);
            [varargin, fig]      = getPropertyValue(varargin, 'figure',   []);
            
            if isempty(Position)
                Position = centrageFig(600, 500);
            end
            
            if isempty(fig)
                fig = figure(varargin{:});
            end
            
            % Check if addOnmenu exists
            addOnMenu = findobj(allchild(fig),  'Tag', 'Add-on');
            if isempty(addOnMenu)
                set(fig, 'NumberTitle', 'on', 'Name',  [' - ' Name], 'Position', Position);
                fig.set('DockControls', 'on');
                
                % add an Add-On Menu in menubar of input figure
                FigUtils.addAdOnMenu(fig);
                
                % Add Undo Redo tool bar items
                FigUtils.addUndoRedoToolBarItems(fig)
                
                % add toolbar items in toolbar of input figure
                FigUtils.addToolBarItems(fig);
                
                % Add Drag n Drop support
                FigUtils.addDragDrop(fig);
            end
        end
        
        
        function fig = createSScUiFigure(varargin)
            %% SSc Figure
            % Creates a figure for Sonarscope with added menus & tool items
            
            [varargin, Name]     = getPropertyValue(varargin, 'Name',     'IFREMER-SonarScope');
            [varargin, Position] = getPropertyValue(varargin, 'Position', []);
            [varargin, fig]      = getPropertyValue(varargin, 'figure',   []);
            
            if isempty(Position)
                Position = centrageFig(600, 500);
            end
            
            if isempty(fig)
                fig = uifigure(varargin{:});
            end
            
            % Check if addOnmenu exists
            addOnMenu = findobj(allchild(fig),  'Tag', 'Add-on');
            if isempty(addOnMenu)
                
                set(fig, 'NumberTitle', 'on', 'Name',  [' - ' Name], 'Position', Position);
                %                 fig.set('DockControls', 'on');
                
                % add an Add-On Menu in menubar of input figure
                FigUtils.addAdOnMenu(fig);
                
                % Add Undo Redo tool bar items
                FigUtils.addUndoRedoToolBarItems(fig)
                
                % add toolbar items in toolbar of input figure
                FigUtils.addToolBarItems(fig);
                
                % Add Drag n Drop support
                FigUtils.addDragDrop(fig);
            end
        end
        
        
        function dlg = createSScDialog(varargin)
            %% SSc Dialog
            % Create a figure dialog for Sonarscope GUI
            [varargin, SuffixName]      = getPropertyValue(varargin, 'SuffixName',      []);
            [varargin, Position]        = getPropertyValue(varargin, 'Position',        []);
            [varargin, closeRequestFcn] = getPropertyValue(varargin, 'CloseRequestFcn', []);
            [varargin, visible]         = getPropertyValue(varargin, 'Visible',         'on');
            [varargin, windowStyle]     = getPropertyValue(varargin, 'WindowStyle',     'modal'); %#ok<ASGLU>
            
            name = 'IFREMER - SonarScope';
            if ~isempty(SuffixName)
                name = [name ' - ' SuffixName];
            end
            
            dlg = figure('NumberTitle', 'off', 'Name', name, 'ToolBar', 'none', 'menubar', 'none', 'WindowStyle', windowStyle);
            %              dlg = dialog('Name', 'IFREMER - SonarScope', varargin{:}); % FIXME : dialog & PlotUtils.createSScPlot incompatible
            if isempty(Position)
                dlg.set('Position', centrageFig(600, 18));
            else
                dlg.set('Position', Position);
            end
            
            if ~isempty(closeRequestFcn)
                dlg.set('CloseRequestFcn', closeRequestFcn);
            end
            
            dlg.set('Visible', visible);
            %             dlg.set('WindowStyle', windowStyle); % Dans le cas o� on supprimerait ce param�trage dans la cr�ation de la figure
        end
        
        
        function dlg = createSScUiDialog(varargin)
            %% SSc Dialog
            % Create a figure dialog for Sonarscope GUI
            [varargin, SuffixName]      = getPropertyValue(varargin, 'SuffixName',      []);
            [varargin, Position]        = getPropertyValue(varargin, 'Position',        []);
            [varargin, closeRequestFcn] = getPropertyValue(varargin, 'CloseRequestFcn', []);
            [varargin, visible]         = getPropertyValue(varargin, 'Visible',         'on'); %#ok<ASGLU>
            % WindowStyle normal ==> modal not implemented in uifigure
            %             [varargin, windowStyle]     = getPropertyValue(varargin, 'WindowStyle',     'modal');
            
            name = 'IFREMER - SonarScope';
            if ~isempty(SuffixName)
                name = [name ' - ' SuffixName];
            end
            
            %             dlg = uifigure('NumberTitle', 'off', 'Name', name, 'ToolBar', 'none', 'menubar', 'none', 'WindowStyle', windowStyle);
            dlg = uifigure('NumberTitle', 'off', 'Name', name, 'ToolBar', 'none', 'menubar', 'none');
            
            % dlg = dialog('Name', 'IFREMER - SonarScope', varargin{:}); % FIXME : dialog & PlotUtils.createSScPlot incompatible
            if isempty(Position)
                dlg.set('Position', centrageFig(600, 18));
            else
                dlg.set('Position', Position);
            end
            
            if ~isempty(closeRequestFcn)
                dlg.set('CloseRequestFcn', closeRequestFcn);
            end
            
            dlg.set('Visible', visible);
            %             dlg.set('WindowStyle', windowStyle); % Dans le cas o� on supprimerait ce param�trage dans la cr�ation de la figure
        end
        
        
        function Position = centrageUiFig(Width, Height, varargin)
            
            [varargin, Fig]       = getPropertyValue(varargin, 'Fig',       []);
            [varargin, colDepMin] = getPropertyValue(varargin, 'colDepMin', 200);
            [varargin, ligDepMin] = getPropertyValue(varargin, 'ligDepMin', 200); %#ok<ASGLU>
            
            %% R�cup�ration de la taille de l'�cran
            
            ScreenSize = get(0, 'ScreenSize');
            
            %% Controle de positionnement
            
            Position(3) = min(Width,  ScreenSize(3)-colDepMin);
            Position(4) = min(Height, ScreenSize(4)-ligDepMin);
            
            milieuEcran   = floor((ScreenSize(3)-colDepMin) / 2);
            milieuFenetre = floor(Position(3) / 2);
            Position(1)   = milieuEcran - milieuFenetre + colDepMin - 50;
            
            milieuEcran   = floor((ScreenSize(4)-ligDepMin) / 2);
            milieuFenetre = floor(Position(4) / 2);
            Position(2)   = milieuEcran - milieuFenetre + 50;
            
            %% Par ici la sortie
            
            if nargout == 0
                if isempty(Fig)
                    figure('Position', Position);
                else
                    %                     figure(Fig);
                    %                     set(Fig, 'Position', Position);
                    Fig.Position = Position;
                end
            end
        end
        
        
        function addUndoRedoToolBarItems(fig)
            % Create undo /redo uipushtool
            % see : http://undocumentedmatlab.com/blog/uisplittool-uitogglesplittool
            %    and http://undocumentedmatlab.com/blog/uisplittool-uitogglesplittool-callbacks
            % see : Undocumented Secrets of MATLAB-Java Programming p216
            
            % find / toolbar
            hToolbar = findobj(allchild(fig), 'Type', 'uitoolbar');
            if isempty(hToolbar)
                hToolbar = uitoolbar(fig);
            end
            
            % Load the Redo icon
            icon = getNomFicDatabase('greenarrowicon.gif');
            [cdata,map] = imread(icon);
            
            % Convert white pixels into a transparent background
            %           map(find(map(:,1)+map(:,2)+map(:,3) == 3)) = NaN;
            map(map(:,1)+map(:,2)+map(:,3) == 3) = NaN; % JMA le 23/03/2018
            
            % Convert into 3D RGB-space
            cdataRedo = ind2rgb(cdata,map);
            cdataUndo = cdataRedo(:, (16:-1:1), :);
            
            % Add the icon (and its mirror image = undo) to latest toolbar
            undoUiPushTool = uipushtool('parent', hToolbar, 'cdata', cdataUndo, 'tooltip','undo', 'Tag', 'undoTag', 'Separator','on');
            redoUiPushTool = uipushtool('parent', hToolbar, 'cdata', cdataRedo, 'tooltip','redo', 'Tag', 'redoTag');
            undoUiPushTool.set('Enable', 'off');
            redoUiPushTool.set('Enable', 'off');
            undoUiPushTool.set('ClickedCallback', {@undoRedoCallback, 'undo'});
            redoUiPushTool.set('ClickedCallback', {@undoRedoCallback, 'redo'});
            % Set appdata handle undo/redo to fig to improve perf
            setappdata(fig, 'undoUiPushTool', undoUiPushTool);
            setappdata(fig, 'redoUiPushTool', redoUiPushTool);
            
            function undoRedoCallback(~, ~, name)
                if strcmp(name, 'undo')
                    try
                        uiundo(gcbf,'execUndo'); % exec undo
                    catch matlabException
                        warning([matlabException.identifier, ' - ' matlabException.message]);
                    end
                else
                    try
                        uiundo(gcbf,'execRedo'); % exec redo
                    catch matlabException
                        warning([matlabException.identifier, ' - ' matlabException.message]);
                    end
                end
                
                % Update Undo Redo Buttons State
                FigUtils.updateUndoRedoButtonState(fig);
            end
            
            %% Create figtool_manager for undo redo (
            figtool_manager = localGetFigureToolManager(fig);
            %function copied from uiundo.m
            function [figtool_manager] = localGetFigureToolManager(fig)
                
                KEY = 'uitools_FigureToolManager';
                
                if isprop(fig,KEY)
                    figtool_manager = fig.(KEY);
                else
                    figtool_manager = [];
                end
                
                % Create a new figure tool manager if one does not already exist
                if isempty(figtool_manager) || ~isa(figtool_manager, 'matlab.uitools.internal.FigureToolManager') || ~isvalid(figtool_manager)
                    figtool_manager = matlab.uitools.internal.FigureToolManager(fig);
                    if ~isprop(fig,KEY)
                        p = addprop(fig,KEY);
                        p.Transient = true;
                        p.Hidden = true;
                    end
                    fig.(KEY) = figtool_manager;
                end
            end
            
            %% Add listner to cmd_manager to update UndoRedo Buttons State when UndoStack and RedoStack are modified
            cmd_manager = figtool_manager.CommandManager;
            addlistener(cmd_manager, 'UndoStack', 'PostSet', @(src,eventdata)FigUtils.updateUndoRedoButtonState(fig));
            addlistener(cmd_manager, 'RedoStack', 'PostSet', @(src,eventdata)FigUtils.updateUndoRedoButtonState(fig));
            
        end
        
        
        function updateUndoRedoButtonState(fig)
            % Retrieve figtool_manager & cmd_manager of undo/redo
            KEY = 'uitools_FigureToolManager';
            if isprop(fig,KEY)
                figtool_manager = fig.(KEY);
            else
                figtool_manager = [];
            end
            
            % Get appdata handle undo/redo from fig to improve perf
            undoUiPushTool = getappdata(fig, 'undoUiPushTool');
            redoUiPushTool = getappdata(fig, 'redoUiPushTool');
            
            if ~isempty(figtool_manager) && ~isempty(undoUiPushTool) && ~isempty(redoUiPushTool)
                cmd_manager = figtool_manager.CommandManager;
                
                % Customize the toolbar buttons
                latestUndoAction = cmd_manager.peekundo;
                if isempty(latestUndoAction)
                    undoUiPushTool.set('Tooltip','', 'Enable','off');
                else
                    tooltipStr = ['undo : ' latestUndoAction.Name];
                    undoUiPushTool.set('Tooltip', tooltipStr, 'Enable', 'on');
                end
                
                nextRedoAction = cmd_manager.peekredo;
                if isempty(nextRedoAction)
                    redoUiPushTool.set('Tooltip', '', 'Enable', 'off');
                else
                    tooltipStr = ['redo : ' nextRedoAction.Name];
                    redoUiPushTool.set('Tooltip', tooltipStr, 'Enable', 'on');
                end
            end
        end
        
        
        function addAdOnMenu(fig, varargin)
            
            [~, isLightAddOnMenu]     = getPropertyValue(varargin, 'isLightAddOnMenu',     false);
            [~, editPropertiesObject] = getPropertyValue(varargin, 'editPropertiesObject', []);
            
            %% Add Add-on Menu
            
            % Add-on Menu
            addonMenu = uimenu(fig, 'Text', 'Add-on', 'Tag', 'Add-on');
            
            % Line Menu
            lineMenu = uimenu(addonMenu, 'Text', 'Lines', 'Tag', 'Line');
            
            % Line Style Menu
            lineTypeMenu = uimenu(lineMenu, 'Text', 'Style');
            uimenu(lineTypeMenu, 'Text', '-',    'Callback', {@FigUtils.setLineStyle, fig});
            uimenu(lineTypeMenu, 'Text', '--',   'Callback', {@FigUtils.setLineStyle, fig});
            uimenu(lineTypeMenu, 'Text', '.',    'Callback', {@FigUtils.setLineStyle, fig});
            uimenu(lineTypeMenu, 'Text', '-.',   'Callback', {@FigUtils.setLineStyle, fig});
            uimenu(lineTypeMenu, 'Text', 'none', 'Callback', {@FigUtils.setLineStyle, fig});
            
            % Line Width Menu
            lineWidthMenu = uimenu(lineMenu, 'Text', 'Width');
            uimenu(lineWidthMenu, 'Text', '0.5',     'Callback', {@FigUtils.setLineWidth, fig});
            uimenu(lineWidthMenu, 'Text', '1',       'Callback', {@FigUtils.setLineWidth, fig});
            uimenu(lineWidthMenu, 'Text', '2',       'Callback', {@FigUtils.setLineWidth, fig});
            uimenu(lineWidthMenu, 'Text', '3',       'Callback', {@FigUtils.setLineWidth, fig});
            uimenu(lineWidthMenu, 'Text', '4',       'Callback', {@FigUtils.setLineWidth, fig});
            uimenu(lineWidthMenu, 'Text', '6',       'Callback', {@FigUtils.setLineWidth, fig});
            uimenu(lineWidthMenu, 'Text', '8',       'Callback', {@FigUtils.setLineWidth, fig});
            uimenu(lineWidthMenu, 'Text', '10',      'Callback', {@FigUtils.setLineWidth, fig});
            uimenu(lineWidthMenu, 'Text', '15',      'Callback', {@FigUtils.setLineWidth, fig});
            uimenu(lineWidthMenu, 'Text', '20',      'Callback', {@FigUtils.setLineWidth, fig});
            uimenu(lineWidthMenu, 'Text', '25',      'Callback', {@FigUtils.setLineWidth, fig});
            uimenu(lineWidthMenu, 'Text', '30',      'Callback', {@FigUtils.setLineWidth, fig});
            
            %Line color
            uimenu(lineMenu, 'Text', 'Color', 'Callback', {@FigUtils.setLineColor, fig});
            
            % Marker Menu
            markerMenu = uimenu(addonMenu, 'Text', 'Markers', 'Tag', 'Marker');
            
            % Marker Style Menu
            markerTypeMenu = uimenu(markerMenu, 'Text', 'Style');
            uimenu(markerTypeMenu, 'Text', '+',         'Callback', {@FigUtils.setMarkerStyle, fig});
            uimenu(markerTypeMenu, 'Text', 'o',         'Callback', {@FigUtils.setMarkerStyle, fig});
            uimenu(markerTypeMenu, 'Text', '*',         'Callback', {@FigUtils.setMarkerStyle, fig});
            uimenu(markerTypeMenu, 'Text', '.',         'Callback', {@FigUtils.setMarkerStyle, fig});
            uimenu(markerTypeMenu, 'Text', 'x',         'Callback', {@FigUtils.setMarkerStyle, fig});
            uimenu(markerTypeMenu, 'Text', 'square',    'Callback', {@FigUtils.setMarkerStyle, fig});
            uimenu(markerTypeMenu, 'Text', 'v',         'Callback', {@FigUtils.setMarkerStyle, fig});
            uimenu(markerTypeMenu, 'Text', '^',         'Callback', {@FigUtils.setMarkerStyle, fig});
            uimenu(markerTypeMenu, 'Text', 'pentagram', 'Callback', {@FigUtils.setMarkerStyle, fig});
            uimenu(markerTypeMenu, 'Text', 'hexagram',  'Callback', {@FigUtils.setMarkerStyle, fig});
            uimenu(markerTypeMenu, 'Text', 'none',      'Callback', {@FigUtils.setMarkerStyle, fig});
            
            % Marker Width Menu
            markerSizeMenu = uimenu(markerMenu, 'Text', 'Width');
            uimenu(markerSizeMenu, 'Text', '0.5', 'Callback', {@FigUtils.setMarkerWidth, fig});
            uimenu(markerSizeMenu, 'Text', '1',   'Callback', {@FigUtils.setMarkerWidth, fig});
            uimenu(markerSizeMenu, 'Text', '2',   'Callback', {@FigUtils.setMarkerWidth, fig});
            uimenu(markerSizeMenu, 'Text', '3',   'Callback', {@FigUtils.setMarkerWidth, fig});
            uimenu(markerSizeMenu, 'Text', '4',   'Callback', {@FigUtils.setMarkerWidth, fig});
            uimenu(markerSizeMenu, 'Text', '6',   'Callback', {@FigUtils.setMarkerWidth, fig});
            uimenu(markerSizeMenu, 'Text', '8',   'Callback', {@FigUtils.setMarkerWidth, fig});
            uimenu(markerSizeMenu, 'Text', '10',  'Callback', {@FigUtils.setMarkerWidth, fig});
            uimenu(markerSizeMenu, 'Text', '15',  'Callback', {@FigUtils.setMarkerWidth, fig});
            uimenu(markerSizeMenu, 'Text', '20',  'Callback', {@FigUtils.setMarkerWidth, fig});
            uimenu(markerSizeMenu, 'Text', '25',  'Callback', {@FigUtils.setMarkerWidth, fig});
            uimenu(markerSizeMenu, 'Text', '30',  'Callback', {@FigUtils.setMarkerWidth, fig});
            % Marker Color Menu
            uimenu(markerMenu, 'Text', 'Color', 'Callback', {@FigUtils.setMarkerColor, fig});
            
            % Color Menu
            uimenu(addonMenu, 'Text', 'Color', 'Callback', {@FigUtils.setLineMarkerPatchColor, fig});
            
            
            
            if ~isLightAddOnMenu
                uimenu(addonMenu, 'Text', 'Font of texts',  'Callback', {@FigUtils.changeFont, fig});
                
                % Limites des axes
                % Axis Menu
                axisMenu = uimenu(addonMenu, 'Text', 'Axis');
                uimenu(axisMenu, 'Text', 'Normal',        'Callback', 'axis normal');
                uimenu(axisMenu, 'Text', 'Equal',         'Callback', 'axis equal');
                uimenu(axisMenu, 'Text', 'Tight',         'Callback', 'axis tight');
                uimenu(axisMenu, 'Text', 'Equal & Tight', 'Callback', 'axis equal; axis tight');
                uimenu(axisMenu, 'Text', 'LatLon',        'Callback', 'axisGeo');
                % Axis X Dir Menu
                axisXDirMenu = uimenu(axisMenu, 'Text', 'XDir');
                uimenu(axisXDirMenu, 'Text', 'Normal',  'Callback', 'set(gca, ''XDir'', ''Normal'')');
                uimenu(axisXDirMenu, 'Text', 'Reverse', 'Callback', 'set(gca, ''XDir'', ''Reverse'')');
                % Axis Y Dir Menu
                axisYDirMenu = uimenu(axisMenu, 'Text', 'YDir');
                uimenu(axisYDirMenu, 'Text', 'Normal',  'Callback', 'set(gca, ''YDir'', ''Normal'')');
                uimenu(axisYDirMenu, 'Text', 'Reverse', 'Callback', 'set(gca, ''YDir'', ''Reverse'')');
                uimenu(axisMenu, 'Text', 'Limits',      'Callback', {@FigUtils.editXLimYLim, fig});
                % Axis Split Menu
                axisSplitMenu = uimenu(axisMenu, 'Text', 'Split');
                uimenu(axisSplitMenu, 'Text', 'As is',           'Callback', {@FigUtils.splitAxisSameFrame, fig});
                uimenu(axisSplitMenu, 'Text', 'YLim same scale', 'Callback', {@FigUtils.splitAxisSameFrame, fig});
                uimenu(axisSplitMenu, 'Text', 'YLim tight',      'Callback', {@FigUtils.splitAxisSameFrame, fig});
                % Axis Labels Menu
                axisLabelsMenu = uimenu(axisMenu, 'Text', 'Labels');
                uimenu(axisLabelsMenu, 'Text', 'X', 'Callback', {@FigUtils.splitAxisLabelX, fig});
                uimenu(axisLabelsMenu, 'Text', 'Y', 'Callback', {@FigUtils.splitAxisLabelY, fig});
                % Axis Tick Labels Mode
                axisLabelsMenu = uimenu(axisMenu, 'Text', 'Tick Label Mode');
                uimenu(axisLabelsMenu, 'Text', 'auto',   'Callback', {@FigUtils.ticklabelmode, fig});
                uimenu(axisLabelsMenu, 'Text', 'manual', 'Callback', {@FigUtils.ticklabelmode, fig});

                % Axis Tick Labels size
                uimenu(axisMenu, 'Text', 'Size of ticks', 'Callback', {@FigUtils.setTicksSize, fig});

                % Axis Tick Labels size
                uimenu(axisMenu, 'Text', 'Orientation of X Tics', 'Callback', {@FigUtils.setXTicksOrientation, fig});

                
                % Saisie de CLim pour un scatter
                uimenu(axisMenu, 'Text', 'CLim',  'Callback', {@FigUtils.editCLim, fig});
                
                % Grid Menu
                gridMenu = uimenu(addonMenu, 'Text', 'Grid');
                uimenu(gridMenu, 'Text', 'On',  'Callback', {@FigUtils.gridOnCallback,  fig});
                uimenu(gridMenu, 'Text', 'Off', 'Callback', {@FigUtils.gridOffCallback, fig});
                
                % Box Menu
                boxMenu = uimenu(addonMenu, 'Text', 'Box');
                uimenu(boxMenu, 'Text', 'On',  'Callback', {@FigUtils.my_boxOn,  fig});
                uimenu(boxMenu, 'Text', 'Off', 'Callback', {@FigUtils.my_boxOff, fig});

                % Background color Menu
                uimenu(addonMenu, 'Text', 'Background color', 'Callback', {@FigUtils.setbackgroundColor, fig});
                
                % Video Menu
                videoMenu = uimenu(addonMenu, 'Text', 'Inverse video', 'Callback', {@FigUtils.my_videpInverse, fig}); %#ok<NASGU>
                
                % Horizontal symetry Menu
                uimenu(addonMenu, 'Text', 'Horizontal symetry', 'Callback', {@FigUtils.plotSymetrieHorizontale, fig});
                
                % Union Menu
                unionMenu = uimenu(addonMenu, 'Text', 'Union');
                uimenu(unionMenu, 'Text', 'Axis',   'Callback', {@FigUtils.unionAxis,   fig});
                uimenu(unionMenu, 'Text', 'Curves', 'Callback', {@FigUtils.unionCurves, fig});
                
                % Derivate
                uimenu(addonMenu, 'Text', 'Derivative', 'Callback', {@FigUtils.derivative, fig});
                
                % Filter
                uimenu(addonMenu, 'Text', 'Filter', 'Callback', {@FigUtils.filter, fig});
                
                % Export Menu
                exportASCIIMenu = uimenu(addonMenu, 'Text', 'Export curves in ASCII'); % Ajout JMA le 13/04/2019
                uimenu(exportASCIIMenu, 'Text', 'Serial',   'Callback', {@FigUtils.exportCurvesSerial,   fig});
                uimenu(exportASCIIMenu, 'Text', 'Parallel', 'Callback', {@FigUtils.exportCurvesParallel, fig});
                
                %% Analyse des signaux
                % Signals Menu
                signalsMenu = uimenu(addonMenu, 'Text', 'Signals analysis');
                % Signals Measurements Menu
                signalsMeasurementsMenu = uimenu(signalsMenu, 'Text', 'Measurements');
                uimenu(signalsMeasurementsMenu, 'Text', 'Segment in X',  'Callback', {@FigUtils.mesureX,  fig});
                uimenu(signalsMeasurementsMenu, 'Text', 'Segment in Y',  'Callback', {@FigUtils.mesureY,  fig});
                uimenu(signalsMeasurementsMenu, 'Text', 'Segment in XY', 'Callback', {@FigUtils.mesureXY, fig});
                
                uimenu(signalsMenu, 'Text', 'Crossplots',        'Callback', {@FigUtils.crossPlot,fig});
                uimenu(signalsMenu, 'Text', 'Self correlation',  'Callback', {@FigUtils.selfCorrelation,fig});
                uimenu(signalsMenu, 'Text', 'Cross correlation', 'Callback', {@FigUtils.crossCorrelation,fig});
                uimenu(signalsMenu, 'Text', 'Envelope',          'Callback', {@FigUtils.envelope,fig});
                
                % Rename this window
                uimenu(addonMenu, 'Text', 'Rename this window', 'Callback', {@FigUtils.renameThisWindow, fig});
                
                % Calculator Menu
                uimenu(addonMenu, 'Text', 'Calculator', 'Callback', @FigUtils.calculette);
                
                % Point&Click Menu
                uimenu(addonMenu, 'Text', 'Point & Click', 'Callback', @FigUtils.pointAndClick);
            end
            
            if ~isempty(editPropertiesObject)
                uimenu(addonMenu, 'Text', 'Edit properties', 'Separator' ,'on', 'Callback', {@FigUtils.editproperties, editPropertiesObject});
            end
            
        end
        
        
        function addToolBarItems(fig)
            %% Cr�ation de la Toolbar parsonnalis�e
            
            hdlToolbar = findobj(allchild(fig), 'Type', 'uitoolbar');
            if isempty(hdlToolbar)
                hdlToolbar = uitoolbar(fig);
            end
            nomFicIcon = getNomFicDatabase('HDF_grid.gif');
            [cdata, map] = imread(nomFicIcon);
            % Convert white pixels into a transparent background
            map(map(:,1) + map(:,2) + map(:,3)==3) = NaN;
            % Convert into 3D RGB-space
            cdataGrid = ind2rgb(cdata, map);
            
            %             if ishandle(hdlToolbar)
            hdlGrid = uitoggletool(hdlToolbar, 'CData', cdataGrid, 'TooltipString', 'Grid On/off', 'Separator', 'On', 'ClickedCallback',{@FigUtils.gridOnOff, fig}); %#ok<NASGU>
            
            % Bouton X Normal/Reverse
            nomFicIcon = getNomFicDatabase('XDirection.jpg');
            cdataXDir = imread(nomFicIcon);
            htXInverse = uitoggletool(hdlToolbar, 'CData', cdataXDir, 'TooltipString', 'X Direction', 'ClickedCallback', {@FigUtils.xDirection, fig}); %#ok<NASGU>
            
            % Bouton Y Normal/Reverse
            nomFicIcon = getNomFicDatabase('YDirection.jpg');
            cdataYDir = imread(nomFicIcon);
            htYInverse = uitoggletool(hdlToolbar, 'CData', cdataYDir, 'TooltipString', 'Y Direction', 'ClickedCallback', {@FigUtils.yDirection, fig}); %#ok<NASGU>
            
            % Bouton Color
            nomFicIcon = getNomFicDatabase('MosaicColor.jpg');
            cdataCol = imread(nomFicIcon);
            htColCourbe = uitoggletool(hdlToolbar, 'CData', cdataCol, 'TooltipString', 'Color', 'ClickedCallback', {@FigUtils.setLineMarkerPatchColor, fig}); %#ok<NASGU>
            
            % Bouton Video Inverse
            nomFicIcon = getNomFicDatabase('VideoInverse.tif');
            cdataVideo = imread(nomFicIcon);
            htVideoInverse = uitoggletool(hdlToolbar, 'CData', cdataVideo, 'TooltipString', 'Video Inverse', 'ClickedCallback', {@FigUtils.my_videpInverse, fig}); %#ok<NASGU>
            %             end
        end
        
        
        function addDragDrop(fig, varargin)
            set(fig, 'windowbuttondownfcn', @dragCallback);
            set(fig, 'windowbuttonupfcn',   @dropCallback);
            
            draggedObjectList= [];
            
            %% Drag Callback
            function dragCallback(fig, ~)
                selectedObject = get(fig,'CurrentObject');
                try
                    selectedObjectType = get(selectedObject, 'type');
                catch
                    disp('selectedObject has no ''type'' property'); %FIXME error when drag datacursor
                    return;
                end
                acceptedType = {'figure';'axes'; 'line'; 'image'; 'scatter'; 'patch'; 'histogram'}; % accepted type for DnD
                matchedType = strfind(acceptedType, selectedObjectType); % matched type between selectedObjectType and acceptedType
                if isempty([matchedType{:}])
                    selectedObjectList = findobj(fig, 'selected', 'on'); % all selected object with different type
                    selectedObjectList.set('selected', 'off'); % unselected all object
                    draggedObjectList = [];
                else
                    selectionType = get(fig, 'selectionType');
                    
                    set(fig, 'pointer', 'fleur');
                    
                    selectedObjectList = findobj(fig, 'selected', 'on'); % all selected object with different type
                    selectedTypedObjectList = findobj(fig, 'type', selectedObjectType, '-and', 'selected', 'on'); % all selected objects of type of selected object
                    
                    selectedObjectList.set('selected', 'off'); % unselected all object
                    
                    if ~isempty(selectedTypedObjectList) && strcmp(selectionType, 'alt') % re-select objects of same type if ctrl + click
                        selectedTypedObjectList.set('selected', 'on');
                    end
                    
                    selectedObject.set('selected', 'on');
                    selectedTypedObjectList = findobj(fig, 'type', selectedObjectType, '-and', 'selected', 'on');
                    draggedObjectList = selectedTypedObjectList;
                end
            end
            
            %% Drop Callback
            function dropCallback(fromFig, ~)
                
                droppedObjectList = draggedObjectList;
                if ~isempty(droppedObjectList)
                    toFig = matlab.ui.internal.getPointerWindow;  % HG2: R2014b or newer; Pb rend la pointeur m�me si la figure est iconis�e
                    if toFig ~= 0
                        if strcmp(toFig.WindowState, 'minimized')
                            toFig = 0;
                        end
                    end
                    
                    droppedObjectType = get(droppedObjectList(1), 'type');
                    set(fromFig, 'pointer', 'arrow');
                    if toFig ~= fromFig % if departure figure and arrival figure are not the same
                        
                        for k=1:length(droppedObjectList)
                            if strcmp(droppedObjectType, 'figure')
                                % clone figure
                                clonedFig = copyobj(droppedObjectList(k), fromFig.get('parent'));
                                FigUtils.createSScFigure('figure', clonedFig);
                                pointerPos = get(0, 'PointerLocation');
                                clonedFig.Position(1) = pointerPos(1);
                                clonedFig.Position(2) = pointerPos(2) - fromFig.Position(4);
                            else
                                if toFig == 0 % create figure if dont exist
                                    toFig = FigUtils.createSScFigure;
                                    pointerPos = get(0, 'PointerLocation');
                                    toFig.Position(1) = pointerPos(1);
                                    toFig.Position(2) = pointerPos(2) - fromFig.Position(4);
                                else
                                    FigUtils.addDragDrop(toFig);
                                end
                                switch droppedObjectType
                                    case 'axes'
                                        toAxes = toFig.get('CurrentAxes');
                                        if isempty(toAxes)
                                            % clone axes
                                            clonedAxes = copyobj(droppedObjectList(k), toFig);
                                            if length(droppedObjectList) == 1 % if only one axes, take all the figure position
                                                clonedAxes.set('Units', 'normalized');
                                                clonedAxes.set('OuterPosition', [0 0 1 1]); % default position
                                                clonedAxes.set('Position', [0.1300 0.1100 0.7750 0.8150]); % default position
                                            end
                                        else
                                            % Get childrens of axes
                                            childrenObjects = droppedObjectList(k).Children;
                                            % clone children
                                            copyobj(childrenObjects, toAxes);
                                        end
                                        
                                    case {'line'; 'image'; 'scatter'; 'patch'; 'histogram'}
                                        % TODO MHO calcule get(position) quel axe
                                        toAxes = toFig.get('CurrentAxes');
                                        if isempty(toAxes) % create axes if dont exist
                                            fromAxes = fromFig.get('CurrentAxes');
                                            toAxes = copyobj(fromAxes, toFig);
                                            toAxes.set('Units', 'normalized');
                                            toAxes.set('OuterPosition', [0 0 1 1]); % default position
                                            toAxes.set('Position', [0.1300 0.1100 0.7750 0.8150]); % default position
                                            delete(toAxes.get('Children'));
                                        end
                                        % clone lines
                                        copyobj(droppedObjectList(k), toAxes);
                                        
                                    otherwise
                                        my_warndlg('message',1);
                                end
                            end
                        end
                        
                        %% Add contextMenu to all created line
                        lineList = findobj(toFig, 'Type', 'Line');
                        for k=1:length(lineList)
                            PlotUtils.addContextMenu(lineList(k));
                        end
                    end
                end
            end
        end
        
        
        function removeDragDrop(fig)
            % Remove dragndrop callback from a figure
            set(fig, 'windowbuttondownfcn',[]);
            set(fig, 'windowbuttonupfcn',  []);
        end
        
    end
    
    methods (Static, Access = private)
        
        %% Callbacks
        function gridOnOff(~,~,fig)
            currentAxes = fig.get('CurrentAxes');
            statusGrid = get(currentAxes, 'XGrid');
            if strcmp(statusGrid, 'on')
                FigUtils.gridOff(fig);
            else
                FigUtils.gridOn(fig);
            end
        end
        
        
        function gridOnCallback(~, ~, fig)
            FigUtils.gridOn(fig);
        end
        
        
        function gridOffCallback(~, ~, fig)
            FigUtils.gridOff(fig);
        end
        
        
        function gridOn(fig)
            axes = findobj(fig, 'Type', 'axes');
            for k=1:length(axes)
                grid(axes(k), 'On')
            end
        end
        
        
        function gridOff(fig)
            axes = findobj(fig, 'Type', 'axes');
            for k=1:length(axes)
                grid(axes(k), 'Off')
            end
        end
        
        
        function xDirection(~, ~, fig)
            currentAxes = fig.get('CurrentAxes');
            XDir = get(currentAxes, 'XDir');
            if strcmp(XDir, 'normal')
                set(currentAxes, 'XDir', 'Reverse');
            else
                set(currentAxes, 'XDir', 'Normal');
            end
        end
        
        
        function yDirection(~, ~, fig)
            currentAxes = fig.get('CurrentAxes');
            YDir = get(currentAxes, 'YDir');
            if strcmp(YDir, 'normal')
                set(currentAxes, 'YDir', 'Reverse');
            else
                set(currentAxes, 'YDir', 'Normal');
            end
        end
        
        
        function setbackgroundColor(~, ~, fig)
            Couleur = uisetcolor;
            if length(Couleur) == 3
                fig.Color = Couleur;
            end
        end
        
        
        function setTicksSize(~, ~, fig)
            currentAxes = fig.get('CurrentAxes');
            p = ClParametre('Name', 'Size', 'MinValue', 5, 'MaxValue', 50, 'Value', currentAxes.FontSize, 'Format', '%d');
            a = StyledSimpleParametreDialog('params', p, 'Title', 'Change the size of X and Y ticks');
            a.openDialog;
            flag = a.okPressedOut;
            if ~flag
                return
            end
            currentAxes.FontSize = a.getParamsValue;
%             currentAxes.XTickLabelRotation = 45;
        end
        
        
        function setXTicksOrientation(~, ~, fig)
            currentAxes = fig.get('CurrentAxes');
            p = ClParametre('Name', 'Size', 'MinValue', 0, 'MaxValue', 90, 'Value', currentAxes.XTickLabelRotation, 'Unit', 'deg', 'Format', '%d');
            a = StyledSimpleParametreDialog('params', p, 'Title', 'Change the orientation of X ticks');
            a.openDialog;
            flag = a.okPressedOut;
            if ~flag
                return
            end
            currentAxes.XTickLabelRotation = a.getParamsValue;
        end

        
        function setLineMarkerPatchColor(~, ~, fig)
            Couleur = uisetcolor;
            if length(Couleur) == 3
                line = findobj(fig, 'Type', 'Line');
                line.set('Color', Couleur)
                line.set('MarkerEdgeColor', Couleur)
                patch = findobj(fig, 'Type', 'Patch');
                patch.set('EdgeColor', Couleur)
                patch.set('FaceColor', Couleur)
            end
        end
        
        
        function my_videpInverse(~,~, fig)
            map = fig.get('Colormap');
            fig.set('Colormap', flipud(map))
        end
        
        
        function editXLimYLim(~, ~, fig)
            currentAxes = fig.get('CurrentAxes');
            Title = currentAxes.get('Title').get('String');
            if iscell(Title)
                Title = Title{1};
            end
            XLim = currentAxes.get('XLim');
            YLim = currentAxes.get('YLim');
            str2 = sprintf('New limits for the current axis (%s)', Title);
            
            % Create parameters
            params(1) = ClParametre('Name', 'X Max', 'Value', XLim(2));
            params(2) = ClParametre('Name', 'X Min', 'Value', XLim(1));
            params(3) = ClParametre('Name', 'Y Max', 'Value', YLim(2));
            params(4) = ClParametre('Name', 'Y Min', 'Value', YLim(1));
            
            % Create parameterDialog
            dlg = SimpleParametreDialog( 'Title', str2, 'params', params, 'sizes', [-1 -1 0 0]);
            dlg.openDialog();
            if dlg.okPressedOut
                XLimNew = sort([params(2).Value , params(1).Value]);

XLimNew

                if XLimNew(2) == XLimNew(1)
                    my_warndlg('The 2 values of XLim are equal', 1);
                else
                    currentAxes.set('XLim', XLimNew)
                end
                YLimNew = sort([params(4).Value , params(3).Value]);

YLimNew

                if YLimNew(2) == YLimNew(1)
                    my_warndlg('The 2 values of XLim are equal', 1);
                else
                    currentAxes.set('YLim', YLimNew)
                end
            end
        end
        
        
        function editCLim(~, ~, fig)
            currentAxes = fig.get('CurrentAxes');
            Title = currentAxes.get('Title').get('String');
            if iscell(Title)
                Title = Title{1};
            end
            CLim = currentAxes.CLim;
            str2 = sprintf('New color limits for the current axis (%s)', Title);
            
            % Create parameters
            params    = ClParametre('Name', 'Max', 'Value', CLim(2));
            params(2) = ClParametre('Name', 'Min', 'Value', CLim(1));
            
            % Create parameterDialog
            dlg = SimpleParametreDialog( 'Title', str2, 'params', params, 'sizes', [-1 -1 0 0]);
            dlg.openDialog();
            if   dlg.okPressedOut
                CLim = dlg.getParamsValue;
                currentAxes.CLim = sort(CLim);
            end
        end
        
        
        function splitAxisSameFrame(~,~, fig)
            axes = findobj(fig, 'Type', 'axes');
            for k=1:length(axes)
                cloneAxe(fig, axes(k))
            end
        end
        
        
        function splitAxisLabelX(~, ~, fig)
            currentAxes = fig.get('CurrentAxes');
            name = get(currentAxes, 'XLabel');
            name = get(name, 'String');
            defaultanswer = {name};
            options.Resize      = 'on';
            options.WindowStyle = 'normal';
            options.Interpreter = 'tex';
            answer = inputdlg('XLabel : ', name, 1, defaultanswer, options);
            xlabel(currentAxes, answer{1})
        end
        
        
        function splitAxisLabelY(~, ~, fig)
            currentAxes = fig.get('CurrentAxes');
            name = get(currentAxes, 'YLabel');
            name = get(name, 'String');
            defaultanswer       = {name};
            options.Resize      = 'on';
            options.WindowStyle = 'normal';
            options.Interpreter = 'tex';
            answer = inputdlg('YLabel : ', name, 1, defaultanswer, options);
            ylabel(currentAxes, answer{1})
        end
        
        
        function ticklabelmode(src, ~, fig)
            currentAxes = fig.get('CurrentAxes');
            mode = src.Label;
            currentAxes.set('XTickLabelMode', mode);
            currentAxes.set('YTickLabelMode', mode);
        end
        
        
        function changeFont(~,~,fig)
            currentAxes = fig.get('CurrentAxes');
            hText(1) = get(currentAxes, 'XLabel');
            hText(2) = get(currentAxes, 'YLabel');
            hText(3) = get(currentAxes, 'Title');
            
            str{1} = 'XLabel';  str{2} = 'YLabel';  str{3} = 'Title';
            %             str1 = 'Changer la font de';
            str2 = 'Change font on';
            [rep, flag] = my_listdlg(str2, str, 'InitialValue', 1:3);
            if ~flag
                return
            end
            
            fontstruct = uisetfont(hText(rep(1)), 'Update Font');
            if isequal(fontstruct, 0)
                return
            end
            
            if any(rep == 1)
                FigUtils.setPointFontOnHandle(hText(1), fontstruct) % apr�s
            end
            if any(rep == 2)
                FigUtils.setPointFontOnHandle(hText(2), fontstruct) % apr�s
            end
            if any(rep ==3)
                % TODO JMA: bidouille pour rattraper une anomalie Matlab. tester sur
                % les nouvelles versions si ce bug est corrig�
                %         fontstruct = getPointFontFromHandle(GCBO);
                fontstruct.FontSize = fontstruct.FontSize + 3; % +3 a l'air de fonctionner pour le titre mais pas pour xlabel et ylabel
                FigUtils.setPointFontOnHandle(hText(3), fontstruct) % apr�s
            end
        end
        
        
        function setPointFontOnHandle(fhandle, fontstruct)
            tempunits = get(fhandle,'FontUnits');
            try
                set(fhandle,fontstruct);
            catch ex %#ok<NASGU>
            end
            set(fhandle,'FontUnits',tempunits);
        end
        
        
        function my_boxOn(~, ~, fig)
            axes = findobj(fig, 'Type', 'axes');
            for k=1:length(axes)
                box(axes(k), 'On')
            end
        end
        
        
        function my_boxOff(~, ~, fig)
            axes = findobj(fig, 'Type', 'axes');
            for k=1:length(axes)
                box(axes(k), 'Off')
            end
        end
        
        
        function plotSymetrieHorizontale(~,~, fig)
            currentAxes = fig.get('CurrentAxes');
            hc = findobj(currentAxes, 'Type', 'Line');
            hold on;
            for k=1:length(hc)
                XData = get(hc(k), 'XData');
                YData = get(hc(k), 'YData');
                Color = get(hc(k), 'Color');
                hp = PlotUtils.createSScPlot(-XData, YData); set(hp, 'Color', Color * 0.8);
            end
            hold off
        end
        
        
        function calculette(varargin)
            %             str1 = 'Mini calculette.';
            str2 = 'Mini calculator';
            [flag, result] = inputOneParametre(str2, 'Write your formula', 'Format', '%g');
            if ~flag
                return
            end
            result %#ok<NOPRT>
        end
        
        
        function pointAndClick(varargin)
            % TODO : fen�tre d'explication
            GCA = gca;

            str = sprintf('Click left on the figure on all the points you want.\nReturn to end');
            my_txtdlg('How to create points', str);

            [x,y] = ginput;
            n = length(x);
%             hold on; scatter(x, y, 'filled', 'b')
            Coul = jet(n);
            hold on; scatter(x, y, [], Coul, 'filled')
            str = cell(n,1);
            strX = cell(n,1);
            strY = cell(n,1);
            for k=1:n
                str{k,1} = sprintf('  P %d', k);
                text(x(k), y(k), str{k}, 'Color', Coul(k,:), 'FontSize',14)
                strX{k,1} = num2strPrecis(x(k));
                strY{k,1} = num2strPrecis(y(k));
            end
            T = table(str, strX, strY, 'VariableNames', {'Names', GCA.XLabel.String, GCA.YLabel.String});
            T %#ok<NOPRT>
            nomFicTable = my_tempname('.csv');
            writetable(T, nomFicTable, 'Delimiter', ';')
            winopen(nomFicTable)
        end


        function unionAxis(~,~,fig)
            hc = findobj(fig, 'Type', 'Line');
            
            [rep, flag] = my_questdlg('Keep the line colors ?');
            if ~flag
                return
            end
            if rep == 2
                TableCouleur = get(fig.get('CurrentAxes'), 'ColorOrder');
                nbColors = size(TableCouleur,1);
            end
            
            fig2 = FigUtils.createSScFigure;
            hold on; grid on; box on;
            ha = fig2.get('CurrentAxes');
            for k=1:length(hc)
                XData = get(hc(k), 'XData');
                YData = get(hc(k), 'YData');
                if rep == 1
                    Color = get(hc(k), 'Color');
                else
                    kColor = 1 + mod(k-1,nbColors);
                    Color = TableCouleur(kColor,:);
                end
                hp = PlotUtils.createSScPlot(ha, XData, YData); set(hp, 'Color', Color);
            end
            hold off
        end
        
        
        function unionCurves(~, ~, fig)
            hAxeIn = findobj(fig, 'Type', 'axes');
            na = length(hAxeIn);
            
            FigUtils.createSScFigure; % Ajout� par JMA le 23/04/2019
            hac = subplot(1,1,1); hold on;
            
            for ka=1:na
                XData = [];
                YData = [];
                ZData = [];
                hcc = findobj(hAxeIn(ka), 'Type', 'line');
                if isempty(hcc)
                    continue
                end
                for kc=1:length(hcc)
                    XData = [XData get(hcc(kc), 'XData')]; %#ok<AGROW>
                    YData = [YData get(hcc(kc), 'YData')]; %#ok<AGROW>
                    ZData = [ZData get(hcc(kc), 'ZData')]; %#ok<AGROW>
                end
                
                %             subNonNaN = ~isnan(XData) & ~isnan(YData);
                %             XData = XData(subNonNaN);
                %             YData = YData(subNonNaN);
                %             [~,subOrdre] = sort(XData);
                %             XData = XData(subOrdre);
                %             YData = YData(subOrdre);
                
                %                 hac = subplot(na,1,ka); % Comment� par JMA le 23/04/2019
                hnew = PlotUtils.createSScPlot(hac, XData, YData);
                if ~isempty(ZData)
                    %                 ZData = ZData(subNonNaN);
                    %                 ZData = ZData(subOrdre);
                    set(hnew, 'ZData', ZData)
                end
                set(hnew, 'Color',       get(hcc(1), 'Color'))
                set(hnew, 'LineStyle',   get(hcc(1), 'LineStyle'))
                set(hnew, 'LineWidth',   get(hcc(1), 'LineWidth'))
                set(hnew, 'Marker',      get(hcc(1), 'Marker'))
                set(hnew, 'MarkerSize',  get(hcc(1), 'MarkerSize'))
                grid on; box on;
            end
        end
        
        
        function derivative(~, ~, fig)
            hAxeIn = findobj(fig, 'Type', 'axes');
            
            na = length(hAxeIn);
            FigDerivative = FigUtils.createSScFigure;
            for ka=1:na
                hcc = findobj(hAxeIn(ka), 'Type', 'line');
                if isempty(hcc)
                    continue
                end
                for kc=1:length(hcc)
                    XData = get(hcc(kc), 'XData');
                    YData = get(hcc(kc), 'YData');
                    ZData = get(hcc(kc), 'ZData');
                    
                    YData = gradient(YData) ./ gradient(XData);
                    
                    hac = subplot(na,1,na-ka+1); hold on;
                    hnew = PlotUtils.createSScPlot(hac, XData, YData);
                    if ~isempty(ZData)
                        %                 ZData = ZData(subNonNaN);
                        %                 ZData = ZData(subOrdre);
                        set(hnew, 'ZData', ZData)
                    end
                    set(hnew, 'Color',      get(hcc(kc), 'Color'))
                    set(hnew, 'LineStyle',  get(hcc(kc), 'LineStyle'))
                    set(hnew, 'LineWidth',  get(hcc(kc), 'LineWidth'))
                    set(hnew, 'Marker',     get(hcc(kc), 'Marker'))
                    set(hnew, 'MarkerSize', get(hcc(kc), 'MarkerSize'))
                    grid on; box on;
                    
                    CoordPoints = getappdata(hcc(kc), 'CoordPoints');
                    setappdata(hnew, 'CoordPoints', CoordPoints);
                    
                end
                titleAxe = get(get(get(hcc(1), 'Parent'), 'Title'), 'String');
                if iscell(titleAxe)
                    titleAxe = titleAxe{1};
                end
                title(hac, ['Derivative of ' titleAxe], 'Interpreter', 'None')
            end
            hAxeOut = findobj(FigDerivative, 'Type', 'axes');
            FigUtils.linkAxesAuto(hAxeIn, hAxeOut)
        end
        
        
        function filter(~, ~, fig)
            hAxeIn = findobj(fig, 'Type', 'axes');
            na = length(hAxeIn);
            
            itemList = {'Freq 1/5'; 'Freq 1/10'; 'Freq 1/20'; 'Freq 1/50'; 'Freq 1/100'; 'Freq 1/200'};
            [selectedIndex, ok] = my_listdlg('Filter', itemList, 'SelectionMode', 'Single');
            if ~ok
                return
            end
            Label = itemList{selectedIndex};
            Freq = eval(Label(6:end));
            [B,A] = butter(2, Freq);
            
            FigFilter = FigUtils.createSScFigure;
            for ka=1:na
                hcc = findobj(hAxeIn(ka), 'Type', 'line');
                if isempty(hcc)
                    continue
                end
                for kc=1:length(hcc)
                    XData = get(hcc(kc), 'XData');
                    YData = get(hcc(kc), 'YData');
                    ZData = get(hcc(kc), 'ZData');
                    
                    Y = YData;
                    subNonNaN = ~isnan(Y);
                    Y(subNonNaN) = filtfilt(B, A, double(YData(subNonNaN)));
                    YData = Y;
                    
                    hac = subplot(na,1,na-ka+1); hold on;
                    hnew = PlotUtils.createSScPlot(hac, XData, YData);
                    if ~isempty(ZData)
                        %                 ZData = ZData(subNonNaN);
                        %                 ZData = ZData(subOrdre);
                        set(hnew, 'ZData', ZData)
                    end
                    set(hnew, 'Color',      get(hcc(kc), 'Color'))
                    set(hnew, 'LineStyle',  get(hcc(kc), 'LineStyle'))
                    set(hnew, 'LineWidth',  get(hcc(kc), 'LineWidth'))
                    set(hnew, 'Marker',     get(hcc(kc), 'Marker'))
                    set(hnew, 'MarkerSize', get(hcc(kc), 'MarkerSize'))
                    grid on; box on;
                    CoordPoints = getappdata(hcc(kc), 'CoordPoints');
                    setappdata(hnew, 'CoordPoints', CoordPoints);
                end
                titleAxe = get(get(get(hcc(1), 'Parent'), 'Title'), 'String');
                if iscell(titleAxe)
                    titleAxe = titleAxe{1};
                end
                title(hac, ['Filter of ' titleAxe], 'Interpreter', 'None')
            end
            hAxeOut = findobj(FigFilter, 'Type', 'axes');
            FigUtils.linkAxesAuto(hAxeIn, hAxeOut)
        end
        
        
        function exportCurvesSerial(~, ~, fig)
            persistent persistent_nomDir
            
            if isempty(persistent_nomDir)
                nomDir = my_tempdir;
            else
                nomDir = persistent_nomDir;
            end
            [flag, nomFic] = my_uiputfile('*.csv', 'Save as', nomDir);
            if ~flag
                return
            end
            persistent_nomDir = fileparts(nomFic);
            
            fid = fopen(nomFic, 'w+');
            if fid == -1
                messageErreurFichier(nomFic, 'WriteFailure');
                return
            end
            
            % Comma or not comma ?
            sep = getCSVseparator; % TODO : ajout� sauvagement par JMA le 13/04/2019
            
            %         hc1 = findobj(gcf, 'Type', 'Line', 'Tag', 'Current Extent');
            %         hc2 = findobj(gcf, 'Type', 'Line', '-not', 'Tag', 'Current Extent');
            %         hc  = [hc1;hc2];
            [flag, hc] = FigUtils.selectAxes(fig, 1);
            if ~flag
                return
            end
            
            for k=1:size(hc,1)
                XData{k}     = []; %#ok<AGROW>
                YData{k}     = []; %#ok<AGROW>
                XPixels{k}   = []; %#ok<AGROW>
                YPixels{k}   = []; %#ok<AGROW>
                LatPixels{k} = []; %#ok<AGROW>
                LonPixels{k} = []; %#ok<AGROW>
                UserData{k}  = []; %#ok<AGROW>
                for k2=1:size(hc,2)
                    xd = get(hc(k,k2), 'XData');
                    yd = get(hc(k,k2), 'YData');
                    XData{k} = [XData{k} xd];
                    YData{k} = [YData{k} yd];
                    try
                        BrushData{k} = get(hc(k,k2), 'BrushData'); %#ok<AGROW>
                    catch %#ok<CTCH>
                        BrushData{k} = false(1,length(XData{k})); %#ok<AGROW>
                    end
                    ud = get(hc(k,k2), 'UserData');
                    UserData{k} = [UserData{k} ud];
                    
                    % Ajout JMA le 13/04/2019
                    CoordPoints = getappdata(hc(k,k2), 'CoordPoints');
                    if isempty(CoordPoints)
                        ProfileXPixels = [];
                        ProfileYPixels = [];
                    else
                        ProfileXPixels   = CoordPoints.X;
                        ProfileYPixels   = CoordPoints.Y;
                        ProfileLatPixels = CoordPoints.Latitude;
                        ProfileLonPixels = CoordPoints.Longitude;
                        if ~isempty(ProfileXPixels) && ~isempty(ProfileYPixels)
                            XPixels{k}   = [XPixels{k}   ProfileXPixels];
                            YPixels{k}   = [YPixels{k}   ProfileYPixels];
                            LatPixels{k} = [LatPixels{k} ProfileLatPixels];
                            LonPixels{k} = [LonPixels{k} ProfileLonPixels];
                        end
                    end
                end
                if k <= length(hc)
                    Titre = get(get(get(hc(k), 'Parent'), 'Title'), 'String');
                    if isempty(Titre)
                        Title{k} = sprintf('Curve %d', k); %#ok<AGROW>
                    else
                        Titre = Titre(1,:);
                        %                     Title{k} = [Titre '-Current_Extent']; %#ok<AGROW>
                        Title{k} = Titre ; %#ok<AGROW>
                    end
                else
                    Title{k} = get(get(get(hc(k), 'Parent'), 'Title'), 'String'); %#ok<AGROW>
                    if isempty(Title{k})
                        % Cas du SoundSpeedProfile
                        Title{k} = get(fig, 'Name'); %#ok<AGROW>
                    end
                end
            end
            
            subKO = (BrushData{k} == 1);
            XData{k}(subKO) = [];
            YData{k}(subKO) = [];
            if ~isempty(ProfileXPixels) && ~isempty(ProfileYPixels)
                XPixels{k}(subKO) = [];
                YPixels{k}(subKO) = [];
                if ~isempty(LatPixels)
                    LatPixels{k}(subKO) = [];
                    LonPixels{k}(subKO) = [];
                end
            end
            if ~isempty(UserData{k})
                UserData{k}(subKO) = [];
            end
            
            if isempty(UserData{1})
                typeTime = 'num';
            else
                typeTime = class(UserData{1});
            end
            
            
            switch typeTime
                case 'cl_time'
                    fprintf(fid, 'Time,  T(s)');
                    for k2=1:length(YData)
                        fprintf(fid,'%s%s', sep, Title{k2});
                    end
                    fprintf(fid, '\n');
                    
                case 'datetime'
                    fprintf(fid, 'Time,  T(s)');
                    for k2=1:length(YData)
                        fprintf(fid,'%s%s', sep, Title{k2});
                    end
                    fprintf(fid, '\n');
                    
                otherwise
                    if ~isempty(ProfileXPixels) && ~isempty(ProfileYPixels)
                        if ~isempty(LatPixels)
                            fprintf(fid, 'Lat%sLon%sX%sY%sAbscissa', sep, sep, sep, sep);
                        else
                            fprintf(fid, 'X%sY%sAbscissa', sep, sep);
                        end
                    else
                        fprintf(fid, 'Abscissa');
                    end
                    for k2=1:length(YData)
                        %                 fprintf(fid,'; %s', Title{k2});
                        if iscell(Title{k2})
                            Titre = cell2str(Title{k2});
                        else
                            Titre = Title{k2};
                        end
                        fprintf(fid,'%s%s', sep, Titre);
                    end
                    fprintf(fid, '\n');
            end
            
            if isempty(UserData{1})
                [XDataUnique, indOrdre] = unique([XData{:}]);
                
            elseif isa(UserData{1}, 'cl_time')
                XDataUnique = [];
                for k=1:length(UserData)
                    [XDataUnique, indOrdre] = unique([XDataUnique UserData{k}.timeMat]);
                    date{k}  = datestr(UserData{k}.timeMat, 'dd/mm/yyyy'); %#ok<AGROW>
                    heure{k} = datestr(UserData{k}.timeMat, 'HH:MM:SS.FFF'); %#ok<AGROW>
                    T{k} = UserData{k}.timeMat; %#ok<AGROW>
                    T{k} = T{k} - T{k}(1); %#ok<AGROW>
                    T{k} = T{k} * (24 * 3600); %#ok<AGROW>
                end
                
            elseif isa(UserData{1}, 'datetime')
                XDataUnique = datetime.empty();
                for k=1:length(UserData)
                    [XDataUnique, indOrdre] = unique([XDataUnique UserData{k}]);
                    date{k}  = datestr(UserData{k}, 'dd/mm/yyyy'); %#ok<AGROW>
                    heure{k} = datestr(UserData{k}, 'HH:MM:SS.FFF'); %#ok<AGROW>
                    T{k} = UserData{k}; %#ok<AGROW>
                    T{k} = T{k} - T{k}(1); %#ok<AGROW>
                    T{k} = T{k} * (24 * 3600); %#ok<AGROW>
                end
                
            else
                [XDataUnique, indOrdre] = unique([XData{:}]);
            end
            
            % TODO : XPixels !!!
            if ~isempty(ProfileXPixels) && ~isempty(ProfileYPixels)
                XPixelsUnique = [XPixels{:}];
                XPixelsUnique = XPixelsUnique(indOrdre);
                YPixelsUnique = [YPixels{:}];
                YPixelsUnique = YPixelsUnique(indOrdre);
                if ~isempty(LatPixels)
                    LatPixelsUnique = [LatPixels{:}];
                    LatPixelsUnique = LatPixelsUnique(indOrdre);
                    LonPixelsUnique = [LonPixels{:}];
                    LonPixelsUnique = LonPixelsUnique(indOrdre);
                end
            end
            
            N = length(XDataUnique);
            %             str1 = 'Ecriture du fichier en cours.';
            str2 = 'Export in action.';
            hw = create_waitbar(str2, 'N', N);
            for k1=1:N
                my_waitbar(k1, N, hw)
                switch typeTime
                    case 'cl_time'
                        fprintf(fid,'%s %s%s%f', date{k2}(k1,:), heure{k2}(k1,:), sep, T{k}(k1));
                        
                    case 'datetime'
                        fprintf(fid,'%s %s%s%f', date{k2}(k1,:), heure{k2}(k1,:), sep, T{k}(k1));
                        
                    otherwise
                        if ~isempty(ProfileXPixels) && ~isempty(ProfileYPixels)
                            if ~isempty(LatPixels)
                                fprintf(fid, '%s%s%s%s%s%s%s%s%s', ...
                                    num2strPrecis(LatPixelsUnique(k1)), sep, num2strPrecis(LonPixelsUnique(k1)), sep, ...
                                    num2strPrecis(XPixelsUnique(k1)),   sep, num2strPrecis(YPixelsUnique(k1)),   sep, ...
                                    num2strPrecis(XDataUnique(k1)));
                            else
                                fprintf(fid, '%s%s%s%s%s', ...
                                    num2strPrecis(XPixelsUnique(k1)), sep, num2strPrecis(YPixelsUnique(k1)), ...
                                    sep, num2strPrecis(XDataUnique(k1)));
                            end
                        else
                            fprintf(fid, '%s', num2strPrecis(XDataUnique(k1)));
                        end
                end
                
                for k2=1:length(YData)
                    switch typeTime
                        case 'cl_time'
                            [C,IA] = intersect(UserData{k2}.timeMat, XDataUnique(k1));
                        case 'datetime'
                            [C,IA] = intersect(UserData{k2}, XDataUnique(k1));
                        otherwise
                            [C,IA] = intersect(XData{k2}, XDataUnique(k1));
                    end
                    
                    if isempty(C)
                        fprintf(fid, '%s', sep);
                    else
                        fprintf(fid,'%s%s', sep, num2strPrecis(YData{k2}(IA)));
                    end
                end
                fprintf(fid, '\n');
            end
            fclose(fid);
            my_close(hw);
        end
        
        
        function exportCurvesParallel(~, ~, fig)
            persistent persistent_nomDir
            
            if isempty(persistent_nomDir)
                nomDir = my_tempdir;
            else
                nomDir = persistent_nomDir;
            end
            [flag, nomFic] = my_uiputfile('*.csv', 'Save as', nomDir);
            if ~flag
                return
            end
            persistent_nomDir = fileparts(nomFic);
            
            fid = fopen(nomFic, 'w+');
            if fid == -1
                messageErreurFichier(nomFic, 'WriteFailure');
                return
            end
            
            % Comma or not comma ?
            sep = getCSVseparator; % TODO JMA : ajout� sauvagement par JMA le 13/04/2019
            
            GCA = gca;
            [flag, hc] = FigUtils.selectAxes(fig, 1);
            if ~flag
                return
            end
            
            hc = flipud(hc(:));
            XLabel = get(GCA, 'XLabel');
            XLabel = XLabel.String;
            
            TabXData      = {};
            TabYData      = {};
            TabXPixels    = {};
            TabYPixels    = {};
            TabLatPixels  = {};
            TabLonPixels  = {};
            TabTitre      = {};
            TabIndSegment = [];
            TabIndCurve   = [];
            TabArray      = [];
            for k=1:numel(hc)
                if isa(hc(k), 'matlab.graphics.GraphicsPlaceholder')
                    continue
                end
                XData = get(hc(k), 'XData');
                YData = get(hc(k), 'YData');
                try
                    BrushData = get(hc(k), 'BrushData');
                catch %#ok<CTCH>
                    BrushData = false(size(XData));
                end
                
                % Ajout JMA le 13/04/2019
                % TODO : le setappdata est r�alis� dans traiter_click_coupe
                % Ce n'est pas tr�s coh�rent. Il faudrait reanalyser le
                % code et le r�agencer si besoin. Voir forge #1109
                
                CoordPoints = getappdata(hc(k), 'CoordPoints');
                if isempty(CoordPoints)
                    Titre = get(hc(k), 'DisplayName');
                    if isempty(Titre)
                        get(get(get(hc(k), 'Parent'), 'Title'), 'String');
                    end
                    ProfileXPixels   = [];
                    ProfileYPixels   = [];
                    ProfileLatPixels = [];
                    ProfileLonPixels = [];
                else
                    Titre            = CoordPoints.Name;
                    ProfileXPixels   = CoordPoints.X;
                    ProfileYPixels   = CoordPoints.Y;
                    ProfileLatPixels = CoordPoints.Latitude;
                    ProfileLonPixels = CoordPoints.Longitude;
                end
                
                if isempty(Titre)
                    Titre = sprintf('Curve %d', k);
                else
                    Titre = Titre(1,:);
                    if iscell(Titre)
                        Titre = Titre{1};
                    end
                end
                
                subKO = (BrushData == 1);
                XData(subKO) = [];
                YData(subKO) = [];
                
                TabXData{end+1} = XData; %#ok<AGROW>
                TabYData{end+1} = YData; %#ok<AGROW>
                TabTitre{end+1} = Titre; %#ok<AGROW>
                if isempty(CoordPoints)
                    TabIndCurve(end+1) = k; %#ok<AGROW>
                else
                    TabIndSegment(end+1) = CoordPoints.indSegment; %#ok<AGROW>
                    TabIndCurve(end+1)   = CoordPoints.indCurve; %#ok<AGROW>
                end
                
                if ~isempty(ProfileXPixels) && ~isempty(ProfileYPixels)
                    ProfileXPixels(subKO) = []; %#ok<AGROW>
                    ProfileYPixels(subKO) = []; %#ok<AGROW>
                    TabXPixels{end+1} = ProfileXPixels; %#ok<AGROW>
                    TabYPixels{end+1} = ProfileYPixels; %#ok<AGROW>
                    if ~isempty(ProfileLatPixels)
                        ProfileLatPixels(subKO) = []; %#ok<AGROW>
                        ProfileLonPixels(subKO) = []; %#ok<AGROW>
                        TabLatPixels{end+1} = ProfileLatPixels; %#ok<AGROW>
                        TabLonPixels{end+1} = ProfileLonPixels; %#ok<AGROW>
                    end
                end
            end
            
            %             XData = [];
            nbCourbes = length(TabXData);
            %             for k=1:nbCourbes
            %                 [XData, indOrdre] = unique([XData(:); TabXData{k}(:)]);
            %             end
            Temp = [TabXData{:}];
            [XData, indOrdre] = unique(Temp(:));
            if ~isempty(ProfileXPixels) && ~isempty(ProfileYPixels)
                Temp = [TabXPixels{:}];
                XProfil = Temp(indOrdre);
                Temp = [TabYPixels{:}];
                YProfil = Temp(indOrdre);
                if ~isempty(ProfileLatPixels)
                    Temp = [TabLatPixels{:}];
                    LatProfil = Temp(indOrdre);
                    Temp = [TabLonPixels{:}];
                    LonProfil = Temp(indOrdre);
                end
            end
            
            if isempty(TabArray)
                TabArray = NaN(length(XData), 1);
                if isa(XData, 'datetime')
                    TabArray(:,1) = datenum(XData);
                else
                    TabArray(:,1) = XData;
                end
            end
            
            if ~isempty(ProfileXPixels) && ~isempty(ProfileYPixels)
                if isempty(ProfileLatPixels)
                    TabArray(:,2) = XProfil(:);
                    TabArray(:,3) = YProfil(:);
                    nCoord = 2;
                else
                    TabArray(:,2) = LatProfil(:);
                    TabArray(:,3) = LonProfil(:);
                    TabArray(:,4) = XProfil(:);
                    TabArray(:,5) = YProfil(:);
                    nCoord = 4;
                end
            else
                nCoord = 0;
            end
            
            TabArray(:,(1+nCoord+1):(1+nCoord+nbCourbes)) = NaN;
            for k=1:nbCourbes
                [~, ia, ib] = intersect(XData, TabXData{k});
                y = TabYData{k}(ib);
                indCurve = TabIndCurve(k);
                %                 indSegment = TabIndSegment(k);
                TabArray(ia,1+nCoord+indCurve) = y(:);
            end
            
            if isempty(XLabel) && isa(XData', 'datetime')
                XLabel = 'Time';
            end
            fprintf(fid, '%s%s', XLabel, sep);
            if ~isempty(ProfileXPixels) && ~isempty(ProfileYPixels)
                if isempty(ProfileLatPixels)
                    fprintf(fid, 'X%s', sep);
                    fprintf(fid, 'Y%s', sep);
                else
                    fprintf(fid, 'Lat%s', sep);
                    fprintf(fid, 'Lon%s', sep);
                    fprintf(fid, 'X%s',   sep);
                    fprintf(fid, 'Y%s',   sep);
                end
            end
            for k2=1:max(TabIndCurve)
                fprintf(fid, '%s%s', TabTitre{k2}, sep);
            end
            fprintf(fid, '\n');
            for k1=1:size(TabArray,1)
                for k2=1:size(TabArray,2)
                    fprintf(fid, '%s%s', num2strPrecis(TabArray(k1,k2)), sep);
                end
                fprintf(fid, '\n');
            end
            fclose(fid);
        end
        
        
        function [flag, hc] = selectAxes(fig, nbMinSignals)
            % Avant
            ha = findobj(fig, 'Type', 'Axes');
            for k=1:length(ha)
                hc1 = findobj(ha(k), 'Type', 'Line', 'Tag', 'Current Extent');
                hc2 = findobj(ha(k), 'Type', 'Line', '-not', 'Tag', 'Current Extent');
                hc12 = [hc1; hc2];
                hc(k,1:length(hc12)) = hc12; %#ok<AGROW>
            end
            
            nc = size(hc, 1);
            
            %             str1 = sprintf('Il faut au moins %d signaux pour faire cette op�ration.', nbMinSignals);
            str2 = sprintf('You need at least %d signals for this action.', nbMinSignals);
            if nc < nbMinSignals
                my_warndlg(str2, 1);
                flag = 0;
                return
            end
            
            for k=1:nc
                if isa(hc(k,1), 'matlab.graphics.GraphicsPlaceholder') % Rajout� par JMA le 17/09/2019 (cas des figures image + signaux)
                    continue
                end
                GCAF1 = get(hc(k,1), 'Parent');
                Titre = get(get(GCAF1, 'Title'), 'String');
                if isempty(Titre)
                    %                 subsub(k) = false; %#ok<AGROW>
                    subsub(k) = true; %#ok<AGROW>
                    Titre = sprintf('Curve %d', k);
                else
                    Titre = rmblank(Titre(1,:));
                    subsub(k) = true; %#ok<AGROW>
                end
                SignalName{k} = Titre; %#ok<AGROW>
            end
            SignalName = SignalName(subsub);
            hc = hc(subsub,:);
            nc = size(hc, 1);
            %             str3 = 'Liste des signaux';
            str4 = 'Signals list';
            [sub, flag] = my_listdlgMultiple(str4, SignalName, 'InitialValue', 1:nc);
            if ~flag
                return
            end
            hc = hc(sub,:);
            nc = size(hc,1);
            if nc < nbMinSignals
                my_warndlg(str2, 1);
                flag = 0;
                return
            end
            flag = 1;
        end
        
        
        function mesureX(~, ~, fig)
            k = waitforbuttonpress; %#ok<NASGU>
            point1 = get(fig.get('CurrentAxes'),'CurrentPoint');    % button down detected
            finalRect = rbbox;                   %#ok<NASGU> % return figure units
            point2 = get(fig.get('CurrentAxes'),'CurrentPoint');    % button up detected
            point1 = point1(1,1:2);              % extract x and y
            point2 = point2(1,1:2);
            hold on
            axis manual
            h = plot([point1(1) point2(1)], [point1(2) point1(2)], 'k');
            set(h, 'LineWidth', 3)
            cmenu = uicontextmenu;
            str = ['Length : ', num2str(abs(point1(1) - point2(1)))];
            uimenu(cmenu, 'Text', str);
            
            currentAxes = fig.get('CurrentAxes');
            Children = get(currentAxes, 'Children');
            for k=1:length(Children)
                Time = get(Children(k), 'UserData');
                if ~isempty(Time')
                    XData = get(Children(k), 'XData');
                    xmin = min(point1(1), point2(1));
                    xmax = max(point1(1), point2(1));
                    sub = find((XData >= xmin) & (XData <= xmax));
                    if isempty(sub)
                        Time = 0;
                    else
                        if isa(Time, 'cl_time')
                            Time = Time.timeMat;
                        elseif isa(Time, 'datetime')
                            Time = datenum(Time);
                        end
                        Time = Time(sub(end)) - Time(sub(1));
                        Time = Time * (24 * 3600);
                    end
                    str = ['Time : ', num2str(Time) ' s'];
                    uimenu(cmenu, 'Text', str);
                end
            end
            
            uimenu(cmenu, 'Text', 'Delete', 'Callback', 'delete(gco)');
            set(h, 'UIContextMenu', cmenu);
        end
        
        
        function mesureY(~, ~, fig)
            k = waitforbuttonpress; %#ok<NASGU>
            point1 = get(fig.get('CurrentAxes'),'CurrentPoint');    % button down detected
            finalRect = rbbox;                   %#ok<NASGU> % return figure units
            point2 = get(fig.get('CurrentAxes'),'CurrentPoint');    % button up detected
            point1 = point1(1,1:2);              % extract x and y
            point2 = point2(1,1:2);
            hold on
            axis manual
            h = plot([point1(1) point1(1)], [point1(2) point2(2)], 'k');
            set(h, 'LineWidth', 3)
            cmenu = uicontextmenu;
            str = ['Length : ', num2str(abs(point1(2) - point2(2)))];
            uimenu(cmenu, 'Text', str);
            uimenu(cmenu, 'Text', 'Delete', 'Callback', 'delete(gco)');
            set(h, 'UIContextMenu', cmenu);
        end
        
        
        function mesureXY(~, ~, fig)
            k = waitforbuttonpress; %#ok<NASGU>
            point1 = get(fig.get('CurrentAxes'),'CurrentPoint');    % button down detected
            finalRect = rbbox;                   %#ok<NASGU> % return figure units
            point2 = get(fig.get('CurrentAxes'),'CurrentPoint');    % button up detected
            point1 = point1(1,1:2);              % extract x and y
            point2 = point2(1,1:2);
            hold on
            axis manual
            h = plot([point1(1) point2(1)], [point1(2) point2(2)], 'k');
            set(h, 'LineWidth', 3)
            cmenu = uicontextmenu;
            str = ['Length : ', num2str(sqrt(sum((point2-point1).^2)))];
            uimenu(cmenu, 'Text', str);
            Angle = atan2(point1(2)-point2(2),point1(1)-point2(1)) * (180/pi) - 90;
            if Angle < -180
                Angle = Angle + 360;
            end
            str = ['Angle : ', num2str(Angle) ' (Valid only if X and Y axis are in meters)'];
            uimenu(cmenu, 'Text', str);
            uimenu(cmenu, 'Text', 'Delete', 'Callback', 'delete(gco)');
            set(h, 'UIContextMenu', cmenu);
        end
        
        
        function crossPlot(~, ~, fig)
            [flag, hc] = FigUtils.selectCurves(fig, 2);
            if ~flag
                return
            end
            FigUtils.createSScFigure;
            nc = length(hc);
            k = 1;
            for k1=1:nc
                for k2=1:nc
                    GCAF1 = get(hc(k1), 'Parent');
                    GCAF2 = get(hc(k2), 'Parent');
                    Title1 = get(get(GCAF1, 'Title'), 'String');
                    Title2 = get(get(GCAF2, 'Title'), 'String');
                    XData1 = get(hc(k1), 'XData');
                    XData2 = get(hc(k2), 'XData');
                    YData1 = get(hc(k1), 'YData');
                    YData2 = get(hc(k2), 'YData');
                    XLim1  = get(GCAF1,  'XLim');
                    XLim2  = get(GCAF2,  'XLim');
                    
                    sub = (XData1 >= XLim1(1)) & (XData1 <= XLim1(2)) & ~isnan(YData1) ...
                        & (XData2 >= XLim2(1)) & (XData2 <= XLim2(2)) & ~isnan(YData2);
                    YData1 = YData1(sub);
                    YData2 = YData2(sub);
                    XData1 = XData1(sub);
                    XData2 = XData2(sub); %#ok<NASGU>
                    
                    subplot(nc,nc,k)
                    
                    M1 = mean(YData1);
                    M2 = mean(YData2);
                    
                    %% Ellipse de covariance
                    covMx = cov(YData1-M1,YData2-M2);
                    [V,D] = eig(covMx);
                    angle = linspace(0, 2*pi, 360);
                    e = V*sqrt(D) * [cos(angle) ; sin(angle)];
                    %STD = 1;                     % 2 standard deviations
                    %                 conf = 2*normcdf(STD)-1;     % covers around 95% of population
                    %                 scale = chi2inv(conf,2);     % inverse chi-squared with dof=#dimensions
                    %                 covMx = covMx * scale;
                    
                    
                    %                 [D, order] = sort(diag(D), 'descend');
                    %                 D = diag(D);
                    %                 V = V(:, order);
                    
                    % project circle back to orig space
                    
                    %%
                    ellipse = dist1mahal([YData1(:), YData2(:)]);
                    
                    if k1 <= k2
                        PlotUtils.createSScPlot(YData1, YData2, '*'); grid on; box on;
                    else
                        PlotUtils.createSScPlot(YData1, YData2, '.'); grid on; box on;
                        [N, binsX, binsY] = histo2D(YData1, YData2);
                        imagesc(binsX, binsY, N)
                    end
                    
                    if k1 == k2
                        PlotUtils.createSScPlot(XData1, YData1, '-'); grid on; box on;
                        Title1 = 'x';
                    elseif k1 ~= k2
                        % TODO JMA : ellipse pas ma�tris�e
                        %%{
                        hold on;
                        hE = plot(ellipse(:,1), ellipse(:,2), 'k');
                        plot(e(1,:)+M1, e(2,:)+M2, 'r');
                        quiver(M1,M2,V(1,1), V(2,1), 'r');
                        quiver(M1,M2,V(1,2), V(2,2), 'r');
                        set(hE, 'LineWidth', 2)
                        %                     axis equal
                        %}
                    end
                    
                    xlabel(Title1)
                    ylabel(Title2)
                    k = k + 1;
                end
            end
        end
        
        
        function selfCorrelation(~, ~, fig)
            [flag, hc] = FigUtils.selectCurves(fig, 1);
            if ~flag
                return
            end
            nc = length(hc);
            FigUtils.createSScFigure;
            for k=1:nc
                XData1 = get(hc(k), 'XData');
                YData1 = get(hc(k), 'YData');
                GCAF1 = get(hc(k), 'Parent');
                
                XLim1  = get(GCAF1, 'XLim');
                
                sub = (XData1 >= XLim1(1)) & (XData1 <= XLim1(2)) & ~isnan(YData1);
                XData1 = XData1(sub);
                YData1 = YData1(sub);
                
                nomSignal = get(get(GCAF1, 'Title'), 'String');
                % Ajout JMA le 03/04/2019
                if iscell(nomSignal)
                    nomSignal = nomSignal{end};
                end
                
                resol = mean(diff(XData1));
                
                [C, x, ~, yModele, strModel]  = my_xcorr(YData1, 'nomSignal', nomSignal, 'resol', resol, 'cl_time', 0);
                
                n = length(C);
                sub = floor(n/2):n;
                hc2(k) = subplot(nc,1,nc-k+1); %#ok<AGROW>
                PlotUtils.createSScPlot(x(sub), C(sub)); grid on; box on;
                hold on; hm = PlotUtils.createSScPlot(x(sub), yModele(sub), 'r');
                title(['Self corr - ' nomSignal], 'Interpreter', 'none'); % title(strModel, 'Interpreter', 'latex')
                set(hm, 'Tag', 'Model')
                %             hold on; PlotUtils.createSScPlot([-ixMaxC -ixMaxC], [min(C) max(C)], 'k'); title(['Shift : ' num2str(-ixMaxC)])
                %             Titre = sprintf('%s %s', nomSignal, strModel);
                %             title(Titre, 'Interpreter', 'latex')
                xPos = x(sub(1)) + (x(sub(end))-x(sub(1)))/ 10;
                yPos = max(C*(2/3));
                h = text(xPos, yPos, strModel);
                set(h, 'Interpreter', 'latex', 'fontsize', 12)
            end
            linkaxes(hc2,'x')
        end
        
        
        function crossCorrelation(~, ~, fig)
            [flag, hc] = FigUtils.selectCurves(fig, 2);
            if ~flag
                return
            end
            nc = length(hc);
            Fig = FigUtils.createSScFigure;
            k = 1;
            for k1=1:nc
                for k2=1:nc
                    GCAF1 = get(hc(k1), 'Parent');
                    GCAF2 = get(hc(k2), 'Parent');
                    Title1 = get(get(GCAF1, 'Title'), 'String');
                    Title2 = get(get(GCAF2, 'Title'), 'String');
                    XData1 = get(hc(k1), 'XData');
                    XData2 = get(hc(k2), 'XData');
                    YData1 = get(hc(k1), 'YData');
                    YData2 = get(hc(k2), 'YData');
                    XLim1  = get(GCAF1, 'XLim');
                    XLim2  = get(GCAF2, 'XLim');
                    
                    % Ajout JMA le 03/04/2019
                    if iscell(Title1)
                        Title1 = Title1{end};
                    end
                    if iscell(Title2)
                        Title2 = Title2{end};
                    end
                    
                    [sub1, sub2] = intersect3(XData1, XData2, XData2);
                    XData1 = XData1(sub1);
                    YData1 = YData1(sub1);
                    XData2 = XData2(sub2);
                    YData2 = YData2(sub2);
                    
                    sub = (XData1 >= XLim1(1)) & (XData1 <= XLim1(2)) & ~isnan(YData1) ...
                        & (XData2 >= XLim2(1)) & (XData2 <= XLim2(2)) & ~isnan(YData2);
                    YData1 = YData1(sub);
                    YData2 = YData2(sub);
                    XData1 = XData1(sub);
                    %                 XData2 = XData2(sub);
                    
                    M1 = mean(YData1);
                    M2 = mean(YData2);
                    Y1 = YData1 - M1;
                    Y2 = YData2 - M2;
                    
                    if k1 < k2
                        %                 covMx = xcorr(Y1, Y2, 'coeff');
                        resol = mean(diff(XData1));
                        [covMx, x, ixMaxC, yModele, strModel]  = my_xcorr(Y1, Y2, 'cl_time', 0, 'resol', resol); %#ok<ASGLU>
                        
                        % TODO : voir comment transf�rer les infos du model
                        figure(Fig)
                        subplot(nc,nc,k)
                        %                     n = floor(length(covMx) / 2);
                        %                     x = (-n:n) * (XData1(2)-XData1(1));
                        PlotUtils.createSScPlot(x, covMx); grid on; box on;
                        if ~isempty(Title1) && ~isempty(Title2)
                            title(['Cross corr - ' Title1 ' / ' Title2(1,:)], 'Interpreter', 'none')
                        end
                        
                        
                        % hold on; hm = PlotUtils.createSScPlot((-n:n), yModele, 'r');
                        % title(['Cross corr - ' nomSignal], 'Interpreter', 'none'); % title(strModel, 'Interpreter', 'latex')
                        % set(hm, 'Tag', 'Model')
                        % hold on; PlotUtils.createSScPlot([-ixMaxC -ixMaxC], [min(covMx) max(covMx)], 'k'); %title(['Shift : ' num2str(-ixMaxC)])
                        % Titre = sprintf('%s %s', nomSignal, strModel);
                        % title(Titre, 'Interpreter', 'latex')
                        % h = text(-length(sub)/5, 0.6, strModel);
                        % set(h, 'Interpreter', 'latex', 'fontsize', 12)
                        % h = text(-length(sub)/5, 0.1, ['Shift : ' num2str(-ixMaxC)]);
                        % set(h, 'Interpreter', 'latex', 'fontsize', 12)
                        
                        
                        
                        
                    elseif k1 == k2
                        subplot(nc,nc,k)
                        PlotUtils.createSScPlot(XData1, YData1, '-k'); grid on; box on;
                        title(Title1)
                    elseif k1 > k2
                        subplot(nc,nc,k)
                        PlotUtils.createSScPlot(YData1, YData2, '*'); grid on; box on;
                        title(['Plot cross - ' Title1(1,:) ' / ' Title2], 'Interpreter', 'none')
                        
                        % %% Comment� pour le moment car pas concluent
                        % flag_time = 0;
                        % modelCurveCorrelation(XData1, covMx, gca, flag_time)
                        % title([Title1 ' / ' Title2])
                        
                    end
                    
                    k = k + 1;
                end
            end
        end
        
        
        function envelope(~, ~, fig)
            [flag, hc] = FigUtils.selectCurves(fig, 1);
            if ~flag
                return
            end
            nc = length(hc);
            FigUtils.createSScFigure;
            for k=1:nc
                XData1 = get(hc(k), 'XData');
                YData1 = get(hc(k), 'YData');
                GCAF1 = get(hc(k), 'Parent');
                
                XLim1  = get(GCAF1, 'XLim');
                
                sub = (XData1 >= XLim1(1)) & (XData1 <= XLim1(2)) & ~isnan(YData1);
                XData1 = XData1(sub);
                YData1 = YData1(sub);
                
                nomSignal = get(get(GCAF1, 'Title'), 'String');
                
                % Ajout JMA le 03/04/2019
                if iscell(nomSignal)
                    nomSignal = nomSignal{end};
                end
                
                Y = hilbert(YData1);
                E = abs(Y);
                
                hc2(k) = subplot(nc,1,nc-k+1); %#ok<AGROW>
                PlotUtils.createSScPlot(XData1, YData1); grid on; box on;
                hold on; hm = PlotUtils.createSScPlot(XData1, E, 'r');
                %             title(['Envelope - ' nomSignal], 'Interpreter', 'none'); % title(strModel, 'Interpreter', 'latex')
                set(hm, 'Tag', 'Model')
                hm = PlotUtils.createSScPlot(XData1, -E, 'g');
                title(['Envelope - ' nomSignal], 'Interpreter', 'none'); % title(strModel, 'Interpreter', 'latex')
                set(hm, 'Tag', 'Model')
            end
            linkaxes(hc2, 'x')
        end
        
        
        function renameThisWindow(~, ~, fig)
            Name = fig.get('Name');
            %             str1 = 'Nouveau nom :';
            str2 = 'New name :';
            nomVar = {str2};
            value = {Name};
            [Name, flag] = my_inputdlg(nomVar, value);
            if ~flag
                return
            end
            fig.set('Name', Name{1});
        end
        
        
        function linkAxesAuto(hAxeIn, hAxeOut)
            flagLinkX = true;
            flagLinkY = true;
            XLim = get(hAxeIn(1), 'XLim');
            YLim = get(hAxeIn(1), 'YLim');
            for k=2:length(hAxeIn)
                if ~isequal(XLim, get(hAxeIn(k), 'XLim'))
                    flagLinkX = false;
                end
                if ~isequal(YLim, get(hAxeIn(k), 'YLim'))
                    flagLinkY = false;
                end
            end
            
            if flagLinkX
                linkaxes(hAxeOut, 'x');
            end
            if flagLinkY
                linkaxes(hAxeOut, 'y');
            end
        end
        
        
        function [flag, hc] = selectCurves(fig, nbMinSignals)
            % Avant
            %         hc = findobj(GCF, 'Type', 'Line');
            
            % TODO : Apparemment utile pour les trac�s qui pr�sentent des supperpositions de
            % signaux sur la zone courante. C'est �trange, l'op�ration semble
            % �quivalente � ce qu'il y avait avant. A voir sur cas concret
            hc1 = findobj(fig, 'Type', 'Line', 'Tag', 'Current Extent');
            hc2 = findobj(fig, 'Type', 'Line', '-not', 'Tag', 'Current Extent');
            hc  = [hc1; hc2];
            
            nc = length(hc);
            
            %             str1 = sprintf('Il faut au moins %d signaux pour faire cette op�ration.', nbMinSignals);
            str2 = sprintf('You need at least %d signals for this action.', nbMinSignals);
            if nc < nbMinSignals
                my_warndlg(str2, 1);
                flag = 0;
                return
            end
            
            for k=1:nc
                GCAF1 = get(hc(k), 'Parent');
                Titre = get(get(GCAF1, 'Title'), 'String');
                if isempty(Titre)
                    %                 subsub(k) = false; %#ok<AGROW>
                    subsub(k) = true; %#ok<AGROW>
                    Titre = sprintf('Curve %d', k);
                else
                    Titre = rmblank(Titre(1,:));
                    subsub(k) = true; %#ok<AGROW>
                end
                SignalName{k} = Titre; %#ok<AGROW>
            end
            SignalName = SignalName(subsub);
            hc = hc(subsub);
            nc = length(hc);
            %             str3 = 'Liste des signaux';
            str4 = 'Signals list';
            [sub, flag] = my_listdlgMultiple(str4, SignalName, 'InitialValue', 1:nc);
            if ~flag
                return
            end
            hc = hc(sub);
            nc = length(hc);
            if nc < nbMinSignals
                my_warndlg(str2, 1);
                flag = 0;
                return
            end
            flag = 1;
        end
        
        
        function setLineStyle(src, ~, fig)
            label = src.Label;
            lineList = findobj(fig, 'Type', 'Line');
            lineList.set('LineStyle', label);
        end
        
        
        function setLineWidth(src, ~, fig)
            label = src.Label;
            W = str2double(label);
            lineList = findobj(fig, 'Type', 'Line');
            lineList.set('LineWidth', W);
        end
        
        
        function setLineColor(src, event, fig)
            lineList =  findobj(fig, 'Type', 'Line');
            PlotUtils.setLineColor(src, event, lineList);
        end
        
        
        function setMarkerStyle(src, ~, fig)
            label = src.Label;
            lineList = findobj(fig, 'Type', 'Line');
            lineList.set('Marker', label);
        end
        
        
        function setMarkerWidth(src, ~, fig)
            label = src.Label;
            W = str2double(label);
            lineList = findobj(fig, 'Type', 'Line');
            lineList.set('MarkerSize', W);
        end
        
        
        function setMarkerColor(src, event, fig)
            lineList =  findobj(fig, 'Type', 'Line');
            PlotUtils.setMarkerColor(src, event, lineList);
        end
        
        
        function editproperties(~, ~, editPropertiesObject)
            editPropertiesObject.editProperties();
        end
    end
end
