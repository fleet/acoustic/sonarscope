classdef FilterDataEventData < event.EventData
    %FilterDataEventData Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        line % line to modify
        modifiedIndex % index to modifiy
        reset % boolean reset value or not
    end
    
    methods
        function this = FilterDataEventData(line, modifiedIndex, reset)
            this.line = line;
            this.modifiedIndex = modifiedIndex;
            this.reset = reset;
        end
    end
    
end

