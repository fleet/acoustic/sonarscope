classdef FilterDataMode < TmpLineUtils & matlab.uitools.internal.uimode
    % classdef FilterDataMode
    % FilterDataMode Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        % ui
        parentFigure
        filterParametreComponent
        filterTool
        jfilterToolManualItemList
        jfilterToolMenuSaveItem
        %         filterParametreDialog % parametre dialog for Butterworth algorithm cleaning
        
        % filter tool list
        manualToolNameList = {'Whole selected axes', 'Current zoom on selected axes', 'Rectangle', 'Polygon', 'FreeHand'};
        
        % data
        dirty = false;
        isStillInFilterMode    = false
        selectedFilterToolName = 'Whole selected axes'; % Whole Axes by default
        mouseButtondown        = false;
        controlKeyPressed      = false;
        
        % saved data to recompute filter if filter parametres change
        savedAxes
        savedPositionList
        savedResetClean
    end
    
    events
        FilterEvent
        PerformFilterEvent % send event when clean finish
        FilterTmpEvent     % send event after delete tmp line
    end
    
    methods
        function this = FilterDataMode(fig)
            % Superclass Constructor
            this = this@matlab.uitools.internal.uimode();
            createuimode(this,fig,'Exploration.Filter');
            
            this.parentFigure = fig;
            
            % create filter tool in figure tool bar
            this.createFilterTool();
            
            this.WindowButtonDownFcn   = @this.windowButtonDownFcnCallback;
            this.WindowButtonUpFcn     = @this.windowButtonUpFcnCallback;
            this.WindowButtonMotionFcn = @this.windowButtonMotionFcnCallback;
            this.WindowKeyPressFcn     = @this.windowKeyPressFcnCallback;
            this.WindowKeyReleaseFcn   = @this.windowKeyReleaseFcnCallback;
            this.WindowScrollWheelFcn  = @this.windowScrollWheelFcnCallback;
            % Start Mode Callback
            this.ModeStartFcn = @this.modeStartFcnCallback;
            % End Mode Callback
            this.ModeStopFcn  = @this.modeStopFcnCallback;
        end
        
        function createFilterTool(this)
            %% Filter Tool
            % find / toolbar
            hToolbar = findobj(allchild(this.parentFigure), 'Type', 'uitoolbar');
            if isempty(hToolbar)
                hToolbar = uitoolbar(fig);
            end
            
            % Load the clean icon
            icon = iconRead(getNomFicDatabase('FilterRegions_16.png'));
            
            % create the cleaner tool
            this.filterTool = uitogglesplittool('parent', hToolbar, 'cdata', icon, ...
                'tooltip', 'Filter data', ...
                'ClickedCallback', @this.filterToolToggleActionCallback);
        end
        
        function createFilterToolMenu(this)
            % Add JCheckBoxMenuItems to cleanerTool
            jfilterTool = get(this.filterTool, 'JavaContainer');
            % Try catch beacause  get(jcleanerTool, 'MenuComponent')
            % return null sometimes if no pause...
            try
                jfilterToolMenu = get(jfilterTool, 'MenuComponent');
            catch
                pause(0.5)
                jfilterToolMenu = get(jfilterTool, 'MenuComponent');
            end
            
            jradioGroup = javax.swing.ButtonGroup();
            
            %% Manual Cleaning
            this.jfilterToolManualItemList = javaArray('javax.swing.JRadioButtonMenuItem', numel(this.manualToolNameList));
            for k=1:numel(this.manualToolNameList)
                this.jfilterToolManualItemList(k) = javax.swing.JRadioButtonMenuItem(this.manualToolNameList(k));
                this.jfilterToolManualItemList(k).set('ActionPerformedCallback', @this.filterToolMenuItemCallback);
                
                jradioGroup.add(this.jfilterToolManualItemList(k));
                jfilterToolMenu.add(this.jfilterToolManualItemList(k));
            end
            
            %% Save button
            jfilterToolMenuSeparatorItem = javax.swing.JSeparator();
            jfilterToolMenu.add(jfilterToolMenuSeparatorItem);
            this.jfilterToolMenuSaveItem = javax.swing.JMenuItem('Save (Shift + S)');
            this.jfilterToolMenuSaveItem.setEnabled(false);
            this.jfilterToolMenuSaveItem.set('ActionPerformedCallback', @this.filterToolMenuItemCallback);
            jfilterToolMenu.add(this.jfilterToolMenuSaveItem);
            
            % select the selected tool
            this.jfilterToolManualItemList(1).setSelected(true);
        end

        function setFilterParametreComponent(this, filterParametreComponent)
            % Set the filterParametreComponent
            this.filterParametreComponent = filterParametreComponent;
            % add listerner
            addlistener(this.filterParametreComponent, 'OneParamValueChange', @this.handleFilterParametreValueChange);
        end
        
        
        %% Filter Data Actions
        function selectFilterData(this)
            currentAxes = this.parentFigure.get('CurrentAxes');
            
            % Check if clic on CurrentAxes
            if TmpLineUtils.checkIfMousePositionInAxes(currentAxes)
                
                resetClean = this.controlKeyPressed;
                
                % clean objet to modify
                this.savedAxes         = [];
                this.savedPositionList = [];
                this.savedResetClean   = [];
                
                %create tmpLineList if needed
                this.createTmpLineList(currentAxes);
                
                if strcmp(this.selectedFilterToolName, this.manualToolNameList{1}) % Axes
                    this.axesFilterData(currentAxes, resetClean);
                    % save objet to modify
                    this.savedAxes         = currentAxes;
                    this.savedPositionList = [];
                    this.savedResetClean   = resetClean;
                    
                    % Prepare an undo/redo action
                    cmd.Name = sprintf('axesCleanData');
                    cmd.Function        = @this.axesFilterData; % Redo action
                    cmd.Varargin        = {currentAxes, resetClean};
                    cmd.InverseFunction = @this.axesFilterData; % Undo action
                    cmd.InverseVarargin = {currentAxes, ~resetClean};
                    % Register the undo/redo action with the figure
                    uiundo(this.parentFigure, 'function', cmd);
                else
                    this.isStillInFilterMode = true;
                    % Remove dragndrop from normal mode because
                    % imrect/impoly/imfreehand set back to normal mode
                    % callbacks
                    modemanager= uigetmodemanager(this.parentFigure);
                    modemanager.set('CurrentMode', []);
                    FigUtils.removeDragDrop(this.parentFigure);
                    
                    % init selection variable
                    selection = [];
                    
                    switch this.selectedFilterToolName
                        case this.manualToolNameList{2} % 'Current zoom on axes'
                            % Check is numeric x axis or datetime
                            if isa(currentAxes.XLim(1), 'datetime') % convert in numeric
                                x1 = ruler2num( currentAxes.XLim(1), currentAxes.XAxis);
                                x2 = ruler2num( currentAxes.XLim(2), currentAxes.XAxis);
                            else
                                
                                x1 = currentAxes.XLim(1);
                                x2 = currentAxes.XLim(2);
                            end
                            selectionPositionList(1,:) = [x1 currentAxes.YLim(1)];
                            selectionPositionList(2,:) = [x1 currentAxes.YLim(2)];
                            selectionPositionList(3,:) = [x2 currentAxes.YLim(2)];
                            selectionPositionList(4,:) = [x2 currentAxes.YLim(1)];
                        case this.manualToolNameList{3} % 'Rectangle'
                            %                             selection = drawrectangle(currentAxes, 'InteractionsAllowed', 'none');
                            %                             position = selection.Position;
                            selection = imrect(currentAxes);
                            position = selection.getPosition();
                            % build the list of selected postions position = [posX, posY, widthX,widthY]
                            selectionPositionList(1,:) = [position(1) position(2)];
                            selectionPositionList(2,:) = [position(1) position(2)+position(4)];
                            selectionPositionList(3,:) = [position(1)+position(3) position(2)+position(4)];
                            selectionPositionList(4,:) = [position(1)+position(3) position(2)];
                        case this.manualToolNameList{4} % 'Polygon'
                            %                             selection = drawpolygon(currentAxes, 'InteractionsAllowed', 'none');
                            %                             selectionPositionList = selection.Position;
                            selection = impoly(currentAxes);
                            selectionPositionList = selection.getPosition();
                        case this.manualToolNameList{5} % 'FreeHand'
                            %                             selection =  drawfreehand(currentAxes, 'InteractionsAllowed', 'none');
                            %                             selectionPositionList = selection.Position;
                            selection = imfreehand(currentAxes);
                            selectionPositionList = selection.getPosition();
                    end
                    % re-set the mode because imrect/impoly/imfreehand set an other mode
                    modemanager.set('CurrentMode', this);
                    this.parentFigure.set('Pointer', 'arrow');
                    
                    % pause(0.1); % let time to close the selection before delete % Comment� par JMA le 07/12/2018 pour �viter bug lors d'un double-clik en mode rectangle
                    % delete polygon
                    if (~isempty(selection))
                        selection.delete();
                    end
                    % Filter Data
                    this.manuallyFilterDataAction(currentAxes, selectionPositionList, resetClean);
                    % save objet to modify
                    this.savedAxes         = currentAxes;
                    this.savedPositionList = selectionPositionList;
                    this.savedResetClean   = resetClean;
                    
                    % Prepare an undo/redo action
                    cmd.Name = sprintf('manuallyFilterDataAction');
                    cmd.Function        = @this.manuallyFilterDataAction; % Redo action
                    cmd.Varargin        = {currentAxes,selectionPositionList, resetClean};
                    cmd.InverseFunction = @this.manuallyFilterDataAction; % Undo action
                    cmd.InverseVarargin = {currentAxes, selectionPositionList, ~resetClean};
                    % Register the undo/redo action with the figure
                    uiundo(this.parentFigure, 'function', cmd);
                    
                    this.mouseButtondown     = false;
                    this.isStillInFilterMode = false;
                    this.controlKeyPressed   = false; % disable alternative mode
                end
            end
        end
        
        function axesFilterData(this, axes, resetClean)
            % Filter Data Action on the whole axes
            
            %init elements to clean
            lineListToClean = gobjects(0);
            tmpLineListToClean = gobjects(0);
            selectedIndexListToClean = {0};
            
            % Find line objects
            [originalLineList, tmpOriginalLineList] = TmpLineUtils.getLines(axes);
            
            if isvalid(tmpOriginalLineList)
                % Find line to clean
                cleanableLineList = this.getLinesToClean(axes);
                [~, cleanableLineIndex] = ismember(cleanableLineList, originalLineList);
                
                lineList = originalLineList(cleanableLineIndex);
                tmpLineList = tmpOriginalLineList(cleanableLineIndex);
                if ~resetClean
                    lineListToIterate = lineList;
                else
                    lineListToIterate = tmpLineList;
                end
                
                % init index for list of lineListToClean
                listIndex = 0;
                
                for k=1:numel(lineListToIterate)
                    yData = lineListToIterate(k).YData;
                    
                    % select all index of line
                    selectedIndex = true(1,numel(yData));
                    
                    if ~isempty(selectedIndex) && ismember(true, selectedIndex)
                        listIndex = listIndex + 1;
                        % add elements to clean in arrays
                        lineListToClean(listIndex) = lineList(k);
                        tmpLineListToClean(listIndex) =  tmpLineList(k);
                        selectedIndexListToClean{listIndex} = selectedIndex;
                    end
                end
            end
            
            % Call filter data action
            this.filterDataAction(lineListToClean, tmpLineListToClean, selectedIndexListToClean, resetClean);
        end
        
        function manuallyFilterDataAction(this, axes, positionList, resetClean)
            % Filter Data Action
            
            %init elements to clean
            lineListToClean = gobjects(0);
            tmpLineListToClean = gobjects(0);
            selectedIndexListToClean = {0};
            
            % Find line objects
            [originalLineList, tmpOriginalLineList] = TmpLineUtils.getLines(axes);
            
            if isvalid(tmpOriginalLineList)
                % Find line to clean
                cleanableLineList = this.getLinesToClean(axes);
                [~,cleanedLineIndex] = ismember(cleanableLineList, originalLineList);
                
                lineList = originalLineList(cleanedLineIndex);
                tmpLineList = tmpOriginalLineList(cleanedLineIndex);
                if resetClean
                    lineListToIterate = lineList;
                else
                    lineListToIterate = tmpLineList;
                end
                
                % init index for list of lineListToClean
                listIndex = 0;
                
                for k=1:numel(lineListToIterate)
                    xData = lineListToIterate(k).XData;
                    yData = lineListToIterate(k).YData;
                    
                    % compute the point in polygon
                    if isa(xData, 'datetime') % compute dX for dateTime dataX
                        x_limits = get(axes, 'XLim');
                        x0 = x_limits(1); %find the x0 of axes
                        
                        dX = xData - x0; % convert to duration
                        xDataNew = days(dX); % convert to double
                        
                        positionListX_DT = num2ruler(positionList(:,1),axes.XAxis); % convert to datetime
                        positionListX_Duration = positionListX_DT - x0; % convert to duration
                        positionListX_Double =  days(positionListX_Duration);  % convert to double
                        
                        [selectedIndex, ~] = inpolygon(xDataNew, yData, positionListX_Double, positionList(:,2));
                    else % compute dX for numeric dataX
                        [selectedIndex, ~] = inpolygon(xData, yData, positionList(:,1), positionList(:,2));
                    end
                    
                    if ~isempty(selectedIndex) && ismember(true, selectedIndex)
                        listIndex = listIndex + 1;
                        % add elements to clean in arrays
                        lineListToClean(listIndex) = lineList(k);
                        tmpLineListToClean(listIndex) =  tmpLineList(k);
                        selectedIndexListToClean{listIndex} = selectedIndex;
                    end
                end
            end
            
            % Call filter data action
            this.filterDataAction(lineListToClean, tmpLineListToClean, selectedIndexListToClean, resetClean);
        end
        
        function  filterDataAction(this, lineListToClean, tmpLineListToClean, selectedIndexListToClean, resetClean)
            
            
            this.filterDataActionWithEvent(lineListToClean, tmpLineListToClean, selectedIndexListToClean, resetClean);
            
            %% Prepare an undo/redo action
            cmd.Name = sprintf('filterDataAction');
            cmd.Function        = @this.cleanDataActionWithEvent; % Redo action
            cmd.Varargin        = {lineListToClean, tmpLineListToClean, selectedIndexListToClean, resetClean};
            cmd.InverseFunction = @this.cleanDataActionWithEvent; % Undo action
            cmd.InverseVarargin = {lineListToClean, tmpLineListToClean, selectedIndexListToClean, ~resetClean};
            % Register the undo/redo action with the figure
            uiundo(gcbf, 'function', cmd);
            
        end
        
        function hasFoundDataToFilter = filterDataActionWithEvent(this, lineListToClean, tmpLineListToClean, selectedIndexListToClean, resetClean)
            
            for lineIndex = 1:numel(lineListToClean)
                line = lineListToClean(lineIndex);
                tmpLine = tmpLineListToClean(lineIndex);
                selectedIndex = selectedIndexListToClean{lineIndex};
                
                if isvalid(line) && isvalid(tmpLine) % check if tmpLine (and line) exist
                    
                    this.filterDataUnitaryAction(line, tmpLine, selectedIndex, resetClean);
                    
                    % set filter mode dirty
                    this.dirty = true;
                    this.jfilterToolMenuSaveItem.setEnabled(true);
                    % filter event
                    filterDataEventData = FilterDataEventData(line, selectedIndex, resetClean);
                    notify(this, 'FilterEvent', filterDataEventData);
                    
                    hasFoundDataToFilter = true;
                end
            end
        end
        
        
        
        function filterDataUnitaryAction(this, line, tmpLine, selectedIndex, resetClean)
            % Clean Data Unitary Action
            if ~resetClean % normal mode
                
                x = tmpLine.XData;
                y = tmpLine.YData;
                % y interpolation if NaN or Inf
                flagNaN = isnan(y) |isinf(y);
                if any(flagNaN)
                    
                    %                         y(flagNaN) = my_interp1(x(~flagNaN), y(~flagNaN), x(flagNaN), 'linear', 'extrap');
                    % Attention, comment� et �a fonctionne en mode non interpol� : a� tester en
                    % mode interpol�
                    
                    y(flagNaN) = my_interp1(x(~flagNaN), y(~flagNaN), x(flagNaN), 'linear');
                end
                
                % FilterDataParametre
                filterDataParametre = this.filterParametreComponent.getFilterDataParametre();
                
                switch filterDataParametre.selectedFilterIndex
                    case 1 % Butterworth
                        wc = filterDataParametre.butterworthWcParam.Value;
                        ordre = filterDataParametre.butterworthOrderParam.Value;
                        
                        % Butterworth algo
                        if wc ~= 0
                            %%TODO : faut il-filter sur tous le signal ou
                            %%juste sur la selection?
                            
                            % Check if line isAngle
                            isAngle = getappdata(line, 'isAngle');
                            
                            
                            if (filterDataParametre.butterworthProgressive)
                                % butterworth progressif
                                yF = filtrageButterSignalProgressif(y(selectedIndex), wc, ordre, 'isAngle', isAngle);
                            else
                                  % butterworth simple
                                yF = filtrageButterSignal(y(selectedIndex), wc, ordre, 'isAngle', isAngle);
                            end

                            line.YData(selectedIndex) = yF;
                        else
                            % no filter
                            line.YData(selectedIndex) = tmpLine.YData(selectedIndex);
                        end
                        %TODO MHO: remplacer cet algo par celui deCLSignal
                        
                    case 2 % Gaussian
                        sigma = filterDataParametre.gaussianSigmaParam.Value;
                        
                        %{
                        width = max(3,2*ceil(sigma*3)+1);
                        h = fspecial('gaussian', [1,width], sigma);
                        %%TODO : faut il-filter sur tous le signal ou
                        %%juste sur la selection?
                        yF = filtfilt(h,1,double(y(selectedIndex))); % TODO check ifAngle
                        line.YData(selectedIndex) = yF;
                        %}

                        order = sigma;
                        if isa(x(selectedIndex), 'datetime')
                            X = datenum(x(selectedIndex));
                            X = X - X(1);
                        else
                            X = double(x(selectedIndex));
                        end

                        p = polyfit(X, double(y(selectedIndex)), order);
                        yF = polyval(p, X);
                        line.YData(selectedIndex) = yF;

                    case 3 % Polynome
                        order = filterDataParametre.polynomeOrderParam.Value;

                        if isa(x(selectedIndex), 'datetime')
                            X = datenum(x(selectedIndex));
                            X = X - X(1);
                        else
                            X = double(x(selectedIndex));
                        end

                        p = polyfit(X, double(y(selectedIndex)), order);
                        yF = polyval(p, X);
                        line.YData(selectedIndex) = yF;
                end
            else % resetClean mode
                line.YData(selectedIndex) =  tmpLine.YData(selectedIndex);
            end
        end
        
        
        %% Callback
        
        function filterToolMenuItemCallback(this, src, ~)
            % Filter Tool Menu Item Callback
            if src.Component == this.jfilterToolMenuSaveItem  % Save button
                % perform clean if needed
                if this.dirty
                    this.performClean();
                end
            else
                % find manual tool
                for k=1:numel(this.jfilterToolManualItemList)
                    if eq(src.Component, this.jfilterToolManualItemList(k))
                        this.selectedFilterToolName = this.manualToolNameList{k};
                    end
                end
            end
            
            % Show FilterParametreComponent
            this.showHideFilterParametreComponent(true);
            
            if strcmp(this.filterTool.get('State'), 'off')
                % filter tool ON
                this.filterTool.set('State', 'on');
                this.filterToolToggleActionCallback(this.filterTool);
            end
        end
        
        function filterToolToggleActionCallback(this, src, ~)
            % Filter Tool Toggle Button  Action Callback
            modemanager= uigetmodemanager(this.parentFigure);
            if strcmp(src.get('State'), 'on') % filter tool ON
                modemanager.set('CurrentMode', this);
            else
                modemanager.set('CurrentMode', []);
            end
        end
        
        function windowButtonDownFcnCallback(this, fig, ~) %#ok<INUSD>
            % Detect if axes 2d or 3d, clean allowed on 2d, not 3d
            [~, el] = view;
            if el == 90 % axes 2D X-Y view
                this.mouseButtondown = true;
                this.selectFilterData();
            end
        end
        
        function windowButtonUpFcnCallback(this, ~, ~)
            this.mouseButtondown = false;
        end
        
        function windowButtonMotionFcnCallback(this, ~, ~)
            if this.mouseButtondown == true
            end
        end
        
        function windowKeyPressFcnCallback(this, obj, event_obj)
            if ~isempty(event_obj.Modifier)
                if strcmp(event_obj.Key, 'control') % alternative mode
                    this.controlKeyPressed = true;
                elseif strcmp(event_obj.Modifier, 'shift') && strcmp(event_obj.Key, 's')
                    % perform clean if needed
                    if this.dirty
                        this.performClean();
                    end
                end   
            elseif strcmp(event_obj.Key, 'uparrow')... % pan mode
                    || strcmp(event_obj.Key, 'downarrow')...
                    || strcmp(event_obj.Key, 'leftarrow')...
                    || strcmp(event_obj.Key, 'rightarrow')
                panHandle = pan(this.parentFigure);
                panMode = panHandle.ModeHandle;
                panMode.KeyPressFcn(obj, event_obj);
            end
        end
        
        function windowKeyReleaseFcnCallback(this, ~, event_obj)
            if strcmp(event_obj.Key, 'control') % alternative mode
                this.controlKeyPressed = false;
            end
        end
        
        function windowScrollWheelFcnCallback(this, obj, event_obj)
            % Zoom in / Zomm out callback
            zoomHandle = zoom(this.parentFigure);
            zoomMode = zoomHandle.ModeHandle;
            zoomMode.WindowScrollWheelFcn(obj, event_obj);
        end
        
        
        function modeStartFcnCallback(this,~,~)
            this.filterTool.set('State', 'on');
            
            % Show FilterParametreComponent
            this.showHideFilterParametreComponent(true);
            
            % Ugly hack, set a WindowButtonMotionFcn to fig to avoid
            % problem with position of mouse with currentPoint. Set []
            % dont work
            this.parentFigure.set('WindowButtonMotionFcn', @emptyCallback);
            
            function emptyCallback(~,~)
            end
        end
        
        function modeStopFcnCallback(this, obj, evnt) %#ok<INUSD>
            if ~this.isStillInFilterMode % Check if we are still in Filter Mode
                this.filterTool.set('State', 'off');
                this.parentFigure.set('Pointer', 'arrow');
                
                % perform clean if needed
                if this.dirty
                    this.performClean();
                end
                
                % Hide FilterParametreComponent
                this.showHideFilterParametreComponent(false);
                
                % clean objet to modify
                this.savedAxes = [];
                this.savedPositionList = [];
                this.savedResetClean = [];
            end
        end
        
        function performClean(this)
            % send Perform clean event
            notify(this, 'PerformFilterEvent');
            
            % delete parentAxesList reference and delete TmpLines
            this.cleanTmp();
            % send Perform clean event
            notify(this, 'FilterTmpEvent');
            
            this.dirty = false;
            this.jfilterToolMenuSaveItem.setEnabled(false);
        end
        
        
        function showHideFilterParametreComponent(this, visible)
            % Show / Hide FilterParametreComponent
            if visible
                this.filterParametreComponent.Visible = 'on';
                size = 42;
            else
                this.filterParametreComponent.Visible = 'off';
                size = 0;
            end
            sizes = this.filterParametreComponent.parent.get('Sizes');
            sizes(1) = size;
            this.filterParametreComponent.parent.set('Sizes', sizes);
            
        end
        
        function handleFilterParametreValueChange(this, ~, ~)
            % event on OneParamValueChange from filterParameteComponent
            if ~isempty(this.savedAxes) && ~isempty(this.savedResetClean)
                if (~isempty(this.savedPositionList))
                    this.manuallyFilterDataAction(this.savedAxes, this.savedPositionList, this.savedResetClean);
                else
                    this.axesFilterData(this.savedAxes, this.savedResetClean);
                end
            end
        end
    end
end
