classdef PlotUtils
    % Class PlotUtils provides static methods creating SSc Plot
    %
    % Examples
    %   % Creates a SSc plot with ssc context menu_
    %   FigUtils.createSScFigure;
    %   sscPlot = PlotUtils.createSScPlot(sin(-2*pi:0.01:2*pi), 'isLightContextMenu', true);
    %
    %   % Create a matlab plot and add a ssc contextmenu to the plot
    %   FigUtils.createSScFigure;
    %   p = plot(sin(-2*pi:0.01:2*pi));
    %   PlotUtils.addContextMenu(p);
    %
    %   % Create matlab plots
    %   FigUtils.createSScFigure;
    %   subplot(2,1,1); PlotUtils.createSScPlot(rand(100,1)); grid on; title('First')
    %   subplot(2,1,2); PlotUtils.createSScPlot(rand(100,1)); grid on; title('Second')
    %
    % See also FigUtils.createSScFigure
    % ----------------------------------------------------------------------------
    
    properties
    end
    
    methods (Static, Access = public)
        
        function lineList = createSScPlot(varargin)
            %% SSc Plot
            [varargin, isLightContextMenu] = getPropertyValue(varargin, 'isLightContextMenu', false);
            
            %% Plot line (can be a list of Line)
            lineList = plot(varargin{:});
            if isempty(lineList)
                return
            end
            
            if isa(varargin{1},'matlab.graphics.axis.Axes') ||isa(varargin{1},'matlab.ui.control.UIAxes')
                axis(varargin{1}, 'tight');
            end
            
            % Add DragnDrop on figure
            FigUtils.addDragDrop(ancestor(lineList(1), 'figure'));
            
            % add ContextMenu to each line
            for k=1:numel(lineList)
                PlotUtils.addContextMenu(lineList(k), 'isLightContextMenu', isLightContextMenu);
            end
        end
        
        
        function line = createSScPlot3(varargin)
            %% SSc Plot3
            [varargin, isLightContextMenu] = getPropertyValue(varargin, 'isLightContextMenu', false);
            
            %% Trac� de la courbe
            line = plot3(varargin{:});
            if isempty(line)
                return
            end
            
            if isa(varargin{1},'matlab.graphics.axis.Axes') ||isa(varargin{1},'matlab.ui.control.UIAxes')
                axis(varargin{1}, 'tight');
            end
            
            % Add DragnDrop on figure
            FigUtils.addDragDrop(ancestor(line(1),'figure'));
            
            % add ContextMenu to line
            PlotUtils.addContextMenu(line(1), 'isLightContextMenu', isLightContextMenu);
        end
        
        
        function addContextMenu(line, varargin)
            
            [~, isLightContextMenu] = getPropertyValue(varargin, 'isLightContextMenu', false);
            
            % Create the context menu
            cmenu = uicontextmenu(ancestor(line, 'figure'), 'Tag', 'addContextMenu');
            % Add context menu to input line
            set(line, 'UIContextMenu', cmenu);
            
            %% color/forme/etc
            
            % Line
            h1 = uimenu(cmenu, 'Text', 'Line');
            uimenu(h1, 'Text', 'Style ...', 'Callback', {@PlotUtils.setLineStyle, line});
            uimenu(h1, 'Text', 'Width ...', 'Callback', {@PlotUtils.setLineWidth, line});
            uimenu(h1, 'Text', 'Color ...', 'Callback', {@PlotUtils.setLineColor, line});
            % Marker
            h1 = uimenu(cmenu, 'Text', 'Marker');
            uimenu(h1, 'Text', 'Style ...', 'Callback', {@PlotUtils.setMarkerStyle, line});
            uimenu(h1, 'Text', 'Width ...', 'Callback', {@PlotUtils.setMarkerWidth, line});
            uimenu(h1, 'Text', 'Color ...', 'Callback', {@PlotUtils.setMarkerColor, line});
            % Color
            uimenu(cmenu, 'Text', 'Color ...','Callback', {@PlotUtils.setLineAndMarkerColor, line});
            
            if ~isLightContextMenu
                
                %% gestion I/o
                uimenu(cmenu, 'Text', 'Copy in a new figure', 'Separator' ,'on', 'Callback', {@PlotUtils.cloneCourbe, line});
                
                uimenu(cmenu, 'Text', 'Delete', 'Callback', 'delete(gco)'); % Modif JMA le 23/04/2019
                
                h1 = uimenu(cmenu, 'Text', 'Export');
                uimenu(cmenu, 'Parent', h1, 'Text', 'ASCII',  'Callback', {@PlotUtils.exportCourbeAscii,  line});
                uimenu(cmenu, 'Parent', h1, 'Text', 'Matlab', 'Callback', {@PlotUtils.exportCourbeMatlab, line});
                
                h1 = uimenu(cmenu, 'Text', 'Import');
                uimenu(cmenu, 'Parent', h1, 'Text', 'ASCII',  'Callback', {@PlotUtils.importCourbeAscii,  line});
                uimenu(cmenu, 'Parent', h1, 'Text', 'Matlab', 'Callback', {@PlotUtils.importCourbeMatlab, line});
                
                %% Operation associ�es � une seule courbe
                uimenu(cmenu, 'Text', 'Self-Correlation', 'Callback', {@PlotUtils.selfCorrelation, line}, 'Separator' ,'on');
                uimenu(cmenu, 'Text', 'Derivative',       'Callback', {@PlotUtils.derivateCourbe,  line});
                
                uimenu(cmenu, 'Text', 'Filter',        'Callback', {@PlotUtils.filterCourbe,   line});
                uimenu(cmenu, 'Text', 'Curve fitting', 'Callback', {@PlotUtils.courbeFit,      line});
                uimenu(cmenu, 'Text', 'Envelope',      'Callback', {@PlotUtils.envelopeCourbe, line});
                
                h1 = uimenu(cmenu, 'Text', 'Center of Gravity');
                uimenu(h1, 'Text', 'On Amplitude',  'Callback', {@PlotUtils.centerGravityCourbe,  line});
                uimenu(h1, 'Text', 'On Energy',     'Callback', {@PlotUtils.centerGravityCourbe2, line});
                
                h1 = uimenu(cmenu, 'Text', 'Arithmetic');
                uimenu(h1, 'Text', 'Add an offset in Y',         'Callback', {@PlotUtils.addOffsetY,    line});
                uimenu(h1, 'Text', 'Add an offset in X',         'Callback', {@PlotUtils.addOffsetX,    line});
                uimenu(h1, 'Text', 'Multiply value by a scalar', 'Callback', {@PlotUtils.multiplyValue, line});
                uimenu(h1, 'Text', 'Abs',                        'Callback', {@PlotUtils.absValue,      line});
                
                uimenu(cmenu, 'Text', 'Statistics of values', 'Callback', {@PlotUtils.statisticsOfValues, line});
                uimenu(cmenu, 'Text', 'reflec_Enr2dB',        'Callback', {@PlotUtils.reflec_dBOfValues,  line});
                uimenu(cmenu, 'Text', 'reflec_dB2Enr',        'Callback', {@PlotUtils.reflec_NatOfValues, line});
                
                %% Operation intercourbes sur un m�me axe
                
                uimenu(cmenu, 'Text', 'Curve math ...', 'Separator' ,'on', 'Callback', {@PlotUtils.courbeMath,          line});
                uimenu(cmenu, 'Text', 'Spike filter',   'Separator' ,'on', 'Callback', {@PlotUtils.callbackSpikeFilter, line});
            end
        end
        
        
        %% Get the title of axes of line object
        function axesTitle = getAxesTitle(line)
            currentAxes = get(line, 'parent');
            axesTitle = get(get(currentAxes, 'Title'), 'String');
            if isempty(axesTitle)
                axesTitle = ' ';
            end
        end
        
        
    end
    
    methods (Static, Access = private)
        %% Callbacks
        
        function cloneCourbe(~, ~, line, varargin)
            
            [varargin, Fig]       = getPropertyValue(varargin, 'Fig',     []); %#ok<ASGLU>
            %             [varargin, flag_time] = getPropertyValue(varargin, 'cl_time', 0);
            %             [varargin, noZData]   = getFlag(varargin, 'noZData'); %#ok<ASGLU>
            
            hAxeIn = get(line, 'parent');
            Titre = get(get(hAxeIn, 'Title'), 'String');
            XLim = get(hAxeIn, 'XLim');
            
            XData = get(line, 'XData');
            YData = get(line, 'YData');
            ZData = get(line, 'ZData');
            
            sub = (XData >= XLim(1)) & (XData <= XLim(2));
            XData = XData(sub);
            YData = YData(sub);
            
            if ~isempty(ZData)
                ZData = ZData(sub);
            end
            
            switch get(line, 'Type')
                case 'patch'
                    isPatch = true;
                    Proprietes = {'EdgeColor'; 'FaceColor'; 'LineStyle'; 'LineWidth'; 'Marker'; 'MarkerSize'; 'ButtonDownFcn'};
                otherwise
                    isPatch = false;
                    Proprietes = {'Color'; 'LineStyle'; 'LineWidth'; 'Marker'; 'MarkerSize'; 'ButtonDownFcn'};
            end
            
            
            if isempty(Fig)
                figure;
            else
                figure(Fig);
                hold(hAxeIn, 'on');
            end
            if isPatch
                h = my_patch(XData, YData, [1 1 1]);
            else
                %                 if isempty(ZData) || noZData
                %                     h = PlotUtils.createSScPlot(XData, YData, 'cl_time', flag_time);
                %                 else
                if isempty(ZData)
                    h = PlotUtils.createSScPlot(XData, YData);
                else
                    h = PlotUtils.createSScPlot3(XData, YData, ZData);
                end
            end
            
            grid(hAxeIn, on);
            title(Titre, 'Interpreter', 'none')
            set(gca, 'XLim', XLim)
            
            for i=1:length(Proprietes)
                P = get(line, Proprietes{i});
                set(h, Proprietes{i}, P)
            end
        end
        
        function exportCourbeAscii(~, ~, line)
            axesTitle = PlotUtils.getAxesTitle(line);
            if iscell(axesTitle)
                axesTitle = axesTitle{1};
            end
            if ~isempty(axesTitle)
                axesTitle = rmblank(axesTitle(1,:));
            end
            
            XData    = get(line, 'XData');
            YData    = get(line, 'YData');
            ZData    = get(line, 'ZData');
            UserData = get(line, 'UserData');
            
            [flag, nomFic] = my_uiputfile('*.txt', 'Give a file name', fullfile(my_tempdir, 'Curve.txt'));
            if ~flag
                return
            end
            
            fid = fopen(nomFic,'w+');
            if fid == -1
                messageErreurFichier(nomFic, 'WriteFailure');
                return
            end
            
            XFormat = create_fprintf_Format(XData);
            if isempty(ZData) && ~isa(UserData, 'cl_time')
                if isempty(UserData)
                    fprintf(fid, 'x\ty\t%s\n', axesTitle);
                    Format = [XFormat '\t%f\n'];
                    for k=1:length(XData)
                        fprintf(fid, Format, XData(k), YData(k));
                    end
                else
                    Format = [XFormat '\t%f\t%s\t%s\n'];
                    fprintf(fid, '%s value \tXCoordinates \tYCoordinates\n', axesTitle);
                    n = length( XData);
                    XCoordinates = interp1([1 n], UserData.X, 1:n);
                    YCoordinates = interp1([1 n], UserData.Y, 1:n);
                    for k=1:length(XData)
                        fprintf(fid, Format, XData(k), YData(k), num2strPrecis(XCoordinates(k)), num2strPrecis(YCoordinates(k)));
                    end
                end
                
            elseif isempty(ZData) && isa(UserData, 'cl_time')
                % on exporte le temps �galement
                date = datestr(UserData.timeMat, 'dd/mm/yyyy');
                heure = datestr(UserData.timeMat, 'HH:MM:SS.FFF');
                Format = '%s\t%s\t%s\t%f\n';
                fprintf(fid, 'Date \tHeure \t%s value \tFlag ()\n', axesTitle);
                for k=1:length(XData)
                    fprintf(fid, Format, date(k,:), heure(k,:), num2strPrecis(YData(k)), 1);
                end
            else
                Format = [XFormat '\t%f\t%f\n'];
                fprintf(fid, 'x\ty\t%s\n', axesTitle);
                for k=1:length(XData)
                    fprintf(fid, Format, num2strPrecis(XData(k)), YData(k), ZData(k));
                end
            end
            fclose(fid);
        end
        
        
        function exportCourbeMatlab(~, ~, line)
            hAxeIn = get(gco, 'parent');
            Titre = get(get(hAxeIn, 'Title'), 'String');
            XData = get(line, 'XData');
            YData = get(line, 'YData');
            ZData = get(line, 'ZData');
            
            [flag, nomFic] = my_uiputfile('*.mat', 'Give a file name', fullfile(my_tempdir, 'Curve.mat'));
            if ~flag
                return
            end
            if isempty(ZData)
                save(nomFic, 'Titre', 'XData', 'YData')
            else
                save(nomFic, 'Titre', 'XData', 'YData', 'ZData')
            end
        end
        
        
        function importCourbeAscii(~, ~, line)
            XData = get(line, 'XData');
            YData = get(line, 'YData');
            
            liste = listeFicOnDir(my_tempdir, '.txt');
            if length(liste) == 1
                Filtre = fullfile(my_tempdir, liste{1});
            else
                Filtre = my_tempdir;
            end
            [flag, nomFic] = my_uigetfile('*.txt', 'Give a file name', Filtre);
            if ~flag
                return
            end
            
            fid = fopen(nomFic, 'r');
            if fid == -1
                messageErreurFichier(nomFic, 'ReadFailure');
                return
            end
            %         Titre = fgetl(fid);
            XDataNew = [];
            YDataNew = [];
            ZDataNew = [];
            while 1
                tline = fgetl(fid);
                if ~ischar(tline)
                    break
                end
                disp(tline)
                mots = strsplit(tline);
                nbMots = length(mots);
                if nbMots < 2
                    %                     str1 = 'Le fichier ne contient pas X et Y';
                    str2 = 'The file does not contain X and Y values.';
                    my_warndlg(str2, 1);
                    fclose(fid);
                    return
                end
                XDataNew(end+1) = str2double(mots{1}); %#ok<AGROW>
                YDataNew(end+1) = str2double(mots{2}); %#ok<AGROW>
                if nbMots == 3
                    ZDataNew(end+1) = str2double(mots{3}); %#ok<AGROW>
                end
            end
            fclose(fid);
            
            if (length(XDataNew) == length(XData)) && (length(YDataNew) == length(YData))
                set(line, 'XData',  XDataNew);
                set(line, 'YData', -YDataNew);
                if ~isempty(ZDataNew)
                    set(line, 'ZData', ZDataNew);
                end
                currentAxes = get(line, 'Parent');
                axis(currentAxes, 'tight')
            else
                %                 str1 = 'Le nombre de points de la courbe import�e ne correspond pas � celle qui est en place.';
                str2 = 'The number of points in the imported curve does not correspond to the one which is IN place.';
                my_warndlg(str2, 1);
            end
        end
        
        
        function importCourbeMatlab(~, ~, line)
            XData = get(line, 'XData');
            YData = get(line, 'YData');
            ZData = get(line, 'ZData');
            
            liste = listeFicOnDir(my_tempdir, '.mat');
            if length(liste) == 1
                Filtre = fullfile(my_tempdir, liste{1});
            else
                Filtre = my_tempdir;
            end
            [flag, nomFic] = my_uigetfile('*.mat', 'Give a file name', Filtre);
            if ~flag
                return
            end
            
            if isempty(ZData)
                XDataNew = loadmat(nomFic, 'nomVar', 'XData');
                YDataNew = loadmat(nomFic, 'nomVar', 'YData');
                ZDataNew = [];
            else
                XDataNew = loadmat(nomFic, 'nomVar', 'XData');
                YDataNew = loadmat(nomFic, 'nomVar', 'YData');
                ZDataNew = loadmat(nomFic, 'nomVar', 'ZData');
            end
            
            if (length(XDataNew) == length(XData)) && (length(YDataNew) == length(YData))
                set(line, 'XData', XDataNew);
                set(line, 'YData', YDataNew);
                if ~isempty(ZDataNew)
                    set(line, 'ZData', ZDataNew);
                end
                currentAxes = get(line, 'Parent');
                axis(currentAxes, 'tight')
            else
                %                 str1 = 'Le nombre de points de la courbe import�e ne correspond pas � celle qui est en place.';
                str2 = 'The number of points oh the imported curve does not correspond to the one which is place.';
                my_warndlg(str2, 1);
            end
        end
        
        
        %% line options
        function setLineAndMarkerColor(~, ~, line)
            color = uisetcolor;
            if length(color) == 3
                line.set('Color', color)
                if isAGraphicElement(line, 'axes')
                    return
                end
                line.set('MarkerEdgeColor', color)
            end
        end
        
        
        function setLineStyle(~, ~, line)
            itemList = {'-', '--', '.', '-.', 'none'};
            [selectedIndex, ok] = my_listdlg('Style', itemList, 'SelectionMode', 'Single');
            if ok
                line.set('LineStyle', itemList{selectedIndex});
            end
        end
        
        
        function setLineWidth(~, ~, line)
            itemList = {'0.5', '1', '2', '3', '4', '6', '8', '10', '15', '20', '25', '30'};
            [selectedIndex, ok] = my_listdlg('Style', itemList, 'SelectionMode', 'Single');
            if ok
                line.set('LineWidth', str2double(itemList{selectedIndex}));
            end
        end
        
        
        function setLineColor(~, ~, line)
            color = uisetcolor;
            if length(color) == 3
                line.set('Color', color)
            end
        end
        
        
        function setMarkerStyle(~, ~, line)
            itemList = {'+', 'o', '*', '.', 'x', 'square', 'v', '^', 'pentagram', 'hexagram', 'none'};
            [selectedIndex, ok] = my_listdlg('Style', itemList, 'SelectionMode', 'Single');
            if ok
                line.set('Marker', itemList{selectedIndex});
            end
        end
        
        
        function setMarkerWidth(~, ~, line)
            itemList = {'0.5', '1', '2', '3', '4', '6', '8', '10', '15', '20', '25', '30'};
            [selectedIndex, ok] = my_listdlg('Style', itemList, 'SelectionMode', 'Single');
            if ok
                line.set('MarkerSize', str2double(itemList{selectedIndex}));
            end
        end
        
        
        function setMarkerColor(~, ~, line)
            selectedColor = uisetcolor;
            if length(selectedColor) == 3
                line.set('MarkerEdgeColor', selectedColor)
            end
        end
        
        
        %%
        
        function selfCorrelation(~, ~, line)
            XData = get(line, 'XData');
            YData = get(line, 'YData');
            currentAxes = get(line, 'parent');
            axesTitle = PlotUtils.getAxesTitle(line);
            if iscell(axesTitle)
                axesTitle = axesTitle{1};
            end
            XLim = get(currentAxes, 'XLim');
            sub = find((XData >= XLim(1)) & (XData <= XLim(2)));
            traiter_signal(XData(sub), YData(sub), axesTitle, 'Process', 5);
        end
        
        
        function derivateCourbe(~, ~, line)
            axesTitle = PlotUtils.getAxesTitle(line);
            
            XData = get(line, 'XData');
            YData = get(line, 'YData');
            ZData = get(line, 'ZData');
            
            FigUtils.createSScFigure();
            h = PlotUtils.createSScPlot(XData, gradient(YData) ./ gradient(XData), 'isLightContextMenu', true);
            if ~isempty(ZData)
                set(h, 'ZData', ZData)
            end
            grid on; box on;
            str(1,:) = ['Derivative of ' axesTitle(1,:)];
            if size(axesTitle,1) > 1
                str(2,1:size(axesTitle,2)) = axesTitle(2,:);
            end
            title(str, 'Interpreter', 'none')
            
            Proprietes = {'Color'; 'LineStyle'; 'LineWidth'; 'Marker'; 'MarkerSize'
                'ButtonDownFcn'};
            
            for k=1:length(Proprietes)
                P = get(line, Proprietes{k});
                set(h, Proprietes{k}, P)
            end
        end
        
        
        function filterCourbe(~, ~, line)
            itemList = {'Freq 1/5', 'Freq 1/10', 'Freq 1/20', 'Freq 1/50'};
            [selectedIndex, ok] = my_listdlg('Filter', itemList, 'SelectionMode', 'Single');
            if ~ok
                return
            end
            
            Label = itemList{selectedIndex};
            Freq = eval(Label(6:end));
            
            axesTitle = PlotUtils.getAxesTitle(line);
            
            XData = get(line, 'XData');
            YData = get(line, 'YData');
            ZData = get(line, 'ZData');
            
            % TODO : faire un IHM permettant de trouver la bonne fr�quence et
            % de rendre la maoin lorsque l'on valide la fr�quence
            
            %         hFilter = fdesign.lowpass('N,F3dB', 12, Freq);
            %         d1 = design(hFilter, 'butter');
            %         Y = filtfilt(d1.sosMatrix,d1.ScaleValues, YData);
            
            [B,A] = butter(2, Freq);
            Y = YData;
            subNonNaN = ~isnan(Y);
            Y(subNonNaN) = filtfilt(B, A, double(YData(subNonNaN)));
            
            FigUtils.createSScFigure();
            h = PlotUtils.createSScPlot(XData, YData, 'isLightContextMenu', true);
            if ~isempty(ZData)
                set(h, 'ZData', ZData)
            end
            
            hold on; PlotUtils.createSScPlot(XData, Y, 'r', 'isLightContextMenu', true);
            grid on; box on;
            str(1,:) = ['Filter of ' axesTitle(1,:)];
            if size(axesTitle,1) > 1
                Titre = axesTitle(2,:);
                str(2,1:length(Titre)) = Titre;
            end
            title(str, 'Interpreter', 'none')
            
            Proprietes = {'Color'; 'LineStyle'; 'LineWidth'; 'Marker'; 'MarkerSize'
                'ButtonDownFcn'};
            
            for k=1:length(Proprietes)
                P = get(line, Proprietes{k});
                set(h, Proprietes{k}, P)
            end
        end
        
        
        function courbeFit(~, ~, line)
            itemList = {'1', '2', '3', '4', '5'};
            [selectedIndex, ok] = my_listdlg('Polynom order', itemList, 'SelectionMode', 'Single');
            if ok
                Label = itemList{selectedIndex};
                ordre = str2double(Label);
                
                currentAxes = get(line, 'parent');
                axesTitle = PlotUtils.getAxesTitle(line);
                
                XData = get(line, 'XData');
                YData = get(line, 'YData');
                ZData = get(line, 'ZData');
                XLim = get(currentAxes, 'XLim');
                YLim = get(currentAxes, 'YLim');
                
                FigUtils.createSScFigure();
                
                sub = ~isnan(YData) & (XData >= XLim(1)) & (XData <= XLim(2)) & (YData >= YLim(1)) & (YData <= YLim(2));
                XData = XData(sub);
                YData = YData(sub);
                YDataOld = YData;
                p = polyfit(double(XData), double(YData), ordre);
                YDataFitted = polyval(p, XData);
                YData = YData - YDataFitted;
                
                hAxe(1) = subplot(2,1,1);
                h = PlotUtils.createSScPlot(XData, YDataOld, 'isLightContextMenu', true); hold on;
                if ~isempty(ZData)
                    set(h, 'ZData', ZData)
                end
                PlotUtils.createSScPlot(XData, YDataFitted, 'r', 'isLightContextMenu', true);
                
                % Titre = sprintf('Curve minus polyfit (%s) of %s', num2str(p), Titre);
                str = sprintf('Curve & Fitting\nPolynome=[%s]', num2str(p));
                title(str, 'Interpreter', 'none')
                grid on; box on;
                
                
                hAxe(2) = subplot(2,1,2);
                h = PlotUtils.createSScPlot(XData, YData, 'isLightContextMenu', true);
                if ~isempty(ZData)
                    set(h, 'ZData', ZData)
                end
                if isempty(axesTitle)
                    axesTitle = sprintf('Curve minus Fitting ');
                else
                    if iscell(axesTitle)
                        axesTitle = sprintf('Curve minus Fitting : %s', axesTitle{1});
                    else
                        axesTitle = sprintf('Curve minus Fitting : %s', axesTitle(1,:));
                    end
                end
                title(axesTitle, 'Interpreter', 'none')
                grid on; box on;
                Proprietes = {'Color'; 'LineStyle'; 'LineWidth'; 'Marker'; 'MarkerSize'
                    'ButtonDownFcn'};
                
                for k=1:length(Proprietes)
                    P = get(line, Proprietes{k});
                    set(h, Proprietes{k}, P)
                end
                
                linkaxes(hAxe, 'x')
            end
        end
        
        
        function envelopeCourbe(~, ~, line)
            itemList = {'Direct', 'Ordre 0', 'Ordre 1', 'Ordre 2', 'Ordre 3', 'Ordre 4', 'Ordre 5', 'Ordre 6', 'Ordre 7', 'Ordre 8'};
            [selectedIndex, ok] = my_listdlg('Filter', itemList, 'SelectionMode', 'Single');
            if ok
                if selectedIndex == 1
                    axesTitle = PlotUtils.getAxesTitle(line);
                    
                    XData = get(line, 'XData');
                    YData = get(line, 'YData');
                    ZData = get(line, 'ZData');
                    
                    Y = hilbert(YData);
                    E = abs(Y);
                    
                    FigUtils.createSScFigure();
                    
                    h = PlotUtils.createSScPlot(XData, YData, 'isLightContextMenu', true);
                    if ~isempty(ZData)
                        set(h, 'ZData', ZData)
                    end
                    hold on; PlotUtils.createSScPlot(XData, E, 'r', 'isLightContextMenu', true); PlotUtils.createSScPlot(XData, -E, 'g', 'isLightContextMenu', true);
                    grid on; box on;
                    str(1,:) = ['Envelope of ' axesTitle(1,:)];
                    if size(axesTitle,1) > 1
                        str(2,1:size(axesTitle,2)) = axesTitle(2,:);
                    end
                    title(str, 'Interpreter', 'none')
                    
                    Proprietes = {'Color'; 'LineStyle'; 'LineWidth'; 'Marker'; 'MarkerSize'
                        'ButtonDownFcn'};
                    
                    for k=1:length(Proprietes)
                        P = get(line, Proprietes{k});
                        set(h, Proprietes{k}, P)
                    end
                else
                    Label = itemList{selectedIndex};
                    ordre = str2double(Label(7:end));
                    
                    currentAxes = get(line, 'parent');
                    axesTitle = PlotUtils.getAxesTitle(line);
                    
                    XData = get(line, 'XData');
                    YData = get(line, 'YData');
                    ZData = get(line, 'ZData');
                    XLim = get(currentAxes, 'XLim');
                    YLim = get(currentAxes, 'YLim');
                    
                    FigUtils.createSScFigure();
                    
                    sub = ~isnan(YData) & (XData >= XLim(1)) & (XData <= XLim(2)) & (YData >= YLim(1)) & (YData <= YLim(2));
                    XData = XData(sub);
                    YData = YData(sub);
                    YDataOld = YData;
                    p = polyfit(double(XData), double(YData), ordre);
                    YDataFitted = polyval(p, XData);
                    YData = YData - YDataFitted;
                    
                    Y = hilbert(YData);
                    E = abs(Y);
                    
                    hAxe(1) = subplot(2,1,1);
                    h = PlotUtils.createSScPlot(XData, YDataOld, 'isLightContextMenu', true); hold on;
                    if ~isempty(ZData)
                        set(h, 'ZData', ZData)
                    end
                    PlotUtils.createSScPlot(XData, YDataFitted, 'r', 'isLightContextMenu', true);
                    hold on; plot(XData, YDataFitted+E, 'r'); plot(XData, YDataFitted-E, 'g')
                    
                    % Titre = sprintf('Curve minus polyfit (%s) of %s', num2str(p), Titre);
                    str = sprintf('Curve & Fitting\nPolynome=[%s]', num2str(p));
                    title(str, 'Interpreter', 'none')
                    grid on; box on;
                    
                    
                    hAxe(2) = subplot(2,1,2);
                    h = PlotUtils.createSScPlot(XData, YData, 'isLightContextMenu', true);
                    if ~isempty(ZData)
                        set(h, 'ZData', ZData)
                    end
                    hold on; plot(XData, E, 'r'); plot(XData, -E, 'g')
                    
                    axesTitle = sprintf('Curve minus Fitting : %s', axesTitle(1,:));
                    title(axesTitle, 'Interpreter', 'none')
                    grid on; box on;
                    Proprietes = {'Color'; 'LineStyle'; 'LineWidth'; 'Marker'; 'MarkerSize'; 'ButtonDownFcn'};
                    
                    for k=1:length(Proprietes)
                        P = get(line, Proprietes{k});
                        set(h, Proprietes{k}, P)
                    end
                    
                    linkaxes(hAxe, 'x')
                end
            end
        end
        
        
        function centerGravityCourbe(~, ~, line)
            XData = get(line, 'XData');
            YData = get(line, 'YData');
            ZData = get(line, 'ZData');
            
            currentAxes = get(line, 'parent');
            XLim = get(currentAxes, 'XLim');
            YLim = get(currentAxes, 'YLim');
            
            FigUtils.createSScFigure();
            
            sub = ~isnan(YData) & (XData >= XLim(1)) & (XData <= XLim(2)) & (YData >= YLim(1)) & (YData <= YLim(2));
            XData = XData(sub);
            YData = YData(sub);
            
            [Ex, Sigma] = CenterGravity(XData, YData);
            
            h = PlotUtils.createSScPlot(XData, YData, 'isLightContextMenu', true); grid on; box on; hold on;
            if ~isempty(ZData)
                set(h, 'ZData', ZData)
            end
            h = plot([Ex Ex], [min(YData) max(YData)], 'r');
            set(h, 'LineWidth', 3)
            h = plot([Ex-Sigma Ex+Sigma], [min(YData) min(YData)], 'r');
            set(h, 'LineWidth', 3)
            hold off;
            
            % Titre = sprintf('Curve minus polyfit (%s) of %s', num2str(p), Titre);
            str = sprintf('Center of Gravity : C=%f  Sigma = %f', Ex, Sigma);
            title(str, 'Interpreter', 'none')
            grid on; box on;
        end
        
        
        function centerGravityCourbe2(~, ~, line)
            XData = get(line, 'XData');
            YData = get(line, 'YData');
            ZData = get(line, 'ZData');
            
            currentAxes = get(line, 'parent');
            XLim = get(currentAxes, 'XLim');
            YLim = get(currentAxes, 'YLim');
            
            FigUtils.createSScFigure();
            
            sub = ~isnan(YData) & (XData >= XLim(1)) & (XData <= XLim(2)) & (YData >= YLim(1)) & (YData <= YLim(2));
            XData = XData(sub);
            YData = YData(sub);
            
            [Ex, Sigma] = calcul_barycentre(XData, YData.^2);
            
            h = PlotUtils.createSScPlot(XData, YData, 'isLightContextMenu', true); grid on; box on; hold on;
            if ~isempty(ZData)
                set(h, 'ZData', ZData)
            end
            h = plot([Ex Ex], [min(YData) max(YData)], 'r');
            set(h, 'LineWidth', 3)
            h = plot([Ex-Sigma Ex+Sigma], [min(YData) min(YData)], 'r');
            set(h, 'LineWidth', 3)
            hold off;
            
            % Titre = sprintf('Curve minus polyfit (%s) of %s', num2str(p), Titre);
            str = sprintf('Center of Gravity : C=%f  Sigma = %f', Ex, Sigma);
            title(str, 'Interpreter', 'none')
            grid on; box on;
        end
        
        
        function addOffsetX(~, ~, line)
            XData = get(line, 'XData');
            
            offsetParam = ClParametre( 'value', 0);
            % Create parameterDialog
            dlg = SimpleParametreDialog( 'Title', 'Offset', 'params', offsetParam, 'sizes', [0 -1 0 0]);
            dlg.openDialog();
            if   dlg.okPressedOut
                Offset = offsetParam.Value;
                
                set(line, 'XData', XData + Offset)
                
                currentAxes = get(line, 'parent');
                hCurves = findobj(currentAxes, 'Type', 'line');
                XLim = [Inf -Inf];
                YLim = [Inf -Inf];
                for k=1:length(hCurves)
                    XData = get(hCurves(k), 'XData');
                    YData = get(hCurves(k), 'YData');
                    XLim(1) = min(XLim(1), min(XData));
                    XLim(2) = max(XLim(2), max(XData));
                    YLim(1) = min(YLim(1), min(YData));
                    YLim(2) = max(YLim(2), max(YData));
                end
                set(currentAxes, 'XLim', XLim);
                set(currentAxes, 'YLim', YLim);
            end
        end
        
        
        function addOffsetY(~, ~, line)
            YData = get(line, 'YData');
            
            offsetParam = ClParametre( 'value', 0);
            % Create parameterDialog
            dlg = SimpleParametreDialog( 'Title', 'Offset', 'params', offsetParam, 'sizes', [0 -1 0 0]);
            dlg.openDialog();
            if   dlg.okPressedOut
                Offset = offsetParam.Value;
                
                set(line, 'YData', YData + Offset)
                
                currentAxes = get(line, 'parent');
                hCurves = findobj(currentAxes, 'Type', 'line');
                XLim = [Inf -Inf];
                YLim = [Inf -Inf];
                for k=1:length(hCurves)
                    XData = get(hCurves(k), 'XData');
                    YData = get(hCurves(k), 'YData');
                    XLim(1) = min(XLim(1), min(XData));
                    XLim(2) = max(XLim(2), max(XData));
                    YLim(1) = min(YLim(1), min(YData));
                    YLim(2) = max(YLim(2), max(YData));
                end
                set(currentAxes, 'XLim', XLim);
                set(currentAxes, 'YLim', YLim);
            end
        end
        
        
        function multiplyValue(~, ~, line)
            YData = get(line, 'YData');
            
            coeffParam = ClParametre( 'value', 1);
            % Create parameterDialog
            dlg = SimpleParametreDialog( 'Title', 'Multiplicator', 'params', coeffParam, 'sizes', [0 -1 0 0]);
            dlg.openDialog();
            if   dlg.okPressedOut
                Coeff = coeffParam.Value;
                
                set(line, 'YData', YData * Coeff)
                
                currentAxes = get(line, 'parent');
                hCurves = findobj(currentAxes, 'Type', 'line');
                XLim = [Inf -Inf];
                YLim = [Inf -Inf];
                for k=1:length(hCurves)
                    XData = get(hCurves(k), 'XData');
                    YData = get(hCurves(k), 'YData');
                    if (k == 1) && isa(XData, 'datetime')
                        dInf2 = datetime(Inf,  'ConvertFrom', 'datenum');
                        dInf1 = datetime(-Inf, 'ConvertFrom', 'datenum');
                        XLim = [dInf2 dInf1];
                    end
                    XLim(1) = min(XLim(1), min(XData));
                    XLim(2) = max(XLim(2), max(XData));
                    YLim(1) = min(YLim(1), min(YData));
                    YLim(2) = max(YLim(2), max(YData));
                end
                set(currentAxes, 'XLim', XLim);
                set(currentAxes, 'YLim', YLim);
            end
        end
        
        
        function absValue(~, ~, line)
            YData = get(line, 'YData');
            
            set(line, 'YData', abs(YData))
            
            currentAxes = get(line, 'parent');
            hCurves = findobj(currentAxes, 'Type', 'line');
            XLim = [Inf -Inf];
            YLim = [Inf -Inf];
            for k=1:length(hCurves)
                XData = get(hCurves(k), 'XData');
                YData = get(hCurves(k), 'YData');
                XLim(1) = min(XLim(1), min(XData));
                XLim(2) = max(XLim(2), max(XData));
                YLim(1) = min(YLim(1), min(YData));
                YLim(2) = max(YLim(2), max(YData));
            end
            set(currentAxes, 'XLim', XLim);
            set(currentAxes, 'YLim', YLim);
        end
        
        
        function statisticsOfValues(~, ~, line)
            YData = get(line, 'YData');
            XData = get(line, 'XData');
            
            currentAxes = get(line, 'parent');
            XLim = get(currentAxes, 'XLim');
            sub = (XData >= XLim(1)) & (XData <= XLim(2));
            histo1D(YData(sub));
        end
        
        
        function reflec_dBOfValues(~, ~, line)
            YData = get(line, 'YData');
            
            currentAxes = get(line, 'parent');
            hYLabel = get(currentAxes, 'YLabel');
            YLabel = get(hYLabel, 'String');
            switch YLabel
                case 'Amp'
                    TypeSignal = 1;
                case 'Enr'
                    TypeSignal = 2;
                otherwise
                    %                     str1 = 'Cette op�ration ne devrait en principe n''�tre ex�cut�e que si YLabel  = ''Amp'' ou ''Enr'', voulez-vous forcer le calcul';
                    str2 = 'This conversion should be done only if YLAbel  = ''Amp'' or ''Enr'', that is not the case, do you want to proceed anyway ?';
                    [rep, flag] = my_questdlg(str2, 'Init', 2);
                    if ~flag || (rep == 2)
                        return
                    end
                    %                     str1 = 'Unit� de la courbe ?';
                    str2 = 'Status of the current curve ?';
                    [TypeSignal, flag] = my_questdlg(str2, 'Amp', 'Enr');
                    if ~flag
                        return
                    end
            end
            
            if TypeSignal == 1 % Amp
                set(line, 'YData', 2 * reflec_Enr2dB(YData))
            else % Enr
                set(line, 'YData', reflec_Enr2dB(YData))
            end
            
            hCurves = findobj(currentAxes, 'Type', 'line');
            XLim = [Inf -Inf];
            YLim = [Inf -Inf];
            for k=1:length(hCurves)
                XData = get(hCurves(k), 'XData');
                YData = get(hCurves(k), 'YData');
                XLim(1) = min(XLim(1), min(XData));
                XLim(2) = max(XLim(2), max(XData));
                YLim(1) = min(YLim(1), min(YData));
                YLim(2) = max(YLim(2), max(YData));
            end
            set(currentAxes, 'XLim', XLim);
            set(currentAxes, 'YLim', YLim);
            set(hYLabel, 'String', 'dB');
        end
        
        
        function reflec_NatOfValues(~, ~, line)
            YData = get(line, 'YData');
            
            currentAxes = get(line, 'parent');
            hYLabel = get(currentAxes, 'YLabel');
            YLabel = get(hYLabel, 'String');
            switch YLabel
                case 'dB'
                otherwise
                    %                     str1 = 'Cette op�ration ne devrait en principe n''�tre ex�cut�e que si YLabel  = ''Amp'' ou ''Enr'', voulez-vous forcer le calcul';
                    str2 = 'This conversion should be done only if YLAbel  = ''Amp'' or ''Enr'', that is not the case, do you want to proceed anyway ?';
                    [rep, flag] = my_questdlg(str2, 'Init', 2);
                    if ~flag || (rep == 2)
                        return
                    end
            end
            %             str1 = 'Type de conversione ?';
            str2 = 'Convert in ?';
            [TypeSignal, flag] = my_questdlg(str2, 'Amp', 'Enr');
            if ~flag
                return
            end
            
            if TypeSignal == 1 % Amp
                set(line, 'YData', reflec_dB2Enr(YData / 2));
                set(hYLabel, 'String', 'Amp');
            else % Enr
                set(line, 'YData', reflec_dB2Enr(YData));
                set(hYLabel, 'String', 'Enr');
            end
            
            hCurves = findobj(currentAxes, 'Type', 'line');
            XLim = [Inf -Inf];
            YLim = [Inf -Inf];
            for k=1:length(hCurves)
                XData = get(hCurves(k), 'XData');
                YData = get(hCurves(k), 'YData');
                XLim(1) = min(XLim(1), min(XData));
                XLim(2) = max(XLim(2), max(XData));
                YLim(1) = min(YLim(1), min(YData));
                YLim(2) = max(YLim(2), max(YData));
            end
            set(currentAxes, 'XLim', XLim);
            set(currentAxes, 'YLim', YLim);
        end
        
        
        function courbeMath(~, ~, line)
            % Soustraction de plusieurs courbes entre elles
            
            itemList = {'-', '+', '/', '*', 'xcorr'};
            [selectedIndex, ok] = my_listdlg('Filter', itemList, 'SelectionMode', 'Single');
            if ok
                Label = itemList{selectedIndex};
                
                currentAxes = get(line, 'parent');
                XData       = get(line, 'XData');
                YData       = get(line, 'YData');
                
                h = findobj(currentAxes, 'Type', 'Line','-and', 'selected', 'on');
                if length(h) < 2
                    % str1 = 'Vous devez s�l�ctionner au moins 2 courbes (ctrl + click) sur le m�me graphe';
                    str2 = 'You must select at least 2 curves (ctrl + click) on the SAME figure';
                    my_warndlg(str2,  1);
                    return
                else
                    ko = 0;
                    strCurve = sprintf('\\bf\\color[rgb]{%f %f %f}courbe_%d ', get(line,'color'), 1);
                    for k=1:length(h)
                        if h(k) == line
                            continue
                        end
                        XData2 = get(h(k), 'XData');
                        if min(XData) >= min(XData2) && min(XData) <= max(XData2) ||...
                                max(XData) >= min(XData2) && max(XData) <= max(XData2) ||...
                                min(XData) <= min(XData2) && max(XData) >= max(XData2)
                            YData2Sub = interp1(XData2, get(h(k), 'YData'), XData);
                            
                            switch Label
                                case '+'
                                    YData = YData + YData2Sub;
                                    opSymbol = '+';
                                case '-'
                                    YData = YData - YData2Sub;
                                    opSymbol = '-';
                                case '/'
                                    YData = YData ./ YData2Sub;
                                    opSymbol = '\div';
                                case '*'
                                    YData = YData .* YData2Sub;
                                    opSymbol = '\times';
                                case 'xcorr'
                                    resol = mean(diff(XData));
                                    flag_time = isa(XData, 'cl_time');
                                    [YData, XData] = my_xcorr(YData, YData2Sub, 'resol', resol, 'cl_time', flag_time);
                                    opSymbol = 'xcorr';
                            end
                            
                            strCurve = [strCurve, ...
                                ['\color{black}', opSymbol, ' '] ...
                                sprintf('\\color[rgb]{%f %f %f}courbe_%d ', get(h(k),'color'), k)]; %#ok<AGROW>
                        else
                            % pas d''intersection commune
                            ko = ko+1;
                        end
                    end
                end
                
                if ko < length(h)-1
                    FigUtils.createSScFigure();
                    PlotUtils.createSScPlot(XData, YData, 'isLightContextMenu', true);
                    str = sprintf('Curve Math :\n%s', strCurve);
                    title(str, 'Interpreter', 'tex')
                    grid on; box on;
                else
                    %                     str1 = 'Aucune intersection entre toutes les courbes';
                    str2 = 'No intersection between curves';
                    my_warndlg(str2, 1);
                end
            end
        end
        
        
        function callbackSpikeFilter(~, ~, line)
            currentAxes = get(line, 'Parent');
            XData = get(line, 'XData');
            YData = get(line, 'YData');
            axesTitle = PlotUtils.getAxesTitle(line);
            if iscell(axesTitle)
                axesTitle = axesTitle{1};
            end
            XLim = get(currentAxes, 'XLim');
            sub = find((XData >= XLim(1)) & (XData <= XLim(2)));
            YData(sub) = traiter_signal(XData(sub), YData(sub), axesTitle, 'Process', 14);
            set(line, 'YData', YData)
            
            hCurves = findobj(currentAxes, 'Type', 'line');
            XLim = [Inf -Inf];
            YLim = [Inf -Inf];
            for k=1:length(hCurves)
                XData = get(hCurves(k), 'XData');
                YData = get(hCurves(k), 'YData');
                XLim(1) = min(XLim(1), min(XData));
                XLim(2) = max(XLim(2), max(XData));
                YLim(1) = min(YLim(1), min(YData));
                YLim(2) = max(YLim(2), max(YData));
            end
            set(currentAxes, 'XLim', XLim);
            set(currentAxes, 'YLim', YLim);
        end
    end
end
