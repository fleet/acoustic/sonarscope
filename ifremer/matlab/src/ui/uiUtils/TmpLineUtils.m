classdef (Abstract) TmpLineUtils < handle
    % TmpLineUtils
    %   Abstract class who provide methode to create, get and delete
    %   tmpLine
    
    properties (Constant)
        toNotCleanLineTagLabel = 'toNotCleanLineTagLabel';
        toNotCleanUnitaryActionLineTagLabel = 'toNotCleanUnitaryActionLineTagLabel';
    end
    
    properties
        parentAxesList % parentAxes
        tmpLineListOfList % list of list of tmpLine indexed by parentAxes
    end
    
    methods (Static)
        
        function [lineList, tmpLineList] = getLines(axes)
            % Find line objects
            lineListReverted = findobj(axes, 'Type', 'Line', '-and', '-not', 'Tag', 'tmpLine');
            % revert ordre because wrong ordre by default
            lineList = flipud(lineListReverted);
            tmpLineList = findobj(axes, 'Type', 'Line', '-and', 'Tag', 'tmpLine');
        end
        
        function cleanTmpLineList(parentAxes)
            
            % Find tmp line objects
            [~, tmpLineList] = TmpLineUtils.getLines(parentAxes);
            
            delete(tmpLineList);
            
        end
        
        function checkIfMousePositionInAxes = checkIfMousePositionInAxes(axes)
            % Check if mouse position is in limits of  axes
            
            mousePosition  = get(axes, 'CurrentPoint');
            mousePositionX = mousePosition(1,1);
            mousePositionY = mousePosition(1,2);
            % Check is numeric x axis or datetime
            if isa(axes.XLim(1), 'datetime') % convert in numeric
                xLim(1) = ruler2num( axes.XLim(1), axes.XAxis);
                xLim(2) = ruler2num( axes.XLim(2), axes.XAxis);
            else
                xLim(1) = axes.XLim(1);
                xLim(2) = axes.XLim(2);
            end
            yLim(1) = axes.YLim(1);
            yLim(2) = axes.YLim(2);
            
            
            % Check if mouse position is in limits
            [checkIfMousePositionInAxes, ~] = inpolygon(mousePositionX, mousePositionY, xLim, yLim);
        end
        
    end
    
    methods (Access = public)
        
        function createTmpLineList(this, parentAxes)
            % Create temporary lines if needed : duplicate original lines to new grey
            % lines
            
            % Find tmp line objects
            [lineList, tmpLineList] = TmpLineUtils.getLines(parentAxes);
            
            %if dont exist, create them
            if isempty(tmpLineList) && ~isempty(lineList)
                % for each, duplicate in grey with Tag = tmpLine
                for  k=1:numel(lineList)
                    if isempty(lineList(k).ZData)
                        tmpLineList(k) = line(lineList(k).XData, lineList(k).YData, 'Parent', parentAxes, 'Tag', 'tmpLine');
                    else
                        tmpLineList(k) = line(lineList(k).XData, lineList(k).YData, lineList(k).ZData, 'Parent', parentAxes, 'Tag', 'tmpLine');
                    end
                    tmpLineList(k).set('Color',     [0.8 0.8 0.8]);
                    tmpLineList(k).set('LineWidth', lineList(k).get('LineWidth'));
                    tmpLineList(k).set('LineStyle', lineList(k).get('LineStyle'));
                    tmpLineList(k).set('Marker',    lineList(k).get('Marker'));
                    tmpLineList(k).set('Visible',   lineList(k).get('Visible'));
                    
                end
                % save parentAxes and tmpLineList
                this.parentAxesList = [this.parentAxesList parentAxes];
                [~,index] = ismember(parentAxes, this.parentAxesList);
                this.tmpLineListOfList{index} = tmpLineList;
                
                % Set tmpLineList array behind other children
                originalChildrenList =  parentAxes.get('Children');
                parentAxes.set('Children', vertcat(originalChildrenList, tmpLineList'));
            end
        end
        
    end
    
    
    methods (Access = protected)
        
        function [lineListToClean] = getLinesToClean(~, axes)
            % Find line objects to clean
            lineListToCleanReverted  = findobj(axes, 'Type', 'Line', '-and', '-not', 'Tag', 'tmpLine', '-and', '-not', 'Tag', TmpLineUtils.toNotCleanLineTagLabel);
            % revert ordre because wrong ordre by default
            lineListToClean = flipud(lineListToCleanReverted);
        end
        
        function OkForCleaningUnitaryAction = checkIfLineOkForCleaningUnitaryAction(~, line)
            if (strcmp(line.Tag, TmpLineUtils.toNotCleanUnitaryActionLineTagLabel))
                OkForCleaningUnitaryAction = false;
            else
                OkForCleaningUnitaryAction = true;
            end
            
        end
        
        function cleanTmp(this)
            % delete parentAxesList reference and delete TmpLines
            this.parentAxesList = [];
            for k=1:numel(this.tmpLineListOfList)
                delete(this.tmpLineListOfList{k});
            end
            
            %             % send Perform clean event
            %             notify(this, 'CleanTmpEvent');
        end
        
    end
    
end

