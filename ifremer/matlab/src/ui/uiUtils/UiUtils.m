classdef UiUtils
    % Description
    %   User Interfaces collection
    
    properties
    end
    
    methods (Static)
        
        function helpBox = createHelpButtonBox(parent, helpHtml)
            % Create Help Button Box
            
            helpBox = uiextras.HBox('Parent', parent);
            uiextras.Empty('Parent', helpBox);
            
            % Create Help Button
            UiUtils.createHelpButton(helpBox, helpHtml);
            
            helpBox.set('Sizes', [-1 25]);
        end
        
        function helpButton = createHelpButton( parent, helpHtml)
            % Create Help Button
            icon = iconizeFic(getNomFicDatabase('Apps-Help-Info-icon.png'));
            helpButton = uicontrol('Parent', parent, ...
                'Style',   'pushbutton', ...
                'CData',    icon, ...
                'Callback', {@UiUtils.helpAction, helpHtml});
        end
        
        function helpButton = createHelpUiButton(parent, helpHtml)
            % Create Help Button
            icon = iconizeFic(getNomFicDatabase('Apps-Help-Info-icon.png'));
            helpButton = uibutton(parent, 'push', ...
                'Text',    '', ...
                'Icon',    icon, ...
                'ButtonPushedFcn', {@UiUtils.helpAction, helpHtml});
        end
        
        function helpAction(~, ~, helpHtml)
            % Callback help Button
            % Open the web browser on the URL defined in "Help"
            if ~isempty(helpHtml) && ischar(helpHtml)
                my_web(helpHtml);
            end
        end
        
        
        function setUiDefaultColor(color)
            set(0,'DefaultFigureColor',color);
            set(0,'DefaultUiControlBackgroundColor',color);
            set(0,'DefaultUibuttongroupBackgroundColor',color);
        end
        
        function [flag, FreqStart, FreqEnd, Duration] = CreateDialog_ChirpParametres(varargin)
            % A User Interface to get the parametres of a chirp signal
            %
            % Syntax
            %   [flag, FreqStart, FreqEnd, Duration] = CreateDialog_ChirpParametres(...)
            %
            % Name-Value Pair Arguments
            %   FreqStart : Starting frequency of the chirp (Hz) (default 1800)
            %   FreqEnd   : Ending frequency of the chirp (Hz) (default 5300)
            %   Duration  : Duration of the chirp (ms) (default 80)
            %
            % output Arguments
            %   flag      : 1=OK, 0=KO
            %   FreqStart : Starting frequency of the chirp (Hz)
            %   FreqEnd   : Ending frequency of the chirp (Hz)
            %   Duration  : Duration of the chirp (ms)
            %
            % Examples
            %   [flag, FreqStart, FreqEnd, Duration] =
            %   UiUtils.CreateDialog_ChirpParametres('FreqStart', 1800, 'FreqEnd', 5300, 'Duration', 80)
            %
            % Authors  : JMA
            % See Also : sbp_processing
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc UiUtils/CreateDialog_ChirpParametres">UiUtils/CreateDialog_ChirpParametres</a>
            
            [varargin, FreqStart] = getPropertyValue(varargin, 'FreqStart', 1800);
            [varargin, FreqEnd]   = getPropertyValue(varargin, 'FreqEnd',   5300);
            [varargin, Duration]  = getPropertyValue(varargin, 'Duration',  80); %#ok<ASGLU>
            
            %% User interface of the parameters
            
            p(1) = ClParametre('Name', 'Starting Frequency', 'Unit', 'Hz', 'MinValue',   10, 'MaxValue', 10000, 'Value', FreqStart);
            p(2) = ClParametre('Name', 'Ending Frequency',   'Unit', 'Hz', 'MinValue', 1000, 'MaxValue', 20000, 'Value', FreqEnd);
            p(3) = ClParametre('Name', 'Duration',           'Unit', 'ms', 'MinValue',   10, 'MaxValue', 10000, 'Value', Duration);
            a = StyledParametreDialog('params', p, 'Title', 'Chirp parametres');
            sz = a.sizes;
            sz(1) = 0;
            a.sizes = sz;
            a.openDialog;
            if a.okPressedOut
                FreqStart = a.params(1).Value;
                FreqEnd   = a.params(2).Value;
                Duration  = a.params(3).Value;
                flag = 1;
            else
                flag = 0;
            end
        end
        
        function CreateDialog_SunshadingAlg(FileName, varargin)
            % A User Interface to display ErViewer images with a sunshading algorithm
            %
            % Syntax
            %   CreateDialog_SunshadingAlg(FileName, ...)
            %
            % Input Arguments
            %   FileName :  Name of an ErMapper algorithm (.alg or .ers)
            %
            % Name-Value Pair Arguments
            %   FileName  : Name of an ErViever algorithm file (.alg)
            %   Elevation : Elevation in deg
            %   Azimuth   : Azimuth in deg
            %   Unit      : Unit of the data
            %   CLim      : Contrast limits
            %
            % More About : This method just displays a .ers file applying
            % sunshading parameters. This is a the ErMapper for the poor
            %
            % Examples
            %   FileName = getNomFicDatabase('Earth_LatLong_Bathymetry.ers');
            %   UiUtils.CreateDialog_SunshadingAlg(FileName, 'Unit', 'm', 'CLim', [-6000 6000])
            %   [nomDir, nomFic] = fileparts(FileName);
            %   FileNameAlgo = fullfile(nomDir, nomFic, [nomFic '_shading.alg'])
            %   UiUtils.CreateDialog_SunshadingAlg(FileNameAlgo, 'Unit', 'm', 'CLim', [-6000 6000])
            %
            % Authors  : JMA
            % See Also : play_shading_alg
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc UiUtils/CreateDialog_SunshadingAlg">UiUtils/CreateDialog_SunshadingAlg</a>
            
            [varargin, Elevation] = getPropertyValue(varargin, 'Elevation', 60);
            [varargin, Azimuth]   = getPropertyValue(varargin, 'Azimuth',   220);
            [varargin, Unit]      = getPropertyValue(varargin, 'Unit',      []);
            [varargin, CLim]      = getPropertyValue(varargin, 'CLim',      []); %#ok<ASGLU>
            
            if ~exist(FileName, 'file')
                return
            end
            
            %% Get the parameters from the existing algorithm name
            
            nomFicXml = my_tempname('.xml');
            flag = export_xml(cl_ermapper_ers([]), FileName, 'nomFicXml', nomFicXml);
            if ~flag
                return
            end
            Alg = xml_read(nomFicXml);
            delete(nomFicXml)
            
            if isfield(Alg, 'Surface')
                Min = Alg.Surface.Stream(1).Transform.Histogram.MinimumValue;
                Max = Alg.Surface.Stream(1).Transform.Histogram.MaximumValue;
                if isempty(CLim)
                    CLim(1) = max(Min, Alg.Surface.Stream(1).Transform.MinimumInputValue);
                    CLim(2) = min(Max, Alg.Surface.Stream(1).Transform.MaximumInputValue);
                end
            else
                Min = CLim(1);
                Max = CLim(2);
            end
            
            %% User interface of the parameters
            
%             str1 = 'Param�tres � transmettre � ErViewer pour faire l''ombrage.';
            str2 = 'Parametres to transmit to ErViewer for the sunshading';
            style = ColorAndFontStyle('BackgroundColor', [0.7 0.7 1], 'ForegroundColor', 'k', 'FontWeight', 'bold', 'FontSize', 13, 'FontAngle', 'italic'); %option
            p    = ClParametre('Name', 'Max value', 'Unit', Unit,  'MinValue', Min, 'MaxValue', Max, 'Value', CLim(2));
            p(2) = ClParametre('Name', 'Min value', 'Unit', Unit,  'MinValue', Min, 'MaxValue', Max, 'Value', CLim(1));
            p(3) = ClParametre('Name', 'Azimuth',   'Unit', 'deg', 'MinValue', 0,   'MaxValue', 360, 'Value', Azimuth);
            p(4) = ClParametre('Name', 'Elevation', 'Unit', 'deg', 'MinValue', 0,   'MaxValue',  90, 'Value', Elevation);
            a = ParametreDialog('params', p, 'Title', str2, 'TitleStyle', style);
            a.openDialog;
            
            %% Launch the display if the user has validated the parameters
            
            if ~isempty(FileName) && a.okPressedOut
                CLim = sort([a.params(1:2).Value]);
                play_shading_alg(FileName, 'Azimuth', a.params(3).Value, 'Elevation', a.params(4).Value, 'CLim', CLim);
            end
        end
    end
end
