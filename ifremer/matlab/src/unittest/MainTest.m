classdef MainTest < matlab.unittest.TestCase
    % Description
    %   MainTest Unit Test
    %
    % Laucnh test :
    %   res = run(MainTest)
    %
    % Output Arguments
    %   res : atlab.unittest.TestResult instance
    %
    % Examples
    %   res = run(MainTest);
    %   rt = table(res)
    %
    % Authors  : MHO
    % See also ClSignal DialogsTest
    % ----------------------------------------------------------------------------
    
    properties
    end
    
    methods (Test)
        function testScienceTest(this)
            res = run(ScienceTest);
            isAllPassed = all([res.Passed]);
            this.verifyTrue(isAllPassed, 'ScienceTest Faile');
        end
        
        function testUiTest(this)
            res = run(UiTest);
            isAllPassed = all([res.Passed]);
            this.verifyTrue(isAllPassed, 'UiTest Faile');
        end
        
        function testSScUtilsTest(this)
            res = run(SScUtilsTest);
            isAllPassed = all([res.Passed]);
            this.verifyTrue(isAllPassed, 'SScUtilsTest Faile');
        end
        
        function testSScTools(this)
            res = run(SScToolsTest);
            isAllPassed = all([res.Passed]);
            this.verifyTrue(isAllPassed, 'SScTools Faile');
        end
    end
end
