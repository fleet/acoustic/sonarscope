classdef ClParametreTest < matlab.unittest.TestCase
    % Description
    %   ClParametreTest Unit Test for ClParametre
    %
    % Laucnh test :
    %   res = run(ClParametreTest)
    %
    % Output Arguments
    %   res : atlab.unittest.TestResult instance
    %
    % Examples
    %   res = run(ClParametreTest);
    %   rt = table(res)
    %
    % Authors  : MHO
    % See also ClParametre DialogsTest
    % ----------------------------------------------------------------------------
    
    properties
        parametre
    end
    
    methods(TestMethodSetup)
        function createParametre(this)
            this.parametre = ClParametre('Name', 'Temperature', 'Unit', 'deg', 'MinValue', -10, 'MaxValue', 50, 'Value', 20, 'Mute', true);
        end
    end
    
    methods(TestMethodTeardown)
        function closeParametre(this) %#ok<MANU>
            % nothing to do here...
        end
    end
    
    methods (Test)
        function testGetters(this)
            % Test des getters de ClParametre
            
            % Name
            nameExpected = 'Temperature';
            nameObj = this.parametre.Name;
            this.verifyEqual(nameExpected,nameObj, sprintf('Name expected : %s, but was %s',nameExpected, nameObj));
            
            % Unit
            unitExpected = 'deg';
            unitObj = this.parametre.Unit;
            this.verifyEqual(unitExpected,unitObj, sprintf('unite expected : %s, but was %s',unitExpected, unitObj));
            
            % MinValue
            minValueExpected = -10;
            minValueObj = this.parametre.MinValue;
            this.verifyEqual(minValueExpected,minValueObj, sprintf('minvalue expected : %f, but was %f',minValueExpected, minValueObj));
            
            % MaxValue
            maxValueExpected = 50;
            maxValueObj = this.parametre.MaxValue;
            this.verifyEqual(maxValueExpected,maxValueObj, sprintf('maxvalue expected : %f, but was %f',maxValueExpected, maxValueObj));
            
            % Value
            valueExpected = 20;
            valueObj = this.parametre.Value;
            this.verifyEqual(valueExpected,valueObj, sprintf('value expected : %f, but was %f',valueExpected, valueObj));  
        end
        
        function testSetters(testCase)
            % Test des setters de ClParametre
            
            % Value
            valueExpected = 25;
            testCase.parametre.Value = valueExpected;
            valueObj = testCase.parametre.Value;
            testCase.verifyEqual(valueExpected,valueObj, sprintf('value expected : %f, but was %f',valueExpected, valueObj));
            
            % Try non numeric value
            previousValue = testCase.parametre.Value;
            testCase.parametre.Value = 'abcdef';
            valueObj = testCase.parametre.Value;
            testCase.verifyEqual(previousValue,valueObj, sprintf('value expected : %f, but was %f',previousValue, valueObj));
        end
    end
end
