classdef ScienceTest < matlab.unittest.TestCase
    % Description
    %   ScienceTest Unit Test for Science Classes
    %
    % Laucnh test :
    %   res = run(ScienceTest)
    %
    % Output Arguments
    %   res : atlab.unittest.TestResult instance
    %
    % Examples
    %   res = run(ScienceTest);
    %   rt = table(res)
    %
    % Authors  : MHO
    % See also ClSignal DialogsTest
    % ----------------------------------------------------------------------------
    
    properties
    end
    
    methods (Test)
        function testModelTest(this) %#ok<MANU>
            % TODO and uncomment the line in matlab_startup
        end
        
        function testOptimTest(this) %#ok<MANU>
            % TODO and uncomment the line in matlab_startup
        end
        
        function testParametreTest(this)
            res = run(ClParametreTest);
            isAllPassed = logical(all([res.Passed]));
            this.verifyTrue(isAllPassed, 'ClParametreTest Failed');
        end
        
        function testSignalTest(this)
            res = run(ClSignalTest);
            isAllPassed = logical(all([res.Passed]));
            this.verifyTrue(isAllPassed, 'ClSignalTest Failed');
            
            res = run(SignalUtilsTest);
            isAllPassed = logical(all([res.Passed]));
            this.verifyTrue(isAllPassed, 'SignalUtilsTest Failed');
        end
    end
end
