classdef ClSignalTest < matlab.unittest.TestCase
    % Description
    %   SignalTest Unit Test for ClSignal
    %
    % Laucnh test :
    %   res = run(ClSignalTest)
    %
    % Output Arguments
    %   res : atlab.unittest.TestResult instance
    %
    % Examples
    %   res = run(ClSignalTest);
    %   rt = table(res)
    %
    % Authors  : MHO
    % See also ClSignal DialogsTest
    % ----------------------------------------------------------------------------
    
    properties
    end
    
    methods (TestMethodSetup)
        function createClSignalTest(this) %#ok<MANU>
            % nothing to do here...
        end
    end
    
    methods(TestMethodTeardown)
        function closeClSignalTest(this) %#ok<MANU>
            close all;
        end
    end
    
    methods (Test)
        function testCreateSignal(this)
            %% Create instances of ClSignal
            % Creating instances of ClSignal with or without parameters
            
            %% Without input parameters
            % Default values will be given to all of the instance's properties
            
            sg1 = ClSignal; %#ok<NASGU>
            
            %% With input parameters
            % The input values will be given to the parameters. If no input was set for
            % a parameter, it will take its default value.
            
            sg2 =  ClSignal('Name', 'Simple sinus function', 'ySample', YSample('data', sin(0:0.1*pi:10*pi-0.1*pi), 'unit', 'W'), 'xSample', XSample('unit', 'rad'));
            
            expectedResult = sin(0:0.1*pi:10*pi-0.1*pi);
            result = sg2.ySample.data;
            this.verifyEqual(expectedResult, result, ...
                sprintf('sg2.ySample.data expected : %s, but was %s', num2strCode(expectedResult), num2strCode(result)));
        end
              
        function testSignalSubsref(this) %#ok<MANU>
            ExClSignal_Subsref
        end
        
        %% Operations
        
        function testSignalMinus(this) %#ok<MANU>
            ExClSignal_Minus
        end
        
        function testSignalPlus(this) %#ok<MANU>
            ExClSignal_Plus
        end
        
        function testSignalDivision(this) %#ok<MANU>
            ExClSignal_Mrdivide
        end
        
        function testSignalMultiplication(this) %#ok<MANU>
            ExClSignal_Mtimes
        end
        
        %% Getters/Setters
        
        function testSignalSetters(this) %#ok<MANU>
            ExClSignal_Set
        end
        
        function testSignalGetters(this) %#ok<MANU>
            ExClSignal_Get
        end
        
        %% Demo
        
%         function testSignalKM(this) %#ok<MANU>
%             ExClSignalKM
%         end
        
        function testSignalStatisticCurves(this) %#ok<MANU>
            ExClSignalStatisticCurves
        end
    end
end
