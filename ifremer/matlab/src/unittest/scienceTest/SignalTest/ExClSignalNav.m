%% ExClSignalNav
% Example of ClSignal with nav file

%% Read input file

% nomFic = 'C:\Temp\Post-Traitement-Navigation-DelphINS.txt';
%
% [flag,Time, Lat, Lon, Immersion, Heading, Pitch, Roll, Heave] = lecFicNav_DelphINS(nomFic);
% if ~flag
%     return
% end
% save C:\Temp\Post-Traitement-Navigation-DelphINS-pierre.mat Time Lat Lon Immersion Heading Pitch Roll Heave;

load C:\Temp\Post-Traitement-Navigation-DelphINS-pierre.mat;

%% Creating samples for each data

% X Sample
T = datetime(Time.timeMat, 'ConvertFrom', 'datenum');
timeSample = XSample('name', 'time', 'data', T);

% Y Samples
latSample       = YSample('Name', 'Lat',       'data', Lat,       'unit', 'deg');
lonSample       = YSample('Name', 'Lon',       'data', Lon,       'unit', 'deg');
immersionSample = YSample('Name', 'Immersion', 'data', Immersion, 'unit', 'm');
headingSample   = YSample('Name', 'Heading',   'data', Heading,   'unit', 'deg');
pitchSample     = YSample('Name', 'Pitch',     'data', Pitch,     'unit', 'deg');
rollSample      = YSample('Name', 'Roll',      'data', Roll,      'unit', 'deg');
heaveSample     = YSample('Name', 'Heave',     'data', Heave,     'unit', 'm');

%% Signal with all data

allDataSample(1) = latSample;
allDataSample(2) = lonSample;
allDataSample(3) = immersionSample;
allDataSample(4) = headingSample;
allDataSample(5) = pitchSample;
allDataSample(6) = rollSample;
allDataSample(7) = heaveSample;

allDataSignal = ClSignal('Name', 'All Data', 'xSample', timeSample, 'ySample', allDataSample);
allDataSignal.plot();

%% Navigation Signal

navSignal = ClSignal('Name', 'Navigation', 'xSample', lonSample, 'ySample', latSample);
navSignal.plot();

%% Nav 3D

FigUtils.createSScFigure; plot3(lonSample.data, latSample.data, immersionSample.data); grid on

% % Attitude Signals without datetime
% attitudeSignal.xSample = XSample.empty;
% attitudeSignal.plot();

% plot(navSignal, 'subAxes', [1 2 3 4 4]);
