% Getting the value of one or more parameters
%%
sg1 = ClSignal('Name', 'Sine Cardinal', 'ySample', YSample('data', sinc(0:0.1*pi:10*pi).', 'unit', 'dB', 'color', 'r'));

%% Getting one value
%With an output variable

x = sg1.name;
% a = get(sg1, 'Name');

%% Getting more than one value

a = sg1.name;
b = sg1.xSample.data;
c = sg1.ySample.data;
d = sg1.ySample.color;


%% Using the results of the "get" command to use another function

var = 2*sg1.ySample.data;
%%
plot(sg1.xSample.data, sg1.ySample.data, 'Color', sg1.ySample.color);
