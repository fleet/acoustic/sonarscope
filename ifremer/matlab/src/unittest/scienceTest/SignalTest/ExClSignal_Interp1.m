%% interp1
% Interpolating a signal with rough edges

%% Description of first code block
ysample(1) = YSample('data', rand(50,1), 'unit', 'W', 'color', 'm');
ysample(2) = YSample('data', sin(linspace(0,50,50)).'+rand(50,1), 'unit', 'W', 'color', 'r');
sg1 = ClSignal('Name', '50 random values', 'ySample', ysample);

plot(sg1);

%% Intetrpolating the signal
% The function needs :
% * The input signal
% * The finer abscissa definition
% * Optional : the interpolation method ('spline', 'linear', 'cubic', etc.)
%
finerAbsc = linspace(1, 50, 1000); %1000 values between 1 and 50

SignalUtils.interp1(sg1, finerAbsc, 'spline');
% sg1.interp1(finerAbsc, 'spline');

%%
sg1.name = 'The interpolated signal';
plot(sg1);
