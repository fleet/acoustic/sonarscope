%% isnan
% Checks if missing values (NaN) exist in an instance of ClSignal

%% Result with no missing values :
% Creating an instance of ClSignal and putting NaNs in it

sgNaN = ClSignal('ySample', YSample('data' , rand(10, 1), 'color', 'g'), 'Name', '10 random values');
%%
plot(sgNaN);
%%
% There are no missing values for the moment
isNaN = SignalUtils.isnan(sgNaN);

%% Result with missing values :
% Introducing NaNs into the signal :

l = length(sgNaN.ySample.data);
r = randi([1 l], 1, randi([1 5], 1, 1));
sgNaN.ySample.data(r) = NaN;
sgNaN.name = '10 random values with missing data';
plot(sgNaN);

%%
isNaN = SignalUtils.isnan(sgNaN);
