%% Multiplication
sg1 = ClSignal('ySample', YSample('data', sin(0:0.1*pi:20*pi-0.1*pi), 'color', 'r', 'unit', 'dB'));
%%
plot(sg1);
sg2 = ClSignal('ySample', YSample('data', (2*sin(0:0.1*pi:20*pi-0.1*pi))+rand(1,200), 'color', 'g', 'unit', 'dB'));
plot(sg2);
%%
% % Cannot be done on dB signals
sg1 = sg1*sg2;
%%
sg1.ySample.unit = 'W';
sg2.ySample.unit = 'W';
%%
sg1 = sg1*sg2;
plot(sg1);
%%
sg1 = sg1*4;

plot(sg1);
%%
sg1 = 2*sg1;
plot(sg1);
%%
sg1 = 2 * sg2;
