%% Operators
% These function are overdefinitions of already existing +,-,*,/ functions.
% They allow the addition/subtraction/multiplication/division between two
% signals, or between a signal and a scalar. Some conditions have to be
% respected in order for the operation to be done.

% For example, addition or subtraction between signals that have different
% units (both abscissa and ordinate) cannot be done :
sg1 = ClSignal('ySample', YSample('data', sin(0:0.1*pi:20*pi-0.1*pi), 'color', 'r', 'unit', 'dB'), 'Name', 'sg1');
sg2 = ClSignal('ySample', YSample('data', (2*sin(0:0.1*pi:20*pi-0.1*pi))+rand(1,200), 'color', 'g', 'unit', 'dB'), 'Name', 'sg2');
%%
sg1.ySample.set('unit', 'dB');
sg2.ySample.set('unit', 'W');

sg1=sg1+sg2;
%%

sg1.ySample.set('unit', 'dB');
sg2.ySample.set('unit', 'dB');
plot(sg1);
plot(sg2);
sg1=sg1+sg2;
sg1.name = 'Sum between sg1 and sg2';
plot(sg1);

%%
sg1 = 2+sg1;
sg1.name = '2+sg1';

plot(sg1);
%%
sg1 = sg1 + 6;
sg1.name = 'sg1 + 6';
plot(sg1);
