%% set
%%
sg1 = ClSignal('Name', 'toto');

% n = get(sg1, 'Name')
n = sg1.name;
%pwd
%% Setting values using the "set" method

%%
% Without output parameters
sg1.set('Name', '2xSine Cardinal', 'ySample', YSample('data', 2*sinc(0:0.1*pi:10*pi), 'unit', 'dB', 'color', 'r'));
plot(sg1);
