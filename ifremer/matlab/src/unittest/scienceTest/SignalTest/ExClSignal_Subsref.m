% KESACO ?

%% subsref
% Getting one single parameter value from an instance of ClSignal, and
% storing it in a variable
%%
sg1 = ClSignal('Name', 'Sine Cardinal', 'ySample', YSample('data', sinc(0:0.1*pi:10*pi), 'unit', 'dB', 'color', 'r'));

%% Storing the value in a variable
%
a = sg1.name;
b = sg1.ySample.data;

%% Using the value in another function

a = 2*sg1.ySample.data;
plot(sg1.xSample.data, sg1.ySample.data, sg1.xSample.data, a);
