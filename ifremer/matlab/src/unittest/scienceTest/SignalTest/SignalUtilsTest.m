classdef SignalUtilsTest < matlab.unittest.TestCase
    % Description
    %   SignalUtilsTest Unit Test for ClSignal
    %
    % Laucnh test :
    %   res = run(SignalUtilsTest)
    %
    % Output Arguments
    %   res : atlab.unittest.TestResult instance
    %
    % Examples
    %   res = run(SignalUtilsTest);
    %   rt = table(res)
    %
    % Authors  : MHO
    % See also ClSignal DialogsTest
    % ----------------------------------------------------------------------------
    
    properties
    end
    
    methods (TestMethodSetup)
        function createSignalUtilsTest(this) %#ok<MANU>
            % nothing to do here...
        end
    end
    
    methods(TestMethodTeardown)
        function closeClSignalTest(this) %#ok<MANU>
            close all;
        end
    end
    
    methods (Test)
        function testSignalFillNaN(this)
            %% fillNaN
            % Filling the NaN values with the mean value of the signal inside a sliding
            % window
            
            %% Creating a signal with missing values
            
            sgNaN = ClSignal('ySample', YSample('data', rand(200, 1), 'color', 'k'), 'Name', '200 random values');
            
            l = length(sgNaN.ySample.data);
            r = randi([1 l], 1, randi([1 20], 1, 1));
            sgNaN.ySample.data(r) = NaN;
            sgNaN.name = '200 random values with missing data';
            
            plot(sgNaN);
            
            %% Filling the NaN values
            
            window = 2;
            % It means that the function will use 2 values around the NaN's location in
            % the signal, calculate the mean value, and replace NaN with this mean
            % value.
            SignalUtils.fillNaN(sgNaN, window);
            
            %sgNaN.Name = 'The signal once processed with fillNaN';
            plot(sgNaN);
            
            result = sgNaN.ySample.data(r);
            this.verifyTrue(~isnan(result(1,:)), ...
                sprintf('sgNaN.ySample.data(r) is nan'));
        end

        
        function testSignalInterpolation(this) %#ok<MANU>
            ExClSignal_Interp1
        end
        
        function testSignalIsNaN(this) %#ok<MANU>
            ExClSignal_Isnan
        end
    end
end
