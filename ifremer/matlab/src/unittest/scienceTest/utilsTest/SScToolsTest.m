classdef SScToolsTest < matlab.unittest.TestCase
    % Description
    %   SScToolsTest Unit Test for Science Classes
    %
    % Laucnh test :
    %   res = run(SScToolsTest)
    %
    % Output Arguments
    %   res : atlab.unittest.TestResult instance
    %
    % Examples
    %   res = run(SScToolsTest);
    %   rt = table(res)
    %
    % Authors : JMA
    % See also ClOptim OptimDialog ClSignal SignalDialog ClNavigation NavigationDialog
    %          PamesDialog
    % ----------------------------------------------------------------------------
    
    properties
    end
    
    methods (TestMethodSetup)
        function createSScToolsTest(this) %#ok<MANU>
            % nothing to do here...
        end
    end
    
    methods (TestMethodTeardown)
        function closeSScToolsTest(this) %#ok<MANU>
            close all;
        end
    end
    
    methods (Test)
        function testOptimDialogTest(this)
            flag = this.checkOptimDialog();
            this.verifyTrue(logical(flag), 'OptimDialog Test Failed');
        end
        
        function testSignalDialogTest(this)
            flag = this.checkSignalDialog();
            this.verifyTrue(logical(flag), 'SignalDialog Test Failed');
        end
        
        function testNavigationDialogTest(this)
            flag = this.checkNavigationDialog();
            this.verifyTrue(logical(flag), 'NavigationDialog Test Failed');
        end
        
        function testSspDialogTest(this)
            flag = this.checkSspDialog();
            this.verifyTrue(logical(flag), 'SspDialog Test Failed');
        end
        
        function testPamesDialogTest(this)
            flag = this.checkPamesDialog();
            this.verifyTrue(logical(flag), 'PamesDialog Test Failed');
        end
    end
    
    
    methods (Static)
        
        function flag = checkOptimDialog
            % Check the optimization tool
            %
            % Syntax
            %   flag = SScToolsTest.checkOptimDialog
            %
            % Output Arguments
            %   flag : 1=OK, 0=KO
            %
            % Examples
            %   dataFileName = SScToolsTest.checkOptimDialog
            %
            % Authors  : JMA
            % See also : ClOptom OptimDialog
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc ClOptim"      >ClOptim</a>
            %   <a href="matlab:doc OptimDialog"  >OptimDialog</a>
            %   <a href="matlab:doc SScToolsTest" >SScToolsTest</a>
            %  -------------------------------------------------------------------------------
            
            try
                ExOptimDialogBackscatter
                pause(1)
                flag = 1;
            catch
                flag = 0;
            end
        end
        
        
        function flag = checkSignalDialog
            % Check the signal tool
            %
            % Syntax
            %   flag = SScToolsTest.checkSignalDialog
            %
            % Output Arguments
            %   flag : 1=OK, 0=KO
            %
            % Examples
            %   dataFileName = SScToolsTest.checkSignalDialog
            %
            % Authors  : JMA
            % See also : TODO
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc ClSignal"     >ClSignal.TODO</a>
            %   <a href="matlab:doc SignalDialog" >SignalDialog.TODO</a>
            %   <a href="matlab:doc SScToolsTest" >SScToolsTest</a>
            %  -------------------------------------------------------------------------------
            
            try
                ExSignalDialogMultiSectors
                pause(1)
                ExSignalDialogMultiSectors
                pause(1)
                flag = 1;
            catch
                flag = 0;
            end
        end
        
        
        function flag = checkNavigationDialog
            % Check the navigation tool
            %
            % Syntax
            %   flag = SScToolsTest.checkNavigationDialog
            %
            % Output Arguments
            %   flag : 1=OK, 0=KO
            %
            % Examples
            %   dataFileName = SScToolsTest.checkNavigationDialog
            %
            % Authors  : JMA
            % See also : TODO
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc ClNavigation"     >ClNavigation</a>
            %   <a href="matlab:doc NavigationDialog" >NavigationDialog</a>
            %   <a href="matlab:doc SScToolsTest"     >SScToolsTest</a>
            %  -------------------------------------------------------------------------------
            
            try
                ExNavigationDialogAUV
                pause(1)
                flag = 1;
            catch
                flag = 0;
            end
        end
        
        
        function flag = checkSspDialog
            % Check the sound speed profile tool
            %
            % Syntax
            %   flag = SScToolsTest.checkSspDialog
            %
            % Output Arguments
            %   flag : 1=OK, 0=KO
            %
            % Examples
            %   dataFileName = SScToolsTest.checkSspDialog
            %
            % Authors  : JMA
            % See also : TODO
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc SScToolsTest" >SScToolsTest</a>
            %  -------------------------------------------------------------------------------
            
            try
                ExSignalDialogSoundSpeedProfile
                pause(1)
                flag = 1;
            catch
                flag = 0;
            end
        end
        
        
        function flag = checkPamesDialog
            % Check the PAMES tools
            %
            % Syntax
            %   flag = SScToolsTest.checkPamesDialog
            %
            % Output Arguments
            %   flag : 1=OK, 0=KO
            %
            % Examples
            %   dataFileName = SScToolsTest.checkPamesDialog
            %
            % Authors  : JMA
            % See also : TODO
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc PamesDialog"  >PamesDialog</a>
            %   <a href="matlab:doc SScToolsTest" >SScToolsTest</a>
            %  -------------------------------------------------------------------------------
            
            try
                ExNewPAMES
                pause(1)
                flag = 1;
            catch
                flag = 0;
            end
        end
    end
end
