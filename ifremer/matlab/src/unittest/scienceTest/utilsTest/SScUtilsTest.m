classdef SScUtilsTest < matlab.unittest.TestCase
    % Description
    %   SScUtilsTest Unit Test for Science Classes
    %
    % Laucnh test :
    %   res = run(SScUtilsTest)
    %
    % Output Arguments
    %   res : atlab.unittest.TestResult instance
    %
    % Examples
    %   res = run(SScUtilsTest);
    %   rt = table(res)
    %
    % Authors : MHO
    % See also ClSignal DialogsTest
    % ----------------------------------------------------------------------------
    
    properties
    end
    
    methods (TestMethodSetup)
        function createSScUtilsTest(this) %#ok<MANU>
            % nothing to do here...
        end
    end
    
    methods (TestMethodTeardown)
        function closeSScUtilsTest(this) %#ok<MANU>
            close all;
        end
    end
    
    methods (Test)
        function testFtpUtilsTest(this)
            flag = this.checkGetFromFtpSScDemoFiles();
            this.verifyTrue(logical(flag), 'FtpUtils Test Failed');
        end
        
        function testSScCacheXMLBinUtilsTest(this)
            flag = this.checkSScCacheXMLBinUtilsReadWholeData();
            this.verifyTrue(logical(flag), 'SScCacheXMLBinUtils Test Failed');
        end
        
        function testSScCacheNetcdfUtilsTest(this)
            flag = this.checkSScCacheNetcdfUtilsReadWholeData();
            this.verifyTrue(logical(flag), 'SScCacheNetcdfUtils Test Failed');
        end
    end
    
    
    methods (Static)
        
        function flag = checkGetFromFtpSScDemoFiles
            % Check the import of a set of demo files from the SSc ftp site
            %
            % Syntax
            %   flag = SScUtilsTest.checkGetFromFtpSScDemoFiles
            %
            % Output Arguments
            %   flag : 1=OK, 0=KO
            %
            % Examples
            %   dataFileName = SScUtilsTest.checkGetFromFtpSScDemoFiles
            %
            % Authors  : JMA
            % See also : getSScDemoFile
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc FtpUtils.getSScDemoFile"  >FtpUtils.getSScDemoFile</a>
            %   <a href="matlab:doc FtpUtils"                 >FtpUtils</a>
            %  -------------------------------------------------------------------------------
            
            [flag, ~] = FtpUtils.getSScDemoFile('ESSTECH19020.rdf', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            
            [flag, ~] = FtpUtils.getSScDemoFile('usblRepeater09Modif.txt', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            
            [flag, ~] = FtpUtils.getSScDemoFile('usblRepeater10Modif.txt', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            
            [flag, ~] = FtpUtils.getSScDemoFile('usblRepeater11Modif.txt', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            
            [flag, ~] = FtpUtils.getSScDemoFile('usblRepeater12Modif.txt', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            
            [flag, ~] = FtpUtils.getSScDemoFile('0014.04.im', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            
            [flag, ~] = FtpUtils.getSScDemoFile('Syp_001_180507104600.sdf', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            
            [flag, ~] = FtpUtils.getSScDemoFile('Syp_001_180507104600.xtf', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            
            [flag, ~] = FtpUtils.getSScDemoFile('tan1505-D20150428-T013331.raw', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            
            [flag, ~] = FtpUtils.getSScDemoFile('EST190019_D20190214_T090030.SEG', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            
            [flag, ~] = FtpUtils.getSScDemoFile('Mobydick_025_20180315_002209.hac', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            
            [flag, ~] = FtpUtils.getSScDemoFile('0059_20130203_134006_Thalia.all', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            
            [flag, ~] = FtpUtils.getSScDemoFile('D20190625-T100713.raw', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            
            [flag, ~] = FtpUtils.getSScDemoFile('ADCP_38khz_Malaga_Brest__006_allPofil.odv', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            
            [flag, ~] = FtpUtils.getSScDemoFile('0013_20191022_173330_SimonStevin.all', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            
            [flag, ~] = FtpUtils.getSScDemoFile('0000_20150309_200643_Belgica.all', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            
            [flag, ~] = FtpUtils.getSScDemoFile('0000_20130802_152942_Europe.all', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            
            [flag, ~] = FtpUtils.getSScDemoFile('0029_20180306_213505_SimonStevin.all', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            
            [flag, ~] = FtpUtils.getSScDemoFile('0029_20180306_213505_SimonStevin.wcd', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            
            [flag, ~] = FtpUtils.getSScDemoFile('0030_20180306_213905_SimonStevin.all', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            
            [flag, ~] = FtpUtils.getSScDemoFile('0030_20180306_213905_SimonStevin.wcd', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            
            [flag, ~] = FtpUtils.getSScDemoFile('0031_20180306_214305_SimonStevin.all', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            
            [flag, ~] = FtpUtils.getSScDemoFile('0031_20180306_214305_SimonStevin.wcd', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            
            flag = 1;
        end
        
        
        
        function flag = checkSScCacheXMLBinUtilsReadWholeData
            % Examples of use of checkSScCacheXMLBinUtilsReadWholeData
            %
            % Syntax
            %   flag = SScUtilsTest.checkSScCacheXMLBinUtilsReadWholeData
            %
            % Output Arguments
            %   flag : 0=KO, 1=OK
            %
            % Examples
            %   flag = SScUtilsTest.checkSScCacheXMLBinUtilsReadWholeData
            %
            % See also readWholeData
            % Authors : JMA
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc SScUtilsTest.checkSScCacheXMLBinUtilsReadWholeData" >SScUtilsTest.checkSScCacheXMLBinUtilsReadWholeData</a>
            %   <a href="matlab:doc SScCacheXMLBinUtils.readWholeData"                  >SScCacheXMLBinUtils.readWholeData</a>
            %   <a href="matlab:doc SScCacheXMLBinUtils"                                >SScCacheXMLBinUtils</a>
            %  -------------------------------------------------------------------------------
                        
            % ------------------ ALL Format V2 ------------------
            dataFileName = getNomFicDatabase('0011_20110307_211804_IKATERE_02.all');
            aKMV2a = cl_simrad_all('nomFic', dataFileName);
            
            [flag, DataRuntime] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_Runtime'); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, DataSeabed] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_SeabedImage59h',    'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, DataRawRange] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_RawRangeBeamAngle', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, DataSurfaceC] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_SurfaceSoundSpeed', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            if ~flag
                return
            end
            [flag, DataClock] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_Clock'); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, DataSSP] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_SoundSpeedProfile'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            % Lectures pas encore rempla�ables
            [flag, DataAttitude] = read_attitude_bin(aKMV2a, 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, DataDepth] = read_depth_bin(aKMV2a, 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, DataPosition] = read_position_bin(aKMV2a, 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, DataIP, isDual] = read_installationParameters_bin(aKMV2a); %#ok<ASGLU>
            if ~flag
                return
            end
            
            % ------------------ ALL Donn�e avec QFIfremer ------------------
            [flag, dataFileName] = FtpUtils.getSScDemoFile('0000_20130802_152942_Europe.all', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            aKMV2b = cl_simrad_all('nomFic', dataFileName); %#ok<NASGU>
            
            [flag, DataQualityFactor] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_QualityFactorFromIfr', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            
            % ------------------ ALL Format V1-F ------------------
            dataFileName = getNomFicDatabase('0034_20070406_081642_raw.all');
            aKMV1F = cl_simrad_all('nomFic', dataFileName); %#ok<NASGU>
            
            [flag, DataSeabed] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_SeabedImage53h',    'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, DataRawRange] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_RawRangeBeamAngle', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            
            % ------------------ ALL Format V1-f ------------------
            [flag, dataFileName] = FtpUtils.getSScDemoFile('0000_20150309_200643_Belgica.all', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            aKMV1f = cl_simrad_all('nomFic', dataFileName); %#ok<NASGU>
            
            [flag, Data] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_Heading'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            % ------------------ ALL Donn�es avec datagramme height ------------------
            [flag, dataFileName] = FtpUtils.getSScDemoFile('0013_20191022_173330_SimonStevin.all', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            aV2c = cl_simrad_all('nomFic', dataFileName);
            
            [flag, DataHeight] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_Height'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            % ------------------ ALL Lectures pas encore rempla�ables ------------------
            [flag, DataWC1] = read_WaterColumn_bin(aV2c, 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            
            % ------------------ ALL Donn�es avec datagrammes Stave ------------------
            [flag, dataFileName] = FtpUtils.getSScDemoFile('0059_20130203_134006_Thalia.all', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            cl_simrad_all('nomFic', dataFileName);
            
            [flag, Data] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_StaveData', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            
            % ------------------ ALL Donn�es avec datagrammes Ssc_Height ------------------
            [flag, dataFileName] = FtpUtils.getSScDemoFile('0056_20150615_234546_ATL_EM710.all', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            cl_simrad_all('nomFic', dataFileName);
            
            [flag, DataHeight] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_Height'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            % ------------------ ALL Donn�es avec datagrammes Mag&Phase ------------------
            flag = FtpUtils.getSScDemoFile('0056_20150615_234546_ATL_EM710.wcd', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            [flag, dataFileName] = FtpUtils.getSScDemoFile('0056_20150615_234546_ATL_EM710.all', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            cl_simrad_all('nomFic', dataFileName);
            
            [flag, Data2] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_RawBeamSamples', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            
            % ------------------ IM Donn�es SAR ------------------
            [flag, dataFileName] = FtpUtils.getSScDemoFile('0014.04.im', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            flag = Sar2ssc(dataFileName);
            if ~flag
                return
            end
            
            [flag, Data] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_Sar', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            
            % ------------------ SDF Donn�es Klein ------------------
            [flag, dataFileName] = FtpUtils.getSScDemoFile('Syp_001_180507104600.sdf', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            a = cl_sdf('nomFic', dataFileName);
            
            [flag, Data] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_V3001', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_V3001', 'XMLOnly', 1); %#ok<ASGLU>
            if ~flag
                return
            end
            listeSignals = get_listeSignals(a); %#ok<NASGU>
            
            % ------------------ RAW Donn�es EK60 ------------------
            [flag, dataFileName] = FtpUtils.getSScDemoFile('tan1505-D20150428-T013331.raw', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            cl_ExRaw('nomFic', dataFileName);
            
            [flag, Data] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_Sample', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_Height'); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_Position'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, f, Unit] = SScCacheXMLBinUtils.getSignalUnit(dataFileName, 'Ssc_Sample',   'AcousticFrequency'); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, t, Unit] = SScCacheXMLBinUtils.getSignalUnit(dataFileName, 'Ssc_Position', 'Time'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, Signal] = SScCacheXMLBinUtils.getTimeSignalUnit(dataFileName, 'Ssc_Position', 'Heading'); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Signal] = SScCacheXMLBinUtils.getTimeSignalUnit(dataFileName, 'Ssc_Position', 'Height'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            % ------------------ RAW Donn�es EK80 ------------------
            [flag, dataFileName] = FtpUtils.getSScDemoFile('D20190625-T100713.raw', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            cl_ExRaw('nomFic', dataFileName);
            
            [flag, Data] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_Sample', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_Height'); %#ok<ASGLU>
            if ~flag
                %                 return
            end
            [flag, Data] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_Position'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, f, Unit] = SScCacheXMLBinUtils.getSignalUnit(dataFileName, 'Ssc_Sample',   'AcousticFrequency'); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, t, Unit] = SScCacheXMLBinUtils.getSignalUnit(dataFileName, 'Ssc_Position', 'Time'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, Signal] = SScCacheXMLBinUtils.getTimeSignalUnit(dataFileName, 'Ssc_Position', 'Heading'); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Signal] = SScCacheXMLBinUtils.getTimeSignalUnit(dataFileName, 'Ssc_Position', 'Height'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            % ------------------ XTF Donn�es Klein ------------------
            [flag, dataFileName] = FtpUtils.getSScDemoFile('Syp_001_180507104600.xtf', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            cl_xtf('nomFic', dataFileName);
            
            [flag, Data] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_FileHeader', 'XMLOnly', 1); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_SideScan',   'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_Bathy',      'Memmapfile', -1); %#ok<ASGLU> % Non test�
            if ~flag
                %                 return
            end
            [flag, Data] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_Bathymetry', 'Memmapfile', -1); %#ok<ASGLU> % Pas test�
            if ~flag
                %                 return
            end
            [flag, Data] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_QPSMultiBeamHeader_0', 'Memmapfile', -1); %#ok<ASGLU> % Non test�
            if ~flag
                %                 return
            end
            
            % ------------------ Test fichiers merdiques ------------------
            %{
            dataFileName = 'Z:\private\ifremer\Herve\0006_20200206_203241_Thalassa.all';
            [flag, DataRuntime]  = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_Runtime'); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, DataSeabed]   = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_SeabedImage59h',    'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, DataRawRange] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_RawRangeBeamAngle', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, DataSurfaceC] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_SurfaceSoundSpeed', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, DataClock]    = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_Clock'); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, DataSSP]      = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_SoundSpeedProfile'); %#ok<ASGLU>
            if ~flag
                return
            end
            %}
            
            % Lectures pas encore rempla�ables
            [flag, DataAttitude] = read_attitude_bin(aKMV2a, 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, DataDepth] = read_depth_bin(aKMV2a, 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, DataPosition] = read_position_bin(aKMV2a, 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, DataIP, isDual] = read_installationParameters_bin(aKMV2a); %#ok<ASGLU>
            if ~flag
                return
            end
            
            % ------------------ Donn�es ADCP format ODV ------------------
            [flag, dataFileName] = FtpUtils.getSScDemoFile('ADCP_38khz_Malaga_Brest__006_allPofil.odv', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            
            [flag, Data] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_Data', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            
            % ------------------ Trac�s graphiques de donn�es ALL ------------------
            dataFileName = getNomFicDatabase('0011_20110307_211804_IKATERE_02.all');
            
            ALL_plotRuntime(dataFileName)
            ALL_plotSeabed(dataFileName)
            ALL_plotRawRange(dataFileName)
            ALL_plotDepth(dataFileName)
            
            %{
            
            %% Attention : RDF supprime le cache XMLBin et force le passage au cache Netcdf met useCacheNetcdf � 1 automatiquement
                        
            % ------------------ RDF Donn�es Geoswath ------------------
            [flag, dataFileName] = FtpUtils.getSScDemoFile('ESSTECH19020.rdf', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            cl_rdf('nomFic', dataFileName);
            
            [flag, Data] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_Navigation'); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_Heading'); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_Attitude'); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_Aux1String'); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_FileIndex'); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_Header'); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_HeadingString'); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_MiniSVSString'); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_MiniSVS'); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_NavigationString'); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_Height'); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_Aux2String'); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_FileHeader', 'XMLOnly', 1); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_Sample', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, Data] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_EchoSounderString'); %#ok<ASGLU> % Non test�
            if ~flag
                return
            end
            [flag, Data] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_EchoSounder'); %#ok<ASGLU>% Non test�
            if ~flag
                return
            end
            
            [flag, Data, Unit] = SScCacheXMLBinUtils.getSignalUnit(dataFileName, 'Ssc_Sample', 'Mask'); %#ok<ASGLU>
             if ~flag
                return
             end
            
            %}
        end
        
        function flag = checkSScCacheNetcdfUtilsReadWholeData
            % Examples of use of checkSScCacheNetcdfUtilsReadWholeData
            %
            %
            % Syntax
            %   flag = SScUtilsTest.checkSScCacheNetcdfUtilsReadWholeData
            %
            % Output Arguments
            %   flag : 0=KO, 1=OK
            %
            % Examples
            %   flag = SScUtilsTest.checkSScCacheNetcdfUtilsReadWholeData
            %
            % See also readWholeData
            % Authors : JMA
            %
            % Reference pages in Help browser
            %   <a href="matlab:doc SScUtilsTest.checkSScCacheNetcdfUtilsReadWholeData" >SScUtilsTest.checkSScCacheNetcdfUtilsReadWholeData</a>
            %   <a href="matlab:doc SScCacheNetcdfUtils.readWholeData"                  >SScCacheNetcdfUtils.readWholeData</a>
            %   <a href="matlab:doc SScCacheNetcdfUtils"                                >SScCacheNetcdfUtils</a>
            %  -------------------------------------------------------------------------------
            
            % ------------------ ALL Format V2 ------------------
            dataFileName = getNomFicDatabase('0011_20110307_211804_IKATERE_02.all');
            
            [flag, Data1] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_Runtime'); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data2] = SScCacheNetcdfUtils.readWholeData(dataFileName, 'Ssc_Runtime'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, Data1] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_SeabedImage59h', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data2] = SScCacheNetcdfUtils.readWholeData(dataFileName, 'Ssc_SeabedImage59h', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, Data1] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_RawRangeBeamAngle', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data2] = SScCacheNetcdfUtils.readWholeData(dataFileName, 'Ssc_RawRangeBeamAngle', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, Data1] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_SurfaceSoundSpeed', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data2] = SScCacheNetcdfUtils.readWholeData(dataFileName, 'Ssc_SurfaceSoundSpeed', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, Data1] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_Clock'); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data2] = SScCacheNetcdfUtils.readWholeData(dataFileName, 'Ssc_Clock'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, Data1] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_SoundSpeedProfile'); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data2] = SScCacheNetcdfUtils.readWholeData(dataFileName, 'Ssc_SoundSpeedProfile'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, Data1] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_Attitude'); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data2] = SScCacheNetcdfUtils.readWholeData(dataFileName, 'Ssc_Attitude'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, Data1] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_Depth'); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data2] = SScCacheNetcdfUtils.readWholeData(dataFileName, 'Ssc_Depth'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, Data1] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_Position'); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data2] = SScCacheNetcdfUtils.readWholeData(dataFileName, 'Ssc_Position'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, Data1] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_InstallationParameters'); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data2] = SScCacheNetcdfUtils.readWholeData(dataFileName, 'Ssc_InstallationParameters'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, Data1] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_RawRangeBeamAngle', 'XMLOnly', 1); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data2] = SScCacheNetcdfUtils.readWholeData(dataFileName, 'Ssc_RawRangeBeamAngle', 'XMLOnly', 1); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, Data1, DataBin1, VarNameInFailure1] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_RawRangeBeamAngle', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data2, DataBin2, VarNameInFailure2] = SScCacheNetcdfUtils.readWholeData(dataFileName, 'Ssc_RawRangeBeamAngle', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, Data1] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_RawRangeBeamAngle', 'XMLOnly', 1); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data2] = SScCacheNetcdfUtils.readWholeData(dataFileName, 'Ssc_RawRangeBeamAngle', 'XMLOnly', 1); %#ok<ASGLU>
            if ~flag
                return
            end
            
            % ------------------ ALL Donn�e avec QFIfremer ------------------
            [flag, dataFileName] = FtpUtils.getSScDemoFile('0000_20130802_152942_Europe.all', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            
            [flag, Data1] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_QualityFactorFromIfr', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data2] = SScCacheNetcdfUtils.readWholeData(dataFileName, 'Ssc_QualityFactorFromIfr', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            
            % ------------------ ALL Format V1-F ------------------
            dataFileName = getNomFicDatabase('0034_20070406_081642_raw.all');
            
            [flag, Data1] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_SeabedImage53h', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data2] = SScCacheNetcdfUtils.readWholeData(dataFileName, 'Ssc_SeabedImage53h', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, Data1] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_RawRangeBeamAngle', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data2] = SScCacheNetcdfUtils.readWholeData(dataFileName, 'Ssc_RawRangeBeamAngle', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            
            
            % ------------------ ALL Format V1-f ------------------
            [flag, dataFileName] = FtpUtils.getSScDemoFile('0000_20150309_200643_Belgica.all', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            
            [flag, Data1] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_Heading', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data2] = SScCacheNetcdfUtils.readWholeData(dataFileName, 'Ssc_Heading'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            % ------------------ ALL Donn�es avec datagramme height ------------------
            [flag, dataFileName] = FtpUtils.getSScDemoFile('0013_20191022_173330_SimonStevin.all', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            
            % NON TESTE NON TESTE NON TESTE NON TESTE NON TESTE NON TESTE NON TESTE NON TESTE NON TESTE
            
            [flag, Data1] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_Height', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data2] = SScCacheNetcdfUtils.readWholeData(dataFileName, 'Ssc_Height'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, Data1] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_WaterColumn', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data2] = SScCacheNetcdfUtils.readWholeData(dataFileName, 'Ssc_WaterColumn'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            % ------------------ ALL Donn�es avec datagrammes Stave ------------------
            [flag, dataFileName] = FtpUtils.getSScDemoFile('0059_20130203_134006_Thalia.all', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            
            [flag, Data1] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_StaveData', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data2] = SScCacheNetcdfUtils.readWholeData(dataFileName, 'Ssc_StaveData', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            
            % ------------------ ALL Donn�es avec datagrammes Ssc_Height ------------------
            [flag, dataFileName] = FtpUtils.getSScDemoFile('0056_20150615_234546_ATL_EM710.all', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            
            [flag, Data1] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_Height', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data2] = SScCacheNetcdfUtils.readWholeData(dataFileName, 'Ssc_Height'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            % ------------------ ALL Donn�es avec datagrammes Mag&Phase ------------------
            [flag, dataFileName] = FtpUtils.getSScDemoFile('0056_20150615_234546_ATL_EM710.all', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            
            [flag, Data1] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_RawBeamSamples', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data2] = SScCacheNetcdfUtils.readWholeData(dataFileName, 'Ssc_RawBeamSamples', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            
            % ------------------ IM Donn�es SAR ------------------
            [flag, dataFileName] = FtpUtils.getSScDemoFile('0014.04.im', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            
            [flag, Data1] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_Sar', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data2] = SScCacheNetcdfUtils.readWholeData(dataFileName, 'Ssc_Sar', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            
            % ------------------ SDF Donn�es Klein ------------------
            [flag, dataFileName] = FtpUtils.getSScDemoFile('Syp_001_180507104600.sdf', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            
            [flag, Data1] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_V3001', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data2] = SScCacheNetcdfUtils.readWholeData(dataFileName, 'Ssc_V3001', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            
            % ------------------ RAW Donn�es EK60 ------------------
            [flag, dataFileName] = FtpUtils.getSScDemoFile('tan1505-D20150428-T013331.raw', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            
            [flag, Data1] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_Sample', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data2] = SScCacheNetcdfUtils.readWholeData(dataFileName, 'Ssc_Sample', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, Data1] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_Height', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data2] = SScCacheNetcdfUtils.readWholeData(dataFileName, 'Ssc_Height'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, Data1] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_Position', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data2] = SScCacheNetcdfUtils.readWholeData(dataFileName, 'Ssc_Position'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, f, Unit] = SScCacheXMLBinUtils.getSignalUnit(dataFileName, 'Ssc_Sample',   'AcousticFrequency'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, t, Unit] = SScCacheXMLBinUtils.getSignalUnit(dataFileName, 'Ssc_Position', 'Time'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, Signal] = SScCacheXMLBinUtils.getTimeSignalUnit(dataFileName, 'Ssc_Position', 'Heading'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, Signal] = SScCacheXMLBinUtils.getTimeSignalUnit(dataFileName, 'Ssc_Position', 'Height'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            % ------------------ RAW Donn�es EK80 ------------------
            [flag, dataFileName] = FtpUtils.getSScDemoFile('D20190625-T100713.raw', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            
            [flag, Data1] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_Sample', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data2] = SScCacheNetcdfUtils.readWholeData(dataFileName, 'Ssc_Sample', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, Data1] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_Height', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                %                 return
            end
            [flag, Data2] = SScCacheNetcdfUtils.readWholeData(dataFileName, 'Ssc_Height'); %#ok<ASGLU>
            if ~flag
%                 return % TODO : �a bugue
            end
            
            [flag, Data1] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_Position', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data2] = SScCacheNetcdfUtils.readWholeData(dataFileName, 'Ssc_Position'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, f, Unit] = SScCacheXMLBinUtils.getSignalUnit(dataFileName, 'Ssc_Sample',   'AcousticFrequency'); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, t, Unit] = SScCacheXMLBinUtils.getSignalUnit(dataFileName, 'Ssc_Position', 'Time'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, Signal]  = SScCacheXMLBinUtils.getTimeSignalUnit(dataFileName, 'Ssc_Position', 'Heading'); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Signal]  = SScCacheXMLBinUtils.getTimeSignalUnit(dataFileName, 'Ssc_Position', 'Height'); %#ok<ASGLU>
            if ~flag
                return
            end
            
            % ------------------ XTF Donn�es Klein ------------------
            [flag, dataFileName] = FtpUtils.getSScDemoFile('Syp_001_180507104600.xtf', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            
            [flag, Data1] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_FileHeader', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data2] = SScCacheNetcdfUtils.readWholeData(dataFileName, 'Ssc_FileHeader', 'XMLOnly', 1); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, Data1] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_SideScan', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data2] = SScCacheNetcdfUtils.readWholeData(dataFileName, 'Ssc_SideScan', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            
            [flag, Data1] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_Bathy', 'Memmapfile', -1); %#ok<ASGLU> % Non test�
            if ~flag
                %                 return
            end
            [flag, Data2] = SScCacheNetcdfUtils.readWholeData(dataFileName, 'Ssc_Bathy', 'Memmapfile', -1); %#ok<ASGLU> % Non test�
            if ~flag
                %                 return
            end
            
            [flag, Data1] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_Bathymetry', 'Memmapfile', -1); %#ok<ASGLU> % Pas test�
            if ~flag
                %                 return
            end
            [flag, Data2] = SScCacheNetcdfUtils.readWholeData(dataFileName, 'Ssc_Bathymetry', 'Memmapfile', -1); %#ok<ASGLU> % Pas test�
            if ~flag
                %                 return
            end
            
            [flag, Data1] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_QPSMultiBeamHeader_0', 'Memmapfile', -1); %#ok<ASGLU> % Non test�
            if ~flag
                %                 return
            end
            [flag, Data2] = SScCacheNetcdfUtils.readWholeData(dataFileName, 'Ssc_QPSMultiBeamHeader_0', 'Memmapfile', -1); %#ok<ASGLU> % Non test�
            if ~flag
                %                 return
            end
            
            % ------------------ Test fichiers merdiques ------------------
            %{
            dataFileName = 'Z:\private\ifremer\Herve\0006_20200206_203241_Thalassa.all'; % TODO : tester
            [flag, Data2] = SScCacheNetcdfUtils.readWholeData(dataFileName, 'Ssc_Runtime'); %#ok<ASGLU>
            [flag, Data2] = SScCacheNetcdfUtils.readWholeData(dataFileName, 'Ssc_SeabedImage59h',    'Memmapfile', -1); %#ok<ASGLU>
            [flag, Data2] = SScCacheNetcdfUtils.readWholeData(dataFileName, 'Ssc_RawRangeBeamAngle', 'Memmapfile', -1); %#ok<ASGLU>
            [flag, Data2] = SScCacheNetcdfUtils.readWholeData(dataFileName, 'Ssc_SurfaceSoundSpeed', 'Memmapfile', -1); %#ok<ASGLU>
            [flag, Data2] = SScCacheNetcdfUtils.readWholeData(dataFileName, 'Ssc_Clock'); %#ok<ASGLU>
            [flag, Data2] = SScCacheNetcdfUtils.readWholeData(dataFileName, 'Ssc_SoundSpeedProfile'); %#ok<ASGLU>
            %}
            
            % ------------------ Donn�es ADCP format ODV ------------------
            [flag, dataFileName] = FtpUtils.getSScDemoFile('ADCP_38khz_Malaga_Brest__006_allPofil.odv', 'OutputDir', 'D:\Temp\ExData');
            if ~flag
                return
            end
            
            [flag, Data1] = SScCacheXMLBinUtils.readWholeData(dataFileName, 'Ssc_Data', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            [flag, Data2] = SScCacheNetcdfUtils.readWholeData(dataFileName, 'Ssc_Data', 'Memmapfile', -1); %#ok<ASGLU>
            if ~flag
                return
            end
            
            % ------------------ Trac�s graphiques de donn�es ALL ------------------
            dataFileName = getNomFicDatabase('0011_20110307_211804_IKATERE_02.all');
            
            ALL_plotRuntime(dataFileName)
            ALL_plotSeabed(dataFileName)
            ALL_plotRawRange(dataFileName)
            ALL_plotDepth(dataFileName)
        end
    end
end
