% Ouvrir le fichier HDF5 en mode lecture
nomFicXsf   = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\TestsFormatXsf\0039_20180905_222154_raw.xsf.nc';
nomFicXsf   = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\TestsFormatXsf\0048_20221211_193145.xsf.nc';
nomFicXsf   = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\From Marc Roche\kmall\0004_20230824_010208.xsf.nc';
dsName      = '/Sonar/Beam_group1/Bathymetry/seabed_image_samples_r';


%%
tic
[flag, Hdf5Data]    = Hdf5Utils.readGrpData(nomFicXsf, '/');
seabedOrig          = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Bathymetry.seabed_image_samples_r);
toc
clear Hdf5Data

%% Lectures des Seabed ping par ping
clear seabed_samples_r1 dataSeabed
fid = H5F.open(nomFicXsf, 'H5F_ACC_RDONLY', 'H5P_DEFAULT');

% Récupérer l'ID du dataset (remplacez 'nom_de_ensemble' par le nom réel)
dset_id = H5D.open(fid, dsName);

% Définir la sélection pour le sous-ensemble
dspace_id       = H5D.get_space(dset_id);
[ndims,h5_dims] = H5S.get_simple_extent_dims(dspace_id);
tic
for iPing=1:h5_dims(1)
    % Spécifier les indices du sous-ensemble que vous souhaitez lire
    start_indices   = [iPing-1, 0, 0];  % Indice de début du sous-ensemble
    end_indices     = [iPing-1, h5_dims(2)-1, h5_dims(3)-1];  % Indice de fin du sous-ensemble
    
    H5S.select_hyperslab(dspace_id, 'H5S_SELECT_SET', start_indices, [], end_indices - start_indices + 1, []);
    
    % Créer une dataspace pour les données lues
    donnees_space   = H5S.create_simple(length(start_indices), fliplr(end_indices - start_indices + 1), []);
    
    % Lire les données du sous-ensemble
    dataSeabed    = H5D.read(dset_id, 'H5ML_DEFAULT', donnees_space, dspace_id, 'H5P_DEFAULT');
    % Transformation necessaire avec ce type de lecture, different de la
    % lecture d'un seul bloc du dataset.
    dataSeabed    = reshape(dataSeabed, [h5_dims(3) h5_dims(2)]);
    dataSeabed    = double(squeeze(dataSeabed));
    dataSeabed(dataSeabed==-32768) = NaN;
    dataSeabed   = dataSeabed * 0.1;
     
    seabed_samples_r1(:, :, iPing) = dataSeabed';
end

% Fermer les identifiants
H5S.close(dspace_id);
H5D.close(dset_id);
H5F.close(fid);
toc

%% Lecture des Seabed samples par paquet de Ping
clear seabed_samples_r2 dataSeabed


fid = H5F.open(nomFicXsf, 'H5F_ACC_RDONLY', 'H5P_DEFAULT');

tic
% Récupérer l'ID du dataset (remplacez 'nom_de_ensemble' par le nom réel)
dset_id = H5D.open(fid, dsName);

% Définir la sélection pour le sous-ensemble
dspace_id       = H5D.get_space(dset_id);
[ndims,h5_dims] = H5S.get_simple_extent_dims(dspace_id);

fillValue   = NaN;
scaleFactor = 1;
addOffset   = 0;
info = H5O.get_info(dset_id);
% Open each attribute, print its name, then close it.
gID = H5G.open(fid,'/Sonar/Beam_group1/Bathymetry/');
for idx = 0:info.num_attrs-1
    attrID = H5A.open_by_idx(gID,'seabed_image_samples_r',"H5_INDEX_NAME","H5_ITER_DEC",idx);
    % fprintf("attribute name:  %s\n",H5A.get_name(attrID));
    switch lower(H5A.get_name(attrID))
        case 'scale_factor'
            scaleFactor = double(H5A.read(attrID,'H5ML_DEFAULT'));
        case 'add_offset'
            addOffset   = double(H5A.read(attrID,'H5ML_DEFAULT'));
        case '_fillvalue'
            fillValue   = double(H5A.read(attrID,'H5ML_DEFAULT'));
    end
    H5A.close(attrID);
end
H5G.close(gID);

nbPingTotal     = h5_dims(1);
nbPingToRead    = min([200 fix(nbPingTotal/10)]);
pingStep        = min([nbPingTotal nbPingToRead]);
nbPingStep      = fix(nbPingTotal/pingStep);
lastNbDataset   = mod(nbPingTotal,pingStep);

varDesc.dset_id     = dset_id;
varDesc.dspace_id   = dspace_id;
varDesc.scaleFactor = scaleFactor;
varDesc.fillValue   = fillValue;
varDesc.addOffset   = addOffset;
varDesc.dims        = h5_dims;

% Lecture par paquet de Ping
for iPS=1:nbPingStep
    idxPingStart    = pingStep*(iPS-1);
    idxPingEnd      = pingStep*(iPS)-1;

    [flag, dataSeabed] = fcnNested_extractPingFromSeabed(varDesc, nbPingToRead, idxPingStart, idxPingEnd);
    seabed_samples_r2(:, :, idxPingStart+1:idxPingEnd+1) = dataSeabed;
end
% Lecture du dernier paquet de Ping
if (idxPingEnd+1) < nbPingTotal
    % Traitement des N Ping padding hors step.
    idxPingStart    = pingStep*iPS;
    idxPingEnd      = nbPingTotal - 1;
    nbPingToRead    = idxPingEnd - idxPingStart + 1;
    [flag, dataSeabed] = fcnNested_extractPingFromSeabed(varDesc, nbPingToRead, idxPingStart, idxPingEnd);
    seabed_samples_r2(:, :, idxPingStart+1:idxPingEnd+1) = dataSeabed;
end

% Fermer les identifiants
H5S.close(dspace_id);
H5D.close(dset_id);
H5F.close(fid);
toc

%% Check de l'écart par les différents de lecture
deltaAmp    = -Inf;
nbBeams     = h5_dims(2);
for iBeam=1:nbBeams
    deltaAmp = max([deltaAmp max(abs(fix(squeeze(seabedOrig(:,iBeam,1)*10000))/10000 - ...
                            fix(squeeze(seabed_samples_r(:,iBeam,1)*10000))/10000))]);
end

%% --- Nested function

function [flag, dataSeabed] = fcnNested_extractPingFromSeabed(varDesc, nbPingToRead, idxPingStart, idxPingEnd)

dataSeabed  = [];
flag        = -1;
start_indices   = [idxPingStart, 0, 0];  % Indice de début du sous-ensemble
end_indices     = [idxPingEnd, varDesc.dims(2)-1, varDesc.dims(3)-1];  % Indice de fin du sous-ensemble
ndims           = fliplr(H5S.get_simple_extent_dims(varDesc.dspace_id));
H5S.select_hyperslab(varDesc.dspace_id, 'H5S_SELECT_SET', start_indices, [], end_indices - start_indices + 1, []);

% Créer une dataspace pour les données lues
data_space   = H5S.create_simple(length(start_indices), fliplr(end_indices - start_indices + 1), []);

% Lire les données du sous-ensemble
dataSeabed    = H5D.read(varDesc.dset_id, 'H5ML_DEFAULT', data_space, varDesc.dspace_id, 'H5P_DEFAULT');
% Transformation necessaire avec ce type de lecture, different de la
% lecture d'un seul bloc du dataset.
dataSeabed    = reshape(dataSeabed, [varDesc.dims(3) varDesc.dims(2) nbPingToRead]);
dataSeabed    = double(squeeze(dataSeabed));
dataSeabed(dataSeabed==varDesc.fillValue) = NaN;
dataSeabed   = dataSeabed * varDesc.scaleFactor + varDesc.addOffset;



flag = 0;

end
