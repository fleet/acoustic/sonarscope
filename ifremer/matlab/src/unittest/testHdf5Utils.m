%% Tests unitaires
import matlab.unittest.parameters.Parameter
import matlab.unittest.TestSuite

% Lancement des tests.
xsfGlobeFileName{1} = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\TestsFormatXsf\Globe-2.0.1\0003_20210616_121018.xsf.nc';
xsfGlobeFileName{2} = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\TestsFormatXsf\Globe-2.0.1\0039_20180905_222154_raw.xsf.nc';
xsfGlobeFileName{3} = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\From Marc Roche\0048_20221211_193145.xsf.nc';
for k=2:2 % numel(xsfGlobeFileName)
    [~, xsfDataRoot]        = Hdf5Utils.readGrpData(xsfGlobeFileName{k}, '/');
    
    param(1)          = Parameter.fromData('xsfGlobeFileName', xsfGlobeFileName(k));
    param(2)          = Parameter.fromData('hdf5Data',{xsfDataRoot});
    suiteTest         = TestSuite.fromClass(?XsfTest, 'ExternalParameters',param);
    results           = suiteTest.run;
end

%% 

xsfGlobeFileName    = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\From JMA\EM304\0023_20180628_122559_ShipNameFromAll.xsf.nc'; %#ok<*NASGU>
xsfGlobeFileName    = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\From JMA\EM304\0023_20180628_122559_ShipName.kmall.xsf.nc';

nomFicAll           = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\Tests_Format_Kongsberg\2040C\0005_20130131_173953_Pingeline.all';
xsfGlobeFileName    = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\Tests_Format_Kongsberg\2040C\0005_20130131_173953_Pingeline.xsf.nc';

nomFicKmAll         = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\Tests_Format_Kongsberg\KmAll\0011_EM2040_140_20160906_1358.kmall';
xsfGlobeFileName    = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\Tests_Format_Kongsberg\KmAll\0011_EM2040_140_20160906_1358.xsf.nc';

nomFicAll           = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\TestsFormatXsf\0039_20180905_222154.all';
xsfGlobeFileName    = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\TestsFormatXsf\0039_20180905_222154.xsf.nc';
nomFicAll           = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\TestsFormatXsf\0039_20180905_222154_raw.kmall';
xsfGlobeFileName    = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\TestsFormatXsf\0039_20180905_222154_raw.xsf.nc';

nomFicAll           = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\TestsFormatXsf\Globe-2.0.1\0003_20210616_121018.kmall';
xsfGlobeFileName    = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\TestsFormatXsf\Globe-2.0.1\0039_20180905_222154_raw.xsf.nc';
nomFicAll           = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\TestsFormatXsf\Globe-2.0.3\0003_20210616_121018.kmall';
nomFicAll           = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\TestsFormatXsf\Globe-2.0.3\0039_20180905_222154_raw.kmall';
xsfGlobeFileName    = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\TestsFormatXsf\Globe-2.0.3\0003_20210616_121018.xsf.nc';
xsfGlobeFileName    = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\TestsFormatXsf\Globe-2.0.3\0039_20180905_222154_raw.xsf.nc';

% Fichier de Marc Roche
nomFicAll           = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\From Marc Roche\0048_20221211_193145.kmall';
xsfGlobeFileName    = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\From Marc Roche\0048_20221211_193145.xsf.nc';

% Creation du Cache SSc si il n'existe pas.
[rootDir, rootFic, ~]   = fileparts(nomFicAll);
ncFileName              = fullfile(rootDir, 'SonarScope', [rootFic '.xsf.nc']);
if exist(ncFileName, 'file')
    DataSscNcPosition   = NetcdfUtils.Netcdf2Struct(ncFileName, 'Position');
    DataSscNcAttitude   = NetcdfUtils.Netcdf2Struct(ncFileName, 'Attitude');
    DataSscNcSSP        = NetcdfUtils.Netcdf2Struct(ncFileName, 'SoundSpeedProfile');
else
    a = cl_simrad_all('nomFic', nomFicAll);
end

h5Info = h5info(nomFicXsf);
[flag, sonarAtt]           = Hdf5Utils.readAtt(nomFicXsf, h5Info.Groups(end-1)); %#ok<*ASGLU>
[flag, Hdf5Data1]          = Hdf5Utils.readGrpData(nomFicXsf, '/');
[flag, Hdf5DataDataEnv]    = Hdf5Utils.readGrpData(nomFicXsf, '/Environment');
[flag, Hdf5DataSummary]    = Hdf5Utils.readGrpData(nomFicXsf, '/summary');
[flag, Hdf5DataDynDraught] = Hdf5Utils.readGrpData(nomFicXsf, '/Environment/Dynamic_draught');
[flag, Hdf5DataPos]        = Hdf5Utils.readGrpData(nomFicXsf, '/Platform/Position');
[flag, Hdf5DataSonar]      = Hdf5Utils.readGrpData(nomFicXsf, '/Sonar');
[flag, Hdf5DataDepth]      = Hdf5Utils.readGrpData(nomFicXsf, '/Sonar/Beam_group1');
[flag, DataDepth]          = Hdf5Utils.readDepth(Hdf5DataDepth);

PingCounter = Hdf5Utils.read_value(Hdf5Data1.Sonar.Beam_group1.Vendor_specific.ping_raw_count);
swath_along_position = Hdf5Utils.read_value(Hdf5Data1.Sonar.Beam_group1.Vendor_specific.swath_along_position);
[flag, DataPosition, indSystemUsed]     = Hdf5Utils.readPosition(Hdf5Data1.Platform, 'Memmapfile', -1);
DataPosition.EmModel                    = Hdf5Data1.summary.Att.model;

[flag, DataAttitude, indSystemUsed]     = Hdf5Utils.readAttitude(Hdf5Data1.Platform, 'Memmapfile', -1, 'SensorId', 1);
DataAttitude.EmModel                    = Hdf5Data1.summary.Att.model;
[flag, DataInstallParameters]           = Hdf5Utils.readInstallParams(Hdf5Data1.Platform);
[flag, DataDepth]                       = Hdf5Utils.readDepth(Hdf5Data1.Sonar.Beam_group1);
[flag, XML, Data]                       = XSF.extractRuntime(Hdf5Data1);
if ~flag
    return
end

[flag, DataRuntime]                     = Hdf5Utils.readRuntime(Hdf5Data1.Platform);
DataRuntime.EmModel                     = Hdf5Data1.summary.Att.model;
[flag, DataSSP]                         = Hdf5Utils.readSoundSpeedProfile(Hdf5Data1.Environment);
DataSSP.EmModel = Hdf5Data1.summary.Att.model;
% A comparer avec
[rootDir, rootFic, ~]   = fileparts(nomFicAll);
ncFileName              = fullfile(rootDir, 'SonarScope', [rootFic '.xsf.nc']);
if exist(ncFileName, 'file')
    DataSscNcPosition   = NetcdfUtils.Netcdf2Struct(ncFileName, 'Position');
    DataSscNcAttitude   = NetcdfUtils.Netcdf2Struct(ncFileName, 'Attitude');
    DataSscNcSSP        = NetcdfUtils.Netcdf2Struct(ncFileName, 'SoundSpeedProfile');
else
    flag = ALL_Cache2Netcdf(nomFicAll);
end

%% Tests des fichiers de tests unitaires du Convert de Globe
nomFicAll       = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\TestsFormatXsf\0039_20180905_222154.all';
nomFicAll       = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\TestsFormatXsf\0039_20180905_222154_raw.kmall';
aKM             = cl_simrad_all('nomFic', nomFicAll);
% [flag, DataRaw] = read_rawRangeBeamAngle(aKM, 'Memmapfile', -1);
% [flag, DataDepth] = read_depth_bin(aKM, 'Memmapfile', -1);
ReadOnly    = 1;
XMLOnly     = 0;  
[flag, DataDepth, VarNameInFailure] = SSc_ReadDatagrams(nomFicAll, 'Ssc_Depth', ...
    'XMLOnly', XMLOnly, 'Memmapfile', -1, 'ReadOnly', ReadOnly);

%% Test Memmapfile
nomFicXsf1       = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\TestsFormatXsf\0039_20180905_222154_raw.xsf.nc';
[flag, Hdf5Data1]    = Hdf5Utils.readGrpData(nomFicXsf1, '/', 'Memmapfile', -1);
[flag, Hdf5Data1]    = Hdf5Utils.readGrpData(nomFicXsf1, '/', 'Memmapfile', 0);

% Creation du cache NC depuis la lecture d'un KmALL (appelé par Ssc lors de
% l'importation d'un fichier KmAll).
[flag, flagRTK, nomFicWc, EmModel, SystemSerialNumber, Version, SousVersion] = KmallXsf2ssc(nomFicXsf);

%% Tests S7K
nomFicXsf   = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\TestsFormatXsf\20150902_041108_PP_7150_24kHz.xsf.nc';
[flag, Hdf5DataS7K]     = Hdf5Utils.readGrpData(nomFicXsf, '/');
[flag, DataEnv]         = Hdf5Utils.readGrpData(nomFicXsf, '/Environment');
[flag, DataDynDraught]  = Hdf5Utils.readGrpData(nomFicXsf, '/Environment/Dynamic_draught');

[flag, DataPosition, nbSensors, indSystemUsed]  = Hdf5Utils.readPosition(Hdf5DataS7K.Platform, 'Memmapfile', -1);
[flag, DataAttitude, nbSensors, indSystemUsed]  = Hdf5Utils.readAttitude(Hdf5DataS7K, 'Memmapfile', -1);
[flag, DataInstallParameters]                   = Hdf5Utils.readInstallParams(Hdf5DataS7K.Platform);
[flag, DataDepth]                               = Hdf5Utils.readDepth(Hdf5DataS7K.Sonar.Beam_group1.Bathymetry);
[flag, DataRuntime]                             = Hdf5Utils.readRuntime(Hdf5DataS7K.Platform);

%% Test fichier Ridha (Globe 2.3.3, Seabed passage des Snippets Seabed avec la version 2.3.3 et chgt de format 
% (passage de VLen à NPing x NBeams X NDetections).
 
nomFicXsf          = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\From Ridha\0134_20221201_194727.xsf.nc';
[flag, Hdf5Data1]  = Hdf5Utils.readGrpData(nomFicXsf, '/');

%% Test de la lecture par Chunk des data Seabed trop volumineuse à charger en matrices 3D.

clear nomFicXsf
dsName              = '/Sonar/Beam_group1/Bathymetry/seabed_image_samples_r';
nomFicXsf{1}        = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\TestsFormatXsf\0039_20180905_222154_raw.xsf.nc';
nomFicXsf{end+1}    = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\TestsFormatXsf\0048_20221211_193145.xsf.nc';
nomFicXsf{end+1}    = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\From Marc Roche\kmall\0004_20230824_010208.xsf.nc';
nomFicXsf{end+1}    = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\From Marc Roche\kmall\0002_20221011_174216.xsf.nc';
nomFicXsf{end+1}    = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\From Marc Roche\kmall\SimonStevin_2023-11\0002_20231202_175436.xsf.nc';
for f=1:3% numel(nomFicXsf)
    [flag, Hdf5Data]    = Hdf5Utils.readGrpData(nomFicXsf{f}, '/');
    
    tic
    X = XSF.getSeabedImageFromFile(Hdf5Data);
    toc
    tic
    X2 = XSF.getSeabedImageFromChunk(Hdf5Data.fileName, dsName);
    toc
    whos X X2
    % % if isfield(Hdf5Data.Sonar.Beam_group1.Bathymetry, 'seabed_image_samples_r')
    % %     % Lecture des Seabed a ete faite prealablement de façon directe et unique via le fichier.
    % %     X = XSF.getSeabedImageFromFile(Hdf5Data);
    % % else
    % %     % Lecture des Seabed par chunk pour eviter de charger au maximum la
    % %     % memoire.
    % %     X = XSF.getSeabedImageFromChunk(Hdf5Data.fileName, dsName);
    % % end
end
% seabedOrig          = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Bathymetry.seabed_image_samples_r);
[flag, DataRuntime] = Hdf5Utils.readRuntime(Hdf5Data.Platform);

[flag, XML, DataRuntimeXSF] = XSF.extractRuntime(Hdf5Data);
[flag, XML, DataSeabed]     = XSF.extractSeabedImage(Hdf5Data, DataRuntimeXSF);

%%
nomFicXsf = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\From Marc Roche\kmall\SimonStevin_2023-11\0001_20231202_173740.xsf.nc';
[flag, Hdf5Data]    = Hdf5Utils.readGrpData(nomFicXsf, '/');

tic
XNew = XSF.getSeabedImageFromFile(Hdf5Data);
toc

seabedImage = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Bathymetry.seabed_image_samples_r);
% Les données sont en NPings x N Detections (Beams) x N Samples. On
% les convertit en Cell pour un traitement calqué sur le fctt VLen.
tmp = permute(seabedImage, [2 1 3]);
tmp = squeeze(num2cell(tmp,2));
XOld   = tmp';
XOld   = cellfun(@(x) x', XOld, 'UniformOutput', false);

% Suppression du 0 Padding (conservation des valeurs négatives).
%     XDummy = [];
[n1,n2] = size(XOld);
XDummy = cell(n1,n2);
for n=1:n1
    for k=1:n2
        XDummy{n,k} = XOld{n,k}(XOld{n,k} < 0);
    end
end
XOld = XDummy;