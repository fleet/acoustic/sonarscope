%% Parsing Ssc
nomFicAll   = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\From JMA\EM304\0023_20180628_122559_ShipName.all';
flag        = ALL_Cache2Netcdf(nomFicAll);

dataFileName        = getNomFicDatabase('0011_20110307_211804_IKATERE_02.all');
[flag, Data, Bin]   = NetcdfUtils.readGrpData(nomFicAll, 'Attitude');
[flag, Data]        = SScCacheNetcdfUtils.readWholeData(nomFicAll, 'Ssc_Depth');
[flag, Data]        = SScCacheNetcdfUtils.readWholeData(nomFicAll, 'Ssc_Attitude'); 

%% Parsing xsf
nomFicXsf       = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\From JMA\EM304\0023_20180628_122559_ShipName.xsf.nc';
[flag, Data]    = SScCacheNetcdfUtils.readWholeData(nomFicXsf, 'Environment');


