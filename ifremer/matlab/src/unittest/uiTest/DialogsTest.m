classdef DialogsTest < matlab.unittest.TestCase
    % Description
    %   DialogsTest Unit Test for several dialogs
    %
    % Laucnh test :
    %   res = run(DialogsTest)
    %
    % Output Arguments
    %   res : atlab.unittest.TestResult instance
    %
    % Examples
    %   res = run(DialogsTest);
    %   rt = table(res)
    %
    % Authors  : MHO
    % See also ParametreDialog ModelDialog DemoPanelComponentsDialog
    %            DemoTabDialog ClParametreTest
    % ----------------------------------------------------------------------------
    
    properties
    end
    
    methods (TestMethodSetup)
        function createDialogs(this) %#ok<MANU>
            % nothing to do here...
        end
    end
    
    methods(TestMethodTeardown)
        function closeDialogsTest(this) %#ok<MANU>
            % nothing to do here...
        end
    end
    
    methods (Test)
        function testParametreDialog(this)
            % Create a ParametreDialog
            parametre(1) = ClParametre('Name', 'Temperature1', 'Unit', 'deg', 'MinValue', -10, 'MaxValue', 50, 'Value', 20);
            parametre(2) = ClParametre('Name', 'Temperature2', 'Unit', 'deg', 'MinValue', -10, 'MaxValue', 50, 'Value', 20);
            paramDialog = ParametreDialog('params', parametre, 'Title', 'test', 'WaitAnswer', false);
            
            % Test OK with ParametreDialog
            paramDialog.openDialog();
            paramDialog.okPressed();
            expectedValue = 1;
            this.verifyEqual(expectedValue, paramDialog.okPressedOut, sprintf('okPressedOut expected : %d, but was %d', ...
                expectedValue, paramDialog.okPressedOut));
        end
        
        function testSimpleParametreDialog(this)
            % Create a SimpleParametreDialog
            parametre(1) = ClParametre('Name', 'Temperature1', 'Unit', 'deg', 'MinValue', -10, 'MaxValue', 50, 'Value', 20);
            parametre(2) = ClParametre('Name', 'Temperature2', 'Unit', 'deg', 'MinValue', -10, 'MaxValue', 50, 'Value', 20);
            simpleParamDialog = SimpleParametreDialog('params', parametre, 'Title', 'test', 'WaitAnswer', false);
            
            % Test OK with SimpleParametreDialog
            simpleParamDialog.openDialog();
            simpleParamDialog.okPressed();
            expectedValue = 1;
            this.verifyEqual(expectedValue, simpleParamDialog.okPressedOut, sprintf('okPressedOut expected : %d, but was %d', ...
                expectedValue, simpleParamDialog.okPressedOut));
        end
        
        function testModelDialog(this)
            % Create a ModelDialog
            model = BSLurtonModel('XData', -80:80);
            model = model.noise('Coeff', 5);
            modelDialog = ModelDialog('This is a demonstration of ModelDialog', model);
            paramDialog = modelDialog.paramDialog;
            paramDialog.waitAnswer = false;
            
            % Test OK with ModelDialog
            modelDialog.openDialog();
            paramDialog.okPressed();
            expectedValue = 1;
            this.verifyEqual(expectedValue, modelDialog.paramDialog.okPressedOut, sprintf('okPressedOut expected : %d, but was %d', ...
                expectedValue, paramDialog.okPressedOut));
        end
        
        function testDemoComponentsDialog(this)
            % Create a DemoComponentsDialog
            demoComponentsDialog = DemoComponentsDialog();
            demoComponentsDialog.waitAnswer = false;
            
            % Test OK with DemoComponentsDialog
            demoComponentsDialog.openDialog();
            demoComponentsDialog.okPressed();
            expectedValue = 1;
            this.verifyEqual(expectedValue, demoComponentsDialog.okPressedOut, sprintf('okPressedOut expected : %d, but was %d', ...
                expectedValue, demoComponentsDialog.okPressedOut));
        end
        
        function testDemoPanelComponentsDialog(this)
            % Create a DemoPanelComponentsDialog
            demoPanelComponentsDialog = DemoPanelComponentsDialog();
            demoPanelComponentsDialog.waitAnswer = false;
            
            % Test OK with DemoPanelComponentsDialog
            demoPanelComponentsDialog.openDialog();
            demoPanelComponentsDialog.okPressed();
            expectedValue = 1;
            this.verifyEqual(expectedValue, demoPanelComponentsDialog.okPressedOut, sprintf('okPressedOut expected : %d, but was %d', ...
                expectedValue, demoPanelComponentsDialog.okPressedOut));
        end
        
        function testDemoTabDialog(this)
            % Create a DemoTabDialog
            demoTabDialog = DemoTabDialog();
            demoTabDialog.waitAnswer = false;
            
            % Test OK with DemoTabDialog
            demoTabDialog.openDialog();
            demoTabDialog.okPressed();
            expectedValue = 1;
            this.verifyEqual(expectedValue, demoTabDialog.okPressedOut, sprintf('okPressedOut expected : %d, but was %d', ...
                expectedValue, demoTabDialog.okPressedOut));
        end
    end
end
