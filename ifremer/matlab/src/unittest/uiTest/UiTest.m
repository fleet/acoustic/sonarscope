classdef UiTest < matlab.unittest.TestCase
    % Description
    %   UiTest Unit Test
    %
    % Laucnh test :
    %   res = run(UiTest)
    %
    % Output Arguments
    %   res : atlab.unittest.TestResult instance
    %
    % Examples
    %   res = run(UiTest);
    %   rt = table(res)
    %
    % Authors : MHO
    % See also ClSignal DialogsTest
    % ----------------------------------------------------------------------------
    
    properties
    end
    
    methods (Test)
        function testDialogsTest(this)
            res = run(DialogsTest);
            isAllPassed = all([res.Passed]);
            this.verifyTrue(isAllPassed, 'DialogsTest Faile');
        end
    end
end
