classdef DemoCheckboxUiDialog < handle & SimpleTitleUiDialog
    % Description
    %   Class DemoCheckboxUiDialog which is used to display a set of example components
    %
    % Syntax
    %   a = DemoCheckboxUiDialog(...)
    %
    % Output Arguments
    %   s : One DemoCheckboxUiDialog instance
    %
    % Examples
    %   a = DemoCheckboxUiDialog();
    %   a.openDialog;
    %
    % Authors : MHO
    % See also ParametreComponent ClModel Authors
    % ----------------------------------------------------------------------------
    
    properties
        logicalList  = []
        checkboxList = []
    end
    
    properties (Access = private)
        LogicalStr = {'false', 'true'};
    end
    
    events
    end
    
    methods
        function this = DemoCheckboxUiDialog(varargin)
            % Superclass Constructor
            this = this@SimpleTitleUiDialog(varargin{:});
            
            this.Title = 'Demo checkbox components';
            
            this.logicalList(1) = true;
            this.logicalList(2) = false;
            this.logicalList(3) = true;
            this.logicalList(4) = false;
        end 
    end
    
    methods (Access = protected)
        function [widthBody ,heightBody] = createBody(this, parent)
            % Create ParametreComponent List
            
            gridlayout = uigridlayout(parent, 'ColumnSpacing', 1, 'RowSpacing', 1, 'Padding', 1);
            gridlayout.ColumnWidth = {'1x'};
            
            for index = 1:length(this.logicalList)
                this.checkboxList(index) = uicheckbox(gridlayout, ...
                    'Text',            ['test' num2str(index)], ...
                    'Value',           this.logicalList(index), ...
                    'Tooltip',         'test tooltip', ...
                    'ValueChangedFcn', @this.enableCallback);
            end
            
            widthBody  = 600;
            heightBody = 25 * length(this.logicalList);
        end
        
        function enableCallback(this, src, ~)
            % Callback enable qui enable les uicontrols du composant
            
            for index = 1:length(this.logicalList)
                if (src == this.checkboxList(index))
                    checkbox = src;
                    break;
                end
            end
            
            this.logicalList(index) = checkbox.Value;
            
            fprintf([checkbox.Text ' : ' this.LogicalStr{checkbox.Value + 1} '\n']);
        end
    end
end
