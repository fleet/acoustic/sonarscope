classdef DemoComponentsDialog < handle & SimpleTitleDialog
    % Description
    %   Class DemoComponentsDialog which is used to display a set of example components
    %
    % Syntax
    %   a = DemoComponentsDialog(...)
    %
    % Output Arguments
    %   s : One ParametreDialog instance
    %
    % Examples
    %   a = DemoComponentsDialog();
    %   a.openDialog;
    %   % Output :
    %     a.okPressedOut %(0 if not pressed, 1 if pressed)
    %     a.params
    %     a.popupmenuOut
    %     a.monthOut
    %     a.yearOut
    %     a.dateOut
    %
    %   style1 = ColorAndFontStyle('BackgroundColor', [0.7 0.7 1], 'ForegroundColor', 'k', 'FontWeight', 'bold', 'FontSize', 13, 'FontAngle', 'italic');
    %   a.titleStyle = style1;
    %   a.openDialog;
    %
    % Authors : MHO
    % See also ParametreComponent ClModel Authors
    % ----------------------------------------------------------------------------
    
    properties
        params = ClParametre.empty % - ClParametre array
        % output
        popupmenuOut
        radioButtonGroupOut
        monthOut
        yearOut
        dateOut
    end
    
    properties (Access = private)
        paramComponentList = ParametreComponent.empty; % - ParametreComponent array
        popupmenu
        radioButtonGroup
        months
        calendar
        jhSpinnerM
        jhSpinnerY
        jhSpinnerD
        contextMenuItemValue = 1;
        contextMenu
        contextMenuItem1
        contextMenuItem2
        contextMenuItem3
    end
    
    events
        OneParamValueChange % event if one param value change
    end
    
    methods
        function this = DemoComponentsDialog(varargin)
            % Superclass Constructor
            this = this@SimpleTitleDialog(varargin{:});
            
            this.Help =  'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope'; %option
            this.Title = 'Demo components';
            
            this.params(1) = ClParametre('Name', 'Temperature 1', 'Value', 0);
            this.params(2) = ClParametre('Name', 'Temperature 2', 'Value', 0);
            this.params(3) = ClParametre('Name', 'Temperature 3', 'Value', 0, 'Unit', 'deg');
            this.params(4) = ClParametre('Name', 'Temperature 4', 'Value', 0, 'Unit', 'deg');
            this.params(5) = ClParametre('Name', 'Temperature 5', 'Value', 0, 'Help', 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope');
            this.params(6) = ClParametre('Name', 'Temperature 6', 'Value', 0, 'Unit', 'deg', 'Help', 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope');
        end
        
        function handleValueChange(this, ~, ~)
            % Send one value change event
            this.notify('OneParamValueChange');
        end
        
        function okPressed(this, ~, ~)
            % Save function : set the Output then close dialog
            
            this.popupmenuOut        = this.popupmenu.Value;
            this.radioButtonGroupOut = this.radioButtonGroup.SelectedObject.String;
            
            % Close the dialog
            okPressed@SimpleTitleDialog(this);
        end
    end
    
    methods (Access = protected)
        
        function [widthBody ,heightBody] = createBody(this, parent)
            verticalBox = uiextras.VBox('Parent', parent);
            
            %% Create ParametreComponent List
            
            lengthComponents = 0;
            
            this.paramComponentList(1) = ParametreComponent(verticalBox, 'param', this.params(1), 'Sizes', [25 -0.95 0 0 0 -1 0 0]);
            % add listener on this.paramComponentList(1).
            % If this.paramComponentList(1) send the event 'ValueChange', this
            % triggers the method 'handleValueChange'
            addlistener(this.paramComponentList(1), 'ValueChange', @this.handleValueChange); % listener example
            lengthComponents = lengthComponents + 1;
            
            style2 = ColorAndFontStyle('BackgroundColor', [1 0.8 0.6], 'ForegroundColor', 'k', 'FontWeight', 'bold');
            this.paramComponentList(2) = ParametreComponent(verticalBox, 'param', this.params(2), 'Sizes', [0 -1 0 0 0 -1 0 0], ...
                'style', style2);
            lengthComponents = lengthComponents + 1;
            
            this.paramComponentList(3) = ParametreComponent(verticalBox, 'param', this.params(3), 'Sizes', [0 -1 0 0 0 -1 -1 0]);
            lengthComponents = lengthComponents + 1;
            
            this.paramComponentList(4) = SpinnerParametreComponent(verticalBox, 'param', this.params(4), 'Sizes', [-1 -2 -1 0]);
            lengthComponents = lengthComponents + 1;
            
            this.paramComponentList(5) = SpinnerParametreComponent(verticalBox, 'param', this.params(5), 'Sizes', [0 -1 0 25]);
            lengthComponents = lengthComponents + 1;
            
            this.paramComponentList(6) = SimpleParametreComponent(verticalBox, 'param', this.params(6), 'Sizes', [-1 -1 -1 25]);
            lengthComponents = lengthComponents + 1;
            
            %% POPUPMENU COMPONENT
            
            popupComponent = uiextras.HBox('Parent', verticalBox);
            
            uicontrol('Parent', popupComponent, ...
                'Style',  'text', ...
                'string', 'Select option : ');
            
            this.popupmenu = uicontrol('parent', popupComponent, ...
                'Style',    'popupmenu', ...
                'String',   {'option1', 'option2', 'option3'}, ...
                'Value',    2, ...
                'Callback', @this.popupmenuCallback);
            popupComponent.set('Sizes', [-1 -4]);
            lengthComponents = lengthComponents + 1;
            
            %% RADIOMENU COMPONENT
            
            this.radioButtonGroup = uibuttongroup('Parent', verticalBox, ...
                'Title', 'radioButtonGroup', ...
                'SelectionChangedFcn', @this.radioButtonGroupCallback);
            
            % Create three radio buttons in the button group.
            uicontrol(this.radioButtonGroup, ...
                'Style',    'radiobutton', ...
                'String',   'Option 1', ...
                'Units',    'normalized', ...
                'Position', [0 0 1/3 1]);
            
            uicontrol(this.radioButtonGroup, ...
                'Style',    'radiobutton', ...
                'String',   '<html><b>Option 2</b>', ...
                'Units',    'normalized', ...
                'Position', [1/3 0 1/3 1]);
            
            uicontrol(this.radioButtonGroup, ...
                'Style',    'radiobutton', ...
                'String',   'Option 3', ...
                'Units',    'normalized', ...
                'Position', [2/3 0 1/3 1]);
            
            lengthComponents = lengthComponents + 1;
            
            %% DROPDOWN BUTTON COMPONENT
            
            dropdownComponent = uiextras.HBox('Parent', verticalBox);
            % ContextMenu
            this.contextMenu = uicontextmenu(gcf);
            menuItemChecked = {'off', 'off', 'off'};
            menuItemChecked{this.contextMenuItemValue} = 'on';
            this.contextMenuItem1 = uimenu(this.contextMenu,'Text','Item1','Checked', menuItemChecked{1},'Callback',@this.contextMenuItemCallback);
            this.contextMenuItem2 = uimenu(this.contextMenu,'Text','Item2','Checked', menuItemChecked{2},'Callback',@this.contextMenuItemCallback);
            this.contextMenuItem3 = uimenu(this.contextMenu,'Text','Item3','Checked', menuItemChecked{3},'Callback',@this.contextMenuItemCallback);
            % DropDown bouton
            DropDownButtonComponent(dropdownComponent, gcf, 'DropDownButton', @this.dropdownButtonCallback, this.contextMenu);
            lengthComponents = lengthComponents + 1;
            dropdownComponent.set('Sizes', 200);
            
            % Repartition
            verticalBox.set('Sizes', [-1 -1 -1 -1 -1 -1 -1 -1.5 -1]);
            verticalBox.set('Spacing', 1);
            
            widthBody = 600;
            heightBody = 25 * lengthComponents;
        end
        
        function popupmenuCallback(this, src, ~) %#ok<INUSL>
            fprintf('PopupMenu choice :\n');
            fprintf('%s\n', src.String{src.Value});
        end
        
        function radioButtonGroupCallback(this, source, callbackdata) %#ok<INUSL>
            fprintf(['Previous: ' callbackdata.OldValue.String '\n']);
            fprintf(['Current: '  callbackdata.NewValue.String '\n']);
            fprintf('------------------\n');
        end
        
        function contextMenuItemCallback(this, src, ~)
            % contextMenuItem Callback
            this.contextMenuItem1.set('Checked', 'off');
            this.contextMenuItem2.set('Checked', 'off');
            this.contextMenuItem3.set('Checked', 'off');
            
            if src == this.contextMenuItem1
                this.contextMenuItemValue = 1;
                this.contextMenuItem1.set('Checked', 'on');
            elseif src == this.contextMenuItem2
                this.contextMenuItemValue = 2;
                this.contextMenuItem2.set('Checked', 'on');
            elseif src == this.contextMenuItem3
                this.contextMenuItemValue = 3;
                this.contextMenuItem3.set('Checked', 'on');
            end
            
            this.dropdownButtonCallback();
        end
        
        function dropdownButtonCallback(this, ~, ~)
            disp(['DropdownButton / contemenuItem' num2str(this.contextMenuItemValue)]);
        end
    end
end
