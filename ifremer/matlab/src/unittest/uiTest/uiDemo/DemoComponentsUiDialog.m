classdef DemoComponentsUiDialog < handle & SimpleTitleUiDialog
    % Description
    %   Class DemoComponentsUiDialog which is used to display a set of example components
    %
    % Syntax
    %   a = DemoComponentsUiDialog(...)
    %
    % Output Arguments
    %   s : One DemoComponentsUiDialog instance
    %
    % Examples
    %   a = DemoComponentsUiDialog();
    %   a.openDialog;
    %   % Output :
    %     a.okPressedOut %(0 if not pressed, 1 if pressed)
    %     a.params
    %     a.popupmenuOut
    %     a.monthOut
    %     a.yearOut
    %     a.dateOut
    %
    %   style1 = ColorAndFontStyle('BackgroundColor', [0.7 0.7 1], 'ForegroundColor', 'k', 'FontWeight', 'bold', 'FontSize', 13, 'FontAngle', 'italic');
    %   a.titleStyle = style1;
    %   a.openDialog;
    %
    % Authors : MHO
    % See also ParametreComponent ClModel Authors
    % ----------------------------------------------------------------------------
    
    properties
        params = ClParametre.empty % - ClParametre array
        % output
        dropdownComponentOut
        radioButtonGroupOut
    end
    
    properties (Access = private)
        paramComponentList = ParametreUiComponent.empty; % - ParametreUiComponent array
        spinnerParamComponentList = SpinnerParametreUiComponent.empty; % - SpinnerParametreUiComponent array
        simpleParamComponentList = SimpleParametreUiComponent.empty; % - SimpleParametreUiComponent array
        dropdownComponent
        radioButtonGroup
        
        contextMenuItemValue = 1;
        contextMenu
        contextMenuItem1
        contextMenuItem2
        contextMenuItem3
    end
    
    events
        OneParamValueChange % event if one param value change
    end
    
    methods
        function this = DemoComponentsUiDialog(varargin)
            % Superclass Constructor
            this = this@SimpleTitleUiDialog(varargin{:});
            
            this.Help =  'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope'; %option
            this.Title = 'Demo components';
            
            this.params(1) = ClParametre('Name', 'Temperature 1',  'MinValue', -1, 'MaxValue', 1, 'Value', 0);
            this.params(2) = ClParametre('Name', 'Temperature 2',  'MinValue', -1, 'MaxValue', 1, 'Value', 0);
            this.params(3) = ClParametre('Name', 'Temperature 3',  'MinValue', -1, 'MaxValue', 1, 'Value', 0, 'Unit', 'deg');
            this.params(4) = ClParametre('Name', 'Temperature 4',  'Value', 0, 'Unit', 'deg');
            this.params(5) = ClParametre('Name', 'Temperature 5',  'Value', 0, 'Help', 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope');
            this.params(6) = ClParametre('Name', 'Temperature 6',  'Value', 0, 'Unit', 'deg', 'Help', 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope');
        end
        
        function handleValueChange(this, ~, ~)
            % Send one value change event
            this.notify('OneParamValueChange');
        end
        
        function okPressed(this, ~, ~)
            % Save function : set the Output then close dialog
            
            this.dropdownComponentOut        = this.dropdownComponent.Value;
            this.radioButtonGroupOut = this.radioButtonGroup.SelectedObject.Text ;
            
            % Close the dialog
            okPressed@SimpleTitleUiDialog(this);
        end
    end
    
    methods (Access = protected)
        
        function [widthBody ,heightBody] = createBody(this, parent)
            verticalGridlayout = uigridlayout(parent, 'ColumnSpacing', 1, 'RowSpacing', 1, 'Padding', 0);
            verticalGridlayout.ColumnWidth = {'1x'};
            
            %% Create ParametreComponent List
            
            lengthComponents = 0;
            
            this.paramComponentList(1) = ParametreUiComponent(verticalGridlayout, 'param', this.params(1), 'Sizes', {25, '0.95x', 0, 0, 0, '1x', 0, 0});
            % add listener on this.paramComponentList(1).
            % If this.paramComponentList(1) send the event 'ValueChange', this
            % triggers the method 'handleValueChange'
            addlistener(this.paramComponentList(1), 'ValueChange', @this.handleValueChange); % listener example
            lengthComponents = lengthComponents + 1;
            
            style2 = ColorAndFontStyle('BackgroundColor', [1 0.8 0.6], 'ForegroundColor', 'k', 'FontWeight', 'bold');
            this.paramComponentList(2) = ParametreUiComponent(verticalGridlayout, 'param', this.params(2), 'Sizes', {0, '1x', 0, 0, 0, '1x', 0, 0}, ...
                'style', style2);
            lengthComponents = lengthComponents + 1;
            
            this.paramComponentList(3) = ParametreUiComponent(verticalGridlayout, 'param', this.params(3), 'Sizes', {0, '1x', 0, 0, 0, '1x', '1x', 0});
            lengthComponents = lengthComponents + 1;
            
            this.spinnerParamComponentList(1) = SpinnerParametreUiComponent(verticalGridlayout, 'param', this.params(4), 'Sizes', {'1x', '2x', '1x', 0});
            lengthComponents = lengthComponents + 1;
            
            this.spinnerParamComponentList(2) = SpinnerParametreUiComponent(verticalGridlayout, 'param', this.params(5), 'Sizes', {0, '1x', 0, 25});
            lengthComponents = lengthComponents + 1;
            
            this.simpleParamComponentList(1) = SimpleParametreUiComponent(verticalGridlayout, 'param', this.params(6), 'Sizes', {'1x', '1x', '1x', 25});
            lengthComponents = lengthComponents + 1;
            
            %% Dropdown COMPONENT
            dropdownGridlayout = uigridlayout(verticalGridlayout, 'ColumnSpacing', 0, 'RowSpacing', 0, 'Padding', 0);
            dropdownGridlayout.ColumnWidth = {'1x', '4x'};
            
            uilabel(dropdownGridlayout, ...
                'Text',  'Select option : ');
            
            this.dropdownComponent = uidropdown(dropdownGridlayout, ...
                'Items',{'option1','option2','option3'},...
                'Value','option2', ...
                'ValueChangedFcn', @this.dropdownCallback);
            
            lengthComponents = lengthComponents + 1;
            
            %% RADIOMENU COMPONENT
            
            this.radioButtonGroup = uibuttongroup(verticalGridlayout, ...
                'Title', 'radioButtonGroup', ...
                'SelectionChangedFcn', @this.radioButtonGroupCallback);
            
            % Create three radio buttons in the button group.
            uiradiobutton(this.radioButtonGroup, 'Text', 'Option 1', 'Position', [0 0 100 25]);
            uiradiobutton(this.radioButtonGroup, 'Text', 'Option 2', 'Position', [100 0 100 25]);
            uiradiobutton(this.radioButtonGroup, 'Text', 'Option 3', 'Position', [200 0 100 25]);
            
            
            lengthComponents = lengthComponents + 1;
            
            %% DROPDOWN BUTTON COMPONENT
            
            dropdownButtonGridlayout = uigridlayout(verticalGridlayout, 'ColumnSpacing', 0, 'RowSpacing', 0, 'Padding', 0);
            dropdownButtonGridlayout.ColumnWidth = {200};
            
            % ContextMenu
            this.contextMenu = uicontextmenu(this.parentFigure);
            menuItemChecked = {'off', 'off', 'off'};
            menuItemChecked{this.contextMenuItemValue} = 'on';
            this.contextMenuItem1 = uimenu(this.contextMenu, 'Text', 'Item1','Checked', menuItemChecked{1},'Callback',@this.contextMenuItemCallback);
            this.contextMenuItem2 = uimenu(this.contextMenu, 'Text', 'Item2','Checked', menuItemChecked{2},'Callback',@this.contextMenuItemCallback);
            this.contextMenuItem3 = uimenu(this.contextMenu, 'Text', 'Item3','Checked', menuItemChecked{3},'Callback',@this.contextMenuItemCallback);
            % DropDown bouton
            DropDownButtonUiComponent(dropdownButtonGridlayout, this.parentFigure, 'DropDownButton', @this.dropdownButtonCallback, this.contextMenu);
            
  
            lengthComponents = lengthComponents + 1;
            
            % Repartition
            verticalGridlayout.RowHeight ={'1x', '1x', '1x', '1x', '1x', '1x', '1x', '2x', '2x'};
 
            
            widthBody = 600;
            heightBody = 30 * lengthComponents;
        end
        
        function dropdownCallback(this, src, ~) %#ok<INUSL>
            fprintf('DropDown choice :\n');
            fprintf('%s\n', src.Value);
        end
        
        function radioButtonGroupCallback(this, source, event) %#ok<INUSL>
            % Algo Type Callback
            fprintf(['Previous: ' event.OldValue.Text '\n']);
            fprintf(['Current: '  event.NewValue.Text '\n']);
            fprintf('------------------\n');
        end
        
        function contextMenuItemCallback(this, src, ~)
            % contextMenuItem Callback
            this.contextMenuItem1.set('Checked', 'off');
            this.contextMenuItem2.set('Checked', 'off');
            this.contextMenuItem3.set('Checked', 'off');
            
            if src == this.contextMenuItem1
                this.contextMenuItemValue = 1;
                this.contextMenuItem1.set('Checked', 'on');
            elseif src == this.contextMenuItem2
                this.contextMenuItemValue = 2;
                this.contextMenuItem2.set('Checked', 'on');
            elseif src == this.contextMenuItem3
                this.contextMenuItemValue = 3;
                this.contextMenuItem3.set('Checked', 'on');
            end
            
            this.dropdownButtonCallback();
        end
        
        function dropdownButtonCallback(this, ~, ~)
            disp(['DropdownButton / contemenuItem' num2str(this.contextMenuItemValue)]);
        end
    end
end
