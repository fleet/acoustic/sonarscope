classdef DemoDropDownButtonComponent < handle & SimpleTitleDialog
    % Description
    %   Class DemoDropDownButtonComponent which is used to display a drop down button component
    %
    % Syntax
    %   a = DemoDropDownButtonComponent(...)
    %
    % Output Arguments
    %   s : One DemoDropDownButtonComponent instance
    %
    % Examples
    %   a = DemoDropDownButtonComponent();
    %   a.openDialog;
    %   % Output :
    %     a.okPressedOut %(0 if not pressed, 1 if pressed)
    %
    %
    % Authors : MHO
    % See also DropDownButtonComponent  Authors
    % ----------------------------------------------------------------------------
    
    properties (Access = private)
        contextMenuItemValue = 1;
        contextMenu
        contextMenuItem1
        contextMenuItem2
        contextMenuItem3
    end
    
    methods
        function this = DemoDropDownButtonComponent(varargin)
            % Superclass Constructor
            this = this@SimpleTitleDialog(varargin{:});
            
            this.Help  = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope'; %option
            this.Title = 'Demo DropDown components';
        end
    end
    
    methods (Access = protected)
        
        function [widthBody, heightBody] = createBody(this, parent)
            
            %% DROPDOWN BUTTON COMPONENT
            
            dropdownComponent = uiextras.HBox('Parent', parent);
            % ContextMenu
            this.contextMenu = uicontextmenu(gcf);
            menuItemChecked = {'off', 'off', 'off'};
            menuItemChecked{this.contextMenuItemValue} = 'on';
            this.contextMenuItem1 = uimenu(this.contextMenu,'Text','Item1','Checked', menuItemChecked{1},'Callback',@this.contextMenuItemCallback);
            this.contextMenuItem2 = uimenu(this.contextMenu,'Text','Item2','Checked', menuItemChecked{2},'Callback',@this.contextMenuItemCallback);
            this.contextMenuItem3 = uimenu(this.contextMenu,'Text','Item3','Checked', menuItemChecked{3},'Callback',@this.contextMenuItemCallback);
            % DropDown bouton
            DropDownButtonComponent(dropdownComponent, gcf, 'DropDownButton', @this.dropdownButtonCallback, this.contextMenu);
            dropdownComponent.set('Sizes', 200);
            
            widthBody = 230;
            heightBody = 25;
        end
        
        function contextMenuItemCallback(this, src, ~)
            % contextMenuItem Callback
            this.contextMenuItem1.set('Checked', 'off');
            this.contextMenuItem2.set('Checked', 'off');
            this.contextMenuItem3.set('Checked', 'off');
            
            if src == this.contextMenuItem1
                this.contextMenuItemValue = 1;
                this.contextMenuItem1.set('Checked', 'on');
            elseif src == this.contextMenuItem2
                this.contextMenuItemValue = 2;
                this.contextMenuItem2.set('Checked', 'on');
            elseif src == this.contextMenuItem3
                this.contextMenuItemValue = 3;
                this.contextMenuItem3.set('Checked', 'on');
            end
            
            this.dropdownButtonCallback();
        end
        
        function dropdownButtonCallback(this, ~, ~)
            disp(['DropdownButton / contemenuItem' num2str(this.contextMenuItemValue)]);
        end
    end
end
