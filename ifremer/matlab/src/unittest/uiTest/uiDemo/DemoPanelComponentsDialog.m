classdef DemoPanelComponentsDialog < handle & SimpleTitleDialog
    % Description
    %   Class DemoPanelComponentsDialog wich is used to display a set of example components
    %
    % Syntax
    %   a = DemoPanelComponentsDialog(...)
    %
    % Output Arguments
    %   s : One ParametreDialog instance
    %
    % Examples
    %   a = DemoPanelComponentsDialog();
    %   a.openDialog;
    %   % Output :
    %     a.okPressedOut % (0 if not pressed, 1 if pressed)
    %     a.popupmenuOut
    %     a.monthOut
    %     a.yearOut
    %     a.dateOut
    %
    % Authors : MHO
    % See also ParametreComponent ClModel Authors
    % ----------------------------------------------------------------------------
    
    properties
        params = ClParametre.empty % - ClParametre array
        % output
        popupmenuOut
        radioButtonGroupOut
        monthOut
        yearOut
        dateOut
    end
    
    properties (Access = private)
        paramComponentList = ParametreComponent.empty; % - ParametreComponent array
        popupmenu
        radioButtonGroup
        months
        calendar
        jhSpinnerM
        jhSpinnerY
        jhSpinnerD
    end
    
    events
        OneParamValueChange % event if one param value change
    end
    
    methods
        function this = DemoPanelComponentsDialog(varargin)
            % Superclass Constructor
            this = this@SimpleTitleDialog(varargin{:});
            
            this.Help =  'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope'; %option
            this.Title = 'Demo panels components';
            
            this.params(1) = ClParametre('Name', 'Temperature 1',  'Value', 0);
            this.params(2) = ClParametre('Name', 'Temperature 2',  'Value', 0);
            this.params(3) = ClParametre('Name', 'Temperature 3',  'Value', 0, 'Unit', 'deg');
            this.params(4) = ClParametre('Name', 'Temperature 4',  'Value', 0, 'Unit', 'deg');
            this.params(5) = ClParametre('Name', 'Temperature 5',  'Value', 0, 'Unit', 'deg','Help', 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope');
            this.params(6) = ClParametre('Name', 'Temperature 6',  'Value', 0, 'Unit', 'deg', 'Help', 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope');
        end
        
        function handleValueChange(this, ~, ~)
            % Send one value change event
            this.notify('OneParamValueChange');
        end
        
        function okPressed(this, ~, ~)
            % Save function : set the Output then close dialog
            
            this.popupmenuOut        = this.popupmenu.Value;
            this.radioButtonGroupOut = this.radioButtonGroup.SelectedObject.String;
            this.monthOut            = this.jhSpinnerM.getValue;
            this.yearOut             = this.jhSpinnerY.getValue;
            this.dateOut             = this.jhSpinnerD.getValue;
            
            % Close the dialog
            okPressed@SimpleTitleDialog(this);
        end
        
    end
    
    methods (Access = protected)
        function [widthBody ,heightBody] = createBody(this, parent)
            verticalBox = uiextras.VBoxFlex('Parent', parent);
            lengthComponents = 0;
            
            %% Create ParametreComponent Panel
            
            parametreComponentPanel = uipanel('Parent', verticalBox,  'Title', 'parametreComponentPanel');
            parametreComponentBox = uiextras.VBox('Parent', parametreComponentPanel);
            
            
            this.paramComponentList(1) = ParametreComponent(parametreComponentBox, 'param', this.params(1), 'Sizes', [25 -0.95 0 0 0 -1 0 0]);
            % add listener on this.paramComponentList(1).
            % If this.paramComponentList(1) send the event 'ValueChange', this
            % triggers the method 'handleValueChange'
            addlistener(this.paramComponentList(1), 'ValueChange', @this.handleValueChange); % listener example
            lengthComponents = lengthComponents + 1;
            
            this.paramComponentList(2) = ParametreComponent(parametreComponentBox, 'param', this.params(2), 'Sizes', [0 -1 0 0 0 -1 0 0]);
            lengthComponents = lengthComponents + 1;
            
            this.paramComponentList(3) = ParametreComponent(parametreComponentBox, 'param', this.params(3), 'Sizes', [0 -1 0 0 0 -1 -1 0]);
            lengthComponents = lengthComponents + 1;
            
            %% Create SpinnerParametreComponent Panel
            spinnerParametreComponentPanel = uipanel('Parent', verticalBox, 'Title', 'spinnerParametreComponentPanel');
            spinnerParametreComponentBox = uiextras.VBox('Parent', spinnerParametreComponentPanel);
            
            this.paramComponentList(4) = SpinnerParametreComponent(spinnerParametreComponentBox, 'param', this.params(4), 'Sizes', [-1 -2 -1 0]);
            lengthComponents = lengthComponents + 1;
            
            this.paramComponentList(5) = SpinnerParametreComponent(spinnerParametreComponentBox, 'param', this.params(5), 'Sizes', [0 -1 0 25]);
            lengthComponents = lengthComponents + 1;
            
            %% Create SimpleParametreComponent Panel
            simpleParametreComponentPanel = uipanel('Parent', verticalBox, 'Title', 'simpleParametreComponentPanel');
            simpleParametreComponentBox = uiextras.VBox('Parent', simpleParametreComponentPanel);
            this.paramComponentList(6) = SimpleParametreComponent(simpleParametreComponentBox, 'param', this.params(6), 'Sizes', [-1 -1 -1 25]);
            lengthComponents = lengthComponents + 1;
            
            %% POPUPMENU COMPONENTs Panel
            
            popupComponentPanel = uipanel('Parent', verticalBox, 'Title', 'popupComponentPanel');
            popupComponent = uiextras.HBox('Parent', popupComponentPanel);
            
            uicontrol('Parent', popupComponent, ...
                'Style',  'text', ...
                'string', 'Select option : ');
            
            this.popupmenu = uicontrol('parent', popupComponent, ...
                'Style',    'popupmenu', ...
                'String',   {'option1', 'option2', 'option3'}, ...
                'Value',    2, ...
                'Callback', @this.popupmenuCallback);
            popupComponent.set('Sizes', [-1 -4]);
            lengthComponents = lengthComponents + 1;
            
            %% RADIOMENU COMPONENTs Group
            
            this.radioButtonGroup = uibuttongroup('Parent', verticalBox, ...
                'Title', 'radioButtonGroup', ...
                'SelectionChangedFcn', @this.radioButtonGroupCallback);
            
            % Create three radio buttons in the button group.
            uicontrol(this.radioButtonGroup, ...
                'Style',    'radiobutton', ...
                'String',   '<html><b>Option 1</b>', ...
                'Units',    'normalized', ...
                'Position', [0 0 1/3 1]);
            
            uicontrol(this.radioButtonGroup, ...
                'Style',    'radiobutton', ...
                'String',   'Option 2', ...
                'Units',    'normalized', ...
                'Position', [1/3 0 1/3 1]);
            
            uicontrol(this.radioButtonGroup, ...
                'Style',    'radiobutton', ...
                'String',   'Option 3', ...
                'Units',    'normalized', ...
                'Position', [2/3 0 1/3 1]);
            
            lengthComponents = lengthComponents + 1;
            
            %             % Make the uibuttongroup visible after creating child objects.
            %             radioButtonGroup.Visible = 'on';
            
            
            %% DATESPINNER COMPONENTS Panel
            
            dateSpinnerPanel = uipanel('Parent', verticalBox, 'Title', 'dateSpinnerComponentPanel');
            dateSpinnerBox = uiextras.VBox('Parent', dateSpinnerPanel);
            
            % Prepare the data values/limits
            t = datetime;
            currentYear = t.Year;
            minYear = currentYear - 1;
            maxYear = currentYear + 5;
            this.months = {'January', 'February', 'March', ...
                'April', 'May', 'June', ...
                'July', 'August', 'September', ...
                'October', 'November', 'December'};
            dates = {};
            for year = minYear:maxYear
                for monthIdx = 1 : 12
                    dates{end+1} = sprintf('%02d/%d', monthIdx,year); %#ok<AGROW>
                end
            end
            this.calendar = java.util.Calendar.getInstance;
            currentDate = this.calendar.getTime;
            this.calendar.set(minYear, 0, -1, 12, 0);  % Jan 1st
            minDate = this.calendar.getTime;
            this.calendar.set(maxYear, 11, 1, 12, 0);  % Jan 1st
            maxDate = this.calendar.getTime;
            
            % Display the spinner & label controls
            monthsModel = javax.swing.SpinnerListModel(this.months);
            this.jhSpinnerM = this.addLabeledSpinner(dateSpinnerBox, '&Month : ', monthsModel, @this.monthYearChangedCallback);
            this.jhSpinnerM.setValue(datestr(now, 'mmmm'));
            lengthComponents = lengthComponents + 1;
            
            yearsModel = javax.swing.SpinnerNumberModel(currentYear, minYear, maxYear, 1);
            this.jhSpinnerY = this.addLabeledSpinner(dateSpinnerBox, '&Year : ', yearsModel, @this.monthYearChangedCallback);
            jEditor = javaObject('javax.swing.JSpinner$NumberEditor', this.jhSpinnerY, '#');
            this.jhSpinnerY.setEditor(jEditor);
            lengthComponents = lengthComponents + 1;
            
            datesModel = javax.swing.SpinnerDateModel(currentDate, minDate, maxDate, this.calendar.MONTH);
            this.jhSpinnerD = this.addLabeledSpinner(dateSpinnerBox,'&Date : ', datesModel, @this.dateChangedCallback);
            jEditor = javaObject('javax.swing.JSpinner$DateEditor', this.jhSpinnerD, 'dd/MM/yyyy HH:mm:ss:SSS');
            this.jhSpinnerD.setEditor(jEditor);
            lengthComponents = lengthComponents + 1;
            
            %repartition
            verticalBox.set('Sizes', [-3 -2.5 -1.5 -1.5 -1.5 -3.5 ]);
            verticalBox.set('Spacing', 15);
            
            widthBody = 600;
            heightBody = 45 * lengthComponents;
        end
        
        function popupmenuCallback(this, src, ~) %#ok<INUSL>
            fprintf('PopupMenu choice :\n');
            fprintf('%s\n', src.String{src.Value});
        end
        
        function radioButtonGroupCallback(this, source, callbackdata) %#ok<INUSL>
            fprintf(['Previous: ' callbackdata.OldValue.String '\n']);
            fprintf(['Current: '  callbackdata.NewValue.String '\n']);
            fprintf('------------------\n');
        end
        
        % Add a label attached to a spinner
        function jhSpinner = addLabeledSpinner(this, parent, label, model, callbackFunc) %#ok<INUSL>
            % Spinner Component
            spinnerComponent = uiextras.HBoxFlex('Parent', parent);
            spinnerComponent.set('Spacing', 5);
            
            % Set the attached label
            jLabel = com.mathworks.mwswing.MJLabel(label);
            jLabel.setHorizontalAlignment(jLabel.RIGHT);  % unneeded
            if jLabel.getDisplayedMnemonic > 0
                hotkey = char(jLabel.getDisplayedMnemonic);
                jLabel.setToolTipText(['<html>Press <b><font color="blue">Alt-' hotkey '</font></b> to focus on<br/>adjacent spinner control']);
            end
            javacomponent(jLabel, [], spinnerComponent);
            
            % Set the spinner control
            jSpinner = com.mathworks.mwswing.MJSpinner(model);
            %jTextField = jSpinner.getEditor.getTextField;
            %jTextField.setHorizontalAlignment(jTextField.RIGHT);  % unneeded
            jhSpinner = javacomponent(jSpinner, [], spinnerComponent);
            jhSpinner.setToolTipText('<html>This spinner is editable, but only the<br/>preconfigured values can be entered')
            set(jhSpinner, 'StateChangedCallback', callbackFunc);
            
            jLabel.setLabelFor(jhSpinner);
        end  % addLabeledSpinner
        
        % Month or Year changed callback
        function monthYearChangedCallback(this, jSpinner, jEventData) %#ok<INUSD>
            persistent inCallback
            try
                if ~isempty(inCallback)
                    return
                end
                inCallback = 1;  %#ok used
                newMonthStr = this.jhSpinnerM.getValue;
                newMonthIdx = find(strcmpi(this.months,newMonthStr));
                newYear = this.jhSpinnerY.getValue;
                this.calendar.set(newYear, newMonthIdx-1, 1, 12, 0);
                this.jhSpinnerD.setValue(this.calendar.getTime);
            catch
                a = 1; %#ok<NASGU> % never mind...
            end
            inCallback = [];
        end  % monthChangedCallback
        
        % Date changed callback
        function dateChangedCallback(this,jSpinner, jEventData) %#ok<INUSD>
            persistent inCallback
            try
                if ~isempty(inCallback)
                    return
                end
                inCallback = 1;  %#ok used
                newDate = jSpinner.getValue;
                this.jhSpinnerM.setValue(this.months{newDate.getMonth+1});
                this.jhSpinnerY.setValue(newDate.getYear + 1900);
            catch
                a = 1; %#ok<NASGU> % never mind...
            end
            inCallback = [];
        end  % dateChangedCallback
    end
end
