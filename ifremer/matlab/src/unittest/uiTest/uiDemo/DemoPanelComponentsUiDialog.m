classdef DemoPanelComponentsUiDialog < handle & SimpleTitleUiDialog
    % Description
    %   Class DemoPanelComponentsUiDialog wich is used to display a set of example components
    %
    % Syntax
    %   a = DemoPanelComponentsUiDialog(...)
    %
    % Output Arguments
    %   s : One DemoPanelComponentsUiDialog instance
    %
    % Examples
    %   a = DemoPanelComponentsUiDialog();
    %   a.openDialog;
    %   % Output :
    %     a.okPressedOut % (0 if not pressed, 1 if pressed)
    %     a.dropdownComponentOut
    %     a.radioButtonGroupOut
    %     a.dateOut
    %
    % Authors : MHO
    % See also ParametreComponent ClModel Authors
    % ----------------------------------------------------------------------------
    
    properties
        params = ClParametre.empty % - ClParametre array
        % output
        dropdownComponentOut
        radioButtonGroupOut
        dateOut
    end
    
    properties (Access = private)
        paramComponentList = ParametreUiComponent.empty; % - ParametreUiComponent array
        spinnerParamComponentList = SpinnerParametreUiComponent.empty; % - SpinnerParametreUiComponent array
        simpleParamComponentList = SimpleParametreUiComponent.empty; % - SimpleParametreUiComponent array
        dropdownComponent
        radioButtonGroup
        datepicker
    end
    
    events
        OneParamValueChange % event if one param value change
    end
    
    methods
        function this = DemoPanelComponentsUiDialog(varargin)
            % Superclass Constructor
            this = this@SimpleTitleUiDialog(varargin{:});
            
            this.Help =  'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope'; %option
            this.Title = 'Demo panels components';
            
            this.params(1) = ClParametre('Name', 'Temperature 1',  'MinValue', -1, 'MaxValue', 1, 'Value', 0);
            this.params(2) = ClParametre('Name', 'Temperature 2',  'MinValue', -1, 'MaxValue', 1, 'Value', 0);
            this.params(3) = ClParametre('Name', 'Temperature 3',  'MinValue', -1, 'MaxValue', 1, 'Value', 0, 'Unit', 'deg');
            this.params(4) = ClParametre('Name', 'Temperature 4',  'Value', 0, 'Unit', 'deg');
            this.params(5) = ClParametre('Name', 'Temperature 5',  'Value', 0, 'Unit', 'deg','Help', 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope');
            this.params(6) = ClParametre('Name', 'Temperature 6',  'Value', 0, 'Unit', 'deg', 'Help', 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope');
        end
        
        function handleValueChange(this, ~, ~)
            % Send one value change event
            this.notify('OneParamValueChange');
        end
        
        function okPressed(this, ~, ~)
            % Save function : set the Output then close dialog
            
            this.dropdownComponentOut        = this.dropdownComponent.Value;
            this.radioButtonGroupOut = this.radioButtonGroup.SelectedObject.Text;
            this.dateOut = this.datepicker.Value;
            
            % Close the dialog
            okPressed@SimpleTitleUiDialog(this);
        end
        
    end
    
    methods (Access = protected)
        function [widthBody ,heightBody] = createBody(this, parent)
            verticalGridlayout = uigridlayout(parent, 'ColumnSpacing', 1, 'RowSpacing', 15, 'Padding', 0);
            verticalGridlayout.ColumnWidth = {'1x'};
            lengthComponents = 0;
            
            %% Create ParametreComponent Panel
            
            parametreComponentPanel = uipanel(verticalGridlayout,  'Title', 'parametreComponentPanel');
            parametreComponentGridlayout = uigridlayout(parametreComponentPanel, 'ColumnSpacing', 0, 'RowSpacing', 0, 'Padding', 0);
            parametreComponentGridlayout.ColumnWidth = {'1x'};
            
            
            this.paramComponentList(1) = ParametreUiComponent(parametreComponentGridlayout, 'param', this.params(1), 'Sizes', {25, '0.95x', 0, 0, 0, '1x', 0, 0});
            % add listener on this.paramComponentList(1).
            % If this.paramComponentList(1) send the event 'ValueChange', this
            % triggers the method 'handleValueChange'
            addlistener(this.paramComponentList(1), 'ValueChange', @this.handleValueChange); % listener example
            lengthComponents = lengthComponents + 1;
            
            this.paramComponentList(2) = ParametreUiComponent(parametreComponentGridlayout, 'param', this.params(2), 'Sizes', {0, '1x', 0, 0, 0, '1x', 0, 0});
            lengthComponents = lengthComponents + 1;
            
            this.paramComponentList(3) = ParametreUiComponent(parametreComponentGridlayout, 'param', this.params(3), 'Sizes', {0, '1x', 0, 0, 0, '1x', '1x', 0});
            lengthComponents = lengthComponents + 1;
            
            %% Create SpinnerParametreComponent Panel
            spinnerParametreComponentPanel = uipanel(verticalGridlayout, 'Title', 'spinnerParametreComponentPanel');
            
            spinnerParametreGridlayout = uigridlayout(spinnerParametreComponentPanel, 'ColumnSpacing', 0, 'RowSpacing', 0, 'Padding', 0);
            spinnerParametreGridlayout.ColumnWidth = {'1x'};
            
            this.spinnerParamComponentList(1) = SpinnerParametreUiComponent(spinnerParametreGridlayout, 'param', this.params(4), 'Sizes', {'1x', '2x', '1x', 0});
            lengthComponents = lengthComponents + 1;
            
            this.spinnerParamComponentList(2) = SpinnerParametreUiComponent(spinnerParametreGridlayout, 'param', this.params(5), 'Sizes', {0, '1x', 0, 25});
            lengthComponents = lengthComponents + 1;
            
            %% Create SimpleParametreComponent Panel
            simpleParametreComponentPanel = uipanel(verticalGridlayout, 'Title', 'simpleParametreComponentPanel');
            simpleParametreGridlayout = uigridlayout(simpleParametreComponentPanel, 'ColumnSpacing', 0, 'RowSpacing', 0, 'Padding', 0);
            simpleParametreGridlayout.ColumnWidth = {'1x'};
            simpleParametreGridlayout.RowHeight = {'fit'};
            this.simpleParamComponentList(1) = SimpleParametreUiComponent(simpleParametreGridlayout, 'param', this.params(6), 'Sizes', {'1x', '1x', '1x', 25});
            lengthComponents = lengthComponents + 1;
            
            %% POPUPMENU COMPONENTs Panel
            
            dropdownComponentPanel = uipanel(verticalGridlayout, 'Title', 'popupComponentPanel');
            
            dropdownGridlayout = uigridlayout(dropdownComponentPanel, 'ColumnSpacing', 0, 'RowSpacing', 0, 'Padding', 0);
            dropdownGridlayout.ColumnWidth = {'1x', '4x'};
            dropdownGridlayout.RowHeight = {'fit'};
            
            uilabel(dropdownGridlayout, ...
                'Text',  'Select option : ');
            
            this.dropdownComponent = uidropdown(dropdownGridlayout, ...
                'Items',{'option1','option2','option3'},...
                'Value','option2', ...
                'ValueChangedFcn', @this.dropdownCallback);
            
            
            lengthComponents = lengthComponents + 1;
            
            %% RADIOMENU COMPONENTs Group
            this.radioButtonGroup = uibuttongroup(verticalGridlayout, ...
                'Title', 'radioButtonGroup', ...
                'SelectionChangedFcn', @this.radioButtonGroupCallback);
            
            % Create three radio buttons in the button group.
            uiradiobutton(this.radioButtonGroup, 'Text', '<html><b>Option 1</b>', 'Position', [0 0 100 25]);
            uiradiobutton(this.radioButtonGroup, 'Text', 'Option 2', 'Position', [200 0 100 25]);
            uiradiobutton(this.radioButtonGroup, 'Text', 'Option 3', 'Position', [300 0 100 25]);
            
            lengthComponents = lengthComponents + 1;
            
            
            %% DATESPINNER COMPONENTS Panel
            
            dateSpinnerPanel = uipanel(verticalGridlayout, 'Title', 'dateSpinnerComponentPanel');
            
            dateSpinnerGridlayout = uigridlayout(dateSpinnerPanel, 'ColumnSpacing', 0, 'RowSpacing', 0, 'Padding', 0);
            dateSpinnerGridlayout.ColumnWidth = {'1x' '1x'};
            dateSpinnerGridlayout.RowHeight = {'fit'};
            
            uilabel(dateSpinnerGridlayout, ...
                'Text',  'Select date : ');
            
            this.datepicker = uidatepicker(dateSpinnerGridlayout, ...
                'Value',datetime('today'), ...
                'ValueChangedFcn', @this.datepickerCallback);
            this.datepicker.DisplayFormat = 'dd/MM/yyyy';
            
            lengthComponents = lengthComponents + 1;
            
            %repartition
            verticalGridlayout.RowHeight ={'3x', '2.5x', '1.5x', 'fit', '1.5x', 'fit'};
            
            widthBody = 600;
            heightBody = 45 * lengthComponents;
        end
        
        function dropdownCallback(this, src, ~) %#ok<INUSL>
            fprintf('DropDown choice :\n');
            fprintf('%s\n', src.Value);
        end
        
        
        function radioButtonGroupCallback(this, source, event) %#ok<INUSL>
            fprintf(['Previous: ' event.OldValue.Text '\n']);
            fprintf(['Current: '  event.NewValue.Text '\n']);
            fprintf('------------------\n');
        end
        
        function datepickerCallback(this, ~ ,event)
            lastdate = char(event.PreviousValue);
            newdate = char(event.Value);
            msg = ['Change date from ' lastdate ' to ' newdate '?'];
            % Confirm new date
            selection = uiconfirm(this.parentFigure, msg, 'Confirm Date');
            
            if (strcmp(selection,'Cancel'))
                % Revert to previous selection if cancelled
                this.datepicker.Value = event.PreviousValue;
            end
        end
        
    end
end
