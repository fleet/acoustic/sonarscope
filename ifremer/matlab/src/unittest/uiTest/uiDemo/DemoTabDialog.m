classdef DemoTabDialog < handle & SimpleTitleDialog
    % Description
    %   Class DemoTabDialog wich is used to display a set of
    %   example tabs
    %
    % Syntax
    %   a = DemoTabDialog(...)
    %
    % Optional Input Arguments
    %   See SimpleTitleDialog
    %
    % Output Arguments
    %   s : One DemoTabDialog instance
    %
    % Examples
    %   a = DemoTabDialog();
    %   a.openDialog;
    %
    % Authors : MHO
    % See also ClParametre ParametreComponent SpinnerParametreComponent SimpleParametreComponent Authors
    % ----------------------------------------------------------------------------
    
    properties
        params = ClParametre.empty % - ClParametre array
    end
    
    properties (Access = private)
        paramComponentList = ParametreComponent.empty; % - ParametreComponent array
    end
    
    methods
        function this = DemoTabDialog(varargin)
            % Superclass Constructor
            this = this@SimpleTitleDialog(varargin{:});
            
            this.Help =  'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope'; %option
            this.Title = 'Demo Tab components';
            
            this.params(1) = ClParametre('Name', 'Temperature1',  'Value', 0);
            this.params(2) = ClParametre('Name', 'Temperature2',  'Value', 0);
            this.params(3) = ClParametre('Name', 'Temperature3',  'Value', 0, 'Unit', 'deg');
            this.params(4) = ClParametre('Name', 'Temperature4',  'Value', 0, 'Unit', 'deg');
            this.params(5) = ClParametre('Name', 'Temperature5',  'Value', 0);
            this.params(6) = ClParametre('Name', 'Temperature 6', 'Value', 0, 'Unit', 'deg', 'Help', 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope');
        end
        
        function okPressed(this,~,~)
            % Save function : set the Output then close dialog
            
            % Close the dialog
            okPressed@SimpleTitleDialog(this);
        end
    end
    
    methods (Access = protected)
        function [widthBody ,heightBody] = createBody(this, parent)
            verticalBox = uiextras.VBox('Parent', parent);
            
            % Create tab group
            tabGroup = uitabgroup(verticalBox);
            
            % Create tab 1
            tab1 = uitab(tabGroup, 'title','Tab 1');
            tabComponenttest = uiextras.VBox('Parent', tab1);
            tabComponent     = uiextras.VBox('Parent', tabComponenttest);
            this.paramComponentList(1) = ParametreComponent(tabComponent,        'param', this.params(1), 'Sizes', [25 -0.95 0 0 0 -1 0 0]);
            this.paramComponentList(2) = ParametreComponent(tabComponent,        'param', this.params(2), 'Sizes', [0 -1 0 0 0 -1 0 0]);
            this.paramComponentList(3) = ParametreComponent(tabComponent,        'param', this.params(3), 'Sizes', [0 -1 0 0 0 -1 -1 0]);
            this.paramComponentList(4) = SpinnerParametreComponent(tabComponent, 'param', this.params(4), 'Sizes', [-1 -2 -1 0]);
            this.paramComponentList(5) = SpinnerParametreComponent(tabComponent, 'param', this.params(5), 'Sizes', [0 -1 0 0]);
            this.paramComponentList(6) = SimpleParametreComponent(tabComponent,  'param', this.params(6), 'Sizes', [-1 -1 -1 25]);
            
            tabComponenttest.set('Sizes', 25*6);
            % Create tab 2
            tab2 = uitab(tabGroup, 'title','Tab 2');
            tab2Component = uiextras.VBox( 'Parent', tab2);
            % Create ParametreComponent Panel
            parametreComponentPanel = uipanel('Parent', tab2Component, ...
                'Title', 'parametreComponentPanel');
            parametreComponentBox = uiextras.VBox('Parent', parametreComponentPanel);
            
            this.paramComponentList(7) = ParametreComponent(parametreComponentBox, 'param', this.params(1), 'Sizes', [25 -0.95 0 0 0 -1 0 0]);
            this.paramComponentList(8) = ParametreComponent(parametreComponentBox, 'param', this.params(2), 'Sizes', [0 -1 0 0 0 -1 0 0]);
            this.paramComponentList(9) = ParametreComponent(parametreComponentBox, 'param', this.params(3), 'Sizes', [0 -1 0 0 0 -1 -1 0]);
            
            
            spinnerParametreComponentPanel = uipanel('Parent', tab2Component, ...
                'Title', 'spinnerParametreComponentPanel');
            spinnerParametreComponentBox = uiextras.VBox('Parent', spinnerParametreComponentPanel);
            this.paramComponentList(10)  = SpinnerParametreComponent(spinnerParametreComponentBox, 'param', this.params(4), 'Sizes', [-1 -2 -1 0]);
            this.paramComponentList(11)  = SpinnerParametreComponent(spinnerParametreComponentBox, 'param', this.params(5), 'Sizes', [0 -1 0 0]);
            
            simpleParametreComponentPanel = uipanel('Parent', tab2Component, ...
                'Title', 'simpleParametreComponentPanel');
            simpleParametreComponentBox = uiextras.VBox('Parent', simpleParametreComponentPanel);
            this.paramComponentList(12) = SimpleParametreComponent(simpleParametreComponentBox, 'param', this.params(6), 'Sizes', [-1 -1 -1 25]);
            
            %tab 3
            tab3 = uitab(tabGroup, 'title','Tab 3');
            a = axes('parent', tab3); surf(peaks); %#ok<NASGU>
            
            widthBody  = 600;
            heightBody = 500;
        end
    end 
end
