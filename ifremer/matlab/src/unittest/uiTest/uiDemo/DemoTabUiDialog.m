classdef DemoTabUiDialog < handle & SimpleTitleUiDialog
    % Description
    %   Class DemoTabUiDialog wich is used to display a set of
    %   example tabs
    %
    % Syntax
    %   a = DemoTabUiDialog(...)
    %
    % Optional Input Arguments
    %   See SimpleTitleUiDialog
    %
    % Output Arguments
    %   s : One DemoTabUiDialog instance
    %
    % Examples
    %   a = DemoTabUiDialog();
    %   a.openDialog;
    %
    % Authors : MHO
    % See also ClParametre ParametreComponent SpinnerParametreComponent SimpleParametreComponent Authors
    % ----------------------------------------------------------------------------
    
    properties
        params = ClParametre.empty % - ClParametre array
    end
    
    properties (Access = private)
        paramComponentList = ParametreUiComponent.empty; % - ParametreUiComponent array
        spinnerParamComponentList = SpinnerParametreUiComponent.empty; % - SpinnerParametreUiComponent array
        simpleParamComponentList = SimpleParametreUiComponent.empty; % - SimpleParametreUiComponent array
    end
    
    methods
        function this = DemoTabUiDialog(varargin)
            % Superclass Constructor
            this = this@SimpleTitleUiDialog(varargin{:});
            
            this.Help =  'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope'; %option
            this.Title = 'Demo Tab components';
            
            this.params(1) = ClParametre('Name', 'Temperature1', 'MinValue', -1, 'MaxValue', 1, 'Value', 0);
            this.params(2) = ClParametre('Name', 'Temperature2', 'MinValue', -1, 'MaxValue', 1, 'Value', 0);
            this.params(3) = ClParametre('Name', 'Temperature3', 'MinValue', -1, 'MaxValue', 1, 'Value', 0, 'Unit', 'deg');
            this.params(4) = ClParametre('Name', 'Temperature4', 'Value', 0, 'Unit', 'deg');
            this.params(5) = ClParametre('Name', 'Temperature5', 'Value', 0);
            this.params(6) = ClParametre('Name', 'Temperature6', 'Value', 0, 'Unit', 'deg', 'Help', 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope');
        end
        
        function okPressed(this,~,~)
            % Save function : set the Output then close dialog
            
            % Close the dialog
            okPressed@SimpleTitleUiDialog(this);
        end
    end
    
    methods (Access = protected)
        function [widthBody ,heightBody] = createBody(this, parent)
            verticalGridlayout = uigridlayout(parent, 'ColumnSpacing', 0, 'RowSpacing', 0, 'Padding', 0);
            verticalGridlayout.ColumnWidth = {'1x'};
            % Create tab group
            tabGroup = uitabgroup(verticalGridlayout);
            
            % Create tab 1
            tab1 = uitab(tabGroup, 'title','Tab 1');
            tabComponenttestLayout = uigridlayout(tab1, 'ColumnSpacing', 0, 'RowSpacing', 0, 'Padding', 0);
            tabComponenttestLayout.ColumnWidth = {'1x'};
            tabComponentLayout = uigridlayout(tabComponenttestLayout, 'ColumnSpacing', 0, 'RowSpacing', 0, 'Padding', 0);
            tabComponentLayout.ColumnWidth = {'1x'};
            this.paramComponentList(1) = ParametreUiComponent(tabComponentLayout,        'param', this.params(1), 'Sizes', {25, '0.95x', 0, 0, 0, '1x', 0, 0});
            this.paramComponentList(2) = ParametreUiComponent(tabComponentLayout,        'param', this.params(2), 'Sizes', {0, '1x', 0, 0, 0, '1x', 0, 0});
            this.paramComponentList(3) = ParametreUiComponent(tabComponentLayout,        'param', this.params(3), 'Sizes', {0, '1x', 0, 0, 0, '1x', '1x', 0});
            this.spinnerParamComponentList(1) = SpinnerParametreUiComponent(tabComponentLayout, 'param', this.params(4), 'Sizes', {'1x', '2x', '1x', 0});
            this.spinnerParamComponentList(2) = SpinnerParametreUiComponent(tabComponentLayout, 'param', this.params(5), 'Sizes', {0, '1x', 0, 0});
            this.simpleParamComponentList(1) = SimpleParametreUiComponent(tabComponentLayout,  'param', this.params(6), 'Sizes', {'1x', '1x', '1x', 25});
            
            tabComponenttestLayout.RowHeight = {25*6};
            
            % Create tab 2
            tab2 = uitab(tabGroup, 'title','Tab 2');
            tab2ComponentLayout = uigridlayout(tab2, 'ColumnSpacing', 0, 'RowSpacing', 0, 'Padding', 0);
            tab2ComponentLayout.ColumnWidth = {'1x'};
            % Create ParametreComponent Panel
            parametreComponentPanel = uipanel('Parent', tab2ComponentLayout, ...
                'Title', 'parametreComponentPanel');
            parametreComponentLayout = uigridlayout(parametreComponentPanel, 'ColumnSpacing', 0, 'RowSpacing', 0, 'Padding', 0);
            parametreComponentLayout.ColumnWidth = {'1x'};
            
            this.paramComponentList(4) = ParametreUiComponent(parametreComponentLayout, 'param', this.params(1), 'Sizes', {25, '0.95x', 0, 0, 0, '1x', 0, 0});
            this.paramComponentList(5) = ParametreUiComponent(parametreComponentLayout, 'param', this.params(2), 'Sizes', {0, '1x', 0, 0, 0, '1x', 0, 0});
            this.paramComponentList(6) = ParametreUiComponent(parametreComponentLayout, 'param', this.params(3), 'Sizes', {0, '1x', 0, 0, 0, '1x', '1x', 0});
        
            
            spinnerParametreComponentPanel = uipanel('Parent', tab2ComponentLayout, ...
                'Title', 'spinnerParametreComponentPanel');
            spinnerParametreComponentLayout = uigridlayout(spinnerParametreComponentPanel, 'ColumnSpacing', 0, 'RowSpacing', 0, 'Padding', 0);
            spinnerParametreComponentLayout.ColumnWidth = {'1x'};
            
            this.spinnerParamComponentList(3)  = SpinnerParametreUiComponent(spinnerParametreComponentLayout, 'param', this.params(4), 'Sizes', {'1x', '2x', '1x', 0});
            this.spinnerParamComponentList(4)  = SpinnerParametreUiComponent(spinnerParametreComponentLayout, 'param', this.params(5), 'Sizes', {0, '1x', 0, 0});
            
            simpleParametreComponentPanel = uipanel('Parent', tab2ComponentLayout, ...
                'Title', 'simpleParametreComponentPanel');
            simpleParametreComponentLayout = uigridlayout(simpleParametreComponentPanel, 'ColumnSpacing', 0, 'RowSpacing', 0, 'Padding', 0);
            simpleParametreComponentLayout.ColumnWidth = {'1x'};
            this.simpleParamComponentList(2) = SimpleParametreUiComponent(simpleParametreComponentLayout, 'param', this.params(6), 'Sizes', {'1x', '1x', '1x', 25});
            
            
            %tab 3
            tab3 = uitab(tabGroup, 'title','Tab 3');
    
            axe = uiaxes(tab3); 
            surf(axe, peaks);
            
            widthBody  = 600;
            heightBody = 500;
        end
    end
end
