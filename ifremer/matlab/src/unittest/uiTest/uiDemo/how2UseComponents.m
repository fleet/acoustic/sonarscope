%% How to use ParametreComponent
% *Create a ClParametre instance*
param  = ClParametre('Name', 'Temperature', 'Unit', 'deg', 'MinValue', -10, 'MaxValue', 50, 'Value', 20);
%%
% *Display the instance*
param %#ok<NOPTS>
%%
% *Create a figure to host the component*
parent = FigUtils.createSScDialog('Position', centrageFig(600, 18));
%%
% *Define a style*
style  = ColorAndFontStyle('BackgroundColor', [0.7 0.7 1], 'ForegroundColor', 'k', ...
    'FontWeight', 'bold', 'FontSize', 13, 'FontAngle', 'italic');
%%
% *Create the component*
a = ParametreComponent(parent, 'param', param, 'style', style);
%% How to use RadioButtonsComponent
% *Create a figure to host the component*
%%
parent = FigUtils.createSScDialog('Position', centrageFig(600, 18));
%%
% *Create the component*
helpHtml = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a = RadioButtonsComponent(parent, 'orientation', RadioButtonsComponent.Orientation{1}, ...
    'title', 'Radio Button Component', 'titleButtons', {'button1'; 'button2'; 'Hello'; 'World'}, ...
    'Init', 3, 'helpHtml', helpHtml);
%%
% *Launch the documentation*
%%
help ParametreComponent
%%
doc ParametreComponent % Sorry the documentation window is not captured by the Publish function
%% How to use SpinnerParametreComponent
% *Create a figure to host the component*
%%
parent = FigUtils.createSScDialog('Position', centrageFig(600, 18)); 
%%
% *Define a style*
style2 = ColorAndFontStyle('BackgroundColor', [1 0.8 0.6], 'ForegroundColor', 'k', 'FontWeight', 'bold');
%%
% *Define a Help*
param.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
%%
% *Create the component*
a = SpinnerParametreComponent(parent, 'param', param, 'style', style2);
%% How to use SimpleParametreComponent 
% *Create a ClParametre instance*
param = ClParametre('Name', 'Temperature', 'Unit', 'deg', 'MinValue', -10, 'MaxValue', 50, 'Value', 20);
param.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
%%
% *Create a figure to host the component*
%%
parent = FigUtils.createSScDialog('Position', centrageFig(600, 18));
%%
% *Create the component*
a = SimpleParametreComponent(parent, 'param', param); %#ok<*NASGU>
%% How to use DropDownButtonComponent
% The use of this component is demonstrated in DemoDropDownButtonComponent 
type DemoDropDownButtonComponent
%%
% *Lauch DemoDropDownButtonComponent*
a = DemoDropDownButtonComponent('WaitAnswer', false);
a.openDialog;
