%% How to use DemoComponentsDialog
% *Create the parametres*
type DemoComponentsDialog
%%
% *Launch DemoComponentsDialog*
a = DemoTabDialog('WaitAnswer', false);
a.openDialog;
%%
% * Simulate the OK push action
a.okPressed();
%% How to use DemoPanelComponentsDialog
% *Create the parametres*
type DemoPanelComponentsDialog
%%
% *Launch DemoPanelComponentsDialog*
a = DemoPanelComponentsDialog('WaitAnswer', false);
a.openDialog;
%%
% *Simulate the OK push action*
a.okPressed();