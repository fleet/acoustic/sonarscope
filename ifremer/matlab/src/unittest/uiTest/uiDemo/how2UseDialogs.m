%% How to use ParametreDialog
% *Create the parametres*
p(1) = ClParametre('Name', 'Temperature', 'Unit', 'deg',  'MinValue', -10, 'MaxValue', 50, 'Value', 20);
p(2) = ClParametre('Name', 'Salinity',    'Unit', '0/00', 'MinValue', 25,  'MaxValue', 35, 'Value', 30);
%%
% *Create the parametreDialog instance*
a = ParametreDialog('params', p, 'Title', 'Demo of ParametreDialog', 'WaitAnswer', false); % WaitAnswer is set to false just for the generation of this document with "Publish"
%%
% *Personalize the fonts*
style1 = ColorAndFontStyle('BackgroundColor', [0.7 0.7 1], 'ForegroundColor', 'k', 'FontWeight', 'bold', 'FontSize', 13, 'FontAngle', 'italic');
style2 = ColorAndFontStyle('BackgroundColor', [1 0.8 0.6], 'ForegroundColor', 'k', 'FontWeight', 'bold');
a.titleStyle = style1;
a.paramStyle = style2;
%%
% *Give a help for the window*
a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
%%
% *Define the sizes of the elements*
a.sizes = [0 -2 -1 -2 -1 -1 -1 0];
%%
% *Open the window*
a.openDialog;
%%
% *Display the parametreDialog instance*
a %#ok<*NOPTS>
%%
% *Edit properties
a.editProperties
%%
% *StyledParametreDialog
a = StyledParametreDialog('params', p, 'Title', 'Demo of ParametreDialog', 'WaitAnswer', false); % WaitAnswer is set to false just for the generation of this document with "Publish"
a.sizes = [0 -2 -1 -2 -1 -1 -1 0];
a.openDialog;
%%
% * Simulate the OK push action
a.okPressed();
%%
% *Check if th user has pressed OK*
a.okPressedOut %(0 if not Ok pressed, 1 if Ok pressed)
%%
% *Get the parameters*
a.getParamsValue

%% How to use ModelDialog
%%
% *Create a ClModel instance*
model = BSLurtonModel('XData', -80:80);
%%
% *Add noise on model*
model = noise(model, 'Coeff', 5);
%%
% *Plot the model*
plot(model);
%%
% *Create a ModelDialog instance*
b = ModelDialog('This is a demonstration of ModelDialog', model, 'WaitAnswer', false);
%%
% *Personalize the style*
style1 = ColorAndFontStyle('BackgroundColor', [0.7 0.7 1], 'ForegroundColor', 'k', 'FontWeight', 'bold', 'FontSize', 13, 'FontAngle', 'italic'); %option
style2 = ColorAndFontStyle('BackgroundColor', [1 0.8 0.6], 'ForegroundColor', 'k', 'FontWeight', 'bold');
b.paramDialog.titleStyle = style1;
b.paramDialog.paramStyle = style2;
%%
% *Define a help for the window*
b.paramDialog.Help =  'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';

%%
% *Open the window*
b.openDialog
%%
% * Simulate the OK push action
b.paramDialog.okPressed();
%%
% Get the parameters
b.model.getParamsValue

%% How to use OptimDialog
%%
% *Create a ClOptim instance*
optim = ExClOptimBS;
%%
% *Create a ModelDialog instance*
a = OptimDialog(optim, 'Title', 'Optim ExClOptimBS', 'WaitAnswer', false);
%%
% *Personalize the style*
style1 = ColorAndFontStyle('BackgroundColor', [0.7 0.7 1], 'ForegroundColor', 'k', 'FontWeight', 'bold', 'FontSize', 13, 'FontAngle', 'italic'); %option
a.titleStyle = style1;

%%
% *Open the window*
a.openDialog();

%%
% * Simulate the OK push action
a.okPressed();

%%
% *Get the parameters*
a.selectedModel.getParamsValue
