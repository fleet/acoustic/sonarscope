function flag = XsfRunUnitTestInBatchMode(dataDir, globeInstallDir, varargin)
% Example : 
% ---------
%   dataDir         = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\TestsFormatXsf';
%   globeInstallDir = 'C:\GLOBE-3DViewer';
%   dryRun          = 1;
%   flag = XsfRunUnitTestInBatchMode(dataDir, globeInstallDir, 'DryRun', dryRun);
%
% Lancement de la commande en batch MatLab
% ----------------------------------------
%   % A Executer dans un powershell
%   matlab -batch -r "try; if strcmpi(getenv('computername'), 'QO-PCP023'); ...
%       cd('C:\SonarScope\SonarScopeTbxGLU'); startUpSSC; ...
%       end; ...
%       cd ('C:\SonarScope\SscTbx\ifremer\matlab\src\unittest\xsfTest'); ...
%       dataDir = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\TestsFormatXsf'; ...
%       globeInstallDir = 'C:\GLOBE-3DViewer'; ...
%       dryRun          = 1; ...
%       flag = XsfRunUnitTestInBatchMode(dataDir, globeInstallDir, 'DryRun', dryRun);
%       catch; end; quit;"
%
% ------------------------------------------

[varargin, dryRun] = getPropertyValue(varargin, 'DryRun', false); %#ok<ASGLU> % Par défaut, mode non-fictif.

flag = 0;

flagDownloadGlobe = false;
if flagDownloadGlobe % Pour s'affranchir du download et convert temporairement.
    %% Lancement du code Python d'analyse de la page Web
    import py.scriptDecodeSeanoeWebPage.*
    
    globeDownloadDir   = my_tempdir;
    urlGlobeWebPage      = 'https://www.seanoe.org/data/00592/70460/';
    % Téléchargement de Globe et décompression via Python
    outPy = py.scriptDecodeSeanoeWebPage.myFunc(globeInstallDir, globeDownloadDir, urlGlobeWebPage, dryRun);
    % urlGlobeInstallDir  = char(outPy{1});
    % urlGlobeZipLink     = char(outPy{2}); % ZIP file downloaded for Globe
    versGlobe           = regexprep(char(outPy{3}), ' ', '');
    globeInstallDir     = fullfile(globeInstallDir, sprintf('Globe%s',versGlobe));
    
    strDateTest         = datestr(now(), 'yyyymmdd'); % 30);
    diary(fullfile(my_tempdir, sprintf('xsfUnitTest_Globe%s_%s.txt', versGlobe, strDateTest))) 
end

%% Chargement de Globe

% A mettre sous forme de test.
listKmAll = dir(fullfile(dataDir, '*.kmall'));

bkPwd = pwd;
cd(globeInstallDir);
for k=1:numel(listKmAll)
    try
        kmallFile = fullfile(dataDir, listKmAll(k).name);
        [~, rootFile]  = fileparts(listKmAll(k).name);
        xsfGlobeFileName{k} = fullfile(dataDir, [rootFile '.xsf.nc']); %#ok<AGROW>
        status = 0;
        if ~dryRun
            if ~exist(xsfGlobeFileName{k}, 'file')
                strCmd = sprintf('converter.bat -in %s -o -fmt xsf -out %s', kmallFile, dataDir);
                [status, cmdout] = system(strCmd) %#ok<ASGLU,NOPRT>
            end
            if status
                strErr = sprintf('Conversion from Globe %s failed for file %s.\nPlease contact C. Poncellet@IFR.', versGlobe, listKmAll(k).name);
                fprintf('%s : %s\n', mfilename, strErr);
                listKmAll(k) = [];
            end
        end

    catch ME
        return
    end
end
cd(bkPwd);

%% Lancement des tests unitaires

import matlab.unittest.parameters.Parameter
import matlab.unittest.TestSuite
import matlab.unittest.TestRunner

versRefGlobe            = '2.0.3';
% rootDirRefData          = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\TestsFormatXsf';
refSscCacheDataDir      = fullfile(dataDir, sprintf('Globe-%s', versRefGlobe));

% Avec sortie du verdict sous forme de fichier.
import matlab.unittest.plugins.LoggingPlugin

% Avec sortie du verdict sous forme de fichier.
import matlab.unittest.plugins.XMLPlugin;
import matlab.unittest.Verbosity;


for k=1:numel(listKmAll)

    [~, xsfDataRoot]    = Hdf5Utils.readGrpData(xsfGlobeFileName{k}, '/');
    a                   = regexp(xsfGlobeFileName{k}, '\.', 'split');
    [~, rootFile, ext]  = fileparts(a{1});
    pppp                = xsfGlobeFileName{k};
    param(1)            = Parameter.fromData('xsfGlobeFileName',{pppp});
    param(2)            = Parameter.fromData('hdf5Data',{xsfDataRoot});
    param(3)            = Parameter.fromData('refSscCacheDataDir', {refSscCacheDataDir});
    suiteTest           = TestSuite.fromClass(?XsfTest, 'ExternalParameters',param);
    runner              = TestRunner.withNoPlugins;
    xmlFilename         = fullfile(my_tempdir, sprintf('xsfUnitTest_%s.xml', rootFile));
    plugin              = XMLPlugin.producingJUnitFormat(xmlFilename,'OutputDetail',Verbosity.Verbose);
    runner.addPlugin(plugin);
    plugin              = LoggingPlugin.withVerbosity(4);
    runner.addPlugin(plugin);
    results             = runner.run(suiteTest);
end
diary off

flag = 1;