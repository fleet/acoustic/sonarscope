classdef XsfTest < matlab.unittest.TestCase
    % Description
    %   XsfTest Unit Test for Science Classes
    %
    % Laucnh test :
    %   res = run(XsfTest)
    %
    % Output Arguments
    %   res : Matlab.unittest.TestResult instance
    %
    % Examples
    %     import matlab.unittest.parameters.Parameter
    %     import matlab.unittest.TestSuite
    %     import matlab.unittest.TestRunner
    %
    %     [~, xsfDataRoot]  = Hdf5Utils.readGrpData(xsfGlobeFileName, '/');
    % 
    %     param(1)          = Parameter.fromData('xsfGlobeFileName',{xsfGlobeFileName});
    %     param(2)          = Parameter.fromData('hdf5Data',{xsfDataRoot});
    %     param(3)          = ...
    %     Parameter.fromData('refSscCacheDataDir', {'E:\SonarScopeAnnexes\SonarScopeDataArchive\TestsFormatXsf\Globe-2.0.3'});
    %     suiteTest         = TestSuite.fromClass(?XsfTest, 'ExternalParameters',param);
    %     results           = suiteTest.run;
    %
    %     %% Avec sortie du verdict sous forme de fichier Xml.
    %     import matlab.unittest.plugins.XMLPlugin;
    %     import matlab.unittest.Verbosity;
    %      
    %     % Create a XML plugin that sends XML Output to a file
    %     fNameXMLUnitTest = fullfile(my_tempdir, 'xsfUnitTest.xml');
    %      
    %     % Create a XML plugin that produces a concise amount of output detail
    %     plugin = XMLPlugin.producingJUnitFormat(fNameXMLUnitTest);
    %     runner = TestRunner.withNoPlugins;
    %     runner.addPlugin(plugin);
    %     result = runner.run(suiteTest);
    % Authors  : GLU
    % See also ClSignal DialogsTest
    % ----------------------------------------------------------------------------
    
    properties (TestParameter)
        hdf5Data            = struct('hdf5Data', []);           % Data HDF from xsf file 
        xsfGlobeFileName    = struct('xsfGlobeFileName', []);   % Name of xsf in test
        refSscCacheDataDir  = struct('refSscCacheDataDir', []); % Folder of Cache data reference.
    end
 
    methods (Test)
        function [results, rt] = testMultiDgm(testCase, xsfGlobeFileName, hdf5Data, refSscCacheDataDir)
            
            %% Useful for parameters in testCase
            import matlab.unittest.parameters.Parameter
            import matlab.unittest.TestSuite
            import matlab.unittest.Verbosity
            
            results = matlab.unittest.TestResult;
            
            [~, rootFic, ~]   = fileparts(regexprep(xsfGlobeFileName, 'xsf.nc', ''));
            % Comparaison avec le Cache de référence de SSc
            ncSscFileName     = fullfile(refSscCacheDataDir, 'SonarScope', [rootFic '.nc']);

            if ~exist(ncSscFileName, 'file')
                testCase.assertFail(sprintf(['XsfTest: Error in reading Ssc Cache file : %s.\n', ...
                                        'Please, check previous run of ALL_Cache2Netcdf for Sounder file'], ncSscFileName));
            end

            % Read Xsf Data id needed.
            if isempty(hdf5Data)
                [flag, hdf5Data] = Hdf5Utils.readGrpData(xsfGlobeFileName, '/');
                if ~flag
                    testCase.assertFail(sprintf('XsfTest: Error in reading XSF file. Please, check field xsf_convention_version : %5.2f', ...
                        hdf5Data.Att.xsf_convention_version));
                end
            end
            
            %% Test existence of Groups / Subgroups significants.
            log(testCase, 4, '==> XftTestCase : Test groups existance.');
%             listGroups = {  'Att', 'Dim', 'Environment', 'Platform', 'Provenance', 'Sonar', ...
%                             'summary', 'fileName'};
            listGroups = {  'Att', 'Environment', 'Platform', 'Provenance', 'Sonar', ...
                            'summary', 'fileName'};
                        
                        
            for k=1:numel(listGroups)
                if ~isfield(hdf5Data, listGroups(k))
                    testCase.assertFail(sprintf('XsfTest: Error in reading XSF file. Please, check field presence of %s group.', ...
                        listGroups{k}));
                end
            end
            
            %% Init and convert files if needed.            
            param(1)        = Parameter.fromData('ncSscFileName',{ncSscFileName});
            param(2)        = Parameter.fromData('hdf5Data',{hdf5Data});
            

            log(testCase, 3, '==> XftTestCase : DgmClock Test');
            suiteTest       = TestSuite.fromClass(?DgmClockTest, 'ExternalParameters',param);
            % {suiteTest.Name};
            results(1)  = suiteTest.run;

            log(testCase, 3, '==> XftTestCase : DgmSoundSpeed Test');
            suiteTest       = TestSuite.fromClass(?DgmSoundSpeedTest, 'ExternalParameters',param);
            % {suiteTest.Name};
            results(end+1)  = suiteTest.run;
           
            log(testCase, 3, '==> XftTestCase : DgmInstallParameters Test');
            suiteTest       = TestSuite.fromClass(?DgmInstallParametersTest, 'ExternalParameters',param);
            % {suiteTest.Name};
            results(end+1)  = suiteTest.run;

            log(testCase, 3, '==> XftTestCase : DgmPosition Test');
            suiteTest       = TestSuite.fromClass(?DgmPositionTest, 'ExternalParameters',param);
            % {suiteTest.Name};
            results(end+1)  = suiteTest.run;
            
            log(testCase, 3, '==> XftTestCase : DgmAttitude Test');
            suiteTest       = TestSuite.fromClass(?DgmAttitudeTest, 'ExternalParameters',param);
            % {suiteTest.Name};
            results(end+1)  = suiteTest.run;

            log(testCase, 3, '==> XftTestCase : DgmRuntime Test');
            suiteTest       = TestSuite.fromClass(?DgmRuntimeTest, 'ExternalParameters',param);
            results(end+1) = suiteTest.run;

            % Test des Dgm interdépendants.
            log(testCase, 3, '==> XftTestCase : DgmDepth Test');
            % DgmDepth dépend des autres Dgm DataAttitude, DataSeabedXSF, DataRawXML
            suiteTest       = TestSuite.fromClass(?DgmDepthTest, 'ExternalParameters',param);
            {suiteTest.Name};
            results(end+1)  = suiteTest.run;
           
            log(testCase, 3, '==> XftTestCase : DgmSeabed Test');
            % DgmSeabedImage dépend des autres Dgm DataRuntime
            suiteTest       = TestSuite.fromClass(?DgmSeabedTest, 'ExternalParameters',param);
            % {suiteTest.Name};
            results(end+1)  = suiteTest.run;
                        
            log(testCase, 3, '==> XftTestCase : DgmRawRangeBeamAngle Test');
            suiteTest       = TestSuite.fromClass(?DgmRawRangeBeamAngleTest, 'ExternalParameters',param);
            % {suiteTest.Name};
            results(end+1)  = suiteTest.run;

            log(testCase, 3, '==> XftTestCase : DgmWaterColumn Test');
            suiteTest       = TestSuite.fromClass(?DgmWaterColumnTest, 'ExternalParameters',param);
            % {suiteTest.Name};
            results(end+1)  = suiteTest.run;
            
            rt = table(results) %#ok<NOPRT>

            if any(rt.Passed == 0)
               testCase.assertFail('Xsf Suite Test in Error');
            end
            
        end
    end
end
