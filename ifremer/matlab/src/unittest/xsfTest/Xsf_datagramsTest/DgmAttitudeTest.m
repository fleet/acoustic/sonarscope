classdef DgmAttitudeTest < matlab.unittest.TestCase
    % Description : DgmAttitudeTest Unit Test.
    % Comparaison between xsf files getted :
    % - from Globe 
    % - from Ssc Cache obtained by converting Xsf file version Globe
    % 1.18.11)
    %
    % Launch test :
    %   res = run(DgmAttitudeTest)
    %
    % Output Arguments
    %   import matlab.unittest.parameters.Parameter
    %   import matlab.unittest.TestSuite
    %   [rootDir, rootFic, ~]   = fileparts(regexprep(xsfGlobeFileName, 'xsf.nc', ''));
    %   if ~exist('hdf5Data', 'var')
    %       [flag, hdf5Data]        = Hdf5Utils.readGrpData(xsfGlobeFileName, '/');
    %   end
    %   ncSscFileName       = fullfile(rootDir, 'SonarScope', [rootFic '.nc']);
    %   param(1)            = Parameter.fromData('ncSscFileName',{ncSscFileName});
    %   param(2)            = Parameter.fromData('hdf5Data',{hdf5Data});
    %
    %   suiteTest           = TestSuite.fromClass(?DgmAttitudeTest, 'ExternalParameters',param);
    %   results             = suiteTest.run;
    %
    % Examples
    %   Cf. Cl XsfTest
    %
    % Authors  : GLU
    % ----------------------------------------------------------------------------
    
    properties (TestParameter)
        hdf5Data      = struct('xsfDataRoot', []); % Data HDF from xsf file
        ncSscFileName = struct('ncSscFileName', []);
    end
    
    methods (Test)
        function testAttitude(testCase, ncSscFileName, hdf5Data)
                        
            dgmXsf = 'Attitude';
            [~, rootFic]   = fileparts(regexprep(ncSscFileName, 'xsf.nc', ''));

            %% Reading Xsf file
            % Reading only Sensor000
            if isfield(hdf5Data.Sonar.Beam_group1.Att','preferred_MRU')
                idMRU       = hdf5Data.Sonar.Beam_group1.Att.preferred_MRU + 1;
                strMRUId    = sprintf('Sensor%03d', idMRU);
            else
                strWrn      = sprintf('Problem in HDF5/Sonar/Beam_group1 MRU identification attributes reading:%s', Hdf5Data.fileName);
                my_warndlg(strWrn, 0, 'Tag', 'MessageHDF5');
                return
            end
            [flag, XML, xsfData] = XSF.extractAttitude(hdf5Data, 'SensorId', strMRUId);

            %% Reading Cache Nc file from Ssc
            ncData = NetcdfUtils.Netcdf2Struct(ncSscFileName, 'Attitude');
             
            % List fields to test
            % 'GPSTime' non comparé car ajout de 0.001 seconds dans le fichier
            % Xsf de Globe.
            % List fields to test
            fNames  = fieldnames(ncData);
            idx     = contains(fNames, 'Signals');
            fNames(idx) = [];
            
            for f=1:numel(fNames)
                try
                    valExpected     = ncData.(fNames{f});
                catch
                    testCase.assertFail(sprintf('%s : Error in reading SonarScope NC cache file for datagram : %s', rootFic, dgmXsf));
                end
                try
                    valObj          = xsfData.(fNames{f});
                catch
                    testCase.assertFail(sprintf('%s: Error in reading XSF file for datagram : %s', rootFic, dgmXsf));
                end
                testCase.verifyEqual(valExpected,valObj, sprintf('%s : Xsf content not conformal to SSc Netcdf cache file, field : %s', rootFic, fNames{f}));
            end
            
        end % testAttitude
       
    end
end
