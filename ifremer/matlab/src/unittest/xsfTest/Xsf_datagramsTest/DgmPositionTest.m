classdef DgmPositionTest < matlab.unittest.TestCase
    % Description : DgmPositionTest Unit Test for position. 
    % Comparaison between xsf files getted :
    % - from Globe 
    % - from Ssc Cache obtained by converting Xsf file version Globe 1.18.11
    %
    % Launch test :
    %   import matlab.unittest.parameters.Parameter
    %   import matlab.unittest.TestSuite
    %   [rootDir, rootFic, ~]   = fileparts(regexprep(xsfGlobeFileName, 'xsf.nc', ''));
    %   if ~exist('hdf5Data', 'var')
    %       [flag, hdf5Data]        = Hdf5Utils.readGrpData(xsfGlobeFileName, '/');
    %   end
    %   ncSscFileName       = fullfile(rootDir, 'SonarScope', [rootFic '.nc']);
    %   param(1)            = Parameter.fromData('ncSscFileName',{ncSscFileName});
    %   param(2)            = Parameter.fromData('hdf5Data',{hdf5Data});
    %
    %   suiteTest           = TestSuite.fromClass(?DgmPositionTest, 'ExternalParameters',param);
    %   results             = suiteTest.run;
    %
    % Output Arguments
    %   res : Matlab.unittest.TestResult instance
    %
    % Examples
    %   Cf. Cl XsfTest
    %
    % Authors  : GLU
    % ----------------------------------------------------------------------------
    
    properties (TestParameter)
        hdf5Data        = struct('xsfDataRoot', []); % Data HDF from xsf file 
        ncSscFileName   = struct('ncSscFileName', []);
    end
    
    methods (Test)
        function testPosition(testCase, ncSscFileName, hdf5Data)
                        
            dgmXsf = 'Position';
            [~, rootFic] = fileparts(regexprep(ncSscFileName, 'xsf.nc', ''));

            %% Reading Xsf file
            if isfield(hdf5Data.Sonar.Beam_group1.Att','preferred_position')
                idPosition    = hdf5Data.Sonar.Beam_group1.Att.preferred_position + 1;
                strPositionId = sprintf('Sensor%03d', idPosition);
            else
                strWrn = sprintf('Problem in HDF5/Sonar/Beam_group1 Position identification attributes reading:%s', hdf5Data.fileName);
                my_warndlg(strWrn, 0, 'Tag', 'MessageHDF5');
                return
            end

            [flag, XML, xsfData] = XSF.extractPosition(hdf5Data, 'SensorId', strPositionId);
           
            %% Reading Cache Nc file from Ssc
            ncData = NetcdfUtils.Netcdf2Struct(ncSscFileName, dgmXsf);
                         
            %% List fields to test
            fNames  = fieldnames(ncData);
            idx     = contains(fNames, 'Signals');
            fNames(idx) = [];
            % Exclusion (temporaire ?) du GPSTime pour lequel Globel ajoute
            % 1 ms au temps.
            idx     = contains(fNames, 'GPSTime');
            fNames(idx) = [];
            
            for f=1:numel(fNames)
                try
                    valExpected     = ncData.(fNames{f});
                catch
                    testCase.assertFail(sprintf('%s : Error in reading SonarScope NC cache file for datagram : %s', rootFic, dgmXsf));
                end
                try
                    valObj          = xsfData.(fNames{f});
                    % Force type because to type Boolean wirtten in cache
                    % netcdf file.
                    if isa(valObj, 'logical')
                        valObj = int8(valObj);
                    end
                catch
                    testCase.assertFail(sprintf('%s: Error in reading XSF file for datagram : %s', rootFic, dgmXsf));
                end
                testCase.verifyEqual(valExpected,valObj, sprintf('%s : Xsf content not conformal to SSc Netcdf cache file, field : %s', rootFic, fNames{f}));
            end
            
        end % testPosition
       
    end
end
