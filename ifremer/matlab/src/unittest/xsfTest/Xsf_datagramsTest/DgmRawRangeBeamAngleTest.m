classdef DgmRawRangeBeamAngleTest < matlab.unittest.TestCase
    % Description : DgmRawRangeBeamAngleTest Unit Test.
    % Comparaison between xsf files getted :
    % - from Globe 
    % - from Ssc Cache obtained by converting Xsf file version Globe
    % 1.18.11)
    %
    % Launch test :
    %   import matlab.unittest.parameters.Parameter
    %   import matlab.unittest.TestSuite
    %
    %   [rootDir, rootFic, ~]   = fileparts(regexprep(xsfGlobeFileName, 'xsf.nc', ''));
    %   if ~exist('hdf5Data', 'var')
    %       [flag, hdf5Data]        = Hdf5Utils.readGrpData(xsfGlobeFileName, '/');
    %   end
    %   ncSscFileName           = fullfile(rootDir, 'SonarScope', [rootFic '.nc']);
    %   param(1)        = Parameter.fromData('ncSscFileName',{ncSscFileName});
    %   param(2)        = Parameter.fromData('hdf5Data',{hdf5Data});
    %
    %   suiteTest         = TestSuite.fromClass(?DgmRawRangeBeamAngleTest, 'ExternalParameters',param);
    %   results           = suiteTest.run;
    %
    % Output Arguments
    %   res : Matlab.unittest.TestResult instance
    %
    % Examples
    %   Cf. Cl XsfTest
    %
    % Authors  : GLU
    % ----------------------------------------------------------------------------
    
    properties (TestParameter)        
        hdf5Data      = struct('xsfDataRoot', []); % Data HDF from xsf file 
        ncSscFileName = struct('ncSscFileName', []);
    end
    
    methods (Test)
        function testRawRangeBeamAngle(testCase, ncSscFileName, hdf5Data)
            
            dgmXsf = 'RawRangeBeamAngle';
            [~, rootFic] = fileparts(regexprep(ncSscFileName, 'xsf.nc', ''));

            %% Reading Xsf file

            % Permet d'identifier le mode du sondeur.
            [flag, DataInstallParams] = XSF.extractInstallParameters(hdf5Data);
            if ~flag
                return
            end
            SounderDualOneTxAntenna = DataInstallParams.isDual; % Ajout JMA le 14/10/2022 vaut 1 pour EM2040 Dual

            [flag, XML, xsfData] = XSF.extractRawRangeBeamAngle(hdf5Data, SounderDualOneTxAntenna); % SounderDualOneTxAntenna);
            
            %% Reading Cache Nc file from Ssc
            ncData = NetcdfUtils.Netcdf2Struct(ncSscFileName, dgmXsf);
            if isempty(ncData)
                return
            end
            
            %% List fields to test
            fNames  = fieldnames(ncData);
            idx     = contains(fNames, 'Signals');
            fNames(idx) = [];
            
            
            for f=1:numel(fNames)
                try
                    valExpected     = ncData.(fNames{f});
                catch
                    testCase.assertFail(sprintf('%s : Error in reading SonarScope NC cache file for datagram : %s', rootFic, dgmXsf));
                end
                try
                    valObj          = xsfData.(fNames{f});
                    % Force type because to type Boolean wirtten in cache
                    % netcdf file.
                    if isa(valObj, 'logical')
                        valObj = int8(valObj);
                    end
                catch
                    testCase.assertFail(sprintf('%s: Error in reading XSF file for datagram : %s', rootFic, dgmXsf));
                end
                testCase.verifyEqual(valExpected,valObj, sprintf('%s : Xsf content not conformal to SSc Netcdf cache file, field : %s', rootFic, fNames{f}));
            end
            
        end % testRawRangeBeamAngle
       
    end
end
