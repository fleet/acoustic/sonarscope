classdef DgmSeabedTest < matlab.unittest.TestCase
    % Description : DgmSeabedTest Unit Test.
    % Comparaison between xsf files getted :
    % - from Globe 
    % - from Ssc Cache obtained by converting Xsf file version Globe
    % 1.18.11)
    %
    % Launch test :
    %   [rootDir, rootFic, ~]   = fileparts(regexprep(xsfGlobeFileName, 'xsf.nc', ''));
    %   if ~exist('hdf5Data', 'var')
    %       [flag, hdf5Data]        = Hdf5Utils.readGrpData(xsfGlobeFileName, '/');
    %   end
    %   ncSscFileName           = fullfile(rootDir, 'SonarScope', [rootFic '.nc']);
    %   param(1)        = Parameter.fromData('ncSscFileName',{ncSscFileName});
    %   param(2)        = Parameter.fromData('hdf5Data',{hdf5Data});
    %
    %   suiteTest         = TestSuite.fromClass(?DgmSeabedTest, 'ExternalParameters',param);
    %   results           = suiteTest.run;
    %
    % Output Arguments
    %   res : Matlab.unittest.TestResult instance
    %
    % Examples
    %   Cf. Cl XsfTest
    %
    % Authors  : GLU
    % ----------------------------------------------------------------------------
    
    properties (TestParameter)
        hdf5Data      = struct('xsfDataRoot', []); % Data HDF from xsf file 
        ncSscFileName = struct('ncSscFileName', []);
    end
    
    methods (Test)
        function testSeabed(testCase, ncSscFileName, hdf5Data)
            
            dgmXsf = 'SeabedImage59h';
            [~, rootFic] = fileparts(regexprep(ncSscFileName, 'xsf.nc', ''));
            
            if isfield(hdf5Data.Sonar.Beam_group1.Att','preferred_MRU')
                idMRU    = hdf5Data.Sonar.Beam_group1.Att.preferred_MRU + 1;
                strMRUId = sprintf('Sensor%03d', idMRU);
            else
                strWrn = sprintf('Problem in HDF5/Sonar/Beam_group1 MRU identification attributes reading:%s', Hdf5Data.fileName);
                my_warndlg(strWrn, 0, 'Tag', 'MessageHDF5');
                return
            end
            [flag, xsfDataRuntimeXML, xsfDataRuntime] = XSF.extractRuntime(hdf5Data);
            [flag, xsfSeabedXML, xsfData]             = XSF.extractSeabedImage(hdf5Data, xsfDataRuntime);
            
            %% Reading Cache Nc file from Ssc
            ncData = NetcdfUtils.Netcdf2Struct(ncSscFileName, dgmXsf);
            if isempty(ncData)
                return
            end
             
            %% List fields to test
            fNames  = fieldnames(ncData);
            idx     = contains(fNames, 'Signals');
            fNames(idx) = [];
            
            idx     = contains(fNames, 'AbsorptionCoefficientSSc');
            fNames(idx)    = [];
            
            for f=1:numel(fNames)
                try
                    valExpected     = ncData.(fNames{f});
                catch
                    testCase.assertFail(sprintf('%s : Error in reading SonarScope NC cache file for datagram : %s', rootFic, dgmXsf));
                end
                try
                    valObj          = xsfData.(fNames{f});
                    % Force type because to type Boolean wirtten in cache
                    % netcdf file.
                    if isa(valObj, 'logical')
                        valObj = int8(valObj);
                    end
                catch
                    testCase.assertFail(sprintf('%s: Error in reading XSF file for datagram : %s', rootFic, dgmXsf));
                end
                testCase.verifyEqual(valExpected,valObj, sprintf('%s : Xsf content not conformal to SSc Netcdf cache file, field : %s', rootFic, fNames{f}));
            end
            
        end % testDepth
       
    end
end
