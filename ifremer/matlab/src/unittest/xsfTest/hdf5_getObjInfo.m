%   function [status, sOut] = getObjInfo(rootID, name, sIn)
%   status = [];
%   sOut= [];
%     objID = H5O.open(rootID, name, 'H5P_DEFAULT');
%     obj_info=H5O.get_info(objID);
%     H5O.close(objID);
%   end  
    
function [status, sOut] = hdf5_getObjInfo(objId, name, sIn)

    
    % objtype = H5I.get_type(objId);
    objId2 = H5O.open(objId, name, 'H5P_DEFAULT');
    % obj_info=H5O.get_info(objId2);
    objtype = H5I.get_type(objId2);

    try
        grpName = regexp(name, '\/', 'split');
        if numel(grpName) == 1
            refGrp = grpName;
        else
            refGrp  = regexprep(name, grpName{end}, ''); 
            % refGrp = arrayfun(@(x) strcat(x, '/'), grpName(1:end-1));
            % refGrp  = [refGrp{:}];
            % Suppression du dernier "/"
            refGrp = refGrp(1:end-1);
        end
        % fprintf('Object #%s is a dataset.\n', name);
        if isempty(sIn.Groups)
            idxGrp = 1;
        else
            % To optimize : 
            %  idxGrp = find(strfind({sIn.Groups(:).Name}, refGrp));
            % listNameGrp = arrayfun(@(x) (x.Name), sIn.Groups, 'Un',0);
            % [C, idxGrp] = intersect(listNameGrp, refGrp);
            idxGrp = numel(sIn.Groups);
        end

        switch(objtype)
            case H5ML.get_constant_value('H5I_BADID')
                fprintf('Object #%s is a Bad Id.\n', name);
            case H5ML.get_constant_value('H5I_DATASET')
                if idxGrp % Elude de le champ History
                    sIn.Groups(idxGrp).Datasets(end+1).Name     = grpName{end};
                    sIn.Groups(idxGrp).Datasets(end).Id         = objId;
                    sIn.Groups(idxGrp).Datasets(end).Datatype   = H5T.get_class(H5D.get_type(objId2));
                end
            case H5ML.get_constant_value('H5I_GROUP')
                % fprintf('Object #%s is a group.\n', name);
                sIn.Groups(end+1).Name      = name;
                sIn.Groups(end).Id          = objId;
                sIn.Groups(end).Groups      = [];
                sIn.Groups(end).Datasets    = [];
            case H5ML.get_constant_value('H5I_DATATYPE')
                % fprintf('Object #%s is a named datatype.\n', name);
            case H5ML.get_constant_value('H5I_ATTR')
                fprintf('Object #%s is a named attribute.\n', name);
            case H5ML.get_constant_value('H5I_DATASPACE')
                fprintf('Object #%s is a named dataspace.\n', name);
        end
    catch ME
        disp('toto')
    end
    status = 0; % keep iterating
    sOut = sIn; 
    H5O.close(objId2);

end