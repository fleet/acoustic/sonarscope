import urllib.request
from bs4 import BeautifulSoup
import os
import zipfile
globe_install_dir = "None"
globe_download_dir = os.path.join("C:", "SonaScope", "temp")
url = "https://www.seanoe.org/data/00592/70460/"
dryRun = 0

def myFunc(globe_install_dir, globe_download_dir, url, dryRun):
    with urllib.request.urlopen(url) as html_connect:
        html = html_connect.read().decode("utf-8")
        decoded_html = BeautifulSoup(html, features="html.parser")
        # Retrieve all <a> tags with href containing url and pointing to a zip file
        snapshot = decoded_html.find_all(
            "a", {"href": lambda href: url in href and ".zip" in href}
        )
        # For every found <a> tag, get its table row and get first row containing "Globe Windows",
        # then, get href
        for item in snapshot:
            tr = item.find_parent("tr")
            if tr:
                spans = tr.findChildren("span")
                for span in spans:
                    if "Globe Windows" in span.text:
                        globe_download_url = item.attrs["href"]
                        versGlobe = span.text.replace("Globe Windows V", "" );
                        break
        
        # download Globe a globe.zip
        zip_file = "globe.zip"
        zip_file = os.path.join(globe_download_dir, zip_file)
        globe_install_dir = os.path.abspath(globe_download_dir)
        if not dryRun:   # if not dummy mode    
            urllib.request.urlretrieve(globe_download_url, zip_file)
        
            with zipfile.ZipFile(zip_file, "r") as zip_ref:
                 zip_ref.extractall(globe_install_dir)
            os.remove("globe.zip")
        # unzip Globe
        print("--- End of unzip Globe dowloaded---")
        print("Unzipping {zip_file} to {globe_install_dir} directory")
        print("-----------------------------------")
  
    return globe_install_dir,globe_download_url,versGlobe