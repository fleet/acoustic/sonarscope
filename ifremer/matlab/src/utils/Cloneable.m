classdef (Abstract)Cloneable < handle
    % Cloneable
    %   The class which implement this Abstract class have to implement
    %       the clone(this)
    
    methods
        function clone = clone(this)
            % clone array of objects
            for k=numel(this):-1:1
                
                mco = metaclass(this(k));
                properiyList = mco.PropertyList;
                className = mco.Name;
                clone(k) = eval(className);
                
                % Set property
                for k2=1:length(properiyList)
                    properyName = properiyList(k2).Name;
                    % test if a superclass of prop(k) is Cloneable
                    cloneableTest = strfind(superclasses(this(k).(properyName)), 'Cloneable');
                    if ~isempty([cloneableTest{:}])
                        % if property is an object, clone this
                        clone(k).(properyName) = this(k).(properyName).clone;
                    elseif ~properiyList(k2).NonCopyable
                        clone(k).(properyName) = this(k).(properyName);
                    end
                end
            end
        end
    end
end
