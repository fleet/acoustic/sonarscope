classdef EditPropertiesUtils
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    methods (Static)
        function createObjEditPropertiesFig(Parent, Fig, WindowStyle)
            
            %% Figure
            if Fig % TODO MHO : mutualiser avec ClParametre
                %% Prepare a properties table containing the Parent
                model = com.jidesoft.grid.PropertyTableModel(Parent);
                model.expandAll();
                grid = com.jidesoft.grid.PropertyTable(model);
                hPanelJava = com.jidesoft.grid.PropertyPane(grid);
                
                %% Display the properties hPanelJava on screen
                hFig = figure;
                set(hFig, 'WindowStyle', WindowStyle);
                Position = get(hFig, 'Position');
                hPanelMatlab = uipanel(hFig);
                javacomponent(hPanelJava, [5 5 Position(3:4)*0.98], hPanelMatlab);
                
                % Set the resize callback function of uihPanelMatlab container
                set(hPanelMatlab, 'ResizeFcn', {@UihPanelMatlabResizeCallback, hPanelMatlab, hPanelJava});
                
                % Wait for figure window to close & display the prop value
                % uiwait(hFig);
                % disp(prop(1).getValue())
                
            end
            
            %% Callback function for resize behavior of the uihPanelMatlab container
            function UihPanelMatlabResizeCallback(hObject, eventdata, handles, hMainPanel, varargin) %#ok<INUSD,INUSL>
                h = get(handles, 'Children');
                hFig = get(handles, 'Parent');
                Position = get(hFig, 'Position');
                set(h, 'Position', [5 5 Position(3:4)*0.98]);
            end
        end
        
        function p2 = addItemText(p2, varargin)
            [varargin, Name]        = getPropertyValue(varargin, 'Name',        'Unknown');
            [varargin, Value]       = getPropertyValue(varargin, 'Value',       'Unknown');
            [varargin, Description] = getPropertyValue(varargin, 'Description', []);
            
            %% Check if all the arguments have been interpretated
            
            if checkIfVararginIsEmpty(varargin)
                return
            end
            
            if isempty(Description)
                Description = Name;
            end
            
            if isnumeric(Value)
                Value = num2strCode(Value);
                %             elseif islogical(Value)
                %                 Value = Value;
                %             elseif ischar(Value)
                %                 Value = Value;
                %             else
                %                 Value = Value;
            end
            
            p3 = com.jidesoft.grid.DefaultProperty();
            p3.setName(Name);
            p3.setType(javaclass('char',1));
            p3.setValue(Value);
            p3.setCategory('Hello world');
            p3.setDisplayName(Name);
            p3.setDescription(Description);
            p3.setEditable(false);
            p2.addChild(p3);
        end
        
        function p2 = addItemCombobox(p2, varargin)
            [varargin, Name]        = getPropertyValue(varargin, 'Name',        'Unknown');
            [varargin, List]        = getPropertyValue(varargin, 'List',        {});
            [varargin, Value]       = getPropertyValue(varargin, 'Value',       'Unknown');
            [varargin, Description] = getPropertyValue(varargin, 'Description', []);
            
            %% Check if all the arguments have been interpretated
            
            if checkIfVararginIsEmpty(varargin)
                return
            end
            
            if isempty(Description)
                Description = Name;
            end
            
            %             if isnumeric(Value)
            %                 Value = num2strCode(Value);
            %             else
            
            if islogical(Value)
                Value = Value + 1;
            end
            
            %             elseif ischar(Value)
            %                 Value = Value;
            %             else
            %                 Value = Value;
            %             end
            
            options0 = List;
            editor0  = com.jidesoft.grid.ListComboBoxCellEditor(options0);
            context0 = com.jidesoft.grid.EditorContext(['comboboxeditor_' num2str(floor(rand(1)*1000000))]);
            com.jidesoft.grid.CellEditorManager.registerEditor(javaclass('char', 1), editor0, context0);
            
            p3 = com.jidesoft.grid.DefaultProperty();
            p3.setName(Name);
            p3.setType(javaclass('char',1));
            p3.setValue(List{Value});
            p3.setCategory('Hello world');
            p3.setEditorContext(context0);
            p3.setDisplayName(Name);
            p3.setDescription(Description);
            p3.setEditable(true);
            p2.addChild(p3);
        end
        
        function p2 = addItemCheckbox(p2, varargin)
            [varargin, Name]        = getPropertyValue(varargin, 'Name',        'Unknown');
            [varargin, Value]       = getPropertyValue(varargin, 'Value',       'Unknown');
            [varargin, Description] = getPropertyValue(varargin, 'Description', []);
            
            %% Check if all the arguments have been interpretated
            
            if checkIfVararginIsEmpty(varargin)
                return
            end
            
            if isempty(Description)
                Description = Name;
            end
            
            %             if isnumeric(Value)
            %                 Value = num2strCode(Value);
            %             elseif islogical(Value)
            %                 Value = Value;
            %             elseif ischar(Value)
            %                 Value = Value;
            %             else
            %                 Value = Value;
            %             end
            
            p3 = com.jidesoft.grid.DefaultProperty();
            p3.setName(Name);
            p3.setType(javaclass('logical'));
            p3.setValue(Value);
            p3.setCategory('Hello world');
            p3.setDisplayName(Name);
            p3.setDescription(Description);
            %             p3.setEditable(false);
            p3.setEditorContext(com.jidesoft.grid.BooleanCheckBoxCellEditor.CONTEXT);
            p2.addChild(p3);
        end
    end
end
