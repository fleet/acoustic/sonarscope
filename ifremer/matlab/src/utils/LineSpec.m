classdef LineSpec < handle
    % Description
    %   Class that deals with color line style and marker type properties for plot
    %
    % Syntax
    %   s = LineSpec(...)
    %
    % LineSpec properties :
    %   color       - Color of line/marker
    %   lineStyle   - Style of line
    %   lineWidth   - Specifies the width (in points) of the line
    %   marker      - Type of marker
    %   markerSize  - Specifies the size of the marker in points (must be greater than 0)
    %
    % Output Arguments
    %   s : One LineSpec instance
    %
    % Examples
    %   s = LineSpec('Color', 'y','lineStyle', '-');
    % ----------------------------------------------------------------------------
    
    properties
        color      % Color of line/marker
        lineStyle  % Style of line
        lineWidth  % Specifies the width (in points) of the line
        marker     % Type of marker
        markerSize % Specifies the size of the marker in points (must be greater than 0)
        markerEdgeColor
    end
    
    methods
        function this = LineSpec(varargin)
            % Constructor of the class
            this = set(this, varargin{:});
        end
        
        function this = set(this, varargin)
            [varargin, this.color]           = getPropertyValue(varargin, 'color',           this.color);
            [varargin, this.lineStyle]       = getPropertyValue(varargin, 'lineStyle',       this.lineStyle);
            [varargin, this.lineWidth]       = getPropertyValue(varargin, 'lineWidth',       this.lineWidth);
            [varargin, this.marker]          = getPropertyValue(varargin, 'marker',          this.marker);
            [varargin, this.markerSize]      = getPropertyValue(varargin, 'markerSize',      this.markerSize);
            [varargin, this.markerEdgeColor] = getPropertyValue(varargin, 'markerEdgeColor', this.markerEdgeColor); %#ok<ASGLU>
        end
        
        function setStyleToLine(this, line)
            % Set the style of this ySample object to input line
            if ~isempty(this.color)
                set(line(:), 'Color', this.color);
            end
            if ~isempty(this.lineStyle)
                set(line(:), 'LineStyle', this.lineStyle);
            end
            if ~isempty(this.lineWidth)
                set(line(:), 'LineWidth', this.lineWidth);
            end
            if ~isempty(this.marker)
                set(line(:), 'Marker', this.marker);
            end
            if ~isempty(this.markerSize)
                set(line(:), 'MarkerSize', this.markerSize);
            end
            if ~isempty(this.markerEdgeColor)
                set(line(:), 'MarkerEdgeColor', this.markerEdgeColor);
            end
            
            % add listeners to link line properties and y sample properties
            addlistener(line(:), 'Color',           'PostSet', @(src,eventdata)updateProperty(this,src,eventdata));
            addlistener(line(:), 'LineStyle',       'PostSet', @(src,eventdata)updateProperty(this,src,eventdata));
            addlistener(line(:), 'LineWidth',       'PostSet', @(src,eventdata)updateProperty(this,src,eventdata));
            addlistener(line(:), 'Marker',          'PostSet', @(src,eventdata)updateProperty(this,src,eventdata));
            addlistener(line(:), 'MarkerSize',      'PostSet', @(src,eventdata)updateProperty(this,src,eventdata));
            addlistener(line(:), 'MarkerEdgeColor', 'PostSet', @(src,eventdata)updateProperty(this,src,eventdata));
        end
        
        function updateProperty(this, ~, eventdata)
            % Update the properties of this object
            line =  eventdata.AffectedObject;
            
            this.color = line.Color;
            this.lineStyle = line.LineStyle;
            this.lineWidth = line.LineWidth;
            this.marker = line.Marker;
            this.markerSize = line.MarkerSize;
            this.markerEdgeColor = line.MarkerEdgeColor;
        end
    end
end
