function [flag, CourbesStatistiquesOut] = CourbesStatsClean(CourbesStatistiques, varargin)

nbCourbesStatsBegin = length(CourbesStatistiques);

[varargin, subCourbes] = getPropertyValue(varargin, 'sub',       1:nbCourbesStatsBegin);
[varargin, ImageName]  = getPropertyValue(varargin, 'Name', []);
[varargin, NewCurve]   = getPropertyValue(varargin, 'NewCurve',  1);
[varargin, Title]      = getPropertyValue(varargin, 'Title',     []);

[varargin, NoPlot] = getFlag(varargin, 'NoPlot');
[varargin, last]   = getFlag(varargin, 'last'); %#ok<ASGLU>

if isempty(Title)
%     str1 = 'Outil de nettoyage de courbes';
%     str2 = 'Curves cleaning tool';
%     Title = Lang(str1,str2);
    Title = 'SignalDialog window';
end

CourbesStatistiquesOut = [];

if NewCurve == 0
    my_warndlg('Attention, NewCurve=0 dans CourbesStatsClean !', 1);
    flag = 0;
    return
end

if last
    subCourbes = subCourbes(end);
end

% {
if isempty(subCourbes)
    CI = cl_image;
    CI.Name = ImageName;
    message_NoCurve(CI)
    flag = 0;
    return
end
% }

Colors = 'brkgmc';
% k1 = 0;
nbCourbes = length(subCourbes);

TabSector = zeros(nbCourbes);
for iCourbe=1:nbCourbes
    if isempty(CourbesStatistiques(subCourbes(iCourbe)).bilan)
        continue
    end
    bilan = CourbesStatistiques(subCourbes(iCourbe)).bilan{1};
    sectors = bilan;
    nbSectors = length(sectors);
    
    if nbCourbes > 1
        kmod = 1 + mod(iCourbe, length(Colors));
    end
%     kEnd = 0;
    flagSectors = true(1,nbSectors); % Rajout� par JMA le 12/04/2019
    for k=1:nbSectors
        if isempty(bilan(k).y)
            flagSectors(k) = false; % Rajout� par JMA le 12/04/2019
            continue
        end
        if isfield(bilan(k), 'numVarCondition') && ~isempty(bilan(k).numVarCondition)
            TabSector(iCourbe,k) = bilan(k).numVarCondition;
        else
            TabSector(iCourbe,k) = 1;
        end
        %         y{k} = bilan(k).y; %#ok<AGROW>
        %         Residu{k} = []; %#ok<AGROW>
        
        if nbCourbes == 1
            kmod = 1 + mod(k, length(Colors));
        end
        
        subNonNaN = ~isnan(sectors(k).y);
        sectors(k).x   = sectors(k).x(subNonNaN);
        sectors(k).y   = sectors(k).y(subNonNaN);
        sectors(k).nx  = sectors(k).nx(subNonNaN);
        sectors(k).std = sectors(k).std(subNonNaN);
        sectors(k).NY  = length(sectors(k).x);
        sectors(k).sub = 1:sectors(k).NY;
        if isfield(sectors, 'ymedian') && ~isempty(sectors(k).ymedian)
            sectors(k).ymedian = sectors(k).ymedian(subNonNaN);
        end
%         kEnd = k;
    end
%     bilan = sectors(1:kEnd);
    bilan = sectors(flagSectors); % Rajout� par JMA le 12/04/2019
    CourbesStatistiques(subCourbes(iCourbe)).bilan{1} = bilan;
end

flagAtLeastOneCurve = 0;
for iCourbe=1:nbCourbes
    if isempty(CourbesStatistiques(subCourbes(iCourbe)).bilan)
        continue
    end
    bilan = CourbesStatistiques(subCourbes(iCourbe)).bilan{1};
    
    sectors = bilan;
    nbSectors = length(sectors);
    clear ySample
    
    if nbCourbes > 1
        kmod = 1 + mod(iCourbe, length(Colors));
    end
    for k=1:nbSectors
        if isempty(bilan(k).y)
            continue
        end
        %         y{k} = bilan(k).y; %#ok<AGROW>
        %         Residu{k} = []; %#ok<AGROW>
        
        if nbCourbes == 1
            kmod = 1 + mod(k, length(Colors));
        end
        
        xSample = XSample('name', bilan(k).DataTypeConditions{1}, 'data', sectors(k).x);
        
        if nbSectors == 1
            try
                try
                    strLegend = CourbesStatsNames(bilan);
                    Name = strLegend{1};
                catch % ce qui existait avant
                    Name = bilan.nom;
                end
            catch
                Name = bilan.nomZoneEtude;
            end
        else
            try % Modif JMA le 19/06/2019
                strLegend = CourbesStatsNames(bilan);
                Name = strLegend{k};
            catch % ce qui existait avant
                Name = strcat(bilan(k).nomZoneEtude, ' - ', num2str(k));
            end
        end
        
        tag = [iCourbe k];
        ySample = YSample('data', sectors(k).y,  'unit',  sectors(k).Unit, ...
            'Color', Colors(kmod), 'marker', '.');
        signalReflec = ClSignal('Name', sectors(k).DataTypeValue, 'xSample', xSample, 'ySample', ySample, 'tag', tag);
        
        ySample = YSample('data', sectors(k).nx, 'unit', 'nb', ...
            'Color', Colors(kmod), 'marker', '.');
        signalNx = ClSignal('Name', 'Nb of points', 'xSample', xSample, 'ySample', ySample, 'tag', tag);
        
        ySample = YSample('data', log10(sectors(k).nx), 'unit', 'log10(nb)', ...
            'Color', Colors(kmod), 'marker', '.');
        signalLog10Nx = ClSignal('Name', 'Nb of points - log10', 'xSample', xSample, 'ySample', ySample, 'tag', tag);
        
        ySample = YSample('data', sectors(k).std, 'unit', sectors(k).Unit, ...
            'Color', Colors(kmod), 'marker', '.');
        signalStd = ClSignal('Name', 'std', 'xSample', xSample, 'ySample', ySample, 'tag', tag);
        
        sigCurves(iCourbe,k) = SignalContainer('Name', Name, 'signalList', [signalReflec signalNx signalLog10Nx signalStd]); %#ok<AGROW>

        flagAtLeastOneCurve = 1;
    end
end

if ~flagAtLeastOneCurve
    %     my_warndlg('Attention, NewCurve=0 dans CourbesStatsClean !', 1);
    flag = 0;
    return % TODO : � valider
end

%% Cr�ation de l'instance SignalDialog et affichage de la fen�tre

style1 = ColorAndFontStyle('BackgroundColor', [0.7 0.7 1], 'ForegroundColor', 'k', 'FontWeight', 'bold', 'FontSize', 13, 'FontAngle', 'italic'); %option
a = SignalDialog(sigCurves, 'Title', Title, 'titleStyle', style1);
% a.editProperties
a.displayedSignalList = [1 1 0 0];

% profile on
% pause(0.1)
a.openDialog();
% profile report

%{
% TODO : ce truc est fait pour permettre de ne pas prendre en compte les
% signaux non visualis�s dans SignalDialog. Ceci est douteux d'un point de
% vue fonctionnel. Il faudrait un menu dans SignalDialog permettant
% d'assigner � NaN les signaux non visualis�s ou quelque-chose comme ��.
if nbSectors == 1
    % TODO : comment� car �a ne fonctionne pas si plusieurs secteurs
    subDisplayedSignalList = (a.displayedSignalContainerList == 1); % TODO : devrait �tre un boolean dans SignalDialog
    subCourbes = subCourbes(identCourbe(subDisplayedSignalList));
else
    %     subDisplayedSignalList = identCourbe(true(1, length(a.signalList)));
    %     subDisplayedSignalList = identSector(true(1, length(a.signalList)));
    subDisplayedSignalList = 1:length(a.signalContainerList); % TODO : en attende de g�rer ce pb de mani�re propre
end
%}

displayedSignalContainerList = reshape(a.displayedSignalContainerList, size(sigCurves));

%% R�cup�ration des r�sultats

curveCleaned = a.signalContainerList;
for iCourbe=1:nbCourbes
    if isempty(CourbesStatistiques(subCourbes(iCourbe)).bilan)
        continue
    end
    bilan = CourbesStatistiques(subCourbes(iCourbe)).bilan{1};
    sectors = bilan;
    nbSectors = length(sectors);

    for iSector=1:nbSectors
        if isempty(bilan(iSector).y)
            continue
        end
        if ~displayedSignalContainerList(iCourbe,iSector)
            continue
        end
        xSampleCleaned = curveCleaned(iCourbe,iSector).signalList(1).xSample;
%         ySampleCleaned = curveCleaned(iCourbe,iSector).ySample;
        ySampleCleaned1 = curveCleaned(iCourbe,iSector).signalList(1).ySample;
        ySampleCleaned2 = curveCleaned(iCourbe,iSector).signalList(2).ySample;
%         ySampleCleaned3 = curveCleaned(iCourbe,iSector).signalList(3).ySample;
        ySampleCleaned4 = curveCleaned(iCourbe,iSector).signalList(4).ySample;
        sub = find(~(isinf(ySampleCleaned1.data(:)) | isinf(ySampleCleaned2.data(:)) | isinf(ySampleCleaned4.data(:))));
%         bilan(iSector).x  = xSampleCleaned.data(sub);
        bilan(iSector).x  = xSampleCleaned.data(sub);
        bilan(iSector).y  = ySampleCleaned1.data(sub);
        bilan(iSector).nx = ySampleCleaned2.data(sub);
        
        %         bilan(iSector).std = bilan(iSector).std(subNonNaN);
        %         bilan(iSector).std = bilan(iSector).std(sub);
        bilan(iSector).std = ySampleCleaned4.data(sub);
        
        bilan(iSector).sub = (1:length(sub))';
        if isfield(bilan, 'ymedian')
            % TODO : ici bug quand stats .all sur fichiers F:\EM710-BSCORR\0032_20170227_142033_ATL.all
            % Retester cela avec nouvelle mouture de ClSignal
            try
                bilan(iSector).ymedian = bilan(iSector).ymedian(sub);
            catch
                my_breakpoint
            end
        end
%         bilan(iSector).NY = [length(sub) nbSectors];
        bilan(iSector).NY = length(sub);    
        
%         deb = strfind(bilan(1).nomZoneEtude, ' - Clean');
        deb = strfind(bilan(iSector).nomZoneEtude, ' - Clean');
        if isempty(deb)
%             bilan(1).nomZoneEtude = [bilan(1).nomZoneEtude ' - Clean 1'];
            bilan(iSector).nomZoneEtude = [bilan(iSector).nomZoneEtude ' - Clean 1']; % Modif JMA le 19/06/2019
%             bilan(iSector).nomZoneEtude = [bilan(1).nomZoneEtude ' - Clean 1']; % Modif JMA le 19/06/2019
        else
%             k2 = str2num(bilan(1).nomZoneEtude(deb+9:end)); %#ok<ST2NM>
            k2 = str2num(bilan(iSector).nomZoneEtude(deb+9:end)); %#ok<ST2NM>
%             bilan(1).nomZoneEtude = [bilan(1).nomZoneEtude(1:deb-1) ' - Clean ' num2str(k2+1)];
            bilan(iSector).nomZoneEtude = [bilan(iSector).nomZoneEtude(1:deb-1) ' - Clean ' num2str(k2+1)];
        end
        
        if isempty(CourbesStatistiquesOut)
            if iSector == 1
                CourbesStatistiquesOut = CourbesStatistiques(subCourbes(iCourbe));
            else
                CourbesStatistiquesOut = CourbesStatistiques(subCourbes(iCourbe));
                CourbesStatistiquesOut(1) = [];
            end
        else
            if iSector == 1
                CourbesStatistiquesOut(iCourbe) = CourbesStatistiques(subCourbes(iCourbe)); %#ok<AGROW>
            end
        end
        CourbesStatistiquesOut(iCourbe).bilan{1}(iSector) = bilan(iSector); %#ok<AGROW>
    end
    if ~NoPlot
        CourbesStatsPlot(CourbesStatistiquesOut(iCourbe).bilan, 'Stats', 0);
    end
end

%% The End

flag = 1;
