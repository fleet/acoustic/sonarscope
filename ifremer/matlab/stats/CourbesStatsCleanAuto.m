function [flag, CourbesStatistiquesOut] = CourbesStatsCleanAuto(CourbesStatistiques, varargin)

nbCourbesStatsBegin = length(CourbesStatistiques);

[varargin, subCourbes] = getPropertyValue(varargin, 'sub',       1:nbCourbesStatsBegin);
[varargin, ImageName]  = getPropertyValue(varargin, 'Name', []);
[varargin, NewCurve]   = getPropertyValue(varargin, 'NewCurve',  1);

[varargin, last] = getFlag(varargin, 'last'); %#ok<ASGLU>

CourbesStatistiquesOut = [];

if NewCurve == 0
    my_warndlg('Attention, NewCurve=0 dans CourbesStatsClean !', 1);
    flag = 0;
    return
end

if last
    subCourbes = subCourbes(end);
end

if isempty(subCourbes)
    IM = cl_image;
    IM.Name = ImageName;
    message_NoCurve(IM)
    flag = 0;
    return
end

nbCourbes = length(subCourbes);

TabSector = zeros(nbCourbes);
for iCourbe=1:nbCourbes
    if isempty(CourbesStatistiques(subCourbes(iCourbe)).bilan)
        continue
    end
    bilan = CourbesStatistiques(subCourbes(iCourbe)).bilan{1};
    sectors = bilan;
    nbSectors = length(sectors);
    
    flagSectors = true(1,nbSectors);
    for k=1:nbSectors
        if isempty(bilan(k).y)
            flagSectors(k) = false;
            continue
        end
        if isfield(bilan(k), 'numVarCondition') && ~isempty(bilan(k).numVarCondition)
            TabSector(iCourbe,k) = bilan(k).numVarCondition;
        else
            TabSector(iCourbe,k) = 1;
        end
        
        flagNx = (sectors(k).nx > (max(sectors(k).nx) / 10));
        first = find(flagNx(:), 1, 'first');
        last = find(flagNx(:), 1, 'last');
        flagNx(first:last) = 1;
        subNonNaN = ~isnan(sectors(k).y) & flagNx;
        sectors(k).x   = sectors(k).x(subNonNaN);
        sectors(k).y   = sectors(k).y(subNonNaN);
        sectors(k).nx  = sectors(k).nx(subNonNaN);
        sectors(k).std = sectors(k).std(subNonNaN);
        sectors(k).NY  = length(sectors(k).x);
        sectors(k).sub = 1:sectors(k).NY;
        if isfield(sectors, 'ymedian') && ~isempty(sectors(k).ymedian)
            sectors(k).ymedian = sectors(k).ymedian(subNonNaN);
        end
    end
    bilan = sectors(flagSectors); % Rajout� par JMA le 12/04/2019
    CourbesStatistiques(subCourbes(iCourbe)).bilan{1} = bilan;
end
CourbesStatistiquesOut = CourbesStatistiques;

%% The End

flag = 1;
