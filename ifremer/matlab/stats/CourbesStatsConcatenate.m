function CourbesStatistiquesOut = CourbesStatsConcatenate(CourbesStatistiques1, CourbesStatistiques2)

if isempty(CourbesStatistiques1)
    CourbesStatistiquesOut = CourbesStatistiques2;
else
    if isempty(CourbesStatistiques2)
        CourbesStatistiquesOut = CourbesStatistiques1;
    else
        CourbesStatistiquesOut.bilan = CourbesStatistiques1(1).bilan;
        for k=2:length(CourbesStatistiques1)
            CourbesStatistiquesOut(end+1,1).bilan = CourbesStatistiques1(k).bilan; %#ok<AGROW>
        end
        for k=1:length(CourbesStatistiques2)
            CourbesStatistiquesOut(end+1,1).bilan = CourbesStatistiques2(k).bilan; %#ok<AGROW>
        end
    end
end

