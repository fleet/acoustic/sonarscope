function [flag, CourbesStatistiquesOut] = CourbesStatsCreateCurveFromResiduals(CourbesStatistiquesIn)

CourbesStatistiquesOut = CourbesStatistiquesIn;

for k1=1:length(CourbesStatistiquesIn)
    for k2=1:length(CourbesStatistiquesIn(k1).bilan)
        for k3=1:length(CourbesStatistiquesIn(k1).bilan{k2})
            if isempty(CourbesStatistiquesIn(k1).bilan{k2}(k3).residuals)
                flag = 0;
                return
            end
            CourbesStatistiquesOut(k1).bilan{k2}(k3).y = CourbesStatistiquesIn(k1).bilan{k2}(k3).residuals;
            CourbesStatistiquesOut(k1).bilan{k2}(k3).residuals = [];
            CourbesStatistiquesOut(k1).bilan{k2}.nomZoneEtude  = [CourbesStatistiquesOut(k1).bilan{k2}.nomZoneEtude ' - residuals'];
        end
    end
end
flag = 1;
