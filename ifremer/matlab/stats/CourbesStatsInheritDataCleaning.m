function CourbesStatistiquesOut = CourbesStatsInheritDataCleaning(CourbesStatistiquesRef, CourbesStatistiquesIn)

CourbesStatistiquesOut = CourbesStatistiquesIn;

%% Test si les courbes CourbesStatistiquesRef et CourbesStatistiquesIn sont compatibles

n1Ref = length(CourbesStatistiquesRef);
n1In  = length(CourbesStatistiquesIn);
if n1Ref ~= n1In
    str1 = sprintf('CourbesStatsInheritDataCleaning : Le nombre de courbes de la r�f�rence (%d) n''est pas le m�me que la courbe h�riti�re (%d). L''h�ritage ne peut pas se faire.', n1Ref, n1In);
    str2 = sprintf('CourbesStatsInheritDataCleaning : The number of input curves (%d) is not the same as the reference ones (%d). No inheritage is done', n1Ref, n1In);
    my_warndlg(Lang(str1,str2), 1);
    return
end

for k1=1:length(CourbesStatistiquesIn)
    n2Ref = length(CourbesStatistiquesRef(k1).bilan);
    n2In  = length(CourbesStatistiquesIn( k1).bilan);
    if n2Ref ~= n2In
        str1 = sprintf('CourbesStatsInheritDataCleaning : Le nombre de courbes de la r�f�rence (%d) n''est pas le m�me que la courbe h�riti�re (%d). L''h�ritage ne peut pas se faire.', n2Ref, n2In);
        str2 = sprintf('CourbesStatsInheritDataCleaning : The number of input curves (%d) is not the same as the reference ones (%d). No inheritage is done', n2Ref, n2In);
        my_warndlg(Lang(str1,str2), 1);
        return
    end
end

for k1=1:length(CourbesStatistiquesIn)
    for k2=1:length(CourbesStatistiquesIn(k1).bilan)
        n3Ref = length(CourbesStatistiquesRef(k1).bilan{k2});
        n3In  = length(CourbesStatistiquesIn( k1).bilan{k2});
        if n3Ref ~= n3In
            str1 = sprintf('CourbesStatsInheritDataCleaning : Le nombre de courbes de la r�f�rence (%d) n''est pas le m�me que la courbe h�riti�re (%d). L''h�ritage ne peut pas se faire.', n3Ref, n3In);
            str2 = sprintf('CourbesStatsInheritDataCleaning : The number of input curves (%d) is not the same as the reference ones (%d). No inheritage is done', n3Ref, n3In);
            my_warndlg(Lang(str1,str2), 1);
            return
        end
    end
end

%%

for k1=1:length(CourbesStatistiquesIn)
    for k2=1:length(CourbesStatistiquesIn(k1).bilan)
        for k3=1:length(CourbesStatistiquesIn(k1).bilan{k2})
            xClean = CourbesStatistiquesRef(k1).bilan{k2}(k3).x;
            x      = CourbesStatistiquesIn( k1).bilan{k2}(k3).x;
            [~, ia] = setdiff(x, xClean);
            CourbesStatistiquesOut(k1).bilan{k2}(k3).x(ia) = [];
            CourbesStatistiquesOut(k1).bilan{k2}(k3).y(ia) = [];
            CourbesStatistiquesOut(k1).bilan{k2}(k3).ymedian(ia) = [];
            CourbesStatistiquesOut(k1).bilan{k2}(k3).sub = 1:length(CourbesStatistiquesOut(k1).bilan{k2}(k3).x);
            CourbesStatistiquesOut(k1).bilan{k2}(k3).nx(ia) = [];
            CourbesStatistiquesOut(k1).bilan{k2}(k3).std(ia) = [];
        end
    end
end
