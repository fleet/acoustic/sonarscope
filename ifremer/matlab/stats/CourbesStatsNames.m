% TODO : cette fonction est une "extraction" du code de CourbesStatsPlot.m

function strLegende = CourbesStatsNames(bilan, varargin)

if iscell(bilan)
    bilan = bilan{1};
end

for k=1:length(bilan)
    if isnumeric(bilan(k).nomZoneEtude)
        bilan(k).nomZoneEtude = num2str(bilan(k).nomZoneEtude);
    end
end

NY = size(bilan);
if (length(NY) == 1) || (NY(2) == 1) % TODO : apparemment la condition (length(NY) == 1) est toujours vraie : serait-ce (length(NY(1)) == 1) ?
    nbDim = 1;
else
    nbDim = 2;
end

if nbDim == 1
    % Avant le 19/06/2019
    %     strLegende{1} = [bilan.Tag ' - ' bilan(1).nomZoneEtude];
    %     strLegende{1} = strrep(strLegende{1}, 'BS - BS', 'BS'); % Verrue ajout� le 06/12/2015 pour calib SurveyProcessing .all
    
    % Apr�s le 19/06/2019
    strLegende = [];
    for kSegment=1:length(bilan)
        if isempty(bilan(kSegment).x)
            continue
        end
        
        try % modif JMA le 19/06/2019
            strDataTypeConditions = [bilan(kSegment).DataTypeConditions{1}];
            strIdent = [bilan(kSegment).nomZoneEtude '-' bilan(kSegment).DataTypeValue '-' strDataTypeConditions];
        catch % Ce qui existait avant
            strIdent = [bilan(kSegment).nomVarCondition ' : ' num2str(bilan(kSegment).numVarCondition)];
        end
        strLegende{end+1} = strIdent; %#ok<AGROW>
        
        if isfield(bilan(kSegment), 'model') && ~isempty(bilan(kSegment).model)
            model = bilan(kSegment).model;
            nomModel = model.Name;
            strLegende{end+1} = [bilan(1).nomZoneEtude ' - Model ' nomModel]; %#ok
        end
    end
    
else
    
    strLegende = [];
    for kSegment=1:length(bilan)
        if isempty(bilan(kSegment).x)
            continue
        end
        
        try % modif JMA le 19/06/2019
            strDataTypeConditions = '';
            for k2=1:length(bilan(kSegment).DataTypeConditions)
                strDataTypeConditions = [strDataTypeConditions bilan(kSegment).DataTypeConditions{k2} '-']; %#ok<AGROW>
            end
%             if isfield(bilan, 'nomZoneEtude')
                strIdent = [bilan(kSegment).nomZoneEtude '-' bilan(kSegment).DataTypeValue '-' strDataTypeConditions num2str(bilan(kSegment).numVarCondition)];
%             else
%                 strIdent = [bilan(kSegment).nomZoneEtude '-' bilan(kSegment).DataTypeValue '-' strDataTypeConditions num2str(bilan(kSegment).numVarCondition)];
%             end
        catch % Ce qui existait avant
            strIdent = [bilan(kSegment).nomVarCondition ' : ' num2str(bilan(kSegment).numVarCondition)];
        end
        strLegende{end+1} = strIdent; %#ok<AGROW>
        
        if isfield(bilan(kSegment), 'model') && ~isempty(bilan(kSegment).model)
            model = bilan(kSegment).model;
            nomModel = model.Name;
            strLegende{end+1} = [bilan(1).nomZoneEtude ' - Model ' nomModel]; %#ok
        end
    end
end
