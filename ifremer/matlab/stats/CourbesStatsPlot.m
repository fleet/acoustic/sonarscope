% TODO : � refaire compl�tement quand les stats deviendront une classe

function [strLegende, fig, fig2, strLegende2] = CourbesStatsPlot(bilan, varargin)

[varargin, fig]           = getPropertyValue(varargin, 'Fig',           []);
[varargin, fig2]          = getPropertyValue(varargin, 'Fig2',          []);
[varargin, flagStats]     = getPropertyValue(varargin, 'Stats',         1); % 0=moyenne seule, 1=Moyenne+1*Sigma, 2=Sigma seul, 3=Moyenne+2*Sigma
[varargin, iCoul]         = getPropertyValue(varargin, 'iCoul',         []);
[varargin, Coul]          = getPropertyValue(varargin, 'Coul',          []);
[varargin, flagMedian]    = getPropertyValue(varargin, 'Median',        0);
[varargin, Offsets]       = getPropertyValue(varargin, 'Offsets',       []);
[varargin, LineStyle]     = getPropertyValue(varargin, 'LineStyle',     '-');
[varargin, LineWidth]     = getPropertyValue(varargin, 'LineWidth',     0.5);
[varargin, Marker]        = getPropertyValue(varargin, 'Marker',        'none');
[varargin, subTypeCurve]  = getPropertyValue(varargin, 'subTypeCurve',  1:2); % 1:courbes, 2=R�sidus
[varargin, TitleAxeCurve] = getPropertyValue(varargin, 'TitleAxeCurve', []);
[varargin, Mute]          = getPropertyValue(varargin, 'Mute',          0);
[varargin, Position]      = getPropertyValue(varargin, 'Position',      []); %#ok<ASGLU>

strLegende  = {};
strLegende2 = {};

titreV = bilan{1}(1).titreV;
% NY     = bilan{1}(1).NY;
nomX   = bilan{1}(1).nomX;

for k=1:length(bilan{1})
    if isnumeric(bilan{1}(k).nomZoneEtude)
        bilan{1}(k).nomZoneEtude = num2str(bilan{1}(k).nomZoneEtude);
    end
end

if isfield(bilan{1}(1), 'TypeCourbeStat')
    flagCourbeBiais = strcmp(bilan{1}(1).TypeCourbeStat, 'For Bias Compensation');
    if flagCourbeBiais
        strBiais = sprintf('(Biais : %f)', bilan{1}(1).Biais);
    else
        strBiais = '';
    end
else
    strBiais = '';
end

if flagStats == 2
    strTypeCourbe = 'Standard deviation';
    strBiais = '';
else
    if flagMedian
        strTypeCourbe = 'Median curve';
    else
        if strcmp(bilan{1}(1).Unit, 'dB') && isfield(bilan{1}(1), 'MeanCompType')
%             % TODO : attention. Cette liste est d�finie � plusieurs endroits (Ex : question_MeanCompType)
%             strMeanCompType{1} = 'dB';
%             strMeanCompType{2} = 'Amplitude';
%             strMeanCompType{3} = 'Energy';
%             strMeanCompType{4} = 'Median';
            if isfield(bilan{1}(1), 'strMeanCompType')
                strMeanCompType = bilan{1}(1).strMeanCompType;
            else
                strMeanCompType = {bilan{1}(1).Unit; 'Amp'; 'Enr'; 'Median'};
            end
            strTypeCourbe = sprintf('Mean curve (computed in %s)', strMeanCompType{bilan{1}(1).MeanCompType});
        else
            strTypeCourbe = 'Mean curve';
        end
    end
end

if isempty(Coul)
    strCoul = 'brgmkc';
    nbCoul = length(strCoul);
    for k=1:nbCoul
        Coul(k,:) = coul2mat(strCoul(k));
    end
else
    nbCoul = size(Coul, 1);
end

NY = size(bilan{1});
if (length(NY) == 1) || (NY(2) == 1)
    nbDim = 1;
else
    nbDim = 2;
end

if nbDim == 1
    if isempty(iCoul)
        iCoul = length(findobj(fig, 'Type', 'line'));
    end
    iCoul = 1 + mod(iCoul, nbCoul);
    
    if isfield(bilan{1}(1), 'Color') && ~isempty(bilan{1}(1).Color)
        Color = bilan{1}(1).Color;
    else
        Color = Coul(iCoul,:);
    end
    
    if any(subTypeCurve == 1) || any(subTypeCurve == 2)
        if isempty(fig)
            fig = FigUtils.createSScFigure('Position', Position);
        else
            FigUtils.createSScFigure(fig);
        end
        hold on;
        if flagStats == 2
            PlotUtils.createSScPlot(bilan{1}.x(:), bilan{1}.std(:), 'Color', Color, ...
                    'LineStyle', LineStyle, 'LineWidth', LineWidth, 'Marker', Marker);
            strLegende{1} = ['Standard deviation - ' bilan{1}(1).nomZoneEtude];
            W = axis;
            if W(3) > 0 % On ne change pas la valeur min si l'�cart type est n�gatif, ce qui peut �tre le cas si les stats on �t� calcul�es en energie sur des donn�es transmises en dB
                W(3) = 0;
            end
            axis(W)
        else
            if any(subTypeCurve == 1)
                PlotUtils.createSScPlot(bilan{1}.x(:), bilan{1}.y(:), 'Color', Color, ...
                    'LineStyle', LineStyle, 'LineWidth', LineWidth, 'Marker', Marker);
                
                if isfield(bilan{1}, 'Unit') && ~isempty(bilan{1}.Unit)
                    ylabel(bilan{1}.Unit)
                end
            end
            strLegende{1} = [bilan{1}.Tag ' - ' bilan{1}(1).nomZoneEtude];
            strLegende{1} = strrep(strLegende{1}, 'BS - BS', 'BS'); % Verrue ajout� le 06/12/2015 pour calib SurveyProcessing .all
        end
        grid on; box on;
        
        if flagStats == 1
            h = PlotUtils.createSScPlot(bilan{1}.x, bilan{1}.y(:) + bilan{1}.std(:), 'Color', Color, 'LineWidth', LineWidth);
            set(h, 'LineStyle', '--');
            h = PlotUtils.createSScPlot(bilan{1}.x, bilan{1}.y(:) - bilan{1}.std(:), 'Color', Color, 'LineWidth', LineWidth);
            set(h, 'LineStyle', '--');
            strLegende{2} = [bilan{1}(1).nomZoneEtude ' + sigma'];
            strLegende{3} = [bilan{1}(1).nomZoneEtude ' - sigma'];
        end
        if flagStats == 3
            h = PlotUtils.createSScPlot(bilan{1}.x, bilan{1}.y(:) + 2*bilan{1}.std(:), 'Color', Color, 'LineWidth', LineWidth);
            set(h, 'LineStyle', '--');
            h = PlotUtils.createSScPlot(bilan{1}.x, bilan{1}.y(:) - 2*bilan{1}.std(:), 'Color', Color, 'LineWidth', LineWidth);
            set(h, 'LineStyle', '--');
            strLegende{2} = [bilan{1}.nomZoneEtude ' + 2*sigma'];
            strLegende{3} = [bilan{1}.nomZoneEtude ' - 2*sigma'];
        end
        
        if isfield(bilan{1}, 'nomZoneEtude')
            strIdent = [titreV ' / ' nomX ' / ' bilan{1}.nomZoneEtude];
        else
            strIdent = [titreV ' / ' nomX];
        end
        cmenu = uicontextmenu;
        uimenu(cmenu, 'Text', strIdent);
        
        xlabel(nomX, 'interpreter', 'none');
        %         ylabel(titreV, 'interpreter', 'none');
        
        if isempty(TitleAxeCurve)
            Titre = sprintf('%s\n%s : %s    %s', titreV, strTypeCourbe, bilan{1}.nomZoneEtude, strBiais);
        else
            Titre = TitleAxeCurve;
        end
        title(Titre, 'interpreter', 'none')
        
        %         % --------------------
        %         % Plot de l'ecart-type
        %
        %         figure(fig+1); hold on; grid on; box on;
        %         PlotUtils.createSScPlot(bilan{1}.x, bilan{1}.std, 'Color', Color);
        %         uimenu(cmenu, 'Text', strIdent);
        %         xlabel(nomX, 'interpreter', 'none');
        %         title(['Ecart-type - ' titreV], 'interpreter', 'none')
        
        if isfield(bilan{1}, 'model') && (flagStats ~= 2) && ~isempty(bilan{1}.model) && any(subTypeCurve == 2)
            model = bilan{1}.model;
            x = model.XData;
            y = model.compute;
            nomModel = model.Name;
            nomParams   = model.getParamsName;
            valParams   = model.getParamsValue;
            uniteParams = model.getParamsUnit;
            
            h = PlotUtils.createSScPlot(x, y, 'Color', Color, 'LineWidth', LineWidth);
            couleur = get(h, 'Color');
            set(h, 'Color', couleur * 0.5);
            strLegende{end+1} = [bilan{1}.nomZoneEtude ' - Model ' nomModel];
            for iParam=1:length(nomParams)
                if isempty(uniteParams{iParam})
                    str = sprintf('%s \t: %f', nomParams{iParam}, valParams(iParam));
                else
                    str = sprintf('%s \t: %f (%s)', nomParams{iParam}, valParams(iParam), uniteParams{iParam});
                end
                disp(str)
                uimenu(cmenu, 'Text', str);
            end
        end
    end
    
    % TODO : faudrait optimiser l'affaire : pb pos�: affichage d'une
    % fen�tre destin�e � la courbe m�me si pas de courbe ni mod�le demand�
    % et manque de
    if isfield(bilan{1}, 'model') && (flagStats ~= 2) && ~isempty(bilan{1}.model) && any(subTypeCurve == 3)
        model = bilan{1}.model;
        x = model.XData;
        %             y = model.compute;
        residuals = model.residuals;
        
        if ~isempty(residuals)
            if isempty(fig2)
                fig2 = FigUtils.createSScFigure('Position', Position);
            else
                FigUtils.createSScFigure(fig2);
            end
%             hAxe = subplot(1,1,1); hold on; 
            subplot(1,1,1); hold on; 
            
            PlotUtils.createSScPlot(x, residuals); grid on; hold on;
            xlabel(nomX, 'interpreter', 'none');
            ylabel('Residuals');
            
            Titre = sprintf('%s\n%s : Residuals   %s', titreV, bilan{1}.nomZoneEtude, strBiais);
            title(Titre, 'interpreter', 'none')
            
            strLegende2{1} = [bilan{1}.Tag ' - ' bilan{1}.nomZoneEtude ' - residuals'];
        end
    end
    
else
    if any(subTypeCurve == 1) || (any(subTypeCurve == 2) && isfield(bilan{1}(1), 'model') && ~isempty(bilan{1}(1).model))
        hAxe = preallocateGraphics(0,0);
        strLegende = [];
        
        for k=1:(nbDim-1)
            if isempty(fig)
                fig = FigUtils.createSScFigure('Position', Position);
            else
                FigUtils.createSScFigure(fig);
            end
            hold on;
            
            if isempty(TitleAxeCurve)
                Titre = sprintf('%s\n%s    %s', bilan{k}(1).nomVarCondition, strTypeCourbe, strBiais);
            else
                Titre = TitleAxeCurve;
            end
            title(Titre, 'interpreter', 'none')
            
            for kSegment=1:length(bilan{k})
                if isempty(bilan{k}(kSegment).x)
                    continue
                end
                iCoul = 1 + mod(kSegment-1, nbCoul);
                
                if isfield(bilan{k}(1), 'Color') && ~isempty(bilan{k}(1).Color)
                    Color = bilan{k}(kSegment).Color;
                else
                    Color = Coul(iCoul,:);
                end
                
                hAxe = subplot(1,1,1); hold on; grid on;
                
                if any(subTypeCurve == 1)
                    if flagStats == 1 % si l'�cart type est n�gatif, c'est que l'on est dans le cas o� les stats on �t� calcul�es en energie sur des donn�es transmises en dB
                        m = bilan{k}(kSegment).y;
                        s = bilan{k}(kSegment).std;
                        m = m(:)';
                        s = s(:)';
                        if any(bilan{k}(kSegment).std < 0)
                            y1 = reflec_Enr2dB(reflec_dB2Enr(m) + reflec_dB2Enr(s));
                            y2 = reflec_Enr2dB(reflec_dB2Enr(m) - reflec_dB2Enr(s));
                        else % Cas normal
                            y1 = m + s;
                            y2 = m - s;
                        end
                        
                        h = PlotUtils.createSScPlot(bilan{k}(kSegment).x, y1, 'Color', Color, 'LineWidth', LineWidth);
                        set(h, 'LineStyle', '--');
                        h = PlotUtils.createSScPlot(bilan{k}(kSegment).x, y2, 'Color', Color, 'LineWidth', LineWidth);
                        set(h, 'LineStyle', '--');
                        if isfield(bilan{1}, 'nomZoneEtude')
                            strLegende{end+1} = [bilan{1}(kSegment).nomZoneEtude ' + sigma']; %#ok
                            strLegende{end+1} = [bilan{1}(kSegment).nomZoneEtude ' - sigma']; %#ok
                        else
                            strLegende{end+1} = '+ sigma'; %#ok
                            strLegende{end+1} = '- sigma'; %#ok
                        end
                    elseif flagStats == 3 % si l'�cart type est n�gatif, c'est que l'on est dans le cas o� les stats on �t� calcul�es en energie sur des donn�es transmises en dB
                        m = bilan{k}(kSegment).y;
                        s = bilan{k}(kSegment).std;
                        m = m(:)';
                        s = s(:)';
                        if any(bilan{k}(kSegment).std < 0)
                            y1 = reflec_Enr2dB(reflec_dB2Enr(m) + 2*reflec_dB2Enr(s));
                            y2 = reflec_Enr2dB(reflec_dB2Enr(m) - 2*reflec_dB2Enr(s));
                        else % Cas normal
                            y1 = m + s;
                            y2 = m - s;
                        end
                        
                        h = PlotUtils.createSScPlot(bilan{k}(kSegment).x, y1, 'Color', Color, 'LineWidth', LineWidth);
                        set(h, 'LineStyle', '--');
                        h = PlotUtils.createSScPlot(bilan{k}(kSegment).x, y2, 'Color', Color, 'LineWidth', LineWidth);
                        set(h, 'LineStyle', '--');
                        if isfield(bilan{1}, 'nomZoneEtude')
                            strLegende{end+1} = [bilan{1}(kSegment).nomZoneEtude ' + 2*sigma']; %#ok
                            strLegende{end+1} = [bilan{1}(kSegment).nomZoneEtude ' - 2*sigma']; %#ok
                        else
                            strLegende{end+1} = '+ sigma'; %#ok
                            strLegende{end+1} = '- sigma'; %#ok
                        end
                    end
                    
                    if flagStats == 2
                        PlotUtils.createSScPlot(bilan{k}(kSegment).x, bilan{k}(kSegment).std, 'Color', Color, 'LineWidth', LineWidth);
                        strIdent = [bilan{k}(kSegment).nomVarCondition ' : ' num2str(bilan{k}(kSegment).numVarCondition)];
                        strLegende{end+1} = ['Standard deviation - ' bilan{k}(kSegment).nomZoneEtude ' - ' strIdent]; %#ok
                    else
                        if flagMedian == 1
                            if isfield(bilan{1}, 'ymedian')
                                PlotUtils.createSScPlot(bilan{k}(kSegment).x, bilan{k}(kSegment).ymedian, 'Color', Color, 'LineWidth', LineWidth);
                            else
                                str1 = 'Cette courbe semble venir d''une version pr�c�dente de SonarSCope, la valeur m�diane n''existe pas dans cette courbe.';
                                str2 = 'This curve seems coming from a previous SonarScope version, the median curve does not exist.';
                                my_warndlg(Lang(str1,str2), 1);
                                strLegende = '';
                                return
                            end
                        else
                            if isempty(Offsets)
                                offsetCourbe = 0;
                            else
                                if length(Offsets) >= kSegment
                                    offsetCourbe = Offsets(kSegment);
                                else
                                    offsetCourbe = 0;
                                end
                                %                         PlotUtils.createSScPlot(-bilan{k}(kSegment).x, bilan{k}(kSegment).y + offsetCourbe, 'Color', [0.5 0.5 0.5]);
                            end
                            hc = PlotUtils.createSScPlot(bilan{k}(kSegment).x, bilan{k}(kSegment).y + offsetCourbe, 'Color', Color, 'LineWidth', LineWidth); %#ok<NASGU>
%                             set(hc, 'Name', Titre);
                            %                             hPlot = PlotUtils.createSScPlot(bilan{k}(kSegment).x, bilan{k}(kSegment).y + offsetCourbe, 'Color', Color, 'LineWidth', LineWidth, 'Name', Titre);
                            %                     if ~isempty(Offsets)
                            %                         set(hPlot, 'LineWidth', 3)
                            %                     end
                        end
                        strIdent = [bilan{k}(kSegment).nomVarCondition ' : ' num2str(bilan{k}(kSegment).numVarCondition)];
                        if isfield(bilan{1}, 'nomZoneEtude')
                            strLegende{end+1} = [bilan{k}(1).nomZoneEtude ' - ' strIdent]; %#ok
                        else
                            strLegende{end+1} = strIdent; %#ok
                        end
                    end
                    % Instruction non verifiee
                    cmenu = uicontextmenu;
                    uimenu(cmenu, 'Text', strIdent);
                    strInfo = ['Centre de ' nomX ' : ' num2str(median(bilan{k}(kSegment).x))];
                    uimenu(cmenu, 'Text', strInfo);
                    
                    xlabel(nomX, 'interpreter', 'none');
                    %                 ylabel(titreV, 'interpreter', 'none')
                    grid on; box on; axis tight;
                    
                    if flagStats == 2
                        W = axis;
                        if W(3) > 0 % On ne change pas la valeur min si l'�cart type est n�gatif, ce qui peut �tre le cas si les stats on �t� calcul�es en energie sur des donn�es transmises en dB
                            W(3) = 0;
                        end
                        axis(W)
                    end
                end
                
                if isfield(bilan{1}(kSegment), 'model') && ~isempty(bilan{1}(kSegment).model) && any(subTypeCurve == 2)
                    model = bilan{1}(kSegment).model;
                    x = model.XData;
                    y = model.compute;
                    nomModel = model.Name;
                    nomParams   = model.getParamsName;
                    valParams   = model.getParamsValue;
                    uniteParams = model.getParamsUnit;
                    
                    %                     x = get(bilan{1}(kSegment).model, 'XData');
                    %                     y = compute(bilan{1}(kSegment).model);
                    h = PlotUtils.createSScPlot(x, y, 'Color', Color, 'LineWidth', LineWidth);
                    couleur = get(h, 'Color');
                    set(h, 'Color', couleur * 0.5);
                    %                     nomModel = get(bilan{1}(kSegment).model, 'nom');
                    strLegende{end+1} = [bilan{1}(1).nomZoneEtude ' - Model ' nomModel]; %#ok
                    %                     nomParams   = get_nomParams(bilan{1}(kSegment).model);
                    %                     valParams   = get_valParams(bilan{1}(kSegment).model);
                    %                     uniteParams = get_uniteParams(bilan{1}(kSegment).model);
                    cmenu = uicontextmenu; % Ajoutt JMA le 02/08/2021 suite � mail de Ridha
                    for iParam=1:length(nomParams)
                        if isempty(uniteParams{iParam})
                            str = sprintf('%s \t: %f', nomParams{iParam}, valParams(iParam));
                        else
                            str = sprintf('%s \t: %f (%s)', nomParams{iParam}, valParams(iParam), uniteParams{iParam});
                        end
                        %                     disp(str)
                        uimenu(cmenu, 'Text', str);
                    end
                end
            end
        end
        if isempty(hAxe) && Mute
            str1 = 'Il n''y a pas de courbe � tracer ici';
            str2 = 'No curve to display here';
            my_warndlg(Lang(str1,str2), 1);
        else
            if isempty(TitleAxeCurve)
                if isfield(bilan{1}, 'nomZoneEtude')
                    Titre = sprintf('%s\n%s : %s    %s', titreV, strTypeCourbe, bilan{1}(1).nomZoneEtude, strBiais);
                    title(Titre, 'interpreter', 'none')
                else
                    Titre = sprintf('%s\n%s    %s', titreV, strTypeCourbe, strBiais);
                end
            else
                Titre = TitleAxeCurve;
            end
            title(Titre, 'interpreter', 'none')
        end
    end
    
    if any(subTypeCurve == 3) % Ajout JMA le 04/10/2015
        
        % Apparemment on ne passe jamais par l�. Supprimer cette partie si
        % confirm�
        
        % ----------------------------------------------------------------
        % Impose un beakpoint ici pour rep�rer les appels � cette fonction
%         my_breakpoint('FctName', 'CourbesStatsPlot');
        % ----------------------------------------------------------------

        
        cmenu = uicontextmenu; % Ajoutt JMA le 02/08/2021 suite � mail de Ridha

        hAxe = preallocateGraphics(0,0);
        strLegende = [];
        
        for k=1:(nbDim-1)
            if isempty(fig2)
                fig2 = FigUtils.createSScFigure('Position', Position);
            else
                FigUtils.createSScFigure(fig2);
            end
            hold on;
            
            Titre = sprintf('%s\n%s    %s', bilan{k}(1).nomVarCondition, strTypeCourbe, strBiais);
            title(Titre, 'interpreter', 'none')
            
            for kSegment=1:length(bilan{k})
                if isempty(bilan{k}(kSegment).x)
                    continue
                end
                iCoul = 1 + mod(kSegment-1, nbCoul);
                
                if isfield(bilan{k}(1), 'Color') && ~isempty(bilan{k}(1).Color)
                    Color = bilan{k}(kSegment).Color;
                else
                    Color = Coul(iCoul,:);
                end
                
                hAxe = subplot(1,1,1); hold on;
                if isfield(bilan{1}(kSegment), 'model') && ~isempty(bilan{1}(kSegment).model) && any(subTypeCurve == 3)
                    model = bilan{1}(kSegment).model;
                    x = model.XData;
                    %                         y = model.compute;
                    nomModel = model.Name;
                    nomParams   = model.getParamsName;
                    valParams   = model.getParamsValue;
                    uniteParams = model.getParamsUnit;
                    residuals = model.residuals;
                    
                    %                     x = get(bilan{1}(kSegment).model, 'XData');
                    %                     y = get(bilan{1}(kSegment).model, 'YData');
                    hold on; grid on;
                    h = PlotUtils.createSScPlot(x, residuals, 'Color', Color, 'LineWidth', LineWidth);
                    couleur = get(h, 'Color');
                    set(h, 'Color', couleur * 0.5);
                    %                     nomModel = get(bilan{1}(kSegment).model, 'nom');
                    strLegende{end+1} = [bilan{1}(1).nomZoneEtude ' - Model ' nomModel]; %#ok
                    %                     nomParams   = get_nomParams(bilan{1}(kSegment).model);
                    %                     valParams   = get_valParams(bilan{1}(kSegment).model);
                    %                     uniteParams = get_uniteParams(bilan{1}(kSegment).model);
                    for iParam=1:length(nomParams)
                        if isempty(uniteParams{iParam})
                            str = sprintf('%s \t: %f', nomParams{iParam}, valParams(iParam));
                        else
                            str = sprintf('%s \t: %f (%s)', nomParams{iParam}, valParams(iParam), uniteParams{iParam});
                        end
                        %                     disp(str)
                        uimenu(cmenu, 'Text', str);
                    end
                    
                    strIdent = [bilan{k}(kSegment).nomVarCondition ' : ' num2str(bilan{k}(kSegment).numVarCondition)];
                    if isfield(bilan{1}, 'nomZoneEtude')
                        strLegende2{end+1} = [bilan{k}(1).nomZoneEtude ' - ' strIdent]; %#ok
                    else
                        strLegende2{end+1} = strIdent; %#ok
                    end
                end
            end
        end
        if isempty(hAxe) && Mute
            str1 = 'Il n''y a pas de courbe � tracer ici';
            str2 = 'No curve to display here';
            my_warndlg(Lang(str1,str2), 1);
        else
            if isfield(bilan{1}, 'nomZoneEtude')
                Titre = sprintf('%s\n%s : %s    %s', titreV, strTypeCourbe, bilan{1}(1).nomZoneEtude, strBiais);
                title(Titre, 'interpreter', 'none')
            else
                Titre = sprintf('%s\n%s    %s', titreV, strTypeCourbe, strBiais);
                title(Titre, 'interpreter', 'none')
            end
        end
    end
end
