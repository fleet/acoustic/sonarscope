function [CourbesStatistiquesOut, nbCurvesCreated] = CourbesStatsUnion(CourbesStatistiques, varargin)

[varargin, nbPMin]               = getPropertyValue(varargin, 'nbPMin',               10);
[varargin, nomCourbe]            = getPropertyValue(varargin, 'nomCourbe',            []);
[varargin, sub]                  = getPropertyValue(varargin, 'sub',                  1:length(CourbesStatistiques));
[varargin, TypeWeight]           = getPropertyValue(varargin, 'TypeWeight',           1);
[varargin, TypeCurve]            = getPropertyValue(varargin, 'TypeCurve',            1);
[varargin, MeanCompType]         = getPropertyValue(varargin, 'MeanCompType',         1);
[varargin, CurveForCompensation] = getPropertyValue(varargin, 'CurveForCompensation', 1); %#ok<ASGLU>

CourbesStatistiquesOut = CourbesStatistiques;
nbCurvesCreated        = 0;

nbCourbesStatsBegin = length(CourbesStatistiques);
if length(CourbesStatistiques) == 1
    return
end

%% Recherche des signatures

Biais              = [];
Unit               = [];
DataTypeValue      = [];
DataTypeConditions = [];
Support            = [];
GeometryType       = [];
TagSup             = [];
Mode               = [];

Tag = [];
for k=1:length(sub)
    k1 = sub(k);
    if ~isempty(CourbesStatistiques(k1).bilan)
        if isempty(Tag)
            Tag = CourbesStatistiques(k1).bilan{1}.Tag;
        end
        for k2=1:length(CourbesStatistiques(k1).bilan{1})
            if isempty(Biais)
                Biais = CourbesStatistiques(k1).bilan{1}(k2).Biais;
            end
            if isempty(Unit)
                Unit = CourbesStatistiques(k1).bilan{1}(k2).Unit;
            end
            if isempty(DataTypeValue)
                DataTypeValue = CourbesStatistiques(k1).bilan{1}(k2).DataTypeValue;
            end
            if isempty(DataTypeConditions)
                DataTypeConditions = CourbesStatistiques(k1).bilan{1}(k2).DataTypeConditions;
                if strcmp(DataTypeConditions, 'IncidenceAngle')
                    Tag = 'BS';
                end
            end
            if isempty(Support)
                Support = CourbesStatistiques(k1).bilan{1}(k2).Support;
            end
            if isempty(GeometryType)
                GeometryType = CourbesStatistiques(k1).bilan{1}(k2).GeometryType;
            end
            if isempty(TagSup)
                TagSup = CourbesStatistiques(k1).bilan{1}(k2).TagSup;
            end
            if isempty(Mode) && isfield(CourbesStatistiques(k1).bilan{1}(k2), 'Mode')
                Mode = CourbesStatistiques(k1).bilan{1}(k2).Mode;
            end
        end
    end
end

%% Assemblage

bilan1 = cell(0, length(CourbesStatistiques(1).bilan{1}));
for k=1:length(sub)
    k1 = sub(k);
    if ~isempty(CourbesStatistiques(k1).bilan)
        OKPourFusionner = 0;
        for k2=1:length(CourbesStatistiques(k1).bilan{1})
            if ~isempty(Biais) && (CourbesStatistiques(k1).bilan{1}(k2).Biais ~= Biais)
                continue
            end
            if ~isempty(Unit) && ~strcmp(CourbesStatistiques(k1).bilan{1}(k2).Unit, Unit)
                continue
            end
            if ~isempty(DataTypeValue) && ~strcmp(CourbesStatistiques(k1).bilan{1}(k2).DataTypeValue, DataTypeValue)
                continue
            end
            if ~isempty(GeometryType)
                if ischar(GeometryType) % Normalement
                    if ~strcmp(CourbesStatistiques(k1).bilan{1}(k2).GeometryType, GeometryType)
                        continue
                    end
                else % Trouver pourquoi on se trouve dans cette situation
                    if ~isempty(GeometryType) && (CourbesStatistiques(k1).bilan{1}(k2).GeometryType ~= GeometryType)
                        continue
                    end
                end
            end
            %             if ~isempty(TagSup) && ~strcmp(CourbesStatistiques(k1).bilan{1}(k2).TagSup, TagSup)
            if ~isempty(TagSup) && ~isequal(CourbesStatistiques(k1).bilan{1}(k2).TagSup, TagSup) % Modif JMA le 04/10/2015
                continue
            end
            if ~isempty(Mode) && isfield(CourbesStatistiques(k1).bilan{1}(k2), 'Mode') &&(CourbesStatistiques(k1).bilan{1}(k2).Mode ~= Mode)
                continue
            end
            if ~isempty(DataTypeConditions)
                for k3=1:length(DataTypeConditions)
                    if ~strcmp(CourbesStatistiques(k1).bilan{1}(k2).DataTypeConditions{k3}, DataTypeConditions{k3})
                        continue
                    end
                end
            end
            if ~isempty(Support)
                for k3=1:length(Support)
                    if ~strcmp(CourbesStatistiques(k1).bilan{1}(k2).Support{k3}, Support{k3})
                        continue
                    end
                end
            end
            OKPourFusionner = 1;
        end
        
        % OK, on peut fusionner
        
        if OKPourFusionner
            for k2=1:length(CourbesStatistiques(k1).bilan{1})
                if isfield(CourbesStatistiques(k1).bilan{1}, 'numVarCondition') && ~isempty(CourbesStatistiques(k1).bilan{1}(k2).numVarCondition)
                    numVarCondition = CourbesStatistiques(k1).bilan{1}(k2).numVarCondition;
                else
                    numVarCondition = 1;
                end
                if isempty(bilan1) || (numVarCondition > length(bilan1)) || isempty(bilan1{numVarCondition})
                    if TypeWeight == 2
                        CourbesStatistiques(k1).bilan{1}(k2).nx(:) = 1;
                    end
                    
                    bilan1{numVarCondition} = CourbesStatistiques(k1).bilan{1}(k2);
                    if TypeCurve == 2 % Residuals
                        bilan1{numVarCondition}.residuals = compute_residuals(bilan1{numVarCondition}.model, 'x', bilan1{numVarCondition}.x, 'y', bilan1{numVarCondition}.y);
                    end
                    
                else
                    if TypeWeight == 2
                        CourbesStatistiques(k1).bilan{1}(k2).nx(:) = 1;
                    end
                    if isempty(bilan1{numVarCondition})
                        bilan1{numVarCondition} = CourbesStatistiques(k1).bilan{1}(k2);
                    else
                        bilan1{numVarCondition} = CourbesStatsUnion_unitaire(bilan1{numVarCondition}, CourbesStatistiques(k1).bilan{1}(k2), 1, TypeCurve, MeanCompType);
                    end
                    %                 figure(6757); plot(bilan1{k2}.x, bilan1{k2}.y, '-'); grid on; hold on;
                    %                 figure(6758); plot(bilan1{k2}.x, bilan1{k2}.nx, '-'); grid on; hold on;
                    
                    %               if any(isnan(bilan1{k2}.y))
                    %                     messageForDebugInspection
                    %               end
                end
            end
        end
    end
end

% Bidouille pour corriger bug quand residuals est pr�sent dans certaines
% stats et pas d'autres
for k1=1:length(bilan1)
    if ~isfield(bilan1{k1}, 'residuals')
        for k2=1:length(bilan1{k1})
            bilan1{k1}(k2).residuals = [];
        end
    end
end

bilan1 = [bilan1{:}]; % Comment� le 14/02/2014 : Attention �a va faire boom

%% Suppression des points ayant moins de nbPMin contributeurs

if TypeWeight == 1
    for k=1:length(sub)
        k1 = sub(k);
        if ~isempty(CourbesStatistiques(k1).bilan)
            for k2=1:length(CourbesStatistiques(k1).bilan{1})
                subNx = find(CourbesStatistiques(k1).bilan{1}(k2).nx < nbPMin);
                if ~isempty(subNx)
                    CourbesStatistiques(k1).bilan{1}(k2).x(subNx)       = [];
                    CourbesStatistiques(k1).bilan{1}(k2).y(subNx)       = [];
                    CourbesStatistiques(k1).bilan{1}(k2).ymedian(subNx) = [];
                    CourbesStatistiques(k1).bilan{1}(k2).sub(subNx)     = [];
                    CourbesStatistiques(k1).bilan{1}(k2).nx(subNx)      = [];
                    CourbesStatistiques(k1).bilan{1}(k2).std(subNx)     = [];
                end
            end
        end
    end
end

%% Mise en forme

for k2=1:length(bilan1)
    if isfield(bilan1(k2), 'model')
        bilan1(k2).model = [];
    end
end

if isempty(nomCourbe)
    %     bilan1(1).nomZoneEtude = [bilan1(1).nomZoneEtude '_Union'];
    ind = strfind(CourbesStatistiques(sub(1)).bilan{1}(1).nomZoneEtude, ':');
    if isempty(ind)
        if isempty(bilan1(1).Tag)
            bilan1(1).nomZoneEtude = sprintf('Union of %d curves', length(sub));
        else
            bilan1(1).nomZoneEtude = sprintf('%s_Union_of_%d_curves', bilan1(1).Tag, length(sub));
        end
    else
        if isempty(Tag)
            bilan1(1).nomZoneEtude = sprintf('%s Union of %d curves', CourbesStatistiques(sub(1)).bilan{1}(1).nomZoneEtude(1:ind), length(sub));
        else
            bilan1(1).nomZoneEtude = sprintf('%s : %s Union of %d curves', Tag, CourbesStatistiques(sub(1)).bilan{1}(1).nomZoneEtude(1:ind), length(sub));
        end
    end
else
    bilan1(1).nomZoneEtude = nomCourbe;
end
bilan1(1).Tag = Tag;

CourbesStatistiquesOut(end+1) = CourbesStatistiques(sub(end));
CourbesStatistiquesOut(end).bilan{1} = bilan1;

%% Calcul d'une courbe de compensation

if CurveForCompensation
    sumy = 0;
    sumnx = 0;
    for k2=1:length(bilan1)
        y = bilan1(k2).y(:);
        nx = bilan1(k2).nx(:);
        subOK = find((nx ~= 0) & ~isnan(y));
        sumy  = sumy + sum(y(subOK) .* nx(subOK));
        sumnx = sumnx + sum(nx(subOK));
    end
    Bias = sumy / sumnx;
    for k2=1:length(bilan1)
        bilan1(k2).y =  bilan1(k2).y - Bias;
    end
    
    bilan1(1).nomZoneEtude = [bilan1(1).nomZoneEtude '_Compens'];
    CourbesStatistiquesOut(end+1) = CourbesStatistiquesOut(end);
    CourbesStatistiquesOut(end).bilan{1} = bilan1;
end
nbCourbesStatsEnd = length(CourbesStatistiquesOut);
nbCurvesCreated = nbCourbesStatsEnd - nbCourbesStatsBegin;


function bilan1 = CourbesStatsUnion_unitaire(bilan1, bilan2, TypeWeight, TypeCurve, MeanCompType)

% TODO : Si on travaille sur la r�flectivit� en dB, il faudrait calculer les
% moyennes pond�r�es sur l'�nergie et repasser en dB pour restituer les
% r�sultat

for k2=1:length(bilan2)
    if isempty(bilan2(k2).x) || all(isnan(bilan2(k2).y))
        continue
    end
    sub = isnan(bilan1(k2).y);
    bilan1(k2).nx(sub)  = 0;
    bilan1(k2).y(sub)   = 0;
    bilan1(k2).std(sub) = 0;
    if TypeCurve == 2
        bilan1(k2).residuals(sub) = 0;
    end
    sub = isnan(bilan2(k2).y);
    bilan2(k2).nx(sub)  = 0;
    bilan2(k2).y(sub)   = 0;
    bilan2(k2).std(sub) = 0;
    if TypeCurve == 2
        bilan2(k2).residuals(sub) = 0;
    end
    
    if isequal(bilan1(k2).x, bilan2(k2).x) % && isequal(bilan1(k2).sub, bilan2(k2).sub)
        if TypeWeight == 1 % Pond�ration par le nombre de points
            n1 = bilan1(k2).nx(:);
            n2 = bilan2(k2).nx(:);
        else % Pas de pond�ration
            n1 = ones(size(bilan1(k2).nx));
            n2 = ones(size(bilan2(k2).nx));
            subNaN = (bilan1(k2).nx == 0);
            n1(subNaN) = 0;
            subNaN = (bilan2(k2).nx == 0);
            n2(subNaN) = 0;
        end
        switch TypeCurve
            case 1 % Curve
                y1 = bilan1(k2).y(:);
                y2 = bilan2(k2).y(:);
            case 2 % Residuals
                bilan2(k2).residuals = compute_residuals(bilan2(k2).model, 'x', bilan2(k2).x, 'y', bilan2(k2).y);
                y1 = bilan1(k2).residuals(:);
                y2 = bilan2(k2).residuals(:);
        end
        
%         if MeanCompType == 2 % Valeur en dB que l'on moyenne en valeurs naturelles
%             y1 = reflec_dB2Enr(y1);
%             y2 = reflec_dB2Enr(y2);
%         end
        
        switch MeanCompType
            case 1 % As is (dB if reflectivity)
            case 2 % Amplitude
                y1  = reflec_dB2Amp(y1);
                y2  = reflec_dB2Amp(y2);
            case 3 % Energy
                y1  = reflec_dB2Enr(y1);
                y2  = reflec_dB2Enr(y2);
            case 4 % Median
                %                 y3 = y3; % TODO :
            case 5 % QF
            case 6 % Uncertainty
                y1  = 10 ^ (-y1); % -log10(y2); 
                y2  = 10 ^ (-y2); % -log10(y2); 
        end
        
        s1 = bilan1(k2).std;
        s2 = bilan2(k2).std;
        n3 = n1 + n2;
        y3 = (y1.*n1 + y2.*n2) ./ n3;
        s3 = (s1.*n1 + s2.*n2) ./ n3;
        bilan1(k2).nx  = n3;
        
%         if MeanCompType == 2
%             y3 = reflec_Enr2dB(y3);
%         end
        
        switch MeanCompType
            case 1 % As is (dB if reflectivity)
            case 2 % Amplitude
                y3  = reflec_Amp2dB(y3);
            case 3 % Energy
                y3  = reflec_Enr2dB(y3);
            case 4 % Median
%                 y3 = y3; % TODO : 
            case 5 % QF
            case 6 % Uncertainty
                y3  = -log10(y3);
        end
      
        bilan1(k2).y   = y3;
        bilan1(k2).std = s3;
        
    else % Les abscisses sont diff�rentes, il faut passer par une interpolation
        if TypeWeight == 1 % Pond�ration par le nombre de points
            n1 = bilan1(k2).nx;
            n2 = bilan2(k2).nx;
        else % Pas de pond�ration
            n1 = ones(size(bilan1(k2).nx));
            n2 = ones(size(bilan2(k2).nx));
            subNaN = (bilan1(k2).nx == 0);
            n1(subNaN) = 0;
            subNaN = (bilan2(k2).nx == 0);
            n2(subNaN) = 0;
        end
        switch TypeCurve
            case 1 % Curve
                y1 = bilan1(k2).y;
                y2 = bilan2(k2).y;
            case 2 % Residuals
                bilan2(k2).residuals = compute_residuals(bilan2(k2).model, 'x', bilan2(k2).x, 'y', bilan2(k2).y);
                y1 = bilan1(k2).residuals(:);
                y2 = bilan2(k2).residuals(:);
        end
        
%         if MeanCompType == 2 % Valeur en dB que l'on moyenne en valeurs naturelles
%             y1 = reflec_dB2Enr(y1);
%             y2 = reflec_dB2Enr(y2);
%         end
        
        switch MeanCompType
            case 1 % As is (dB if reflectivity)
            case 2 % Amplitude
                y1  = reflec_dB2Amp(y1);
                y2  = reflec_dB2Amp(y2);
            case 3 % Energy
                y1  = reflec_dB2Enr(y1);
                y2  = reflec_dB2Enr(y2);
            case 4 % Median
%                 y1 = y1; % TODO : 
            case 5 % QF
            case 6 % Uncertainty
                y1  = 10 ^ (-12); % -log10(y2);
                y2  = 10 ^ (-y2); % -log10(y2);
        end
        
        s1 = bilan1(k2).std;
        s2 = bilan2(k2).std;
        
        x1 = bilan1(k2).x;
        if isempty(x1)
            bilan1(k2) = bilan2(k2);
            continue
        end
        
        x2 = bilan2(k2).x;
        if isempty(x2)
            continue
        end
        
        if length(x1) > 1
            deltax1 = median(diff(x1));
        else
            deltax1 = [];
        end
        if length(x2) > 1
            deltax2 = median(diff(x2));
        else
            deltax2 = [];
        end
        deltax = min([deltax1 deltax2]);
        minx = min(x1(1), x2(1));
        maxx = max(x1(end), x2(end));
        newx = minx:deltax:maxx;
        newx = centrage_magnetique(newx);
        
        switch length(n1)
            case 0
                N1 = zeros(size(newx));
                Y1 = zeros(size(newx));
                S1 = zeros(size(newx));
            case 1
                N1 = interp1([bilan1(k2).x-100*eps bilan1(k2).x+100*eps], [n1 n1], newx);
                Y1 = interp1([bilan1(k2).x-100*eps bilan1(k2).x+100*eps], [y1 y1], newx);
                S1 = interp1([bilan1(k2).x-100*eps bilan1(k2).x+100*eps], [s1 s1], newx);
            otherwise
                subNonNaN = (n1 ~= 0);
                N1 = interp1(bilan1(k2).x(subNonNaN), n1(subNonNaN), newx);
                Y1 = interp1(bilan1(k2).x(subNonNaN), y1(subNonNaN), newx);
                S1 = interp1(bilan1(k2).x(subNonNaN), s1(subNonNaN), newx);
        end
        sub2 = isnan(N1);
        N1(sub2) = 0;
        Y1(sub2) = 0;
        S1(sub2) = 0;
        
        switch length(n2)
            case 0
                N2 = zeros(size(newx));
                Y2 = zeros(size(newx));
                S2 = zeros(size(newx));
            case 1
                N2 = interp1([bilan2(k2).x-100*eps bilan2(k2).x+100*eps], [n2 n2], newx);
                Y2 = interp1([bilan2(k2).x-100*eps bilan2(k2).x+100*eps], [y2 y2], newx);
                S2 = interp1([bilan2(k2).x-100*eps bilan2(k2).x+100*eps], [s2 s2], newx);
            otherwise
                subNonNaN = (n2 ~= 0);
                N2 = interp1(bilan2(k2).x(subNonNaN), n2(subNonNaN), newx);
                Y2 = interp1(bilan2(k2).x(subNonNaN), y2(subNonNaN), newx);
                S2 = interp1(bilan2(k2).x(subNonNaN), s2(subNonNaN), newx);
        end
        sub2 = isnan(N2);
        N2(sub2) = 0;
        Y2(sub2) = 0;
        S2(sub2) = 0;
        
        N3 = N1(:) + N2(:);
        sub2 = (N3 ~= 0);
        Y1 = Y1(sub2);
        Y2 = Y2(sub2);
        S1 = S1(sub2);
        S2 = S2(sub2);
        N1 = N1(sub2);
        N2 = N2(sub2);
        N3 = N3(sub2);
        newx = newx(sub2);
        
        Y3 = (Y1(:) .* N1(:) + Y2(:) .* N2(:)) ./ N3;
        S3 = (S1(:) .* N1(:) + S2(:) .* N2(:)) ./ N3;
        
        bilan1(k2).x  = newx;
        bilan1(k2).nx = N3;
        bilan1(k2).ymedian = NaN(size(newx));
        bilan1(k2).std = S3;
        bilan1(k2).sub = 1:numel(newx);
        
%         if MeanCompType == 2
%             Y1 = reflec_Enr2dB(Y1);
%             Y3 = reflec_Enr2dB(Y3);
%         end
        
        switch MeanCompType
            case 1 % As is (dB if reflectivity)
            case 2 % Amplitude
                Y1  = reflec_Amp2dB(Y1);
                Y3  = reflec_Amp2dB(Y3);
            case 3 % Energy
                Y1  = reflec_Enr2dB(Y1);
                Y3  = reflec_Enr2dB(Y3);
            case 4 % Median
%                 y3 = y3; % TODO : 
            case 5 % QF
            case 6 % Uncertainty
                Y1  = -log10(Y1); % 10 ^ (-Y1)
                Y3  = -log10(Y3); % 10 ^ (-Y3)
        end
        
        switch TypeCurve
            case 1 % Curve
                bilan1(k2).y = Y3;
            case 2 % Residuals
                bilan1(k2).y = Y1;
                bilan1(k2).residuals = Y3;
        end
        
    end
    %{
%TODO : voir ce qu'on peut faire de std et ymedian
ymedian: [19x1 double]
nomVarCondition: [1x66 char]
numVarCondition: 3
std: [19x1 double]
    %}
end
