% Synth�se d'une densit� de probabilit� compos�e de plusieurs gaussiennes
%
% [y, composantes] = NGausspdf(x, params)
%
% Input Arguments 
%   x      : abscisses
%   params : Tableau contenant les parametres de N gaussiennes :  
%            [weight1, mean1, sigma1, ..., weightN, meanN, sigmaN]
%
% Output Arguments
%   []          : Auto-plot activation
%   y           : Somme des gaussiennes
%   composantes : Gaussiennes individuelles
%
% Examples 
%   x = 5:0.2:15;
%   NGausspdf(x, [1 10 2]);
%   NGausspdf(x, [0.4 10 0.5   0.6 13 1]);
%
%   y = NGausspdf(x, [0.4 10 0.5   0.6 13 1]);
%   figure; plot(x, y); grid on; hold on;
%
% See also fitHisto1D Authors
% Authors : JMA + XL
%------------------------------------------------------------------------------

function varargout = NGausspdf(x, params)

if nargin == 0
    return
end

N = size(params, 1);
if N > 1
    % Cas o� params est une matrice. On consid�re que les lignes de la
    % matrice sont les param�tres d'une s�rie de m�langes de
    % gaussiennes.
    % WARNING : Cette fonctionnalit� est impos�e pour une
    % utilisation par l'algorithme d'optimisation du simplex

    composantes = [];
    for k=1:size(params, 1)
        [r, c] = NGausspdf(x, params(k,:));
        y(k,:) = r; %#ok<AGROW>
        composantes(end+1:(end+size(c, 1)),:) = c;
    end

    %% Par ici la sortie

    if nargout == 0
        figure;
        plot(x, y); grid on;
    else
        varargout{1} = y;
        varargout{2} = composantes;
    end
    
else

    x = (x(:))';

    n = length(params) / 3;
    y = zeros(size(x));
   
% TODO : Attempt to force summation of proportions to 1
%     subp = 1:3:length(params);
%     params(subp) = params(subp) / sum(params(subp));
    
    for k=1:n
        kp = 1 + (k-1)*3;
        sig = params(kp+2);
        moy = params(kp+1);
        p   = params(kp); % / sig;
        Y = normpdf(x, moy, sig);
        Y = Y / sum(Y);
        composantes(k,:) = p * Y; %#ok<AGROW>
                
        y = y + composantes(k,:);
    end

    %% Par ici la sortie

    if nargout == 0
        figure;
        plot(x, y, 'kp'); hold on;
        plot(x, composantes); grid on;
    else
        varargout{1} = y;
        varargout{2} = composantes;
    end
end
