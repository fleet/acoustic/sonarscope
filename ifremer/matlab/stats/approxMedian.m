%   nbBeams = 880;
%   x = rand(1, nbBeams);
%   x(100)= 100;
%   median(x)
%   val = approxMedian(x)

function val = approxMedian(x)

sub = ~isnan(x);
x = x(sub);

nbMaxVal = 50;
moyenne = NaN(1,nbMaxVal);
km = 0;
step = ceil(length(x) / nbMaxVal);
for k=1:step:length(x)
    sub = k:min(length(x), (k+step-1));
    km = km + 1;
    moyenne(km) = mean(x(sub));
end
moyenne = moyenne(1:km);
val = median(moyenne);