% Calcul de courbes moyennes d'une image en fonction d'autres images
%
% Syntax
%   [means, sem, counts, bilan] = courbesStats(V, titreV, NV, binsX1, X1, titreX1, ...)
%
% Input Arguments
%   V       : Image sur laquelle les statistiques seront calculees
%   titreV  : Nom de l'image (reflectivite par Ex)
%   NV      : Nombre de points ayant servi au calcul de Y ([] si pas connu)
%   binsX1  : Centres des classes a utiliser pour le partionnement selon X1
%   X1      : Image servant au premier decoupage
%   titreX1 : Nom de l'image (Angle d'emission par Ex)
%
% Name-Value Pair Arguments
%   binsX2  : Centres des classes a utiliser pour le partionnement selon X
%   X2      : Image servant au premier decoupage
%   titreX2 : Nom de l'image (Angle d'emission par Ex)
%   ...
%
% Output Arguments
%   means  : Matrice contenant les moyennes. (dimension [length(binsX1) length(binsX2) ...])
%   sem    : Matrice contenant les ecart(types.
%   counts : Matrice contenant les nombre de points moyennes.
%   bilan  : Structure contenant les moyennes calculees separemment sur les differents X.
%
% Examples
%   nomFicRec = '/home4/doppler/TestsJMA/EM1002_ExStr.imo.imo'
%   a = cl_car_ima(nomFicRec);
%
%   R  = get(a, 'REFLECTIVITY');
%   E  = get(a, 'EMISSION');
%   S  = get(a, 'EMISSION_SECTOR');
%   N  = get(a, 'BEAM_NB');
%   NV = get(a, 'AVRG_PIX_NB');
%
%   [means, sem, counts, bilan] = courbesStats(R, 'REFLECTIVITY', NV, ...
%                                       [-75:0.5:75],   E, 'EMISSION');
%   [means, sem, counts, bilan] = courbesStats(R, 'REFLECTIVITY', NV, ...
%                                       [-75:0.5:75],   E, 'EMISSION', ...
%                                       1:3,            S, 'EMISSION_SECTOR');
%   [means, sem, counts, bilan] = courbesStats(R, 'REFLECTIVITY', NV, ...
%                                       [-75:0.5:75],   E, 'EMISSION', ...
%                                       1:111,          N, 'BEAM_NB', 'nbPMin', 10);
%   [means, sem, counts, bilan] = courbesStats(R, 'REFLECTIVITY', NV, ...
%                                       [-75:0.5:75],   E, 'EMISSION', ...
%                                       1:3,            S, 'EMISSION_SECTOR', ...
%                                       1:111,          N, 'BEAM_NB', 'nbPMin', 10);
%
% See also cl_image cl_image/histo Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [means, sem, counts, bilan, medians] = courbesStats(V, titreV, NV, varargin)

[varargin, nbPMin]           = getPropertyValue(varargin, 'nbPMin',           1);
[varargin, fig]              = getPropertyValue(varargin, 'fig',              []);
[varargin, CourbeBias]       = getPropertyValue(varargin, 'CourbeBias',       0);
[varargin, EditBias]         = getPropertyValue(varargin, 'EditBias',         0);
[varargin, CentrageMagnetic] = getPropertyValue(varargin, 'CentrageMagnetic', []);
[varargin, Unit]             = getPropertyValue(varargin, 'Unit',             []);
[varargin, MeanCompType]     = getPropertyValue(varargin, 'MeanCompType',     1);
[varargin, NomFic]           = getPropertyValue(varargin, 'NomFic',           []);

strMeanCompType = {Unit; 'Amp'; 'Enr'; 'Median'};

nBY = length(varargin) / 3;
if isempty(CentrageMagnetic)
    CentrageMagnetic = ones(1,nBY);
end

sub = isnan(V);
for k1=1:nBY
    k2 = 1 + 3*(k1-1);
    binsY{k1} = varargin{k2}; %#ok
    Y{k1}     = varargin{k2+1}; %#ok
    nomY{k1}  = varargin{k2+2}; %#ok
    %     sub = sub .* isnan(Y{k1});
    sub = sub | isnan(Y{k1});
end
% sub = find(sub);
% V(sub) = NaN;
V(sub) = [];
if ~isempty(NV)
    NV(sub) = [];
end

for k=1:nBY
    if isa(Y{k}, 'double') || isa(Y{k}, 'single')
        Y{k} = Y{k}(~sub);
    else
        Y{k} = single(Y{k}(~sub));
    end
    % Y{k}(sub) = NaN; %#ok
    
    NY(k) = length(binsY{k}); %#ok
    rangeIn(1) = min(binsY{k});
    rangeIn(2) = max(binsY{k});
    % YQ = quantify(Y{k}, 'rangeIn', rangeIn, 'N', NY(k), 'rangeOut', [1 NY(k)]);
    
    if length(binsY{k}) == 1
        Step = 1;
    else
        Step = mean(diff(binsY{k}));
    end
    [YQ, YQ_bins] = quantify(Y{k}, 'rangeIn', rangeIn, 'Step', Step, 'CentrageMagnetic', CentrageMagnetic(k));
    binsY{k} = YQ_bins;
    NY(k) = length(binsY{k}); %#ok
    
    Y{k} = YQ;
    % figure; plot(Y{k}); grid on;
end

Z = Y{1};
N = 1;

for k=2:nBY
    N = N * NY(k-1);
    Z = Z + N * (Y{k}-1);
end

Z = Z(:);
V = V(:);

% sub = find(~isnan(V) & ~isnan(Z) & (Z > 0)); % Derniere condition (Z > 0) theoriquement pas necessaire
sub = ~isnan(Z) & (Z > 0) & ~isinf(V); % Derni�re condition (Z > 0) th�oriquement pas n�cessaire
% mais je suis tomb� sur le cas o� 2 pixels etaient egaux � z�ro (donn�es SAR)

switch MeanCompType
    case 1 % As is (dB)
%         pi;%KMA
    case 2 % Amplitude
        V = reflec_dB2Amp(V);
    case 3 % Energy
        V = reflec_dB2Enr(V);
    case 4 % Median
        % On ne fait rien car on va prendre la m�diane
        pi;%KMA
    case 5 % QF
        pi;%KMA
    case 6 % Uncertainty
        V = 10 .^ (-V);
end

if isempty(NV)
    if ~isa(V, 'double') && ~isa(V, 'single')
        V = single(V);
    end
    %     tic
    %     [means, sem, counts, name] = grpstats(V(sub), Z(sub));
    %     toc
    %     tic
    [means, medians, sem, counts, name] = grpstats(V(sub), Z(sub), {'mean'; @median; 'sem'; 'numel'; 'gname'});
    % toc
    
    % A TESTER : peut-�tre pourrait-on param�trer le nom de la fonction
    %     means = grpstats(V(sub), Z(sub), @median);
    
    sem = sem .* sqrt(counts);
else
    V  = V(:);
    NV = NV(:);
    Z  = Z(:);
    [means, medians, sem, counts, name] = my_grpstats(V(sub), NV(sub), Z(sub));
end

switch MeanCompType
    case 1 % As is (dB)
    case 2 % Amplitude
        m1 = reflec_Amp2dB(means - sem);
        m2 = reflec_Amp2dB(means + sem);
        sem = (m2 - m1) / 2;
        means   = reflec_Amp2dB(means);
        medians = reflec_Amp2dB(medians);
    case 3 % Energy
        m1 = reflec_Amp2dB(means - sem);
        m2 = reflec_Amp2dB(means + sem);
        sem = (m2 - m1) / 2;
        means   = reflec_Enr2dB(means);
        medians = reflec_Enr2dB(medians);
    case 4 % Median
        means = medians;
%         medians = reflec_Amp2dB(medians);
%         sem     = sem;
    case 5 % QF
    case 6 % Uncertainty
        means   = -log10(means);
        medians = -log10(medians);
        sem     = -log10(sem);
        %         means   = -log10(sqrt(means));
        %         medians = -log10(sqrt(medians));
        %         sem     = -log10(sqrt(sem));
end

if any(isinf(means))
    my_breakpoint
end

if CourbeBias == 1 % Courbe de biais globale
    Biais = sum(counts .* means) / sum(counts);
    if EditBias
        figBias = figure;
        subplot(4, 1, 1:3); plot(means);  grid on;
        %         xlabel(Lang('', 'Statistic samples'));
        ylabel(Lang('Moyennes', 'Means'));
        str1 = 'Cette figure est une aide pour choisir la valeur soustractive';
        str2 = 'This figure is a help to choose the subtractive value';
        title(Lang(str1,str2));
        hold on; plot([1 length(means)], [Biais Biais], 'r')
        legend({Lang('Courbes','Curves'); Lang('Valeur moyenne', 'Mean value')})
        subplot(4, 1, 4); plot(counts); grid on;
        %         xlabel(Lang('TODO', 'Statistic samples'));
        ylabel(Lang('Nb de points', 'Counts'))
        
        % TODO : Tag EditValWithCurve mentionn� dans fiche forge 918
        
        str1 = 'Valeur soustractive';
        str2 = 'Subtractive value';
        p    = ClParametre('Name', 'Value', 'Value', Biais);
        a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
        a.openDialog;
        flag = a.okPressedOut;
        my_close(figBias)
        if ~flag
            means   = [];
            medians = [];
            sem     = [];
            counts  = [];
            bilan   = [];
            return
        end
        Biais = a.getParamsValue;
    end
    means = means - Biais;
    medians = medians - Biais;
else
    Biais = 0;
end

nbDim = length(NY);
if nbDim == 1
    tabmean   = zeros(NY,1);
    tabmedian = zeros(NY,1);
    tabsem    = zeros(NY,1);
    tabcounts = zeros(NY,1);
else
    tabmean   = zeros(NY);
    tabmedian = zeros(NY);
    tabsem    = zeros(NY);
    tabcounts = zeros(NY);
end
for k1=1:length(name)
    k2 = ceil(str2num(name{k1})); %#ok<ST2NM>
    if k2 <= prod(NY)
        [I1, I2, I3, I4, I5, I6, I7, I8]          = ind2sub(NY, k2);
        tabmean(I1, I2, I3, I4, I5, I6, I7, I8)   = means(k1);
        tabmedian(I1, I2, I3, I4, I5, I6, I7, I8) = medians(k1);
        tabsem(I1, I2, I3, I4, I5, I6, I7, I8)    = sem(k1);
        tabcounts(I1, I2, I3, I4, I5, I6, I7, I8) = counts(k1);
    end
end


% Test
if CourbeBias == 2 % Courbe de biais par sous-courbe
    for k=1:size(tabmean,2)
        means  = tabmean(:,k);
        medians  = tabmedian(:,k);
        counts = tabcounts(:,k);
        Biais = sum(counts .* means) / sum(counts);
        if EditBias
            figBias = figure;
            subplot(4, 1, 1:3); plot(means);  grid on; xlabel('Statistic samples'); ylabel('Means');
            title(['Mean value for curve ' num2str(k) ' . This figure is an help to choose Biais value']);
            hold on; plot([1 length(means)], [Biais Biais], 'r')
            legend({'Means'; 'Bias'})
            subplot(4, 1, 4); plot(counts); grid on; xlabel('Statistic samples'); ylabel('Counts')
            
            str1 = sprintf('Edition de la valeur du biais pour la courbe # %d', k);
            str2 = sprintf('Bias value edition for curve # %d', k);
            p    = ClParametre('Name', 'Biais value', 'Value', Biais);
            a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
            a.openDialog;
            flag = a.okPressedOut;
            if ishandle(figBias)
                close(figBias)
            end
            if ~flag
                means  = [];
                medians = [];
                sem    = [];
                counts = [];
                bilan  = [];
                return
            end
            Biais = a.getParamsValue;
        end
        tabmean(:,k)   = means   - Biais;
        tabmedian(:,k) = medians - Biais;
    end
else
    %     Biais = 0; % Comment� le 31/10/2009
end

means   = tabmean;
medians = tabmedian;
sem     = tabsem;
counts  = tabcounts;

%% Cr�ation du bilan

subNbPMin = find(counts < nbPMin);
if nbDim == 1
    means(subNbPMin)   = NaN;
    medians(subNbPMin) = NaN;
    sub = 1:length(means);
    bilan{1}(1).x       = binsY{1}(sub);
    bilan{1}(1).y       = means;
    bilan{1}(1).ymedian = medians;
    
    if isempty(NomFic)
        bilan{1}(1).nom = nomY{1};
    else
        bilan{1}(1).nom = NomFic;
    end
    
    bilan{1}(1).sub = sub;
    bilan{1}(1).nx  = counts;
    bilan{1}(1).std = sem;
    
    bilan{1}(1).titreV = titreV;
    bilan{1}(1).NY     = NY;
    bilan{1}(1).nomX   = nomY{1};
    bilan{1}(1).Biais  = Biais;
    bilan{1}(1).Unit   = Unit;
    bilan{1}(1).MeanCompType    = MeanCompType;
    bilan{1}(1).strMeanCompType = strMeanCompType;
else
    for k1=1:(nbDim-1)
        for k2=1:NY(k1+1)
            switch k1
                case 1
                    m = squeeze(means(:,k2,:));
                    me = squeeze(medians(:,k2,:));
                    c = squeeze(counts(:,k2,:));
                    sigma = squeeze(sem(:,k2,:));
                case 2
                    m = squeeze(means(:,:,k2));
                    me = squeeze(medians(:,:,k2));
                    c = squeeze(counts(:,:,k2));
                    sigma = squeeze(sem(:,k2,:));
                case 3
                    'Pas teste';
                    m = squeeze(means(:,:,:,k2));
                    me = squeeze(medians(:,:,:,k2));
                    c = squeeze(counts(:,:,:,k2));
                    sigma = squeeze(sem(:,k2,:));
                case 4
                    'Pas teste';
                    m = squeeze(means(:,:,:,:,k2));
                    me = squeeze(medians(:,:,:,:,k2));
                    c = squeeze(counts(:,:,:,:,k2));
                    sigma = squeeze(sem(:,k2,:));
                otherwise
                    'Pas fait';
            end
            
            sumc = sum(c, 2);
            sub = find(sumc >= nbPMin);
            if isempty(sub)
                %                 my_warndlg('Not enought points to compute statistics.', 0, 'Tag', 'Not enought points');
            end
            
            p = sum(m .* c, 2);
            moyenne = p(sub) ./ sumc(sub);
            valMediane = me(sub);
            
            bilan{k1}(k2).x               = binsY{1}(sub); %#ok
            bilan{k1}(k2).y               = moyenne; %#ok
            bilan{k1}(k2).ymedian         = valMediane; %#ok<AGROW>
            bilan{k1}(k2).nomVarCondition = nomY{k1+1}; %#ok
            bilan{k1}(k2).numVarCondition = binsY{k1+1}(k2); %#ok
            bilan{k1}(k2).indVarCondition = k2; %#ok
            %                 bilan{k1}(k2).nom = strIdent;
            if ~isempty(NomFic)
                bilan{k1}(k2).nom = NomFic; %#ok<AGROW>
            end
            bilan{k1}(k2).sub = sub; %#ok
            bilan{k1}(k2).nx  = sumc(sub); %#ok
            bilan{k1}(k2).std = sigma(sub); %#ok
            
            bilan{k1}(k2).titreV = titreV; %#ok
            bilan{k1}(k2).NY     = NY; %#ok
            bilan{k1}(k2).nomX   = nomY{1}; %#ok
            bilan{k1}(k2).Biais  = Biais; %#ok<AGROW>
            bilan{k1}(k2).Unit   = Unit; %#ok<AGROW>
            bilan{k1}(k2).MeanCompType    = MeanCompType; %#ok<AGROW>
            bilan{k1}(k2).strMeanCompType = strMeanCompType; %#ok<AGROW>
        end
    end
end
means(subNbPMin) = NaN;
medians(subNbPMin) = NaN;

if ~isempty(fig)
    CourbesStatsPlot(bilan, 'fig', fig);
end

%{
FigUtils.createSScFigure;
nc = length(bilan{1});
YLim = [-40 -10];
for kc=1:nc
h(kc) = subplot(nc,1,kc); PlotUtils.createSScPlot(bilan{1}(kc).x, bilan{1}(kc).y); grid on;
end
linkaxes(h, 'xy')
%}
