% Calcul de l'ellipse de covariance � K*STD, en 2D
%
% Syntax
%   e = covEllipse(x, y, k) 
%
% Input Arguments 
%   X,Y   : Coordonees des points definissant le nuage de points
%   k     : k fois l'�cart-type
%
% Output Arguments 
%   e     : Coordonnees des points de l'ellipse
%
% Examples
%   X = 10+randn(1,20);
%   Y = 20+3*randn(1,20);
%   e = covEllipse(X, Y, 1);
%
% See also dist1mahal cl_cooc/plotNuages Authors
% Authors : GLT
% ----------------------------------------------------------------------------
function e = covEllipse(x, y, k)

if nargin < 2
    error('covEllipse:argChk', Lang('au moins 2 arguments : e = covEllipse(x, y, k)','Wrong number of input arguments : e = covEllipse(x, y, k)'));
elseif nargin < 3
    k = 1;                    % nbre de STD
end

M1 = mean(x);
M2 = mean(y);               

%% Ellipse de covariance
covMx = cov(x-M1,y-M2);
angle   = linspace(0, 2*pi, 360);
circle = [cos(angle) ; sin(angle)];

%% M�thode 2
e2 = k*chol(covMx) * circle;
e2(1,:) = e2(1,:) + M1;
e2(2,:) = e2(2,:) + M2;

%% Methode 1
% Calcul du rayon de Mahalanobis de l'ellipse contenant la masse de probabilit� d�sir�e
conf = 2*normcdf(k)-1;    
% Translates a confidence interval to a Mahalanobis distance.  
scale = chi2inv(conf,2);   
covMx = covMx * scale;
[V,D] = eig(covMx);
e = V*sqrt(D) * circle;
e(1,:) = e(1,:) + M1;
e(2,:) = e(2,:) + M2;

%% Methode3
e3 = dist1mahal([x;y]');

if nargout == 0
    figure; 
    hold on;
    PlotUtils.createSScPlot(x, y, 'k+');
    PlotUtils.createSScPlot(e(1,:), e(2,:), 'r'); 
    PlotUtils.createSScPlot(e2(1,:), e2(2,:), 'g'); 
    PlotUtils.createSScPlot(e3(:,1), e3(:,2), 'b'); 
    legend({'data', sprintf('Cov ellipse @%d*STD',k)});
    axis equal;
end