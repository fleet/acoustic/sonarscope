% Calcul de la distance de Mahalanobis
%
% Syntax
%   ellipse = dist1mahal(XY) 
%
% Input Arguments 
%   XY   : Coordonees des points definissant le nuage de points
%
% Output Arguments 
%   []      : Auto-plot activation
%   ellipse : Coordonnees des points de l'ellipse de distance 4
%
% Examples
%   X = 10+randn(20);
%   Y = 20+3*randn(20);
%   dist1mahal([X(:) Y(:)])
%   ellipse = dist1mahal([X(:) Y(:)])
%
%   XY = mvnrnd([2 3], [1 0.9;0.9 1], 100);
%   dist1mahal(XY)
%   ellipse = dist1mahal(XY)
%
% See also dist1mahal cl_cooc/plotNuages Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function varargout = dist1mahal(XY)

[rx,cx] = size(XY);

if rx < cx
   error('The number of rows of XY must exceed the number of columns.');
end
if any(imag(XY(:)))
   error('MAHAL does not accept complex inputs.');
end

meanXY = mean(XY, 1);

C = XY - meanXY(ones(rx,1),:);
[~,R] = qr(C,0);

angle   = linspace(0, 2*pi, 360);
ellipse = R' * [cos(angle) ; sin(angle)] / 4;
ellipse = ellipse' + meanXY(ones(360,1),:);

if nargout == 0
    minX = min(XY(:,1));
    maxX = max(XY(:,1));
    minY = min(XY(:,2));
    maxY = max(XY(:,2));
    
    minX = min(minX, min(ellipse(:,1)));
    maxX = max(maxX, max(ellipse(:,1)));
    minY = min(minY, min(ellipse(:,2)));
    maxY = max(maxY, max(ellipse(:,2)));
    
    xg = linspace(minX, maxX, 100);
    yg = linspace(minY, maxY, 100);
    [Xg,Yg] = meshgrid( xg, yg);
    
%     DMahal = zeros(100, 100) * Inf;
    D = mahalanobis([Xg(:) Yg(:)], XY);
    N = sqrt(size(D,1));
    D = reshape(D, N, N);
    figure; imagesc(xg, yg, D'); axis xy; colorbar
    hold on; PlotUtils.createSScPlot(XY(:,1), XY(:,2), 'k+'); 
    hold on; PlotUtils.createSScPlot(ellipse(:,1), ellipse(:,2), 'k'); 
    axis equal
else
    varargout{1} = ellipse;
end
