% Calcul de l'entropie : formule provisoire SOS matheux
% Syntax
%   e = entropy(x)
% 
% Input Arguments 
%   x : Vecteur de donnees
% 
% Output Arguments 
%   e : Entropie 
%
% Remarks : Il y a un gros probleme de definition de l'entropie pour des donnees qui
%           ne sont pas en niveaux de gris. Cette formule n'est certainement pas
%           la bonne
%
% Examples
%   Y = speckle(50);
%   histo1D(Y, 100);
%   e = entropy(Y)
%
% See also histo1D stats Authors
% Authors : JMA
% VERSION    : $Id: entropy.m,v 1.4 2003/04/18 11:51:20 augustin Exp $
%-------------------------------------------------------------------------------

function e = entropy(x)

x = double(x(~isnan(x)));

if length(x) <= 9
    e = NaN;
else
    if issparse(x)
        if nnz(x) <= 9
            e = NaN;
            return
        end
    end
    m = mean(x);
    s = std(x);

    if s == 0
        e = NaN;
    else
        x = (x - m) / (3 * s);
        x = x((x > -1) & (x < 1));
        x = 1 + x;
        x = (x .* log10(x)) ;
        e = sum(x) / length(x);
    end
end
