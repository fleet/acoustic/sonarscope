% Recherche de gaussiennes constituant la densit� de probabilit� d'un signal
%
% Syntax
%   [moy, sigma, p, Eqm] = fitHisto1D(x, y, n)
%
% Input Arguments
%   x : Abscisses
%   y : Signal
%   n : Nombre de gaussiennes a rechercher.
%
% Name-only Arguments
%   Edit : Edition interactive
%   Draw : Dessine la ddp meme si parametres de sortie demandes
%
% Output Arguments
%   []    : auto-PlotUtils.createSScPlot activation
%   moy   : Moyennes des gaussiennes
%   sigma : Ecart-types des gaussiennes
%   p     : Proportions des gaussiennes
%
% Examples
%   nomFic = getNomFicDatabase('textureSonar01.png');
%   img = imread(nomFic);
%   figure; imagesc(img)
%   [y, x] = histo1D(img, 'Proba');
%   figure; PlotUtils.createSScPlot(x, y, '+'); grid on;
%   sub = find(y ~= 0);
%   fitHisto1D(x(sub), y(sub), 2, 'Edit', 1)
%
%   nomFic = getNomFicDatabase('textureSonar02.png');
%   img = imread(nomFic);
%   C1 = coocMat(img, 1, 0);
%   [R45, xp45]  = radon(C1, 45);
%   [R135,xp135] = radon(C1, 135);
%
%   fitHisto1D(xp45,  R45,  1)
%   fitHisto1D(xp135, R135, 2)
%   fitHisto1D(xp135, R135, 2, 'Edit')
%   [moy, sigma, p, Eqm] = fitHisto1D(xp1, R135, 2)
%
%   C6 = coocMat(img, 6, 0);
%   [R45, xp45]  = radon(C6, 45);
%   [R135,xp135] = radon(C6, 135);
%   fitHisto1D(xp45,  R45,  2)
%   fitHisto1D(xp135, R135, 1)
%
% See also normpdf histo1D stats Authors
% References : A component-wise EM Algorithm for Mixtures No 3746 Aout 1999
% Authors : IK + JMA
%--------------------------------------------------------------------------------

function varargout = fitHisto1D(x, y, n, varargin)

[varargin, m] = getPropertyValue(varargin, 'm', []);
[varargin, s] = getPropertyValue(varargin, 's', []);
[varargin, w] = getPropertyValue(varargin, 'w', []);

[varargin, flagEdit] = getFlag(varargin, 'Edit');
[varargin, flagDraw] = getFlag(varargin, 'Draw'); %#ok<ASGLU>

%% Initialisations

x  = x(:)';
y  = y(:)';

moyy = sum(x .* y); % Valeur moyenne de la donn�e
sigy = sqrt(sum(x .^ 2 .* y) - moyy ^ 2);  % Ecart-type de la donn�e

if isempty(s)
    for k=1:n
        stdy(k) = sigy;  %#ok<AGROW> % M�me std pour toutes les composantes
    end
else
    stdy = s;
end

if isempty(m)
    % R�partition lin�aire des moyennes des diff�rentes composantes
    moy = linspace(moyy - 2*sigy, moyy + 2*sigy, n);
else
    moy = m;
end

if isempty(w)
    ComponentProportion = max(y) / n;
else
    ComponentProportion = w;
end

%%

minx = min(x);
maxx = max(x);
deltax = mean(diff(x));
for k=1:n
    kp = 1 + (k-1) * 3;
    params(kp) = ClParametre('Name', sprintf('Proportion %d', k), ...
        'MinValue', 0, ...
        'maxvalue', 1, ...
        'Value',    ComponentProportion(k), ...
        'Mute',   1); %#ok<AGROW>
    
    params(kp+1) = ClParametre('Name', sprintf('Mean %d', k), ...
        'MinValue', minx, ...
        'maxvalue', maxx, ...
        'Value',    moy(k), ...
        'Mute',   1); %#ok<AGROW>
    
    params(kp+2) = ClParametre('Name', sprintf('Std %d', k), ...
        'MinValue', deltax, ...
        'maxvalue', maxx-minx, ...
        'Value',    stdy(k), ...
        'Mute',   1); %#ok<AGROW>
    
    nomComposantes{k} = sprintf('Gaussian%d', k); %#ok<AGROW>
end
modeleNGausspdf = ClModel('Name', 'NGausspdf', 'Params', params, 'ComponentsName', nomComposantes, 'XData', x);
modeleNGausspdf.plot();
modeleNGausspdf.editProperties();

%% D�claration d'une instance d'ajustement

optim = ClOptim('XData', x, 'YData', y, ...
    'xlabel', 'x', ...
    'ylabel', 'y', ...
    'Name', 'Hello World', ...
    'AlgoType', 1, 'UseSimAnneal', 0, 'models', modeleNGausspdf);
% optim.editProperties();
% optim.plot
optim.optimize();
% optim.plot


if flagEdit
    a = OptimUiDialog(optim, 'Title', 'fitHisto1D', 'windowStyle', 'normal');
    a.openDialog();
end

%% R�cup�ration du r�sultat

params = optim.getParamsValue();

[synthese, composante] = NGausspdf(x, params);

coef = sum(y) / sum(synthese);
Eqm  = eqm(y, synthese*coef);

[p, moy, sigma] = params2p_moy_sigma(params);

%% Par ici la sortie

if (nargout == 0) || flagDraw
    FigUtils.createSScFigure;
    Color = 'bgmc';
    for k=1:n
        kc = 1 + mod(k-1, length(Color));
        PlotUtils.createSScPlot(x, composante(k,:), Color(kc)); hold on; grid on;
        str = sprintf('Gauss %d : Mean %f  Std %f  Proportion %f', k, moy(k), sigma(k), p(k));
        disp(str)
        Legende{k} = str; %#ok<AGROW>
    end
    PlotUtils.createSScPlot(x, y, 'k*');
    Legende{end+1} = 'Data';
    PlotUtils.createSScPlot(x, synthese, 'r');
    Legende{end+1} = 'Synthesis';
    
    legend(Legende)
end
if nargout ~= 0
    varargout{1} = moy;
    varargout{2} = sigma;
    varargout{3} = p;
    varargout{4} = Eqm;
end


function [p, moy, sigma] = params2p_moy_sigma(params)
n = length(params) / 3;
for k=1:n
    kp = 1 + (k-1) * 3;
    p(k)     = params(kp);   %#ok<AGROW>
    moy(k)   = params(kp+1); %#ok<AGROW>
    sigma(k) = params(kp+2); %#ok<AGROW>
end
