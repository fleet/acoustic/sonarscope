function InitModel = getLastDefinedModel(CourbesStatistiques, TypeCourbe, varargin)

[varargin, sub]    = getPropertyValue(varargin, 'sub',    []);
[varargin, TagSup] = getPropertyValue(varargin, 'TagSup', []); %#ok<ASGLU>

InitModel = ClModel.empty;

if isempty(sub)
    sub = 1:length(CourbesStatistiques);
end

for k=1:length(sub)
    k1 = sub(k);
    if isfield(CourbesStatistiques(k1).bilan{1}, 'model')
        for k2=1:length(CourbesStatistiques(k1).bilan{1})
            
            if ~isempty(TagSup)
                if ~isequal(TagSup, CourbesStatistiques(k1).bilan{1}(k2).TagSup)
                    continue
                end
            end
            
            model = CourbesStatistiques(k1).bilan{1}(k2).model;
            if isempty(model)
                continue
            end
            nom = model.Name;
            
            for iTypeCourbe=1:length(TypeCourbe)
                if strcmp(nom, TypeCourbe{iTypeCourbe})
                    %                     InitModel(k2) = model;
                    if isfield(CourbesStatistiques(k1).bilan{1}(k2), 'numVarCondition')
                        n = CourbesStatistiques(k1).bilan{1}(k2).numVarCondition;
                        InitModel(n) = model;
                    else
                        % Debut rajout JMA le 02/04/2013 pour palier � la
                        % non existence de la variable numVarCondition dans
                        % le cas o� il n'y a pas de layer conditionnel.
                        % C'est pas top
                        InitModel = model;
                    end
                end
            end
        end
    end
end
