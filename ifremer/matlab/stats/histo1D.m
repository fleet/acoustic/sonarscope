% Calcul de l'histogramme, des probas ou de la ddp (densite de probabilite)
% d'une serie de valeurs
%
% Syntax
%   [N, bins, Val, Sub] = histo1D(X, ...)
%   [N, bins, Val, Sub] = histo1D(X, bins, ...)
%
% INPUT PARANbinsETERS :
%   X : matrice des valeurs
%
% Name-Value Pair Arguments
%   bins : Si est un scalaire : Nombre de bins en X (100 par dfaut)
%          Si est un vecteur  : Centres des bins en X
%
% Name-Value Pair Arguments
%   ValNaN      : Valeur utilisee pour les valeurs manquantes
%   ParamsGauss : Parametres de gaussiennes pour superposition graphique.
%                 (Utile uniquement si mode "auto-plot activation")
%                 {moyennes, sigmas, proportions}
%                 Voir melangeGaussiennes1D pour illustration
%   Titre       : Titre de la figure sur "Auto-plot activation"
%
% Name-only Arguments
%   Histo      : Calcul de l'histogramme (par defaut)
%   Proba      : Calcul de la proba
%   Ddp        : Calcul de la densite de proba
%   FiltGauss  : Filtrage gaussien du resultat
%   Cumul      : Fonction cumulee
%
% Output Arguments
%   []   : Auto-plot activation
%   N    : Histogramme ou proba ou ddp pour chaque classe
%   bins : Vecteur des centres des classes
%   Val  : Liste des valeurs par classe
%   Sub  : Adresses des valeurs dans X
%
% Examples
%   mu = 0;
%   sigma = 1;
%   taille=[100 100];
%   X = normrnd(mu, sigma, taille);
%   figure; my_hist(X(:), 100);
%
%   histo1D(X);
%   [N, bins] = histo1D(X);
%
%   histo1D(X, 100, 'Proba');
%   [N, bins] = histo1D(X, 'Proba');
%   histo1D(X, 'Proba', 'Cumul');
%
%   histo1D(X, 100, 'Ddp');
%   [N, bins] = histo1D(X, 'Ddp');
%
%   [N1, bins] = histo1D(X, 'Ddp');
%   figure; plot(bins,  N1,'k', bins, normpdf(bins,mu,sigma),'r');
%   legend('histogramme empirique','ddp theorique');
%
%   X = speckle(50);
%   histo1D(X);
%   histo1D(X, 'FiltGauss');
%   histo1D(X, 'FiltGauss', 'Proba');
%   histo1D(X, 'FiltGauss', 'Ddp');
%   [N, bins, Val, Sub] = histo1D(X);
%
% See also my_hist stats melangeGaussiennes1D Authors
% Authors : GLC + JMA
%-------------------------------------------------------------------------------

function [N, bins, val, sub] = histo1D(X, varargin)

%% Valeurs utilis�es pour les valeurs manquantes

[varargin, ValNaN]      = getPropertyValue(varargin, 'ValNaN',      NaN);
[varargin, Titre]       = getPropertyValue(varargin, 'Titre',       []);
[varargin, Fig]         = getPropertyValue(varargin, 'Fig',         []);
[varargin, XLabel]      = getPropertyValue(varargin, 'XLabel',      '');
[varargin, ParamsGauss] = getPropertyValue(varargin, 'ParamsGauss', []);
[varargin, Sampling]    = getPropertyValue(varargin, 'Sampling',    []); % Pas d'�chantillonnage impos� de la donn�e

[varargin, flagHisto]     = getFlag(varargin, 'Histo');
[varargin, flagProba]     = getFlag(varargin, 'Proba');
[varargin, flagDdp]       = getFlag(varargin, 'Ddp');
[varargin, flagFiltGauss] = getFlag(varargin, 'FiltGauss');
[varargin, Cumul]         = getFlag(varargin, 'Cumul');

nbMaxSamplesStats = 2500000;

XIn = X;
sz = size(X);
v = X(1);
if (length(sz) == 1) && any(sz == 1)
    if isempty(Sampling)
        Sampling = numel(X) / nbMaxSamplesStats;
        Sampling = max(Sampling, 1);
    end
    sub = floor(1:Sampling:numel(X));
    
    if isa(v, 'double')
        X = X(sub);
    elseif isa(v, 'single')
        X = X(sub);
    else
        X = single(X(sub));
    end
elseif length(sz) == 2    % Matrice 2D
    if isempty(Sampling)
        Sampling = numel(X) / nbMaxSamplesStats;
        Sampling = max(sqrt(Sampling), 1);
    end
    subx = floor(1:Sampling:sz(2));
    suby = floor(1:Sampling:sz(1));
    
    if isa(v, 'double')
        X = X(suby,subx);
    elseif isa(v, 'single')
        X = X(suby,subx);
    else
        X = single(X(suby,subx));
    end
elseif length(sz) == 3    % Matrice 3D
    if isempty(Sampling)
        Sampling = numel(X) / nbMaxSamplesStats;
        Sampling = max(sqrt(Sampling), 1);
    end
    subx = floor(1:Sampling:sz(2));
    suby = floor(1:Sampling:sz(1));
    
    if isa(v, 'double')
        X = X(suby,subx,:);
    elseif isa(v, 'single')
        X = X(suby,subx,:);
    else
        X = single(X(suby,subx,:));
    end
else    % Matrice de dimension > 3
    if isempty(Sampling)
        Sampling = numel(X) / nbMaxSamplesStats;
        Sampling = max(Sampling, 1);
    end
    sub = floor(1:Sampling:numel(X));
    
    if isa(v, 'double')
        X = X(sub);
    elseif isa(v, 'single')
        X = X(sub);
    else
        X = single(X(sub));
    end
end
X = X(:);

%% Cas des transform�es de Fourier en complex

if ~isreal(X)
    X(X == 0) = NaN;
    X = 20 * log10(abs(X));
    X(isinf(X)) = NaN;
end

%% Supression des valeurs manquantes

% Nombreuses modif LMA le 02/04/2021

X(~isfinite(X)) = []; % Modif JMA le 02/04/2021
if ~isnan(ValNaN)
    % Rajout� par JMA le 30/08/2011 pour bug constat� sur fichiers de Marc Roche provenant d'ErMapper
    X(X == ValNaN) = [];
end

% X(isinf(X)) = [];

if isempty(X)
    N    = [];
    bins = [];
    val  = [];
    sub  = [];
    return
end

if isempty(varargin)
    [N, bins] = hist(X, 100);
else
    [N, bins] = my_hist(X, varargin{:}); 
end

nom = 'Histo';
NBrut = N;
N0 = N;
sumN0 = sum(N0);

%% Filtrage gaussien de l'histogramme

if flagFiltGauss
    w = gausswin(11); % wvtool(w);
    N = filtfilt(w/sum(w), 1, double(N));
end

%% Calcul des probabilit�s

if flagProba
    N = N / sum(N);
    NBrut = NBrut / sum(NBrut);
    nom = 'proba';
end

%% Calcul de la densit� de probabilit�

if numel(bins) == 1 % Only one value (TxBeamIndex EM2040)
    dx = 1;
else
    dx = diff(bins(:));
    dx = dx';
    dx = [dx(1) dx dx(end)];
    dx = (dx(1:end-1) + dx(2:end)) / 2;
end
if flagDdp
    N = N / sum(N);
    N = N ./ dx;
    NBrut = NBrut / sum(NBrut);
    NBrut = NBrut ./ dx;
    nom = 'ddp';
end

%% Cumulation si demand�e -> courbe hypsom�trique

if Cumul
    NBrut = cumsum(NBrut);
    N     = cumsum(N);
    
    if isempty(Fig)
        Fig = FigUtils.createSScFigure('Position', centrageFig(1000, 500));
    else
        FigUtils.createSScFigure(Fig)
    end
    
    if isempty(findobj(Fig, 'Text', 'SonarScope'))
        f = uimenu(Fig, 'Text', 'SonarScope');
        uimenu(f, 'Text', 'Save stats in a file', 'Callback', @SaveStats);
    end
    
    hp = uipanel('Title', Lang('courbe hypsometrique', 'Hypsometric curve'), 'FontSize', 8, ...
        'BackgroundColor', 'white', 'Position', [.02 .02 .96 .96]);
    axes('parent', hp);
    
    PlotUtils.createSScPlot(100-(N/max(N)*100), bins); grid on; hold on,
    PlotUtils.createSScPlot(100-(NBrut/max(NBrut)*100), bins, 'b');
    
    if isempty(Titre)
        title(sprintf('%s sur %d valeurs', nom, length(bins)), 'interpreter', 'none')
    else
        title(sprintf('%s\n%s sur %d valeurs', Titre, nom, length(bins)), 'interpreter', 'none')
    end
    
    ylabel(XLabel);
    xlabel( Lang('surface cummul�e (%)', 'cumulative surface (%)') );
end

%% Par ici la sortie

if  ~Cumul && nargout == 0
    
    % --------------------
    % Auto-plot activation
    
    if isempty(Fig)
        Fig = FigUtils.createSScFigure;
    else
        Fig = figure(Fig);
    end
    hp    = uipanel('Position',[.05 .05 .58 .90]);
    hp(2) = uipanel('Position',[.65 .05 .38 .90]);
    
    if isempty(findobj(Fig, 'Text', 'SonarScope'))
        f = uimenu(Fig, 'Text', 'SonarScope');
        uimenu(f, 'Text', 'Save stats in a file', 'Callback', @SaveStats);
    end
    
    set(hp(1), 'Title', 'Histogram', 'FontSize',8, 'BackgroundColor', 'white');
    hAxes = axes(hp(1));
    PlotUtils.createSScPlot(hAxes, bins, N); grid(hAxes(1), 'on');
    if isempty(Titre)
        title(hAxes, sprintf('%s sur %d valeurs', nom, length(bins)), 'interpreter', 'none')
    else
        title(hAxes, sprintf('%s\n%s sur %d valeurs', Titre, nom, length(bins)), 'interpreter', 'none')
    end
    if flagFiltGauss
        hold on; PlotUtils.createSScPlot(hAxes(1), bins, NBrut, 'Color', [0.7 0.7 0.7]); hold off;
    end
    
    xlabel(hAxes, XLabel)
    if flagHisto
        ylabel(hAxes,'Histo')
    end
    if flagProba
        ylabel(hAxes,'Proba')
    end
    if flagDdp
        ylabel(hAxes(1),'Ddp')
    end
    
    [StatValues, str] = stats(X, 'ValNaN', ValNaN);
    %     hp = get(hAxes(2), 'Parent');
    set(hp(2), 'Title', 'Statistics', 'FontSize', 8, 'BackgroundColor', 'white');
    % Effacement dans le cas pr�sent du Handle d'axe au profit d'un
    % contr�le de type Text.
    
    h = uicontrol(hp(2), 'Style','text', 'Units', 'normalized',...
        'Position', [.1 .1 .8 .8],...
        'String', str, 'HorizontalAlignment', 'left');
    
    str1 = sprintf('Le skewness est representatif de l''''assysmetie. Sa valeur est 0 pour une gaussienne.\nLe kurtosis est representatif de l''''aplatissement. Sa valeur est 3 pour une gaussienne.\nQuantiles sont utiles pour realiser un rehaussement de contraste.');
    str2 = sprintf('Skewness informs on dyisymetry. Value=0 for a gaussian distribution.\nkurtosis informs on "flateness". Value=3 for a gaussin distribution.\nQuantils are useful for contrast enhancement.');
    set(h, 'UserData', Lang(str1,str2));
    set(h, 'ButtonDownFcn', 'my_warndlg(get(gcbo, ''UserData''), 0); disp(get(gcbo, ''UserData''))');
    
    %% Sauvegarde de donnees utiles dans le UserData de la figure
    
    set(gcf, 'UserData', {hAxes(1), bins, N, Fig})
    
    %% Trac� des composantes th�oriques ou estim�es
    
    if ~isempty(ParamsGauss)
        set(Fig, 'CurrentAxes', hAxes(1))
        hold on;
        moy   = ParamsGauss{1};
        sigma = ParamsGauss{2};
        n = length(moy);
        if length(ParamsGauss) == 3
            p = ParamsGauss{3};
            p = p / sum(p);
        else
            p = ones(1,n);
        end
        composante = zeros(n, length(bins));
        
        if flagDdp
            coef = 1;
        elseif flagProba
            coef = dx(1);
        else
            coef = sum(N) * dx(1);
        end
        
        str = [];
        for k=1:n
            composante(k,:) = composante(k,:) + p(k) * normpdf(bins, moy(k), sigma(k));
            PlotUtils.createSScPlot(bins, composante(k,:) * coef, 'k--');
            str{end+1} = ['-- Composante ' num2str(k) '--']; %#ok<AGROW>
            str{end+1} = ['Moyenne    : '  num2str(moy(k))]; %#ok<AGROW>
            str{end+1} = ['Ecart-type : '  num2str(sigma(k))]; %#ok<AGROW>
            str{end+1} = ['Proportion : '  num2str(p(k))]; %#ok<AGROW>
        end
        synthese = sum(composante, 1);
        EQM = eqm(N0, synthese*sumN0*dx(1));
        str{end+1} = ' ';
        str{end+1} = ['EQM : ' num2str(EQM)];
        PlotUtils.createSScPlot(bins, synthese * coef, 'k');
        
        str = sprintf('%s\n', str{:});
        set(gcf, 'CurrentAxes', hAxes(1))
        text(0, 0.3, str, 'interpreter', 'none'); % TODO : o� est-ce que �a s'affiche ? Je ne vois rien
    end
    set(Fig, 'CurrentAxes', hAxes(1));
    
else
    if ~isnan(ValNaN)
        sub = find(XIn == ValNaN);
        XIn = singleUnlessDouble(XIn, ValNaN);
        XIn(sub) = NaN; %#ok<FNDSB> 
    end
    
    clear sub
    N = N(:)';
    bins = bins(:)';
    if nargout > 2
        frontieres = bins(1:end-1) + diff(bins)/2;
        for k=1:length(frontieres)
            subX = find(XIn <= frontieres(k));
            val{k} =  XIn(subX); %#ok
            XIn(subX) = NaN;
            sub{k} = subX; %#ok<AGROW> 
        end
        k = length(frontieres) + 1;
        subX = find(XIn > frontieres(end));
        val{k} = XIn(subX);
        sub{k} = subX;
    end
end

    function SaveStats(varargin)
        nomFic = fullfile(my_tempdir, 'Stats.dat');
        nomVar = {Lang('Nom du fichier', 'File name') ; Lang('Description', 'Label')};
        value = {nomFic ; 'Area 0'};
        [value, flag] = my_inputdlg(nomVar, value);
        if ~flag
            return
        end
        nomFic = value{1};
        nomArea = value{2};
        
        if exist(nomFic, 'file')
            fid = fopen(nomFic, 'a');
        else
            fid = fopen(nomFic, 'w');
            fprintf(fid, 'Mean\tStd\tMedian\tMin\tMax\tQ0.5\tQ99.5\tNan (%%)\tSampling\tLabel\n');
        end
        fprintf(fid, '%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%s\n', ...
            StatValues.Moyenne, StatValues.Sigma, StatValues.Mediane, ...
            StatValues.Min, StatValues.Max, ...
            StatValues.Quant_25_75(1), StatValues.Quant_25_75(2), ...
            StatValues.PercentNaN, StatValues.Sampling, ...
            nomArea);
        fclose(fid);
    end
end
