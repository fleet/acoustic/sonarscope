% Calcul de l'histogramme des probas ou de la ddp conjointes
% ie. (Histogramme bi-dimentionnel)
%
% Syntax
%   [N, binsX, binsY, Val, Sub] = histo1D(X, Y)
%   [N, binsX, binsY, Val, Sub] = histo2D(X, Y, binsX)
%   [N, binsX, binsY, Val, Sub] = histo2D(X, Y, binsX, binsY)
%
% INPUT PARANbinsETERS :
%   X : matrice des valeurs
%   Y : matrice des valeurs
%
% Name-Value Pair Arguments
%   binsX : Si scalaire : Nombre de bins en X.
%           Si vecteur  : Centres des bins en X
%   binsY : Si scalaire : Nombre de bins en Y.
%           Si vecteur  : Centres des bins en Y
%   nomX  : Nom de la variable representee en X (utilise si auto_plot)
%   nomY  : Nom de la variable representee en Y (utilise si auto_plot)
%
% Name-only Arguments
%   Histo      : Calcul de l'histogramme (par defaut)
%   Proba      : Calcul de la proba
%   Ddp        : Calcul de la densite de proba
%   FiltGauss  : Filtrage gaussien du resultat
%   Cumul      : Fonction cumulee
%
% Output Arguments
%   []    : Auto-plot activation
%   N     : Histogramme ou proba ou ddp pour chaque classe
%   binsX : Centres des classes en X
%   binsY : Centres des classes en Y
%   Val   : Liste des valeurs par classe
%   Sub   : Adresses des valeurs dans X
%
% Examples
%   nomFic = getNomFicDatabase('EM300_ExStr.imo')
%   [flag, a] = import(cl_image, nomFic)
%   X = get_Image(a(1));
%   Y = get_Image(a(2));
%   histo1D(X);
%   figure; imagesc(X, [-38 -18]); title('X'); colormap(jet(256));
%
%   histo1D(Y);
%   figure; imagesc(Y, [-50 50]); title('Y');; colormap(jet(256));
%
%   histo2D(X, Y);
%   [N, binsX, binsY] = histo2D(X, Y);
%     figure; imagesc(binsX,binsY, N)
%
%   histo2D(X, Y, 'Proba');
%   histo2D(X, Y, 'Ddp');
%
%   [N, binsX, binsY] = histo2D(X, Y, 'Ddp');
%   figure; imagesc(binsX, binsY, N);
%
%   [N, binsX, binsY, Val, Sub] = histo2D(X, Y);
%
% See also my_hist stats histo1DAuthors
% Authors : GLC + JMA
%-------------------------------------------------------------------------------

function varargout = histo2D(X, Y, varargin)

% [varargin, flagProba]     = getFlag(varargin, 'Proba');
% [varargin, flagDdp]       = getFlag(varargin, 'Ddp');
% [varargin, flagFiltGauss] = getFlag(varargin, 'FiltGauss');
% [varargin, Cumul]         = getFlag(varargin, 'Cumul');

[varargin, nomXY] = getPropertyValue(varargin, 'nomXY', 'Histobidim');
[varargin, nomX]  = getPropertyValue(varargin, 'nomX',  []);
[varargin, nomY]  = getPropertyValue(varargin, 'nomY',  []);

%% Dans le cas o� on ne pr�cise pas le nombre de bins, on impose 100 car my_hist prend 10 par defaut

switch length(varargin)
    case 0
        binsX = 100;
        binsY = 100;
    case 1
        binsX = varargin{1};
        binsY = 100;
    otherwise
        binsX = varargin{1};
        binsY = varargin{2};
end

sub = isnan(X) | isnan(Y);
X(sub) = NaN;
Y(sub) = NaN;

if length(binsX) == 1
    minX = min(X(:));
    maxX = max(X(:));
    binsX = linspace(minX, maxX, binsX);
end
if length(binsY) == 1
    minY = min(Y(:));
    maxY = max(Y(:));
    binsY = linspace(minY, maxY, binsY);
end

%% Calcul de l'histogramme empirique sur X

[~, binsY, ValY, SubY] = histo1D(Y, binsY);

%% Calcul des histogrammes sur chaque element de ValX

n = length(binsY);
NXY = zeros(n, length(binsX));
Moy = NaN(n, 1);
for k=1:n
    if ~isempty(SubY{k})
        x = X(SubY{k});
        x = x(~isnan(x));
        if ~isempty(x)
            Moy(k) = mean(x);
            NX = histo1D(x, binsX);
            NXY(k,:) = NX;
        end
    end
end

%% Par ici la sortie

if nargout == 0
    
    %% Auto-plot activation
    
    FigUtils.createSScFigure;
    subplot(5, 5, [1:4 6:9 11:14 16:19]);
    imagesc(binsX, binsY, NXY); axis xy; axis tight;
    title(nomXY)
    
    subplot(5, 5, [5 10 15 20])
    PlotUtils.createSScPlot(sum(NXY, 2), binsY); grid on; axis tight;
    if isempty(nomY)
        title('Histo')
    else
        xlabel(['Histo ' nomY])
        ylabel(nomY)
    end
    
    subplot(5, 5, 21:24)
    PlotUtils.createSScPlot(binsX, sum(NXY)); grid on; axis tight;
    if isempty(nomX)
        title(['Histo of ' nomX])
    else
        xlabel(nomX)
        ylabel(['Histo of ' nomX])
    end
    
else
    varargout{1} = NXY;
    varargout{2} = binsX;
    varargout{3} = binsY;
    %     varargout{4} = Moy;
    if nargout > 3
        k = 0;
        frontieresX = binsX(1:end-1) + diff(binsX)/2;
        frontieresY = binsY(1:end-1) + diff(binsY)/2;
        NX = length(frontieresX);
        NY = length(frontieresY);
        sub = cell(NY+1,NX+1);
        val = cell(NY+1,NX+1);
        NbLoops = NX * NY;
        str1 = 'Indexation des points dans le calcul de l''histogramme 2D en cours';
        str2 = 'Computing Histo 2D index';
        hw = create_waitbar(Lang(str1,str2), 'N', NbLoops);
        for k1=1:NY
            for k2=1:NX
                my_waitbar(k, NbLoops, hw);
                k = k+1;
                sub{k1,k2} = find((X <= frontieresX(k2)) & (Y <= frontieresY(k1)));
                val{k1,k2} = X(sub{k1,k2});
                X(sub{k1,k2}) = NaN;
            end
            sub{k1,  NX+1}  = find((X > frontieresX(end)) & (Y <= frontieresY(end)));
            X(sub{k1,NX+1}) = NaN;
        end
        for k=1:NX
            sub{NY+1,k} = find((X <= frontieresX(end)) & (Y > frontieresY(end)));
            val{NY+1,k} = X(sub{end,end});
        end
        sub{NY+1,NX+1} = find((X > frontieresX(end)) & (Y > frontieresY(end)));
        val{NY+1,NX+1} = X(sub{NY+1,NX+1});
        
        varargout{4} = val;
        varargout{5} = sub;
        my_close(hw, 'MsgEnd');
    end
end
