function str = infoCourbesStats(bilan, varargin)

str = {};
str{end+1} = ' ';
str{end+1} = '--------------------------------------------------------'; 
a = bilan{1}(1);
if isnumeric(a.nomZoneEtude)
    str{end+1} = sprintf('nomZoneEtude : %s', num2str(a.nomZoneEtude));
else
    str{end+1} = sprintf('nomZoneEtude : %s', a.nomZoneEtude);
end
str{end+1} = ' ';

for k=1:length(bilan{1})
    a = bilan{1}(k);
    
    str{end+1} = sprintf(' --- Curve %d ---', k); %#ok<AGROW>
    str{end+1} = sprintf('x                     <-- %s', num2strCode(a.x)); %#ok<AGROW>
    str{end+1} = sprintf('y                     <-- %s', num2strCode(a.y)); %#ok<AGROW>
    if isfield(a, 'ymedian')
        str{end+1} = sprintf('ymedian               <-- %s', num2strCode(a.ymedian)); %#ok<AGROW>
    end
    if isfield(a, 'nom')
        str{end+1} = sprintf('nom                   <-- %s', a.nom); %#ok<AGROW>
    end
    str{end+1} = sprintf('sub                   <-- %s', num2strCode(a.sub)); %#ok<AGROW>
    str{end+1} = sprintf('nx                    <-- %s', num2strCode(a.nx)); %#ok<AGROW>
    str{end+1} = sprintf('std                   <-- %s', num2strCode(a.std)); %#ok<AGROW>
    str{end+1} = sprintf('titreV                <-- %s', a.titreV); %#ok<AGROW>
    str{end+1} = sprintf('NY                    <-- %d', a.NY); %#ok<AGROW>
    str{end+1} = sprintf('nomX                  <-- %s', a.nomX); %#ok<AGROW>
    if isfield(a, 'Biais')
        str{end+1} = sprintf('Biais                 <-- %f', a.Biais); %#ok<AGROW>
    end
    if isfield(a, 'Unit')
        str{end+1} = sprintf('Unit                  <-- %s', a.Unit); %#ok<AGROW>
    end
    str{end+1} = sprintf('commentaire           <-- %s', a.commentaire); %#ok<AGROW>
    str{end+1} = sprintf('Tag                   <-- %s', a.Tag); %#ok<AGROW>
    str{end+1} = sprintf('DataTypeValue         <-- %s', a.DataTypeValue); %#ok<AGROW>
    for k2=1:length(a.DataTypeConditions)
        str{end+1} = sprintf('DataTypeConditions    <-- %s', a.DataTypeConditions{k2}); %#ok<AGROW>
        str{end+1} = sprintf('Support               <-- %s', a.Support{k2}); %#ok<AGROW>
    end
    if isfield(a, 'GeometryType')
        str{end+1} = sprintf('GeometryType          <-- %s', a.GeometryType); %#ok<AGROW>
    end
    if isfield(a, 'TypeCourbeStat')
        str{end+1} = sprintf('TypeCourbeStat        <-- %s', a.TypeCourbeStat); %#ok<AGROW>
    end
    str{end+1} = ' '; %#ok<AGROW>
end

%% Concatenation en une chaine

str = cell2str(str);
