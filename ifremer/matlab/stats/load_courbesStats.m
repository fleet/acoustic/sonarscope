% Lecture de courbes de statistiques conditionnelles
%
% Syntax
%   Courbes = load_courbesStats(nomFicMat) 
%
% Input Arguments
%   nomFicMat : Nom du fichier .mat
%
% Examples 
%
% See also courbesStats cl_image/histo Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [CourbeConditionnelle, flag] = load_courbesStats(nomFicMat)

flag = 0;

if isempty(nomFicMat)
    return
end

W = warning;
warning('Off')
CourbeConditionnelle = loadmat(nomFicMat, 'nomVar', 'CourbeConditionnelle');

%% Ajout JMA le 16/11/2017 pour pouvoir r�cup�rer les mod�les sauv�s dans les .mat (mod�le EK60 de Dimitrios par exemple)

for k=1:length(CourbeConditionnelle)
    if isfield(CourbeConditionnelle(k), 'bilan') && ~isempty(CourbeConditionnelle(k).bilan) && isfield(CourbeConditionnelle(k).bilan{1}(1), 'model') && ~isempty(CourbeConditionnelle(k).bilan{1}(1).model)
        if isempty(CourbeConditionnelle(k).bilan{1}(1).model.functionName)
            CourbeConditionnelle(k).bilan{1}(1).model.functionName = CourbeConditionnelle(k).bilan{1}(1).model.Name;
        end
    end
end
% Fin rajout JMA

warning(W)

if isempty(CourbeConditionnelle)
    str1 = sprintf('Le fichier %s ne contient aucune courbe.', nomFicMat);
    str2 = sprintf('%s does not contain any curve.', nomFicMat);
    my_warndlg(Lang(str1,str2), 1);
    return
end

flag = 1;
