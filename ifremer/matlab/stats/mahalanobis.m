% Calcul de la distance de Mahalanobis modifiee
%
% Syntax
%   d = mahalanobis(Data, XY) 
%
% Input Arguments 
%   Data : Point(s) pour le(s) quel(s) on veut calculer la distance de
%          mahalanobis par rapport a un nuage de points
%   XY   : Coordonees des points definissant le nuage de points
%
% Output Arguments 
%   [] : Auto-plot activation
%   d  : Distance de Mahalanobis modifiee (sqrt(mahal) / 2)
%
% Remarks : Cette fonction peur etre utilisee pour rechercher des "outliers"
% 
% Examples
%   X = 10+randn(20);
%   Y = 20+3*randn(20);
%   d = mahalanobis([X(1) Y(1)], [X(:) Y(:)])
%
%   XY = mvnrnd([0 0],[1 0.9;0.9 1],100);
%   data = [10 10];
%   d = mahalanobis(data, XY)
%   d = mahalanobis([data; XY], XY)
%
% See also mahal dist1mahal cl_cooc/plotNuages Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function varargout = mahalanobis(Data, XY)

[rx,cx] = size(XY);
[ry,cy] = size(Data);

if cx ~= cy
   error('Requires the inputs to have the same number of columns.');
end

if rx < cx
   error('The number of rows of XY must exceed the number of columns.');
end
if any(imag(XY(:))) || any(imag(Data(:)))
   error('MAHAL does not accept complex inputs.');
end

meanXY = mean(XY,1);

C = XY - meanXY(ones(rx,1),:);
[~,R] = qr(C,0);

ri = R' \ (Data - meanXY(ones(ry,1),:))';

d = sum(ri .* ri, 1)' * (rx-1);
d = sqrt(d) / 2;
sub = (d < 1);

if nargout == 0
    minX = min(XY(:,1));
    maxX = max(XY(:,1));
    minY = min(XY(:,2));
    maxY = max(XY(:,2));
    
    minX = min(minX, min(Data(:,1)));
    maxX = max(maxX, min(Data(:,1)));
    minY = min(minY, min(Data(:,2)));
    maxY = max(maxY, min(Data(:,2)));
    
    xg = linspace(minX, maxX, 100);
    yg = linspace(minY, maxY, 100);
    [Xg,Yg] = meshgrid(xg, yg);
    
%     DMahal = zeros(100, 100) * Inf;
    D = mahalanobis([Xg(:) Yg(:)], XY);
    N = sqrt(size(D,1));
    D = reshape(D, N, N);
    figure; imagesc(xg, yg, D); axis xy; colorbar
    hold on; plot(XY(:,1), XY(:,2), 'k+'); %axis equal; axis tight;
    hold on; plot(Data(:,1), Data(:,2), 'r*'); %axis equal; axis tight;
    hold on; plot(XY(sub,1), XY(sub,2), 'g*'); %axis equal; axis tight;
else
    varargout{1} = d;
    varargout{2} = sub;
end
