% Recherche de gaussiennes constituant la densite de probabilite d'un signal 
%
% Syntax
%   [moy, sigma, p, Eqm] = melangeGaussiennes1D(y)
%   [moy, sigma, p, Eqm] = melangeGaussiennes1D(y, n)
%
% Input Arguments
%   I1 : Signal
% 
% Name-Value Pair Arguments
%   n : Nombre de gaussiennes a rechercher. Si n n'est pas donne, la
%       fonction tente de trouver toute seule ne nombre de gaussiennes.
%   
% Output Arguments
%   []    : auto-plot activation
%   moy   : Moyennes des gaussiennes
%   sigma : Ecart-types des gaussiennes
%   p     : Proportions des gaussiennes
% 
% Examples 
%   m = [1 3 4 5]; 
%   s = [1 2 1 2];
%   p = [100 50 50 100];
%
%   y1 = m(1) + (s(1) * randn(100, p(1)));
%   y2 = m(2) + (s(2) * randn(100, p(2)));
%   y3 = m(3) + (s(3) * randn(100, p(3)));
%   y4 = m(4) + (s(4) * randn(100, p(4)));
%
%   histo1D(y1, 'ParamsGauss', {m(1), s(1)});
%
%   y = [y1];           n = 1;
%   y = [y1 y2];        n = 2;
%   y = [y1 y2 y3];     n = 3;
%   y = [y1 y2 y3 y4];  n = 4;
%
%   figure; imagesc(y); histo1D(y)
%   y = shake(y);
%   figure; imagesc(y);
%
%   histo1D(y, 'ParamsGauss', {m(1:n), s(1:n), p(1:n)})
%
%   melangeGaussiennes1D(y, n)
%   [moy, sigma, p, Eqm] = melangeGaussiennes1D(y, n)
%
%   melangeGaussiennes1D(y)
%   [moy, sigma, p, Eqm] = melangeGaussiennes1D(y)
%
% See also normpdf histo1D stats Authors
% References : A component-wise EM Algorithm for Mixtures No 3746 Aout 1999
% Authors : IK + JMA
%--------------------------------------------------------------------------------

function varargout = melangeGaussiennes1D(y, varargin)

if nargin == 1
    for n=1:4
        [moyn, sigman, pn, Eqmn] = melangeGaussiennes1D(y, n);
        moy{n} = moyn; %#ok<AGROW>
        sigma{n} = sigman; %#ok<AGROW>
        p{n} = pn; %#ok<AGROW>
        Eqm(n) = Eqmn; %#ok<AGROW>
    end
    %         figure;
    %         subplot(2,1,1); plot(abs(Eqm ./ max(Eqm))); grid on; title('Eqm relatif')
    %         subplot(2,1,2); plot(abs(diff(Eqm) ./ Eqm(1:end-1))); grid on; title('Eqm relatif')

    % Premier essai : pas concluant
    [~, ind] = min(Eqm);
    n = ind(1);

    %         % Deuxieme essai : pas concluant
    %         ind = find(abs(diff(Eqm) ./ Eqm(1:end-1)) < 0.1);
    %         if isempty(ind)
    %             [pppp, ind] = min(Eqm);
    %         end
    %         n = ind(1);

    if nargout == 0

        % -------------------------------------------------
        % Affichage de la ddp si pas "auto-plot activation"

        for i=1:4
            histo1D(y, 'ParamsGauss', {moy{i}, sigma{i}, p{i}})
            if i == n
                disp(['C''est la ' num2str(n) ' qui a ete retenue'])
            end
        end

    else
        % Variables de sortie

        varargout{1} = moy{n};
        varargout{2} = sigma{n};
        varargout{3} = p{n};
        varargout{4} = Eqm(n);
    end
    return
else
    n = varargin{1};
end

% ---------------
% Initialisations

y    = y(:);
ny   = length(y);
moyy = mean(y); % Valeur moyenne de la donnee
stdy = std(y);  % Ecart-type de la donnee

for i=1:n
    p(i)   = 1/n;       %#ok<AGROW> % Poids equivalent pour toutes les composantes
    var(i) = stdy ^ 2;  %#ok<AGROW> % Meme variance pour toutes les composantes
end
% Repartition lineaire des moyenne des differentes composantes
moy = linspace(moyy - 2*stdy, moyy + 2*stdy, n);

%% Calcul de la ddp

[N, bins] = histo1D(y, 'Ddp');

%% Algorithme EM

seuil = 100;
k = 0;
%     while (seuil > 0.1) | (k < 50)
while seuil > 0.1
    k = k + 1;

    % -------
    % Etape E

    denominateur = 0;
    for i=1:n
        proba{i} = normpdf(y, moy(i), sqrt(var(i))); %#ok<AGROW>
        denominateur = denominateur + (p(i) * proba{i});
    end

    for i=1:n
        t{i} = (p(i) * proba{i}) ./ denominateur; %#ok<AGROW>
        somme_t(i) = sum(t{i}); %#ok<AGROW>
    end

    % -------
    % Etape M

    for i=1:n
        p(i)     = sum(t{i}) / ny; 
        moy(i)   = sum(t{i} .* y) / somme_t(i);
        v        = t{i} .* (y - moy(i)) .^ 2;
        var(i)   = sum(v) / somme_t(i); 
        sigma(i) = sqrt(var(i)); %#ok<AGROW>
    end

    composante = zeros(n, length(bins));
    for i=1:n
        composante(i,:) = composante(i,:) + p(i) * normpdf(bins, moy(i), sigma(i));
    end
    synthese = sum(composante, 1);

    coef = sum(N) / sum(synthese);
    Eqm(k) = eqm(N, synthese*coef); %#ok<AGROW>
    if k > 2
        seuil = abs(100 * diff(Eqm(k-1:k)) / Eqm(k));
        str = sprintf('n=%d  k=%d Eqm=%f', n, k, seuil);
        %disp(str)
    end
end

% -----------------
% Par ici la sortie

if nargout == 0

    % -------------------------------
    % Affichage des valeurs a l'ecran

%     moy
%     sigma
%     p

    % ------------------------------------------------------
    % Affichage du pourcentage de variation de l'eqm relatif

    %         figure; plot(diff(Eqm) ./ Eqm(1:end-1)); grid on; title('Eqm')

    % -------------------
    % Affichage de la ddp

    histo1D(y, 'ParamsGauss', {moy, sigma, p})

else
    varargout{1} = moy;
    varargout{2} = sigma;
    varargout{3} = p;
    varargout{4} = Eqm(k);
end
