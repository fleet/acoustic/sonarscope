% Calcul de statistiques group�es avec prise en compte du nombre de points ayant servi au calcul de x
% Cette fonction est une copie de grpstat amelior�e. ATTENTION sem n'a pas
% la m�me signification que dans grpstats
%
% Syntax
%   [means, sem, counts, gname] = my_grpstats(x, Nx, group, alpha)
%
% Input Arguments
%   x     : Echantillons de la donnee
%   Nx    : Nombre de points ayant servi au calcul de x
%   group : Etiquettes correspondant  la donnee
%
% Name-Value Pair Arguments
%   alpha : 
%
% Output Arguments
%   means  : Matrice contenant les moyennes
%   sem    : Matrice contenant les ecart-types. ATTENTION sem n'a pas
%            la meme signification que dans grpstats
%   counts : Matrice contenant les nombre de points moyennes. 
%   gname  : Noms des groupes. 
%
% Examples 
%   group = unidrnd(4,100,1);
%   true_mean = 1:5;
%   true_mean = true_mean(ones(100,1),:);
%   x = normrnd(true_mean,1);
%   means = grpstats(x, group)
%
% See also cl_image cl_image/histo Authors
% Authors : B.A. Jones The MathWorks, Inc., JMA
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%   B.A. Jones 3-5-95
%   Copyright 1993-2002 The MathWorks, Inc. 
%   $Revision: 2.15 $  $Date: 2002/01/17 21:30:49 $
% -------------------------------------------------------------------------

function [means, medians, sem, counts, gname] = my_grpstats(x, Nx, group, alpha)

if (nargin == 1) && (length(x(:))== 1) && ishandle(x)
    resizefcn(x);
    return
end

if nargin < 3
    error('my_grpstats requires at least two arguments.')
elseif nargin == 4
    if numel(alpha) ~= 1
        error('ALPHA must be a scalar.');
    elseif ~isnumeric(alpha) || (alpha<=0) || (alpha>=1)
        error('ALPHA must be a number larger than 0 and smaller than 1.');
    end
end

% Get grouping variable information
[row,cols] = size(x);
[group,glabel,gname,multigroup] = mgrp2idx(group,row);

n       = max(group(~isnan(group)));
means   = zeros(n,cols);
medians = means;
sem     = means;
counts  = means;

for idx1=1:cols
    k = find(~isnan(x(:,idx1)) & ~isnan(group));
    y = x(k,idx1);
    idx = group(k);
%     if (any(any(group)) < 0) || (any(any(group)) ~= floor(group))
%         error('Requires a vector second input argument with only positive integers.');
%     end
    
    for idx2 = 1:n
        k = find(idx == idx2);
        kk = (Nx(k) > 0);
        k = k(kk);
        if isempty(k)
            means(idx2,idx1)   = NaN;
            medians(idx2,idx1) = NaN;
            sem(idx2,idx1)     = NaN;
            counts(idx2,idx1)  = 0;
        else
            Nxk = sum(Nx(k));
            m = sum(y(k) .* Nx(k)) / Nxk;
            
            sem(idx2,idx1)     = sqrt(sum(((y(k) - m) .^ 2) .* Nx(k)) / Nxk);
            counts(idx2,idx1)  = Nxk;
            means(idx2,idx1)   = m;
            medians(idx2,idx1) = median(y(k));
        end
    end
end

if nargin == 4
    p = 1 - alpha/2;
    xd = (1:n)';
    xd = xd(:,ones(cols,1));
    h = errorbar(xd,means,tinv(p,counts) .* sem);
    set(h(1+end/2:end),'LineStyle','none','Marker','o','MarkerSize',2);
    set(gca,'Xlim',[0.5 n+0.5],'Xtick',(1:n));
    xlabel('Group');
    ylabel('Mean');
    if (multigroup)
        % Turn off tick labels and axis label
        set(gca, 'XTickLabel','','UserData',size(gname,2));
        xlabel('');
%         ylim = get(gca, 'YLim');
        
        % Place multi-line text approximately where tick labels belong
%         for j=1:n
%             ht = text(j,ylim(1),glabel{j,1},'HorizontalAlignment','center',...
%                 'VerticalAlignment','top', 'UserData','xtick');
%         end
        
        % Resize function will position text more accurately
        set(gcf, 'ResizeFcn', sprintf('my_grpstats(%d)', gcf), ...
            'Interruptible','off');
        resizefcn(gcf);
    else
        set(gca, 'XTickLabel',glabel);
    end
    title('Means and Confidence Intervals for Each Group');
    set(gca, 'YGrid', 'on');
end

function resizefcn(f)
% Adjust figure layout to make sure labels remain visible
h = findobj(f, 'UserData','xtick');
if (isempty(h))
    set(f, 'ResizeFcn', '');
    return;
end
ax = get(f, 'CurrentAxes');
nlines = get(ax, 'UserData');

% Position the axes so that the fake X tick labels have room to display
set(ax, 'Units', 'characters');
p = get(ax, 'Position');
ptop = p(2) + p(4);
if (p(4) < nlines+1.5)
    p(2) = ptop/2;
else
    p(2) = nlines + 1;
end
p(4) = ptop - p(2);
set(ax, 'Position', p);
set(ax, 'Units', 'normalized');

% Position the labels at the proper place
xl = get(gca, 'XLabel');
set(xl, 'Units', 'data');
p = get(xl, 'Position');
ylim = get(gca, 'YLim');
p2 = (p(2)+ylim(1))/2;
for j=1:length(h)
    p = get(h(j), 'Position') ;
    p(2) = p2;
    set(h(j), 'Position', p);
end



function [ogroup,glabel,gname,multigroup] = mgrp2idx(group,rows,sep)
%MGRP2IDX Convert multiple grouping variables to index vector
%   [OGROUP,GLABEL,GNAME,MULTIGROUP] = MGRP2IDX(GROUP,ROWS) takes
%   the inputs GROUP, ROWS, and SEP.  GROUP is a grouping variable (numeric
%   vector, string matrix, or cell array of strings) or a cell array
%   of grouping variables.  ROWS is the number of observations.
%   SEP is a separator for the grouping variable values.
%
%   The output OGROUP is a vector of group indices.  GLABEL is a cell
%   array of group labels, each label consisting of the values of the
%   various grouping variables separated by the characters in SEP.
%   GNAME is a cell array containing one column per grouping variable
%   and one row for each distinct combination of grouping variable
%   values.  MULTIGROUP is 1 if there are multiple grouping variables
%   or 0 if there are not.

%   Tom Lane, 12-17-99
%   Copyright 1993-2002 The MathWorks, Inc. 
%   $Revision: 1.4 $  $Date: 2002/02/04 19:25:44 $

multigroup = (iscell(group) & size(group,1)==1);
if (~multigroup)
   [ogroup,gname] = grp2idx(group);
   glabel = gname;
else
   % Group according to each distinct combination of grouping variables
   ngrps = size(group,2);
   grpmat = zeros(rows,ngrps);
   namemat = cell(1,ngrps);
   
   % Get integer codes and names for each grouping variable
   for j=1:ngrps
      [g,gn] = grp2idx(group{1,j});
      grpmat(:,j) = g;
      namemat{1,j} = gn;
   end
   
   % Find all unique combinations
   [urows,~,uj] = unique(grpmat,'rows');
   
   % Create a cell array, one col for each grouping variable value
   % and one row for each observation
   ogroup = uj;
   gname = cell(size(urows));
   for j=1:ngrps
      gn = namemat{1,j};
      gname(:,j) = gn(urows(:,j));
   end
   
   % Create another cell array of multi-line texts to use as labels
   glabel = cell(size(gname,1),1);
   if (nargin > 2)
      nl = sprintf(sep);
   else
      nl = newline;
   end
   fmt = sprintf('%%s%s', nl);
   lnl = length(fmt)-3; % one less than the length of nl
   for j=1:length(glabel)
      gn = sprintf(fmt, gname{j,:});
      gn(end-lnl:end) = [];
      glabel{j,1} = gn;
   end
end
