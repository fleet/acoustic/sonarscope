% Fct de remplacement de histcounts selon les pr�conisations R2019a.
%
% Syntax
%   [no,xo] = my_hist(Y, bins);
%
% Input Arguments
%   Y    : matrice de valeurs � d�compter
%   bins : intervalles de d�comptage.
% 
% Examples 
%   TypeDatagram = [10:10:70 100 100 100 101 102 200 200];
%   Code    = min(TypeDatagram):max(TypeDatagram);
%   my_hist(TypeDatagram, Code);
%   [no,xo] = my_hist(TypeDatagram, Code);
% ou 
%     im   = rand(50,50);
%     xmin = min(im(:));
%     xmax = max(im(:));
%     xpas = (xmax - xmin) / 100;
%     x    = xmin:xpas:xmax;
%     my_hist(im(:), x);
%     h = my_hist(im(:), x);
% ou
%     figure; imagesc(Lena); colormap(gray(256))
%     colorbarHist
%
% See also mat2image
%            https://fr.mathworks.com/help/matlab/creating_plots/replace-discouraged-instances-of-hist-and-histc.html
% Authors : GLU
%--------------------------------------------------------------------------------

function [no, xo] = my_hist(varargin)

switch nargin
    case 1
        Y = varargin{1};
        Param2 = 100;
    case 2
        Y = varargin{1};
        N = varargin{2};
        Y = Y(isfinite(Y(:))); % Ajout JMA le 12/02/2020
        if numel(N) == 1
            bins = linspace(min(Y), max(Y), N);
        else
            bins = N;
        end
        bins = double(bins);
        if iscolumn(bins)
            bins = bins';
        end
        deltaX = diff(bins)/2;
        edges  = [bins(1)-deltaX(1), bins(1:end-1)+deltaX, bins(end)+deltaX(end)];
        edges(2:end) = edges(2:end)+eps(edges(2:end));
        Param2 = edges;
    otherwise
        my_breakpoint('FctName', my_hist);
end

if nargout == 0
    histogram(Y);
    % Pas de test de comparaison avec ancienne m�thode.
    return
end

[no, xo] = histcounts(Y, Param2);
% On ote la derni�re limite pour �tre raccord avec le vecteur proche de hist.
xo = xo(1:end-1);

%{
if isdeployed
    return
end

nOld = hist(Y, bins); % A �t� remplac� par histcounts
if ~isequal(no, nOld)
    difn = no - nOld;
    if sum(abs(difn) ~= 0) > 2
        % ----------------------------------------------------------------
        % Impose un beakpoint ici pour rep�rer les appels � cette fonction
        my_breakpoint('FctName', 'my_hist');
        % ----------------------------------------------------------------
        %{
        figure; plot(figure, 'or'); grid on;
        figure; plot(nOld, '+b'); grid on; hold on; plot(no, 'xr');
        %}
        pppp = dbstack;
        if numel(pppp)> 1
            fprintf('==> Beurrk ! Ne remplace pas bien histcount par %s depuis %s\n', mfilename, pppp(2).name);
        else
            fprintf('==> Beurrk ! Ne remplace pas bien histcount par %s depuis %s\n', mfilename, 'Workspace');
        end
    end
end
%}
