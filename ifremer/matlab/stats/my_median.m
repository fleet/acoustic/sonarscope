function y = my_median(x)

switch class(x)
    case {'uint8';'int8';'uint16';'int16'}
        x = single(x);
    otherwise
        x = double(x);
end

y = median(x, 'omitnan');
