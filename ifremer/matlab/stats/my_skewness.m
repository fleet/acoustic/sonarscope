function s = my_skewness(x,flag)
%SKEWNESS Skewness. 
%   For vectors, SKEWNESS(x) returns the sample my_skewness.  
%   For matrices, SKEWNESS(X)is a row vector containing the sample
%   my_skewness of each column. The my_skewness is the third central 
%   moment divided by the cube of the standard deviation.
%
%   SKEWNESS(X,0) adjusts the my_skewness for bias.  SKEWNESS(X,1) is
%   the same as SKEWNESS(X), and does not adjust for bias.
%
%   See also MEAN, MOMENT, STD, VAR, KURTOSIS.

%   B.A. Jones 2-6-96
%   Copyright 1993-2002 The MathWorks, Inc. 
%   $Revision: 1.9 $  $Date: 2002/01/17 21:31:59 $

[row, col] = size(x);
if row == 1
    x = x(:);
    row = col;
end

if row == 1 && col == 1
    s = NaN;
elseif (nargin >= 2) && isequal(flag, 0)
    s2 = std(x(:), 'omitnan'); % standard deviation
    n = sum(~isnan(x));        % size of each sample
    ok = (n>2) & (s2>0);       % otherwise bias adjustment is undefined
    s = NaN(size(n));          % initialize to NaN
    
    if any(ok)
        s2 = s2(ok);
        n = n(ok);
        x = x(:,ok);
        m = my_nanmean(x);
        m = m(ones(row,1),:);
        s(ok) = (n ./ ((n-1).*(n-2))) .* my_nansum((x-m).^3) ./ (s2.^3);
    end
else
    m = my_nanmean(x);
    m = m(ones(row,1),:);
    m3 = my_nanmean((x - m) .^ 3);
    sm2 = sqrt(my_nanmean((x - m) .^ 2));
    if sm2 == 0
        s = NaN;
    else
        s = m3 ./ sm2 .^ 3;
    end
end

