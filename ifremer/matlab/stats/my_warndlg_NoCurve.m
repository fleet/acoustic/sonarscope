function my_warndlg_NoCurve(this, Tag, varargin)

[varargin, NoMessage] = getPropertyValue(varargin, 'NoMessage', 0); %#ok<ASGLU>

if ~NoMessage
    NomImage = this.Name;
    if isempty(Tag)
        str1 = sprintf('Il n''y a pas de courbe dans cette image : "%s".', NomImage);
        str2 = sprintf('There is no curve in this image : "%s".', NomImage);
    else
        str1 = sprintf('Il n''y a pas de courbe correspondant au crit�re "%s" dans cette image : "%s".', Tag, NomImage);
        str2 = sprintf('There is no curve with criteria "%s" in this image : "%s".', Tag, NomImage);
    end
    my_warndlg(Lang(str1,str2), 1);
end
