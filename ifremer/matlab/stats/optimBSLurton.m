% TODO : refaire � partir de ExClOptimBS (h�ritage)
function bilan = optimBSLurton(bilan, varargin)

[varargin, ComputeType] = getPropertyValue(varargin, 'ComputeType', 2);  % 1=GUI only, 2=background only, 3=background + GUI
[varargin, InitModel]   = getPropertyValue(varargin, 'InitModel',   []); % []
% [varargin, Freq]        = getPropertyValue(varargin, 'Freq',        13);
[varargin, XLim]        = getPropertyValue(varargin, 'XLim',        []); %#ok<ASGLU>

XData   = bilan{1}.x;
YData   = bilan{1}.y';
Weights = bilan{1}.nx';

sub = find((XData >= -80) & (XData <= 80));
XData = XData(sub);
YData = YData(sub);
Weights = Weights(sub);

if size(XData,2) == 1
    XData = XData';
end
if size(YData,2) == 1
    YData = YData';
end
if size(Weights,2) == 1
    Weights = Weights';
end

sub = find(~isnan(XData) & ~isnan(YData) & ~isnan(Weights));
XData = XData(sub);
YData = YData(sub);
Weights = Weights(sub);

coef = tan(abs(XData) * (pi / 180));
Weights = Weights ./ (coef + 1);

maxYData  = max(YData);
minYData  = min(YData);
meanYData = mean(YData);

if isempty(InitModel)
    if isfield(bilan{1}, 'model') && ~isempty(bilan{1}.model)
        valParams    = getParamsValue(bilan{1}.model);
        valminParams = getParamsMinValue(bilan{1}.model);
        valmaxParams = getParamsMaxValue(bilan{1}.model);
    else
        % Modif JMA le 20/11/2018
%         valParams    = [maxYData    6.0 meanYData 2 minYData   20 0];
%         valminParams = [minYData    0.1 minYData  0 minYData   20 0];
%         valmaxParams = [maxYData+3 20.0 maxYData  4 maxYData+3 max(abs(XData))-10 max(abs(XData))];
        valParams    = [maxYData       6.0 meanYData  2   minYData     20 0];
        valminParams = [minYData-30    0.1 minYData   0   minYData-30  20 0];
        valmaxParams = [maxYData+3    20.0 maxYData   4   maxYData+3   max(abs(XData))-10 max(abs(XData))];
    end
    % valParamsBSLC    = [valParams(1:6)    90 2];
    % valminParamsBSLC = [valminParams(1:6) 30 0];
    % valmaxParamsBSLC = [valmaxParams(1:6) 90 8];
else
    valParams    = InitModel.getParamsValue;
    valminParams = InitModel.getParamsMinValue;
    valmaxParams = InitModel.getParamsMaxValue;
    
    % valParamsBSLC    = [valParams(1:6)    90 2];
    % valminParamsBSLC = [valminParams(1:6) 30 0];
    % valmaxParamsBSLC = [valmaxParams(1:6) 90 8];
end

%% Cr�ation de mod�les

%% Create the models

modeleBSLurton     = BSLurtonModel(    'XData', XData, 'YData', YData, 'WeightsData', Weights);
modeleBSJackson    = BSJacksonModel(   'XData', XData, 'YData', YData, 'WeightsData', Weights);
% modeleBSGuillon  = BSGuillonModel(   'XData', XData, 'YData', YData, 'WeightsData', Weights);
modeleBSHamilton   = BSHamiltonModel(  'XData', XData, 'YData', YData, 'WeightsData', Weights);
modeleBSJacksonAPL = BSJacksonAPLModel('XData', XData, 'YData', YData, 'WeightsData', Weights);

modeleBSLurton.setParamsValue(valParams);
modeleBSLurton.setParamsMinValue(valminParams);
modeleBSLurton.setParamsMaxValue(valmaxParams);

%% Create the ClOptim instance

optim = ClOptim('XData', XData, 'YData', YData, ...
    'xLabel', 'Incidence angles', ...
    'yLabel', 'Gain', ...
    'Name', 'Gain');

if ~isempty(XLim)
	XDataFlags = (XData >= XLim(1)) & (XData <= XLim(2));
    modeleBSLurton.XDataFlags = XDataFlags;
end

%% Set the models in the ClOptim instance

% optim.models = [modeleBSLurton, modeleBSJackson, modeleBSHamilton modeleBSJacksonAPL]; % TODO : ne fonctionne pas
optim.set('models', [modeleBSLurton, modeleBSJackson, modeleBSHamilton modeleBSJacksonAPL]);

% editProperties(optim);
% plot(optim);

if (ComputeType == 1) || (ComputeType == 3)
    optim = optimize(optim);
end

%% Create and open the OptimDialog

if (ComputeType == 2) || (ComputeType == 3)
    style1 = ColorAndFontStyle('BackgroundColor', [0.7 0.7 1], 'ForegroundColor', 'k', 'FontWeight', 'bold', 'FontSize', 13, 'FontAngle', 'italic'); %option
    a = OptimUiDialog(optim, 'Title', 'Angular backscatter model fitting', 'titleStyle', style1);
    a.openDialog;
    % a.optim.plot;
    % ParamsValue = a.optim.getParamsValue;
end

%% Get the fitted model

% optim = a.optim % TODO : question pour Pierre : pourquoi on a un acc�s
% direct � optim sans ex�cuter cette fonction ?
bilan{1}.model = optim.models(optim.currentModel);

%% BiaisModel

sumy  = 0;
sumnx = 0;
for k2=1:length(bilan{1})
    y = bilan{1}(k2).y(:);
    nx = bilan{1}(k2).nx(:);
    subOK = find((nx ~= 0) & ~isnan(y));
    sumy  = sumy + sum(y(subOK) .* nx(subOK));
    sumnx = sumnx + sum(nx(subOK));
end
Bias = sumy / sumnx;
bilan{1}.BiaisModel = Bias;

%% Calcul des r�sidus

% yModel = compute(optim, 'x', bilan{1}.x); % Avant
yModel = optim.compute('x', bilan{1}.x);
% figure; plot(bilan{1}.x, bilan{1}.y, '+k'); grid on; hold on;  plot(bilan{1}.x, yModel, 'r');
bilan{1}.residuals = (bilan{1}.y(:) - yModel(:))'; % D�comment� par JMA le 25/08/2015
% bilan{1}.residuals = compute_residuals(bilan{1}.model, 'x', bilan{1}.x, 'y', bilan{1}.y); % Comment� car �a donne la m�me chose que la ligne au dessus
% figure; plot(bilan{1}.x, bilan{1}.residuals, '+k'); grid on;
