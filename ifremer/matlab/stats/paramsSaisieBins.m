function [flag, bins] = paramsSaisieBins(Titre, bins, StatValues, unite, isIntegerVal, varargin)

bins = bins(:);
if isempty(StatValues)
    ValMin = double(min(bins));
    ValMax = double(max(bins));
else
    ValMin = double(StatValues.Min);
    ValMax = double(StatValues.Max);
end

% if numel(bins)
if numel(bins) == 0 % Modif JMA le 19/05/2017
    Pas = 1;
else
    if isempty(bins)
        str1 = sprintf('L''histogramme de l''image "%s" est vide.', Titre);
        str2 = sprintf('The histogram of image "%s" is empty.', Titre);
        my_warndlg(Lang(str1,str2), 0);
        flag = 0;
        return
    end
    Pas = double(bins(2)) - double(bins(1));
%     Pas = mean(diff(double(bins)));
    Pas = round(Pas, 2, 'significant');
end

if isIntegerVal
    Pas = max(1, floor((bins(end) - bins(1)) / 100));
    ValMin = floor(ValMin);
    ValMax = ceil(ValMax);
else
    if strcmp(unite, 'num') || strcmp(unite, '#')
        Pas = 1;
    end
end

if nargin == 6
    HistValues = varargin{1};
    Fig = figure;
    try
        bar(bins, HistValues); grid
    catch %#ok<CTCH>
        my_close(Fig)
    end
end

str1 = sprintf('Param�tres de l''histogramme pour\n%s: ', Titre);
str2 = sprintf('Histogram parameters for\n%s: ', Titre);
p    = ClParametre('Name', 'Min',               'Unit', unite, 'MinValue', ValMin, 'MaxValue', ValMax,        'Value', ValMin);
p(2) = ClParametre('Name', 'Max',               'Unit', unite, 'MinValue', ValMin, 'MaxValue', ValMax,        'Value', ValMax);
p(3) = ClParametre('Name', Lang('Pas', 'Step'), 'Unit', unite, 'MinValue', 0,      'MaxValue', ValMax-ValMin, 'Value', Pas);
a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [0 -1 -1 -2 -1 -1 -1 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    bins = [];
    return
end
value = a.getParamsValue;
Min = value(1);
Max = value(2);
Pas = value(3);

if nargin == 6
    my_close(Fig)
end

if (nargin == 6) && (Pas == 0)
    sub1 = ((bins >= Min) & (bins <= Max));
    MaxHist = max(HistValues(sub1));
    sub2 = find(HistValues > MaxHist/10);
    bins = bins(sub2); %#ok<FNDSB>
else
    bins = Min:Pas:Max;
end

% Ajout� par JMA le 06/03/2015 ! Attention danger
bins = centrage_magnetique(bins);
 