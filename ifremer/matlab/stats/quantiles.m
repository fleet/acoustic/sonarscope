% Quantite de points a gauche de la valeur de fonction de repartition
% de data
%
% Syntax
%   x = quantiles(data, percentage)
% 
% Input Arguments 
%   data       : Vecteur de donnees
%   percentage : Pourcentage(s) 
% 
% Output Arguments 
%   x   : Valeurs de data correspondant aux pourcentages 
%
% Examples
%   Y = speckle(50);
%   histo1D(Y, 100);
%   CLim = quantiles(Y, [3 97])
%
% See also histo1D stats prctile Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function x = quantiles(data, percentage, varargin)

global NUMBER_OF_PROCESSORS %#ok<GVMIS>

if (nargin == 3) && strcmp(varargin{1}, 'AlreadySorted')
else
    data = data(~isnan(data));
    data = sort(data);
end

m = length(data);

q = (0.5:(m-0.5)) * 100 ./ m;
if length(q) < 2
    x = q;
else
    if NUMBER_OF_PROCESSORS == 0
    	x = interp1(q, data, percentage, 'linear', 'extrap'); % Car bug openmp sur windows8
    else
        try
            x = interp1Linear_mex(q, double(data), double(percentage));
        catch %#ok<CTCH>
            str1 = 'Il semble qu''il y ait un pb de "mex function", veuillez basculer en "Software acceleration = No" par le menu "Info / SonarScope / Preferences". Signalez ce probl�me � JMA SVP';
            str2 = 'It seems there is a problem of "mex function", switch to "Software acceleration = No" using menu "Info / SonarScope / Preferences". Please report to JMA.';
            my_warndlg(Lang(str1,str2), 0, 'Tag', 'SoftwareAccelerationBug', 'TimeDelay', 60);
            x = interp1(q, data, percentage, 'linear', 'extrap'); % Car bug openmp sur windows8
        end
    end
end
