function [flag, CurveOffset] = replaceCourbesStats(bilan, Offsets)

NY = bilan{1}(1).NY;
       
nbDim = length(NY);
if nbDim == 1
    bilan{1}.y(:) = -Offsets; % TODO, : Jamais test�
else
    for k=1:(nbDim-1)
        bilan{k}(1).nomZoneEtude = [bilan{k}(1).nomZoneEtude ' - Offsets'];
        for kSegment=1:length(bilan{k})
            if isempty(bilan{k}(kSegment).x)
                continue
            end
            if isempty(Offsets)
                offsetCourbe = 0;
            else
                if length(Offsets) >= kSegment
                    offsetCourbe = -Offsets(kSegment);
                else
                    offsetCourbe = 0;
                end
            end
            bilan{k}(kSegment).y(:) = offsetCourbe;
        end
    end
end

CurveOffset = bilan;

flag = 1;
