% Calcul des parametres statistiques elementaires d'une donnee
%
% Syntax
%   [Val, str] = stats(Y, ...)
%
% Input Arguments
%   Y : matrice des valeurs
%
% Name-Value Pair Arguments
%   Seuil  : Bornes de l'histogramme calcule sur l'histogramme cumule :
%            pourcentage d'energie en dessous des seuils [3 97] par defaut
%   ValNaN : Valeur utilisee pour les valeurs manquantes
%
% Name-only Arguments
%   Complete : Demande de calculer le skewness, le kurtosis et l'entropie
%
% Output Arguments
%   Val : Resulats statistiques dans un tableau :
%         Moyenne Variance Sigma Mediane Min Max Range ...
%   str : Resulats statistiques resumes dans une chaine de caracteres
%
% Remarks : CLim_3&97% sont utiles pour faire un rehaussement de contraste
% d'une image
%
% Examples
%   Y = normrnd(0, 1, [100 100]);
%   histo1D(Y(:), 100);
%   [Val, str] = stats(Y, 'Seuil', [3 95])
%
%   Y = speckle(50);
%   histo1D(Y(:), 100);
%   [Val, str] = stats(Y)
%
% See also histo1D quantiles Authors
% Authors : JMA
%-------------------------------------------------------------------------------


%% New function

function [s, sStr] = stats(x, varargin)

global NUMBER_OF_PROCESSORS %#ok<GVMIS>

[varargin, Unit]     = getPropertyValue(varargin, 'Unit',     []);
[varargin, ValNaN]   = getPropertyValue(varargin, 'ValNaN',   NaN); % Valeur representant les donn�es manquantes
[varargin, Seuil]    = getPropertyValue(varargin, 'Seuil',    []);  % Bornes sur histo cumul normalis� en %
[varargin, Sampling] = getPropertyValue(varargin, 'Sampling', []);  % Pas d'�chantillonnage impos� de la donn�e
[varargin, flagFFT]  = getPropertyValue(varargin, 'flagFFT',  0);   % Pour indiquer si on doit faire le calcul des stats sur log10(abs(Image))
[varargin, DataType] = getPropertyValue(varargin, 'DataType', []);  %#ok<ASGLU>

nbMaxSamplesStats = 2500000 * 2; % Max image 1500x1500, Ce chifre pourrait �tre augment� sur machines puissantes
% x = x(:,:,:,:,:,:,:,:); % modif JMA le 03/09/2019 car bug sur grosse matrice (mosaique M�diterran�e EM122 Charline)
switch ndims(x)
    case {1;2}
        x = x(:,:);
    case 3
        x = x(:,:,:);
    case 4
        x = x(:,:,:,:);
    case 5
        x = x(:,:,:,:,:);
    case 6
        x = x(:,:,:,:,:,:);
    case 7
        x = x(:,:,:,:,:,:,:);
    case 8
        x = x(:,:,:,:,:,:,:,:);
end

qx = [ 0.5   1   3  25  50
      99.5  99  97  75  50];
qx2 = [qx(:); Seuil(:)];

%% Cas des transform�es de Fourier en complex

if flagFFT || ~isreal(x)
    x(x == 0) = NaN;
    x = 20 * log10(abs(x));
    x(isinf(x)) = NaN;
end

%% Convert in single unless it is already in double

x = singleUnlessDouble(x(:), ValNaN);

%% Suppress NaN values

N = numel(x);
%{
% if isnan(ValNaN)
    x(isnan(x)) = [];
% else
%     x(x == ValNaN) = []; % Comment� le 02/04/2021 car singleUnlessDouble remplace les ValNaN par NaN
% end
x(isinf(x)) = [];
%}
x(~isfinite(x)) = []; % Modif JMA le 02/04/2021
% x = x(isfinite(x)); % Modif JMA le 02/04/2021

%% subsampling

if isempty(Sampling)
    Sampling = floor(numel(x) / nbMaxSamplesStats);
    Sampling = max(Sampling, 1);
end

%%

if ~isempty(Unit) && strcmpi(Unit, 'deg')
%     x = exp(1i * x*(pi/360));
    % TODO : passage de la valeur complexe, calcul de stats et renvoi
    % valeur angle ? Max, etc ... ???
end

%% Compute stats

s.NbData = numel(x);
s.nan    = N - s.NbData;
if s.NbData == 0
    s.Max = NaN;
    s.Min = NaN;
else
    s.Max = max(x);
    s.Min = min(x);
end
s.Range = s.Max - s.Min;
s.Moyenne    = mean(x); % TODO : remplacer par Mean

%% Subsampling to compute the quantiles : avoids sorting large matrices

if Sampling ~= 1
    sub = floor(1:Sampling:s.NbData);
    x = x(sub);
    clear sub
end

%% Calcul des autres moments

xs           = x - s.Moyenne; % subtract mean
xss          = xs .* xs;
s.Variance   = mean(xss); 
s.Sigma      = sqrt(s.Variance); % TODO : remplacer par Std
s.Skewness   = mean(xss .* xs) ./ (s.Variance .^ 1.5);
s.Kurtosis   = mean(xss .*xss) ./ (s.Variance .^ 2); 
s.PercentNaN = (s.nan / s.NbData) * 100;
s.Sampling   = Sampling;

N = numel(x);

xs = sort(x);

qx2 = qx2 / 100;

s.q1      = NaN;
s.Mediane = NaN;
s.q3      = NaN;

if s.NbData > 3
    q = [0, (0.5:(N-0.5))/N, 1]';
    xx = [s.Min; xs; s.Max];
    if NUMBER_OF_PROCESSORS == 0
        xq = interp1(q, xx, qx2, 'linear', 'extrap'); % Car bug openmp sur windows8
    else
        try
            xq = interp1Linear_mex(q, double(xx), qx2); % fast interp
        catch
            xq = interp1(q, xx, qx2, 'linear', 'extrap'); % Car bug openmp sur windows8
        end
    end
    
    s.Mediane = xq(9);
    s.Quant_25_75 = xq(1:2)'; % 0.5%, 99.5% en r�alit�
    s.Quant_01_99 = xq(3:4)';
    s.Quant_03_97 = xq(5:6)';
    
%     s.Entropy = entropy(uint8(quantify(x, 'rangeIn', s.Quant_01_99)));
    subEntropy = floor(1: max(numel(x) / 10000, 1):length(x)); % Modif JMA le 29/06/2020 car Entropy prend beaucoup de temps
    s.Entropy = entropy(uint8(quantify(x(subEntropy), 'rangeIn', s.Quant_01_99)));
    if numel(qx2) > 10
        s.Quant_x = xq(11:end)';
    end
else
    s.Mediane = NaN;
    s.Quant_25_75 = [NaN NaN]; % 0.5%, 99.5% en r�alit�
    s.Quant_01_99 = [NaN NaN];
    s.Quant_03_97 = [NaN NaN];
    s.Entropy = NaN;
    if numel(qx2) > 10
        s.Quant_x = NaN(1,length(qx2)-10);
    end
end

sStr = stats2str(s, 'DataType', DataType);
