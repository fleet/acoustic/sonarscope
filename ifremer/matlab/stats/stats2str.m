function str = stats2str(Val, varargin)

[varargin, DataType] = getPropertyValue(varargin, 'DataType', []); %#ok<ASGLU>

num2strFunc = @num2str;
if ~isempty(DataType)
    switch DataType
        case cl_image.indDataType('RxTime')
            num2strFunc = @num2strDatetime;
    end
end

if isempty(Val)
    str = '';
else
    str = cell(16,1);
    str{1}  = ['Mean          : '  num2strFunc(Val.Moyenne)];
    str{2}  = ['std           : '  num2strFunc(Val.Sigma)];
    str{3}  = ['Median        : '  num2strFunc(Val.Mediane)];
    str{4}  = ['Min           : '  num2strFunc(Val.Min)];
    str{5}  = ['Max           : '  num2strFunc(Val.Max)];
    str{6}  = ['Range         : '  num2strFunc(Val.Range)];
    str{7}  = ['Quantile  1%  : [' num2strFunc(Val.Quant_01_99) ']'];
    str{8}  = ['Quantile  3%  : [' num2strFunc(Val.Quant_03_97) ']'];
    str{9}  = ['Quantile 0.5% : [' num2strFunc(Val.Quant_25_75) ']'];
    str{10} = ['PercentNaN    : '  num2strFunc(Val.PercentNaN) ' %'];
    str{11} = ['Sampling      : '  num2strFunc(Val.Sampling)];
    if isfield(Val, 'NbData')
        str{12} = ['NbData        : ' num2strFunc(Val.NbData)];
    else
        str{12} = 'NbData        : New feature' ;
    end
    str{13} = ['Variance      : ' num2strFunc(Val.Variance)];
    str{14} = ['Skewness      : ' num2strFunc(Val.Skewness)];
    str{15} = ['Kurtosis      : ' num2strFunc(Val.Kurtosis)];
    str{16} = ['Entropy       : ' num2strFunc(Val.Entropy)];
    if isfield(Val, 'Quant_x')
        str{17} = ['Quant_x       : ' num2strCode(Val.Quant_x)];
    end
    
    str = cell2str(str);
end


function str = num2strDatetime(t)

if ischar(t)
    str = t;
else
    str = char(datetime(t(1), 'ConvertFrom', 'datenum', 'Format', 'yyyy-MM-dd HH:mm.ss.SSS'));
    for k=2:length(t)
        str = [str '    ' char(datetime(t(k), 'ConvertFrom', 'datenum', 'Format', 'yyyy-MM-dd HH:mm.ss.SSS'))]; %#ok<AGROW>
    end
end
