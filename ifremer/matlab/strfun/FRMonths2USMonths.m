% Remplacement des caract�res accentu�s
%
% Syntax
%   strUS = FRMonths2USMonths(strFR)
%
% Input Arguments
%   x : Description du parametre (unite)
%
% Output Arguments
%   S : Cha�ne de caract�re d�accentu�e
%
% Remarks : Remarques Utile pour traiter les dates du type : '09-ao�-2005 10:39:49'
%
% Examples
%   nomFic = getNomFicDatabase('EM12D_PRISMED_500m.mnt')
%   S = dir(nomFic)
%   datenum(S.date)
%   S.date = FRMonths2USMonths(S.date)
%   datenum(S.date)
%
% See also dir Authors
% Authors : JMA 
% VERSION    : $Id: entete.m,v 1.2 2003/11/12 12:46:11 augustin Exp $
%-------------------------------------------------------------------------------

function str = FRMonths2USMonths(str)

monthsUS = {'Jan';'Feb';'Mar';'Apr';'May';'Jun';'Jul';'Aug';'Sep';'Oct';'Nov';'Dec'};

monthsFR = {'Janvier';'F�vrier';'Mars';'Avril';'Mai';'Juin';'Juillet';'Ao�t';'Septembre';'Octobre';'Novembre';'D�cembre'};
for i=1:12
    sub = strfind(str, monthsFR{i});
    if ~isempty(sub)
        n = length(monthsFR{i});
        str(sub:sub+n-1) = monthsUS{i};
    end
end

monthsFR = {'Jan';'F�v';'Mar';'Avr';'Mai';'Jui';'Jui';'Ao�';'Sep';'Oct';'Nov';'D�c'};
for i=1:12
    sub = strfind(str, monthsFR{i});
    if ~isempty(sub)
        str(sub:sub+2) = monthsUS{i};
    end
    sub = strfind(lower(str), lower(monthsFR{i}));
    if ~isempty(sub)
        str(sub:sub+2) = monthsUS{i};
    end
end
