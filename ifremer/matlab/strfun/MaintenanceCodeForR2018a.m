% nomDir = 'D:\SScTbx\ifremer';
% listeFicIn = listeFicOnDepthDir(nomDir, '.m');
% MaintenanceCodeForR2018a(listeFicIn)

function MaintenanceCodeForR2018a(listeFicIn)

nbGetFlag          = 0;
nbGetPropertyValue = 0;

N = length(listeFicIn);
hw = create_waitbar('Processing', 'N', N);
for k=1:N
    nomFic = listeFicIn{k};
    my_waitbar(k, N, hw)
    strLinesIn = readMFile(nomFic);
    
    %% Exceptions
    
    if isempty(strLinesIn)
        nomFic %#ok<*NOPRT>
        continue
    end
    [~, nomFicSeul] = fileparts(nomFic);
    if strcmp(nomFicSeul, 'MaintenanceCodeForR2018a')
        continue
    end
    if strcmp(nomFicSeul, 'getFlagOld')
        continue
    end
    if strcmp(nomFicSeul, 'getPropertyValueOld')
        continue
    end
    
    %% Traitement des getFlagOld
    
    [flag, strLinesOut, nbGetFlag] = processGetFlag(strLinesIn, nomFic, nbGetFlag);
    if flag
        flag = writeMFile(nomFic, strLinesOut);
        if ~flag
            strLinesIn
            strLinesOut
        end
    end
    
    %% Traitement des getPropertyValueOld
    
    [flag, strLinesOut, nbGetPropertyValue] = processGetPropertyValue(strLinesIn, nomFic, nbGetPropertyValue);
    if flag
        flag = writeMFile(nomFic, strLinesOut);
        if ~flag
            strLinesIn
            strLinesOut
        end
    end
end
my_close(hw, 'MsgEnd');


function [flag, strLinesOut, nbGetPropertyValue] = processGetPropertyValue(strLinesIn, nomFic, nbGetPropertyValue)
flag = 0;
for k=1:length(strLinesIn)
    strIn = strLinesIn{k};
    strIn = strtrim(strIn);
           
    if strcmp(strIn, 'descriptorsFileName = getPropertyValueOld(varargin, ''parameterFileName'', fullfile(my_tempdir, ''Descriptors.txt''));')
        strIn
    end
 
    if contains(strIn, 'getPropertyValueOld')
        mots = split(strIn, {'='; '('; ','; ')'});
        mots = replace(mots, ' ', '');
        if (length(mots) == 6) && strcmp(mots{2}, 'getPropertyValueOld')
            if strcmp(mots{1}(1), '%')
                flag = 1;
                mots{1} = mots{1}(2:end);
                strOut = sprintf('%% [%s, %s] = getPropertyValue(%s, %s, %s);', mots{3}, mots{1}, mots{3}, mots{4}, mots{5});
                strLinesOut{k,1} = strOut; %#ok<AGROW>
                
                nbGetPropertyValue = nbGetPropertyValue + 1;
                fprintf('\n%4d : %s\nIn  : %s\nOut : %s\n', nbGetPropertyValue, nomFic, strIn, strOut);
            else
                flag = 1;
                strOut = sprintf('[%s, %s] = getPropertyValue(%s, %s, %s);', mots{3}, mots{1}, mots{3}, mots{4}, mots{5});
                strLinesOut{k,1} = strOut; %#ok<AGROW>
                
                nbGetPropertyValue = nbGetPropertyValue + 1;
                fprintf('\n%4d : %s\nIn  : %s\nOut : %s\n', nbGetPropertyValue, nomFic, strIn, strOut);
            end
            
        elseif (length(mots) == 6) && strcmp(mots{3}, 'getPropertyValue')
            strLinesOut{k,1} = strIn; %#ok<AGROW>
            
        elseif (length(mots) >= 6) && strcmp(mots{2}, 'getPropertyValueOld')
                ind = strfind(strIn, mots{5});
                finPhrase = strIn(ind:end);
             if strcmp(mots{1}(1), '%')
                flag = 1;
                mots{1} = mots{1}(2:end);
                strOut = sprintf('%% [%s, %s] = getPropertyValue(%s, %s, %s', mots{3}, mots{1}, mots{3}, mots{4}, finPhrase);
                strLinesOut{k,1} = strOut; %#ok<AGROW>
                
                nbGetPropertyValue = nbGetPropertyValue + 1;
                fprintf('\n%4d : %s\nIn  : %s\nOut : %s\n', nbGetPropertyValue, nomFic, strIn, strOut);
            else
                flag = 1;
                strOut = sprintf('[%s, %s] = getPropertyValue(%s, %s, %s', mots{3}, mots{1}, mots{3}, mots{4}, finPhrase);
                strLinesOut{k,1} = strOut; %#ok<AGROW>
                
                nbGetPropertyValue = nbGetPropertyValue + 1;
                fprintf('\n%4d : %s\nIn  : %s\nOut : %s\n', nbGetPropertyValue, nomFic, strIn, strOut);
            end
           
        else
            strLinesOut{k,1} = strIn; %#ok<AGROW>
        end
    else
        strLinesOut{k,1} = strIn; %#ok<AGROW>
    end
end

function [flag, strLinesOut, nbGetFlag] = processGetFlag(strLinesIn, nomFic, nbGetFlag)
flag = 0;
for k=1:length(strLinesIn)
    strIn = strLinesIn{k};
    strIn = strtrim(strIn);
    
%     if strcmp(strIn, 'NoInit = getFlagOld(varargin, ''NoInit'');')
%         strIn
%     end
    
    if contains(strIn, 'getFlagOld')
        mots = split(strIn, {'='; '('; ','; ')'});
        mots = strrep(mots, ' ', '');
        if (length(mots) == 5) && strcmp(mots{2}, 'getFlagOld')
            flag = 1;
            strOut = sprintf('[%s, %s] = getFlag(%s, %s);', mots{3}, mots{1}, mots{3}, mots{4});
            strLinesOut{k,1} = strOut; %#ok<AGROW>
            
            nbGetFlag = nbGetFlag + 1;
            fprintf('\n%4d : %s\nIn  : %s\nOut : %s\n', nbGetFlag, nomFic, strIn, strOut);
        elseif (length(mots) == 6) && strcmp(mots{3}, 'getFlag')
            strLinesOut{k,1} = strIn; %#ok<AGROW>
        else
            strLinesOut{k,1} = strIn; %#ok<AGROW>
        end
    else
        strLinesOut{k,1} = strIn; %#ok<AGROW>
    end
end


function str = readMFile(nomFic)
str = {};
fid = fopen(nomFic);
while 1
    tline = fgetl(fid);
    if ~ischar(tline)
        break
    end
    str{end+1,1} = tline; %#ok<AGROW>
end
fclose(fid);


function flag = writeMFile(nomFic, strLinesOut)
fid = fopen(nomFic, 'w+');
if fid == 1
    flag = 0;
    return
end
for k=1:length(strLinesOut)
    fprintf(fid, '%s\n', strLinesOut{k});
end
fclose(fid);
flag = 1;
