% nomDir = 'D:\SScTbx\ifremer';
% listeFicIn = listeFicOnDepthDir(nomDir, '.m');
% MaintenanceCodeForR2018a_Check(listeFicIn)

function MaintenanceCodeForR2018a_Check(listeFicIn)

nbGetPropertyValue = 0;

N = length(listeFicIn);
hw = create_waitbar('Processing', 'N', N);
for k=1:N
    nomFic = listeFicIn{k};
    my_waitbar(k, N, hw)
    strLinesIn = readMFile(nomFic);
    
    %% Exceptions
    
    if isempty(strLinesIn)
        nomFic %#ok<NOPRT>
        continue
    end
    [~, nomFicSeul] = fileparts(nomFic);
    if strcmp(nomFicSeul, 'MaintenanceCodeForR2018a')
        continue
    end
    if strcmp(nomFicSeul, 'MaintenanceCodeForR2018a_Check')
        continue
    end
    if strcmp(nomFicSeul, 'getFlag')
        continue
    end
    if strcmp(nomFicSeul, 'getPropertyValue')
        continue
    end
    
    %% Traitement des getPropertyValue
    
    nbGetPropertyValue = processGetPropertyValue(strLinesIn, nomFic, nbGetPropertyValue);
end
my_close(hw, 'MsgEnd');


function nbGetPropertyValue = processGetPropertyValue(strLinesIn, nomFic, nbGetPropertyValue)
for k=1:length(strLinesIn)
    strIn = strLinesIn{k};
    strIn = strtrim(strIn);
 
    ind = strfind(strIn, 'getPropertyValue');
    if contains(strIn, 'getPropertyValue')
        if contains(strIn(ind:end), '[]')
            continue
        end
        entreCrochets = extractBetween(strIn(ind:end), '[', ']');
        mots = split(entreCrochets, {','; ' '});
        if length(mots) > 1
            strIn %#ok<NOPRT>
            continue
        end
        if contains(strIn(ind:end), '[')
            edit(nomFic)
        end
    end
end



function str = readMFile(nomFic)
str = {};
fid = fopen(nomFic);
while 1
    tline = fgetl(fid);
    if ~ischar(tline)
        break
    end
    str{end+1,1} = tline; %#ok<AGROW>
end
fclose(fid);
