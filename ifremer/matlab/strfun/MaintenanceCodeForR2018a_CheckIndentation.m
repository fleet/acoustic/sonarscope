% nomDir = 'D:\SScTbx\ifremer';
% listeFicIn = listeFicOnDepthDir(nomDir, '.m');
% MaintenanceCodeForR2018a_CheckIndentation(listeFicIn)

function MaintenanceCodeForR2018a_CheckIndentation(listeFicIn)

nbGetPropertyValue = 0;

N = length(listeFicIn);
%{
for k=N:-1:1
    nomFic = listeFicIn{k};
    if contains(nomFic, 'pames') && ~contains(nomFic, 'D:\SScTbx\ifremer\matlab\src')
        kDeb = k;
        break
    end
end
%}
kDeb = 6561; % 6710

hw = create_waitbar('Processing', 'N', N);
for k=kDeb:N
    nomFic = listeFicIn{k};
    my_waitbar(k, N, hw)
 
    strLinesIn = readMFile(nomFic);
    
    %% Exceptions
    
    if isempty(strLinesIn)
        nomFic %#ok<NOPRT>
        continue
    end
    [~, nomFicSeul] = fileparts(nomFic);
    if strcmp(nomFicSeul, 'MaintenanceCodeForR2018a')
        continue
    end
    if strcmp(nomFicSeul, 'MaintenanceCodeForR2018a_Check')
        continue
    end
    if strcmp(nomFicSeul, 'MaintenanceCodeForR2018a_CheckIndentation')
        continue
    end
    if strcmp(nomFicSeul, 'getFlag')
        continue
    end
    if strcmp(nomFicSeul, 'getPropertyValue')
        continue
    end
    
    %% Traitement des getPropertyValue
    
    nbGetPropertyValue = processGetPropertyValue(strLinesIn, nomFic, nbGetPropertyValue, k, N);
end
my_close(hw, 'MsgEnd')


function nbGetPropertyValue = processGetPropertyValue(strLinesIn, nomFic, nbGetPropertyValue, kFile, N)
flagIdente = 0;
for k=1:length(strLinesIn)
    strIn = strLinesIn{k};
    
    if isempty(strIn)
        continue
    end
    
    if strcmp(strIn(1), ' ')
        flagIdente = 1;
    end
end
if ~flagIdente
    fprintf('%d / %d : %s\n', kFile, N, nomFic);
    
    h = matlab.desktop.editor.openDocument(nomFic);
    h.smartIndentContents
    h.save
    h.close
    
    edit(nomFic)
    
    pause
    h = matlab.desktop.editor.openDocument(nomFic);
    h.close
end


function str = readMFile(nomFic)
str = {};
fid = fopen(nomFic);
while 1
    tline = fgetl(fid);
    if ~ischar(tline)
        break
    end
    str{end+1,1} = tline; %#ok<AGROW>
end
fclose(fid);
