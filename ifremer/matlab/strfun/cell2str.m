% Creation d'une matrice de caracteres � partir d'une liste de cellules
% contenant des strings (utile dans les methodes char des classes).
%
% Syntax
%   str = cell2str(c)
%
% Input Arguments
%   c   : Tableau de cellules contenant des chaines de caracteres
%
% Output Arguments
%   str : Matrice de caracteres
%
% Examples
%   c = {'a';  'ddd';  't'}
%   str = cell2str(c)
%   whos str
%
% See also Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function str = cell2str(c)

N = 0;
M = 0;
for i=1:length(c)
    ci = c{i};
    if ~ischar(ci) % TODO : GLU : Rajout� par JMA le 11/11/2012
        ci = num2str(ci);
    end
    [n,m] = size(ci);
    N = N + n;
    M = max(M, m);
end

str = repmat(' ', N, M);
ind = 1;
for i=1:length(c)
    ci = c{i};
    if ~ischar(ci) % TODO : GLU : Rajout� par JMA le 11/11/2012
        ci = num2str(ci);
    end
    [n,m] = size(ci);
    for k=1:n
        str(ind,1:m) = ci(k,:);
        ind = ind + 1;
    end
end
% str = char(str);