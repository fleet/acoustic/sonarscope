% Reconstitution d'une chaine de caractere a partir d'un tableau de cellule 
% de caracteres.
%
% Syntax
%   cellstr2str( c , separateur )
%
% Input Arguments
%   c   : Tableau de cellules contenant des chaines de caracteres
%
% Output Arguments
%   str : chaine de caracteres
%
% Examples
%   c = {'a';  'ddd';  't'}
%   cellstr2str( c , ' ')
%
% See also Authors
% Authors : PP
%--------------------------------------------------------------------------------

function str = cellstr2str( c , separateur)

retour_ligne = newline;
n = length(c);

str = '' ;

deblank(c);

if n == 0
    str = '';
else
    for j = 1:n
        strc = c{j};
        if strc(end) == retour_ligne
            str = sprintf('%s%s', str, c{j});
        else
            str = sprintf('%s%s%s', str, c{j}, separateur);
        end
    end
end

deblank(str);
