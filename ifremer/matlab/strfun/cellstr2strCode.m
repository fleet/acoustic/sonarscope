% Codage sous forme de commande matlab d'une liste de chaines de caracteres
%
% Syntax
%   cellstr2strCode( c )
%
% Input Arguments
%   c   : Tableau de cellules contenant des chaines de caracteres
%
% Output Arguments
%   str : chaine de caracteres interpretable par Matlab
%
% Examples
%   c = {'a';  'ddd';  't'}
%   cellstr2strCode( c )
%
% See also Authors
% Authors : JMA
% VERSION  : $Id: cellstr2strCode.m,v 1.5 2002/06/06 12:21:48 augustin Exp $
%--------------------------------------------------------------------------------

function str = cellstr2strCode( c )

if iscell(c)
    n = length(c);
    str = sprintf('{');
    for j = 1:n
        str = sprintf('%s''%s''', str, c{j});
        if j < n
            str = sprintf('%s;', str);
        end
    end
    str = sprintf('%s}', str);
elseif ischar(c)
    str = sprintf('{ ''%s'' }', c);
else
    str = '';
end
