% Reconstitution d'une chaine de caractere a partir d'un tableau de cellule 
% de caracteres.
%
% Syntax
%   cellstr2str( c , separateur )
%
% Input Arguments
%   c   : Tableau de cellules contenant des chaines de caracteres
%
% Output Arguments
%   str : chaine de caracteres
%
% Examples
%   c = {'a';  'ddd';  't'}
%   cellstr2str( c , ' ')
%
% See also Authors
% Authors : PP
% VERSION  : $Id: cellstrerase.m,v 1.1 2002/06/06 12:21:48 augustin Exp $
%--------------------------------------------------------------------------------

function str = cellstrerase(c , separateur)

n = length(c);
str = '' ;

if n == 0
    str = '';
else
    for j = 1:n
        str = sprintf('%s%s%s', str, c{j}, separateur);
    end
end
