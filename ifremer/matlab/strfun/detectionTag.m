function [Tag, pos] = detectionTag(str1, str2)

Tag = [];
pos = [];

if strcmp(str1,str2)
    return
end

n1 = length(str1);
n2 = length(str2);
n = min(n1,n2);
for k1=1:n1
    if ~strcmp(str1(k1), str2(k1))
        pos = k1-1;
        sub1 = k1:n;
        for k2=1:(n-k1)
            if (sub1(end)+k2) > n2
                return
            else
                if strcmp(str1(sub1), str2(sub1+k2))
                    Tag = str2((k1-1):(k1+k2-2));
                    return
                end
            end
        end
    end
end
