% Check if a Property flag is in the input variable list
%
% Syntax
%   PV = getFlag(Liste, 'PN')
%
% Remarks
%   - It eliminates the variable of the list if it was found
%   - The compareasoin in made independently of the case.
%
% Input Arguments
%   Liste  : List of variables
%   PN : Property name to be checked
%
% Output Arguments
%   PV : true if the property name was found, false if not
%
% Examples
%   Liste = {'Base0', 'Ping', 1:10, 'Weight', 'Angle', [30 80]};
%   [Liste, isBase0]  = getFlag(Liste, 'Base0') 
%   [Liste, isWeight] = getFlag(Liste, 'Weight') 
%   [Liste, isPing]   = getFlag(Liste, 'Ping') 
%   [Liste, isPong]   = getFlag(Liste, 'Pong') 
%
% See also getPropertyValue Authors
% Authors : DCF
%------------------------------------------------------------------------------

function [Liste, PV] = getFlag(Liste, PN)

lengthPN = size (PN, 2);
lengthV  = size (Liste, 2);
PV       = 0;

%% Parcours la liste a la recherche du nom du flag. Le nom peut etre partiel

for indice = 1:lengthV
    lengthV = size (Liste{indice},2);
    if lengthV == lengthPN
        if strcmpi(Liste{indice}, PN)
            PV            = 1;
            Liste(indice) = [];
            break
        end
    elseif lengthV > lengthPN
        if strcmpi(Liste{indice}(1:lengthPN), PN)
            PV            = 1;
            Liste(indice) = [];
            break
        end
    end
end
