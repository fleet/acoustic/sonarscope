% Lecture d'une propertyValue associ�e � un propertyName  
%
% Syntax
%   [Liste, PropertyValue] = getPropertyValue(Liste, 'PropertyName', DefaultValue)
%
% Remarks
%   Elimine le couple PropertyName/PropertyValue de la liste
%
%   En cas de probleme on retourne une valeur vide [].
%
%   La comparaison est faite sur des chaines de caracteres sans tenir compte 
%   des majuscules.
%
% Input Arguments
%   Liste        : Liste � traiter
%   PropertyName : Property name � rechercher
%   DefaultValue : valeur par defaut si recherche infructueuse
%
% Output Arguments
%   Liste         : Liste remise � jour
%   PropertyValue : valeur associee a PropertyName
%
% Examples
%   Liste = {'Base0', 'Ping', 1:10, 'Weight', 'Angle', [30 80]};
%   [Liste, ListePing] = getPropertyValue(Liste, 'Ping', 1:20) 
%   [Liste, ListePing] = getPropertyValue(Liste, 'Ping', 1:20) 
%   [Liste, DispBase0] = getPropertyValue(Liste, 'Base0', 'none')
%   [Liste, DispBase1] = getPropertyValue(Liste, 'Base1', 'Flag')
%   [Liste, DispAngle] = getPropertyValue(Liste, 'Angle', [])
%
%   Liste = {};
%   [Liste, ListePing] = getPropertyValue(Liste, 'Ping', 1:20) 
%   [Liste, DispBase0] = getPropertyValue(Liste, 'Base0', 'Flag')
%
% See also getFlag Authors
% Authors : DCF
%------------------------------------------------------------------------------

function [Liste, PropertyValue] = getPropertyValue(Liste, PropertyName, DefaultValue)

if isempty(Liste)
    PropertyValue = DefaultValue;
else
    ind = find(strcmpi(PropertyName, Liste), 1, 'first');
    if isempty(ind)
        PropertyValue = DefaultValue;
    else
        PropertyValue = Liste{ind+1};
        Liste(ind:ind+1) = [];
    end
end
