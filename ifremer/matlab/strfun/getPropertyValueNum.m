% Lecture avec controle d'une propertyValue numerique associee a un propertyName
%
% Syntax
%   PropertyValue = getPropertyValueNum(V, 'PropertyName', ValInit, valMin, valMax)
%
% Remarks
%   Elimine le couple PropertyName/PropertyValue de la liste
%
%   En cas de probleme on retourne une valeur vide [].
%
%   La comparaison est faite sur des chaines de caracteres sans tenir compte 
%   des majuscules.
%
% Input Arguments
%   V      : Liste a traiter
%   PropertyName     : Property name a rechercher
%   ValInit     : valeur par defaut si recherche infructueuse
%   valMin : Valeur min de la donnee
%   valMax : Valeur max de la donnee
%
% Output Arguments
%   PropertyValue : valeur associee a PropertyName
%
% Examples
%   V = {'Ping', 1:10, 'Angle', [30 80]};
%   [V, V, ListePing] = getPropertyValueNum(V, 'Ping', 1:20) 
%   [V, DispAngle] = getPropertyValueNum(V, 'Angle', [])
%
% See also getPropertyValue getFlag getPropertyValueNum Authors
% Authors : JMA
%------------------------------------------------------------------------------

function [listeParams, PropertyValue] = getPropertyValueNum(listeParams, PropertyName, ValInit, valMin, valMax)

nbParams = size(listeParams, 2);
PropertyValue = ValInit;

% ----------------------------------------------------------------------------
% Parcours de couples (PropertyName, PropertyValue). Recherche identification PropertyName
% - PropertyName est plus long que le nom de propriete courant : solution eliminee
% - PropertyName est de longueur moindre ou egale au nom de propriete courant : test
%   l'equivalence

for indice = 1:nbParams
    if ischar(listeParams{indice})
        if strcmpi(listeParams{indice}, PropertyName)
            PropertyValue = listeParams{indice+1};
            if isnumeric(PropertyValue)
                listeParams(indice:indice+1) = [];
                if (PropertyValue < valMin) || (PropertyValue > valMax)
                    PropertyValue = ValInit;
                    str = sprintf('La PropertyValue associee a  %s doit etre comprise dans [%s,%s]', ...
                        PropertyName, num2str(valMin), num2str(valMax));
                    my_warndlg(['strfun/getpropertyValueNum', str], 0);
                end
            else
                str = sprintf('La PropertyValue associee a %s doit etre une valeur numerique', PropertyName);
                my_warndlg(['strfun/getpropertyValueNum : ' str], 1);
            end
            break
        end
    end
end
