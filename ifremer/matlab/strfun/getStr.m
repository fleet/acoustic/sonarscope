% Codage des valeurs de un ou plusieurs vecteurs
%
% Syntax
%   s = getStr(A, B, ...)
%
% Input Arguments
%	A, B, ... : Vecteurs
%
% Output Arguments
%	s : chaine de caracteres
%
% Examples 
%   s = getStr( rand(1,5) )
%   s = getStr( rand(1,5), rand(1,5) )
%   s = getStr( rand(1,5), rand(1,5), rand(1,5), rand(1,5) )
%
% See also Authors
% Authors : JMA
% VERSION  : $Id: getStr.m,v 1.5 2002/06/06 12:21:48 augustin Exp $
%--------------------------------------------------------------------------------

function s = getStr(varargin)

if nargin == 0
        return
end

s = cell(length(varargin{1}), 1);
for i = 1:length(varargin)
	x = varargin{i};
	for k = 1:length(x)
		if i == 1
			s{k} = sprintf('%f', x(k));
		else
			s{k} = sprintf('%s\t%f', s{k}, x(k));
		end
	end
end
