% Codage abrege d'une liste de nombres. 
% Si la serie est une suite arithmetique, la sortie correspond au codage matlab de cette serie 
%
% Syntax
%   s = num2strMoney(x)
%
% Input Arguments
%	x : suite de nombres
%
% Output Arguments
%	s : code retourne permetant de regenerer la serie de nombres
%
% Examples
%   s = num2strMoney(pi*1e9)
%   s = num2strMoney([1234567890 987654321 89061025 890610251 8906102512 89061025123 1 11 111 1111])
%   whos s
%   s = num2strMoney([1 2 4 8 10])
%   whos s
%
% See also Authors 
% Authors : JMA
%--------------------------------------------------------------------------------

function str = num2strMoney(x)

str = repmat(' ', numel(x), 1);
if isequal(x, floor(x))
    nx = numel(x);
    for k=1:nx
        s = sprintf('%d ', x(k));
        s = fliplr(s);
        s = money(s);
        str(k,1:length(s)) = s;
    end

    str = fliplr(str);

else
    for k=1:numel(x)
        s = num2strPrecis(x(k));
        sub = 1:length(s);
        str(k,sub) = s;
    end
end

function str = money(str)
n = ceil(length(str) / 3) - 1;
for k=1:n
    m = k * 4;
%     if m <= length(str)
        str = [str(:,1:m) ' ' str(:,(m+1):end)];
%     end
end
str = strtrim(str);
