% Suppression de blancs superflus dans une phrase
%
% Syntax 
%   mots = rmblank(phrase, ...)
%
% Input Arguments
%   phrase : string contenant des mots
%
% Name-Value Pair Arguments 
%   nbBlancs : Nombre de blancs laiss�s entre chaque mot
%
% Output Arguments
%	mots :  mots separes
%
% Examples 
%   sOut = rmblank('  Ceci est    un exemple  de   phrase remplie     de trous     ')
%   sOut = rmblank('  Ceci est    un exemple  de   phrase remplie     de trous     ', 0)
%
% See also strtok LecRevRta Authors
% Authors : JMA
%--------------------------------------------------------------------------

function sOut = rmblank(phrase, varargin)

switch nargin
    case 1
        % Remplacement de(s) espace(s) par un seul dans le corps de la phrase.
        sOut = regexprep(phrase, '\s*', ' ');
        % Suppression de(s) espace(s) de d�but et fin de phrase (c'est le fonctionnement attendu).
        sOut = regexprep(sOut, '^\s*|\s*$', '');
    case 2
        replace = repmat(' ', 1, varargin{1});
        sOut    = regexprep(phrase, '^\s*|\s*$|\s*', replace);
    otherwise
        return
end
