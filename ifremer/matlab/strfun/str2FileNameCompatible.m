function FileName = str2FileNameCompatible(FileName)

FileName = strrep(FileName, '/', 'On');
FileName = strrep(FileName, ' ', '_');
FileName = strrep(FileName, '(', '');
FileName = strrep(FileName, ')', '');
FileName = strrep(FileName, '_-_', '-');
if strcmp(FileName(1), '-') || strcmp(FileName(1), '_')
    FileName(1) = [];
end
FileName = strrep(FileName, '.all', '');
FileName = strrep(FileName, '_/_', '_');
FileName = strrep(FileName, '_:_', '_');
FileName = strrep(FileName, '[', '_');
FileName = strrep(FileName, '[', '_');
FileName = strrep(FileName, ']', '_');
FileName = strrep(FileName, '%', '');
FileName = strrep(FileName, ':', '_');
FileName = strrep(FileName, '__', '_');
if strcmp(FileName(end), '-') || strcmp(FileName(end), '_')
    FileName(end) = [];
end
