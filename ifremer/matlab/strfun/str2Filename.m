% Exemple :
%   s = sprintf('Fer � cheval o�\tl''on peut voir tant�t_20clous')
%   clean_s = str2Filename(s)

function clean_s = str2Filename(s)
    clean_s = removediacritics(s);
    idx = regexp([' ' clean_s],'(?<=\s+)\S','start')-1;
    clean_s(idx) = upper(clean_s(idx));
    clean_s = regexprep(clean_s, (' |_|\t|'''), '');


function clean_s = removediacritics(s)

%%
% https://stackoverflow.com/questions/12771592/replace-special-characters-by-equivalent
%REMOVEDIACRITICS Removes diacritics from text.
%   This function removes many common diacritics from strings, such as
%     � - the acute accent
%     � - the grave accent
%     � - the circumflex accent
%     � - the diaeresis, or trema, or umlaut
%     � - the tilde
%     � - the cedilla
%     � - the ring, or bolle
%     � - the slash, or solidus, or virgule

% uppercase
s = regexprep(s,'(?:�|�|�|�|�|�)','A');
s = regexprep(s,'(?:�)','AE');
s = regexprep(s,'(?:�)','ss');
s = regexprep(s,'(?:�)','C');
s = regexprep(s,'(?:�)','D');
s = regexprep(s,'(?:�|�|�|�)','E');
s = regexprep(s,'(?:�|�|�|�)','I');
s = regexprep(s,'(?:�)','N');
s = regexprep(s,'(?:�|�|�|�|�|�)','O');
s = regexprep(s,'(?:�)','OE');
s = regexprep(s,'(?:�|�|�|�)','U');
s = regexprep(s,'(?:�|�)','Y');

% lowercase
s = regexprep(s,'(?:�|�|�|�|�|�)','a');
s = regexprep(s,'(?:�)','ae');
s = regexprep(s,'(?:�)','c');
s = regexprep(s,'(?:�)','d');
s = regexprep(s,'(?:�|�|�|�)','e');
s = regexprep(s,'(?:�|�|�|�)','i');
s = regexprep(s,'(?:�)','n');
s = regexprep(s,'(?:�|�|�|�|�|�)','o');
s = regexprep(s,'(?:�)','oe');
s = regexprep(s,'(?:�|�|�|�)','u');
s = regexprep(s,'(?:�|�)','y');

clean_s = s;
