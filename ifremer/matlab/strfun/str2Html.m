%{
htmlStr = str2Html(Lang(strFR, strUS))

C = 'rgbymcwk'
for k=1:length(C)
htmlStr = str2Html(Lang(strFR, strUS), 'Color', C(k))
end
%}

function HTML = str2Html(str, varargin)
% TODO : proposer la taille de la font

[varargin, Color] = getPropertyValue(varargin, 'Color', 'b'); %#ok<ASGLU>

switch lower(Color(1))
    case 'r'
        Color = 'red';
    case 'g'
        Color = 'green';
    case 'b'
        Color = 'blue';
        %     case 'c'
        %         Color = 'cian';
        %     case 'm'
        %         Color = 'magenta';
    case 'y'
        Color = 'yellow';
    case 'k'
        Color = 'black';
    case 'w'
        Color = 'white';
    otherwise
        Color = 'black';
end

HTML = sprintf('<b><br><div align=center style="font-family:semi-bold;color:%s"><Center>%s</div></i></Center>', ...
    Color, str);
