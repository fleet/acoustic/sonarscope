% Creation d'une liste de cellules a partir d'une matrice de caractere
%
% Syntax
%   c = str2cell(str)
%
% Input Arguments
%   str : Matrice de caracteres
%
% Output Arguments
%   c   : Tableau de cellules contenant des chaines de caracteres
%
% Examples
%   c = str2cell(['AZERTY';'QWERTY'])
%   whos c
%
% See also Authors
% Authors : JMA
% VERSION  : $Id: str2cell.m,v 1.4 2002/06/06 12:21:48 augustin Exp $
%--------------------------------------------------------------------------------

function s = str2cell(str)

n = size(str,1);
s = cell(n,1);
for i=1:n
    s{i} = str(i,:);
end
