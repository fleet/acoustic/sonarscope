% Codage abrege d'une liste de nombres. 
% Si la s�rie est une suite arithm�tique, la sortie correspond au codage matlab de cette s�rie 
%
% Syntax
%   s = summaryArrayValues(x)
%
% Input Arguments
%	x : suite de nombres
%
% Output Arguments
%	s : code retourn� permetant de reg�n�rer la serie de nombres
%
% Examples 
%   str = summaryArrayValues(0:0.5:10)
%   str = summaryArrayValues([1 2 4 8 10])
%   str = summaryArrayValues(1:2:100)
%   str = summaryArrayValues(rand(1,20))
%
% See also Authors 
% Authors : JMA
%--------------------------------------------------------------------------------

function s = summaryArrayValues(x)

[m, n] = size(x);
nbx = m * n;

% il y a peu de valeurs on affiche tout
if nbx <= 10
    s = mat2str(x);
    return
end

x = x(isfinite(x)); % Ajout JMA le 26/03/2017

% il y a peu de valeurs on affiche tout

if length(x) == 1
    s = sprintf('%.6g ', x);
    
elseif(length(x) <= 5)
    s = sprintf('%.6g ', x);
    s = sprintf('[%s]', s);

elseif (m > 1) && (n > 1) % tableau 2 D
    minx = double(min(x(:)));
    maxx = double(max(x(:)));
    s = sprintf('%d*%d vals from %s to %s', m, n, num2str(minx), num2str(maxx));

elseif (m > 1) || (n > 1)	% Tableau 1D
    a = unique(diff(x(:)));
    if length(a) ~= 1 % le pas est-il constant ?
        b = unique(a < 0.0000001 & a > -0.0000001);
        if (length(b) == 1) && (b ~= 0)
            a = a(1);
        end
    end
    if length(a) == 1
        if a == 0
            if (m*n) <= 5
                s = sprintf('[%s]', sprintf('%g ', x));
            else
                s = sprintf('%g * ones(%d,%d)', x(1), m, n);
            end
        else
            pas = x(2) - x(1);
            if pas == 1
                s = sprintf('[%s:%s]', num2str(x(1)), num2str(x(end)));	% Voir Nota
            else
                s = sprintf('[%s:%s:%s]', num2str(x(1)), num2str(pas), num2str(x(end))); % Voir Nota
            end
        end
    else
        [m, n] = size(x);
        if (m*n) <= 10
            %s = sprintf('[%s]', num2str(x));
            s = sprintf('%.4g ', x);
            s = sprintf('[%s]', s);
        else
            st = stats(x(:));
            s = sprintf('Min=%s, Max=%s, Mean=%s, Med=%s, std=%s', ...
                num2str(st.Min), num2str(st.Max), ...
                num2str(st.Moyenne), num2str(st.Mediane), num2str(st.Sigma)); % Voir Nota
        end
    end
end
s = rmblank(s, 1);

% Nota : on fait sprintf('%s ...', num2str(x), ...) plut�t que sprintf('%f ...', x, ...)
% car cela permet de supprimer automatiquement les zeros � la fin :
% sprintf('%f', 4) ---> 4.000000
% sprintf('%s', num2str(4)) ---> 4
