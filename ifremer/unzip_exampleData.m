% Decompression des fichiers d'exemple
%
% Syntax
%   unzip_exampleData
%
% Examples
%   unzip_exampleData
%
% See also getNomFicDatabase Authors
% Authors : JMA
% -------------------------------------------------------------------------

function unzip_exampleData(varargin)

global SonarScopeData %#ok<GVMIS> 

[varargin, FileSelection] = getPropertyValue(varargin, 'FileSelection', 0);
[varargin, nomDir]        = getPropertyValue(varargin, 'nomDir',        []); %#ok<ASGLU>

if isempty(nomDir)
    listeDir = {fullfile(SonarScopeData, 'Levitus')
        fullfile(SonarScopeData, 'Sonar')
        fullfile(SonarScopeData, 'Public')};
else
    listeDir = {nomDir};
end

FilesToUncompress = {};
ReceptionDir = {};
FilesToDelete = {};
for i=1:length(listeDir)
    liste = listeFicOnDir(listeDir{i}, '.gz');
    for k=1:length(liste)
        FilesToUncompress{end+1} = liste{k}; %#ok<AGROW>
        ReceptionDir{end+1} = listeDir{i}; %#ok<AGROW>
        FilesToDelete{end+1} = liste{k}; %#ok<AGROW>
    end
end

if FileSelection == 1
    sub = my_listdlg('Files that can be uncompressed :', FilesToUncompress);
    FilesToUncompress = FilesToUncompress(sub);
    ReceptionDir = ReceptionDir(sub);
    FilesToDelete = FilesToDelete(sub);
end

for i=1:length(FilesToUncompress)
    disp(['Uncompress : ' FilesToUncompress{i} ' Please wait'])
    try
        gunzip(FilesToUncompress{i}, ReceptionDir{i});
        delete(FilesToDelete{i})
    catch
        str = sprintf('File %s could not be uncompressed', FilesToDelete{i});
        my_warndlg(str, 1);
    end
end
