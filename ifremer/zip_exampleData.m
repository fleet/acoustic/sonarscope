% Compression des fichiers d'exemple
%
% Syntax
%   zip_exampleData
%
% Examples
%   zip_exampleData
%
% See also getNomFicDatabase Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function zip_exampleData(varargin)

global SonarScopeData %#ok<GVMIS> 

[varargin, FileSelection] = getPropertyValue(varargin, 'FileSelection', 0);
[varargin, nomDir]        = getPropertyValue(varargin, 'nomDir',       []); %#ok<ASGLU>

if isempty(nomDir)
    listeDir = {fullfile(SonarScopeData, 'Levitus')
        fullfile(SonarScopeData, 'Sonar')
        fullfile(SonarScopeData, 'Public')};
else
    listeDir = {nomDir};
end

listeExtend = {'.nc', '.hgt', '.grd', '.tif' '.TIFF', '.bmp', '.imo', '.mos', '.mnt', '.dtm', '.mbg', '.png'};

FilesToCompress = {};
ReceptionDir = {};
FilesToDelete = {};
for i=1:length(listeDir)
    for j=1:length(listeExtend)
        liste = listeFicOnDir(listeDir{i}, listeExtend{j});
        for k=1:length(liste)
            FilesToCompress{end+1} = liste{k}; %#ok<AGROW>
            ReceptionDir{end+1} = listeDir{i}; %#ok<AGROW>
%             disp(['File compression : ' liste{k} ' Please wait'])
%             pppp = gzip(liste{k}, listeDir{i});
            FilesToDelete{end+1} = liste{k}; %#ok<AGROW>
%             delete(liste{k})
        end
    end
    
    % --------------------------------
    % cas particulier des fichiers ers
    
    liste = listeFicOnDir(listeDir{i}, '.ers');
    for k=1:length(liste)
        [nomDir, nomFic] = fileparts(liste{k});
        nomFicERS = fullfile(nomDir, nomFic);
        if exist(nomFicERS, 'file')
            FilesToCompress{end+1} = nomFicERS; %#ok<AGROW>
            FilesToDelete{end+1} = nomFicERS; %#ok<AGROW>
            ReceptionDir{end+1} = nomDir; %#ok<AGROW>
        end
%         disp(['File compression : ' FilesToCompress{end} ' Please wait'])
%         pppp = gzip(FilesToDeleteErs{end}, listeDir{i});
%         FilesToDelete{end+1} = FilesToDeleteErs;
%         delete(FilesToDeleteErs)
    end
end

if FileSelection == 1
    sub = my_listdlg('Files that can be compressed :', FilesToCompress);
    FilesToCompress = FilesToCompress(sub);
    ReceptionDir = ReceptionDir(sub);
    FilesToDelete = FilesToDelete(sub);
end

for i=1:length(FilesToCompress)
    disp(['File compression : ' FilesToCompress{i} ' Please wait'])
    gzip(FilesToCompress{i}, ReceptionDir{i});
    delete(FilesToDelete{i})
end
