// Fichier CD_Util.c
//
// OBJET :
// Comporte les fonctions utilitaires pour les mex fonctions CIB_DOU.
// (passerelle entre MatLab et la librairie CIB_DOU de manipulations des 
//  fichiers au format netCDF).
// 
// COMPIL :
// mex mexGetAtt.cpp CD_Utils.c CD_Utils.h ..\lib\libCIB_DOU.dll.a ..\lib\libstdc++.a -outdir ..\lib
// (Faire pr�c�der d'un clear Functions au besoin).
// 
// SEE ALSO : Authors
// AUTHORS  : GLU
// VERSION  : $Id: .c,v 1.0 2006/02/24 08:23:13 Gallou Exp Gallou $
// ----------------------------------------------------------------------------

/*** Includes ***/
#include "CD_Utils.h"
#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 

/////////////////////////////////////////////////////////////////////////////////
// Procedure	: 	CD_UtilErrorString
// Objectif  	: 	Retourne une Cha�ne synonyme du code Erreur.
/////////////////////////////////////////////////////////////////////////////////
char *cCD_UtilErrorString(int iCodeMsg)
{
    switch(iCodeMsg) 
    {
		// Pour un nouveau message, �viter le caract�re ":" pour l'exploitation des messages
		// dans le fichier R�sultats.
		case CIBDOU_E_NO_ERROR:				return " NO_ERROR";
		case CIBDOU_E_INPUT_TYPE:           return " Type of inputs incorrect !!!";
		case CIBDOU_E_INPUT_NUMBER:       	return " Number of inputs incorrect !!!";
		case CIBDOU_E_TYPE_UNTREATED:       return " Type not treated yet !!!";
		case CIBDOU_E_VAR_NOTEXIST:         return " Variable %s does not exist !!!";
		case CIBDOU_E_ATT_NOTEXIST:         return " Attribut %s does not exist !!!";
		
		default:
			break;
    }
    return "Unknown Error";
}   // CD_UtilErrorString

/////////////////////////////////////////////////////////////////////////////////
// Procedure	: 	CD_UtilWarningString
// Objectif  	: 	Retourne une Cha�ne synonyme du code d'avertissement.
/////////////////////////////////////////////////////////////////////////////////
char *cCD_UtilWarningString(int iCodeMsg)
{
    switch(iCodeMsg) 
    {
		// Pour un nouveau message, �viter le caract�re ":" pour l'exploitation des messages
		// dans le fichier R�sultats.
		case CIBDOU_W_NO_WARNING:			return "NO_WARNING";
		case CIBDOU_W_DIM_NULL:             return "Dimensions of variable null !!!";
		
		default:
			break;
    }
    return "Unknown Warning";
}   // cCD_UtilWarningString


/////////////////////////////////////////////////////////////////////////////////
// Procedure	: 	CD_UtilErrorHandler
// Objectif  	: 	Proc�dure de gestion d'erreur.
/////////////////////////////////////////////////////////////////////////////////
char *cCD_UtilErrorHandler( int iCode,
                            char *cSourceFile,
                            int iSourceLine,
                            const void *cData)
{														  
    char 	cMsg[255] = {0},
    		cMsg2[255] = {0},
    		*cLibelle,
            cLabel[200];

    // Si le code d'erreur est bien r�el.
	if (iCode <0)
	{
        //strcpy(cMsg,cCD_UtilErrorString(iCode));
        sprintf(cMsg, "%s\n", cCD_UtilErrorString(iCode));
        sprintf(cLabel,"%s : %s-line %d :", "ERROR", cSourceFile, iSourceLine); 
	}
    else
    {
        //strcpy(cMsg,cCD_UtilWarningString(iCode));
        sprintf(cMsg, "%s\n", cCD_UtilWarningString(iCode));
        sprintf(cLabel,"%s : %s-line %d :", "WARNING", cSourceFile, iSourceLine); 
    }
    
    if (cData != NULL)
    {
        // Dans ce cas, affichage d'un libell� et d'une donn�e.
        // Le libell� contient le format de la donn�e
        cLibelle = calloc(500, sizeof(char));
        sprintf(cMsg2, cMsg, cData);
        sprintf(cLibelle, "%s %s", cLabel, cMsg2);
    }
    else
    {
        // Dans ce cas, affichage d'un libell� seul
        cLibelle = calloc(500, sizeof(char));
        sprintf(cLibelle, "%s %s", cLabel, cMsg);
    }
    return cLibelle;
}   // CD_UtilErrorHandler


/////////////////////////////////////////////////////////////////////////////////
// Procedure	: 	CD_UtilStrType
// Objectif  	: 	Retourne une cha�ne selon le code entier Type.
/////////////////////////////////////////////////////////////////////////////////
size_t iCD_UtilStrType(char *cType, int iTypeCode)
{
size_t  iSizeType;

    switch(iTypeCode) 
    {
		case CIBDOU_BYTE:		
            strcpy(&cType[0],"byte");
            iSizeType = 1;
            break;
		case CIBDOU_CHAR:		
            strcpy(&cType[0],"char");
            iSizeType = 1;
            break;
		case CIBDOU_SHORT:		
            strcpy(&cType[0],"short");
            iSizeType = 2;
            break;
		case CIBDOU_LONG:		
            strcpy(&cType[0],"long");
            iSizeType = 8;
            break;
		case CIBDOU_FLOAT:		
            strcpy(&cType[0],"float");
            iSizeType = 4;
            break;
		case CIBDOU_DOUBLE:		
            strcpy(&cType[0],"double");
            iSizeType = 4;
            break;
		default:
            strcpy(&cType[0],"byte");
            iSizeType = 1;
			break;
    }
    return iSizeType;
    
}   // iCD_UtilStrType



/////////////////////////////////////////////////////////////////////////////////
// Procedure	: 	CD_UtilIntType
// Objectif  	: 	Retourne un entier selon la cha�ne identifiant le type.
/////////////////////////////////////////////////////////////////////////////////
size_t iCD_UtilIntType(int *iTypeCode, const char *cType)
{
size_t  iSizeType;
    if (strcmp(cType, "byte") == 0)
    {
        iSizeType = 1;
        *iTypeCode = 1;
    }
    else if (strcmp(cType, "char") == 0)
    {
        iSizeType = 1;
        *iTypeCode = 2;
    }
    else if (strcmp(cType, "short") == 0)
    {
        iSizeType = 2;
        *iTypeCode = 3;
    }
    else if (strcmp(cType, "long") == 0)
    {
        iSizeType = 8;
        *iTypeCode = 4;
    }
    else if (strcmp(cType, "float") == 0)
    {
        iSizeType = 4;
        *iTypeCode = 5;
    }
    else if (strcmp(cType, "double") == 0)
    {
        iSizeType = 4;
        *iTypeCode = 6;
    }
    else
    {
        iSizeType = 1;
        *iTypeCode = 1;
    }

    return iSizeType;
    
}   // CD_UtilIntType

/////////////////////////////////////////////////////////////////////////////////
// Procedure	: 	CD_UtilSplitStr
// Objectif  	: 	Retourne un entier selon la cha�ne identifiant le type.
// Auteur	:	
//	http://www.cppfrance.com/codes/SPLIT-EXPLODE-cChaineIn-CARACTERE_33276.aspx
// Parametres	:		
//			Retour tableau des cChaineIns recup�rer. Termin� par NULL. 
//			cChaineIn : cChaineIn � splitter 
//			cDelim : delimiteur qui sert � la decoupe 
//			iVide :	0 : on n'accepte pas les cChaineIns vides 
//					1 : on accepte les cChaineIns vides 
/////////////////////////////////////////////////////////////////////////////////
char** cCD_UtilSplitStr(	char* cChaineIn,
						const char* cDelim,
						int iVide){ 
     
    char	**cTabOut=NULL,		//tableau de cChaineIn, tableau resultat 
			*cPtr,				//pointeur sur une partie de 
			*cLargestring;		//chaine � traiter 
    int		iSizeStr,			//taille de la cChaineIn � recup�rer 
			iSizeTab=0,			//taille du tableau de cChaineIn 
			iSizeDelim=strlen(cDelim);	//taille du delimiteur 
  
  
    cLargestring = cChaineIn; //comme ca on ne modifie pas le pointeur d'origine 
                                   //(faut ke je verifie si c bien n�cessaire) 
     
  
    while( (cPtr=strstr(cLargestring, cDelim))!=NULL ){ 
           iSizeStr=cPtr-cLargestring; 
      
           //si la cChaineIn trouv� n'est pas iVide ou si on accepte les cChaineIn iVide 
           if(iVide==1 || iSizeStr!=0){ 
               //on alloue une case en plus au tableau de cChaineIns 
               iSizeTab++; 
               cTabOut= (char**) realloc(cTabOut,sizeof(char*)*iSizeTab); 
                               
               //on alloue la cChaineIn du tableau 
               cTabOut[iSizeTab-1]=(char*) malloc( sizeof(char)*(iSizeStr+1) ); 
               strncpy(cTabOut[iSizeTab-1],cLargestring,iSizeStr); 
               cTabOut[iSizeTab-1][iSizeStr]='\0'; 
           } 
            
           //on decale le pointeur cLargestring pour continuer la boucle apres le premier el�ment traiter 
           cPtr=cPtr+iSizeDelim; 
           cLargestring=cPtr; 
    } 
     
    //si la cChaineIn n'est pas iVide, on recupere le dernier "morceau" 
    if(strlen(cLargestring)!=0){ 
           iSizeStr=strlen(cLargestring); 
           iSizeTab++; 
           cTabOut= (char**) realloc(cTabOut,sizeof(char*)*iSizeTab); 
           cTabOut[iSizeTab-1]=(char*) malloc( sizeof(char)*(iSizeStr+1) ); 
           strncpy(cTabOut[iSizeTab-1],cLargestring,iSizeStr); 
           cTabOut[iSizeTab-1][iSizeStr]='\0'; 
    } 
    else if(iVide==1){ //si on fini sur un delimiteur et si on accepte les mots vides,on ajoute un mot iVide 
           iSizeTab++; 
           cTabOut= (char**) realloc(cTabOut,sizeof(char*)*iSizeTab); 
           cTabOut[iSizeTab-1]=(char*) malloc( sizeof(char)*1 ); 
           cTabOut[iSizeTab-1][0]='\0'; 
            
    } 
     
    //on ajoute une case � null pour finir le tableau 
    iSizeTab++; 
    cTabOut= (char**) realloc(cTabOut,sizeof(char*)*iSizeTab); 
    cTabOut[iSizeTab-1]=NULL; 
  
    return cTabOut; 
}  // CD_UtilSplitStr
