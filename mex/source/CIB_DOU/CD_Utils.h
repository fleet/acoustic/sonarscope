// Fichier CD_Util.h
//
// OBJET :
// Comporte les déclarations nécessaires aux utilitaires du fichier CD_Utils.c
// 
// COMPIL :
// Sans objet
//
// SEE ALSO : Authors
// AUTHORS  : GLU
// VERSION  : $Id: .c,v 1.0 2006/02/24 08:23:13 Gallou Exp Gallou $
// ----------------------------------------------------------------------------

#ifndef CD_UTILS_H

	/*** Includes ***/
    #include <stddef.h> // Utile pour la déclaration par size_t.

	/*** Types***/

	/*** Constantes ***/
	#define CD_UTILS_H
	#define	SGN(A)	((A) > (0) ? (1) : (-1))	// Macro de détermination du signe.

	enum {
	    CIBDOU_E_NO_ERROR       = -1,
		CIBDOU_E_INPUT_TYPE     = CIBDOU_E_NO_ERROR -1,
		CIBDOU_E_INPUT_NUMBER	= CIBDOU_E_INPUT_TYPE -1,
        CIBDOU_E_TYPE_UNTREATED = CIBDOU_E_INPUT_NUMBER - 1,
        CIBDOU_E_VAR_NOTEXIST   = CIBDOU_E_TYPE_UNTREATED - 1,
        CIBDOU_E_ATT_NOTEXIST   = CIBDOU_E_VAR_NOTEXIST - 1
	} CIBDOU_ERRORS_CODE;

	enum {
	    CIBDOU_W_NO_WARNING     = 0,
		CIBDOU_W_DIM_NULL       = CIBDOU_W_NO_WARNING +1
	} CIBDOU_WARNINGS_CODE;

    enum {
        CIBDOU_NAT    = 0,              // Not A Type.
        CIBDOU_BYTE   = CIBDOU_NAT + 1,
        CIBDOU_CHAR   = CIBDOU_BYTE + 1,
        CIBDOU_SHORT  = CIBDOU_CHAR + 1,
        CIBDOU_LONG   = CIBDOU_SHORT + 1,
        CIBDOU_FLOAT  = CIBDOU_LONG + 1,
        CIBDOU_DOUBLE = CIBDOU_FLOAT + 1
    } CIBDOU_TYPE;


    /*** Prototypes ***/
    size_t	iCD_UtilStrType(char *cType, 
                            int iTypeCode);
    
    size_t  iCD_UtilIntType(int *iTypeCode, 
                            const char *cType);

    char	*cCD_UtilErrorString(int iCodeMsg);

    char	*cCD_UtilWarningString(int iCodeMsg);
	
    char    *cCD_UtilErrorHandler(  int iCode,
                                    char *cSourceFile,
                                    int  iSourceLine,
                                    const void *cData);
    
	char	**cCD_UtilSplitStr(	char* cChaine,
								const char* cDlim,
								int iVide); 
    
#endif
