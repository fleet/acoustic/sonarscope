% Compilation des fonctions mexCIB_DOU cr��es.
%
% SYNTAX :
%   compil_mexCIB_DOU(file1, file2, ...)
%   compil_mexCIB_DOU(all)
%
% INPUT PARAMETERS :
%   nom du fichier � compiler,
%   option : soit verbose, soit debuggage, soit ...
%   all : tous les fichiers sont � compiler.
%
% PROPERTY NAMES / PROPERTY VALUES:
%   ...
%
% OUTPUT PARAMETERS :
%   Sans objet
%
% EXAMPLES :
%   compil_CIB_DOU('env','Linux','compiler', 'gcc', 'option', '-v -g', 'filesource',...
%               {'mexCD_AddAtt.cpp' 'mexCD_NbDim.cpp'});
%   compil_CIB_DOU('env','Win32','option', '-v -g');
%   compil_CIB_DOU('compiler', 'MSVC', 'filesource',{'all'});
%
% REMARQUE:
%   Ne pas oublier de changer de Compilateur par mex -setup si l'option de 
%   compilation change.
%
% SEE ALSO : cl_netcdf cl_netcdf/set_value Authors
% AUTHORS  : JMA + DCF
% VERSION  : $Id: get.m,v 1.3 2003/09/02 09:59:25 augustin Exp augustin $
% ----------------------------------------------------------------------------
function compil_CIB_DOU(varargin)

global IfrTbx;


% N�cessaire pour compiler sinon apparition d'un message d'erreur
% (A confirmer !!!)
clear functions;

compiler    = getPropertyValue(varargin, 'compiler', 'gcc');
env         = getPropertyValue(varargin, 'env', 'Win32');
option      = getPropertyValue(varargin, 'option', []);
filesource	= getPropertyValue(varargin, 'filesource', {'all'});

if strcmp(compiler, 'gcc')
    % Librairies g�n�r�es sous gcc (mode Debug inexploitable pour l'instant)
    if strcmp(env, 'Linux')
        IfrLibNetCDF = [IfrTbx '\contributions\netcdf\NetCDF\Debug\libNetCDF.so.a']; %#ok
        IfrLibCIB_DOU = [IfrTbx '\contributions\netcdf\CIB_DOU\Debug\libCIB_DOU.so.a'];%#ok
    else
        IfrLibNetCDF = [IfrTbx '\contributions\netcdf\NetCDF\Debug\libNetCDF.dll.a'];%#ok
        IfrLibCIB_DOU = [IfrTbx '\contributions\netcdf\CIB_DOU\Debug\libCIB_DOU.dll.a'];%#ok
    end
else
    % Librairies g�n�r�es sous Visual C++ (pour le vrai mode Debug)
    IfrLibNetCDF = [IfrTbx '\mex\lib\PCWIN\CIB_DOU\MSVC\MSVC_NetCDF.lib'];
    IfrLibCIB_DOU = [IfrTbx '\mex\lib\PCWIN\CIB_DOU\MSVC\MSVC_CIB_DOU.lib'];
end

IfrIncCIB_DOU = [IfrTbx '\contributions\netcdf\CIB_DOU\INC'];
IfrIncNetCDF = [IfrTbx '\contributions\netcdf\NetCDF\INC'];
IfrSrcCIB_DOU = [IfrTbx '\mex\source\CIB_DOU'];

if strcmp(env, 'Linux')
    IfrLibOut = [IfrTbx '\mex\lib\GLNX86\CIB_DOU'];
    IfrIncCIB_DOU = strrep(IfrIncCIB_DOU, '\','/');
    IfrIncNetCDF = strrep(IfrIncNetCDF, '\','/');
    IfrSrcCIB_DOU = strrep(IfrSrcCIB_DOU, '\','/');

else
    if strcmp(compiler, 'gcc')
        pppp = winqueryreg('HKEY_LOCAL_MACHINE', 'Software\MinGW Developer Studio');
        % Pour la compilation mex, il faut unifier la cha�ne Program Files (ou
        % installer ailleurs MinGWStudio)
        keyMinGWStudio = strrep(pppp,'Program Files','Progra~1');
        LibMinGWStudio = [keyMinGWStudio '\MinGW\lib\'];
        if ~isdir(LibMinGWStudio)
            errordlg(Lang('MinGWStudio et le compilateur gcc ne sont pas correctement install�. ','MinGWStudio and gcc compilator aren''t correctly installed'));
        end
    end
    IfrLibOut = [IfrTbx '\mex\lib\PCWIN\CIB_DOU'];
end


source = {};   %#ok
for i=1:numel(filesource)
    source{i} = fullfile(IfrSrcCIB_DOU, cell2str(filesource(i)));
end

% Construction de la liste des fichiers � compiler
FileSources = dir( fullfile(IfrSrcCIB_DOU, 'mex*.c*'));
NameSources = {};   %#ok
for i=1:numel(FileSources)
    NameSources{i} = fullfile(IfrSrcCIB_DOU, FileSources(i).name);
end

if nargin ~= 0
    if strcmp(filesource, 'all')
        if isempty(NameSources)
            errordlg(Lang('Aucun fichier source trouv�, v�rifier le contenu du r�pertoire','None source files found. Check the content of the directory'),'Erreur');
        end
        %Compilation successives des diff�rentes fonctions
        for i=1:numel (NameSources)
            if strcmp(compiler, 'gcc')
                if strcmp(env, 'Linux')
                    cmd = ['mex ' option ' ' cell2str(NameSources(i)) ' ' IfrSrcCIB_DOU '\CD_Utils.c -I' IfrIncNetCDF ' -I' IfrIncCIB_DOU ' ' IfrLibCIB_DOU ' ' IfrLibNetCDF ' -outdir ' IfrLibOut];
                    cmd = strrep(cmd, '\','/');
            else
                    %R�f�rence au libstdc++.a
                    cmd = ['mex -D_CRT_SECURE_NO_DEPRECATE ' option ' ' cell2str(NameSources(i)) ' ' IfrSrcCIB_DOU '\CD_Utils.c -I' IfrIncNetCDF ' -I' IfrIncCIB_DOU ' ' IfrLibCIB_DOU ' ' IfrLibNetCDF ' ' LibMinGWStudio 'libstdc++.a -outdir ' IfrLibOut];
                end
            else % Compilo MSVC sous Win32
                cmd = ['mex -D_CRT_SECURE_NO_DEPRECATE ' option ' ' cell2str(NameSources(i)) ' ' IfrSrcCIB_DOU '\CD_Utils.c -I' IfrIncNetCDF ' -I' IfrIncCIB_DOU ' ' IfrLibCIB_DOU ' ' IfrLibNetCDF ' -outdir ' IfrLibOut];
            end
            cmd = [cmd ' -output .dll'];%#ok
            eval(cmd);
        end
    else
        if isempty(filesource)
            errordlg(Lang('Aucun fichier source trouv�, v�rifier le contenu du r�pertoire ou la commande','None source files found. Check the content of the directory or the command'),'Erreur');
        end
        %Compilation unitaire"
        for i = 1:numel(filesource)
            if strcmp(compiler, 'gcc')
                if strcmp(env, 'Linux')
                    cmd = ['mex ' option ' ' cell2str(source(i)) ' ' IfrSrcCIB_DOU '\CD_Utils.c -I' IfrIncNetCDF ' -I' IfrIncCIB_DOU ' ' IfrLibCIB_DOU ' ' IfrLibNetCDF ' -outdir ' IfrLibOut];
                    cmd = strrep(cmd, '\','/');
                else
                    %R�f�rence au libstdc++.a
                    cmd = ['mex -D_CRT_SECURE_NO_DEPRECATE ' option ' ' cell2str(source(i)) ' ' IfrSrcCIB_DOU '\CD_Utils.c -I' IfrIncNetCDF ' -I' IfrIncCIB_DOU ' ' IfrLibCIB_DOU ' ' IfrLibNetCDF ' ' LibMinGWStudio 'libstdc++.a -outdir ' IfrLibOut];
                end
            else % Compilo MSVC sous Win32
                cmd = ['mex -D_CRT_SECURE_NO_DEPRECATE ' option ' ' cell2str(source(i)) ' ' IfrSrcCIB_DOU '\CD_Utils.c -I' IfrIncNetCDF ' -I' IfrIncCIB_DOU ' ' IfrLibCIB_DOU ' ' IfrLibNetCDF ' -outdir ' IfrLibOut];
            end
            cmd = [cmd ' -output .dll'];%#ok
            eval(cmd);
        end
    end
end