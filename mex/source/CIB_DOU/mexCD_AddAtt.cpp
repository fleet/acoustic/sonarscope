// Fichier mexCD_AddAtt.cpp
//
// SYNTAX :
//   mexCD_AddAtt(idFile, AttName, AttData)
//
// INPUT PARAMETERS :
//   idFile     : identifiant du fichier (d�j� ouvert)
//   AttName	: nom de l'attribut � ajouter.
//   AttData	: data de l'attribut � ajouter.
//
// COMPIL :
//   S�lectionner le compilateur de fa�on ad hoc : gcc ou Visual C++(=MSVC).
//   compil_CIB_DOU('compiler', 'MSVC', 'option', '-v ', 'filesource',{'all'})
//   ou
//   compil_CIB_DOU('compiler', 'MSVC', 'option', '-v ', 'filesource',{'nom_du_source.cpp'})
//
// SEE ALSO : Authors
// AUTHORS  : GLU
// VERSION  : $Id: .c,v 1.0 2006\01\25 08:23:13 Gallou Exp Gallou $
// ----------------------------------------------------------------------------

#include <mex.h>
#include <mat.h>
#include "CIB_DOU_File.h"

extern "C" { 
    #include "CD_Utils.h"
} 

#define DEBUG 0

void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
int         iNbItems,
            iTypeInput;

char        cDummy[500],
            *cNameAttribut,
            *cTabData;

short int       *siTabData;

double          *dTabData;

float           *fTabData;

BOOL            bRet;
   
CIB_DOU_File    *CDFile;        // Cr�ation de la variable d'acc�s au fichier;

    if (DEBUG) 
    {
        mexPrintf("Fichier %s, nrhs : %d\n", __FILE__, nrhs);
    }
    switch (nrhs)  
    {
        case 3:
            // R�cup�ration du pointeur de fichier (OBLIGATOIRE en 1er 
            // argument).
            CDFile = (CIB_DOU_File *)(long)mxGetScalar(prhs[0]);

            if (mxGetClassID(prhs[1]) == mxCHAR_CLASS)
            {
                // Longueur du nom de la variable pass�e en argument.
                iNbItems = mxGetNumberOfElements(prhs[1]);
                // R�cup�ration du nom.
                cNameAttribut=(char *)mxCalloc(iNbItems+1,sizeof(char));
                mxGetString(prhs[1],cNameAttribut,iNbItems+1);
            }
            else
            {
                sprintf(&cDummy[0],"%s", cCD_UtilErrorHandler(  CIBDOU_E_INPUT_TYPE, 
                                                                __FILE__,
                                                                __LINE__,
                                                                ""));
                mexErrMsgTxt(cDummy);
            }

            // Identification du type des valeurs.
            iTypeInput = mxGetClassID(prhs[2]);
            
            // R�cup�ration de la taille des donn�es.
            iNbItems = mxGetNumberOfElements(prhs[2]);
           
            break;
        
        default:
            sprintf(&cDummy[0],"%s", cCD_UtilErrorHandler(  CIBDOU_E_INPUT_NUMBER, 
                                                            __FILE__,
                                                            __LINE__,
                                                            ""));
            mexErrMsgTxt(cDummy);
            break;
    }
    
    if (DEBUG) 
    {
        mexPrintf("cNameAttribut : %s\n", cNameAttribut);
        mexPrintf("Nombre d'�l�ments de prhs[2] : %d\n", iNbItems);
    }
    switch (iTypeInput)
    {    
        case mxUNKNOWN_CLASS:
            sprintf(&cDummy[0],"%s", cCD_UtilErrorHandler(  CIBDOU_E_TYPE_UNTREATED, 
                                                            __FILE__,
                                                            __LINE__,
                                                            ""));
            mexErrMsgTxt(cDummy);
            break;
        case mxCHAR_CLASS:
            cTabData=(char *)mxCalloc(iNbItems+1,2*sizeof(char));
            mxGetString(prhs[2],cTabData,iNbItems+1);
            bRet = CDFile->Add_att(cNameAttribut, iNbItems, cTabData);
            break;
        case mxINT16_CLASS:    // Valeurs de type Short.
            siTabData = (short int *)mxGetData(prhs[2]);
            iNbItems = mxGetNumberOfElements(prhs[2]);
            bRet = CDFile->Add_att(cNameAttribut, iNbItems, (const short *)siTabData);
            break;
        case mxSINGLE_CLASS:    // Valeurs de type Float
            fTabData = (float *)mxGetData(prhs[2]);
            iNbItems = mxGetNumberOfElements(prhs[2]);
            bRet = CDFile->Add_att(cNameAttribut, iNbItems, (const float *)fTabData);
            break;
        case mxDOUBLE_CLASS:	// Valeurs de type Long ou Double
            dTabData = (double *)mxGetData(prhs[2]);
            iNbItems = mxGetNumberOfElements(prhs[2]);
			bRet = CDFile->Add_att(cNameAttribut, iNbItems, (const double *)dTabData);
            break;
        
       default:
            break;
                      
    }            

    mxFree(cNameAttribut);
      
    return;
    
    
} // mexCD_AddAtt
