// Fichier mexCD_AddAttVar.cpp
//
// SYNTAX :
//   mexCD_AddAttVar(idFile, Varname, AttName, AttData)
//
// INPUT PARAMETERS :
//   idFile     : identifiant du fichier (d�j� ouvert)
//   VarName	: nom de la variable.
//   AttName	: nom de l'attribut.
//   AttData	: data de l'attribut.
//
// COMPIL :
//   S�lectionner le compilateur de fa�on ad hoc : gcc ou Visual C++(=MSVC).
//   compil_CIB_DOU('compiler', 'MSVC', 'option', '-v ', 'filesource',{'all'})
//   ou
//   compil_CIB_DOU('compiler', 'MSVC', 'option', '-v ', 'filesource',{'nom_du_source.cpp'})
//
// SEE ALSO : Authors
// AUTHORS  : GLU
// VERSION  : $Id: .c,v 1.0 2006\01\25 08:23:13 Gallou Exp Gallou $
// ----------------------------------------------------------------------------

#include <mex.h>
#include <mat.h>
#include "CIB_DOU_File.h"


extern "C" { 
    #include "CD_Utils.h"
} 

#define DEBUG 0

void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
int             iNbItems,
                iNbVals,
                iTypeInput,
                iDummy;

const int       *iTabData;

short int       *siTabData;

double          *dTabData;

float           *fTabData;

char            *cNameVariable,
                *cNameAttribut,
                cDummy[500],
                *cTabData;

BOOL            bRet;
   
CIB_DOU_File    *CDFile;        // Cr�ation de la variable d'acc�s au fichier;

    if (DEBUG) 
    {
        mexPrintf("Fichier %s, nrhs : %d\n", __FILE__, nrhs);
    }
    switch (nrhs)  
    {
        case 4:
            // R�cup�ration du pointeur de fichier (OBLIGATOIRE en 1er 
            // argument).
            CDFile = (CIB_DOU_File *)(long)mxGetScalar(prhs[0]);

            // R�cup�ration du 2�me argument.
            if (mxGetClassID(prhs[1]) == mxCHAR_CLASS &&
                mxGetClassID(prhs[2]) == mxCHAR_CLASS)
            {
                // Longueur du nom de la variable pass�e en argument.
                iNbItems = mxGetN(prhs[1]);    
                // R�cup�ration du nom de la variable.
                cNameVariable=(char *)mxCalloc(iNbItems+1,sizeof(char));
                mxGetString(prhs[1],cNameVariable,iNbItems+1);

                // V�rification de l'existence de la variable
                bRet = CDFile->Exists_var(cNameVariable);
                if (bRet == FALSE)
                {
                    sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(  CIBDOU_E_VAR_NOTEXIST, 
                                                                    __FILE__,
                                                                    __LINE__,
                                                                    cNameVariable));
                    mexErrMsgTxt(cDummy);
                }

                // Longueur du nom de l'attribut pass�e en argument.
                iNbItems = mxGetN(prhs[2]);    
                // R�cup�ration du nom de la variable.
                cNameAttribut=(char *)mxCalloc(iNbItems+1,sizeof(char));
                mxGetString(prhs[2],cNameAttribut,iNbItems+1);
                
            }
            else
            {
                sprintf(&cDummy[0], "%s", cCD_UtilErrorHandler(  CIBDOU_E_INPUT_TYPE, 
																__FILE__,
																__LINE__,
																""));
                mexErrMsgTxt(cDummy);
            }
            
            // Identification du type des valeurs.
            iTypeInput = mxGetClassID(prhs[3]);            
            iNbVals = mxGetNumberOfElements(prhs[3]);
    

            break;
        default:
            sprintf(&cDummy[0],"%s", cCD_UtilErrorHandler(  CIBDOU_E_INPUT_NUMBER, 
                                                           __FILE__,
                                                           __LINE__,
                                                           ""));
            mexErrMsgTxt(cDummy);
            break;
    }
    
            
    if (DEBUG)
    {
        mexPrintf("Nombre de Data: %d\n", iNbVals);
        mexPrintf("Adresse de CDFile\t:\t%d\n",CDFile);
        mexPrintf("Type de data : %d\n", iTypeInput);
    }
    
    switch (iTypeInput)
    {    
        case mxUNKNOWN_CLASS:
            sprintf(&cDummy[0],"%s", cCD_UtilErrorHandler(  CIBDOU_E_TYPE_UNTREATED, 
                                                            __FILE__,
                                                            __LINE__,
                                                            ""));
            mexErrMsgTxt(cDummy);
            break;
        case mxCHAR_CLASS:
            cTabData=(char *)mxCalloc(iNbVals+1,2*sizeof(char));
            mxGetString(prhs[3],cTabData,iNbVals+1);
            bRet = CDFile->Add_att_var(cNameVariable, cNameAttribut, (const char *)cTabData);
            break;
        case mxINT16_CLASS:    // Valeurs de type Short.
            siTabData = (short int *)mxGetData(prhs[3]);
            bRet = CDFile->Add_att_var(cNameVariable, cNameAttribut, iNbVals,  (const short int*)siTabData);
            break;
        case mxSINGLE_CLASS:    // Valeurs de type Float
            fTabData = (float *)mxGetData(prhs[3]);
            bRet = CDFile->Add_att_var(cNameVariable, cNameAttribut, iNbVals,  (const float *)fTabData);
            break;
        case mxDOUBLE_CLASS:	// Valeurs de type Long ou Double
            dTabData = (double *)mxGetData(prhs[3]);
            bRet = CDFile->Add_att_var(cNameVariable, cNameAttribut, iNbVals, (const double *)dTabData);
            break;
        
       default:
            break;
                      
    }            

    mxFree(cNameVariable);
    
    
    return;
    
    
} // mexCD_AddAttVar
