// Fichier mexCD_AddDim.cpp
//
// SYNTAX :
//   mexCD_AddDim(idFile, DimName, DimSize)
//
// INPUT PARAMETERS :
//   idFile     : identifiant du fichier (d�j� ouvert)
//   DimName	: nom de la dimension � ajouter.
//   DimSize	: taille de la dimension � ajouter.
//
// COMPIL :
//   S�lectionner le compilateur de fa�on ad hoc : gcc ou Visual C++(=MSVC).
//   compil_CIB_DOU('compiler', 'MSVC', 'option', '-v ', 'filesource',{'all'})
//   ou
//   compil_CIB_DOU('compiler', 'MSVC', 'option', '-v ', 'filesource',{'nom_du_source.cpp'})
//   
// SEE ALSO : Authors
// AUTHORS  : GLU
// VERSION  : $Id: .c,v 1.0 2006\01\25 08:23:13 Gallou Exp Gallou $
// ----------------------------------------------------------------------------

#include <mex.h>
#include <mat.h>
#include "CIB_DOU_File.h"

extern "C" { 
    #include "CD_Utils.h"
} 

#define DEBUG 0

void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
int         iDimSize,
            iNbItems;

char        cDummy[500],
            *cNameDim;

CIB_DOU_File* CDFile;   // Cr�ation de la variable d'acc�s au fichier;

    switch (nrhs)  
    {
        case 3:
            // R�cup�ration du pointeur de fichier (OBLIGATOIRE en 1er 
            // argument).
            CDFile = (CIB_DOU_File *)(long)mxGetScalar(prhs[0]);

            if (mxGetClassID(prhs[1]) == mxCHAR_CLASS)
            {
                // Longueur du nom de la variable pass�e en argument.
                iNbItems = mxGetNumberOfElements(prhs[1]);
                // R�cup�ration du nom.
                cNameDim=(char *)mxCalloc(iNbItems+1,sizeof(char));
                mxGetString(prhs[1],cNameDim,iNbItems+1);
            }
            else
            {
                sprintf(&cDummy[0],"%s", cCD_UtilErrorHandler(  CIBDOU_E_INPUT_TYPE, 
                                                                __FILE__,
                                                                __LINE__,
                                                                ""));
                mexErrMsgTxt(cDummy);
            }
            if (mxGetClassID(prhs[2]) == mxDOUBLE_CLASS)
            {
                // R�cup�ration de la taille.
                iDimSize=(int)mxGetScalar(prhs[2]);
            }
            else
            {
                sprintf(&cDummy[0],"%s", cCD_UtilErrorHandler(  CIBDOU_E_INPUT_TYPE, 
                                                                __FILE__,
                                                                __LINE__,
                                                               ""));
                mexErrMsgTxt(cDummy);
            }
            
            if (DEBUG) 
            {
                mexPrintf("Longueur Nom Dim  : %d\n", iNbItems);
                mexPrintf("iDimSize : %d\n", iDimSize);
                mexPrintf("cNameDim : %s\n", cNameDim);
            }
           // Cr�ation de la dimension.
            CDFile->Add_dim(cNameDim, iDimSize);
            break;
        
        default:
            sprintf(&cDummy[0],"%s", cCD_UtilErrorHandler(  CIBDOU_E_INPUT_NUMBER, 
                                                            __FILE__,
                                                            __LINE__,
                                                           ""));
            mexErrMsgTxt(cDummy);
            break;
    }
    
    mxFree(cNameDim);
      
    return;
    
    
} // mexCD_AddDim
