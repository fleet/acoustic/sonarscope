// Fichier mexCD_AddVar.cpp
//
// SYNTAX :
//   mexCD_AddVar(idFile, VarName, VarData, ...)
//
// INPUT PARAMETERS :
//   idFile     : identifiant du fichier (d�j� ouvert)
//   VarName	: nom de la variable � ajouter.
//   VarData	: data de la variable � ajouter.
//
// COMPIL :
//   S�lectionner le compilateur de fa�on ad hoc : gcc ou Visual C++(=MSVC).
//   compil_CIB_DOU('compiler', 'MSVC', 'option', '-v ', 'filesource',{'all'})
//   ou
//   compil_CIB_DOU('compiler', 'MSVC', 'option', '-v ', 'filesource',{'nom_du_source.cpp'})
//
// SEE ALSO : Authors
// AUTHORS  : GLU
// VERSION  : $Id: .c,v 1.0 2006\01\25 08:23:13 Gallou Exp Gallou $
// ----------------------------------------------------------------------------

#include <mex.h>
#include <mat.h>
#include "CIB_DOU_File.h"  // Inclut d�j� les classes String et List n�cessaires.

extern "C" { 
    #include "CD_Utils.h"
} 

#define DEBUG 0

void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
int         iNbItems,
            iIndex;

char        cDummy[500],
            *cTabDims[100],
            *cNameVariable,
            *cTypeVariable;

CIB_DOU_File    *CDFile;                        // Cr�ation de la variable d'acc�s au fichier;
CIB_STC_List<CIB_STC_String>    CDTabDims;      // Cr�ation de la liste des dimensions.

    if (DEBUG) 
    {
        mexPrintf("Fichier %s, nrhs : %d\n", __FILE__, nrhs);
    }
    if (nrhs >= 3)
    {
        // R�cup�ration du pointeur de fichier (OBLIGATOIRE en 1er 
        // argument).
        CDFile = (CIB_DOU_File *)(long)mxGetScalar(prhs[0]);

        if (mxGetClassID(prhs[1]) == mxCHAR_CLASS)
        {
            // Longueur du nom de la variable pass�e en argument.
            iNbItems = mxGetNumberOfElements(prhs[1]);
            // R�cup�ration du nom.
            cNameVariable=(char *)mxCalloc(iNbItems+1,sizeof(char));
            mxGetString(prhs[1],cNameVariable,iNbItems+1);

            // Longueur du nom du type de la variable pass�e en argument.
            iNbItems = mxGetNumberOfElements(prhs[2]);
            // R�cup�ration du type.
            cTypeVariable=(char *)mxCalloc(iNbItems+1,sizeof(char));
            mxGetString(prhs[2],cTypeVariable,iNbItems+1);
            if (nrhs > 3)
            {
                for (iIndex=3; iIndex<nrhs; iIndex++)
                {
                    if(!mxIsChar(prhs[iIndex]))
                    {
                        mexErrMsgTxt("Input must be of type char.");
                    }
                    // Copie de la dimension dans le tableau de Dim.
                    cTabDims[iIndex-3] = mxArrayToString(prhs[iIndex]);
                    CDTabDims.append(new CIB_STC_String(mxArrayToString(prhs[iIndex])));
                }
            }
            
            // Cr�ation de la variable avec ses dimensions associ�s.
            CDFile->Add_variable(cNameVariable, cTypeVariable, &CDTabDims, NULL);
        }
        else
        {
            sprintf(&cDummy[0],"%s", cCD_UtilErrorHandler(  CIBDOU_E_INPUT_TYPE, 
                                                           __FILE__,
                                                           __LINE__,
                                                           ""));
            mexErrMsgTxt(cDummy);
        }

    }       
    else
    {
        sprintf(&cDummy[0],"%s", cCD_UtilErrorHandler(  CIBDOU_E_INPUT_NUMBER, 
                                                        __FILE__,
                                                        __LINE__,
                                                        ""));
        mexErrMsgTxt(cDummy);
    }
          
    return;
    
    
} // mexCD_AddVar
