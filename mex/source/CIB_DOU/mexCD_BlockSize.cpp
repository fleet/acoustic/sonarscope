// Fichier mexCD_BlockSize.cpp
//
// SYNTAX :
//   mexCD_BlockSize(idFile)
//
// INPUT PARAMETERS :
//   idFile     : identifiant du fichier (d�j� ouvert)
//
// COMPIL :
//   S�lectionner le compilateur de fa�on ad hoc : gcc ou Visual C++(=MSVC).
//   compil_CIB_DOU('compiler', 'MSVC', 'option', '-v ', 'filesource',{'all'})
//   ou
//   compil_CIB_DOU('compiler', 'MSVC', 'option', '-v ', 'filesource',{'nom_du_source.cpp'})
//
// SEE ALSO : Authors
// AUTHORS  : GLU
// VERSION  : $Id: .c,v 1.0 2006\01\25 08:23:13 Gallou Exp Gallou $
// ----------------------------------------------------------------------------

#include <mex.h>
#include <mat.h>
#include "CIB_DOU_File.h"
extern "C" { 
    #include "CD_Utils.h"
}

#define DEBUG 0

void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
int         iNumber;

char        cDummy[500];

CIB_DOU_File *idFile;   // Cr�ation de la variable d'acc�s au fichier;

    if (DEBUG) 
    {
        mexPrintf("Fichier %s, nrhs : %d\n", __FILE__, nrhs);
    }

    if (nrhs != 1)  
    {
        sprintf(&cDummy[0],"%s", cCD_UtilErrorHandler(  CIBDOU_E_INPUT_NUMBER, 
                                                        __FILE__,
                                                        __LINE__,
                                                        ""));
        mexErrMsgTxt(cDummy);
    }
     
    idFile = (CIB_DOU_File *)(long)mxGetScalar(prhs[0]);
    iNumber = idFile->BlockSize();

          
    if (DEBUG)
    {
        mexPrintf("Adresse de idFile\t:\t%d\n",idFile);
        mexPrintf("Taille des blocs \t:\t%d\n",iNumber);
    }
    
    
    // Rapatriement de la variable de sortie.
    plhs[0] = mxCreateDoubleMatrix(1,1, mxREAL);
    *mxGetPr(plhs[0]) = (int)iNumber;
        
    return;
    
    
} // mexCD_BlockSize
