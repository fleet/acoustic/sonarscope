// Fichier mexCD_CloseFile.cpp
//
// SYNTAX :
//   mexCD_CloseFile(CDFile)
//
// INPUT PARAMETERS :
//   idFile     : identifiant du fichier (d�j� ouvert)
//
// COMPIL :
//   S�lectionner le compilateur de fa�on ad hoc : gcc ou Visual C++(=MSVC).
//   compil_CIB_DOU('compiler', 'MSVC', 'option', '-v ', 'filesource',{'all'})
//   ou
//   compil_CIB_DOU('compiler', 'MSVC', 'option', '-v ', 'filesource',{'nom_du_source.cpp'})
//
// SEE ALSO : Authors
// AUTHORS  : GLU
// VERSION  : $Id: .c,v 1.0 2006\01\25 08:23:13 Gallou Exp Gallou $
// ----------------------------------------------------------------------------

#include <mex.h>
#include <mat.h>
#include "CIB_DOU_File.h"

extern "C" { 
    #include "CD_Utils.h"
} 


#define DEBUG 0

void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
    
char            cDummy[500];
int             iDummy;
    
CIB_DOU_File*   CDFile; // Cr�ation de la variable d'acc�s au fichier

    if (nrhs != 1) 
    {
        sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(  CIBDOU_E_INPUT_NUMBER, 
                                                        __FILE__,
                                                        __LINE__,
                                                        ""));
        mexErrMsgTxt(cDummy);
    }
    // Assign pointers to each output and input
    CDFile = (CIB_DOU_File *)(long)mxGetScalar(prhs[0]);
    if (DEBUG)
    {
        mexPrintf("Fermeture de CDFile\t:\t%d\n",CDFile);
    }
    
    // Flush dans le fichier en �criture.
    iDummy = CDFile->Flush();
		
	// Fermeture si non-Debug
	#ifdef NDEBUG
		CDFile->Close();
	#endif

    // Fermeture OBLIGATOIRE pour ce pointeur effectu�e dans
    // la m�thode Close de la classe (probl�me avec Visual C++)
    #ifndef _MSC_VER
        delete CDFile;
    #endif
    
    return;
        
} // mexCD_CloseFile
