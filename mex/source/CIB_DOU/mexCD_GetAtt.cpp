// Fichier mexCD_GetAtt.cpp
//
// SYNTAX :
//   mexCD_GetAtt(idFile, indexAtt)
//   mexCD_GetAtt(idFile, nameAtt)
//
// INPUT PARAMETERS :
//   idFile     : identifiant du fichier (d�j� ouvert)
//   indexAtt	: indice de l'attribut.
//   indexName	: nom de l'attribut.
//
// COMPIL :
//   S�lectionner le compilateur de fa�on ad hoc : gcc ou Visual C++(=MSVC).
//   compil_CIB_DOU('compiler', 'MSVC', 'option', '-v ', 'filesource',{'all'})
//   ou
//   compil_CIB_DOU('compiler', 'MSVC', 'option', '-v ', 'filesource',{'nom_du_source.cpp'})
//
// SEE ALSO : Authors
// AUTHORS  : GLU
// VERSION  : $Id: .c,v 1.0 2006\01\25 08:23:13 Gallou Exp Gallou $
// ----------------------------------------------------------------------------

#include <mex.h>
#include <mat.h>
#include "CIB_DOU_File.h"
extern "C" { 
    #include "CD_Utils.h"
} 


#define DEBUG 0

void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
int         iNbItems,
            iNumber,
            iIndexVariable,
            iLoop;
double      *dDummy;

char        *cNameAttribut,
            *cNomFic,
            cDummy[500];

mxChar      *mxCharData;
size_t      iSizebuf;

BOOL            bRet;

CIB_DOU_File    *CDFile;        // Cr�ation de la variable d'acc�s au fichier;
NcAtt           *CDAtt;         // Cr�ation du containeur des attributs;
NcValues        *CDPtrValues;   // Cr�ation du containeur de valeurs des variables;

    switch (nrhs)  
    {
        case 2:
            // R�cup�ration du pointeur de fichier (OBLIGATOIRE en 1er 
            // argument).
            CDFile = (CIB_DOU_File *)(long)mxGetScalar(prhs[0]);

            // R�cup�ration du 2�me argument.
            if (mxGetClassID(prhs[1]) == mxCHAR_CLASS)
            {
                // Longueur du nom de l'attribut pass�e en argument.
                iNbItems = mxGetN(prhs[1]);    
                cNameAttribut=(char *)mxCalloc(iNbItems+1,sizeof(char));
                mxGetString(prhs[1],cNameAttribut,iNbItems+1);
                // V�rification de l'existence de l'attribut
                bRet = CDFile->Exists_var(cNameAttribut);
                if (bRet == FALSE)
                {
                    sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(  CIBDOU_E_VAR_NOTEXIST, 
                                                                    __FILE__,
                                                                    __LINE__,
                                                                    cNameAttribut));
                    mexErrMsgTxt(cDummy);
                }
                CDAtt = CDFile->Get_Att(cNameAttribut);
                mxFree(cNameAttribut);
            }
            else if (mxGetClassID(prhs[1]) == mxDOUBLE_CLASS)
            {
                iIndexVariable = (int)mxGetScalar(prhs[1]) - 1;
                // V�rification de l'existence de la variable
                if (iIndexVariable > CDFile->Num_atts())
                {
                    sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(  CIBDOU_E_ATT_NOTEXIST, 
                                                                    __FILE__,
                                                                    __LINE__,
                                                                    ""));
                    mexErrMsgTxt(cDummy);
                }
                CDAtt = CDFile->Get_Att(iIndexVariable);
            }
            else
            {
                sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(  CIBDOU_E_INPUT_TYPE, 
                                                            __FILE__,
                                                            __LINE__,
                                                            ""));
                mexErrMsgTxt(cDummy);
            }
            break;
        default:
            sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(  CIBDOU_E_INPUT_NUMBER, 
                                                            __FILE__,
                                                            __LINE__,
                                                            ""));
            mexErrMsgTxt(cDummy);
            break;
    }
    
            
    if (DEBUG)
    {
        mexPrintf("nrhs : %d\n", nrhs);
        mexPrintf("Nb d'attributs de la variable\t:\t%d\n", iNumber);
        mexPrintf("Adresse de CDFile\t:\t%d\n",CDFile);
    }
    
    // Rapatriement des variables de sortie.
    // Nom de l'attribut
    plhs[0] = mxCreateString(CDAtt->name());
    
    // Type de l'attribut    
    iSizebuf = iCD_UtilStrType(&cDummy[0], (int)CDAtt->type());
    plhs[1] = mxCreateString(cDummy);

    // Allocation pr�alable du pointeur de valeur de la variable en jeu.
    CDPtrValues = CDAtt->values();
    
    // Valeurs de l'attribut
    switch ((int)CDAtt->type())
    {    
        case CIBDOU_BYTE:
            sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(  CIBDOU_E_TYPE_UNTREATED, 
                                                            __FILE__,
                                                            __LINE__,
                                                            ""));
            mexErrMsgTxt(cDummy);
            break;
        case CIBDOU_CHAR:
            sprintf(&cDummy[0],"%s", CDPtrValues->as_string(0));
            plhs[2] = mxCreateString(cDummy);
            break;
        case CIBDOU_SHORT:
            iNbItems = (int)CDAtt->num_vals();
            plhs[2] = mxCreateDoubleMatrix(1, iNbItems, mxREAL);
            dDummy = mxGetPr(plhs[2]);
            for (iLoop=0; iLoop<iNbItems; iLoop++) 
            {
              *(dDummy+iLoop) = (double)CDPtrValues->as_short(iLoop);
            }
            break;
        case CIBDOU_LONG:
            iNbItems = (int)CDAtt->num_vals();
            plhs[2] = mxCreateDoubleMatrix(1, iNbItems, mxREAL);
            dDummy = mxGetPr(plhs[2]);
            for (iLoop=0; iLoop<iNbItems; iLoop++) 
            {
              *(dDummy+iLoop) = (double)CDPtrValues->as_long(iLoop);
            }
            break;
        case CIBDOU_FLOAT:
            iNbItems = (int)CDAtt->num_vals();
            plhs[2] = mxCreateDoubleMatrix(1, iNbItems, mxREAL);
            dDummy = mxGetPr(plhs[2]);
            for (iLoop=0; iLoop<iNbItems; iLoop++) 
            {
              *(dDummy+iLoop) = (double)CDPtrValues->as_float(iLoop);
            }
            break;
        case CIBDOU_DOUBLE:
            iNbItems = (int)CDAtt->num_vals();
            plhs[2] = mxCreateDoubleMatrix(1,iNbItems, mxREAL);
            dDummy = mxGetPr(plhs[2]);
            for (iLoop=0; iLoop<iNbItems; iLoop++) 
            {
              *(dDummy+iLoop) = (double)CDPtrValues->as_double(iLoop);
            }
            break;
        
       default:
            break;
                      
    }            
        
    // D�sallocation du pointeur de valeurs (probl�me avec Visual C++)
    #ifndef _MSC_VER
        delete CDPtrValues;
    #endif

    
    return;
    
    
} // mexGet_Att
