// Fichier mexCD_GetAttVar.cpp
//
// SYNTAX :
//   mexCD_GetAttVar(CDFile, indexVar, indexAtt)
//   mexCD_GetAttVar(CDFile, nameVar, indexAtt)
//
// INPUT PARAMETERS :
//   idFile     : identifiant du fichier (d�j� ouvert)
//   indexAtt	: indice de l'attribut.
//   indexVar	: indice de la variable.
//   nameVar	: nom de la variable.
//
// COMPIL :
//   S�lectionner le compilateur de fa�on ad hoc : gcc ou Visual C++(=MSVC).
//   compil_CIB_DOU('compiler', 'MSVC', 'option', '-v ', 'filesource',{'all'})
//   ou
//   compil_CIB_DOU('compiler', 'MSVC', 'option', '-v ', 'filesource',{'nom_du_source.cpp'})
//
// SEE ALSO : Authors
// AUTHORS  : GLU
// VERSION  : $Id: .c,v 1.0 2006\01\25 08:23:13 Gallou Exp Gallou $
// ----------------------------------------------------------------------------

#include <mex.h>
#include <mat.h>
#include "CIB_DOU_File.h"
extern "C" { 
    #include "CD_Utils.h"
} 

#define DEBUG 0


extern "C" {
void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
int         iNbItems,
            iIndexVar,
            iIndexAtt,
            iLoop;

double      *dDummy;

char        *cNameVariable,
            cDummy[500];

BOOL        bRet;
size_t      iSizebuf;
    
CIB_DOU_File    *CDFile;        // Cr�ation de la variable d'acc�s au fichier;
NcAtt           *CDAtt;         // Cr�ation de la variable de containeur des attributs;
NcValues        *CDPtrValues;   // Cr�ation du containeur de valeurs des variables;

    if (DEBUG) 
    {
        mexPrintf("Fichier %s, nrhs : %d\n", __FILE__, nrhs);
    }
    switch (nrhs)  
    {
        case 3:
            // R�cup�ration du pointeur de fichier (OBLIGATOIRE en 1er 
            // argument).
            CDFile = (CIB_DOU_File *)(long)mxGetScalar(prhs[0]);
            // R�cup�ration de l'indice de l'attribut.
            if (mxGetClassID(prhs[2]) == mxDOUBLE_CLASS) 
            {
                iIndexAtt = (int)mxGetScalar(prhs[2]) - 1;
            }
            else
            {
                sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(   CIBDOU_E_INPUT_TYPE, 
                                                                __FILE__,
                                                                __LINE__,
                                                                ""));
                mexErrMsgTxt(cDummy);
            }
            
            // R�cup�ration du 1er argument.
            if (mxGetClassID(prhs[1]) == mxCHAR_CLASS)
            {
                // Longueur du nom de la variable pass�e en argument.
                iNbItems = mxGetN(prhs[1]);    
                // R�cup�ration du nom de fichier.
                cNameVariable=(char *)mxCalloc(iNbItems+1,sizeof(char));
                mxGetString(prhs[1],cNameVariable,iNbItems+1);
                // V�rification de l'existence de la variable
                bRet = CDFile->Exists_var(cNameVariable);
                if (bRet == FALSE)
                {
                    sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(  CIBDOU_E_VAR_NOTEXIST, 
                                                                    __FILE__,
                                                                    __LINE__,
                                                                    cNameVariable));
                    mexErrMsgTxt(cDummy);
                }
                // V�rification de l'existence de l'attribut
                if (iIndexAtt > CDFile->Num_atts_var(cNameVariable))
                {
                    sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(  CIBDOU_E_ATT_NOTEXIST, 
                                                                    __FILE__,
                                                                    __LINE__,
                                                                    ""));
                    mexErrMsgTxt(cDummy);
                }
				if (DEBUG)
				{
					mexPrintf("Identifiant de l'attribut : %d\n", iIndexAtt);
					mexPrintf("Identifiant de la variable : %s\n", cNameVariable);
				}
                CDAtt = CDFile->Get_att_var(cNameVariable,iIndexAtt);
				mxFree(cNameVariable);
             }
            else if (mxGetClassID(prhs[1]) == mxDOUBLE_CLASS)
            {
                iIndexVar = (int)mxGetScalar(prhs[1]) - 1;
                // V�rification de l'existence de la variable
                if (iIndexVar > CDFile->Num_vars())
                {
                    sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(  CIBDOU_E_VAR_NOTEXIST, 
                                                                    __FILE__,
                                                                    __LINE__,
                                                                    ""));
                    mexErrMsgTxt(cDummy);
                }

                // V�rification de l'existence de l'attribut
                if (iIndexAtt > CDFile->Num_atts_var(iIndexVar))
                {
                    sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(  CIBDOU_E_ATT_NOTEXIST, 
                                                                    __FILE__,
                                                                    __LINE__,
                                                                    ""));
                    mexErrMsgTxt(cDummy);
                }
				if (DEBUG)
				{
					mexPrintf("Identifiant de l'attribut : %d\n", iIndexAtt);
					mexPrintf("Identifiant de la variable : %d\n", iIndexVar);
				}
                CDAtt = CDFile->Get_att_var(iIndexVar, iIndexAtt);
            }
            else
            {
                sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(  CIBDOU_E_INPUT_TYPE, 
                                                                __FILE__,
                                                                __LINE__,
                                                                ""));
                mexErrMsgTxt(cDummy);
            }
            break;
        default:
            sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(  CIBDOU_E_INPUT_NUMBER, 
                                                            __FILE__,
                                                            __LINE__,
                                                            ""));
            mexErrMsgTxt(cDummy);
            break;
    }
    
                
    // Rapatriement des variables de sortie.
    // Nom de l'attribut
    plhs[0] = mxCreateString(CDAtt->name());
    
    // Type de l'attribut    
    iSizebuf = iCD_UtilStrType(&cDummy[0], (int)CDAtt->type());
    plhs[1] = mxCreateString(cDummy);

    // Nombre de valeurs (= Produit des N dimensions).
    iNbItems = (int)CDAtt->num_vals();
    
    // Allocation pr�alable du pointeur de valeur de la variable en jeu.
    CDPtrValues = CDAtt->values();

    if (DEBUG)
    {
        mexPrintf("Adresse de CDFile\t:\t%d\n",CDFile);
        mexPrintf("Type de l'attribut\t:\t%d\n", (int)CDAtt->type());
        mexPrintf("Type de l'attribut\t:\t%s\n", cDummy);
    }
    
    // Valeurs de l'attribut
    switch ((int)CDAtt->type())
    {    
        case CIBDOU_BYTE:
            sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(  CIBDOU_E_TYPE_UNTREATED, 
                                                            __FILE__,
                                                            __LINE__,
                                                            ""));
            mexErrMsgTxt(cDummy);
            break;
        case CIBDOU_CHAR:
            sprintf(&cDummy[0],"%s", CDPtrValues->as_string(0));
            plhs[2] = mxCreateString(cDummy);
            break;
        case CIBDOU_SHORT:
            iNbItems = (int)CDAtt->num_vals();
            plhs[2] = mxCreateDoubleMatrix(1, iNbItems, mxREAL);
            dDummy = mxGetPr(plhs[2]);
            for (iLoop=0; iLoop<iNbItems; iLoop++) 
            {
              *(dDummy+iLoop) = CDPtrValues->as_short(iLoop);
            }
            break;
        case CIBDOU_LONG:
            iNbItems = (int)CDAtt->num_vals();
            plhs[2] = mxCreateDoubleMatrix(1, iNbItems, mxREAL);
            dDummy = mxGetPr(plhs[2]);
            for (iLoop=0; iLoop<iNbItems; iLoop++) 
            {
              *(dDummy+iLoop) = CDPtrValues->as_long(iLoop);
            }
            break;
        case CIBDOU_FLOAT:
            iNbItems = (int)CDAtt->num_vals();
            plhs[2] = mxCreateDoubleMatrix(1,iNbItems, mxREAL);
            dDummy = mxGetPr(plhs[2]);
            for (iLoop=0; iLoop<iNbItems; iLoop++) 
            {
              *(dDummy+iLoop) = CDPtrValues->as_float(iLoop);
            }
            break;
        case CIBDOU_DOUBLE:
            iNbItems = (int)CDAtt->num_vals();
            plhs[2] = mxCreateDoubleMatrix(1,iNbItems, mxREAL);
            dDummy = mxGetPr(plhs[2]);
            for (iLoop=0; iLoop<iNbItems; iLoop++) 
            {
              *(dDummy+iLoop) = CDPtrValues->as_double(iLoop);
            }
            break;
        
       default:
            break;
                      
    }            
        
    return;
    
    
} // mexCD_GetAttVar

} // Fin de la d�claration des fonctions Extern.
