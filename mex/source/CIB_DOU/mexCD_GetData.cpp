// Fichier mexCD_GetData.cpp
//
// SYNTAX :
//   mexCD_GetData(idFile, indexVar)
//   mexCD_GetData(idFile, nameVar)
//
// INPUT PARAMETERS :
//   idFile     : identHifiant du fichier (d�j� ouvert)
//   indexVar	: indice de la variable.
//   nameVar	: nom de la variable.
//
// COMPIL :
//   S�lectionner le compilateur de fa�on ad hoc : gcc ou Visual C++(=MSVC).
//   compil_CIB_DOU('compiler', 'MSVC', 'option', '-v ', 'filesource',{'all'})
//   ou
//   compil_CIB_DOU('compiler', 'MSVC', 'option', '-v ', 'filesource',{'nom_du_source.cpp'})
//
// SEE ALSO : Authors
// AUTHORS  : GLU
// VERSION  : $Id: .c,v 1.0 2006\01\25 08:23:13 Gallou Exp Gallou $
// ----------------------------------------------------------------------------

#include <mex.h>
#include <mat.h>
#include "CIB_DOU_File.h"

// D�tection des fuites M�moire sous Visual C++
/*
#ifdef _MSC_VER
	#define _CRTDBG_MAP_ALLOC
	#include <stdlib.h>
	#include <crtdbg.h>
#endif
*/
extern "C" { 
    #include "CD_Utils.h"
} 

#define DEBUG 0

void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
int             iNbItems,
                iIndexVariable,
                iLoop, 
                iLoop2,
                iNbDims,
                iNbDimsOut,
                *iTabDims,
                iDimOffset;

short int       *siTabData;

long int        *lTabData;
mxChar          *mxCharData;
unsigned char   *ucTabData;
double          *dDummy,
                *dTabData;

float           *fTabData;

char            *cNameVariable,
                cDummy[500],
                **cDummy2,
                *cItem,
                *cTabData;

BOOL            bRet;
size_t          iSizebuf;
   
CIB_DOU_File    *CDFile;        // Cr�ation de la variable d'acc�s au fichier;
NcVar           *CDVar;         // Cr�ation du containeur des variables;
NcValues        *CDPtrValues;   // Cr�ation du containeur de valeurs des variables;

    if (DEBUG) 
    {
        mexPrintf("nrhs : %d\n", nrhs);
    }
    switch (nrhs)  
    {
        case 2:
            // R�cup�ration du pointeur de fichier (OBLIGATOIRE en 1er 
            // argument).
            CDFile = (CIB_DOU_File *)(long)mxGetScalar(prhs[0]);

            // R�cup�ration du 2�me argument.
            if (mxGetClassID(prhs[1]) == mxCHAR_CLASS)
            {
                // Longueur du nom de la variable pass�e en argument.
                iNbItems = mxGetN(prhs[1]);    
                // R�cup�ration du nom de la variable.
                cNameVariable=(char *)mxCalloc(iNbItems+1,sizeof(char));
                mxGetString(prhs[1],cNameVariable,iNbItems+1);
                
                // V�rification de l'existence de la variable
                bRet = CDFile->Exists_var(cNameVariable);
                if (bRet == FALSE)
                {
                    sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(  CIBDOU_E_VAR_NOTEXIST, 
                                                                    __FILE__,
                                                                    __LINE__,
                                                                    cNameVariable));
                    mexErrMsgTxt(cDummy);
                }
                // Affectation de la variable VAR.
                CDVar = CDFile->Get_var(cNameVariable);
            }
            else if (mxGetClassID(prhs[1]) == mxDOUBLE_CLASS)
            {
                iIndexVariable = (int)mxGetScalar(prhs[1]) - 1;
                
                // V�rification de l'existence de la variable
                if (iIndexVariable > CDFile->Num_vars())
                {
                    sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(  CIBDOU_E_VAR_NOTEXIST, 
                                                                    __FILE__,
                                                                    __LINE__,
                                                                    ""));
                    mexErrMsgTxt(cDummy);
                }
                iNbItems = strlen(CDFile->Name_var(iIndexVariable));    
                cNameVariable=(char *)mxCalloc(iNbItems+1,sizeof(char));
                sprintf(cNameVariable,CDFile->Name_var(iIndexVariable));

                // Affectation de la variable VAR.
                CDVar = CDFile->Get_var(iIndexVariable);
            }
            else
            {
                sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(  CIBDOU_E_INPUT_TYPE, 
                                                                __FILE__,
                                                                __LINE__,
                                                                ""));
                mexErrMsgTxt(cDummy);
            }
            break;
        default:
            sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(  CIBDOU_E_INPUT_NUMBER, 
                                                        __FILE__,
                                                        __LINE__,
                                                        ""));
            mexErrMsgTxt(cDummy);
            break;
    }
    
            
    if (DEBUG)
    {
        mexPrintf("Adresse de CDFile\t:\t%d\n",CDFile);
    }
    
    // Rapatriement des variables de sortie.
    // Nom de l'attribut
    plhs[0] = mxCreateString(CDVar->name());
    
    // Type de l'attribut    
    iSizebuf = iCD_UtilStrType(&cDummy[0], (int)CDVar->type());
    plhs[1] = mxCreateString(cDummy);

    // Nombre de dimensions associ�es
    iNbDims = (int)CDVar->num_dims();

    // Ajout pour �liminer la dimension CIB_BLOCK_DIM des fichiers "block�s"
    // sous CARAIBES.
    if(CDFile->Is_Unlimited() && CDVar->get_dim (0)->is_unlimited())
    {
        iNbDims = iNbDims - 1;
        iDimOffset = 1;
    }
    else
    {
        iDimOffset = 0;
    }
    
    plhs[2] = mxCreateDoubleMatrix(1,1, mxREAL);
    *mxGetPr(plhs[2]) = (int)iNbDims;
    

	// Valeurs des dimensions (nombre et d�tail)
    plhs[3] = mxCreateDoubleMatrix(iNbDims,1, mxREAL);
    dDummy = mxGetPr(plhs[3]);
    
    // Affectation des dimensions de la variable.
    iTabDims = (int *)mxMalloc(iNbDims*sizeof(int));
    for (iLoop=0; iLoop<iNbDims; iLoop++) 
    {
        *(dDummy+iLoop) = (double)(CDVar->get_dim(iLoop+iDimOffset)->size());
        iTabDims[iLoop] = (int)(CDVar->get_dim(iLoop+iDimOffset)->size());
    }
    if (iNbDims == 0)
    {
        iNbDimsOut = 1;
        iTabDims = (int *)mxMalloc(1*sizeof(int));
        iTabDims[0] = 1;
    }
    else
	{
		iNbDimsOut = iNbDims;
    }
    // Nombre de valeurs (= Produit des N dimensions).
    iNbItems = (int)CDVar->num_vals();
    
    
   
    if (DEBUG)
    {
        mexPrintf("CDVar->type() : %d\n", (int)CDVar->type());
    }
    
    switch ((int)CDVar->type())
    {    
        case CIBDOU_BYTE:
            // Allocation pr�alable du pointeur de valeur de la variable en jeu.
            CDPtrValues = CDVar->values();
            plhs[4] = mxCreateNumericArray(iNbDimsOut, (const mwSize *)iTabDims, mxUINT8_CLASS, mxREAL);
            ucTabData = (unsigned char *)mxGetData(plhs[4]);
            for (iLoop=0; iLoop<iNbItems; iLoop++) 
            {
                ucTabData[iLoop] = (unsigned char)CDPtrValues->as_ncbyte(iLoop);
            }


            // D�sallocation du pointeur de valeurs (probl�me avec Visual C++)
            #ifndef _MSC_VER
                delete CDPtrValues;
            #endif
            break;

         case CIBDOU_CHAR:
            // Allocation pr�alable du pointeur de valeur de la variable en jeu.
            // On force la lecture des Char (normalement 2 octets ous MatLAb)
            // en UINT 8 (1 octet) pour traiter correctement les variables cod�es
            // en Char mais � la signification num�rique (!!! CARAIBES).
            CDPtrValues = CDVar->values();
            plhs[4] = mxCreateNumericArray(iNbDimsOut, (const mwSize *)iTabDims, mxUINT8_CLASS, mxREAL);
            ucTabData = (unsigned char *)mxGetData(plhs[4]);
            for (iLoop=0; iLoop<iNbItems; iLoop++) 
            {
                ucTabData[iLoop] = (unsigned char)CDPtrValues->as_ncbyte(iLoop);
            }


            // D�sallocation du pointeur de valeurs (probl�me avec Visual C++)
            #ifndef _MSC_VER
                delete CDPtrValues;
            #endif
            break;
        /*
        case CIBDOU_CHAR:
            plhs[4] = mxCreateCharArray(iNbDimsOut, iTabDims);
			switch (iNbDims) 
            {
                case 0: // Caract�re unique
					cTabData = (char *)mxCalloc(1, sizeof(char));
                    bRet = CDFile->Getdata(cNameVariable, cTabData);
					mxCharData = (mxChar *)mxGetData(plhs[4]);
                case 1:
					cTabData = (char *)mxCalloc(iTabDims[0], sizeof(char));
                    bRet = CDFile->Getdata(cNameVariable, cTabData, iTabDims[0]);
					mxCharData = (mxChar *)mxGetData(plhs[4]);
					for (iLoop = 0; iLoop < iTabDims[0] ; iLoop++) 
                    {
						*mxCharData = (mxChar)cTabData[iLoop];
						mxCharData++;
					}
                    mxFree(cTabData);
					break;
                case 2:
					// GLUGLUGLU 21/09/06
                    
                    cTabData = (char *)mxCalloc(iTabDims[1]*iTabDims[0], sizeof(char));
					bRet = CDFile->Getdata(cNameVariable, cTabData, iTabDims[0],
                                                                    iTabDims[1]);
					cDummy2 = (char **)mxCalloc(iTabDims[0], sizeof(char*));
					for (iLoop = 0; iLoop < iTabDims[0] ; iLoop++) 
                    {
						cDummy2[iLoop] = (char *)mxCalloc(iTabDims[1]+1, sizeof(char));
						cItem = cDummy2[iLoop];
						for (iLoop2 = 0; iLoop2 < iTabDims[1] ; iLoop2++) 
						{
							if(cTabData[iLoop*iTabDims[1]+iLoop2] == (char) 0) 
							{
								cTabData[iLoop*iTabDims[1]+iLoop2]  = (char) ' ';
							}
							*cItem = cTabData[iLoop*iTabDims[1]+iLoop2];
							cItem++;
						}
						*cItem = '\0';
					}
					plhs[4]= mxCreateCharMatrixFromStrings(iTabDims[0], (const char **)cDummy2); 
					for (iLoop = 0; iLoop < iTabDims[0] ; iLoop++) 
					{
						mxFree(cDummy2[iLoop]);
					}
					cDummy2 = NULL; 
                    break;
                case 3:
					cTabData = (char *)mxGetData(plhs[4]);
                    bRet = CDFile->Getdata(cNameVariable, cTabData, iTabDims[0],
                                                                    iTabDims[1],
                                                                    iTabDims[2]);
                    bRet = 0;
					mexPrintf("La r�cup�ration des cha�nes � nDim> 2 est � finaliser\n");
                    break;
                case 4:
					cTabData = (char *)mxGetData(plhs[4]);
					bRet = CDFile->Getdata(cNameVariable, cTabData, iTabDims[0],
                                                                    iTabDims[1],
                                                                    iTabDims[2],
                                                                    iTabDims[3]);
                    bRet = 0;
					mexPrintf("La r�cup�ration des cha�nes � nDim> 2 est � finaliser\n");
                    break;
            }
			break;
        */
		case CIBDOU_SHORT:
            plhs[4] = mxCreateNumericArray(iNbDimsOut, (const mwSize *)iTabDims, mxINT16_CLASS, mxREAL); 
            siTabData = (short int *)mxGetData(plhs[4]);
            switch (iNbDims) 
            {
                case 0:
                    bRet = CDFile->Getdata(cNameVariable, siTabData);
                    break;
                case 1:
                    bRet = CDFile->Getdata(cNameVariable, siTabData,iTabDims[0]);
                    break;
                case 2:
                    bRet = CDFile->Getdata(cNameVariable, siTabData,iTabDims[0],
                                                                    iTabDims[1]);
                    break;
                case 3:
                    bRet = CDFile->Getdata(cNameVariable, siTabData,iTabDims[0],
                                                                    iTabDims[1],
                                                                    iTabDims[2]);
                    break;
                case 4:
                    bRet = CDFile->Getdata(cNameVariable, siTabData,iTabDims[0],
                                                                    iTabDims[1],
                                                                    iTabDims[2],
                                                                    iTabDims[3]);
                    break;
            }
            break;
        case CIBDOU_LONG:
            plhs[4] = mxCreateNumericArray(iNbDimsOut, (const mwSize *)iTabDims, mxINT32_CLASS, mxREAL);
            lTabData = (long *)mxGetData(plhs[4]);
            switch (iNbDims) 
            {
                case 0:
                    bRet = CDFile->Getdata(cNameVariable, lTabData);
                    break;
                case 1:
                    bRet = CDFile->Getdata(cNameVariable, lTabData,iTabDims[0]);
                    break;
                case 2:
                    bRet = CDFile->Getdata(cNameVariable, lTabData,iTabDims[0],
                                                                    iTabDims[1]);
                    break;
                case 3:
                    bRet = CDFile->Getdata(cNameVariable, lTabData,iTabDims[0],
                                                                    iTabDims[1],
                                                                    iTabDims[2]);
                    break;
                case 4:
                    bRet = CDFile->Getdata(cNameVariable, lTabData,iTabDims[0],
                                                                    iTabDims[1],
                                                                    iTabDims[2],
                                                                    iTabDims[3]);
                    break;
            }
            break;
        case CIBDOU_FLOAT:
            plhs[4] = mxCreateNumericArray(iNbDimsOut, (const mwSize *)iTabDims, mxSINGLE_CLASS, mxREAL);
            fTabData = (float *)mxGetData(plhs[4]);
            switch (iNbDims) 
            {
                case 0:
                    bRet = CDFile->Getdata(cNameVariable, fTabData);
                    break;
                case 1:
                    bRet = CDFile->Getdata(cNameVariable, fTabData, iTabDims[0]);
                    break;
                case 2:
                    bRet = CDFile->Getdata(cNameVariable, fTabData,	iTabDims[0],
                                                                    iTabDims[1]);
                    break;
                case 3:
                    bRet = CDFile->Getdata(cNameVariable, fTabData,	iTabDims[0],
                                                                    iTabDims[1],
                                                                    iTabDims[2]);
                    break;
                case 4:
                    bRet = CDFile->Getdata(cNameVariable, fTabData,	iTabDims[0],
                                                                    iTabDims[1],
                                                                    iTabDims[2],
                                                                    iTabDims[3]);
                    break;
            }
            break;
        case CIBDOU_DOUBLE:
            plhs[4] = mxCreateNumericArray(iNbDimsOut, (const mwSize *)iTabDims, mxDOUBLE_CLASS, mxREAL);
            dTabData = (double *)mxGetData(plhs[4]);
            switch (iNbDims) 
            {
                case 0:
                    bRet = CDFile->Getdata(cNameVariable, dTabData);
                    break;
                case 1:
                    bRet = CDFile->Getdata(cNameVariable, dTabData, iTabDims[0]);
                    break;
                case 2:
                    bRet = CDFile->Getdata(cNameVariable, dTabData,	iTabDims[0],
                                                                    iTabDims[1]);
                    break;
                case 3:
                    bRet = CDFile->Getdata(cNameVariable, dTabData,	iTabDims[0],
                                                                    iTabDims[1],
                                                                    iTabDims[2]);
                    break;
                case 4:
                    bRet = CDFile->Getdata(cNameVariable, dTabData,	iTabDims[0],
                                                                    iTabDims[1],
                                                                    iTabDims[2],
                                                                    iTabDims[3]);
                    break;
            }
            break; 
       default:
            sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(  CIBDOU_E_TYPE_UNTREATED, 
                                                            __FILE__,
                                                            __LINE__,
                                                             ""));
            mexErrMsgTxt(cDummy);
            break;
                      
    }            

    mxFree(iTabDims);
    mxFree(cNameVariable);
    
    
	// D�tection des fuites M�moire sous Visual C++
    /*
	#ifdef _MSC_VER
		_CrtDumpMemoryLeaks();
	#endif
    */
	return;
    
    
} // mexCD_GetData
