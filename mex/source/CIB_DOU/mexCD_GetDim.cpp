// Fichier mexCD_GetDim.cpp
//
// SYNTAX :
//   mexCD_GetDim(idFile, indexDim)
//   mexCD_GetDim(idFile, nameDim)
//   mexCD_GetDim(idFile, nameVar, indexDim)
//   mexCD_GetDim(idFile, indexVar, indexDim)
//
// INPUT PARAMETERS :
//   idFile     : identifiant du fichier (d�j� ouvert)
//   indexVar	: indice de la variable.
//   nameVar	: nom de la variable.
//   indexDim	: indice de la dimension.
//   nameDim	: nom de la dimension.
//
// COMPIL :
//   S�lectionner le compilateur de fa�on ad hoc : gcc ou Visual C++(=MSVC).
//   compil_CIB_DOU('compiler', 'MSVC', 'option', '-v ', 'filesource',{'all'})
//   ou
//   compil_CIB_DOU('compiler', 'MSVC', 'option', '-v ', 'filesource',{'nom_du_source.cpp'})
//
// SEE ALSO : Authors
// AUTHORS  : GLU
// VERSION  : $Id: .c,v 1.0 2006\01\25 08:23:13 Gallou Exp Gallou $
// ----------------------------------------------------------------------------

#include <mex.h>
#include <mat.h>
#include "CIB_DOU_File.h"

extern "C" { 
    #include "CD_Utils.h"
} 

#define DEBUG 0

void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
int         iNbItems,
            iNumber,
            iIndexVariable,
            iIndexDim;

char        cDummy[500],
            *cNameVariable,
            *cNameDim;

CIB_DOU_File *CDFile;   // Cr�ation de la variable d'acc�s au fichier;
NcDim        *CDDim;	// Cr�ation de la variable d'acc�s aux dimensions;

    if (DEBUG) 
    {
        mexPrintf("Fichier %s, nrhs : %d\n", __FILE__, nrhs);
    }

    switch (nrhs)  
    {
        case 2:
            // R�cup�ration du pointeur de fichier (OBLIGATOIRE en 1er 
            // argument).
            CDFile = (CIB_DOU_File *)(long)mxGetScalar(prhs[0]);

            // R�cup�ration du 2�me argument.
            if (mxGetClassID(prhs[1]) == mxCHAR_CLASS)
            {
                // Longueur du nom de la variable pass�e en argument.
                iNbItems = mxGetNumberOfElements(prhs[1]);    
                // R�cup�ration du nom.
                cNameDim=(char *)mxCalloc(iNbItems+1,sizeof(char));
                mxGetString(prhs[1],cNameDim,iNbItems+1);
                CDDim = CDFile->Get_dim(cNameDim);
				iNumber = (int)(CDDim->size());
                mxFree(cNameDim);
            }
            else if (mxGetClassID(prhs[1]) == mxDOUBLE_CLASS)
            {
                iIndexDim = (int)mxGetScalar(prhs[1]) - 1;
                CDDim = CDFile->Get_dim(iIndexDim);
				iNumber = (int)(CDDim->size());
            }
            else
            {
                sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(  CIBDOU_E_INPUT_TYPE, 
                                                                __FILE__,
                                                                __LINE__,
                                                                ""));
                mexErrMsgTxt(cDummy);
            }
            // Appel de la m�thode incluse dans la librairie CIB_DOU.
            if (DEBUG)
            {
                mexPrintf("Valeur de la Dimension \t:\t%d\n", iNumber);
            }
            break;
        case 3:
            // R�cup�ration du pointeur de fichier (OBLIGATOIRE en 1er 
            // argument).
            CDFile = (CIB_DOU_File *)(long)mxGetScalar(prhs[0]);

            // R�cup�ration du 2�me argument.
            if (mxGetClassID(prhs[1]) == mxCHAR_CLASS &&
                mxGetClassID(prhs[2]) == mxDOUBLE_CLASS)
            {
                // Longueur du nom de la variable pass�e en argument.
                iNbItems = mxGetNumberOfElements(prhs[1]);    
                // R�cup�ration du nom.
                cNameVariable=(char *)mxCalloc(iNbItems+1,sizeof(char));
                mxGetString(prhs[1],cNameVariable,iNbItems+1);
                // R�cup�ration de l'indice de la dimension.
                iIndexDim = (int)mxGetScalar(prhs[2]) - 1;
                // Affectation de la variable DIM.
                CDDim = CDFile->Get_dim(cNameVariable, iIndexDim);
				iNumber = (int)(CDDim->size());
                mxFree(cNameVariable);
            }
            else if (mxGetClassID(prhs[1]) == mxDOUBLE_CLASS &&
                    mxGetClassID(prhs[2]) == mxDOUBLE_CLASS)
            {
                iIndexVariable = (int)mxGetScalar(prhs[1]) - 1;
                // R�cup�ration de l'indice de la dimension.
                iIndexDim = (int)mxGetScalar(prhs[2]) - 1;
                CDDim = CDFile->Get_dim(iIndexVariable, iIndexDim);
				iNumber = (int)(CDDim->size());
            }
            else
            {
                sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(  CIBDOU_E_INPUT_TYPE, 
                                                                __FILE__,
                                                                __LINE__,
                                                                ""));
                mexErrMsgTxt(cDummy);
            }
            // Appel de la m�thode incluse dans la librairie CIB_DOU.
            if (DEBUG)
            {
                mexPrintf("Valeur de la Dimension \t:\t%d\n", iNumber);
            }
            break;
        default:
            sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(  CIBDOU_E_INPUT_NUMBER, 
                                                            __FILE__,
                                                            __LINE__,
                                                            ""));
            mexErrMsgTxt(cDummy);
            break;
    }
    
            
    if (DEBUG)
    {
        mexPrintf("Adresse de CDFile\t:\t%d\n",CDFile);
        mexPrintf("iNumber\t:\t%d\n",iNumber);
    }
    
    
    // Rapatriement de la variable de sortie.
    plhs[0] = mxCreateDoubleMatrix(1,1, mxREAL);    
    *mxGetPr(plhs[0]) = iNumber;
        
    return;
    
    
} // mexCD_GetDim
