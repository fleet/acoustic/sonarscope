// Fichier mexCD_GetDimSize.cpp
//
// SYNTAX :
//   mexCD_GetDimSize(idFile, indexDim)
//   mexCD_GetDimSize(idFile, nameDim)
//
// INPUT PARAMETERS :
//   idFile     : identifiant du fichier (d�j� ouvert)
//   indexDim	: indice de la dimension.
//   nameDim	: nom de la dimension.
//
// COMPIL :
//   S�lectionner le compilateur de fa�on ad hoc : gcc ou Visual C++(=MSVC).
//   compil_CIB_DOU('compiler', 'MSVC', 'option', '-v ', 'filesource',{'all'})
//   ou
//   compil_CIB_DOU('compiler', 'MSVC', 'option', '-v ', 'filesource',{'nom_du_source.cpp'})
//
// SEE ALSO : Authors
// AUTHORS  : GLU
// VERSION  : $Id: .c,v 1.0 2006\01\25 08:23:13 Gallou Exp Gallou $
// ----------------------------------------------------------------------------

#include <mex.h>
#include <mat.h>
#include "CIB_DOU_File.h"

extern "C" { 
    #include "CD_Utils.h"
} 

#define DEBUG 0

void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
int         iDummy,
            iNumber;

char        cDummy[500];      
char        *cNameVariable;

CIB_DOU_File* idFile;   // Cr�ation de la variable d'acc�s au fichier;

    if (DEBUG) 
    {
        mexPrintf("Fichier %s, nrhs : %d\n", __FILE__, nrhs);
    }
    switch (nrhs)  
    {
        case 2:
            // R�cup�ration du pointeur de fichier (OBLIGATOIRE en 1er 
            // argument).
            idFile = (CIB_DOU_File *)(long)mxGetScalar(prhs[0]);

            if (mxGetClassID(prhs[1]) == mxCHAR_CLASS)
            {
                // Longueur du nom de la variable pass�e en argument.
                iDummy = mxGetN(prhs[1]);
                // R�cup�ration du nom.
                cNameVariable=(char *)mxCalloc(iDummy+1,sizeof(char));
                mxGetString(prhs[1],cNameVariable,iDummy+1);
                if (DEBUG) 
                {
                    mexPrintf("Nom de la variable : %s\n", cNameVariable);
                }
                iNumber = (int)idFile->Get_dim_size(cNameVariable);
            }
            else
            {
                sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(  CIBDOU_E_INPUT_TYPE, 
                                                                __FILE__,
                                                                __LINE__,
                                                                ""));
                mexErrMsgTxt(cDummy);
            }
            break;
        
        default:
            sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(  CIBDOU_E_INPUT_NUMBER, 
                                                            __FILE__,
                                                            __LINE__,
                                                            ""));
            mexErrMsgTxt(cDummy);
            break;
    }
    if (DEBUG)
    {
        mexPrintf("Adresse de idFile\t:\t%d\n",idFile);
        mexPrintf("Taille de la dimension %s\t\t\t:\t%d\n", cNameVariable, iNumber);
    }
    
    // Rapatriement de la variable de sortie.
    plhs[0] = mxCreateDoubleMatrix(1,1, mxREAL);
    *mxGetPr(plhs[0]) = (int)iNumber;
    
    mxFree(cNameVariable);
      
    return;
    
    
} // mexCD_GetDimSize
