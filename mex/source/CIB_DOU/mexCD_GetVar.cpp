// Fichier mexCD_GetVar.cpp
//
// SYNTAX :
//   mexCD_GetVar(CDFile, indexVar)
//   mexCD_GetVar(CDFile, nameVar)
//
// INPUT PARAMETERS :
//   idFile     : identifiant du fichier (d�j� ouvert)
//   indexVar	: indice de la variable.
//   nameVar	: nom de la variable.
//
// COMPIL :
//   S�lectionner le compilateur de fa�on ad hoc : gcc ou Visual C++(=MSVC).
//   compil_CIB_DOU('compiler', 'MSVC', 'option', '-v ', 'filesource',{'all'})
//   ou
//   compil_CIB_DOU('compiler', 'MSVC', 'option', '-v ', 'filesource',{'nom_du_source.cpp'})
//
// SEE ALSO : Authors
// AUTHORS  : GLU
// VERSION  : $Id: .c,v 1.0 2006\01\25 08:23:13 Gallou Exp Gallou $
// ----------------------------------------------------------------------------

#include <mex.h>
#include <mat.h>
#include "CIB_DOU_File.h"

extern "C" { 
    #include "CD_Utils.h"
} 

#define DEBUG 0


void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
int             iNbItems,
                iIndexVariable,
                iLoop,
                iNbDims,
                *iTabDims;

double          *dDataOut;
float           *fDataOut;
long            *lDataOut;
short           *sDataOut;
unsigned char   *ucDataOut;
char            *cNameVariable,
                cDummy[500];

mxChar          *mxCharData;

BOOL            bRet;

size_t          iSizebuf;

CIB_DOU_File    *CDFile;        // Cr�ation de la variable d'acc�s au fichier;
NcVar           *CDVar;         // Cr�ation du containeur des variables;
NcValues        *CDPtrValues;   // Cr�ation du containeur de valeurs des variables;

    if (DEBUG) 
    {
        mexPrintf("Fichier %s, nrhs : %d\n", __FILE__, nrhs);
    }
    switch (nrhs)  
    {
        case 2:
            // R�cup�ration du pointeur de fichier (OBLIGATOIRE en 1er 
            // argument).
            CDFile = (CIB_DOU_File *)(long)mxGetScalar(prhs[0]);

            // R�cup�ration du 2�me argument.
            if (mxGetClassID(prhs[1]) == mxCHAR_CLASS)
            {
                // Longueur du nom de la variable pass�e en argument.
                iNbItems = mxGetNumberOfElements(prhs[1]);    
                // R�cup�ration du nom de la variable.
                cNameVariable=(char *)mxCalloc(iNbItems+1,sizeof(char));
                mxGetString(prhs[1],cNameVariable,iNbItems+1);
                
                // V�rification de l'existence de la variable
                bRet = CDFile->Exists_var(cNameVariable);
                if (bRet == FALSE)
                {
                    sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(  CIBDOU_E_VAR_NOTEXIST, 
                                                                    __FILE__,
                                                                    __LINE__,
                                                                    cNameVariable));
                    mexErrMsgTxt(cDummy);
                }
                // Affectation de la variable VAR.
                CDVar = CDFile->Get_var(cNameVariable);
                mxFree(cNameVariable);
            }
            else if (mxGetClassID(prhs[1]) == mxDOUBLE_CLASS)
            {
                iIndexVariable = (int)mxGetScalar(prhs[1]) - 1;
                // V�rification de l'existence de la variable (ne peut �tre �gal
                // au nombre de variables, cf. ligne au-dessus).
                if (iIndexVariable >= CDFile->Num_vars())
                {
                    sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(  CIBDOU_E_VAR_NOTEXIST, 
                                                                    __FILE__,
                                                                    __LINE__,
                                                                    ""));
                    mexErrMsgTxt(cDummy);
                }

                // Affectation de la variable VAR.
                CDVar = CDFile->Get_var(iIndexVariable);
            }
            else
            {
                sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(  CIBDOU_E_INPUT_TYPE, 
                                                                __FILE__,
                                                                __LINE__,
                                                                ""));
                mexErrMsgTxt(cDummy);
            }
            break;
        default:
            sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(  CIBDOU_E_INPUT_NUMBER, 
                                                            __FILE__,
                                                            __LINE__,
                                                            ""));
            mexErrMsgTxt(cDummy);
            break;
    }
    
            
    if (DEBUG)
    {
        mexPrintf("Adresse de CDFile\t:\t%d\n",CDFile);
    }
    
    // Rapatriement des variables de sortie.
    // Nom de l'attribut
    plhs[0] = mxCreateString(CDVar->name());
    
    // Type de l'attribut    
    iSizebuf = iCD_UtilStrType(&cDummy[0], (int)CDVar->type());
    plhs[1] = mxCreateString(cDummy);

    // Nombre de dimensions associ�es
    iNbDims = (int)CDVar->num_dims();
    
    plhs[2] = mxCreateDoubleMatrix(1,1, mxREAL);
    *mxGetPr(plhs[2]) = (int)iNbDims;
    
    // Valeurs des dimensions (nombre et d�tail)
    plhs[3] = mxCreateDoubleMatrix(iNbDims,1, mxREAL);
    dDataOut = mxGetPr(plhs[3]);
    if (iNbDims == 0)
	{
        iNbDims = 1;
		iTabDims = (int *)malloc(iNbDims*sizeof(int));
        iTabDims[0] = 1;
	}
    else    // For�age des dimensions.
    {
		// Taille totale des donn�es
		iNbItems = 1;
		iTabDims = (int *)malloc(iNbDims*sizeof(int));
		for (iLoop=0; iLoop<iNbDims; iLoop++) 
		{
            *(dDataOut+iLoop) = (double)(CDVar->get_dim(iLoop)->size());
            iTabDims[iLoop] = (int)(CDVar->get_dim(iLoop)->size());
		}
    }

    // Nombre de valeurs (= Produit des N dimensions).
    iNbItems = (int)CDVar->num_vals();
    
    // Allocation pr�alable du pointeur de valeur de la variable en jeu.
    CDPtrValues = CDVar->values();

    if (DEBUG)
    {
        mexPrintf("Type de la variable \t:\t%d\n",(int)CDVar->type());
    }
   
    switch ((int)CDVar->type())
    {    
        case CIBDOU_BYTE:
            plhs[4] = mxCreateNumericArray(iNbDims, (const mwSize *)iTabDims, mxUINT8_CLASS, mxREAL);
            ucDataOut = (unsigned char *)mxGetData(plhs[4]);
            for (iLoop=0; iLoop<iNbItems; iLoop++) 
            {
                ucDataOut[iLoop] = (unsigned char)CDPtrValues->as_ncbyte(iLoop);
            }
            break;
        case CIBDOU_CHAR:
            //sprintf(&cDummy[0], "%s", CDPtrValues->as_string(0));
            //plhs[4] = mxCreateString(cDummy);
            plhs[4] = mxCreateCharArray(iNbDims, (const mwSize *)iTabDims);
            mxCharData = (mxChar *)mxGetData(plhs[4]);
            for (iLoop=0; iLoop<iNbItems; iLoop++) 
            {
                mxCharData[iLoop] = (mxChar)CDPtrValues->as_char(iLoop);
            }
            break;
        case CIBDOU_SHORT:
            plhs[4] = mxCreateNumericArray(iNbDims, (const mwSize *)iTabDims, mxINT16_CLASS, mxREAL);
            sDataOut = (short *)mxGetData(plhs[4]);
            for (iLoop=0; iLoop<iNbItems; iLoop++) 
            {
                *(sDataOut+iLoop) = (short)CDPtrValues->as_short(iLoop);
            }
            break;
        case CIBDOU_LONG:
            plhs[4] = mxCreateNumericArray(iNbDims, (const mwSize *)iTabDims, mxINT32_CLASS, mxREAL);
            lDataOut = (long *)mxGetData(plhs[4]);
            //dDataOut = mxGetPr(plhs[4]);
            for (iLoop=0; iLoop<iNbItems; iLoop++) 
            {
                *(lDataOut+iLoop) = (long)CDPtrValues->as_long(iLoop);
            }
            break;
        case CIBDOU_FLOAT:
            plhs[4] = mxCreateNumericArray(iNbDims, (const mwSize *)iTabDims, mxSINGLE_CLASS, mxREAL);
            fDataOut = (float *)mxGetData(plhs[4]);
            for (iLoop=0; iLoop<iNbItems; iLoop++) 
            {
                *(fDataOut+iLoop) = (float)CDPtrValues->as_float(iLoop);
            }
            break;
        case CIBDOU_DOUBLE:
            plhs[4] = mxCreateNumericArray(iNbDims, (const mwSize *)iTabDims, mxDOUBLE_CLASS, mxREAL);
            dDataOut = (double *)mxGetData(plhs[4]);
            for (iLoop=0; iLoop<iNbItems; iLoop++) 
            {
                *(dDataOut+iLoop) = (double)CDPtrValues->as_double(iLoop);
            }
            break;
        
       default:
            break;
                      
    }            

    // D�sallocation du pointeur de valeurs (probl�me avec Visual C++)
    #ifndef _MSC_VER
        delete CDPtrValues;
    #endif
    mxFree(iTabDims);
    
    return;
    
    
} // mexCD_GetVar
