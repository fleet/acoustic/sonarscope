// Fichier mexCD_GetVarType.cpp
//
// SYNTAX :
//   mexCD_GetVarType(idFile, nameVar)
//
// INPUT PARAMETERS :
//   idFile     : identifiant du fichier (d�j� ouvert)
//   nameVar	: nom de la variable.
//
// COMPIL :
//   S�lectionner le compilateur de fa�on ad hoc : gcc ou Visual C++(=MSVC).
//   compil_CIB_DOU('compiler', 'MSVC', 'option', '-v ', 'filesource',{'all'})
//   ou
//   compil_CIB_DOU('compiler', 'MSVC', 'option', '-v ', 'filesource',{'nom_du_source.cpp'})
//
// SEE ALSO : Authors
// AUTHORS  : GLU
// VERSION  : $Id: .c,v 1.0 2006\01\25 08:23:13 Gallou Exp Gallou $
// ----------------------------------------------------------------------------

#include <mex.h>
#include <mat.h>
#include "CIB_DOU_File.h"

extern "C" { 
    #include "CD_Utils.h"
} 


#define DEBUG 0

void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
int         iDummy,
            iNumber,
            iIndexVariable,
            iLoop;

double      *dDummy;

char        *cNameVariable,
            *cNomFic,
            cDummy[500];

size_t      iSizebuf;
    
CIB_DOU_File    *idFile;        // Cr�ation de la variable d'acc�s au fichier;
CIB_DOU_TYPE    iType;

    if (DEBUG) 
    {
        mexPrintf("Fichier %s, nrhs : %d\n", __FILE__, nrhs);
    }
    switch (nrhs)  
    {
        case 2:
            // R�cup�ration du pointeur de fichier (OBLIGATOIRE en 1er 
            // argument).
            idFile = (CIB_DOU_File *)(long)mxGetScalar(prhs[0]);

            // R�cup�ration du 2�me argument.
            if (mxGetClassID(prhs[1]) == mxCHAR_CLASS)
            {
                // Longueur du nom de la variable pass�e en argument.
                iDummy = mxGetN(prhs[1]);    
                // R�cup�ration du nom de fichier.
                cNameVariable=(char *)mxCalloc(iDummy+1,sizeof(char));
                mxGetString(prhs[1],cNameVariable,iDummy+1);
                iType = idFile->GetVarType(cNameVariable);
                mxFree(cNameVariable);
            }
            else
            {
                sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(  CIBDOU_E_INPUT_TYPE, 
                                                                __FILE__,
                                                                __LINE__,
                                                                ""));
                mexErrMsgTxt(cDummy);
            }
            // Appel de la m�thode incluse dans la librairie CIB_DOU.
            if (DEBUG)
            {
                mexPrintf("Nb d'attributs de la variable\t:\t%d\n", iNumber);
            }
            break;
        default:
            sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(  CIBDOU_E_INPUT_NUMBER, 
                                                            __FILE__,
                                                            __LINE__,
                                                            ""));
            mexErrMsgTxt(cDummy);
            break;
    }
    
            
    if (DEBUG)
    {
        mexPrintf("Adresse de idFile\t:\t%d\n",idFile);
    }
    
    // Rapatriement des variables de sortie.    
    // Type de la variable    
    iSizebuf = iCD_UtilStrType(&cDummy[0], iType);
    plhs[0] = mxCreateString(cDummy);
    
    return;
    
} // Fin de mexFunction
