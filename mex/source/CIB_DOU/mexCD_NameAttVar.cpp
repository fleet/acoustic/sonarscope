// Fichier mexCD_NameAttVar.cpp
//
// SYNTAX :
//   mexCD_NameAttVar(idFile, indexVar, indexAtt)
//   mexCD_NameAttVar(idFile, nameVar, indexAtt)
//
// INPUT PARAMETERS :
//   idFile     : identifiant du fichier (d�j� ouvert)
//   indexVar	: indice de la variable.
//   nameVar	: nom de la variable.
//   indexAtt	: indice de l'attribut.
//
// COMPIL :
//   S�lectionner le compilateur de fa�on ad hoc : gcc ou Visual C++(=MSVC).
//   compil_CIB_DOU('compiler', 'MSVC', 'option', '-v ', 'filesource',{'all'})
//   ou
//   compil_CIB_DOU('compiler', 'MSVC', 'option', '-v ', 'filesource',{'nom_du_source.cpp'})
// 
// SEE ALSO : Authors
// AUTHORS  : GLU
// VERSION  : $Id: .c,v 1.0 2006\01\25 08:23:13 Gallou Exp Gallou $
// ----------------------------------------------------------------------------

#include <mex.h>
#include <mat.h>
#include "CIB_DOU_File.h"
extern "C" { 
    #include "CD_Utils.h"
} 

#define DEBUG 0


extern "C" {
void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
int         iNbItems,
            iIndexVariable,
            iIndexVarAtt;

char        cDummy[100];      
char        *cNameVariable;

BOOL        bRet;

CIB_STC_String  CDString_NameAttVar;
CIB_DOU_File    *CDFile;   // Cr�ation de la variable d'acc�s au fichier;

    if (DEBUG) 
    {
        mexPrintf("Fichier %s, nrhs : %d\n", __FILE__, nrhs);
    }

    switch (nrhs)  
    {
        case 3:
            // R�cup�ration du pointeur de fichier (OBLIGATOIRE en 1er 
            // argument).
            CDFile = (CIB_DOU_File *)(long)mxGetScalar(prhs[0]);
            if (mxGetClassID(prhs[1]) == mxDOUBLE_CLASS && 
                mxGetClassID(prhs[2]) == mxDOUBLE_CLASS)
            {
                iIndexVariable = (int)mxGetScalar(prhs[1]) - 1;
                iIndexVarAtt = (int)mxGetScalar(prhs[2]) - 1;
                CDString_NameAttVar = CDFile->Name_att_var(iIndexVariable, iIndexVarAtt);
                // V�rification de l'existence de la variable
                bRet = CDFile->Exists_att(CDString_NameAttVar);
                if (bRet == FALSE)
                {
                    sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(  CIBDOU_E_ATT_NOTEXIST, 
                                                                    __FILE__,
                                                                    __LINE__,
                                                                    CDString_NameAttVar));
                    mexErrMsgTxt(cDummy);
                }
                mxFree(cNameVariable);
            }
            else if (mxGetClassID(prhs[1]) == mxCHAR_CLASS &&
                     mxGetClassID(prhs[2]) == mxDOUBLE_CLASS)
            {
                iIndexVarAtt = (int)mxGetScalar(prhs[2]) - 1;
                iNbItems = mxGetNumberOfElements(prhs[1]);
                // R�cup�ration du nom de la variable.
                cNameVariable=(char *)mxCalloc(iNbItems+1,sizeof(char));
                mxGetString(prhs[1],cNameVariable,iNbItems+1);
                CDString_NameAttVar = CDFile->Name_att_var(cNameVariable, iIndexVarAtt);
                // V�rification de l'existence de la variable
                bRet = CDFile->Exists_att(CDString_NameAttVar);
                if (bRet == FALSE)
                {
                    sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(  CIBDOU_E_ATT_NOTEXIST, 
                                                                    __FILE__,
                                                                    __LINE__,
                                                                    CDString_NameAttVar));
                    mexErrMsgTxt(cDummy);
                }
                mxFree(cNameVariable);
           }
            else
            {
                sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(  CIBDOU_E_INPUT_TYPE, 
                                                                __FILE__,
                                                                __LINE__,
                                                                ""));
                mexErrMsgTxt(cDummy);
            }
            // Appel de la m�thode incluse dans la librairie CIB_DOU.
            if (DEBUG)
            {
              mexPrintf("nrhs : %d\n", nrhs);
              mexPrintf("Nom de la dimension %d de la variable\t:\t%s\n", iIndexVariable, (const char*)CDString_NameAttVar);
            }
            break;
        default:
            sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(  CIBDOU_E_INPUT_NUMBER, 
                                                            __FILE__,
                                                            __LINE__,
                                                            ""));
            mexErrMsgTxt(cDummy);
            break;
    }
    
            
    if (DEBUG)
    {
        mexPrintf("Adresse de CDFile\t:\t%d\n",CDFile);
    }
    
    
    // Rapatriement de la variable de sortie.
    plhs[0] = mxCreateString((const char *)CDString_NameAttVar);
        
    return;
    
    
} // mexCD_NameAttVar

} // Fin de la d�claration des fonctions Extern.
