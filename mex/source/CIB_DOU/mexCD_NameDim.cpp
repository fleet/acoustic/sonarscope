// Fichier mexCD_NameDim.cpp
//
// SYNTAX :
//   mexCD_NameDim(idFile, indexDim)
//
// INPUT PARAMETERS :
//   idFile     : identifiant du fichier (d�j� ouvert)
//   indexDim	: indice de la dimension.
//
// COMPIL :
//   S�lectionner le compilateur de fa�on ad hoc : gcc ou Visual C++(=MSVC).
//   compil_CIB_DOU('compiler', 'MSVC', 'option', '-v ', 'filesource',{'all'})
//   ou
//   compil_CIB_DOU('compiler', 'MSVC', 'option', '-v ', 'filesource',{'nom_du_source.cpp'})
//
// SEE ALSO : Authors
// AUTHORS  : GLU
// VERSION  : $Id: .c,v 1.0 2006\01\25 08:23:13 Gallou Exp Gallou $
// ----------------------------------------------------------------------------

#include <mex.h>
#include <mat.h>
#include "CIB_DOU_File.h"
extern "C" { 
    #include "CD_Utils.h"
} 


#define DEBUG 0


extern "C" {
void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{ 
int         iDummy,
            iIndexVariable;

char        cDummy[100];      
const char  *cNameDim;

CIB_DOU_File    *CDFile;   // Cr�ation de la variable d'acc�s au fichier;

    if (DEBUG) 
    {
        mexPrintf("Fichier %s, nrhs : %d\n", __FILE__, nrhs);
    }

    switch (nrhs)  
    {
        case 2:
            // R�cup�ration du pointeur de fichier (OBLIGATOIRE en 1er 
            // argument).
            CDFile = (CIB_DOU_File *)(long)mxGetScalar(prhs[0]);
            if (mxGetClassID(prhs[1]) == mxDOUBLE_CLASS)
            {
                iIndexVariable = (int)mxGetScalar(prhs[1]) - 1;
                cNameDim = CDFile->Name_dim(iIndexVariable);
            }
            else
            {
                sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(  CIBDOU_E_INPUT_TYPE, 
                                                                __FILE__,
                                                                __LINE__,
                                                                ""));
                mexErrMsgTxt(cDummy);
            }
            // Appel de la m�thode incluse dans la librairie CIB_DOU.
            if (DEBUG)
            {
            }
            break;
        default:
            sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(  CIBDOU_E_INPUT_NUMBER, 
                                                            __FILE__,
                                                            __LINE__,
                                                            ""));
            mexErrMsgTxt(cDummy);
            break;
    }
    
            
    if (DEBUG)
    {
        mexPrintf("Index de la dimension %d\n", iIndexVariable);
        mexPrintf("Nom de la dimension %d de la variable\t:\t%s\n", iIndexVariable, cNameDim);
        mexPrintf("Adresse de CDFile\t:\t%d\n",CDFile);
    }
    
    
    // Rapatriement de la variable de sortie.
    plhs[0] = mxCreateString(cNameDim);
        
	
    return;
    
    
} // mexCD_NameDim

} // Fin de la d�claration des fonctions Extern.
