// Fichier mexCD_NbAtt.cpp
//
// SYNTAX :
//   mexCD_NbAtt(idFile)
//
// INPUT PARAMETERS :
//   idFile     : identifiant du fichier (d�j� ouvert)
//
// COMPIL :
//   S�lectionner le compilateur de fa�on ad hoc : gcc ou Visual C++(=MSVC).
//   compil_CIB_DOU('compiler', 'MSVC', 'option', '-v ', 'filesource',{'all'})
//   ou
//   compil_CIB_DOU('compiler', 'MSVC', 'option', '-v ', 'filesource',{'nom_du_source.cpp'})
//
// SEE ALSO : Authors
// AUTHORS  : GLU
// VERSION  : $Id: .c,v 1.0 2006\01\25 08:23:13 Gallou Exp Gallou $
// ----------------------------------------------------------------------------

#include <mex.h>
#include <mat.h>
#include "CIB_DOU_File.h"
extern "C" { 
    #include "CD_Utils.h"
} 


#define DEBUG 0


void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
int     iNumber;

char    cDummy[500];

    if (nrhs != 1) 
    {
        sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(  CIBDOU_E_INPUT_NUMBER, 
                                                __FILE__,
                                                __LINE__,
                                                ""));
        mexErrMsgTxt(cDummy);
    }
    // Cr�ation de la variable d'acc�s au fichier;
    CIB_DOU_File* idFile;
    // Assign pointers to each output and input
    idFile = (CIB_DOU_File *)(long)mxGetScalar(prhs[0]);
    if (DEBUG)
    {
        mexPrintf("Adresse de idFile\t:\t%d\n",idFile);
    }
    
    // Appel de la m�thode incluse dans la librairie CIB_DOU.
    iNumber = idFile->Num_atts();
    if (DEBUG)
    {
        mexPrintf("Nb Attributs\t\t\t:\t%d\n", iNumber);
    }
    
    // Rapatriement de la variable de sortie.
    plhs[0] = mxCreateDoubleMatrix(1,1, mxREAL);
    *mxGetPr(plhs[0]) = (int)iNumber;
        
    return;
    
    
} // mexCD_NbAtt
