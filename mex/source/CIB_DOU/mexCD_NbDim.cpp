// Fichier mexCD_NbDim.cpp
//
// SYNTAX :
//   mexCD_NbDim(idFile)
//   mexCD_NbDim(idFile, index_var)
//   mexCD_NbDim(idFile, name_var)
// 
// INPUT PARAMETERS :
//   idFile     : identifiant du fichier (d�j� ouvert)
//   index_var	: index de la variable.
//   name_var	: nom de la variable.
//
// COMPIL :
//   S�lectionner le compilateur de fa�on ad hoc: gcc ou Visual C++(=MSVC).
//   compil_CIB_DOU('compiler', 'MSVC', 'option', '-v ', 'filesource',{'all'})
//   ou
//   compil_CIB_DOU('compiler', 'MSVC', 'option', '-v ', 'filesource',{'nom_du_source.cpp'})
//
// SEE ALSO : Authors
// AUTHORS  : GLU
// VERSION  : $Id: .c,v 1.0 2006\01\25 08:23:13 Gallou Exp Gallou $
// ----------------------------------------------------------------------------

#include <mex.h>
#include <mat.h>
#include "CIB_DOU_File.h"

extern "C" { 
    #include "CD_Utils.h"
} 

#define DEBUG 0

void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
int         iDummy,
            iNumber,
            iIndexVariable;

char        cDummy[500],
            *cNameVariable;

CIB_DOU_File *CDFile;   // Cr�ation de la variable d'acc�s au fichier;

    if (DEBUG) 
    {
        mexPrintf("Fichier %s, nrhs : %d\n", __FILE__, nrhs);
    }

    switch (nrhs)  
    {
        case 1:
            // Recuperation du pointeur de fichier.
            CDFile = (CIB_DOU_File *)(long)mxGetScalar(prhs[0]);
            iNumber = CDFile->Num_dims();
            // Appel de la m�thode incluse dans la librairie CIB_DOU.
            if (DEBUG)
            {
                mexPrintf("Nb Dimensions du fichier\t\t\t:\t%d\n", iNumber);
            }
            break;
        case 2:
            // R�cup�ration du pointeur de fichier (OBLIGATOIRE en 1er 
            // argument).
            CDFile = (CIB_DOU_File *)(long)mxGetScalar(prhs[0]);

            // R�cup�ration du 2�me argument.
            if (mxGetClassID(prhs[1]) == mxCHAR_CLASS)
            {
                // Longueur du nom de la variable pass�e en argument.
                iDummy = mxGetN(prhs[1]);    
                // R�cup�ration du nom.
                cNameVariable=(char *)mxCalloc(iDummy+1,sizeof(char));
                mxGetString(prhs[1],cNameVariable,iDummy+1);
                iNumber = CDFile->Num_dims(cNameVariable);
                mxFree(cNameVariable);
            }
            else if (mxGetClassID(prhs[1]) == mxDOUBLE_CLASS)
            {
                iIndexVariable = (int)mxGetScalar(prhs[1]) - 1;
                iNumber = CDFile->Num_dims(iIndexVariable);
            }
            else
            {
                sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(  CIBDOU_E_INPUT_TYPE, 
                                                                __FILE__,
                                                                __LINE__,
                                                                ""));
                mexErrMsgTxt(cDummy);
            }
            // Appel de la m�thode incluse dans la librairie CIB_DOU.
            if (DEBUG)
            {
                mexPrintf("Nb Dimensions de la variable\t:\t%d\n", iNumber);
            }
            break;
        default:
            sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(  CIBDOU_E_INPUT_NUMBER, 
                                                            __FILE__,
                                                            __LINE__,
                                                            ""));
            mexErrMsgTxt(cDummy);
            break;
    }
    
            
    if (DEBUG)
    {
        mexPrintf("Adresse de CDFile\t:\t%d\n",CDFile);
    }
    
    
    // Rapatriement de la variable de sortie.
    plhs[0] = mxCreateDoubleMatrix(1,1, mxREAL);
    *mxGetPr(plhs[0]) = (int)iNumber;
        
    return;
    
    
} // mexCD_NbDim
