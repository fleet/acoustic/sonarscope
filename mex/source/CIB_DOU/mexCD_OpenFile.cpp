// Fichier mexCD_OpenFile.cpp
//
// SYNTAX :
//   idfile = mexCD_OpenFile(fileName, 'Readonly')
//   idfile = mexCD_OpenFile(fileName, 'Write')
//   idfile = mexCD_OpenFile(fileName, 'Replace')
//
// INPUT PARAMETERS :
//   fileName   : nom du fichier (avec arborescence).
//   Mode   	: mode d'ouverture du fichier (Lecture, Modif ou Création).
//
// COMPIL :
//   Sélectionner le compilateur de façon ad hoc : gcc ou Visual C++(=MSVC).
//   compil_CIB_DOU('compiler', 'MSVC', 'option', '-v ', 'filesource',{'all'})
//   ou
//   compil_CIB_DOU('compiler', 'MSVC', 'option', '-v ', 'filesource',{'nom_du_source.cpp'})
//
// SEE ALSO : Authors
// AUTHORS  : GLU
// VERSION  : $Id: .c,v 1.0 2006\01\25 08:23:13 Gallou Exp Gallou $
// ----------------------------------------------------------------------------

#include <mex.h>
#include <mat.h>
#include "CIB_DOU_File.h"
#define DEBUG 0

extern "C" { 
    #include "CD_Utils.h"
} 


extern "C" {
void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
int     iDummy;

char	*cFileName,
        *cOpenMode,
        cDummy[500];

CIB_DOU_File* pfile;

CIB_STC_String CSMode;      // Mode d'ouverture du fichier NetCDF.

    if (DEBUG) 
    {
        mexPrintf("Fichier %s, nrhs : %d\n", __FILE__, nrhs);
    }

    switch (nrhs)  
    {
        case 2:
            // Récupération du nom de fichier (OBLIGATOIRE en 1er)
            iDummy = mxGetN(prhs[0]);
            cFileName=(char *)mxCalloc(iDummy+1,sizeof(char));
            mxGetString(prhs[0],cFileName,iDummy+1);

            if (DEBUG)
            {
                mexPrintf("NetCDF_File: %s\n", cFileName);
            }
            if (mxGetClassID(prhs[1]) == mxCHAR_CLASS)
            {
                // Récupération du mode d'ouverture de fichier (OBLIGATOIRE en 1er)
                iDummy = mxGetN(prhs[1]);
                cOpenMode=(char *)mxCalloc(iDummy+1,sizeof(char));
                mxGetString(prhs[1],cOpenMode,iDummy+1);
                
                if (strcmp(cOpenMode, "Replace") ==0)
                {
                    CSMode = CIB_DOU_k_CREATE_FILE;
                }
                else if (strcmp(cOpenMode, "ReadOnly") ==0)
                {
                    CSMode = CIB_DOU_k_READ_FILE;
                }
                else if (strcmp(cOpenMode, "Write") ==0)
                {
                    CSMode = CIB_DOU_k_WRITE_FILE;
                }
               
                // Ouverture du fichier netCDF (sans gestion de Block)
                // Autre appel : iNbVarTotal = file.Num_vars();
                //CIB_DOU_File* pfile = new CIB_DOU_File(cFileName, CSMode, 0);
                pfile = new CIB_DOU_File;
				pfile->Open(cFileName, CSMode, 0);
                
                plhs[0] = mxCreateDoubleMatrix(1,1, mxREAL);
                *mxGetPr(plhs[0]) = (long)pfile;
                if (DEBUG) 
                {
                    mexPrintf("Ouverture du fichier pFile\t:\t%d\n",pfile);
                }
                // Désallocation de cFileName
                mxFree(cOpenMode);
            }
            else
            {
                sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(  CIBDOU_E_INPUT_TYPE, 
                                                                __FILE__,
                                                                __LINE__,
                                                                ""));
                mexErrMsgTxt(cDummy);
            }
            
            // Désallocation de cFileName
            mxFree(cFileName);
            break;

        default:
            sprintf(&cDummy[0],"%s", cCD_UtilErrorHandler(   CIBDOU_E_INPUT_NUMBER, 
                                                            __FILE__,
                                                            __LINE__,
                                                            ""));
            mexErrMsgTxt(cDummy);
            break;
    }

    return;
    
} // mexCD_OpenFile

} // Fin de la déclaration des fonctions Extern.
