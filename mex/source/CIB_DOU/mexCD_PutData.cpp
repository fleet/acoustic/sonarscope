// Fichier mexCD_PutData.cpp
//
// SYNTAX :
//   mexCD_PutData(CDFile, indexVar, dataVar)
//   mexCD_PutData(CDFile, nomVar, dataVar)
//
// INPUT PARAMETERS :
//   idFile     : identifiant du fichier (d�j� ouvert)
//   indexVar	: indice de la variable.
//   nameVar	: nom de la variable.
//   dataVar	: data de la varaible.
//
// COMPIL :
//   S�lectionner le compilateur de fa�on ad hoc : gcc ou Visual C++(=MSVC).
//   compil_CIB_DOU('compiler', 'MSVC', 'option', '-v ', 'filesource',{'all'})
//   ou
//   compil_CIB_DOU('compiler', 'MSVC', 'option', '-v ', 'filesource',{'nom_du_source.cpp'})
//
// SEE ALSO : Authors
// AUTHORS  : GLU
// VERSION  : $Id: .c,v 1.0 2006\01\25 08:23:13 Gallou Exp Gallou $
// ----------------------------------------------------------------------------

#include <stdio.h>

#include <mex.h>
#include <mat.h>
#include "CIB_DOU_File.h"


extern "C" { 
    #include "CD_Utils.h"
} 


#define DEBUG 0

void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
int             iNbItems,
                iIndexVariable,
                iNbDims,
                iLoop,
                iTypeInput;

const mwSize   *iTabDims;
short int       *siTabData;
double          *dTabData;
float           *fTabData;
long			*lTabData;
unsigned char	*ucTabData;
char            *cNameVariable,
                cDummy[500],
                *cTabData;

double			dCharIsByte;

BOOL            bRet;
   
CIB_DOU_File    *CDFile;        // Cr�ation de la variable d'acc�s au fichier;
NcVar           *CDVar;         // Cr�ation du containeur des variables;
NcValues        *CDPtrValues;   // Cr�ation du containeur de valeurs des variables;

    if (DEBUG) 
    {
        mexPrintf("Fichier %s, nrhs : %d\n", __FILE__, nrhs);
    }
    switch (nrhs)  
    {
        case 4:
            // R�cup�ration du pointeur de fichier (OBLIGATOIRE en 1er 
            // argument).
            CDFile = (CIB_DOU_File *)(long)mxGetScalar(prhs[0]);

            // R�cup�ration du 2�me argument.
            if (mxGetClassID(prhs[1]) == mxCHAR_CLASS)
            {
                // Longueur du nom de la variable pass�e en argument.
                iNbItems = mxGetN(prhs[1]);    
                // R�cup�ration du nom de la variable.
                cNameVariable=(char *)mxCalloc(iNbItems+1,sizeof(char));
                mxGetString(prhs[1],cNameVariable,iNbItems+1);
                // Affectation de la variable VAR.
                CDVar = CDFile->Get_var(cNameVariable);
                
                // V�rification de l'existence de la variable
                bRet = CDFile->Exists_var(cNameVariable);
                if (bRet == FALSE)
                {
                    sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(  CIBDOU_E_VAR_NOTEXIST, 
                                                                    __FILE__,
                                                                    __LINE__,
                                                                    cNameVariable));
                    mexErrMsgTxt(cDummy);
                }
            }
            else if (mxGetClassID(prhs[1]) == mxDOUBLE_CLASS)
            {
                iIndexVariable = (int)mxGetScalar(prhs[1]) - 1;
                
                // V�rification de l'existence de la variable
                if (iIndexVariable > CDFile->Num_vars())
                {
                    sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(  CIBDOU_E_VAR_NOTEXIST, 
                                                                    __FILE__,
                                                                    __LINE__,
                                                                    ""));
                    mexErrMsgTxt(cDummy);
                }
                iNbItems = strlen(CDFile->Name_var(iIndexVariable));    
                cNameVariable=(char *)mxCalloc(iNbItems+1,sizeof(char));
                sprintf(cNameVariable,CDFile->Name_var(iIndexVariable));
                // Affectation de la variable VAR.
                CDVar = CDFile->Get_var(cNameVariable);

            }
            else
            {
                sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(  CIBDOU_E_INPUT_TYPE, 
                                                                __FILE__,
                                                                __LINE__,
                                                                ""));
                mexErrMsgTxt(cDummy);
            }
            
            // Identification du type des valeurs.
            iTypeInput = mxGetClassID(prhs[2]);
            
            // Nombre de dimensions associ�es
            iNbDims = (int)CDVar->num_dims();
            iTabDims = mxGetDimensions(prhs[2]);
    
 			// Nombre de valeurs (= Produit des N dimensions).
			// iNbItems = (int)CDVar->num_vals();
			iNbItems = mxGetNumberOfElements(prhs[2]);

            break;
        default:
            sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(  CIBDOU_E_INPUT_NUMBER, 
                                                            __FILE__,
                                                            __LINE__,
                                                            ""));
            mexErrMsgTxt(cDummy);
            break;
    }
    
            
    if (DEBUG)
    {
        mexPrintf("Adresse de CDFile\t:\t%d\n",CDFile);
        mexPrintf("Nombre de dimensions : %d\n", iNbDims);
    }

    // Les valeurs sont-elles � traiter comme des num�riques.
    dCharIsByte = mxGetScalar(prhs[3]);
    if (dCharIsByte == 1)
	{
		iTypeInput = mxCHAR_CLASS;
	}

	switch (iTypeInput)
    {    
        case mxUNKNOWN_CLASS:
            sprintf(&cDummy[0],"%s",cCD_UtilErrorHandler(  CIBDOU_E_TYPE_UNTREATED, 
                                                            __FILE__,
                                                            __LINE__,
                                                            ""));
            mexErrMsgTxt(cDummy);
            break;
        
        case mxUINT8_CLASS:
            // On caste les donn�es de type nc_BYTE en unsigned char, type
            // CIB_DOU s'approchant le plus.
            ucTabData = (unsigned char *)mxGetData(prhs[2]);
            if (DEBUG)
            {
				mexPrintf("ucTabData : %s\n", ucTabData);
            }
            switch (iNbDims) 
            {
                case 0: // 1 seul caract�re
                    bRet = CDFile->Putdata(cNameVariable, ucTabData);
                    break;
                case 1:
                    bRet = CDFile->Putdata(cNameVariable, ucTabData, iTabDims[0]);
                    break;
                case 2:
                    bRet = CDFile->Putdata(cNameVariable, ucTabData, iTabDims[0],
                                                                    iTabDims[1]);
                    break;
                case 3:
                    bRet = CDFile->Putdata(cNameVariable, ucTabData, iTabDims[0],
                                                                    iTabDims[1],
                                                                    iTabDims[2]);
                    break;
                case 4:
                    bRet = CDFile->Putdata(cNameVariable, ucTabData, iTabDims[0],
                                                                    iTabDims[1],
                                                                    iTabDims[2],
                                                                    iTabDims[3]);
                    break;
            }
            break;

		case mxCHAR_CLASS:
			// Cast pour les cas o� la donn�e doit �crite en num�rique pour du Char.
			if (dCharIsByte == 1)
			{
				cTabData=(char *)mxCalloc(iNbItems+1,sizeof(char));
				cTabData = (char *)mxGetData(prhs[2]);
			}
			else
			{
				cTabData=(char *)mxCalloc(iNbItems+1,sizeof(char));
				mxGetString(prhs[2],cTabData,iNbItems+1);
			}

            if (DEBUG)
            {
                mexPrintf("cTabData : %s\n", cTabData);
            }
			switch (iNbDims) 
            {
                case 0: // 1 seul caract�re
                    bRet = CDFile->Putdata(cNameVariable, cTabData);
                    break;
                case 1:
                    bRet = CDFile->Putdata(cNameVariable, cTabData, iTabDims[0]);
                    break;
                case 2:
                    bRet = CDFile->Putdata(cNameVariable, cTabData, iTabDims[0],
                                                                    iTabDims[1]);
                    break;
                case 3:
                    bRet = CDFile->Putdata(cNameVariable, cTabData, iTabDims[0],
                                                                    iTabDims[1],
                                                                    iTabDims[2]);
                    break;
                case 4:
                    bRet = CDFile->Putdata(cNameVariable, cTabData, iTabDims[0],
                                                                    iTabDims[1],
                                                                    iTabDims[2],
                                                                    iTabDims[3]);
                    break;
            }
            break;

		
		case mxINT16_CLASS:    // Valeurs de type Short.
            siTabData = (short int *)mxGetData(prhs[2]);
            if (DEBUG)
            {
                mexPrintf("iTabDims[0] : %d\n", iTabDims[0]);
                mexPrintf("iTabDims[1] : %d\n", iTabDims[1]);
                for (iLoop=0; iLoop< 10; iLoop++) {
                    mexPrintf("siTabData[iLoop] : %d\n", siTabData[iLoop]);
                }
            }
            switch (iNbDims) 
            {
                case 0: // Valeur scalaire.
                    bRet = CDFile->Putdata(cNameVariable, siTabData);
                    break;
                case 1:
                    bRet = CDFile->Putdata(cNameVariable, siTabData,iTabDims[0]);
                    break;
                case 2:                    
                    bRet = CDFile->Putdata(cNameVariable, siTabData,iTabDims[0],
                                                                    iTabDims[1]);
                    break;
                case 3:
                    bRet = CDFile->Putdata(cNameVariable, siTabData,iTabDims[0],
                                                                    iTabDims[1],
                                                                    iTabDims[2]);
                    break;
                case 4:
                    bRet = CDFile->Putdata(cNameVariable, siTabData,iTabDims[0],
                                                                    iTabDims[1],
                                                                    iTabDims[2],
                                                                    iTabDims[3]);
                    break;
            }
            break;

		case mxSINGLE_CLASS:    // Valeurs de type Float
		    fTabData = (float *)mxGetData(prhs[2]);
            if (DEBUG)
            {
                mexPrintf("iTabDims[0] : %d\n", iTabDims[0]);
                mexPrintf("iTabDims[1] : %d\n", iTabDims[1]);
                for (iLoop=0; iLoop< 10; iLoop++) {
                    mexPrintf("fTabData[%d] : %f\n", iLoop, fTabData[iLoop]);
                }
            }
            switch (iNbDims) 
            {
                case 0: // Valeur scalaire.
                    bRet = CDFile->Putdata(cNameVariable, fTabData);
                    break;
                case 1:
                    bRet = CDFile->Putdata(cNameVariable, fTabData, iTabDims[0]);
                    break;
                case 2:
                    bRet = CDFile->Putdata(cNameVariable, fTabData,	iTabDims[0],
                                                                    iTabDims[1]);
                    break;
                case 3:
                    bRet = CDFile->Putdata(cNameVariable, fTabData,	iTabDims[0],
                                                                    iTabDims[1],
                                                                    iTabDims[2]);
                    break;
                case 4:
                    bRet = CDFile->Putdata(cNameVariable, fTabData,	iTabDims[0],
                                                                    iTabDims[1],
                                                                    iTabDims[2],
                                                                    iTabDims[3]);
                    break;
            }
            break;

        case mxDOUBLE_CLASS :	
            dTabData = (double *)mxGetData(prhs[2]);
			if (DEBUG)
            {
                mexPrintf("iTabDims[0] : %d\n", iTabDims[0]);
                mexPrintf("iTabDims[1] : %d\n", iTabDims[1]);
                for (iLoop=0; iLoop< 10; iLoop++) {
                    mexPrintf("dTabData[%d] : %f\n", iLoop, dTabData[iLoop]);
                }
            }
            switch (iNbDims) 
            {
                case 0: // Valeur scalaire.
                    bRet = CDFile->Putdata(cNameVariable, dTabData);
                    break;
                case 1:
                    bRet = CDFile->Putdata(cNameVariable, dTabData, iTabDims[0]);
                    break;
                case 2:
                    bRet = CDFile->Putdata(cNameVariable, dTabData,	iTabDims[0],
                                                                    iTabDims[1]);                        
                    break;
                case 3:
                    bRet = CDFile->Putdata(cNameVariable, dTabData,	iTabDims[0],
                                                                    iTabDims[1],
                                                                    iTabDims[2]);
                case 4:
                    bRet = CDFile->Putdata(cNameVariable, dTabData,	iTabDims[0],
                                                                    iTabDims[1],
                                                                    iTabDims[2],
                                                                    iTabDims[3]);
                    break;
            }
            break;
        
        case mxINT32_CLASS:
            lTabData = (long *)mxGetData(prhs[2]);
			if (DEBUG)
            {
                mexPrintf("iTabDims[0] : %d\n", iTabDims[0]);
                mexPrintf("iTabDims[1] : %d\n", iTabDims[1]);
                for (iLoop=0; iLoop< 10; iLoop++) {
                    mexPrintf("lTabData[%d] : %ld\n", iLoop, lTabData[iLoop]);
                }
            }
            switch (iNbDims) 
            {
                case 0: // Valeur scalaire.
                    bRet = CDFile->Putdata(cNameVariable, lTabData);
                    break;
                case 1:
                    bRet = CDFile->Putdata(cNameVariable, lTabData, iTabDims[0]);
                    break;
                case 2:
                    bRet = CDFile->Putdata(cNameVariable, lTabData,	iTabDims[0],
                                                                    iTabDims[1]);                        
                    break;
                case 3:
                    bRet = CDFile->Putdata(cNameVariable, lTabData,	iTabDims[0],
                                                                    iTabDims[1],
                                                                    iTabDims[2]);
                case 4:
                    bRet = CDFile->Putdata(cNameVariable, lTabData,	iTabDims[0],
                                                                    iTabDims[1],
                                                                    iTabDims[2],
                                                                    iTabDims[3]);
                    break;
            }
            break;

		default:   // pour tous les autres cas de types.
		   break;
                      
    }            

    mxFree(cNameVariable);
    
    
    return;
    
    
} // mexCD_PutData
