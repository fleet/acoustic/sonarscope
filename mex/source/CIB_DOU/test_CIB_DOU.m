%% Fichier de Test divers
%% Test Read_Depth_new
nomFic = getNomFicDatabase('EM1002_0001_20040916_054013_raw.all')
a = cl_simrad_all('nomFic', nomFic);
Data_new = read_depth_new(a)


%% Test mexCD_GetData.cpp
fileName = getNomFicDatabase('EM120_CALIMERO_A20040915.mbb');
a = cl_netcdf('fileName', fileName);
get_value(a,4);     % 20X20 car en sortie.

fileName = getNomFicDatabase('EM12D_PRISMED_500m.mnt');
a = cl_netcdf('fileName', fileName);
get_value(a,1);     % DTM en sortie.

%% Test mexCD_PutData.cpp
fileName = getNomFicDatabase('toto_corre.nc');
a = cl_netcdf('fileName', fileName);
pold = get_value(a,7);     % 1024X1 double en sortie (avec des NAN)
pnew = pold+1;
pnew(2) = 100;
set_value(a,7,pnew);
get_value(a,7, 'sub1', [1:10]);     % 10X1 double en sortie (avec des NAN)


%% Probl�me CIB_BLOCK_DIM 
fileName = getNomFicDatabase('toto.nc');
a = cl_netcdf('fileName', fileName);
get_value(a,7);     % Pb avec la dimension CIB_BLOCK_DIM


%% Test d'extraction de type MNT
nomFicIn  = getNomFicDatabase('EM300_Ex2.mnt')
nomFicOut = [tempname '.mnt']
extract_mnt(nomFicIn, nomFicOut)
