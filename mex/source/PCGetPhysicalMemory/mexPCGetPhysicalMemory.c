// Fichier PCGetPhysicalMemory.c
//
// SYNTAX :
//   [x, y] = mexPCGetPhysicalMemory
// 
//  
// OUTPUT PARAMETERS : 
//   x : Taille m�moire physique totale
//   y : Taiile m�moire physique disponible
// 
// EXAMPLES :
//   [x, y] = PCGetPhysicalMemory
//   x =
//      2096620
//   y =
//     1550308
// 
// COMPIL :
//   mex -v mexPCGetPhysicalMemory.c -outdir C:\IfremerToolbox\mex\lib\PCWIN\PCGetPhysicalMemory
//   (le chemin du source doit �tre dans le path)
//
// SEE ALSO : Authors
// AUTHORS  : GLU
// VERSION  : $Id: .c,v 1.0 2005/10/07 08:23:13 Gallou Exp Gallou $
// ----------------------------------------------------------------------------


//  Sample output:
//  The MemoryStatus structure is 32 bytes long.
//  It should be 32.
//  78 percent of memory is in use.
//  There are   65076 total Kbytes of physical memory.
//  There are   13756 free Kbytes of physical memory.
//  There are  150960 total Kbytes of paging file.
//  There are   87816 free Kbytes of paging file.
//  There are  1fff80 total Kbytes of virtual memory.
//  There are  1fe770 free Kbytes of virtual memory.

#include <windows.h>
#include "mex.h"


// Use to change the divisor from Kb to Mb.

#define DIV 1024
// #define DIV 1

char *divisor = "K";
// char *divisor = "";

// Handle the width of the field in which to print numbers this way to
// make changes easier. The asterisk in the print format specifier
// "%*ld" takes an int from the argument list, and uses it to pad and
// right-justify the number being formatted.
#define WIDTH 9

void PCGetPhysicalMemory(double *pPhysTotalMemory, double *pPhysFreeMemory)
{
  MEMORYSTATUS stat;

  // Fonction MSDN appel�e.
  GlobalMemoryStatus (&stat);

    /*
    mexPrintf ("The MemoryStatus structure is %ld bytes long.\n",
            stat.dwLength);
    mexPrintf ("It should be %d.\n", sizeof (stat));
    mexPrintf ("%ld percent of memory is in use.\n",
            stat.dwMemoryLoad);
    mexPrintf ("There are %*ld total %sbytes of physical memory.\n",
            WIDTH, stat.dwTotalPhys/DIV, divisor);
    mexPrintf ("There are %*ld free %sbytes of physical memory.\n",
            WIDTH, stat.dwAvailPhys/DIV, divisor);
    mexPrintf ("There are %*ld total %sbytes of paging file.\n",
            WIDTH, stat.dwTotalPageFile/DIV, divisor);
    mexPrintf ("There are %*ld free %sbytes of paging file.\n",
            WIDTH, stat.dwAvailPageFile/DIV, divisor);
    mexPrintf ("There are %*ld total %sbytes of virtual memory.\n",
            WIDTH, stat.dwTotalVirtual/DIV, divisor);
    mexPrintf ("There are %*ld free %sbytes of virtual memory.\n",
            WIDTH, stat.dwAvailVirtual/DIV, divisor);
     */

  // Assignation des valeurs de sorties.
    *pPhysTotalMemory = (double)stat.dwTotalPhys/DIV;
    *pPhysFreeMemory = (double)stat.dwAvailPhys/DIV;
}
  

void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[]){
  double    *dPTM,
            *dPFM;

    /* check for the proper number of arguments */
    if(nlhs > 2)
    {
      mexErrMsgTxt("Too many output arguments.");
    }

    // Allocation des pointeurs de sorties (plhs : l pour left !) 
    plhs[0] = mxCreateDoubleMatrix(1,1, mxREAL);
    plhs[1] = mxCreateDoubleMatrix(1,1, mxREAL);
  
    /* Assign pointers to each output*/
    dPTM = mxGetPr(plhs[0]);
    dPFM = mxGetPr(plhs[1]);
    
    /* Call the C subroutine. */
    PCGetPhysicalMemory(dPTM, dPFM);
  
    return;
}
