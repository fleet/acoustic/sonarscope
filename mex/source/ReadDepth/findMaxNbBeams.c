// Fichier findMaxNbBeams.c, permet de d�terminer le nombre Max de faisceaux sur le 
// .all en entr�e.
//
// SYNTAX :
//   iNbMaxBeams = findMaxNbBeams(cNomFic, iFlagEndian, iN, dPosition, &iRetour);
//  
// OUTPUT PARAMETERS : 
//   iNbMaxBeams : nombre max de faisceaux trouv�s l'ensemble des acquisitions.
//
// EXAMPLES :
//   Cf. ReadDepth.m (et appel dans mexRead_Depth.c)
// 
// COMPIL :
//   mex mexReadDepth.c readDepth.c findMaxNbBeams.c utilsFunc.c ...
//      -outdir I:\IfremerToolBox\mex\lib\PCWIN (ou GLNX86) 
//   (-out .dll sous SP3 pour forc� � cr�er en dll).
//
// SEE ALSO : Authors
// AUTHORS  : GLU
// VERSION  : $Id: .c,v 1.0 2005/10/07 08:23:13 Gallou Exp Gallou $
// ----------------------------------------------------------------------------


#include <mat.h>
#include <mex.h>  // N�cessaire pour les "mexPrintf" sous MatLab.
#include "utilsFunc.h"
#include "string.h"


#define DEBUG 0

int findMaxNbBeams( char *cNomFic, 
                    int iN,
                    int iFlagEndian, 
                    double dPosition[],
                    int *iRetour)
{ 
    int     iVal,
            iNbLoopRead,
            iLoopRead,
            iNbMaxBeams = 0;
    
    unsigned short int  uiVal,
                        uiEmModel;
    
    unsigned char       ucMaxNbBeams,
                        ucNbBeams,
                        ucVal;
    
    FILE	*fp;
    

    *iRetour = 1;
    if( DEBUG ) {
        mexPrintf("Nom du fichier � lire : %s\n", cNomFic);
        mexPrintf("Fichier : %s\n", __FILE__);
    }

    // R�cup�ration du nom de fichier.
    iNbLoopRead = iN;

    // Open the Data-File to read from
	fp = fopen(cNomFic,"rb ");
    if ( NULL == fp )
	{
        char errmsg[]="cl_simrad_all:read_depth : Failed to open '";
        *iRetour = 0;

        strcat(errmsg,cNomFic);
        strcat(errmsg,"'");

        free(cNomFic);
        mexErrMsgTxt(errmsg);
    }

    for (iLoopRead = 0; iLoopRead < iNbLoopRead; iLoopRead++)
    {
        // Positionnement en d�but de fichier + offset.
        fseek(fp, (int)dPosition[iLoopRead], SEEK_SET);
        
        // Lecture des Data.
        // NbOfBytesInDatagram
        fread( &iVal, sizeof(int), 1, fp);  // Int32        
        // STX
        fread(&ucVal, sizeof(unsigned char), 1, fp);        
        // TypeOfDatagram
        fread(&ucVal, sizeof(unsigned char), 1, fp);
        // EmModel
        fread(&uiVal, sizeof(unsigned short int), 1, fp);  // Int16 non-sign�
        uiEmModel = uiVal;
        if (iFlagEndian == 1) {
            iUICodageEndian(uiVal, &uiEmModel);
        }
        // Date
        fread(&iVal, sizeof(int), 1, fp); // Int32
        // NbMilliSec
        fread(&iVal, sizeof(int), 1, fp); // Int32
        // PingCounter
        fread(&uiVal, sizeof(unsigned short int), 1, fp);  // Int16 non-sign�
        // Serial Number
        fread(&uiVal, sizeof(unsigned short int), 1, fp); // Int16 non-sign�
        // dHeading
        fread(&uiVal, sizeof(unsigned short int), 1, fp);  // Int16 non-sign�
        // Sound Speed
        fread(&uiVal, sizeof(unsigned short int), 1, fp);  // Int16 non-sign�
        // Transducer Depth
        fread(&uiVal, sizeof(unsigned short int), 1, fp);  // Int16 non-sign�
        // Max Nb Beams
        fread(&ucMaxNbBeams, sizeof(unsigned char), 1, fp); // Int8
        if( DEBUG ) {
            mexPrintf("NbMaxBeams\t\t\t\t:\t%d\n", (int)ucMaxNbBeams);
        }
        if (iNbMaxBeams < ucMaxNbBeams)
        {
            iNbMaxBeams = ucMaxNbBeams;
        }
        if( DEBUG ) {
            mexPrintf("iMaxNbBeams augment�\t\t:\t%d\n", iNbMaxBeams);
        }
        // Nb Beams        
        fread(&ucNbBeams, sizeof(unsigned char), 1, fp); // Int8
        if( DEBUG ) {
             mexPrintf("cNbBeams[%d]\t\t\t\t:\t%d\n", 0, ucNbBeams);
        }
        // Z Resol
        fread(&ucVal, sizeof(unsigned char), 1, fp); // Int8
        // XY Resol
        fread(&ucVal, sizeof(unsigned char), 1, fp); // Int8
        // Sampling Rate
        fread(&uiVal, sizeof(unsigned short int), 1, fp);
        
        // On arr�te la lecture car on se repositionne en d�but de boucle
        // sur l'offset de position.

    }

    fclose(fp);
     
    return iNbMaxBeams;
} // Read_Depth
