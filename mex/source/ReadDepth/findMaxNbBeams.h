// Fichier findMaxNbBeams.h, déclaration du fichier findMaxNbBeams.c
//
// SEE ALSO : Read_Depth.c
// AUTHORS  : GLU
// VERSION  : $Id: .c,v 1.0 2005/10/07 08:23:13 Gallou Exp Gallou $
// ----------------------------------------------------------------------------


#ifndef FINDMAXNBBEAMS_H
#define FINDMAXNBBEAMS_H


int findMaxNbBeams( char *cNomFic, 
                    int iN,
                    int iFlagEndian, 
                    double dPosition[],
                    int *iRetour);
#endif
