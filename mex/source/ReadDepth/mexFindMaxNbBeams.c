// Fichier mexFindMaxNbBeams.c
//
// SYNTAX :
//   [Y, Ret] = mexFindMaxNbBeams(this.nomFic, flagEndian, N, Position, Retour)
//  
// INPUT PARAMETERS : (plhs : r pour right !)
//   this.nomFic : nom du fichier et de son arborescence 
//   flagEndian  : indicateur d'inversion Big Endian-Little Endian
//   N           : nombre de boucles d'acquisition.
//   Position    : tableau[N] de valeurs d'offset de lecture dans le fichier .All
//   Retour      : indicateur de bon fonctionnement.
//
// OUTPUT PARAMETERS : (plhs : l pour left !) 
//   MaxBeams : Nb max de faisceaux sur l'ensemble des acquisitions.
//   Ret : flag de validation de la proc�dure (1: OK, 0:KO).
//
// EXAMPLES :
//   cf. ReadDepth
// 
// COMPIL :
//   mex -g mexFindMaxNbBeams.c findMaxNbBeams.c utilsFunc.c...
//  -v -outdir C:\IfremerToolBox\mex\lib\ReadDepth (-out .dll sous SP3 pour forcer � cr�er en dll).
//   (le chemin du source doit �tre dans le path)
//
// SEE ALSO : Authors
// AUTHORS  : GLU
// VERSION  : $Id: .c,v 1.0 2006/01/25 08:23:13 Gallou Exp Gallou $
// ----------------------------------------------------------------------------


#include <mex.h>
#include <mat.h>
#include "findMaxNbBeams.h"

#define DEBUG 0 


void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[]){

   
    double  *dPosition,
            *dRetour,
            *dNbMaxBeams;
    
    int     iBuflen,
            iFlagEndian,
            iNbPosition,
            iN,
            iNbMaxBeams,
            iRetour;
    
    char	*cNomFic;

    if (nrhs != 4) 
    {
        mexErrMsgTxt("Error : Number of inputs incorrect.");
        *dRetour = 0;
    }
    if (nlhs != 2) 
    {
        mexErrMsgTxt("Error : Number of outputs incorrect.");
        *dRetour = 0;
    }

    // Assign pointers to each output and input
    iBuflen = mxGetN(prhs[0]);
    
    // R�cup�ration du nom de fichier.
    cNomFic=(char *)mxCalloc(iBuflen+1,sizeof(char));
    mxGetString(prhs[0],cNomFic,iBuflen+1);
    
    // Assign pointers to each output and input
    iFlagEndian = (int)mxGetScalar(prhs[1]);
    
    
    // R�cup�ration des dimensions et tableaux de tailles.
    iN = (int)mxGetScalar(prhs[2]);
    if( DEBUG ) {mexPrintf("iN : %d\n",iN);}

    iNbPosition = (int)mxGetNumberOfElements(prhs[3]);
    if( DEBUG ) {mexPrintf("iNbPosition : %d\n",iNbPosition);}
    dPosition = (double *)mxGetPr(prhs[3]);

    // V�rification des param�tres d'entr�e.
    if (iN != iNbPosition)
    {
        mexErrMsgTxt("Error : Nb of items in arrays N and Position are not equals. !!!");
        *dRetour = 0;
    }
    if (mxGetClassID(prhs[0]) != mxCHAR_CLASS) 
    {
        mexErrMsgTxt("Error : Input nomFic must be a string.");
        *dRetour = 0;
    }
    if (!mxIsLogical(prhs[1]) || mxIsComplex(prhs[1]) ||
        mxGetN(prhs[1])*mxGetM(prhs[1]) != 1) 
    {
        mexErrMsgTxt("Error : Input flagEndian must be a logical scalar.");
        *dRetour = 0;
    }
	
    if (!mxIsDouble(prhs[2]) || mxIsComplex(prhs[2])) 
    {
        mexErrMsgTxt("Error : Input N must be a double scalar.");
        *dRetour = 0;
    }

    if (!mxIsDouble(prhs[3]) || mxIsComplex(prhs[3])) 
    {
        mexErrMsgTxt("Error : Input Position must be a double Scalar.");
        *dRetour = 0;
    }
    
    // R�servation m�moire des pointeurs de sorties.
    plhs[1] = mxCreateDoubleMatrix(1,1, mxREAL);
    dRetour = mxGetPr(plhs[1]);
    plhs[0] = mxCreateDoubleMatrix(1,1, mxREAL);
    dNbMaxBeams = mxGetPr(plhs[0]);

    // On recherche le nombre de faisceaux Max par une lecture pr�alable du fichier.
    iNbMaxBeams = findMaxNbBeams(cNomFic, iFlagEndian, iN, dPosition, &iRetour);

    // Allocation des valeurs de sorties.
    *dNbMaxBeams = (double)iNbMaxBeams;
    *dRetour = (double)iRetour;
    if (DEBUG)
    {
        mexPrintf("Valeur ramen�e par la proc�dure findMaxNbBeams : %d\n",iNbMaxBeams);    
        mexPrintf("Valeur ramen�e par la proc�dure findMaxNbBeams : %f\n",dNbMaxBeams);    
    }
    
    mxFree(cNomFic);
    
    return;
} // mexFindMaxNbBeams
