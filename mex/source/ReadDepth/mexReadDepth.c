// Fichier mexReadDepth.c
//
// SYNTAX :
//   [Y, Ret] = mexReadDepth(this.nomFic, flagEndian, N, Position, Data)
//  
// INPUT PARAMETERS : (plhs : r pour right !)
//   this.nomFic : nom du fichier et de son arborescence 
//   flagEndian  : indicateur d'inversion Big Endian-Little Endian
//   N           : nombre de boucles d'acquisition.
//   Position    : tableau[N] de valeurs d'offset de lecture dans le fichier .All
//   Data        : structure d'entr�e de la proc�dure.
//
// OUTPUT PARAMETERS : (plhs : l pour left !) 
//   Y : Structure contenant Depth, AcrossDist, AlongDist, AlongDist,
//       BeamAzimuthAngle, Range, QualityFactor, LengthDetection et Reflectivity
//   Ret : flag de validation de la proc�dure (1: OK, 0:KO).
//
// EXAMPLES :
//   cf. readDepth
//
// COMPIL :
//   avec le compilateur gcc, compilation possible sous Linux ou Win32
//   mex -g mexReadDepth.c readDepth.c utilsFunc.c findMaxNbBeams.c...
//  -v -outdir C:\IfremerToolBox\mex\lib\PCWIN\ReadDepth (-out .dll sous SP3 pour forcer � cr�er en dll).
//   (le chemin du source doit �tre dans le path)
// ou 
//   mex -g mexReadDepth.c readDepth.c utilsFunc.c findMaxNbBeams.c...
//  -v -outdir C:\IfremerToolBox\mex\lib\GLNX86\ReadDepth (-out .dll sous SP3 pour forcer � cr�er en dll).
//
// SEE ALSO : Authors
// AUTHORS  : GLU
// VERSION  : $Id: .c,v 1.0 2006/01/25 08:23:13 Gallou Exp Gallou $
// ----------------------------------------------------------------------------


#include <mex.h>
#include <string.h>
#include <mat.h>
#include "readDepth.h"


// D�tection des fuites M�moire sous Visual C++
#ifdef _MSC_VER
	#define _noCRTDBG_MAP_ALLOC
	#include <stdlib.h>
	#include <crtdbg.h>
#endif


#define DEBUG 0


void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[]){

   
    T_DATA  strData;

    double  *dPosition,
            *dRetour;
    
    int     iBuflen,
            iFlagEndian,
            iNbPosition,
            iN,
            iMrows,
            iNcols,
            iField, 
            iStruct,
            *iClassIDflags,
            iNStructElems,
            iNFields,
            iRetour,
            *iTabDims;
    
    char	*cNomFic,
            *cData;
    
    const char **cFieldNames;
    
    mxArray *mxArrTmp,
            *mxArrFout;
    
    size_t     iSizebuf;


    plhs[1] = mxCreateDoubleMatrix(1,1, mxREAL);
    dRetour = mxGetPr(plhs[1]);
    // Initialisation du Flag de retour.
    *dRetour = (double)1;

    if (nrhs != 5) 
    {
        mexErrMsgTxt("Error : Number of inputs incorrect.");
        *dRetour = 0;
    }
    if (nlhs != 2) 
    {
        mexErrMsgTxt("Error : Number of outputs incorrect.");
        *dRetour = 0;
    }

    // Assign pointers to each output and input
    iBuflen = mxGetN(prhs[0]);
    
    // R�cup�ration du nom de fichier.
    cNomFic=(char *)mxCalloc(iBuflen+1,sizeof(char));
    mxGetString(prhs[0],cNomFic,iBuflen+1);
    
    // Assign pointers to each output and input
    iFlagEndian = (int)mxGetScalar(prhs[1]);
    
    
    // R�cup�ration des dimensions et tableaux de tailles.
    iN = (int)mxGetScalar(prhs[2]);
    if( DEBUG ) {mexPrintf("iN : %d\n",iN);}

    iNbPosition = (int)mxGetNumberOfElements(prhs[3]);
    if( DEBUG ) {mexPrintf("iNbPosition : %d\n",iNbPosition);}
    dPosition = (double *)mxGetPr(prhs[3]);

    // V�rification des param�tres d'entr�e.
    if (iN != iNbPosition)
    {
        mexErrMsgTxt("Error : Nb of items in arrays N and Position are not equals. !!!");
        *dRetour = 0;
    }
    if (mxGetClassID(prhs[0]) != mxCHAR_CLASS) 
    {
        mexErrMsgTxt("Error : Input nomFic must be a string.");
        *dRetour = 0;
    }
    if (!mxIsLogical(prhs[1]) || mxIsComplex(prhs[1]) ||
        mxGetN(prhs[1])*mxGetM(prhs[1]) != 1) 
    {
        mexErrMsgTxt("Error : Input flagEndian must be a logical scalar.");
        *dRetour = 0;
    }
	
    if (!mxIsDouble(prhs[2]) || mxIsComplex(prhs[2])) 
    {
        mexErrMsgTxt("Error : Input N must be a double scalar.");
        *dRetour = 0;
    }

    if (!mxIsDouble(prhs[3]) || mxIsComplex(prhs[3])) 
    {
        mexErrMsgTxt("Error : Input Position must be a double Scalar.");
        *dRetour = 0;
    }
     if (!mxIsStruct(prhs[4]) || mxIsComplex(prhs[4])) 
    {
        mexErrMsgTxt("Error : Input/Output Position must be a Structure.");
        *dRetour = 0;
    }
    
    // R�cup�ration des champs d'entr�e de la structure.
    iNFields = mxGetNumberOfFields(prhs[4]);
    iNStructElems = mxGetNumberOfElements(prhs[4]);
    // Allocations pour les noms de champs de la structure.
    cFieldNames = mxCalloc(iNFields, sizeof(*cFieldNames));
    // Allocations pour les types de champs de la structure.
    iClassIDflags = mxCalloc(iNFields, sizeof(int));
    for (iField = 0; iField < iNFields; iField++) {
        for (iStruct = 0; iStruct < iNStructElems; iStruct++) {
            mxArrTmp = mxGetFieldByNumber(prhs[4], iStruct, iField);
            if (DEBUG)
            {
                mexPrintf("FIELD:%d\t%s%d\n",iField+1, "STRUCT INDEX :", iStruct+1);
                mexPrintf("Type de champs de la structure : %s\n", (char *)mxGetClassName(mxArrTmp));
            }
            iClassIDflags[iField]=mxGetClassID(mxArrTmp); 
        }
        cFieldNames[iField] = mxGetFieldNameByNumber(prhs[4],iField);
        if (DEBUG) {mexPrintf("Nom du champ de la structure : %s\n", cFieldNames[iField]); }
    }  


    // Copie des donn�es d'entr�es dans la structure strData
    for(iField=0; iField<iNFields; iField++) {
        // R�cup�ration des valeurs d'entr�e de la structure.
        mxArrTmp = mxGetFieldByNumber(prhs[4], 0, iField);
		// Allocation des pointeurs de sorties 
        iMrows = mxGetM(mxArrTmp);
        iNcols = mxGetN(mxArrTmp);
        if (DEBUG)
        {
            mexPrintf("strData : Nom du Champ[%d] : %s\n", iField, cFieldNames[iField]);
            mexPrintf("strData : iMrows[%d]: %d\n", iField, iMrows);
            mexPrintf("strData : iNcols[%d]: %d\n", iField, iNcols);
        }
        if(iClassIDflags[iField] == mxCHAR_CLASS) 
		{
            //mxArrFout = mxCreateCellArray(iNdim, iDims);
            if(!strcmp(cFieldNames[iField], "EmModel")) 
			{
				strData.cEmModel = calloc(iMrows*iNcols, sizeof(char));
				strcpy(strData.cEmModel, mxArrayToString(mxArrTmp));
			}
			else if(!strcmp(cFieldNames[iField], "SystemSerialNumber")) 
			{
 				strData.cSystemSerialNumber = calloc(iMrows*iNcols, sizeof(char));
                strcpy(strData.cSystemSerialNumber, mxArrayToString(mxArrTmp));
			}
		}	
        else 
		{
			// Copie des Data depuis la r�cup�ration du param�tre d'entr�e.
            iTabDims = (int *)mxMalloc(2*sizeof(int));
            iTabDims[0] = iMrows;
            iTabDims[1] = iNcols;
            mxArrFout = mxCreateNumericArray(2, iTabDims, mxSINGLE_CLASS, mxREAL);
			cData = mxGetData(mxArrFout);
			iSizebuf = mxGetElementSize(mxArrTmp);
			memcpy(cData, mxGetData(mxArrTmp), iSizebuf*iMrows*iNcols);
			cData += iSizebuf*iMrows*iNcols;

			// Recopie des Data dans le champ associ�.
			if (!strcmp(cFieldNames[iField], "Date"))
			{
                strData.fDate =   (float *)mxGetPr(mxArrFout);
			}
			if (!strcmp(cFieldNames[iField], "NbMilliSec"))
			{
                strData.fNbMilliSec =   (float *)mxGetPr(mxArrFout);
			}
			if (!strcmp(cFieldNames[iField], "PingCounter"))
			{
                strData.fPingCounter =   (float *)mxGetPr(mxArrFout);
			}
			else if (!strcmp(cFieldNames[iField], "Heading"))
			{
				strData.fHeading = (float *)mxGetPr(mxArrFout);
			}
			else if (!strcmp(cFieldNames[iField], "SoundSpeed"))
			{
				strData.fSoundSpeed = (float *)mxGetPr(mxArrFout);
			}
			else if (!strcmp(cFieldNames[iField], "TransducerDepth"))
			{
				strData.fTransducerDepth = (float *)mxGetPr(mxArrFout);
			}
			else if (!strcmp(cFieldNames[iField], "NbBeams"))
			{
				strData.fNbBeams = (float *)mxGetPr(mxArrFout);
			}
			else if (!strcmp(cFieldNames[iField], "SamplingRate"))
			{
				strData.fSamplingRate = (float *)mxGetPr(mxArrFout);
			}
			else if (!strcmp(cFieldNames[iField], "TransducerDepthOffsetMultiplier"))
			{
				strData.fTransducerDepthOffsetMultiplier = (float *)mxGetPr(mxArrFout);
			}
			else if (!strcmp(cFieldNames[iField], "Depth"))
			{
				strData.fDepth = (float *)mxGetPr(mxArrFout);
			}
			else if (!strcmp(cFieldNames[iField], "AcrossDist"))
			{
				strData.fAcrossDist = (float *)mxGetPr(mxArrFout);
			}
			else if (!strcmp(cFieldNames[iField], "AlongDist"))
			{
				strData.fAlongDist = (float *)mxGetPr(mxArrFout);
			}
			else if (!strcmp(cFieldNames[iField], "BeamDepressionAngle"))
			{
				strData.fBeamDepressionAngle = (float *)mxGetPr(mxArrFout);
			}
			else if (!strcmp(cFieldNames[iField], "BeamAzimuthAngle"))
			{
				strData.fBeamAzimuthAngle = (float *)mxGetPr(mxArrFout);
			}
			else if (!strcmp(cFieldNames[iField], "Range"))
			{
				strData.fRange = (float *)mxGetPr(mxArrFout);
			}
			else if (!strcmp(cFieldNames[iField], "QualityFactor"))
			{
				strData.fQualityFactor = (float *)mxGetPr(mxArrFout);
			}
			else if (!strcmp(cFieldNames[iField], "LengthDetection"))
			{
				strData.fLengthDetection = (float *)mxGetPr(mxArrFout);
			}
			else if (!strcmp(cFieldNames[iField], "Reflectivity"))
			{
				strData.fReflectivity = (float *)mxGetPr(mxArrFout);
			}
			mxFree(iTabDims);
		}
        
    } // fin de la boucle sur les champs de prhs[4] (Entr�e).
	
    // Appel de la proc�dure de lecture compl�te du fichier.
    readDepth(cNomFic, iFlagEndian, iN, dPosition, &strData, &iRetour);   
    *dRetour = (double)iRetour;

    
    // Copie des donn�es dans la structure phls[0].
 	// Allocation des pointeurs de sorties.
    plhs[0] = mxCreateStructMatrix(1, 1, iNFields, cFieldNames);
	for(iField=0; iField<iNFields; iField++) {
        mxArrTmp = mxGetFieldByNumber(prhs[4], 0, iField);
        iNcols = mxGetN(mxArrTmp);
        iMrows = mxGetM(mxArrTmp);
        // Cr�ation de la variable Tampon et copie de la data.
        if(!strcmp(cFieldNames[iField], "EmModel")) 
        {
            // set C-style string output_buf to MATLAB mexFunction output
            mxArrFout = mxCreateString((const char *)strData.cEmModel);
        }
        else if(!strcmp(cFieldNames[iField], "SystemSerialNumber")) 
        {
            mxArrFout = mxCreateString((const char *)strData.cSystemSerialNumber);
        }
        else 
        {
            iTabDims = (int *)mxMalloc(2*sizeof(int));
            iTabDims[0] = iMrows;
            iTabDims[1] = iNcols;
            mxArrFout = mxCreateNumericArray(2, iTabDims, mxSINGLE_CLASS, mxREAL);
            cData = mxGetData(mxArrFout);
            if(!strcmp(cFieldNames[iField], "Date")) 
            {
                memcpy(cData, &strData.fDate[0], sizeof(float)*iMrows*iNcols);
			}
            else if(!strcmp(cFieldNames[iField], "NbMilliSec")) 
            {
                memcpy(cData, &strData.fNbMilliSec[0], sizeof(float)*iMrows*iNcols);
			}
            else if(!strcmp(cFieldNames[iField], "PingCounter")) 
            {
                memcpy(cData, &strData.fPingCounter[0], sizeof(float)*iMrows*iNcols);
			}
			else if (!strcmp(cFieldNames[iField], "Heading"))
			{
                memcpy(cData, &strData.fHeading[0], sizeof(float)*iMrows*iNcols);
			}
			else if (!strcmp(cFieldNames[iField], "SoundSpeed"))
			{
                memcpy(cData, &strData.fSoundSpeed[0], sizeof(float)*iMrows*iNcols);
			}
			else if (!strcmp(cFieldNames[iField], "TransducerDepth"))
			{
                memcpy(cData, &strData.fTransducerDepth[0], sizeof(float)*iMrows*iNcols);
			}
			else if (!strcmp(cFieldNames[iField], "NbBeams"))
			{
                memcpy(cData, &strData.fNbBeams[0], sizeof(float)*iMrows*iNcols);
			}
			else if (!strcmp(cFieldNames[iField], "SamplingRate"))
			{
                memcpy(cData, &strData.fSamplingRate[0], sizeof(float)*iMrows*iNcols);
			}
			else if (!strcmp(cFieldNames[iField], "TransducerDepthOffsetMultiplier"))
			{
                memcpy(cData, &strData.fTransducerDepthOffsetMultiplier[0], sizeof(float)*iMrows*iNcols);
			}
			else if (!strcmp(cFieldNames[iField], "Depth"))
			{
              	memcpy(cData, &strData.fDepth[0], sizeof(float)*iMrows*iNcols);
			}
			else if (!strcmp(cFieldNames[iField], "AcrossDist"))
			{
                memcpy(cData, &strData.fAcrossDist[0], sizeof(float)*iMrows*iNcols);
			}
			else if (!strcmp(cFieldNames[iField], "AlongDist"))
			{
                memcpy(cData, &strData.fAlongDist[0], sizeof(float)*iMrows*iNcols);
            }
			else if (!strcmp(cFieldNames[iField], "BeamDepressionAngle"))
			{
                memcpy(cData, &strData.fBeamDepressionAngle[0], sizeof(float)*iMrows*iNcols);
			}
			else if (!strcmp(cFieldNames[iField], "BeamAzimuthAngle"))
			{
                memcpy(cData, &strData.fBeamAzimuthAngle[0], sizeof(float)*iMrows*iNcols);
			}
			else if (!strcmp(cFieldNames[iField], "Range"))
			{
                memcpy(cData, &strData.fRange[0], sizeof(float)*iMrows*iNcols);
			}
			else if (!strcmp(cFieldNames[iField], "QualityFactor"))
			{
                memcpy(cData, &strData.fQualityFactor[0], sizeof(float)*iMrows*iNcols);
			}
			else if (!strcmp(cFieldNames[iField], "LengthDetection"))
			{
                memcpy(cData, &strData.fLengthDetection[0], sizeof(float)*iMrows*iNcols);
			}
			else if (!strcmp(cFieldNames[iField], "Reflectivity"))
			{
                memcpy(cData, &strData.fReflectivity[0], sizeof(float)*iMrows*iNcols);
			}
			mxFree(iTabDims);
                   
            
        }
        // Affectation de chaque champ dans la structure de sortie.
        mxSetFieldByNumber(plhs[0], 0, iField, mxArrFout);
        cData = NULL;
        
        
    } // fin de la boucle sur les champs de prhs[4]
 	
      
    //mxFree(cFieldNames);
    mxFree(iClassIDflags);
    mxFree(cNomFic);
    
    // Lib�ration (forc�e) de la structure.
    strData.cEmModel = NULL;
    strData.cSystemSerialNumber = NULL;
    strData.fDate= NULL;
    strData.fNbMilliSec= NULL;
    strData.fPingCounter= NULL;
    strData.fHeading= NULL;
    strData.fSoundSpeed= NULL;              
    strData.fTransducerDepth= NULL;
    strData.fNbBeams= NULL;
    strData.fSamplingRate= NULL;
    strData.fTransducerDepthOffsetMultiplier= NULL;
    strData.fDepth= NULL;
    strData.fAcrossDist= NULL;
    strData.fAlongDist= NULL;
    strData.fBeamDepressionAngle= NULL;
    strData.fBeamAzimuthAngle= NULL;
    strData.fRange= NULL;
    strData.fQualityFactor= NULL;           
    strData.fLengthDetection= NULL;
    strData.fReflectivity= NULL;
    
	// D�tection des fuites M�moire sous Visual C++
	#ifdef _MSC_VER
		_CrtDumpMemoryLeaks();
	#endif
    

	return;
} // mexReadDepth
