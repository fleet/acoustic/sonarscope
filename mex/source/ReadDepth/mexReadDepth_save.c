// Fichier mexRead_Depth.c
//
// SYNTAX :
//   [ADU] = mexRead_Depth(ADU)
// 
//  
// OUTPUT PARAMETERS : 
// 
// EXAMPLES :
//   nomFic = getNomFicDatabase('EM1002_0001_20040916_054013_raw.all')
//   a = cl_simrad_all('nomFic', nomFic);
//   [ADU] = mexReadDepth(ADU)
// 
// COMPIL :
//   mex mexRead_Depth.c Read_Depth.c findMaxNbBeams.c utilsFunc.c ...
//  -outdir I:\IfremerToolBox\mex\lib (-out .dll sous SP3 pour forc� � cr�er en dll).
//   (le chemin du source doit �tre dans le path)
//
// SEE ALSO : Authors
// AUTHORS  : GLU
// VERSION  : $Id: .c,v 1.0 2005/10/07 08:23:13 Gallou Exp Gallou $
// ----------------------------------------------------------------------------


#include <mex.h>
#include "string.h"
#include <mat.h>

#include "Read_Depth.h"

#define DEBUG 0


void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[]){

    double  *dData,
            *dRetour,
            *dPosition;
    
    int     iBuflen,
            iFlagEndian,
            iNbPosition,
            iN,
            iMrows,
            iNcols,
            iLoop;
    
    char	*cNomFic;
    
    const char **fnames;       /* pointers to field names */
    const int  *dims;
    mxArray    *tmp, *fout;
    char       *pdata, *cStr[1];
    int        iField, iStruct, *classIDflags;
    int        iNstructElems, iNfields, ndim;
  
  
    if (nrhs != 5) 
    {
        mexErrMsgTxt("Error : Number of inputs incorrect.");
        *dRetour = 0;
    }
    if (nlhs != 2) 
    {
        mexErrMsgTxt("Error : Number of outputs incorrect.");
        *dRetour = 0;
    }

    // Assign pointers to each output and input
    iBuflen = mxGetN(prhs[0]);
    
    // R�cup�ration du nom de fichier.
    cNomFic=(char *)mxCalloc(iBuflen+1,sizeof(char));
    mxGetString(prhs[0],cNomFic,iBuflen+1);
    
    // Assign pointers to each output and input
    iFlagEndian = (int)mxGetScalar(prhs[1]);
    
    
    // R�cup�ration des dimensions et tableaux de tailles.
    iN = (int)mxGetScalar(prhs[2]);
    if( DEBUG ) {printf("iN : %d\n",iN);}

    iNbPosition = (int)mxGetNumberOfElements(prhs[3]);
    if( DEBUG ) {printf("iNbPosition : %d\n",iNbPosition);}
    dPosition = (double *)mxGetPr(prhs[3]);

    dData = mxGetPr(prhs[4]);
    
    // V�rification des param�tres d'entr�e.
    if (iN != iNbPosition)
    {
        mexErrMsgTxt("Error : Nb of items in arrays N and Position are not equals. !!!");
        *dRetour = 0;
    }
    if (mxGetClassID(prhs[0]) != mxCHAR_CLASS) 
    {
        mexErrMsgTxt("Error : Input nomFic must be a string.");
        *dRetour = 0;
    }
    if (!mxIsLogical(prhs[1]) || mxIsComplex(prhs[1]) ||
        mxGetN(prhs[1])*mxGetM(prhs[1]) != 1) 
    {
        mexErrMsgTxt("Error : Input flagEndian must be a logical scalar.");
        *dRetour = 0;
    }
	
    if (!mxIsDouble(prhs[2]) || mxIsComplex(prhs[2])) 
    {
        mexErrMsgTxt("Error : Input N must be a double scalar.");
        *dRetour = 0;
    }

    if (!mxIsDouble(prhs[3]) || mxIsComplex(prhs[3])) 
    {
        mexErrMsgTxt("Error : Input Position must be a double Scalar.");
        *dRetour = 0;
    }
     if (!mxIsStruct(prhs[4]) || mxIsComplex(prhs[4])) 
    {
        mexErrMsgTxt("Error : Input/Output Position must be a Structure.");
        *dRetour = 0;
    }
    // Allocation des pointeurs de sorties (plhs : l pour left !) 
    iMrows = mxGetM(prhs[4]);
    iNcols = mxGetN(prhs[4]);
    
    // Get input arguments.
    iNfields = mxGetNumberOfFields(prhs[4]);
    iNstructElems = mxGetNumberOfElements(prhs[4]);
    mexPrintf("Nombre de champs de la structure : %d\n", iNfields);
    mexPrintf("Nombre de niveau de la structure : %d\n", iNstructElems);
    /* Allocate memory  for storing pointers */
    fnames = mxCalloc(iNfields, sizeof(*fnames));
    /* allocate memory  for storing classIDflags */
    classIDflags = mxCalloc(iNfields, sizeof(int));
    for (iField = 0; iField < iNfields; iField++) {
        for (iStruct = 0; iStruct < iNstructElems; iStruct++) {
            tmp = mxGetFieldByNumber(prhs[4], iStruct, iField);
            if (DEBUG)
            {
                mexPrintf("FIELD:%d\t%s%d\n",iField+1, "STRUCT INDEX :", iStruct+1);
                mexPrintf("Type de champs de la structure : %s\n", (char *)mxGetClassName(tmp));
            }
            classIDflags[iField]=mxGetClassID(tmp); 
        }
        fnames[iField] = mxGetFieldNameByNumber(prhs[4],iField);
        if (DEBUG)
        {
            mexPrintf("Nom du champ de la structure : %s\n", fnames[iField]);
        }
    }  

    // Create a 1x1 struct matrix for output */
    ndim = mxGetNumberOfDimensions(prhs[4]);
    dims = mxGetDimensions(prhs[4]);
    plhs[0] = mxCreateStructArray(ndim, dims, iNfields, fnames);
    mexPrintf("Nombre de dimensions en Vertical du param�tre 4 : %d\n", ndim);
    for (iLoop = 0; iLoop < ndim; iLoop++) 
    {
       mexPrintf("Nombre de dimensions dims[%d] : %d\n", iLoop, dims[iLoop]);
    }

    plhs[0] = prhs[4];
    mxSetFieldByNumber(plhs[0], 0, 17, mxCreateString("Bidon\n"));
    plhs[1] = mxCreateDoubleMatrix(1,1, mxREAL);
    dData = mxGetPr(plhs[0]);
    dRetour = mxGetPr(plhs[1]);
    *dRetour = (double)1;
   
    // Call the C subroutine.
    Read_Depth(cNomFic, iFlagEndian, iN, dPosition, dData, dRetour);   

    mxFree(classIDflags);
    mxFree(fnames);
    mxFree(cStr);
    
    return;
} // mexRead_Depth