// Fichier ReadDepth.c
//
// SYNTAX :
//   ReadDepth(cNomFic, iFlagEndian, iN, dPosition, &strData, &iRetour);
//  
// OUTPUT PARAMETERS : 
//   strData : structure de sortie des Datas lues.
//   iRetour : indicateur de validit� de la lecture (1:Ok, 0:KO)
// EXAMPLES :
//   
//   Cf. mexReadDepth.c
// 
// COMPIL :
//   mex -g mexReadDepth.c readDepth.c utilsFunc.c findMaxNbBeams.c ...
//      -outdir C:\IfremerToolBox\mex\lib
//   (-out .dll sous SP3 pour forc� � cr�er en dll).
//   (le chemin du source doit �tre dans le path)
//
// SEE ALSO : Authors
// AUTHORS  : GLU
// VERSION  : $Id: .c,v 1.0 2005/10/07 08:23:13 Gallou Exp Gallou $
// ----------------------------------------------------------------------------

#include <mex.h>  // N�cessaire pour les "mexPrintf" sous MatLab.
#include "readDepth.h"
#include "utilsFunc.h"
#include "string.h"

#define DEBUG 0

// D�tection des fuites M�moire sous Visual C++
#ifdef _MSC_VER
	#define _CRTDBG_MAP_ALLOC
	#include <stdlib.h>
	#include <crtdbg.h>
#endif
  
void readDepth( char *cNomFic, 
                 int iFlagEndian,
                 int iN,
                 double dPosition[],
                 T_DATA *strData, 
                 int *iRetour)
{ 
    int     iFileSize = 0,
            iNbOfBytesInDatagram = 0,
            iRead,
            iVal,
            iNbLoopRead,
            iLoopRead,
            iLoopBeam,
            iNbBlocsLus;
    
    short int   siRead,
                siVal;
    
    unsigned short int  usiRead,
                        usiVal;
    
    char            cVal;
    
    unsigned char   ucSTX,
                    ucTypeOfDatagram,
                    ucMaxNbBeams,
                    ucNbBeams,
                    ucVal,
                    ucQualityFactor,
                    ucLengthDetection,
                    ucNumBeam;
    
    float           fZResol,
                    fXYResol,
                    fDepth,
                    fAcrossDist,
                    fAlongDist,
                    fBeamDepressionAngle,
                    fBeamAzimuthAngle,
                    fRange,
                    fQualityFactor,
                    fLengthDetection,
                    fReflectivity;

    char            cBuffer[3000];  // Nominalement, 111(faisceaux)x16(octets de data).

    FILE            *fp;
         
    
    if( DEBUG ) {
        mexPrintf ("Nom du fichier � lire : %s\n", cNomFic);
    }
    
    // R�cup�ration du nom de fichier.
    iNbLoopRead = iN;
    
    // Ouverture du fichier.
	fp = fopen(cNomFic,"rb");
    if ( NULL == fp )
	{
        char cErrmsg[]="cl_simrad_all:readDepth : Failed to open '";
        *iRetour = 0;

        strcat(cErrmsg,cNomFic);
        mexErrMsgTxt(cErrmsg);
    }
    
    for (iLoopRead = 0; iLoopRead < iNbLoopRead; iLoopRead++)
    {
        // Positionnement en d�but de fichier + offset.
        fseek(fp, (int)dPosition[iLoopRead], SEEK_SET);
        
        // Lecture des Data.
        iNbBlocsLus = fread( &iVal, sizeof(int), 1, fp);  // Int32
        iNbOfBytesInDatagram = iVal;
        if (iFlagEndian == 1) {
            iICodageEndian(iVal, &iNbOfBytesInDatagram);
        }
        if( DEBUG ) {
            mexPrintf("iNbOfBytesInDatagram\t\t:\t%d\n", iNbOfBytesInDatagram);
        }
        *iRetour = iReadError(fp, "NbofBytesinDataGram",1 , iNbBlocsLus, iLoopRead);
        if (*iRetour == 0)
        {
            return;
        }
                
        iNbBlocsLus = fread(&ucSTX, sizeof(unsigned char), 1, fp);
        if( DEBUG ) {
            mexPrintf("STX\t\t\t\t\t\t\t:\t%d\n", (int)ucSTX);
        }
        *iRetour = iReadError(fp, "STX",1 , iNbBlocsLus, iLoopRead);
        if (*iRetour == 0)
        {
            return;
        }
        
        iNbBlocsLus = fread(&ucTypeOfDatagram, sizeof(unsigned char), 1, fp);
        if( DEBUG ) {
            mexPrintf("ucTypeOfDatagram\t\t:\t%c\n", ucTypeOfDatagram);
        }
        *iRetour = iReadError(fp, "TypeOfDatagram",1 , iNbBlocsLus, iLoopRead);
        if (*iRetour == 0)
        {
            return;
        }
        
        iNbBlocsLus = fread(&usiRead, sizeof(unsigned short int), 1, fp);  // Int16 non-sign�
        usiVal = usiRead;
        if (iFlagEndian == 1) {
            iUICodageEndian(usiRead, &usiVal);
        }
        *iRetour = iReadError(fp, "EmModel",1 , iNbBlocsLus, iLoopRead);
        if (*iRetour == 0)
        {
            return;
        }
        sprintf(strData->cEmModel, "%d", usiVal);
        if( DEBUG ) {
            mexPrintf("EmModel\t\t\t\t:\t%s\n", strData->cEmModel);
        }        
        
        iNbBlocsLus = fread(&iRead, sizeof(int), 1, fp); // Int32
        iVal = iRead;
        *iRetour = iReadError(fp, "Date",1 , iNbBlocsLus, iLoopRead);
        if (*iRetour == 0)
        {
            return;
        }
        if (iFlagEndian == 1) {
            iICodageEndian(iRead, &iVal);
        }
        strData->fDate[iLoopRead] = (float)iVal;
        if( DEBUG ) {
            mexPrintf("Date [%d]\t\t:\t%f\n", iLoopRead, strData->fDate[iLoopRead]);
        }
        
        iNbBlocsLus = fread(&iRead, sizeof(int), 1, fp); // Int32
        iVal = iRead;
        *iRetour = iReadError(fp, "NbMilliSec",1 , iNbBlocsLus, iLoopRead);
        if (*iRetour == 0)
        {
            return;
        }
        if (iFlagEndian == 1) {
            iICodageEndian(iRead, &iVal);
        }
        strData->fNbMilliSec[iLoopRead] = (float)iVal;
        if( DEBUG ) {
            mexPrintf("NbMilliSec(%d)\t\t\t:\t%d\n", iLoopRead, strData->fNbMilliSec[iLoopRead]);
        }
        
        iNbBlocsLus = fread(&usiRead, sizeof(unsigned short int), 1, fp);
        *iRetour = iReadError(fp, "PingCounter",1 , iNbBlocsLus, iLoopRead);
        if (*iRetour == 0)
        {
            return;
        }
        usiVal = usiRead;
        if (iFlagEndian == 1) {
            iUICodageEndian(usiRead, &usiVal);
        }
        strData->fPingCounter[iLoopRead] = (float)usiVal;
        if( DEBUG ) {
            mexPrintf("PingCounter [%d]\t\t:\t%f\n", iLoopRead, strData->fPingCounter[iLoopRead]);
        }
        
        iNbBlocsLus = fread(&usiRead, sizeof(unsigned short int), 1, fp); // Int16 non-sign�
        usiVal = usiRead;
        *iRetour = iReadError(fp, "Serial Number",1 , iNbBlocsLus, iLoopRead);
        if (*iRetour == 0)
        {
            return;
        }
        if (iFlagEndian == 1) {
            iUICodageEndian(usiRead, &usiVal);
        }
        sprintf(strData->cSystemSerialNumber, "%d", usiVal);
        if( DEBUG ) {
            mexPrintf("Serial Number\t\t\t:\t%s\n", strData->cSystemSerialNumber);
        }
        
        iNbBlocsLus = fread(&usiRead, sizeof(unsigned short int), 1, fp);  // Int16 non-sign�
        usiVal = usiRead;
        *iRetour = iReadError(fp, "Heading",1 , iNbBlocsLus, iLoopRead);
        if (*iRetour == 0)
        {
            return;
        }
        if (iFlagEndian == 1) {
            iUICodageEndian(usiRead, &usiVal);
        }
        strData->fHeading[iLoopRead] = (float)usiVal/100; // en deg.
        if( DEBUG ) {
            mexPrintf("Heading [%d]\t\t\t:\t%f\n", iLoopRead, strData->fHeading[iLoopRead]);
        }
        
        iNbBlocsLus = fread(&usiRead, sizeof(unsigned short int), 1, fp);  // Int16 non-sign�
        usiVal = usiRead;
        *iRetour = iReadError(fp, "Sound Speed",1 , iNbBlocsLus, iLoopRead);
        if (*iRetour == 0)
        {
            return;
        }
        if (iFlagEndian == 1) {
            iUICodageEndian(usiRead, &usiVal);
        }
        strData->fSoundSpeed[iLoopRead] =  (float)usiVal/10;    // en m/s
        if( DEBUG ) {
            mexPrintf("Sound Speed [%d]\t\t\t:\t%f\n", iLoopRead, strData->fSoundSpeed[iLoopRead]);
        }
        
        iNbBlocsLus = fread(&usiRead, sizeof(unsigned short int), 1, fp);  // Int16 non-sign�
        usiVal = usiRead;
        *iRetour = iReadError(fp, "Transducter Depth",1 , iNbBlocsLus, iLoopRead);
        if (*iRetour == 0)
        {
            return;
        }
        if (iFlagEndian == 1) {
            iUICodageEndian(usiRead, &usiVal);
        }
        strData->fTransducerDepth[iLoopRead] = (float)usiVal/100;    // en m
        if( DEBUG ) {
            mexPrintf("Transducter Depth [%d]\t:\t%f\n", iLoopRead, strData->fTransducerDepth[iLoopRead]);
        }
        
        iNbBlocsLus = fread(&ucMaxNbBeams, sizeof(unsigned char), 1, fp); // Int8
        if( DEBUG ) {
            mexPrintf("NbMaxBeams\t\t\t\t:\t%d\n", (int)ucMaxNbBeams);
        }
        *iRetour = iReadError(fp, "MaxNbBeams",1 , iNbBlocsLus, iLoopRead);
        if (*iRetour == 0)
        {
            return;
        }
                
        iNbBlocsLus = fread(&ucNbBeams, sizeof(unsigned char), 1, fp); // Int8
        strData->fNbBeams[iLoopRead] = (float)ucNbBeams;
        if( DEBUG ) {
            mexPrintf("dNbBeams[%d]\t\t\t:\t%f\n", iLoopRead, strData->fNbBeams[iLoopRead]);
        }
        *iRetour = iReadError(fp, "NbBeams",1 , iNbBlocsLus, iLoopRead);
        if (*iRetour == 0)
        {
            return;
        }
        
        iNbBlocsLus = fread(&ucVal, sizeof(unsigned char), 1, fp); // Int8
        fZResol = (float)ucVal/100;
        if( DEBUG ) {
            mexPrintf("fZResol\t\t\t\t\t:\t%f\n", fZResol);
        }
        *iRetour = iReadError(fp, "fZResol",1 , iNbBlocsLus, iLoopRead);
        if (*iRetour == 0)
        {
            return;
        }
        
        iNbBlocsLus = fread(&ucVal, sizeof(unsigned char), 1, fp); // Int8
        fXYResol = (float)ucVal/100;
        if( DEBUG ) {
            mexPrintf("fXYResol\t\t\t\t:\t%f\n", fXYResol);
        }
        *iRetour = iReadError(fp, "fXYResol",1 , iNbBlocsLus, iLoopRead);
        if (*iRetour == 0)
        {
            return;
        }
        
        if (!strcmp(strData->cEmModel,"3000")) // Si mod�le = EM3000
        {
            iNbBlocsLus = fread(&usiRead, sizeof(unsigned short int), 1, fp);
        }
        else
        {
            iNbBlocsLus = fread(&usiRead, sizeof(unsigned short int), 1, fp);
            usiVal = usiRead;
            if (iFlagEndian == 1) {
                iUICodageEndian(usiRead, &usiVal);
            }
            strData->fSamplingRate[iLoopRead] = (float)usiVal;    // en Hz
            if( DEBUG ) {
               mexPrintf("SamplingRate[%d]\t\t\t:\t%f\n\n", iLoopRead, strData->fSamplingRate[iLoopRead]);
            }
        }
        *iRetour = iReadError(fp, "dSamplingRate",1 , iNbBlocsLus, iLoopRead);
        if (*iRetour == 0)
        {
            return;
        }
        
        iNbBlocsLus = fread(&cBuffer, sizeof(char), 16*(int)ucNbBeams, fp);
        *iRetour = iReadError(fp, "cBuffer", 16*(int)ucNbBeams, iNbBlocsLus, iLoopRead);
        if (*iRetour == 0)
        {
            return;
        }
        // -------------------------------------------------------
        // Boucle sur les Faisceaux des boucles de lecture.
        // -------------------------------------------------------
        for (iLoopBeam = 0; iLoopBeam < (int)ucNbBeams; iLoopBeam++)
        {

            if( DEBUG ) {
                mexPrintf("****** Boucle sur le faisceau : %d ******\n", iLoopBeam);
            }
            if (!strcmp(strData->cEmModel,"300") || !strcmp(strData->cEmModel,"120"))
            {
                memcpy(&usiRead, &cBuffer[16*iLoopBeam], 2*sizeof(char));
                usiVal = usiRead;
                if (iFlagEndian == 1) {
                    iUICodageEndian(usiRead, &usiVal);
                }
                fDepth = (float)usiVal * fZResol;
            }
            else
            {
                memcpy(&siRead, &cBuffer[16*iLoopBeam], 2*sizeof(char));
                siVal = siRead;
                if (iFlagEndian == 1) {
                    iSICodageEndian(siRead, &siVal);
                }
                fDepth = (float)siVal * fZResol;
            }
            if( DEBUG )
            {
                mexPrintf("Depth\t\t\t\t\t:\t%f\n", fDepth); // en m.
            }
            
            memcpy(&siRead, &cBuffer[16*iLoopBeam+2], 2*sizeof(char));
            siVal = siRead;
            if (iFlagEndian == 1) {
                iSICodageEndian(siRead, &siVal);
            }
            fAcrossDist = (float)siVal * fXYResol;    // en m
            if( DEBUG ) 
            {
               mexPrintf("fAcrossDist\t\t\t\t:\t%f\n", fAcrossDist);
            }
            
            memcpy(&siRead, &cBuffer[16*iLoopBeam+4], 2*sizeof(char));
            siVal = siRead;
            if (iFlagEndian == 1) {
                iSICodageEndian(siRead, &siVal);
            }
            fAlongDist = (float)siVal * fXYResol;    // en m
            if( DEBUG ) 
            {
                 mexPrintf("fAlongDist\t\t\t\t:\t%f\n", fAlongDist);
            }
            
            memcpy(&siRead, &cBuffer[16*iLoopBeam+6], 2*sizeof(char));
            siVal = siRead;
            if (iFlagEndian == 1) {
                iSICodageEndian(siRead, &siVal);
            }
            fBeamDepressionAngle = (float)siVal / 100;    // en deg
            if( DEBUG ) 
            {
                mexPrintf("fBeamDepressionAngle\t:\t%f\n", fBeamDepressionAngle);
            }
            
            memcpy(&siRead, &cBuffer[16*iLoopBeam+8], 2*sizeof(char));
            siVal = siRead;
            if (iFlagEndian == 1) {
                iSICodageEndian(siRead, &siVal);
            }
            fBeamAzimuthAngle = (float)siVal /100;    // en deg
            if( DEBUG ) 
            {
                mexPrintf("fBeamAzimuthAngle\t\t:\t%f\n", fBeamAzimuthAngle);
            }
            
            // Distance en nombre d'echantillons Aller-Retour
            memcpy(&usiRead, &cBuffer[16*iLoopBeam+10], 2*sizeof(char));
            usiVal = usiRead;
            if (iFlagEndian == 1) 
            {
                iUICodageEndian(usiRead, &usiVal);
            }
            fRange = (float)usiVal; 
            if( DEBUG ) 
            {
                mexPrintf("fRange\t\t\t\t\t:\t%f\n", fRange);
            }
            
            ucQualityFactor = cBuffer[16*iLoopBeam+12];
            fQualityFactor = (float)ucQualityFactor;
            if( DEBUG ) 
            {
                mexPrintf("fQualityFactor\t\t\t:\t%f\n", fQualityFactor);
            }
            
            ucLengthDetection = cBuffer[16*iLoopBeam+13];
            fLengthDetection = (float)ucLengthDetection;
            if( DEBUG ) 
            {
                mexPrintf("ucLengthDetection\t\t:\t%d\n", ucLengthDetection);
            }
            
            cVal = cBuffer[16*iLoopBeam+14];
            fReflectivity = (float)(cVal) * (float)0.5;
            if( DEBUG )
            {
                mexPrintf("fReflectivity\t\t\t:\t%f\n", fReflectivity);
            }
            
            ucNumBeam = cBuffer[16*iLoopBeam+15];
            if( DEBUG ) 
            {
                mexPrintf("ucNumBeam\t\t\t\t:\t%d\n", (int)ucNumBeam);
            }
            
            // 1�re expression conserv�e pour comparaison avec ancien code.
            if ((int)ucNumBeam >=0 && (int)ucNumBeam <= (int)ucMaxNbBeams)
            {
                if( DEBUG ) {
                    mexPrintf("-fDepth\t\t\t\t:\t%f\n", -fDepth);
                }
                fDepth = -fDepth;
                strData->fDepth[iLoopRead*(int)ucMaxNbBeams+iLoopBeam] = fDepth;
                strData->fAcrossDist[iLoopRead*(int)ucMaxNbBeams+iLoopBeam] = fAcrossDist;
                strData->fAlongDist[iLoopRead*(int)ucMaxNbBeams+iLoopBeam] = fAlongDist;
                if (fAcrossDist < 0)
                {
                    strData->fBeamDepressionAngle[iLoopRead*(int)ucMaxNbBeams+iLoopBeam] = fBeamDepressionAngle - 90;
                    strData->fBeamAzimuthAngle[iLoopRead*(int)ucMaxNbBeams+iLoopBeam] = fBeamAzimuthAngle - 360;
                }
                else
                {
                    strData->fBeamDepressionAngle[iLoopRead*(int)ucMaxNbBeams+iLoopBeam] = 90 - fBeamDepressionAngle;
                    strData->fBeamAzimuthAngle[iLoopRead*(int)ucMaxNbBeams+iLoopBeam]= fBeamAzimuthAngle;
                }
                strData->fRange[iLoopRead*(int)ucMaxNbBeams+iLoopBeam] = fRange;
                strData->fQualityFactor[iLoopRead*(int)ucMaxNbBeams+iLoopBeam] = fQualityFactor;
                strData->fLengthDetection[iLoopRead*(int)ucMaxNbBeams+iLoopBeam] = fLengthDetection;
                strData->fReflectivity[iLoopRead*(int)ucMaxNbBeams+iLoopBeam] = fReflectivity;
            }
            
            
        } // fin de la boucle sur les faisceaux
        
   } // fin de boucle de lecture.

   
   
//     iCurrentPosition = ftell(fp);       // save current position
//     fseek(fp,0,SEEK_END);               // seek to end of file
//     iFileSize = ftell(fp);              // get current position = file size
//     fseek(fp,iCurrentPosition,SEEK_SET);// seek to end of file
//     if( DEBUG ) {
//         mexPrintf("Taille du fichier en otects  : %d\n", iFileSize);
//     }

    *iRetour = 1; 
    fclose(fp);

	// D�tection des fuites M�moire sous Visual C++
	#ifdef _MSC_VER
		_CrtDumpMemoryLeaks();
	#endif
     
    return;
} // readDepth
