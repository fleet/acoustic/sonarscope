// Fichier ReadDepth.h, d�claration des prototypes de la fonction c du m�me nom.
//
// SEE ALSO : ReadDepth.c
// AUTHORS  : GLU
// VERSION  : $Id: .c,v 1.0 2005/10/07 08:23:13 Gallou Exp Gallou $
// ----------------------------------------------------------------------------


#ifndef READ_DEPTH_H
#define READ_DEPTH_H

typedef struct  {
        float
                *fDate,
                *fNbMilliSec,
                *fPingCounter,
                *fHeading,
                *fSoundSpeed,              
                *fTransducerDepth,
                *fNbBeams,
                *fSamplingRate,
                *fTransducerDepthOffsetMultiplier,
                *fDepth,
                *fAcrossDist,
                *fAlongDist,
                *fBeamDepressionAngle,
                *fBeamAzimuthAngle,
                *fRange,
                *fQualityFactor,           
                *fLengthDetection,
                *fReflectivity;

        char    *cEmModel,          
                *cSystemSerialNumber;
        
} T_DATA;

void ReadDepth( char *cNomFic, 
                    int iFlagEndian,
                    int iN,
                    double dPosition[],
                    T_DATA *strData, 
                    int *iRetour);

#endif
