
nomFic = getNomFicDatabase('EM1002_0001_20040916_054013_raw.all');
a = cl_simrad_all('nomFic', nomFic);


%% Nouvelle m�thode (mex Function), 10 appels
% tic
% for i=1:10
%      Data_new = read_depth_new(a)
%      clear Data_new; % Tps infime
% end
% toc

%% Ancienne m�thode (mex Function), 10 appels
tic
for i=1:10
    Data_old = read_depth_old(a);
    clear Data_old; % Tps infime
end
toc