// Fichier utilsFunc.c, d'utilitaires de fonctions C.
//
// SYNTAX :
//  
// OUTPUT PARAMETERS : 
// 
// EXAMPLES :
//   nomFic = getNomFicDatabase('EM1002_0001_20040916_054013_raw.all')
//   a = cl_simrad_all('nomFic', nomFic);
//   [ADU] = mexReadDepth(ADU)
// 
// COMPIL :
//   Cf. mexReadDepth.m (et appel dans mexRead_Depth.c)
//
// SEE ALSO : Authors
// AUTHORS  : GLU
// VERSION  : $Id: .c,v 1.0 2005/10/07 08:23:13 Gallou Exp Gallou $
// ----------------------------------------------------------------------------


#include "mex.h"  // N�cessaire pour les "mexPrintf" sous MatLab.
#include "utilsFunc.h"

////////////////////////////////////////////////////////////////////////////
// Nom :    iICodageEndian
// Object : Fonction de transcodage Little Endian-Big ou l'inverse 
//          pour des entiers.
//////////////////////////////////////////////////////////////////////////// 
int iICodageEndian(	int iDataIn,
                    int *iDataOut)
{

int iIndex;
union unTransLEndianBEndian
{
	unsigned int	iData;		// Entier � inverser
	unsigned char 	cData[4];   // Tableau d'octets. 
} unTransLEBEIn, unTransLEBEOut;

	unTransLEBEIn.iData = iDataIn;
	// Inversion octet par octet de la cha�ne �quivalente � l'entier stock�.
	for (iIndex = 0; iIndex <4;iIndex++)
	{
		unTransLEBEOut.cData[iIndex] = unTransLEBEIn.cData[3-iIndex];
	}
	*iDataOut = unTransLEBEOut.iData;
	
return 0;

}   // iICodageEndian

////////////////////////////////////////////////////////////////////////////
// Nom :    iSICodageEndian
// Object : Fonction de transcodage Little Endian-Big ou l'inverse 
//          pour des entiers courts sign�s.
//////////////////////////////////////////////////////////////////////////// 
int iSICodageEndian(	short int siDataIn,
                        short int *siDataOut)
{

int iIndex;
union unTransLEndianBEndian
{
	short int   	iData;		// Entier � inverser
	unsigned char 	cData[2];   // Tableau d'octets. 
} unTransLEBEIn, unTransLEBEOut;

	unTransLEBEIn.iData = siDataIn;
	// Inversion octet par octet de la cha�ne �quivalente � l'entier stock�.
	for (iIndex = 0; iIndex <2;iIndex++)
	{
        unTransLEBEOut.cData[iIndex] = unTransLEBEIn.cData[1-iIndex];        
	}
	*siDataOut = unTransLEBEOut.iData;
	
return 0;

}   // iSICodageEndian

////////////////////////////////////////////////////////////////////////////
// Nom :    iUICodageEndian
// Object : Fonction de transcodage Little Endian-Big ou l'inverse pour des 
//          entiers courts non-sign�s.
////////////////////////////////////////////////////////////////////////////
int iUICodageEndian(	unsigned short int uiDataIn,
                        unsigned short int *uiDataOut)
{

int iIndex;
union unTransLEndianBEndian
{
	unsigned int	iData;		// Entier � inverser
	unsigned char 	cData[2];   // Tableau d'octets. 
} unTransLEBEIn, unTransLEBEOut;

	unTransLEBEIn.iData = uiDataIn;
    // Inversion octet par octet de la cha�ne �quivalente � l'entier stock�.
	for (iIndex = 0; iIndex <2;iIndex++)
	{
        unTransLEBEOut.cData[iIndex] = unTransLEBEIn.cData[1-iIndex];
	}
	*uiDataOut = unTransLEBEOut.iData;
	
return 0;
}   // iUICodageEndian


////////////////////////////////////////////////////////////////////////////
// Nom :    iReadError
// Object : R�daction d'une erreur si nombre de data lus incorrect ou fin de
//          fichier rencontr�e.
////////////////////////////////////////////////////////////////////////////
int iReadError(	FILE *fp,
                const char *cVarError,
                int iNbBlocsALire,
                int iNbBlocs,
                int iLoop)
{
    if (iNbBlocs != iNbBlocsALire)
    {
        mexPrintf("Nombre de blocs lus (iLoop = %d): %d\n", iLoop, iNbBlocs);
        mexPrintf("*** Erreur de lecture du fichier (variable : %s) ! ***\n", cVarError);
        return 0;
    }
    if (feof(fp))
    {
        mexPrintf("*** Fin (iLoop = %d, Variable: %s) de fichier rencontr�e ! :***\n", iLoop, cVarError);
        return 0;
    }
	
return 1;

}   // iReadError
