// Fichier utilsFunc.h, d�claration des prototypes de la fonction c du m�me nom.
//
// SEE ALSO : Read_Depth.c
// AUTHORS  : GLU
// VERSION  : $Id: .c,v 1.0 2005/10/07 08:23:13 Gallou Exp Gallou $
// ----------------------------------------------------------------------------

#ifndef UTILS_FUNC_H
#define UTILS_FUNC_H
#include <stdio.h>


// Fonction de transcodage Little Endian-Big ou l'inverse pour des entiers sign�s.
int iICodageEndian(	int iDataIn,
                    int *iDataOut);

// Fonction de transcodage Little Endian-Big ou l'inverse pour des entiers courts.
int iUICodageEndian(unsigned short int uiDataIn,
					unsigned short int *uiDataOut);

// Fonction de transcodage Little Endian-Big ou l'inverse pour des entiers courts sign�s.
int iSICodageEndian(	short int siDataIn,
                        short int *siDataOut);

// Fonction de r�daction d'une erreur.
int iReadError(	FILE *fp,
                const char *cVarError,
                int iNbBlocsALire,
                int iNbBlocs,
                int iLoop);


#endif
